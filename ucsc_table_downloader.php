<?php
include("simple_html_dom.php");

class UCSC_table_downloader
{
	var $ucsc_table_url = "http://genome.ucsc.edu/cgi-bin/hgTables";
	var $table_html = "";
	var $hgsid = "";
	var $select_name_list = array("clade", "org", "db", "hgta_group", "hgta_track", "hgta_table");
	var $tree = array();
	var $table_limit = array(
		"clade" => array("mammal"=>1)
		,"org" => array("Human" => 1, "Mouse" => 1)
		,"db" => array("hg18"=>1, "hg19"=>1, "mm9"=>1, "mm10"=>1)
		,"hgta_group" => array("genes"=>1)
		,"hgta_track" => array(
			"knownGene"=>1
			,"refGene"=>1
			, "wgEncodeGencodeV19"=>1
			, "ensGene"=>1
			, "lincRNAsTranscripts"=>1
			, "wgRna"=>1
			, "tRNAs"=>1
			, "wgEncodeGencodeVM2"=>1
			, "miRNA"=>1
			, ""=>1
			, ""=>1
		)
	);
	var $key_name = array();
	var $output = "anno";
	var $limit_level = 6;
	function UCSC_table_downloader($limit = 6)
	{
		$this->limit_level = $limit; 
		//$this->get_main_table_html();
		//$this->get_hgsid();
		//$this->get_select_list("clade");
		//$this->parse_all_table();
	}
	function para2array($para)
	{
		$r = array();
		$tmp = explode("&", $para);
		foreach($tmp as $t)
		{
			$ps = explode("=", $t);
			$r[ $ps[0] ] = $ps[1];
		}
		return $r;
	}
	function parse_all_table()
	{
		$url = $this->ucsc_table_url;
		$tree = $this->get_select_list( 0 );
		print_r($tree);
		
	}
	function get_table($para)
	{
		//http://genome.ucsc.edu/cgi-bin/hgTables?clade=mammal&org=Human&db=hg19&hgta_group=genes&hgta_track=knownGene&hgta_table=knownGene&hgta_outputType=bed&hgta_compressType=none&hgta_doGetBed=get%20BED&fbQual=
		$type = array("", "utr5", "utr3", "cds");
		foreach($type as $t)
		{
			//hgta_outputType: primaryTable, bed, gff
			//hgta_compressType: none gzip
			$para .= "hgta_outputType=bed&hgta_compressType=none&hgta_doGetBed=get%20BED&fbQual=$t";
			$paras = $this->para2array($para);
			$url = $this->ucsc_table_url ."?". $para;
			$track = str_replace(" ", "_", $this->key_name[$paras["hgta_track"]] );
			$track = str_replace("/", "-", $track );
			if($t=="")
				$filename = "UCSC.".$paras["db"].".".$track.".all."."bed";
			else
				$filename = "UCSC.".$paras["db"].".".$track.".$t."."bed";
			$output_filename = $this->output . "/$filename";
			$output_filename = str_replace("(", "-", $output_filename );
			$output_filename = str_replace(")", "", $output_filename );
			$cmd = "curl \"$url\" > $output_filename 2>/dev/null ";
			shell_exec($cmd);
			
			if(filesize($output_filename) < 500)
				unlink($output_filename);
			else
				echo "$output_filename\n";
		}
	}
	function get_select_list($level, $para="")
	{
		if ($level == $this->limit_level)
		{
			if($level == count($this->select_name_list))
			$this->get_table($para);
			return "$para";
		}
			
		$this->get_main_table_html($para);
		
		$name = $this->select_name_list[$level];
		$options = $this->table_html->find("select[name=$name] option");
		$opts = array();
		foreach($options as $opt)
		{
			$value = $opt->value;
			$text = $opt->plaintext;
			
			if( !isset($this->table_limit[ $this->select_name_list[$level] ][$value] ) && count(@$this->table_limit[ $this->select_name_list[$level] ]) != 0 )
				continue;
			if($level != 5)
				$this->key_name[$value] = trim($text);
			if($level != count($this->select_name_list) )
			{
				$ropts = $this->get_select_list($level+1, $para.$this->select_name_list[$level]."=$value&");
				$opts[$value] = $ropts;
			}
			//table 只留第一個
			if($level == 5)
				break;
		}
		return $opts;
	}
	function get_main_table_html($para)
	{
		$options = Array(
			CURLOPT_URL => "{$this->ucsc_table_url}?$para",
			CURLOPT_RETURNTRANSFER => true
		);
		$html = $this->query_curl($options);
		$this->table_html = str_get_html($html);
	}
	function get_hgsid()
	{
		$this->hgsid = $this->table_html->find('input[name=hgsid]', 0)->value;
		return $this->hgsid;
	}
	function query_curl($opts)
	{
		$ch = curl_init();
		$opts[CURLOPT_COOKIEJAR] = "cookie.txt";
		$opts[CURLOPT_COOKIEFILE] = "cookie.txt";
		curl_setopt_array($ch, $opts);
		$response = curl_exec($ch); 
		curl_close($ch);
		return $response;
	}
	
};

$down = new UCSC_table_downloader();

$down->table_limit = array(
	"clade" => array("mammal"=>1)
	,"org" => array("Human" => 1, "Mouse" => 1)
	,"db" => array("hg18"=>1, "hg19"=>1, "mm9"=>1, "mm10"=>1)
	,"hgta_group" => array("genes"=>1)
	,"hgta_track" => array(
		"knownGene"=>1
		,"refGene"=>1
		, "wgEncodeGencodeV19"=>1
		, "ensGene"=>1
		, "lincRNAsTranscripts"=>1
		, "wgRna"=>1
		, "tRNAs"=>1
		, "wgEncodeGencodeVM2"=>1
		, "miRNA"=>1
		, ""=>1
		, ""=>1
	)
);

$down->table_limit = array(
	"clade" => array("mammal"=>1)
	,"org" => array("Human" => 1, "Mouse" => 1)
	,"db" => array("hg18"=>1, "hg19"=>1, "mm9"=>1, "mm10"=>1)
	,"hgta_group" => array("varRep"=>1, "rep")
	,"hgta_track" => array(
		"rmsk"=>1
		,"simpleRepeat"=>1
		, "microsat"=>1
		, "snp138Common"=>1
		, "snp137Common"=>1
		, "snp138"=>1
		, "snp137"=>1
		
	)
);

$down -> parse_all_table();

?>