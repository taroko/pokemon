<?php
//php pipeline/typelist_generator.php basespace yy.hpp

if ($argc==1)
	$obj = new TypeListFillWorker();
else if ($argc==2)
	$obj = new TypeListFillWorker($argv[1]);
else if ($argc==3)
	$obj = new TypeListFillWorker($argv[1], $argv[2]);
else if ($argc==4)
	$obj = new TypeListFillWorker($argv[1], $argv[2], $argv[3]);
else
	exit (1);

//TypeList_A	lendist x22x2 + bio_seq x84 + miRNA_seq x60 + other x1 = 189
$obj->PrintStartStr();

//biotype lendist (protein coding gene, miRNA, lincRNA, snoRNA ...)
//CalReadCountClass=GMOnly/PMOnly/GMPM, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength analyzer x6 + Printer (x6x2+4) = 22 - 2
$obj->ListExhaustiveLendist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("CalReadCountGMPM", "CalReadCountGMOnly", "CalReadCountPMOnly"), 
		$obj->GetDefaultSet(AnalyzerEnum::LenDist));

//所有 miRNA detail(mir-1, mir-2 ...) lendist 
//CalReadCountClass=GMOnly/PMOnly/GMPM, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength analyzer x6 + Printer (x6x2+4) = 22 + 4
$obj->ListExhaustiveLendist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("CalReadCountGMPM", "CalReadCountGMOnly", "CalReadCountPMOnly"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array (
				 "AnaParaLenDist::DbDepthNameType" 		=> "boost::mpl::string<'mi', 'RNA'>"
				,"AnaParaLenDist::DbDepth2NameType"		=> "boost::mpl::string<'-1'>")));

//biotype GetReadSeq=tail/seed/first 1nt/last 1nt/first 2nt/last 2nt seq, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength
//lendist: analyzer (x6x2) + Printer (x6x2x6) = 84
$obj->ListExhaustiveBiotypeSeqdist (
		array ("GetReadFullLength", "GetReadPrefixLength"),//array ("CalReadCountGMPM", "CalReadCountGMOnly", "CalReadCountPMOnly"), 
		array ("GetReadTailing", "GetReadSeed <1, 6>", "GetReadFirstNLastComposition <0, 0, 1>", "GetReadFirstNLastComposition <1, 0, 1>", "GetReadFirstNLastComposition <0, 0, 2>", "GetReadFirstNLastComposition <1, 0, 2>"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array ("AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountGMPM")));

//miRNA subclasses GetReadSeq=tail/seed/first 1nt/last 1nt/first 2nt/last 2nt seq, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength 
//lendist: analyzer (x6x2) + Printer (x6x2x4) = 60
$obj->ListExhaustiveMiRNASeqdist (//ListExhaustiveBiotypeSeqdist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("GetReadTailing", "GetReadSeed <1, 6>", "GetReadFirstNLastComposition <0, 0, 1>", "GetReadFirstNLastComposition <1, 0, 1>", "GetReadFirstNLastComposition <0, 0, 2>", "GetReadFirstNLastComposition <1, 0, 2>"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array (
				 "AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountGMPM"
				,"AnaParaLenDist::DbDepthNameType" 		=> "boost::mpl::string<'mi', 'RNA'>"
				,"AnaParaLenDist::DbDepth2NameType"		=> "boost::mpl::string<'-1'>"
				)));

//rest analysis
$obj->AddAnalyzerSet($obj->GetDefaultSet(AnalyzerEnum::MapPrinter),"");
$obj->PrintEndStr ("_A");

//TypeList_B	bio_seq x84 + miRNA_seq x60 + other x2 = 146
$obj->PrintStartStr();

//biotype GetReadSeq=tail/seed/first 1nt/last 1nt/first 2nt/last 2nt seq, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength
//lendist: analyzer (x6x2) + Printer (x6x2x6) = 84
$obj->ListExhaustiveBiotypeSeqdist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("GetReadTailing", "GetReadSeed <1, 6>", "GetReadFirstNLastComposition <0, 0, 1>", "GetReadFirstNLastComposition <1, 0, 1>", "GetReadFirstNLastComposition <0, 0, 2>", "GetReadFirstNLastComposition <1, 0, 2>"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array ("AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountGMOnly")));

//miRNA subclasses GetReadSeq=tail/seed/first 1nt/last 1nt/first 2nt/last 2nt seq, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength 
//lendist: analyzer (x6x3) + Printer (x6x3x6) = 60
$obj->ListExhaustiveMiRNASeqdist (//ListExhaustiveBiotypeSeqdist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("GetReadTailing", "GetReadSeed <1, 6>", "GetReadFirstNLastComposition <0, 0, 1>", "GetReadFirstNLastComposition <1, 0, 1>", "GetReadFirstNLastComposition <0, 0, 2>", "GetReadFirstNLastComposition <1, 0, 2>"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array (
				 "AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountGMOnly"
				,"AnaParaLenDist::DbDepthNameType" 		=> "boost::mpl::string<'mi', 'RNA'>"
				,"AnaParaLenDist::DbDepth2NameType"		=> "boost::mpl::string<'-1'>"
				)));

//rest analysis
$obj->AddAnalyzerSet($obj->GetDefaultSet(AnalyzerEnum::HeteroGeneity));
$obj->AddAnalyzerSet($obj->GetDefaultSet(AnalyzerEnum::HeteroGeneityPrinter), "");
$obj->PrintEndStr ("_B");

//TypeList_C	bio_seq x84 + miRNA_seq x60 + other x2 = 146
$obj->PrintStartStr();

//biotype GetReadSeq=tail/seed/first 1nt/last 1nt/first 2nt/last 2nt seq, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength
//lendist: analyzer (x6x2) + Printer (x6x2x6) = 84
$obj->ListExhaustiveBiotypeSeqdist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("GetReadTailing", "GetReadSeed <1, 6>", "GetReadFirstNLastComposition <0, 0, 1>", "GetReadFirstNLastComposition <1, 0, 1>", "GetReadFirstNLastComposition <0, 0, 2>", "GetReadFirstNLastComposition <1, 0, 2>"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array ("AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountPMOnly")));

//miRNA subclasses GetReadSeq=tail/seed/first 1nt/last 1nt/first 2nt/last 2nt seq, GetReadLengthClass=GetReadPrefixLength/GetReadFullLength 
//lendist: analyzer (x6x3) + Printer (x6x3x6) = 60
$obj->ListExhaustiveMiRNASeqdist (//ListExhaustiveBiotypeSeqdist (
		array ("GetReadFullLength", "GetReadPrefixLength"), 
		array ("GetReadTailing", "GetReadSeed <1, 6>", "GetReadFirstNLastComposition <0, 0, 1>", "GetReadFirstNLastComposition <1, 0, 1>", "GetReadFirstNLastComposition <0, 0, 2>", "GetReadFirstNLastComposition <1, 0, 2>"), 
		$obj->AlterArrayByArray (
			$obj->GetDefaultSet(AnalyzerEnum::LenDist),
			array (
				 "AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountPMOnly"
				,"AnaParaLenDist::DbDepthNameType" 		=> "boost::mpl::string<'mi', 'RNA'>"
				,"AnaParaLenDist::DbDepth2NameType"		=> "boost::mpl::string<'-1'>"
				)));

//rest analysis
$obj->AddSeqLogoSet($obj->GetDefaultSet(AnalyzerEnum::SeqLogo));
$obj->AddSeqLogoPrinterSet($obj->GetDefaultSet(AnalyzerEnum::SeqLogoPrinter));
//$obj->AddSeqLogoSet($obj->AlterArrayByArray (
//						$obj->GetDefaultSet(AnalyzerEnum::SeqLogo), 
//							array (
//							 "AnaParaSeqLogo::DbDepthNameType"      => "boost::mpl::string<'-1'>" // Biotype = -1, all name
//							,"AnaParaSeqLogo::DbDepth2NameType"     => "boost::mpl::string<'-3'>"
//						)));
//$obj->AddSeqLogoPrinterSet($obj->GetDefaultSet(AnalyzerEnum::SeqLogoPrinter));
$obj->AddAnalyzerSet($obj->GetDefaultSet(AnalyzerEnum::ToBam));
$obj->AddAnalyzerSet($obj->GetDefaultSet(AnalyzerEnum::ToBwg), "");
$obj->PrintEndStr ("_C");


abstract class AnalyzerEnum
{
	const LenDist = 0;
	const LenDistPrinter = 1;
	const HeteroGeneity = 2;
	const HeteroGeneityPrinter = 3;
	const ToBam = 4;
	const ToBwg = 5;
	const MapPrinter = 6;
	const LenDistPrinterDualAnalyzer = 7;
	const SeqLogo = 8;
	const SeqLogoPrinter = 9;

	const ToBamUnsorted = 10;
};


class TypeListFillWorker
{
	var $LenDist = "typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;\n";
	var $lenPrinter = "typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinter> AnaParaLenDistPrinter;\n";
	var $HeteroGeneity = "typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;\n";
	var $HeteroGeneityPrinter = "typedef AnalyzerParameter <AnalyzerTypes::HeterogeneityPrinter> AnaParaHeterogeneityPrinter;\n";
	var $ToBam = "typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaParaToBam;\n";
	var $ToBwg = "typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaParaToBwg;\n";
	var $MapPrinter = "typedef AnalyzerParameter <AnalyzerTypes::MappabilityPrinter> AnaParaMapPrinter;\n";
	var $lenPrinterDualAnalyzer = "typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinterDualAnalyzer> AnaParaLenDistPrinterDualAnalyzer;\n";
	var $SeqLogo = "typedef AnalyzerParameter <AnalyzerTypes::SequenceLogo> AnaParaSeqLogo;\n";
	var $SeqLogoPrinter = "typedef AnalyzerParameter <AnalyzerTypes::SequenceLogoPrinter> AnaParaSeqLogoPrinter;\n";

	var $head_str = "typedef boost::mpl::vector\n<\n";
	var $tail_str = "> AnalyzerTypeList";//;\n";
  	var $typelist_file;
	var $len_dist_analyzer_count = -1;	//keep record of a current index for mapping lendist analyzer and the corresponding  printer
	var $seq_logo_analyzer_count = -1;	//keep record of a current index for mapping seq logo  analyzer and the corresponding  printer  

	var $default_len_dist = array (
         "AnaParaLenDist::AnalyzerType" 		=> "boost::mpl::int_< AnalyzerTypes::LengthDistribution >"
		,"AnaParaLenDist::FilterType" 			=> "boost::mpl::int_<1>"
		,"AnaParaLenDist::DbIndexType" 			=> "boost::mpl::int_<0>" // first database
		,"AnaParaLenDist::DbDepthType" 			=> "boost::mpl::int_<0>" // classify or gene name
		,"AnaParaLenDist::DbDepthNameType" 		=> "boost::mpl::string<'-1'>" // Biotype = -1, all name
		,"AnaParaLenDist::DbDepth2NameType"		=> "boost::mpl::string<'-3'>"
		,"AnaParaLenDist::GetReadLengthClass" 	=> "GetReadLengthDefault"
		,"AnaParaLenDist::CalReadCountClass" 	=> "CalReadCountDefault"
		,"AnaParaLenDist::GetReadSeqClass" 		=> "GetReadSeqDefault"
	);
	var $default_len_dist_printer = array (
         "AnaParaLenDistPrinter::AnalyzerType" 	=> "boost::mpl::int_< AnalyzerTypes::LenDistPrinter >"
		,"AnaParaLenDistPrinter::AnnoIdx"		=> "boost::mpl::int_<0>"
		,"AnaParaLenDistPrinter::Xaxis"			=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
		,"AnaParaLenDistPrinter::Yaxis"			=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
		,"AnaParaLenDistPrinter::Zaxis"			=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
		,"AnaParaLenDistPrinter::Xlimit"		=> "boost::mpl::int_<0>"
		,"AnaParaLenDistPrinter::Ylimit"		=> "boost::mpl::int_<0>" 
		,"AnaParaLenDistPrinter::Zlimit"		=> "boost::mpl::int_<0>" 
		//,"AnaParaLenDistPrinter::Len","AnaParaLenDistPrinter::Anno","AnaParaLenDistPrinter::Seq","AnaParaLenDistPrinter::PrefixName"
		,"AnaParaLenDistPrinter::IoHandlerType"	=> "IoHandlerOfstream"
	);
	var $default_heterogeneity = array (
         "AnaParaLenDistH::AnalyzerType" 			=> "boost::mpl::int_< AnalyzerTypes::Heterogeneity >"
		,"AnaParaLenDistH::FilterType" 				=> "boost::mpl::int_<1>"
		,"AnaParaLenDistH::DbIndexType"			 	=> "boost::mpl::int_<1>" // second database
        ,"AnaParaLenDistH::DbDepthTypeFor3Or5Prime"	=> "boost::mpl::int_<0>"
        ,"AnaParaLenDistH::DbDepthTypeForName"		=> "boost::mpl::int_<0>"
        ,"AnaParaLenDistH::Db5PrimeName"			=> "boost::mpl::string<'miRN', 'A-5p'>"
        ,"AnaParaLenDistH::Db3PrimeName"			=> "boost::mpl::string<'miRN', 'A-3p'>"
	);
	var $default_heterogeneity_printer = array (
         "AnaParaHeterogeneityPrinter::AnalyzerType" 	=> "boost::mpl::int_< AnalyzerTypes::HeterogeneityPrinter >"
		,"AnaParaHeterogeneityPrinter::AnnoIdx"			=> "boost::mpl::int_<0>"
		,"AnaParaHeterogeneityPrinter::IoHandlerType"	=> "IoHandlerOfstream"
		,"AnaParaHeterogeneityPrinter::XSort"			=> "boost::mpl::bool_<true>"
	);
	var $default_tobam = array (
         "AnaParaToBam::AnalyzerType"			=> "boost::mpl::int_< AnalyzerTypes::ToBam >"
		,"AnaParaToBam::FilterType"  			=> "boost::mpl::int_<1>"
		,"AnaParaToBam::DbIndexType"  			=> "boost::mpl::int_<0>"
        ,"AnaParaToBam::DbDepthType"			=> "boost::mpl::int_<0>"
		,"AnaParaToBam::IoHandlerType"			=> "IoHandlerOfstream"
		,"AnaParaToBam::IoHandlerTypeForBAI"	=> "IoHandlerOfstream"
	);
	var $default_tobam_unsorted = array (
		 "AnaParaToBam::AnalyzerType"	=> "boost::mpl::int_< AnalyzerTypes::ToBam >"
		,"AnaParaToBam::IoHandlerType"	=> "IoHandlerOfstream"
	);
	var $default_tobwg = array (
         "AnaParaToBwg::AnalyzerType"	=> "boost::mpl::int_< AnalyzerTypes::ToBwg >"
		,"AnaParaToBwg::FilterType"  	=> "boost::mpl::int_<1>"
		,"AnaParaToBwg::DbIndexType"  	=> "boost::mpl::int_<0>"
        ,"AnaParaToBwg::DbDepthType"	=> "boost::mpl::int_<0>"
		,"AnaParaToBwg::IoHandlerType"	=> "IoHandlerOfstream"
	);
	var $default_mappability_printer = array (
         "AnaParaMapPrinter::AnalyzerType"	=> "boost::mpl::int_< AnalyzerTypes::MappabilityPrinter >"
		,"AnaParaMapPrinter::IoHandlerType"	=> "IoHandlerOfstream"
	);
	var $default_len_dist_printer_dual_analyzer = array (
         "AnaParaLenDistPrinterDualAnalyzer::AnalyzerType" 	=> "boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer >"
		,"AnaParaLenDistPrinterDualAnalyzer::AnnoIdx"		=> "boost::mpl::int_<1>"
        ,"AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2"		=> "boost::mpl::int_<2>"
		,"AnaParaLenDistPrinterDualAnalyzer::Xaxis"			=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
		,"AnaParaLenDistPrinterDualAnalyzer::Yaxis"			=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
		,"AnaParaLenDistPrinterDualAnalyzer::Zaxis"			=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> >"
		//,"AnaParaLenDistPrinterDualAnalyzer::Len","AnaParaLenDistPrinter::Anno","AnaParaLenDistPrinter::Seq","AnaParaLenDistPrinter::PrefixName"
		,"AnaParaLenDistPrinterDualAnalyzer::IoHandlerType"	=> "IoHandlerOfstream"
	);
	var $default_seq_logo = array (
         "AnaParaSeqLogo::AnalyzerType" 		=> "boost::mpl::int_< AnalyzerTypes::SequenceLogo >"
		,"AnaParaSeqLogo::FilterType" 			=> "boost::mpl::int_<1>"
		,"AnaParaSeqLogo::DbIndexType" 			=> "boost::mpl::int_<0>" // first database
		,"AnaParaSeqLogo::DbDepthType" 			=> "boost::mpl::int_<0>" // classify or gene name
		,"AnaParaSeqLogo::DbDepthNameType" 		=> "boost::mpl::string<'mi', 'RNA'>"
		,"AnaParaSeqLogo::DbDepth2NameType"		=> "boost::mpl::string<'-1'>"
		,"AnaParaSeqLogo::GetReadSeqClass" 		=> "GetReadFirstNLastComposition<0, 0, 15>"
	);
	var $default_seq_logo_printer = array (
         "AnaParaSeqLogoPrinter::AnalyzerType" 	=> "boost::mpl::int_< AnalyzerTypes::SequenceLogoPrinter >"
		,"AnaParaSeqLogoPrinter::AnnoIdx"		=> "boost::mpl::int_<0>"
		,"AnaParaSeqLogoPrinter::IoHandlerType"	=> "IoHandlerOfstream"
		,"AnaParaSeqLogoPrinter::PrefixName"	=> "boost::mpl::string<'Tab', 'le'>"
	);

	public function __construct ($model="", $typelist_path = "/home/oman/pokemon/pipeline/typelist.hpp", $iohandler = "IoHandlerOfstream")
	{
		$this->SetIohandler ($iohandler);		
		if ($model=="basespace")
			$this->SetIohandler_bs ();

		$this->typelist_file = fopen($typelist_path, 'w');
		$output_str = $this->LenDist.$this->lenPrinter.$this->HeteroGeneity.$this->HeteroGeneityPrinter.$this->ToBam.$this->ToBwg.$this->MapPrinter.$this->lenPrinterDualAnalyzer.$this->SeqLogo.$this->SeqLogoPrinter;
		fwrite ($this->typelist_file, $output_str);
	}

	private function SetIohandler_bs ()
	{
		$this->default_tobam["AnaParaToBam::IoHandlerType"] = "IoHandlerBaseSpace";//"IoHandlerBaseSpaceEasyUpload";
		$this->default_tobam["AnaParaToBam::IoHandlerTypeForBAI"] = "IoHandlerBaseSpace";//"IoHandlerBaseSpaceEasyUpload";
		$this->default_tobwg["AnaParaToBwg::IoHandlerType"] = "IoHandlerBaseSpace";//"IoHandlerBaseSpaceEasyUpload";
		$this->default_tobam_unsorted["AnaParaToBam::IoHandlerType"] = "IoHandlerBaseSpace";
	}

	private function SetIohandler ($iohandler)
	{
		foreach ($this->default_len_dist_printer as $key => $value)
			if ($key == "AnaParaLenDistPrinter::IoHandlerType" && $value!=$iohandler)
				$this->default_len_dist_printer["AnaParaLenDistPrinter::IoHandlerType"] = $iohandler;
		foreach ($this->default_heterogeneity_printer as $key => $value)
			if ($key == "AnaParaHeterogeneityPrinter::IoHandlerType" && $value!=$iohandler)
				$this->default_heterogeneity_printer["AnaParaHeterogeneityPrinter::IoHandlerType"] = $iohandler;
		foreach ($this->default_tobam as $key => $value)
			if ($key == "AnaParaToBam::IoHandlerType" && $value!= $iohandler)
				$this->default_tobam["AnaParaToBam::IoHandlerType"] = $iohandler;
		foreach ($this->default_tobwg as $key => $value)
			if ($key == "AnaParaToBwg::IoHandlerType" && $value!= $iohandler)
				$this->default_tobwg["AnaParaToBwg::IoHandlerType"] = $iohandler;
		foreach ($this->default_mappability_printer as $key => $value)
			if ($key == "AnaParaMapPrinter::IoHandlerType" && $value!= $iohandler)
				$this->default_mappability_printer["AnaParaMapPrinter::IoHandlerType"] = $iohandler;

		foreach ($this->default_len_dist_printer_dual_analyzer as $key => $value)
			if ($key == "AnaParaLenDistPrinterDualAnalyzer::IoHandlerType" && $value!=$iohandler)
				$this->default_len_dist_printer_dual_analyzer["AnaParaLenDistPrinterDualAnalyzer::IoHandlerType"] = $iohandler;
		foreach ($this->default_seq_logo_printer as $key => $value)
			if ($key == "AnaParaSeqLogoPrinter::IoHandlerType" && $value!=$iohandler)
				$this->default_seq_logo_printer["AnaParaSeqLogoPrinter::IoHandlerType"] = $iohandler;

		foreach ($this->default_tobam_unsorted as $key => $value)
			if ($key == "AnaParaToBam::IoHandlerType" && $value!=$iohandler)
				$this->default_tobam_unsorted["AnaParaToBam::IoHandlerType"] = "IoHandlerBaseSpace";
	}

	public function PrintStartStr ()
	{
		fwrite ($this->typelist_file, $this->head_str);
	}

	public function GetDefaultSet ($input_enum)
	{
		switch ($input_enum)
		{
			case AnalyzerEnum::LenDist: 
				return $this->default_len_dist;
			case AnalyzerEnum::LenDistPrinter:
				return $this->default_len_dist_printer;
			case AnalyzerEnum::HeteroGeneity: 
				return $this->default_heterogeneity;
			case AnalyzerEnum::HeteroGeneityPrinter:
				return $this->default_heterogeneity_printer;
			case AnalyzerEnum::ToBam: 
				return $this->default_tobam;
			case AnalyzerEnum::ToBwg: 
				return $this->default_tobwg;
			case AnalyzerEnum::MapPrinter: 
				return $this->default_mappability_printer;
			case AnalyzerEnum::ToBamUnsorted:
				return $this->default_tobam_unsorted;
			case AnalyzerEnum::LenDistPrinterDualAnalyzer:
				return $this->default_len_dist_printer_dual_analyzer;

			case AnalyzerEnum::SeqLogo:
				return $this->default_seq_logo;
			case AnalyzerEnum::SeqLogoPrinter:
				return $this->default_seq_logo_printer;
		}
	}

	public function AlterArrayByPair (&$array_in, $key_content, $value_content)
	{
		$array_in[$key_content] = $value_content;
		return $array_in;
	}

	public function AlterArrayByArray (&$array_target, $array_config)
	{
		foreach ($array_config as $key => $value)
			$array_target[$key] = $value;
		return $array_target;
	}

	public function AddAnalyzerSet ($parameter_array, $comma=",")
	{
		$head_str = "\tboost::mpl::map\n\t<\n";
		$tail_str = "\t>";
		fwrite ($this->typelist_file, $head_str);
		foreach ($parameter_array as $key => $value)
		{
			if ($key != $this->EndKey($parameter_array))
				fwrite ($this->typelist_file, "\t\tboost::mpl::pair< ".$key.", ".$value." >,\n");
			else
				fwrite ($this->typelist_file, "\t\tboost::mpl::pair< ".$key.", ".$value." >\n");
		}
		fwrite ($this->typelist_file, $tail_str.$comma."\n");
	}

	public function AddLenDistSet ($parameter_array, $comma=",")
	{
		$this->AddAnalyzerSet ($parameter_array, $comma);
		++$this->len_dist_analyzer_count;
	}

	public function AddSeqLogoSet ($parameter_array, $comma=",")
	{
		$this->AddAnalyzerSet ($parameter_array, $comma);
		++$this->seq_logo_analyzer_count;
	}

	public function AddLenDistPrinterSet ($parameter_array, $comma=",")
	{
		$parameter_array["AnaParaLenDistPrinter::AnnoIdx"] = "boost::mpl::int_<".strval($this->len_dist_analyzer_count).">"; 
		$this->AddAnalyzerSet ($parameter_array, $comma);
	}

	public function AddSeqLogoPrinterSet ($parameter_array, $comma=",")
	{
		$parameter_array["AnaParaSeqLogoPrinter::AnnoIdx"] = "boost::mpl::int_<".strval($this->seq_logo_analyzer_count).">"; 
		$this->AddAnalyzerSet ($parameter_array, $comma);
	}

	private function EndKey ($input_array)
	{
		end($input_array);
		return key($input_array);
	}

	public function GetAllMiRNASeqPrinter ()
	{
		//FIG_SEQ_#1, sequence trait by miRNA subclass PieChart
		$this->AddLenDistPrinterSet (
				$this->AlterArrayByArray (
					$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
					array (
						 "AnaParaLenDistPrinter::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
						,"AnaParaLenDistPrinter::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Xlimit"	=> "boost::mpl::int_<500>"
						,"AnaParaLenDistPrinter::Ylimit"	=> "boost::mpl::int_<0>"
						,"AnaParaLenDistPrinter::Zlimit"	=> "boost::mpl::int_<0>"
						,"AnaParaLenDistPrinter::XSort"		=> "boost::mpl::bool_<true>"
//						,"AnaParaLenDistPrinter::YSort"		=> "boost::mpl::bool_<true>"
						)));
		//FIG_SEQ_#2, sequence trait by miRNA subclass length distribution
		$this->AddLenDistPrinterSet (
				$this->AlterArrayByArray (
					$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
					array (
						 "AnaParaLenDistPrinter::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Xlimit"	=> "boost::mpl::int_<0>"// lendistribution with no sorting & limitation	
						,"AnaParaLenDistPrinter::Ylimit"	=> "boost::mpl::int_<50>"
						,"AnaParaLenDistPrinter::Zlimit"	=> "boost::mpl::int_<0>"
//						,"AnaParaLenDistPrinter::XSort"		=> "boost::mpl::bool_<true>"
						,"AnaParaLenDistPrinter::YSort"		=> "boost::mpl::bool_<true>"
						)));
		//FIG_SEQ_#5-1, z detail table - different z values, i.e. with different length, printed in different tables
		$this->AddLenDistPrinterSet (
				$this->AlterArrayByArray (
					$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
					array (
						 "AnaParaLenDistPrinter::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Xlimit"	=> "boost::mpl::int_<500>"
						,"AnaParaLenDistPrinter::Ylimit"	=> "boost::mpl::int_<50>"
						,"AnaParaLenDistPrinter::Zlimit"	=> "boost::mpl::int_<0>"
						,"AnaParaLenDistPrinter::XSort"		=> "boost::mpl::bool_<true>"
						,"AnaParaLenDistPrinter::YSort"		=> "boost::mpl::bool_<true>"
						)));
		//FIG_SEQ_#5-2, z merged table - value with different z values are merged into one table
		$this->AddLenDistPrinterSet (
				$this->AlterArrayByArray (
					$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
					array (
						 "AnaParaLenDistPrinter::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinter::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
						,"AnaParaLenDistPrinter::Xlimit"	=> "boost::mpl::int_<500>"
						,"AnaParaLenDistPrinter::Ylimit"	=> "boost::mpl::int_<50>"
						,"AnaParaLenDistPrinter::Zlimit"	=> "boost::mpl::int_<0>"
						,"AnaParaLenDistPrinter::XSort"		=> "boost::mpl::bool_<true>"
						,"AnaParaLenDistPrinter::YSort"		=> "boost::mpl::bool_<true>"
						)));
	}

	public function GetAllBiotypeSeqPrinter ()
	{
		//FIG_SEQ_#4
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<0>"// lendistribution with no sorting & limitation	
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<50>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
//				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
		//FIG_SEQ_#3
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<500>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
//				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
		//FIG_SEQ_#6-1 detail
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<500>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<50>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
		//FIG_SEQ_#6-2 merged
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<500>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<50>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
		//FIG_SEQ_#7-1 detail
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<500>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
		//FIG_SEQ_#7-2 merged
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<500>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
	}

	public function GetAllNormalPrinter ()
	{
		//FIG_NORMAL_#1
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::XSort"	=> "boost::mpl::bool_<true>"
				)));
		//FIG_NORMAL_#2
		$this->AddLenDistPrinterSet (
			$this->AlterArrayByArray (
				$this->GetDefaultSet(AnalyzerEnum::LenDistPrinter),
				array (
				 "AnaParaLenDistPrinter::Xaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Yaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
				,"AnaParaLenDistPrinter::Zaxis"	=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> >"
				,"AnaParaLenDistPrinter::Xlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Ylimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::Zlimit"=> "boost::mpl::int_<0>"
				,"AnaParaLenDistPrinter::YSort"	=> "boost::mpl::bool_<true>"
				)));
	}

	public function ListExhaustiveLendist ($get_read_length_setting, $get_read_count_setting, $input_setting)
	{
		$miRNA_indicator = false;
		if ($input_setting ["AnaParaLenDist::DbDepth2NameType"]=="boost::mpl::string<'-1'>")//miRNA situation
			$miRNA_indicator = true;

		foreach ($get_read_length_setting as $dim_1)
		{
			$annoidx0 = 0;
			$annoidx1 = 0;
			foreach ($get_read_count_setting as $dim_2)
			{
				$this->AddLenDistSet (
						$this->AlterArrayByArray (
							$input_setting,
							array (
								 "AnaParaLenDist::GetReadLengthClass" 	=> $dim_1
								,"AnaParaLenDist::CalReadCountClass" 	=> $dim_2)));
				$this->GetAllNormalPrinter ();

				if ($dim_2 == "CalReadCountGMPM")
					$annoidx0 = $this->len_dist_analyzer_count;
				else if ($dim_2 == "CalReadCountPMOnly")
					$annoidx1 = $this->len_dist_analyzer_count;
			}

//			if ($miRNA_indicator)
			{
				// print tables for tailing ratio, where annoidx0 & annoidx1 respectively correspond to lendistribution analyzers of PMGM and PMOnly
				$this->AddAnalyzerSet (
					$this->AlterArrayByArray (
						$this->GetDefaultSet(AnalyzerEnum::LenDistPrinterDualAnalyzer),
						array (
							 "AnaParaLenDistPrinterDualAnalyzer::AnnoIdx"	=> "boost::mpl::int_<".strval($annoidx0).">"
							,"AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2"	=> "boost::mpl::int_<".strval($annoidx1).">"
							,"AnaParaLenDistPrinterDualAnalyzer::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Xlimit"	=> "boost::mpl::int_<0>"
							,"AnaParaLenDistPrinterDualAnalyzer::Ylimit"	=> "boost::mpl::int_<0>"
							,"AnaParaLenDistPrinterDualAnalyzer::YSort"		=> "boost::mpl::bool_<true>"//sorted according to pm+gm value 
							)));

				$this->AddAnalyzerSet (
					$this->AlterArrayByArray (
						$this->GetDefaultSet(AnalyzerEnum::LenDistPrinterDualAnalyzer),
						array (
							 "AnaParaLenDistPrinterDualAnalyzer::AnnoIdx"	=> "boost::mpl::int_<".strval($annoidx0).">"
							,"AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2"	=> "boost::mpl::int_<".strval($annoidx1).">"
							,"AnaParaLenDistPrinterDualAnalyzer::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Xlimit"	=> "boost::mpl::int_<0>"
							,"AnaParaLenDistPrinterDualAnalyzer::Ylimit"	=> "boost::mpl::int_<50>"
							,"AnaParaLenDistPrinterDualAnalyzer::XSort"		=> "boost::mpl::bool_<true>"//sorted according to pm+gm value 
							)));

				$this->AddAnalyzerSet (
					$this->AlterArrayByArray (
						$this->GetDefaultSet(AnalyzerEnum::LenDistPrinterDualAnalyzer),
						array (
							 "AnaParaLenDistPrinterDualAnalyzer::AnnoIdx"	=> "boost::mpl::int_<".strval($annoidx0).">"
							,"AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2"	=> "boost::mpl::int_<".strval($annoidx1).">"
							,"AnaParaLenDistPrinterDualAnalyzer::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
							,"AnaParaLenDistPrinterDualAnalyzer::Xlimit"	=> "boost::mpl::int_<0>"
							,"AnaParaLenDistPrinterDualAnalyzer::Ylimit"	=> "boost::mpl::int_<50>"
							)));
			}

			$this->AddAnalyzerSet (
				$this->AlterArrayByArray (
					$this->GetDefaultSet(AnalyzerEnum::LenDistPrinterDualAnalyzer),
					array (
						 "AnaParaLenDistPrinterDualAnalyzer::AnnoIdx"	=> "boost::mpl::int_<".strval($annoidx0).">"
						,"AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2"	=> "boost::mpl::int_<".strval($annoidx1).">"
						,"AnaParaLenDistPrinterDualAnalyzer::Xaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> >"
						,"AnaParaLenDistPrinterDualAnalyzer::Yaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> >"
						,"AnaParaLenDistPrinterDualAnalyzer::Zaxis"		=> "boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> >"
						,"AnaParaLenDistPrinterDualAnalyzer::Xlimit"	=> "boost::mpl::int_<0>"//in order to over crowded x-axis 
						,"AnaParaLenDistPrinterDualAnalyzer::Ylimit"	=> "boost::mpl::int_<0>" 
						,"AnaParaLenDistPrinterDualAnalyzer::XSort"		=> "boost::mpl::bool_<true>"//sorted according to pm+gm value 
						)));
		}
	}

	public function ListExhaustiveBiotypeSeqdist ($get_read_length_setting, $get_read_seq_setting, $input_setting)
	{
		foreach ($get_read_seq_setting as $dim_2)
		{
			foreach ($get_read_length_setting as $dim_1)
			{
				$this->AddLenDistSet (
						$this->AlterArrayByArray (
							$input_setting,
							array (
								 "AnaParaLenDist::GetReadLengthClass" 	=> $dim_1
								,"AnaParaLenDist::GetReadSeqClass" 		=> $dim_2)));
				$this->GetAllBiotypeSeqPrinter();
			}
		}
	}

	public function ListExhaustiveMiRNASeqdist ($get_read_length_setting, $get_read_seq_setting, $input_setting)
	{
		foreach ($get_read_seq_setting as $dim_2)
		{
			foreach ($get_read_length_setting as $dim_1)
			{
				$this->AddLenDistSet (
						$this->AlterArrayByArray (
							$input_setting,
							array (
								 "AnaParaLenDist::GetReadLengthClass" 	=> $dim_1
								,"AnaParaLenDist::GetReadSeqClass" 		=> $dim_2)));
				$this->GetAllMiRNASeqPrinter();
			}
		}
	}

	public function PrintEndStr ($label="")
	{
		$end_str = $this->tail_str.$label.";\n\n";
		fwrite ($this->typelist_file, $end_str);
	}
	
	public function ResetLenDistAnalyzerCount ()
	{
		$this->len_dist_analyzer_count = -1;
	}

	public function __destruct()
	{
		//fwrite ($this->typelist_file, $end_str);
		//fwrite ($this->typelist_file, "\ntypedef ");
		//fclose ($this->typelist_file);
		fwrite ($this->typelist_file, "\ntypedef ");
		$this->AddAnalyzerSet($this->GetDefaultSet(AnalyzerEnum::ToBamUnsorted),"");
		fwrite ($this->typelist_file, "TypeListUnsortedBam;");
		fclose ($this->typelist_file);
	}
};

?>

