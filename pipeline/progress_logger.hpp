#ifndef PROGRESS_LOGGER_HPP_
#define PROGRESS_LOGGER_HPP_
#include <vector>
#include <cstdint>
#include <iostream>
#include <iomanip>
class ProgressLogger
{
	std::vector<double> stage_ratio_;
	int stage_indicator_;
	uint32_t total_read_count_;
	double current_read_count_;
	double progress_record_;
	double limit_;

	public:

	ProgressLogger (std::vector<double> ratio_input = {0.6, 0.35})
		: stage_ratio_ (ratio_input)
		, stage_indicator_ (0)
		, total_read_count_ (0)
		, current_read_count_ (0.0)
		, progress_record_ (0.0)
		, limit_ (stage_ratio_[stage_indicator_])
	{}

	void SetTotalCount (uint32_t total_read_count)
	{
		total_read_count_ = total_read_count;
	}

	void AdvanceStage (void)
	{
		std::cout<<std::endl;
		++stage_indicator_;
		if (stage_indicator_<stage_ratio_.size())
			limit_ += stage_ratio_[stage_indicator_];

		current_read_count_ = 0;
		progress_record_=0.0;
		for (auto ind=0; ind!=stage_indicator_; ++ind)
			progress_record_ += stage_ratio_[ind];
	}

	void GetProgress (uint32_t input_read_count)
	{
		if (stage_indicator_ >= stage_ratio_.size())
			std::cout << "Progress: 100%" << std::endl;
		else 
		{
			current_read_count_ += input_read_count;
			double diff = (current_read_count_ / total_read_count_)*stage_ratio_[stage_indicator_];
			if (progress_record_ > limit_)
				std::cout << "Progress: "<<std::setprecision(0)<< std::fixed << limit_*100 <<"%" << std::endl;
			else if (diff >= 0.02)
			{
				progress_record_ += diff;
				std::cout << "Progress: "<<std::setprecision(0)<< std::fixed << progress_record_*100 <<"%" << std::endl;
				current_read_count_ = 0;
			}
		}
	}
};

#endif
