#ifndef SRNA_PIPELINE_JSON_PARSER_HPP_
#define SRNA_PIPELINE_JSON_PARSER_HPP_
#include <sstream>
#include <iostream>
#include <fstream>
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

struct SRNAPipelineJsonParser
{
	std::stringstream ss_;
	std::string json_str_;
	boost::property_tree::ptree p_tree_;

	public:
	std::string adapter_sequence_;// = p_tree_.get<std::string>("adapter_sequence");
	int barcode_type_;// = p_tree_.get<std::string>("barcode_type");
	std::string reference_genome_;
	uint32_t total_read_count_;
	bool is_paired_input_;

	std::vector<std::string> barcode_sequence_vec_;

	std::string base_api_url_;// = p_tree_.get<std::string>("base_api_url");
	std::string project_href_;
	std::string sample_href_;
	std::string app_platform_;
	std::string appsession_href_;
	std::string appresult_href_;
	std::string access_token_;// = p_tree_.get<std::string>("access_token");
	std::string analysis_prefix_name_;

	std::string app_session_name_;

	std::string genome_path_, database_path1_, database_path2_, index_prefix_;  
	std::vector<std::string> name_vec_, url_vec_, upload_url_vec_;
	std::vector<uint64_t> size_vec_;

	SRNAPipelineJsonParser (const std::string& input_js_path = "config.js")
	{
		std::ifstream json_reader (input_js_path);
		ss_ << json_reader.rdbuf();
		json_reader.close();
		json_str_ = ss_.str();
		boost::property_tree::json_parser::read_json ( ss_, p_tree_ );
		ParseImpl ();
		ParseBarcodeSeqVec ();
		ParseReferencePath ();
		ParseFileList ();
	}

	private:
	void ParseImpl ()
	{
		analysis_prefix_name_ = p_tree_.get<std::string>("analysis_prefix_name");//("app_session_name");
		std::replace (analysis_prefix_name_.begin(), analysis_prefix_name_.end(), ' ', '_');
		std::replace (analysis_prefix_name_.begin(), analysis_prefix_name_.end(), '/', '_');
		adapter_sequence_ = p_tree_.get<std::string>("adapter_sequence");
		barcode_type_ = p_tree_.get<int>("barcode_type");
		reference_genome_ = p_tree_.get<std::string>("reference_genome") ;
		base_api_url_ = p_tree_.get<std::string>("base_api_url");
		project_href_ = p_tree_.get<std::string>("project_href");
		sample_href_ = p_tree_.get<std::string>("sample_href");
		app_platform_ = p_tree_.get<std::string>("app_platform");
		appsession_href_ = p_tree_.get<std::string>("appsessionuri");
		appresult_href_ = p_tree_.get<std::string>("app_results");
		access_token_ = p_tree_.get<std::string>("access_token");
		is_paired_input_ = p_tree_.get<bool>("paired_end_input");
		if (is_paired_input_)
			total_read_count_ =  p_tree_.get<uint32_t>("input_read_count") * 2;
		else
			total_read_count_ =  p_tree_.get<uint32_t>("input_read_count");

		app_session_name_ = p_tree_.get<std::string> ("app-session-name");
	}

	void ParseBarcodeSeqVec ()
	{
		if (json_str_.find ("barcode_sequence_vec") != std::string::npos)
			for (boost::property_tree::ptree::value_type &v: p_tree_.get_child ("barcode_sequence_vec"))
				barcode_sequence_vec_.push_back (v.second.data());

		for (auto&q: barcode_sequence_vec_)
			std::cerr<<q<<'\n';
	}

	void ParseReferencePath ()
	{
		boost::property_tree::ptree pptree = p_tree_.get_child ("download_file");
		genome_path_ = pptree.get <std::string> ("genome_path");
		database_path1_ = pptree.get <std::string> ("database_path1");
		database_path2_ = pptree.get <std::string> ("database_path2");
		index_prefix_ = pptree.get <std::string> ("index_prefix");
		std::cerr<<"download path: "<<genome_path_<<'\t'<<database_path1_<<'\t'<<database_path2_<<'\t'<<index_prefix_<<'\n';
	}
	
	void ParseFileList ()
	{
		for (boost::property_tree::ptree::value_type &v: p_tree_.get_child ("file_list"))
		{
			name_vec_.emplace_back (v.second.get <std::string> ("Name"));
			url_vec_.emplace_back (base_api_url_ + v.second.get <std::string> ("Content") + "content?access_token=" + access_token_ );
			size_vec_.emplace_back (v.second.get<uint64_t> ("Size"));
			upload_url_vec_.emplace_back ( 
					base_api_url_ + p_tree_.get<std::string> ("app_results") + "files?name=" + v.second.get <std::string> ("Name") + "&multipart=true");
		}
		for (auto& q : name_vec_)
			std::cerr<<q<<'\n';
		for (auto& q : url_vec_)
			std::cerr<<q<<'\n';
		for (auto& q : size_vec_)
			std::cerr<<q<<'\n';
		for (auto& q : upload_url_vec_)
			std::cerr<<q<<'\n';
	}
};
#endif
