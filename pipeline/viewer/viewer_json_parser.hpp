#ifndef VIEWER_JSON_PARSER_HPP_
#define VIEWER_JSON_PARSER_HPP_
#include <sstream>
#include <iostream>
#include <fstream>
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

struct ViewerJsonParser
{
	std::stringstream ss_;
	std::string json_str_;
	boost::property_tree::ptree p_tree_;

	public:
	std::string base_api_url_;// = p_tree_.get<std::string>("base_api_url");
	std::string app_platform_;
	std::string appsession_href_;
	std::string appresult_href_;
	std::string access_token_;// = p_tree_.get<std::string>("access_token");
	std::vector<std::string> appresult_vec_;

//	std::vector<std::string> name_vec_, url_vec_, dir_vec_;//upload_url_vec_, 
//	std::vector<uint64_t> size_vec_;
	std::vector < std::vector<std::string> > name_vec_, url_vec_, dir_vec_;//upload_url_vec_, 
	std::vector < std::vector<uint64_t> > size_vec_;

	ViewerJsonParser (const std::string& input_js_path = "config.js")
	{
		std::ifstream json_reader (input_js_path);
		ss_ << json_reader.rdbuf();
		json_reader.close();
		json_str_ = ss_.str();
		boost::property_tree::json_parser::read_json ( ss_, p_tree_ );
		ParseImpl ();
		ParseFileList ();
	}

	void PrintContent ()
	{
std::cerr<<"size of name : url : dir : size "<<name_vec_.size()<<'\t'<<url_vec_.size()<<'\t'<<dir_vec_.size()<<'\t'<<size_vec_.size()<<'\n';
		for (auto idx=0; idx!=name_vec_.size(); ++idx)
		{
			std::cerr<<"group "<<idx<<"--------------\n";
			for (auto idx2=0; idx2!=name_vec_[idx].size(); ++idx2)
				std::cerr<<dir_vec_[idx][idx2]+name_vec_[idx][idx2]<<'\n';
		}
		for (auto idx=0; idx!=url_vec_.size(); ++idx)//(auto& q : url_vec_)
		{
			std::cerr<<"group "<<idx<<"--------------\n";
			for (auto idx2=0; idx2!=url_vec_[idx].size(); ++idx2)
				std::cerr<<url_vec_[idx][idx2]<<'\n';
		}
		for (auto idx=0; idx!=size_vec_.size(); ++idx)//(auto& q : size_vec_)
		{
			std::cerr<<"group "<<idx<<"--------------\n";
			for (auto idx2=0; idx2!=size_vec_[idx].size(); ++idx2)
				std::cerr<<size_vec_[idx][idx2]<<'\n';
		}
	}

	private:
	void ParseImpl ()
	{
		base_api_url_ = p_tree_.get<std::string>("base_api_url");
		app_platform_ = p_tree_.get<std::string>("app_platform");
		appsession_href_ = p_tree_.get<std::string>("appsessionuri");
		appresult_href_ = p_tree_.get<std::string>("app_results");
		access_token_ = p_tree_.get<std::string>("access_token");

		for (boost::property_tree::ptree::value_type& value: p_tree_.get_child ("input_appresult_href"))
        {
            auto tmp = value.second.data();
            std::replace (tmp.begin(), tmp.end(), '/', '_');
            appresult_vec_.push_back (tmp);
        }
    }

	void ParseFileList ()
	{
		boost::property_tree::ptree& input_appresult_map = p_tree_.get_child ("file_list");
		int appresult_set_num = input_appresult_map.size();
		int ind=0;	
		name_vec_.resize (appresult_set_num);
		dir_vec_.resize (appresult_set_num);
		url_vec_.resize (appresult_set_num);
		size_vec_.resize (appresult_set_num);
		for (boost::property_tree::ptree::iterator it = input_appresult_map.begin(); it != input_appresult_map.end(); ++it, ++ind)   
		{
			for (boost::property_tree::ptree::value_type &v: it->second)//input_appresult_map.second[ind])
			{
				name_vec_[ind].emplace_back (v.second.get <std::string> ("Name"));
				dir_vec_[ind].emplace_back (v.second.get <std::string> ("Dir"));
				url_vec_[ind].emplace_back (base_api_url_ + v.second.get <std::string> ("Content") + "content?access_token=" + access_token_ );
				size_vec_[ind].emplace_back (v.second.get<uint64_t> ("Size"));
			}
		}
	}
};
#endif
