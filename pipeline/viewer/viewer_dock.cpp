#include <sstream>
#include <iostream>
#include <fstream>
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "viewer_json_parser.hpp"
#include "../../src/iohandler/basespace_def.hpp"
#include <set>
#include "boost/filesystem.hpp"
#include <stdlib.h>

typedef boost::mpl::map
<
	boost::mpl::pair< IoHandlerGlobalParameter::FilteringStreamType, boost::iostreams::filtering_streambuf<boost::iostreams::input> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DeviceParameter, DeviceParameter >
	, boost::mpl::pair< IoHandlerGlobalParameter::MutipleNumber, boost::mpl::int_<2> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DeviceBufferSize, boost::mpl::int_<8> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DevicePushbackSize, boost::mpl::int_<16> >
	, boost::mpl::pair< IoHandlerGlobalParameter::BaseStreamType, std::istream >
> IO_HANDLER_GLOBAL_SETTING;

typedef boost::mpl::vector
<
	boost::mpl::map
	<  
		boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::BasespaceDevice_download> >
	>
> IO_HANDLER_LIST;



ThreadPool CurlPool ( 20 );

int main (int argc, char** argv)
{
	ViewerJsonParser Json_content (argv[1]);//("Viewerconfig.json");	
	Json_content.PrintContent();
/*
	std::set <std::string> uniq_dir;
	for (auto& item: Json_content.dir_vec_)
		for (auto& item_dir: item)
			uniq_dir.insert (item_dir); 

	boost::filesystem::path dir("output");
	boost::filesystem::create_directory(dir);

	for (auto& path: uniq_dir)
	{
		std::cerr<<"current path "<<path<<'\n';
		boost::filesystem::path dir(path);
		boost::filesystem::create_directory(dir);
	}
*/
	int path_index=0;
	for (auto& default_path: Json_content.appresult_vec_)
	{
		boost::filesystem::path dir(default_path);
		boost::filesystem::create_directory(dir);
		std::string path_prefix = default_path + "/output/";
		boost::filesystem::path dir2(path_prefix);
		boost::filesystem::create_directory(dir2);	

		std::set <std::string> uniq_dir;
		for (auto& dir_path: Json_content.dir_vec_[path_index])
			uniq_dir.insert (default_path+'/'+dir_path); 

		for (auto& path: uniq_dir)
		{
			std::cerr<<"current path "<<path<<'\n';
			boost::filesystem::path dir(path);
			boost::filesystem::create_directory(dir);
		}
		++path_index;
	}

	DeviceParameter dp;
std::cerr<<"a"<<std::endl;
	dp.bs_basic_url_ = Json_content.base_api_url_;//"https://api.cloud-hoth.illumina.com/";//"https://api.basespace.illumina.com/";
std::cerr<<"b"<<std::endl;
	dp.bs_app_result_ = Json_content.appresult_href_.substr (7);//"appresults/2538015/";//"appresults/7525518/";
std::cerr<<"c"<<std::endl;
	dp.bs_access_token_ = ("x-access-token: "+Json_content.access_token_);//9ca77f5225404c448fbe57096640ec19";//"x-access-token: 7b88f5bc19c342f5937259692b0c3eed";
std::cerr<<"d"<<std::endl;
	dp.bs_version_ = "v1pre3/";
std::cerr<<"e"<<std::endl;
	dp.bs_tailing_str_ = "multipart=true";
std::cerr<<"f"<<std::endl;
	dp.bs_content_type_ = "Content-Type: application/txt";
std::cerr<<"g"<<std::endl;
	dp.bs_upload_content_type_ = "Content-Type: multipart/form-data";
std::cerr<<"dp.bs_basic_url_: "<<dp.bs_basic_url_<<std::endl;
std::cerr<<"dp.bs_app_result_ "<<dp.bs_app_result_<<" "<<dp.bs_app_result_.substr(7)<<std::endl;

	for (auto idx=0; idx!=Json_content.name_vec_.size(); ++idx)
	{
		for (auto idx2=0; idx2!=Json_content.name_vec_[idx].size(); ++idx2)
		{
			auto url = Json_content.url_vec_[idx][idx2];
			auto size = Json_content.size_vec_[idx][idx2];
			auto dir = Json_content.dir_vec_[idx][idx2];
			auto name = Json_content.name_vec_[idx][idx2];
			auto dir_prefix = Json_content.appresult_vec_[idx];
std::cerr<<"downloading : "<<url<<'\t'<<name<<'\t'<<dir_prefix<<std::endl;	
			CurlPool.JobPost(
			[url, size, dir, name, dp, dir_prefix]()
			{
				DeviceParameter dp2 (dp);
				dp2.bs_download_url_ = url;
				dp2.bs_download_size_ = size;
				iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp2);
	
				std::ofstream ffy (dir_prefix+'/'+dir+name);
				int ii = 1024*1024;
				char* ff = new char[ii+1];
				while (!bb.eof())
				{   
					bb.read (ff, ii);
					ffy.write (ff, bb.gcount());
				}
				delete ff;
				ffy.close();

				if (name.find (".des3")!=std::string::npos)
				{
					std::string folder_name (name);
std::cerr<<"obtained folder_name: "<<folder_name<<std::endl;
folder_name.resize (folder_name.size()-20);
					std::string tar_name (folder_name+".tar.gz");
std::cerr<<"updated tar & folder_name: "<<tar_name<<'\t'<<folder_name<<std::endl;

					std::string untar_cmd ("cd "+ dir_prefix + "&& dd if="+dir+name+"|openssl des3 -d -k 5566 | tar zxf -");
std::cout<<"untar_cmd "<<untar_cmd<<'\n';
					system ( untar_cmd.c_str() );
std::cerr<<"done system #1 "<<std::endl;
					std::string target_folder ("output/"+ folder_name);
					std::string cmd ("cd "+dir_prefix +" && tar -zcvf "+tar_name +" "+target_folder+" --exclude '*.bam' --exclude '*.bwg' --exclude '*.bai' --exclude '*.des3'");
std::cout<<"tar_cmd "<<cmd<<'\n';					
					system ( cmd.c_str() );
//std::cerr<<"done system #2 "<<std::endl;
		   			std::ifstream fin (dir_prefix+"/"+tar_name);
//std::cerr<<"ifstream "<<std::string (dir_prefix+"/"+tar_name)<<std::endl;
					DeviceParameter dp3 (dp);
		   			dp3.set_filename (tar_name, folder_name);

		   			IoHandlerBaseSpace QQ (dp3);
		   			char* f2 = new char [4096+1];
		   			while (true)
		   			{
		   				fin.read (f2, 4096);
		   				QQ.write(f2, 4096);
		   				if (fin.eof())
		   				break;
		   			}
		   			delete f2;
		   			QQ.close();
				}
//std::cerr<<"done download : "<<url<<'\t'<<name<<'\t'<<dir_prefix<<std::endl;	
			});
		}
	}
		CurlPool.FlushPool();
	return 0;
}
