typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinter> AnaParaLenDistPrinter;
typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;
typedef AnalyzerParameter <AnalyzerTypes::HeterogeneityPrinter> AnaParaHeterogeneityPrinter;
typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaParaToBam;
typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaParaToBwg;
typedef AnalyzerParameter <AnalyzerTypes::MappabilityPrinter> AnaParaMapPrinter;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinterDualAnalyzer> AnaParaLenDistPrinterDualAnalyzer;
typedef AnalyzerParameter <AnalyzerTypes::SequenceLogo> AnaParaSeqLogo;
typedef AnalyzerParameter <AnalyzerTypes::SequenceLogoPrinter> AnaParaSeqLogoPrinter;
typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaMapPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::MappabilityPrinter > >,
		boost::mpl::pair< AnaParaMapPrinter::IoHandlerType, IoHandlerOfstream >
	>
> AnalyzerTypeList_A;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistH::AnalyzerType, boost::mpl::int_< AnalyzerTypes::Heterogeneity > >,
		boost::mpl::pair< AnaParaLenDistH::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDistH::DbIndexType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDistH::DbDepthTypeFor3Or5Prime, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistH::DbDepthTypeForName, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistH::Db5PrimeName, boost::mpl::string<'miRN', 'A-5p'> >,
		boost::mpl::pair< AnaParaLenDistH::Db3PrimeName, boost::mpl::string<'miRN', 'A-3p'> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaHeterogeneityPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::HeterogeneityPrinter > >,
		boost::mpl::pair< AnaParaHeterogeneityPrinter::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaHeterogeneityPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaHeterogeneityPrinter::XSort, boost::mpl::bool_<true> >
	>
> AnalyzerTypeList_B;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaSeqLogo::AnalyzerType, boost::mpl::int_< AnalyzerTypes::SequenceLogo > >,
		boost::mpl::pair< AnaParaSeqLogo::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaSeqLogo::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaSeqLogo::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaSeqLogo::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaSeqLogo::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaSeqLogo::GetReadSeqClass, GetReadFirstNLastComposition<0, 0, 15> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaSeqLogoPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::SequenceLogoPrinter > >,
		boost::mpl::pair< AnaParaSeqLogoPrinter::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaSeqLogoPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaSeqLogoPrinter::PrefixName, boost::mpl::string<'Tab', 'le'> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > >,
		boost::mpl::pair< AnaParaToBam::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaToBam::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBam::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBam::IoHandlerType, IoHandlerOfstream >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaToBwg::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBwg > >,
		boost::mpl::pair< AnaParaToBwg::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaToBwg::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBwg::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBwg::IoHandlerType, IoHandlerOfstream >
	>
> AnalyzerTypeList_C;


typedef 	boost::mpl::map
	<
		boost::mpl::pair< AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > >,
		boost::mpl::pair< AnaParaToBam::IoHandlerType, IoHandlerOfstream >
	>
TypeListUnsortedBam;