typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinter> AnaParaLenDistPrinter;
typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;
typedef AnalyzerParameter <AnalyzerTypes::HeterogeneityPrinter> AnaParaHeterogeneityPrinter;
typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaParaToBam;
typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaParaToBwg;
typedef AnalyzerParameter <AnalyzerTypes::MappabilityPrinter> AnaParaMapPrinter;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinterDualAnalyzer> AnaParaLenDistPrinterDualAnalyzer;
typedef AnalyzerParameter <AnalyzerTypes::SequenceLogo> AnaParaSeqLogo;
typedef AnalyzerParameter <AnalyzerTypes::SequenceLogoPrinter> AnaParaSeqLogoPrinter;
typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<2> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<2> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<2> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<3> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<3> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<4> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<4> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<5> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<5> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<3> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<5> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<6> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<6> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<7> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<7> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<8> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<8> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<6> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<8> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<6> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<8> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<6> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<8> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<50> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<6> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<8> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<9> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<9> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<10> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<10> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<11> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<11> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<9> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<11> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<9> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<11> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<9> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<11> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<50> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<9> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<11> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<12> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<12> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<12> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<12> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<12> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<12> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<13> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<13> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<13> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<13> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<13> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<13> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<14> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<14> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<14> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<14> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<14> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<14> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<15> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<15> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<15> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<15> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<15> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<15> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<16> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<16> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<16> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<16> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<16> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<16> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<17> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<17> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<17> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<17> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<17> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<17> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<18> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<18> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<18> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<18> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<18> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<18> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<19> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<19> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<19> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<19> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<19> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<19> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<20> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<20> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<20> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<20> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<20> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<20> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<21> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<21> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<21> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<21> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<21> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<21> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<22> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<22> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<22> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<22> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<22> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<22> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<23> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<23> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<23> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<23> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<23> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<23> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<24> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<24> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<24> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<24> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<25> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<25> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<25> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<25> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<26> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<26> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<26> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<26> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<27> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<27> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<27> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<27> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<28> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<28> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<28> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<28> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<29> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<29> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<29> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<29> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<30> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<30> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<30> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<30> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<31> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<31> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<31> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<31> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<32> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<32> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<32> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<32> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<33> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<33> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<33> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<33> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<34> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<34> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<34> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<34> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<35> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<35> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<35> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<35> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaMapPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::MappabilityPrinter > >,
		boost::mpl::pair< AnaParaMapPrinter::IoHandlerType, IoHandlerOfstream >
	>
> AnalyzerTypeList_A;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<36> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<36> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<36> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<36> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<36> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<36> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<37> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<37> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<37> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<37> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<37> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<37> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<38> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<38> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<38> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<38> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<38> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<38> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<39> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<39> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<39> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<39> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<39> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<39> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<40> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<40> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<40> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<40> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<40> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<40> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<41> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<41> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<41> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<41> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<41> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<41> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<42> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<42> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<42> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<42> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<42> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<42> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<43> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<43> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<43> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<43> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<43> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<43> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<44> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<44> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<44> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<44> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<44> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<44> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<45> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<45> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<45> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<45> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<45> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<45> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<46> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<46> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<46> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<46> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<46> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<46> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<47> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<47> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<47> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<47> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<47> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<47> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<48> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<48> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<48> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<48> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<49> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<49> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<49> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<49> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<51> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<51> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<51> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<51> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<52> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<52> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<52> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<52> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<53> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<53> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<53> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<53> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<54> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<54> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<54> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<54> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<55> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<55> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<55> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<55> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<56> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<56> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<56> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<56> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<57> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<57> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<57> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<57> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<58> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<58> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<58> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<58> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountGMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<59> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<59> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<59> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<59> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistH::AnalyzerType, boost::mpl::int_< AnalyzerTypes::Heterogeneity > >,
		boost::mpl::pair< AnaParaLenDistH::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDistH::DbIndexType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDistH::DbDepthTypeFor3Or5Prime, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistH::DbDepthTypeForName, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistH::Db5PrimeName, boost::mpl::string<'miRN', 'A-5p'> >,
		boost::mpl::pair< AnaParaLenDistH::Db3PrimeName, boost::mpl::string<'miRN', 'A-3p'> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaHeterogeneityPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::HeterogeneityPrinter > >,
		boost::mpl::pair< AnaParaHeterogeneityPrinter::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaHeterogeneityPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaHeterogeneityPrinter::XSort, boost::mpl::bool_<true> >
	>
> AnalyzerTypeList_B;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<60> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<60> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<60> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<60> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<60> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<60> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<61> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<61> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<61> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<61> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<61> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<61> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<62> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<62> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<62> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<62> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<62> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<62> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<63> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<63> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<63> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<63> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<63> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<63> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<64> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<64> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<64> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<64> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<64> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<64> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<65> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<65> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<65> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<65> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<65> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<65> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<66> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<66> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<66> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<66> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<66> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<66> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<67> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<67> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<67> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<67> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<67> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<67> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<68> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<68> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<68> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<68> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<68> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<68> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<69> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<69> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<69> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<69> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<69> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<69> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<70> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<70> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<70> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<70> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<70> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<70> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-3'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<71> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<71> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<71> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<71> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<71> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<71> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<72> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<72> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<72> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<72> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadTailing >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<73> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<73> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<73> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<73> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<74> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<74> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<74> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<74> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<75> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<75> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<75> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<75> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<76> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<76> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<76> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<76> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<77> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<77> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<77> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<77> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<78> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<78> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<78> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<78> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<79> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<79> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<79> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<79> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<80> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<80> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<80> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<80> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<81> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<81> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<81> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<81> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadFullLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<82> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<82> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<82> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<82> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >,
		boost::mpl::pair< AnaParaLenDist::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaLenDist::GetReadLengthClass, GetReadPrefixLength >,
		boost::mpl::pair< AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >,
		boost::mpl::pair< AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<83> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<83> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<83> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
		boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<83> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-2'> > >,
		boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<50> >,
		boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaLenDistPrinter::XSort, boost::mpl::bool_<true> >,
		boost::mpl::pair< AnaParaLenDistPrinter::YSort, boost::mpl::bool_<true> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaSeqLogo::AnalyzerType, boost::mpl::int_< AnalyzerTypes::SequenceLogo > >,
		boost::mpl::pair< AnaParaSeqLogo::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaSeqLogo::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaSeqLogo::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaSeqLogo::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >,
		boost::mpl::pair< AnaParaSeqLogo::DbDepth2NameType, boost::mpl::string<'-1'> >,
		boost::mpl::pair< AnaParaSeqLogo::GetReadSeqClass, GetReadFirstNLastComposition<0, 0, 15> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaSeqLogoPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::SequenceLogoPrinter > >,
		boost::mpl::pair< AnaParaSeqLogoPrinter::AnnoIdx, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaSeqLogoPrinter::IoHandlerType, IoHandlerOfstream >,
		boost::mpl::pair< AnaParaSeqLogoPrinter::PrefixName, boost::mpl::string<'Tab', 'le'> >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > >,
		boost::mpl::pair< AnaParaToBam::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaToBam::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBam::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBam::IoHandlerType, IoHandlerBaseSpace >,
		boost::mpl::pair< AnaParaToBam::IoHandlerTypeForBAI, IoHandlerBaseSpace >
	>,
	boost::mpl::map
	<
		boost::mpl::pair< AnaParaToBwg::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBwg > >,
		boost::mpl::pair< AnaParaToBwg::FilterType, boost::mpl::int_<1> >,
		boost::mpl::pair< AnaParaToBwg::DbIndexType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBwg::DbDepthType, boost::mpl::int_<0> >,
		boost::mpl::pair< AnaParaToBwg::IoHandlerType, IoHandlerBaseSpace >
	>
> AnalyzerTypeList_C;


typedef 	boost::mpl::map
	<
		boost::mpl::pair< AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > >,
		boost::mpl::pair< AnaParaToBam::IoHandlerType, IoHandlerBaseSpace >
	>
TypeListUnsortedBam;