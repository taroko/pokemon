#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "tobigwig.hpp"
//extern "C"
//{
//	#include "kent/src/inc/common.h"
//	#include "kent/src/inc/bwgInternal.h" 
//}

struct aa
{
	float kk;
};

int main(int argc, char** argv)
{
	std::vector<aa> pp(10);
	
	pp[0].kk = 1.1;
	pp[1].kk = 2.2;
	pp[2].kk = 3.3;
	pp[3].kk = 4.4;
	
	aa *ff = &pp[0]; 
	
	std::cout << ff->kk << " : " << (ff+1)->kk << ":" << (ff+2)->kk << std::endl;


//	std::vector < std::vector<aa> > ppp (3, std::vector<aa>(3));//({ {1.1, 1.2, 1.3}, {2.1,2.2,2.3}, {3.1,3.2,3.3} });
	std::map < int, std::vector<aa> > ppp;// (3, std::vector<aa>(3));//({ {1.1, 1.2, 1.3}, {2.1,2.2,2.3}, {3.1,3.2,3.3} });
	ppp[0].resize(3);
	ppp[0][0].kk = 0.1;
	ppp[0][1].kk = 0.2;
	ppp[0][2].kk = 0.3;

	ppp[1].resize(3);
	ppp[1][0].kk = 1.1;
	ppp[1][1].kk = 1.2;
	ppp[1][2].kk = 1.3;

	ppp[2].resize(3);
	ppp[2][0].kk = 2.1;
	ppp[2][1].kk = 2.2;
	ppp[2][2].kk = 2.3;

	aa *f1= &(ppp[0][0]);
	aa* f2= &(ppp[1][0]);
	aa* f3= &(ppp[2][0]);

	std::cerr << f1->kk << " : " << (f1+1)->kk << " : " << (f1+2)->kk <<'\n';
	std::cerr << f2->kk << " : " << (f2+1)->kk << " : " << (f2+2)->kk <<'\n';
	std::cerr << f3->kk << " : " << (f3+1)->kk << " : " << (f3+2)->kk <<'\n';
	
	//struct bwgSection *sectionList;
	//bits64 sectionCount = (bits64)slCount((const void*)sectionList);
	AnalyzerToBigWig bw;
	bw.wigToBigWig(argv[1], argv[2], argv[3]);
	
	return 0;
}
