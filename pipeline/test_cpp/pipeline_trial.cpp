#include <boost/mpl/vector.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/string.hpp>
#include <boost/type_traits/add_pointer.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/map.hpp>
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>
#include "../src/file_reader.hpp"
#include "../src/barcode_handler/barcode_handler_impl.hpp"
#include "../src/peat/single_end_adapter_trimmer.hpp"
#include "../src/aligner/aligner.hpp"
#include "sam2rawbed.hpp"
#include "sam2bam.hpp"
#include "htslib/htslib/sam.h"
#include "pipeline_terminator.hpp"
#include "annotation_set.hpp"
#include "annotation.hpp"
#include "filter.hpp"
#include "analyzer.hpp"
#include "tobam.hpp"
#include "tobigwig.hpp"

typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;
typedef FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE > Reader;
typedef BarcodeHandlerImpl < BarcodeHandleScheme::Five_Prime, Fastq, TUPLETYPE > Barcode_Handler;
typedef SingleEndAdapterTrimmer < ParallelTypes::M_T, Fastq, TUPLETYPE, QualityScoreType::SANGER > Adapter_Trimmer;
typedef Aligner< Aligner_trait<> > Tailer_;
typedef Sam2RawBed < std::vector< Sam<> >* > Sam_to_RawBed;
typedef Sam2Bam< std::tuple<std::string, std::vector< Sam<> > >* > Sam_to_Bam;


//

/// @brief 定義Analyzer

typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;
typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;

typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaParaToBam;

typedef boost::mpl::vector
<
/*	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > > //LengthDistribution > >
		,boost::mpl::pair<AnaParaToBam::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaToBam::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaToBam::DbDepthType, boost::mpl::int_<0> >
	>

	,*/boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
	//	,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
	//	,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadTailing>
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
	//	,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeed<> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
	//	,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
	//	,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadSpecies>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 1, 2> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistH::AnalyzerType, boost::mpl::int_< AnalyzerTypes::Heterogeneity > >
		,boost::mpl::pair<AnaParaLenDistH::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbDepthTypeFor3Or5Prime, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbDepthTypeForName, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDistH::Db5PrimeName, boost::mpl::string<'5P'> >
		,boost::mpl::pair<AnaParaLenDistH::Db3PrimeName, boost::mpl::string<'3P'> >
	>
> AnalyzerTypeList;

typedef Analyzer <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> Analyzers;
typedef AnalyzerToBam <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> AnalyzersToBam;


	/// @brief vector 順序越前面權重越低，越後面越高
	/// @brief tuple 0=>字串, 1=>is_filter;
	/// @brief '^', '$', '%', 'R'
typedef boost::mpl::vector < 	
	//boost::mpl::vector< boost::mpl::string<'nc', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	//boost::mpl::vector< boost::mpl::string<'([pl', '].*)'>, boost::mpl::int_<1>, boost::mpl::char_<'R'> >
	//boost::mpl::vector< boost::mpl::string<'#([a', '-f0-', '9]{2', '})'>, boost::mpl::int_<1>, boost::mpl::char_<'R'> >
	boost::mpl::vector< boost::mpl::string<'pro', 'tein', ' cod', 'ing ', 'gene'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
> Filter_Type_List;


typedef FilterWorker <std::vector< AnnotationRawBed<> >*, Filter_Type_List> Filters;


/// @brief 定義 Annotations
class AnnoTrait_MGI
{
public:
	const char* file_path = "/home/andy/db/MGI_mm9_no_cluster.bed";
};

class AnnoTrait_QQQ
{
public:
	const char* file_path = "/home/andy/db/MGI_mm9_no_cluster.bed";
};

typedef AnnotationSet < 
		//std::function < void ( AnnotationRawBed<>& ) >,
		std::vector< AnnotationRawBed<> >,
		1,
		Annotation<	FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
			,AnnoTrait_MGI
			,AnnoIgnoreStrand::IGNORE
			,AnnoType::INTERSET
		>
> Annotations;



std::mutex m1;
template<class InputType, class OutputType, class ObjectType>
struct run 
{
	OutputType operator()(InputType i,bool eof_)
	{
		std::cerr << "pipeline error " << std::endl;
		return 0;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Reader>
{
	OutputType operator()(InputType i,bool eof_)
	{
		std::cerr << "pipeline Reader " << std::endl;
		auto ptr_ = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
		std::cerr<<"ptr_"<<ptr_<<std::endl;
		std::vector<std::string> read_vec ({"/mnt/godzilla/johnny/PEAT/million_0/origin0_1.fq"});
		Reader o (read_vec, ptr_);
		auto yy = o.Read (10000);
		std::cerr<<"yy"<<yy<<std::endl;
		return yy;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Barcode_Handler>
{
	OutputType operator()(InputType i, bool eof_)
	{
		std::cerr << "pipeline Barcode_Handler " << std::endl;
		std::vector<std::string> barcode_vec ({"acga", "acgc", "acgg", "acgt"});
		Barcode_Handler o (barcode_vec);
		return o (i);
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Adapter_Trimmer>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::cerr << "pipeline Adapter_Trimmer " << std::endl;
		ParameterTrait <QualityScoreType::SANGER> Qoo ("AGATCGGAAGAGCGG");
		std::vector<int> trim_pos;
//		int nthread = 1;	//18.84s	88%	
		int nthread = 2;	//10.69s	193%
//		int nthread = 4;	//10.91s	193%
//		int nthread = 8;	//10.59s	183%
		Adapter_Trimmer o (Qoo);
		o.Trim (ccc, nthread, trim_pos);
		return ccc;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Tailer_>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		//
		
		std::cerr << "pipeline Tailer " << std::endl;
		Tailer_ o;
		o.load_table("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");
		//o.load_table("/mnt/mammoth/jones/bwt_table/hg19/hg19");
		std::cout << "load index finish ccc " << ccc << std::endl;
		auto Q = o.search(ccc, 3, 18, 100, 200);
		delete ccc;
		std::ofstream of ("out.sam");
		
		of << std::get<0>(*Q) ;
		for (auto& aa : std::get<1>(*Q))
		{
			of << aa;
		}
		of.close();
		return Q;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Sam_to_RawBed>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::cerr << "pipeline Sam_to_RawBed " << std::endl;
		Sam2RawBed < std::vector< Sam<> >* > o;
		auto Q = o.Convert (ccc);
//		for (auto& i : *ccc)
//			std::cerr<<i;
		std::ofstream of ("out.bed");
		for (auto& i : *Q)
			of<<i.first;
//			std::cerr<<i.first;
		of.close();
		return Q;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Sam_to_Bam>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::cerr << "pipeline Sam_to_Bam " << std::endl;
		Sam2Bam< std::tuple<std::string, std::vector< Sam<> > >* > s2b;
		
		std::stringstream* ll;
		s2b.run(ccc);
		s2b.final_Sam2Bam();
		 
		std::cerr << "bam_length " << s2b.bam_length << std::endl;

		return ll;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::cerr << "pipeline Pipeline_terminator " << std::endl;
		
		std::cerr << "\tpipeline Sam_to_RawBed " << std::endl;
		
		Sam2RawBed < std::vector< Sam<> >* > o;
		o.Convert ( &(std::get<1>(*ccc)) );
		 
		{
			//std::lock_guard<std::mutex> lock(m1);	 
		
			std::cerr << "\tpipeline Sam_to_Bam " << std::endl;
			
			//建構時，傳入上傳 functor
			Sam2Bam< std::tuple<std::string, std::vector< Sam<> > >* > s2b;
			
			//reader 要給eof訊息
std::cerr<<"run with file name "<<std::get<0>(*ccc)<<'\n';
			s2b.run(ccc);
			if( eof_)//Reader::is_eof)
			{
	std::cerr<<"eof terminator"<<std::endl;
				s2b.final_Sam2Bam();
			}
		
		
		}
		
		OutputType ll;
		return ll;
	}
};



/// @brief Pipeline II

template<class InputType, class OutputType>
struct run < InputType, OutputType, Annotations>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::vector<std::string> AnnoDBPath
		{
			 "/home/andy/db/MGI_mm9_no_cluster.bed"
			,"/home/andy/db/MGI_mm9_no_cluster.bed"
		};
			
		
		std::cout << "vector size of raw bed : " << ccc->size() << std::endl;
		
		
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		
		for(auto &annorawbed : *ccc)
		{
			std::cout << annorawbed << std::endl;
			if(annorawbed.annotation_info_.size() == 0)
				continue;
				
			std::cout << "========================" << std::endl;
			
			for(auto &kk : annorawbed.annotation_info_)
			{
				for(auto &kkk : kk)
				{
					std::cout << kkk << std::endl;
				}
				
			}
			std::cout << "========================" << std::endl;

		}
		
		return ccc;		
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Filters>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::cerr << "pipeline Filter " << std::endl;
		Filters o;
		return o.Filter (ccc);
	}
};

struct pipeline
{
	void* transfer_ptr;
	bool eof_;

	pipeline(void* inptr, bool eof)
		: transfer_ptr (inptr), eof_ (eof)
	{
		std::cerr << "void*" << transfer_ptr << std::endl;
		std::cerr << "pipeline start... " << std::endl;
	}
    template<class T>
    void operator()(T t)
	{
		std::cerr << "AAA" << std::endl;
		
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr = (void*) running ( (InputType)transfer_ptr, eof_ );	
		
		std::cerr << "transfer ptr "<< transfer_ptr << std::endl;
    }
};

struct pipeline2
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;

	pipeline2(void* inptr, size_t index, bool eof)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof)
	{
		std::cerr << "void*" << transfer_ptr_ << std::endl;
		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
		std::cerr << "pipeline2 start... " << std::endl;
	}
    template<class T>
    void operator()(T t)
	{
		std::cerr << "ABB" << std::endl;
		
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		//transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_ );	
		
		std::cerr << "transfer ptr "<< transfer_ptr_ << std::endl;
    }
};


template<class InputType, class OutputType>
struct run < InputType, OutputType, AnalyzersToBam>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_)
	{
		std::cerr << "pipeline SamToBam " << std::endl;
		AnalyzersToBam o;
		//return 
		o.run (ccc, index, eof_);
	}
};

typedef AnalyzerToBam <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> AnalyzersToBam;

int main(int argc, char** argv)
{
	
//	std::map<int, std::map<int , int>> ooo;
//	std::map<int, std::map<int , int>> ppp;
	std::map<int, std::map<int, std::tuple <int, double> > > ooo;
	std::map<int, std::map<int, std::tuple <int, double> > > ppp;
	
	ooo[0][0]=std::make_tuple (1,0.1);
	ooo[0][1]=std::make_tuple (1,0.2);
	ooo[1][0]=std::make_tuple (1,0.3);
	ooo[1][1]=std::make_tuple (1,0.4);
	
	ppp[0][0]=std::make_tuple (10,10.1);
	ppp[0][1]=std::make_tuple (20,20.1);
	ppp[1][0]=std::make_tuple (30,30.1);
	ppp[1][1]=std::make_tuple (40,40.1);
	
	ooo = ooo+ppp;
	ooo += ppp;

	for(auto &aaa : ooo)
	{
		for(auto &kkk : aaa.second)
		std::cout << (aaa.first) << " " << kkk.first<<" " <<std::get<0>(kkk.second) << std::endl;
	}
	
	namespace mpl = boost::mpl;
	
	/// @brief pipeline session I
	/// @brief pipeline session I
	/// @brief pipeline session I

	typedef mpl::vector	< 
//		mpl::vector < 	void*, 
//						std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
//						Reader	>,
//		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
//						std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
//						Barcode_Handler	>,
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						Adapter_Trimmer	>,
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*,
						//std::vector< Sam<> >*,
						std::tuple<std::string, std::vector< Sam<> > > *,
						Tailer_	>,
		mpl::vector < 	std::tuple<std::string, std::vector< Sam<> > > *, 
						std::map < RawBed<>, uint16_t >*,
						Pipeline_terminator	>
//		mpl::vector < 	std::tuple<std::string, std::vector< Sam<> > > *, 
//						std::stringstream*,
//						Sam_to_Bam	>
//		mpl::vector < 	std::vector< Sam<> >*, 
//						std::map < RawBed<>, uint16_t >*,
//						Sam_to_RawBed	>
	> typelist;
	

	std::map <int, std::vector < Fastq<TUPLETYPE> > > yy;
	std::vector<std::string> read_vec ({"/mnt/godzilla/johnny/PEAT/million_0/origin0_1.fq"});
	Reader o (read_vec, &yy);

	size_t i=0, limit=2;
	std::vector <size_t> job_id;
	
	while (i!=limit)
	{
			auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
			std::cerr << "ptr " << ptr << std::endl;
			auto eof_info = o.Read (ptr, 1000);
			if(i==(limit-1))
			{
				std::cerr << "final -1 job_id " << job_id.back() << std::endl;
				job_id.push_back (GlobalPool.JobPost(
					[&o, i, ptr, eof_info](){
						mpl::for_each<typelist>( pipeline( (void*)ptr, true ) );
					}
					,std::vector<size_t>({job_id})
				));
				std::cerr << "final job_id " << job_id.back() << std::endl;
			}
			else
			{
				job_id.push_back (GlobalPool.JobPost(
					[&o, i, ptr, eof_info](){
						mpl::for_each<typelist>( pipeline( (void*)ptr, false ) );		
					}
				));
			}
		++i;
	}
//	GlobalPool.ResetPool();
//	GlobalPool.FlushPool();	
	for (auto& ii : job_id)
		GlobalPool.FlushOne (ii);
	
	std::cerr << "rawbed_map_: " << Sam_to_RawBed::rawbed_map_ << std::endl;
	
	
	/// @brief pipeline session II
	/// @brief pipeline session II
	/// @brief pipeline session II
	
	typedef mpl::vector	< 
		mpl::vector < 	std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Annotations	>,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Filters >
	> typelist2;
	
	
	int number = 0;
	std::vector< AnnotationRawBed<> > *ptr_vector;
	
	for(auto &rawbed : *Sam_to_RawBed::rawbed_map_)
	{
		if(number % 1000 == 0)
		{
			ptr_vector = new std::vector< AnnotationRawBed<> > ();
		}
		ptr_vector->push_back(rawbed.first);
		
		
		if(number % 1000 == 999)
		{
			// post job
			mpl::for_each<typelist2>( pipeline( (void*)ptr_vector, false ) );
		}
		
		++number;
		if(number == 800)
			break;
	}
	std::cerr << "ptr_vector " << ptr_vector << '\t' << ptr_vector->size() << std::endl;
	mpl::for_each<typelist2>( pipeline( (void*)ptr_vector, false ) );
	
	std::cerr << "pipeline II finish" << std::endl;
	
//	GlobalPool.ResetPool();
//	GlobalPool.FlushPool();	

	typedef mpl::vector	< 
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						void*, //std::vector< std::map <std::string, std::shared_ptr <std::stringstream> > >*,
						Analyzers >
	> typelist3;

	size_t index, limit_run=9;
	size_t final_job_id;
	std::vector <size_t> flush_list (0);
	for (index=0; index!=limit_run; ++index) 
	{
		if(index==(limit_run-1))
		{
			flush_list.push_back ( GlobalPool.JobPost(
				[index, ptr_vector, limit_run]()
				{
					mpl::for_each<typelist3> ( pipeline ( (void*)ptr_vector, limit_run-1 ) );
				}
			) );
		}
		else
		{
			flush_list.push_back ( GlobalPool.JobPost(
				[index, ptr_vector]()
				{
					mpl::for_each<typelist3> ( pipeline ( (void*)ptr_vector, index ) );
				}
			) );
		}
	}
	for (auto ii:flush_list)
		GlobalPool.FlushOne (ii);
	std::cerr << "pipeline III finish" << std::endl;
	std::cerr << "Analyzers finish" << std::endl;
	
	//for test annotation
	
	//./interset GFPKD_Hdac6IP.bg.macs.peak 0 1 2 /home/andy/db/MGI_mm9_no_cluster.bed 0 1 2 4 match nomatch
	/*
	Annotation<	FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
			,AnnoTrait_MGI
			,AnnoIgnoreStrand::IGNORE
			,AnnoType::INTERSET
	>aa;
	
	std::vector<AnnotationRawBed<> > *ttt = new std::vector<AnnotationRawBed<> >();
	
	std::ifstream ooo("GFPKD_Hdac6IP.bg.macs.peak");
	std::string line;
	std::vector<std::string> splited;
	while(std::getline(ooo, line))
	{
		boost::split (splited, line, boost::is_any_of ("\t") );
		
		AnnotationRawBed<> tt;
		tt.strand_ = '+';
		
		tt.chromosome_ = splited[0];
		tt.start_ = std::stoi(splited[1]);
		tt.end_ = std::stoi(splited[2]);
		
		
		int n = aa.Annotate(tt);
		
		if(n == 0)
			continue;
		
		
		for(int i(0); i<n;i++)
		{
			int j=i*2;
			std::cout << tt.chromosome_ << '\t' << tt.start_ << '\t' << tt.end_;
			for(int k=j;k<j+2;k++)
			{
			  std::cout << '\t' << tt.annotation_info_[k] ;
			}
			std::cout << '\n';
		}
	}
	*/
	
/*	
	std::vector< AnnotationRawBed<> > *ptr_vector;
	int number = 0;

	for(auto &rawbed : *Sam_to_RawBed::rawbed_map_)
	{
		if(number % 1000 ==0)
		{
			ptr_vector = new std::vector< AnnotationRawBed<> > ();
		}
		//ptr_vector->push_back(rawbed.first);
		AnnotationRawBed<> annorawbed (rawbed.first);
		aa.Annotate(annorawbed);
		
		
		
		if(annorawbed.annotation_info_.size() == 0)
			continue;
			
		std::cout << "========================" << std::endl;
		std::cout << annorawbed << std::endl;
		for(auto &kk : annorawbed.annotation_info_)
		{
			
			std::cout << kk << std::endl;
		}
		std::cout << "========================" << std::endl;
		//
		++number;
		if(number == 3)
			break;
	}
*/	
}

