#include <unistd.h>
#include <ios>
#include <fstream>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/string.hpp>
#include <boost/type_traits/add_pointer.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/map.hpp>
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>
#include "../src/file_reader.hpp"
#include "../src/barcode_handler/barcode_handler_impl.hpp"
#include "../src/barcode_handler/barcode_handler_impl_static.hpp"
#include "../src/trimmer/single_end_adapter_trimmer.hpp"
#include "../src/aligner/aligner.hpp"
#include "../src/converter/sam2rawbed.hpp"
#include "htslib/htslib/sam.h"

#include "../src/annotator/annotation_set.hpp"
#include "../src/annotator/annotation.hpp"
#include "../src/annotator/filter.hpp"
#include "../src/analyzer/analyzer.hpp"
#include "../src/pipeline/pipeline_preparator.hpp"


void process_mem_usage()
{
	double vm_usage, resident_set;
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0;
   resident_set = rss * page_size_kb;
   
   std::cerr << "VM: " << (vm_usage/1024.0/1024.0) << "; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
}

class Pipeline_terminator
{
public:
    Pipeline_terminator()
    {}
};

class Pipeline_terminator2
{
public:
    Pipeline_terminator2()
    {}
};

typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;
typedef FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE > Reader;
typedef SingleEndAdapterTrimmer < ParallelTypes::M_T, Fastq, TUPLETYPE, QualityScoreType::SANGER > Adapter_Trimmer;
typedef Aligner< Aligner_trait<> > Tailer_;
typedef Sam2RawBed < std::map <int, std::vector< Sam<> > >* > Sam_to_RawBed;

	/// @brief vector 順序越前面權重越低，越後面越高
	/// @brief tuple 0=>字串, 1=>is_filter;
	/// @brief '^', '$', '%', 'R'
typedef boost::mpl::vector 
< 	
	boost::mpl::vector< boost::mpl::string<'linc', 'RNA ', 'gene'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
> Filter_Type_List;
typedef FilterWorker <std::vector< AnnotationRawBed<> >*, Filter_Type_List> Filters;

/// @brief 定義 Annotations
class AnnoTrait_MGI
{
public:
	const char* file_path = "/home/andy/db/MGI_mm9_no_cluster.bed";
};

class AnnoTrait_QQQ
{
public:
	const char* file_path = "/home/andy/db/MGI_mm9_no_cluster.bed";
};

typedef AnnotationSet < 
		std::vector< AnnotationRawBed<> >,
		1,
		Annotation<	FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
			,AnnoTrait_MGI
			,AnnoIgnoreStrand::IGNORE
			,AnnoType::INTERSET
		>
> Annotations;

/// @brief 定義Analyzer
typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinter> AnaParaLenDistPrinter;
typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;
typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaParaToBam;
typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaParaToBwg;
typedef boost::mpl::vector
<
	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<-1> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
	>
		,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-3'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<-1> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadTailing>
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeed<> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadSpecies>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 1, 2> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistH::AnalyzerType, boost::mpl::int_< AnalyzerTypes::Heterogeneity > >
		,boost::mpl::pair<AnaParaLenDistH::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbDepthTypeFor3Or5Prime, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbDepthTypeForName, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDistH::Db5PrimeName, boost::mpl::string<'5P'> >
		,boost::mpl::pair<AnaParaLenDistH::Db3PrimeName, boost::mpl::string<'3P'> >
	>
	, boost::mpl::map
	<
		 boost::mpl::pair<AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > >
		,boost::mpl::pair<AnaParaToBam::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaToBam::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaToBam::DbDepthType, boost::mpl::int_<0> >
	>
    , boost::mpl::map
    <   
         boost::mpl::pair<AnaParaToBwg::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBwg > > //LengthDistribution > >
        ,boost::mpl::pair<AnaParaToBwg::FilterType, boost::mpl::int_<1> >
        ,boost::mpl::pair<AnaParaToBwg::DbIndexType, boost::mpl::int_<0> >
        ,boost::mpl::pair<AnaParaToBwg::DbDepthType, boost::mpl::int_<0> >
    >
> AnalyzerTypeList;
typedef AnalyzerC <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> AnalyzersC;


template <typename BARCODE_TYPE, int N>
struct TupleToVector
{
	typedef typename std::tuple_element<N, BARCODE_TYPE>::type ListedType;
	static void ToDynamic (std::vector<std::string>& output)
	{
		TupleToVector <BARCODE_TYPE, N-1>::ToDynamic (output);
 		output.push_back (boost::mpl::c_str <ListedType>::value);
	}
};

template <typename BARCODE_TYPE>
struct TupleToVector <BARCODE_TYPE, 0>//std::tuple_size <BARCODE_TYPE>::value-1>
{
	typedef typename std::tuple_element<0, BARCODE_TYPE>::type ListedType;
	static void ToDynamic (std::vector<std::string>& output)
	{
 		output.push_back (boost::mpl::c_str <ListedType>::value);
	}
};


std::mutex m1;
template<class InputType, class OutputType, class ObjectType>
struct run 
{
	OutputType operator()(InputType i,bool eof_)
	{
		std::cerr << "pipeline error " << std::endl;
		return 0;
	}
};

/// @brief Pipeline I
template<class InputType, class OutputType>
struct run < InputType, OutputType, Adapter_Trimmer>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Adapter_Trimmer " << std::endl;
		ParameterTraitSeat <QualityScoreType::SANGER> Qoo ("CGTATGCCGTCTTC");//("AGATCGGAAGAGCGG");
		std::vector<int> trim_pos;
//		int nthread = 1;	//18.84s	88%	
//		int nthread = 2;	//10.69s	193%
//		int nthread = 4;	//10.91s	193%
		int nthread = 8;	//10.59s	183%
		Adapter_Trimmer o (Qoo);

		for (auto& QQ : *ccc)
			o.Trim (ccc, nthread, trim_pos, QQ.first);

		//std::cerr << "done pipeline Adapter_Trimmer " << std::endl;
		return ccc;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Tailer_>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Tailer " << std::endl;
		Tailer_ o;
		//OutputType Q = new std::map < int, std::tuple<std::string, std::vector< Sam<> > >* > ();	
		OutputType Q =  new std::map <int, std::vector< Sam<> > > ();
		for (auto& QQ : *ccc)
		{
			auto gg = o.search(ccc, 3, 18, 100, 200, QQ.first);
			Q->insert ({QQ.first, std::vector< Sam<> >()});	//avoid copy in the future

			std::swap ((*Q)[QQ.first], (*gg));
			delete gg;
			//std::stringstream ss;
			//ss << "out_" << QQ.first << ".sam";
			//std::ofstream of (ss.str().c_str(), std::ios::app);
			//for (auto& aa : (*Q)[QQ.first])
			//	of << aa;
			//of.close();
		}
		delete ccc;
		//std::cerr << "done pipeline Tailer " << std::endl;
		return Q;
	}
};

typedef std::tuple <
//		boost::mpl::string<'null'>
        boost::mpl::string<'A'>,
        boost::mpl::string<'C'>,
        boost::mpl::string<'G'>,
        boost::mpl::string<'T'>
        > BarcodeType;
typedef BarcodeHandler <BarcodeHandleScheme::Five_Prime, BarcodeType> BarcodeProcessor;

template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Pipeline_terminator " << std::endl;
		Sam_to_RawBed o;
		auto raw_map = o.Convert2 (ccc);//o.Convert ( &(std::get<1>(*ccc)) );

		//std::cerr << "\tpipeline Sam_to_Bam " << std::endl;
		//建構時，傳入上傳 functor

		if (boost::is_same <typename std::tuple_element<0, BarcodeType>::type, boost::mpl::string<'null'> >::value)
			;
		else
		{
			std::vector <std::string> barcode_vec;
			TupleToVector<BarcodeType, std::tuple_size <BarcodeType>::value-1>::ToDynamic (barcode_vec);
		}
//		for (auto& Q : barcode_vec)
//			std::cerr<<Q<<'\t';

		typedef boost::mpl::map < boost::mpl::pair<AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > > > TypeList;
		AnalyzerImpl< std::map <int, std::vector< Sam<> > >, TypeList, AnalyzerTypes::ToBam> Analysis((*ccc));
		Analysis(map_index, eof_);//, barcode_vec);  
		delete ccc;
		OutputType ll;
		//std::cerr << "done2 pipeline Pipeline_terminator " << std::endl;
		return ll;
	}
};

/// @brief Pipeline II
template<class InputType, class OutputType>
struct run < InputType, OutputType, Annotations>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::vector<std::string> AnnoDBPath
		{
			 "/home/andy/db/MGI_mm9_no_cluster.bed"
//			,"/home/andy/db/MGI_mm9_no_cluster.bed"
		};
//		std::cout << "vector size of raw bed : " << ccc->size() << std::endl;
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		for(auto &annorawbed : *ccc)
		{
//			std::cout << annorawbed << std::endl;
			if(annorawbed.annotation_info_.size() == 0)
				continue;				
//			std::cout << "========================" << std::endl;
//			
//			for(auto &kk : annorawbed.annotation_info_)
//			{
//				for(auto &kkk : kk)
//				{
//					std::cout << kkk << std::endl;
//				}
//				
//			}
//			std::cout << "========================" << std::endl;
		}
		return ccc;		
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index)
	{
		std::vector<std::string> AnnoDBPath
		{
			 "/home/andy/db/MGI_mm9_no_cluster.bed"
//			,"/home/andy/db/MGI_mm9_no_cluster.bed"
		};
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		return ccc;		
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Filters>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
//		std::cerr << "pipeline Filter " << std::endl;
		Filters o;
		return o.Filter (ccc);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index)
	{
		Filters o;
		return o.Filter (ccc);
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, AnalyzersC>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_)
	{
		AnalyzersC o;
		auto rr = o.run (ccc, index, eof_);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		AnalyzersC o;
		//std::cerr<<"run with barcode_index: "<<barcode_index<<'\n';
		//auto rr = 
		o.run (ccc, index, eof_, barcode_index);
		return ccc;//NULL;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator2>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		delete ccc;
		return NULL;
	}
};


struct pipeline2
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;

	pipeline2(void* inptr, size_t index, bool eof)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof)
	{
//		std::cerr << "void*" << transfer_ptr_ << std::endl;
//		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
//		std::cerr << "pipeline2 start... " << std::endl;
	}
    template<class T>
    void operator()(T t)
	{
//		std::cerr << "ABB" << std::endl;
		
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_ );	
		
//		std::cerr << "transfer ptr "<< transfer_ptr_ << std::endl;
    }
};

struct pipeline3
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;
	size_t barcode_index_;
	pipeline3(void* inptr, size_t index, bool eof, size_t barcode_index=0)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof), barcode_index_ (barcode_index)
	{
//		std::cerr << "void*" << transfer_ptr_ << std::endl;
//		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
//		std::cerr << "pipeline2 start... " << std::endl;
	}
    template<class T>
    void operator()(T t)
	{
//		std::cerr << "ABB" << std::endl;
		
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_, barcode_index_ );	
		
//		std::cerr << "transfer ptr "<< transfer_ptr_ << std::endl;
    }
};


ThreadPool PipePool ( 1 );

int main(int argc, char** argv)
{
    std::string qq //("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
	("/mnt/godzilla/GENOME/db/hg19/hg19.fa");//("/mnt/godzilla/GENOME/db/mm9/mm9.fa");
	std::string qq2 //("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//
	("/mnt/mammoth/jones/bwt_table/hg19/hg19");//("/mnt/godzilla/BOWTIE_INDEX/tailer/mm9");
    
    std::cerr << "INIT Memory usage: "; process_mem_usage();
    
	PipelinePreparator<> PP (qq, qq2);
	std::vector <std::string> barcode_vec(0);
    if (boost::is_same <typename std::tuple_element<0, BarcodeType>::type, boost::mpl::string<'null'> >::value)                                                                         
        barcode_vec.push_back ("all");
    else
        TupleToVector<BarcodeType, std::tuple_size <BarcodeType>::value-1>::ToDynamic (barcode_vec);

    PipelinePreparator<>::gDeviceParameter_.bs_basic_url_="https://api.basespace.illumina.com/";
    PipelinePreparator<>::gDeviceParameter_.bs_version_="v1pre3/";
    PipelinePreparator<>::gDeviceParameter_.bs_app_result_="appresults/7525518/";
	//2938939/";//2898896/";//1650649/";
//  PipelinePreparator<>::gDeviceParameter_.bs_file_path_="files?name=text.txt&";
//  PipelinePreparator<>::gDeviceParameter_.bs_dir_path_="directory?name=output/sample-0&";
    PipelinePreparator<>::gDeviceParameter_.bs_tailing_str_="multipart=true";

    PipelinePreparator<>::gDeviceParameter_.bs_access_token_ = "x-access-token: 7b88f5bc19c342f5937259692b0c3eed";
	//ac84971a44bd43189403fe19c6f4f9fd";//fe7e579479fb40aa97a36643ba70915c";//4b2bf2c94ea34721ac01fc177111707f";
    PipelinePreparator<>::gDeviceParameter_.bs_content_type_ = "Content-Type: application/txt";
    PipelinePreparator<>::gDeviceParameter_.bs_upload_content_type_  = "Content-Type: multipart/form-data";


	namespace mpl = boost::mpl;
	
	/// @brief pipeline session I
	/// @brief pipeline session I
	/// @brief pipeline session I

	typedef mpl::vector	< 
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						Adapter_Trimmer	>,
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*,
						std::map <int, std::vector< Sam<> > >*,
						Tailer_	>,
		mpl::vector < 	std::map <int, std::vector< Sam<> > >*,
						std::map < int, std::map < RawBed<>, uint16_t > >*,
						Pipeline_terminator	>
	> typelist;
	
	std::cerr << "Load index done. Memory usage: "; process_mem_usage();
	
	std::map <int, std::vector < Fastq<TUPLETYPE> > > yy;
	//std::vector<std::string> read_vec ({"/mnt/godzilla/jones/human/dra000205/DRX000313/DRR000555.fastq"});////
	//std::vector<std::string> read_vec ({"mini_test.fastq"});
	std::vector<std::string> read_vec ({"/mnt/godzilla/FASTQ/test_download.fq"});
	
	Reader o (read_vec, &yy);

	size_t ii=0, limit=10;
	std::vector <size_t> job_id;

//	FastQ Archive writting
//	----------------------------------------------------------------------
//	std::vector< Fastq<TUPLETYPE> > QQR;
//	auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
//	int ind=0, jj=10;
//	while (ind!=jj)//(true)
//	{
//		auto eof_info = o.Read_NSkip (ptr, 40000);
//		for (auto& item: (*ptr)[0])
//			QQR.push_back (item);
//		//		arc0 & item;
//		if (eof_info)
//			break;
//		++ind;
//	}
//	std::ofstream ofs ("/home/oman/work/pokemon/fastq_archive_partial");
//	boost::archive::text_oarchive arc0 (ofs);
//	arc0 & QQR;
//	ofs.close();
//	std::cerr<<"done archive"<<'\n';
//	delete ptr;
//	return 5566;

	while (true)//	(ii!=limit)
	{
		auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
		auto eof_info = o.Read_NSkip (ptr, 40000);
		
		BarcodeProcessor BP;
		BP (ptr);

//		mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, true) );//false ) );		
//		break;

		if (eof_info)//	(ii==(limit-1))
		{
			job_id.push_back (PipePool.JobPost(
				[&o, ptr, eof_info, ii](){
				mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, true ) );
				}
				,job_id
			));
			//std::cerr << "final job_id " << job_id.back() << std::endl;
			break;
		}
		else
		{
			job_id.push_back (PipePool.JobPost(
				[&o, ptr, eof_info, ii](){
			  	mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, false ) );		
				}
			));
		}
		std::cerr << "Piplein I. Memory usage: "; process_mem_usage();
		++ii;
	}
	PipePool.FlushPool();	
	std::cerr << "Pipeline session I Finish. Memory usage: "; process_mem_usage();

//	std::cerr<<"SLEEEEEEEEEEEEEEEEEEEEEEEPPPPPPPPPPPPPPPPPP"<<'\n';
//	system ("sleep 20");
//	return 0;	

	/// @brief pipeline session II
	/// @brief pipeline session II
	/// @brief pipeline session II
	
	std::cerr << "E Memory usage: "; process_mem_usage();
		
	PP.ReleaseIndex();
	
//	for (auto& q : *Sam_to_RawBed::rawbed_map2_)
//		std::cerr<<"str : map.size "<<q.first<<"\t"<<q.second.size()<<'\n';
	
	std::cerr << "Release Tailor indexer Memory usage: "; process_mem_usage();

    PP.Load ();
	
	std::cerr << "Load Genome. Memory usage: "; process_mem_usage();
	
	typedef mpl::vector	< 
		mpl::vector < 	std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Annotations	>,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Filters >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						AnalyzersC >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						void*, 
						Pipeline_terminator2>
	> typelist2;

//	RawBed Archive writting
//	----------------------------------------------------------------------------------
	std::cerr<<"rawbed archive"<<'\n';
	std::ofstream ofs ("/home/oman/work/pokemon/rawbed_archive_test_download");
	//std::ofstream ofs ("/home/oman/work/pokemon/rawbed_archive_full_chrall_nobarcode");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & ( *Sam_to_RawBed::rawbed_map2_);
	ofs.close();
	std::cerr<<"done archive"<<'\n';
	
	for (auto& content : *Sam_to_RawBed::rawbed_map2_)
	{
		std::cerr<<"raw_bed_map2_ "<<content.first<<'\t';
		std::cerr << "Pipeline II Memory usage: "; process_mem_usage();
		std::cerr << "content " << content.second.size() << std::endl;
		
		int iii=content.first;
		auto itr = content.second.begin();
		//int limit_run = 20;
		std::vector<size_t> flush_list (0);
		int index=0;
		while (itr != content.second.end())//(true)
		{
			std::vector< AnnotationRawBed<> > *ptr_vector = new std::vector <AnnotationRawBed<> > ();
			int idx=0;
			int idx_max = 400;
			while (idx!=idx_max && itr!=content.second.end())
			{
				ptr_vector->push_back (itr->first);
				++itr;
				++idx;
			}

			if(itr==content.second.end())
			{
	            flush_list.push_back ( PipePool.JobPost(
	                [index, ptr_vector, iii]()
	                {
						mpl::for_each<typelist2>( pipeline3 ( (void*)ptr_vector, index, true, iii ) );
	                }
					, flush_list
	            ) );
				break;
	        }
	        else
	        {
	            flush_list.push_back ( PipePool.JobPost(
	                [index, ptr_vector, iii]()
	                {
						mpl::for_each<typelist2>( pipeline3 ( (void*)ptr_vector, index, false, iii ) );
	                }
	            ) );
			}
			++index;
		}
		PipePool.FlushPool();

		AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::ClearContent ();

		typedef boost::mpl::map
			<   
			boost::mpl::pair<AnaParaToBwg::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBwg > > //LengthDistribution > >
			,boost::mpl::pair<AnaParaToBwg::FilterType, boost::mpl::int_<1> >
			,boost::mpl::pair<AnaParaToBwg::DbIndexType, boost::mpl::int_<0> >
			,boost::mpl::pair<AnaParaToBwg::DbDepthType, boost::mpl::int_<0> >
			> TYPE_LIST_TOBWG;


		typedef AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_TYPELIST_GLOBAL_TOBWG, AnalyzerTypes::ToBwg> ATTB;
		ATTB::WriteBwg (iii);
		ATTB::ClearContent();
	}
	std::cerr << "Pipline session II finish. Memory usage: "; process_mem_usage();
    PP.ReleaseChr ();
	std::cerr << "chromosome deleted. Memory usage: "; process_mem_usage();
	std::cerr << "Analyzers finish" << std::endl;
}

