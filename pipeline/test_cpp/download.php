<?php
	$base_url = "http://www.jhhlab.tw";

	$genome = $argv[1];
	if($genome == "")
		die("no genome");
	$list = get_file_name($genome);
	download_list($base_url, $genome, $list);
	
	
	function get_file_name($g)
	{
		return array(
			"$g.chrLen"
			, "$g.chrStart"
			, "$g.NposLen.z"
			, "$g.t_table.bwt"
			, "ENSEMBL.$g.all.bed"
			, "ENSEMBL.$g.miRNA-3p5p.bed"
			, "$g.fa"
//			, "{$g}_random.fa"
		);
	}
	function download_list($url, $g, $list)
	{
		foreach($list as $l)
		{
			if(file_exists($l))
				continue;
			$link = "$url/pipeline_index/$g/$l";
			$cmd = "lftp -c \"pget -n 4 $link\";";
//			$cmd = "cd /mnt;sudo lftp -c \"pget -n 4 $link\";";
			echo "$cmd\n";
			shell_exec($cmd);
		}
//		$cmd = "chmod 777 -R /mnt";
//		shell_exec($cmd);
//		$cmd = "ln -s /mnt/* .";
//		shell_exec($cmd);
	}
	
?>
