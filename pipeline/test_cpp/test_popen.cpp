#include<iostream>
#include<vector>
#include<fstream>
#include<sstream>
#include<string>

#include <unistd.h>
#include <stdio.h>



#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>

#define READ 0
#define WRITE 1

pid_t
popen2(const char *command, int *infp, int *outfp)
{
    int p_stdin[2], p_stdout[2];
    pid_t pid;

    if (pipe(p_stdin) != 0 || pipe(p_stdout) != 0)
        return -1;

    pid = fork();

    if (pid < 0)
        return pid;
    else if (pid == 0)	
    {
        //close(p_stdin[WRITE]);
        //dup2(p_stdin[READ], READ);
        //close(p_stdout[READ]);
        //dup2(p_stdout[WRITE], WRITE);

        //execl("/bin/sh", "sh", "-c", command, NULL);
        //
        //perror("execl");
        
        
        //exit(1);
    }
    
/*
    if (infp == NULL)
        close(p_stdin[WRITE]);
    else
        *infp = p_stdin[WRITE];

    if (outfp == NULL)
        close(p_stdout[READ]);
    else
        *outfp = p_stdout[READ];
        
  */  

    return pid;
}

int
main(int argc, char **argv)
{
    int infp, outfp;
    char buf[20];
	
    //if (popen2("samtools view -Sb -", &infp, &outfp) <= 0)
    if (popen2("samtools view -Sbu -", &infp, &outfp) <= 0)
    {
        printf("Unable to exec sort\n");
        exit(1);
    }

	std::cout << std::endl;
	
	std::ifstream *in = new std::ifstream("/home/andy/andy/bowtie2-2.1.0/samtest/aa.sam");
	std::string bb;
	int len;
	int i=0;
	
	
	//auto pid = fork();

   
	
	while ( std::getline(*in, bb) )
	{
		
		//std::cout << "origin: " << bb << std::endl;
		
		//write(outfd[1],buf.c_str(),buf.size()-1); // Write to child’s stdin
		write(infp, bb.c_str(), bb.size());
		write(infp, "\n", 1);
		
		
		if(i > 5)
		{
			int ii = dup(infp);
			//std::cerr << "close " << close(infp) << std::endl;
			close(infp);
			//close(infp);
			*buf = '\0';
			read(outfp, buf, 20);
			std::cerr.write(buf, 20);// << buf << std::endl;
			infp = dup(ii);
			//printf("buf = '%s'\n", buf);
		}
		
		
		
		i++;
		if(i == 1000)
			break;
	}
	
	
	//if (pid < 0)
    //   return pid;
    //else if (pid != 0)
	
	//while((len = read(outfp, buf, 20)) > 0) {
	    	//buf[len]='\0';
	//		std::cerr.write(buf, 20);// << buf << std::endl;
	//	}
	
	//std::stringstream ll(std::ios::binary);
	

	//std::cerr << "OK" << std::endl;
	//*buf = '\0';
	//read(outfp, buf, 102);
	
	//ll << buf;
	//std::cerr << "ll pos " <<  ll.tellg() << std::endl;
	//printf("buf = '%s'\n", buf);
	

    //write(infp, "Z\n", 2);
    //write(infp, "D\n", 2);
    //write(infp, "A\n", 2);
    //write(infp, "C\n", 2);
    //

    
    

    

    return 0;
}
/*
int main()
{
	
	//FILE * samtools_sam2bam = popen("samtools view -Sb -", "w+");
	//FILE * samtools_sam2bam = popen("samtools view -Sb /home/andy/andy/bowtie2-2.1.0/samtest/aa.sam", "r");
	//if(samtools_sam2bam == NULL)
	//{
	//	std::cerr <<"can not open popen" << std::endl;
	//	return 0;
	//}
	
	//char            aa [1234];
	//std::cout << fgets(aa, 1234, samtools_sam2bam) << std::endl;
	//std::cout << fgets(aa, 1234, samtools_sam2bam) << std::endl;
	//std::cout << fgets(aa, 1234, samtools_sam2bam) << std::endl;
	//std::stringstream ss;
	//std::stringstream kk(samtools_sam2bam);
	
	
	
int outfd[2];
int infd[2];
int oldstdin, oldstdout;
pipe(outfd); // Where the parent is going to write to
pipe(infd); // From where parent is going to read
oldstdin = dup(0); // Save current stdin
oldstdout = dup(1); // Save stdout
close(0);
close(1);
dup2(outfd[0], 0); // Make the read end of outfd pipe as stdin
dup2(infd[1],1); // Make the write end of infd as stdout





if(!fork())
{

	//char *cmd[]={"samtools","view","-Sb","-"};
	char *cmd[]={"/usr/bin/samtools","view", "-Sb", "-", 0};
	close(outfd[0]); // Not required for the child
	close(outfd[1]);
	close(infd[0]);
	close(infd[1]);
	execv(cmd[0],cmd);
	exit(0);
}
else
{

	char input[1024];
	close(0); // Restore the original std fds of parent
	close(1);
	dup2(oldstdin, 0);
	dup2(oldstdout, 1);
	close(outfd[0]); // These are being used by the child
	close(infd[1]);
	
	
	
	std::cout << std::endl;
	
	std::ifstream *in = new std::ifstream("/home/andy/andy/bowtie2-2.1.0/samtest/aa.sam");
	std::string buf;
	
	int i=0;
	while ( std::getline(*in, buf) )
	{
		std::cout << "origin: " << buf << std::endl;
		
		write(outfd[1],buf.c_str(),buf.size()-1); // Write to child’s stdin
		//std::cout << "a: " << buf << std::endl;
		//read(infd[0],input,1024);
		//std::cout << "b: " << buf << std::endl;
		//std::cerr << input << std::endl;
		
		i++;
		if(i == 5)
			break;
	}
	
int len = read(infd[0], input, sizeof(input) - 1);

if (len < 0) {
    perror("read error");
} else {
    input[len] = 0;
    printf("Buffer recived: %s\n", input);
}
	//read(infd[0],input,1024);
	//std::cerr << input << std::endl;

	//
	
	//std::cerr << read(infd[0],input,100) << std::endl;
	//input[read(infd[0],input,100)] = 0; // Read from child’s stdout
	//read(infd[0],input,100);
	//std::cerr << input << std::endl;
	//printf("%s",input);

}

	
	
	return 0;
}
*/


