#include <string>
#include <typeinfo>
#include <vector>
#include <memory>
#include "../src/file_reader.hpp"
#include "../src/iohandler/iohandler.hpp"
#include "../src/pipeline/pipeline_preparator.hpp"
#include "../src/iohandler/basespace_def.hpp"
#include "srna_pipeline_json_parser.hpp"
#include "native_config.hpp"
#include "run_timer.hpp"
RunTimer <> timer;

//typedef IoHandlerBaseSpace IoHandlerType;
typedef IoHandlerBaseSpaceEasyUpload IoHandlerType;

//config.json
/*
{"base_api_url":"https:\/\/api.cloud-hoth.illumina.com\/","app_platform":"https:\/\/cloud-hoth.illumina.com\/","access_token":"841dd9bbbb5642758ea8f01d2f0eae47","appsessionuri":"v1pre3\/appsessions\/4187328\/","adapter_sequence":"AGATCGGAAGAGCG","barcode_type":"5","project_href":"v1pre3\/projects\/1903927\/","reference_genome":"hg19","sample_href":"v1pre3\/samples\/1277292\/","input_read_count":441812,"paired_end_input":true,"barcode_sequence_vec":["A","C","G","T"],"download_file":{"genome_path":"hg19.fa","database_path1":"ENSEMBL.all.bed","database_path2":"ENSEMBL.miRNA-3p5p.bed","index_prefix":"hg19"},"app_results":"v1pre3\/appresults\/3256256\/","file_list":[{"Name":"BC-12_S12_L001_R1_001.fastq.gz","Content":"v1pre3\/files\/12583990\/","Size":59505419},{"Name":"BC-12_S12_L001_R2_001.fastq.gz","Content":"v1pre3\/files\/12583991\/","Size":57995799}]}
*/


void process_mem_usage()
{
	double vm_usage, resident_set;
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage	 = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
			   >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
			   >> utime >> stime >> cutime >> cstime >> priority >> nice
			   >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage	 = vsize / 1024.0;
   resident_set = rss * page_size_kb;
   
   std::cerr << "VM: " << (vm_usage/1024.0/1024.0) << " GB; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
}

void make_random_string(std::vector<std::string> &r, int filenum = 10, int filesize = 2000)
{
	std::string dic = "abcdefghijklmnopqrstuvwxyz";
	r.resize(filenum);
	for(int j=0; j<filenum; j++)
	{
		for(int i=0; i<filesize; i++)
		{
			r[j] += "a";
		}
	}
	return;
}

void easy_push(int filenum, int filesize)
{
	timer.start_timer ("A1");
	std::vector<std::string> files;
	make_random_string(files, filenum, filesize);
	
	std::vector< std::shared_ptr<IoHandlerType> > vec;
	
	int i = 0;
	for(auto &content : files)
	{
		std::string filename = "Test_" + std::to_string(i) + ".txt";
		DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
        file_writter_parameter.set_filename( filename , "Tests");
		std::shared_ptr <IoHandlerType> out_ (std::make_shared <IoHandlerType> (file_writter_parameter));
		vec.push_back (out_);
		*(out_) << "====================" << std::endl;
		*(out_) << filename << std::endl;
		*(out_) << "--------------------" << std::endl;
		*(out_) << files[i] << std::endl;
		*(out_) << "--------------------" << std::endl;
		out_->bs_async_close();
		i++;
		std::cout << " ============ TEST " << i << std::endl;
	}
	process_mem_usage();
	timer.stop_timer ("A1");
	timer.start_timer ("A3");
	for (auto& q : vec)
		q->close();
	timer.stop_timer ("A3");
	
	timer.PrintAllTime();
	process_mem_usage();
}


int main(int argc, char** argv)
{
	SRNAPipelineJsonParser Json_content (argv[1]);
	g_adapter_seq = Json_content.adapter_sequence_;
	genome_fa = Json_content.genome_path_;//("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
	genome_index_prefix = Json_content.index_prefix_; //("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//

	PipelinePreparator<>::gDeviceParameter_.bs_basic_url_ = Json_content.base_api_url_;//"https://api.cloud-hoth.illumina.com/";//"https://api.basespace.illumina.com/";
	PipelinePreparator<>::gDeviceParameter_.bs_app_result_ = Json_content.appresult_href_.substr (7);//"appresults/2538015/";//"appresults/7525518/";
	PipelinePreparator<>::gDeviceParameter_.bs_access_token_ = ("x-access-token: "+Json_content.access_token_);//9ca77f5225404c448fbe57096640ec19";//"x-access-token: 7b88f5bc19c342f5937259692b0c3eed";
	PipelinePreparator<>::gDeviceParameter_.bs_version_ = "v1pre3/";
	PipelinePreparator<>::gDeviceParameter_.bs_tailing_str_ = "multipart=true";
	PipelinePreparator<>::gDeviceParameter_.bs_content_type_ = "Content-Type: application/txt";
	PipelinePreparator<>::gDeviceParameter_.bs_upload_content_type_ = "Content-Type: multipart/form-data";

	//PipelinePreparator<> PP (genome_fa, genome_index_prefix);
	//std::vector <std::string> barcode_vec ({"all"});
	//if (Json_content.barcode_sequence_vec_.size()!=0)
	//	barcode_vec = Json_content.barcode_sequence_vec_;
	//PP.GetBarcodeVec (barcode_vec);
	//
	//
	//fastq_file_vec.clear(), fastq_size_vec.clear();
	//for (auto&qq: Json_content.url_vec_)
	//	fastq_file_vec.push_back (std::vector<std::string>({qq}));
	//for (auto&qq: Json_content.size_vec_)
	//	fastq_size_vec.push_back (std::vector<std::uint64_t>({qq}));
	//FileReaderWrapper<IoHandlerBaseSpaceDownload> frw (fastq_file_vec, fastq_size_vec);
	easy_push(std::stoi(argv[2]), std::stoi(argv[3]));
	



	return 0;
}