
#include <iostream>
#include <string>

#include "runtime_analyzer.hpp"

int main(int argc, char** argv)
{

	std::string filename = argv[1];
	std::string region = argv[2];
	std::string region2 = argv[3];
	
	
	//std::vector<std::string> results(0);
	//BamParserRegion bam_parser(filename);
	
	//int r1 = bam_parser.ReadRegionLines(region, results);
	//int r2 = bam_parser.ReadRegionLines(region2, results);
	
	//std::string s;
	
	//bam_parser.set_region(region);
	//bam_parser.getline(s);
	
	//std::cout << r1 << " " << r2 << " " << s << std::endl;
	
	//bam_parser.getline(s);
	//std::cout << s << std::endl;
	//bam_parser.getline(s);
	//std::cout << s << std::endl;
	
	
	std::vector< AnnotationRawBed<> > rawbed_results;
	RuntimeAnalyzer ra(filename);
	//ra.bam2rawbed(region, rawbed_results);
	
	//ra.LengthDistribution(region);
	ra.TailingRatio(region);
	//ra.SeedRatio(region);
	//ra.First1nt(region);
	
	return 0;
}