#include <stdio.h>                                                                                                                                              
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "htslib/htslib/sam.h"

#include <string>
#include <fstream>
#include <iostream>
#include <thread>

int fd[2];
//int fd[2];




int main()
{
	pipe(fd);
	
	htsFile *out;//
	char modew[8];//
	strcpy(modew, "w");//
	sprintf(modew + 1, "%d", 1);//
	strcat(modew, "b");//
	out = (htsFile*)calloc(1, sizeof(htsFile));//
	out->fn = strdup("-");//
	out->is_be = ed_is_big();//
	out->is_bin = 1;//
	out->fp = bgzf_dopen(fd[1], modew);//
	
	std::thread t( 
		[&]()
		{
			char* buf  = new char[30*1024*1024];
			char* pbuf = buf;
			size_t len;
			while((len=read(fd[0], pbuf, 1024))>0) 
			{
				pbuf += len;
				std::cout<<"child: "<<len<<std::endl;
				if (pbuf - buf >= 25*1024)
				{
					std::cerr.write(buf, pbuf-buf);
					pbuf = buf;
				}
			}
			std::cerr.write(buf, pbuf-buf);
			delete[] buf;
			//dup2(saved_stdout, STDOUT_FILENO);
			//exit(0);
		}
	);

	
		std::ifstream *in_file = new std::ifstream("/home/andy/andy/pokemon/pipeline/out2.sam");
		//std::ifstream *in_file = new std::ifstream("/home/andy/andy/bowtie2-2.1.0/samtest/aa.sam");
		std::string bb;
		//int len;
		std::string line;
		std::getline(*in_file, line);
		bb+=line;
		//std::getline(*in_file, line);
		//bb=bb+"\n"+line;
		std::getline(*in_file, line);
		bb=bb+"\n"+line+"\n";
		
		
		bam_hdr_t *bam_header;//
		bam1_t *bam;//
		
		int i=0;
		char* p = (char*)bb.c_str();
	
		//bam_header = sam_hdr_parse(, bb.c_str() );//
		bam_header = sam_hdr_parse(bb.length(), bb.c_str() );//
		std::cout << bb.length() << " " << bb << std::endl;
		char * cc = (char*) malloc(bb.length()+1);//// char[bb.length()+1];
		strcpy(cc, (char*) bb.c_str());//
		
		bam_header->l_text = bb.length();//
		bam_header->text = cc;//
		bam_header->ignore_sam_err = 0;//
		
		bam = bam_init1();//
	
		//std::cout << "AA1" << std::endl;
		//sam_hdr_write(out, bam_header);//
		//std::cout << "AA2" << std::endl;
	
		kstring_t ks;
		int q=0;
		while ( getline(*in_file, line) )
		{
			//std::cerr<<"parent: "<<q<<std::endl;
			q++;
			kstring_t ks;//
			ks.l = line.length();////length of last cigar
			ks.s = (char*) line.c_str();////bb should include last cigar
			ks.m = line.length();//
			kroundup32(ks.m);//
			sam_parse1(&ks, bam_header, bam);//
			
			std::cout << "bam l_data "<< bam->l_data << std::endl;
			std::cout << "block offset "<< ((BGZF*) out->fp)->block_offset << std::endl;
			std::cout << "block address "<< ((BGZF*) out->fp)->block_address << std::endl;
			int bam_length = sam_write1(out, bam_header, bam);//
			std::cout << "bam_length "<< bam_length << std::endl;
			
		}
		
		std::cout << "block offset "<< ((BGZF*) out->fp)->block_offset << std::endl;
		std::cout << "block address "<< ((BGZF*) out->fp)->block_address << std::endl;
				
		sam_close(out);//
		//dup2(STDOUT_FILENO,fd[1]);
		close(fd[1]);
		
	
		std::cout<<"123"<<std::endl;
		delete in_file;
		//write(fd[1],"123",3);
		bam_destroy1(bam);//
		
		bam_hdr_destroy(bam_header);//
		
		
		
		//t.join();
	
	
	
	
	t.join();
	
	std::cout << "run 2 ok " << std::endl;
	return 0;
  
 
}
