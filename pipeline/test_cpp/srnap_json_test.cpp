#include <sstream>
#include <iostream>
#include <fstream>
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "srna_pipeline_json_parser.hpp"

int main (void)
{
	SRNAPipelineJsonParser Json_content ("config.json");	
	std::cerr<<"is_paired_input & read_count "<<Json_content.is_paired_input_ <<'\t'<< Json_content.read_count_<<'\n';
	return 0;
}


