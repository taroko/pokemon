

typedef std::tuple <
//boost::mpl::string<'null'>
boost::mpl::string<'A'>,
boost::mpl::string<'C'>,
boost::mpl::string<'G'>,
boost::mpl::string<'T'>
> BarcodeType;
typedef BarcodeHandler <BarcodeHandleScheme::Five_Prime, BarcodeType> BarcodeProcessor;

std::string g_adapter_seq ("AAAAAA");

struct AnnoTrait_MGI{ const char* file_path = "ENSEMBL.hg19.all.bed"; };
struct AnnoTrait_mirna_3p5p{ const char* file_path = "ENSEMBL.hg19.miRNA-3p5p.bed"; };

