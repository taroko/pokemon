
//typedef std::tuple <
////boost::mpl::string<'null'>
//boost::mpl::string<'A'>,
//boost::mpl::string<'C'>,
//boost::mpl::string<'G'>,
//boost::mpl::string<'T'>
//> BarcodeType;
//typedef BarcodeHandler <BarcodeHandleScheme::Five_Prime, BarcodeType> BarcodeProcessor;

std::string g_adapter_seq ("AAAAAA");

struct AnnoTrait_MGI{ const char* file_path = "biomart.all.bed"; }; //"ENSEMBL.all.bed"; };
struct AnnoTrait_mirna_3p5p{ const char* file_path = "biomart.miRNA-3p5p.bed"; }; //"ENSEMBL.miRNA-3p5p.bed"; };


std::string genome_fa ("/mnt/godzilla/GENOME/db/mm10/mm10.fa");
//(genome+"_random.fa");
//("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
//("/mnt/godzilla/GENOME/db/hg19/hg19.fa");
std::string genome_index_prefix	("/mnt/godzilla/BOWTIE_INDEX/tailer/mm10/mm10");
//("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//
//("/mnt/mammoth/jones/bwt_table/hg19/hg19");

//std::vector < std::vector <std::string> > fastq_file_vec({{ "/home/andy/andy/work_tailor/fastq/SRR529100.trim.fastq" }});
std::vector < std::vector <std::string> > fastq_file_vec({{ "/home/andy/andy/work_tailor/fastq/SRR529100.2000000.trim.fastq" }});
//std::vector < std::vector <std::string> > fastq_file_vec({{"/mnt/godzilla/jones/human/gse34494/fastq/SRR390723.fastq"}, {"/mnt/godzilla/jones/human/gse34494/fastq/SRR390724.fastq"}});
//std::vector < std::vector <std::string> > fastq_file_vec({{ "/mnt/mammoth/oman/SRR922283.fastq" }});
std::vector < std::vector <uint64_t> > fastq_size_vec(0);

int total_fastq = 18478204;
int progress_pipeline1 = 70;
int progress_pipeline2 = 30;


