#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS                                                                                                                                           
#define BOOST_MPL_LIMIT_VECTOR_SIZE 50
#include <unistd.h>
#include <ios>
#include <fstream>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/string.hpp>
#include <boost/type_traits/add_pointer.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/map.hpp>
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>
#include "../src/file_reader.hpp"
#include "../src/barcode_handler/barcode_handler_impl.hpp"
#include "../src/barcode_handler/barcode_handler_impl_static.hpp"
#include "../src/trimmer/single_end_adapter_trimmer.hpp"
#include "../src/aligner/aligner.hpp"
#include "../src/converter/sam2rawbed.hpp"
#include "htslib/htslib/sam.h"

#include "../src/annotator/annotation_set.hpp"
#include "../src/annotator/annotation.hpp"
#include "../src/annotator/filter.hpp"
#include "../src/analyzer/analyzer.hpp"
#include "../src/pipeline/pipeline_preparator.hpp"
#include "../src/iohandler/iohandler.hpp"
#include "../src/pipeline/reader_wrapper.hpp"
#include "../src/format/json_handler.hpp"

#include "config.hpp"

void process_mem_usage()
{
	double vm_usage, resident_set;
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage	 = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
			   >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
			   >> utime >> stime >> cutime >> cstime >> priority >> nice
			   >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage	 = vsize / 1024.0;
   resident_set = rss * page_size_kb;
   
   std::cerr << "VM: " << (vm_usage/1024.0/1024.0) << "; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
}





typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;
typedef SingleEndAdapterTrimmer < ParallelTypes::M_T, Fastq, TUPLETYPE, QualityScoreType::SANGER > Adapter_Trimmer;
typedef Aligner< Aligner_trait<> > Tailer_;
typedef Sam2RawBed < std::map <int, std::vector< Sam<> > >* > Sam_to_RawBed;

/// @brief vector 順序越前面權重越低，越後面越高
/// @brief tuple 0=>字串, 1=>is_filter;
/// @brief '^', '$', '%', 'R'
typedef boost::mpl::vector
<
	boost::mpl::vector< boost::mpl::string<'misc', '_RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'Mt_', 'rRNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'Mt_', 'tRNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'rRNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'sn', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'sc', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'srp', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'mi', 'RNA'>, boost::mpl::int_<0>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'linc', 'RNA'>, boost::mpl::int_<0>, boost::mpl::char_<'='> >
> Filter_Type_List;

typedef FilterWorker <std::vector< AnnotationRawBed<> >*, Filter_Type_List> Filters;

/// @brief 定義 Annotations
//class AnnoTrait_MGI
//{
//public:
//	const char* file_path = "mm9.MGI.no_cluster.bed";//"/home/andy/db/MGI_mm9_no_cluster.bed";
//};
//AnnoTrait_mirna_3p5p

typedef AnnotationSet < 
		std::vector< AnnotationRawBed<> >,
		2,
		Annotation< FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
            ,AnnoTrait_mirna_3p5p
            ,AnnoIgnoreStrand::NO_IGNORE
            ,AnnoType::INTERSET
        >,
		Annotation<	FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
			,AnnoTrait_MGI
			,AnnoIgnoreStrand::NO_IGNORE
			,AnnoType::INTERSET
		>
> Annotations;

/// @brief 定義Analyzer
typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaParaLenDist;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinter> AnaParaLenDistPrinter;
typedef AnalyzerParameter <AnalyzerTypes::Heterogeneity> AnaParaLenDistH;
typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaParaToBam;
typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaParaToBwg;
typedef AnalyzerParameter <AnalyzerTypes::MappabilityPrinter> AnaParaMapPrinter;
typedef AnalyzerParameter <AnalyzerTypes::HeterogeneityPrinter> AnaParaHeterogeneityPrinter;
typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinterDualAnalyzer> AnaParaLenDistPrinterDualAnalyzer;
/// @brief 定義 analyzer list
typedef boost::mpl::vector
<
/*	tailing ratio test
	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >
        ,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadFullLength >
        ,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>,
	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >
        ,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadFullLength >
        ,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>,
    boost::mpl::map
    <  
        boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
        boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream>
    >,
    boost::mpl::map
    <  
        boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
        boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<1> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream>
    >,
    boost::mpl::map
    <  
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<1> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream>
    >
*/





	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
        ,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadFullLength >
        ,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountGMPM >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>,
	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
        ,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadFullLength >
        ,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountPMOnly >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>,

    boost::mpl::map
    <  
        boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
        boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<25> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<25> >,
        boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream>
    >,
    boost::mpl::map
    <  
        boost::mpl::pair< AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >,
        boost::mpl::pair< AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<1> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
        boost::mpl::pair< AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >,
        boost::mpl::pair< AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<25> >,
        boost::mpl::pair< AnaParaLenDistPrinter::IoHandlerType, IoHandlerOfstream>
    >,
    boost::mpl::map
    <  
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinterDualAnalyzer > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::AnnoIdx2, boost::mpl::int_<1> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Xlimit, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Ylimit, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::Zlimit, boost::mpl::int_<0> >,
        boost::mpl::pair< AnaParaLenDistPrinterDualAnalyzer::IoHandlerType, IoHandlerOfstream>
    >


/*
	// 0, biotype lendist (protein coding gene, miRNA, lincRNA, snoRNA ...)
	boost::mpl::map
	<
	   boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
	  ,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
	  ,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<-1> >
	  ,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
	  ,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
	>
	// 1, 所有 miRNA detail(mir-1, mir-2 ...) lendist
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepth2NameType, boost::mpl::string<'-1'> >
	>
	// 2, miRNA tailing lendist (with seq, e.g., AA 100, TT 200,...)
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		//,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadTailing>
	>
	// 3,
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		//,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 1> >
	>
	// 4,
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		//,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 1> >
	>
	//5
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		//,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeed <1, 6> >
	>
	// 6,real    5m49.963s
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		//,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 0, 2> >
	>
	// 7,
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		//,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'mi', 'RNA'> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'-1'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <1, 0, 2> >
	>
	//0, biotype lendist (protein coding gene, miRNA, lincRNA, snoRNA ...)
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >

	>
*/
	/*
	//1, 所有 miRNA detail(mir-1, mir-2 ...) lendist
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-2'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	
	//2, Tail miRNA tailing lendist (with seq, e.g., AA 100, TT 200,...)
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<2> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	//3
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<3> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	
	//4
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<4> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	//5
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<5> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-2'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	/*
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<6> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<7> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<25> >
	>
	*/
/*
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LenDistPrinter > >
		,boost::mpl::pair<AnaParaLenDistPrinter::AnnoIdx, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xaxis, boost::mpl::pair< AnaParaLenDistPrinter::Len, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zaxis, boost::mpl::pair< AnaParaLenDistPrinter::Anno, boost::mpl::string<'-1'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Yaxis, boost::mpl::pair< AnaParaLenDistPrinter::Seq, boost::mpl::string<'-3'> > >
		,boost::mpl::pair<AnaParaLenDistPrinter::Xlimit, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Ylimit, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistPrinter::Zlimit, boost::mpl::int_<0> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<-1> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
	>
	
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeqDefault>
	>
	
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadTailing>
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeed<> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadSpecies>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 1, 2> >
	>
*/
	/*
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadTailing>
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadCountDefault>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadSeed<> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDist::AnalyzerType, boost::mpl::int_< AnalyzerTypes::LengthDistribution > >
		,boost::mpl::pair<AnaParaLenDist::DbDepthNameType, boost::mpl::string<'miRN', 'A'> >
		,boost::mpl::pair<AnaParaLenDist::GetReadLengthClass, GetReadLengthDefault>
		,boost::mpl::pair<AnaParaLenDist::CalReadCountClass, CalReadSpecies>
		,boost::mpl::pair<AnaParaLenDist::GetReadSeqClass, GetReadFirstNLastComposition <0, 1, 2> >
	>
	*/
/*
	, boost::mpl::map
	<
		 boost::mpl::pair<AnaParaLenDistH::AnalyzerType, boost::mpl::int_< AnalyzerTypes::Heterogeneity > >
		,boost::mpl::pair<AnaParaLenDistH::FilterType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbIndexType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaLenDistH::DbDepthTypeFor3Or5Prime, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::DbDepthTypeForName, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaLenDistH::Db5PrimeName, boost::mpl::string<'miRN', 'A-5p'> >
		,boost::mpl::pair<AnaParaLenDistH::Db3PrimeName, boost::mpl::string<'miRN', 'A-3p'> >
	>
	, boost::mpl::map
	<
		 boost::mpl::pair<AnaParaHeterogeneityPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::HeterogeneityPrinter > >
        ,boost::mpl::pair<AnaParaHeterogeneityPrinter::AnnoIdx, boost::mpl::int_<0> >
	>
	, 
	boost::mpl::map
	<
		 boost::mpl::pair<AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > >
		,boost::mpl::pair<AnaParaToBam::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaToBam::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaToBam::DbDepthType, boost::mpl::int_<0> >//
	>
	, boost::mpl::map
	<   
		 boost::mpl::pair<AnaParaToBwg::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBwg > > //LengthDistribution > >
		,boost::mpl::pair<AnaParaToBwg::FilterType, boost::mpl::int_<1> >
		,boost::mpl::pair<AnaParaToBwg::DbIndexType, boost::mpl::int_<0> >
		,boost::mpl::pair<AnaParaToBwg::DbDepthType, boost::mpl::int_<0> >
	>
	,boost::mpl::map
	<
		 boost::mpl::pair<AnaParaMapPrinter::AnalyzerType, boost::mpl::int_< AnalyzerTypes::MappabilityPrinter > >
	>
*/
> AnalyzerTypeList;

typedef AnalyzerC <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList> AnalyzersC;





std::mutex m1;
template<class InputType, class OutputType, class ObjectType>
struct run 
{
	OutputType operator()(InputType i,bool eof_)
	{
		std::cerr << "pipeline error " << std::endl;
		return 0;
	}
};

/// @brief Pipeline I
template<class InputType, class OutputType>
struct run < InputType, OutputType, Adapter_Trimmer>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Adapter_Trimmer " << std::endl;
		ParameterTraitSeat <QualityScoreType::SANGER> Qoo (g_adapter_seq);//("CGTATGCCGTCTTC");//("AGATCGGAAGAGCGG");
		std::vector<int> trim_pos;
//		int nthread = 1;	//18.84s	88%	
//		int nthread = 2;	//10.69s	193%
//		int nthread = 4;	//10.91s	193%
		int nthread = 8;	//10.59s	183%
		Adapter_Trimmer o (Qoo);

		for (auto& QQ : *ccc)
			o.Trim (ccc, nthread, trim_pos, QQ.first);

		//std::cerr << "done pipeline Adapter_Trimmer " << std::endl;
		return ccc;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Tailer_>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Tailer " << std::endl;
		Tailer_ o;
		//OutputType Q = new std::map < int, std::tuple<std::string, std::vector< Sam<> > >* > ();	
		OutputType Q =  new std::map <int, std::vector< Sam<> > > ();
		for (auto& QQ : *ccc)
		{
			auto gg = o.search(ccc, 3, 18, 100, 200, QQ.first);
			Q->insert ({QQ.first, std::vector< Sam<> >()});	//avoid copy in the future

			std::swap ((*Q)[QQ.first], (*gg));
			delete gg;
			
			/// @brief count mapped reads for mappability 
			{
				std::lock_guard<std::mutex> lock (Sam_to_RawBed::map_insert_mutex_);
				auto &g_reads_mapped_count = Sam_to_RawBed::reads_mapped_count_[QQ.first];
				std::get<0>(g_reads_mapped_count) += std::get<0>(o.reads_map_count_);
				std::get<1>(g_reads_mapped_count) += std::get<1>(o.reads_map_count_);
				std::get<2>(g_reads_mapped_count) += std::get<2>(o.reads_map_count_);
				std::get<3>(g_reads_mapped_count) += std::get<3>(o.reads_map_count_);
				//Sam_to_RawBed::reads_mapped_count_[QQ.first] += o.reads_map_count_;
				
			}
			
		}
		delete ccc;
		//std::cerr << "done pipeline Tailer " << std::endl;
		return Q;
	}
};

class Pipeline_terminator
{
public:
	Pipeline_terminator()
	{}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Pipeline_terminator " << std::endl;
		Sam_to_RawBed o;
		auto raw_map = o.Convert2 (ccc);//o.Convert ( &(std::get<1>(*ccc)) );
		
		typedef boost::mpl::map < 
				boost::mpl::pair<AnaParaToBam::AnalyzerType, boost::mpl::int_< AnalyzerTypes::ToBam > > 
//		        ,boost::mpl::pair<AnaParaToBam::IoHandlerType, IoHandlerBaseSpace >
				> TypeList;

		AnalyzerImpl< std::map <int, std::vector< Sam<> > >, TypeList, AnalyzerTypes::ToBam> Analysis((*ccc));
		Analysis(map_index, eof_); 
		delete ccc;
		OutputType ll;
		return ll;
	}
};

/// @brief Pipeline II
template<class InputType, class OutputType>
struct run < InputType, OutputType, Annotations>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::vector<std::string> AnnoDBPath
		{
			 "/home/andy/db/MGI_mm9_no_cluster.bed"
//			,"/home/andy/db/MGI_mm9_no_cluster.bed"
		};
//		std::cout << "vector size of raw bed : " << ccc->size() << std::endl;
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		for(auto &annorawbed : *ccc)
		{
//			std::cout << annorawbed << std::endl;
			if(annorawbed.annotation_info_.size() == 0)
				continue;				
//			std::cout << "========================" << std::endl;
//			
//			for(auto &kk : annorawbed.annotation_info_)
//			{
//				for(auto &kkk : kk)
//				{
//					std::cout << kkk << std::endl;
//				}
//				
//			}
//			std::cout << "========================" << std::endl;
		}
		return ccc;		
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index)
	{
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		return ccc;		
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Filters>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
//		std::cerr << "pipeline Filter " << std::endl;
		Filters o;
		return o.Filter (ccc);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index)
	{
		Filters o;
		return o.Filter (ccc);
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, AnalyzersC>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_)
	{
		AnalyzersC o;
		auto rr = o.run (ccc, index, eof_);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		AnalyzersC o;
		//std::cerr<<"run with barcode_index: "<<barcode_index<<'\n';
		//auto rr = 
		o.run (ccc, index, eof_, barcode_index);
		return ccc;//NULL;
	}
};

class Pipeline_terminator2
{
public:
	Pipeline_terminator2()
	{}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator2>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		delete ccc;

		if (eof_)
		{
			AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::ClearContent ();
            AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_TYPELIST_GLOBAL_HETEROGENEITY, AnalyzerTypes::Heterogeneity>::ClearContent ();
			BwgWriter <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList, boost::mpl::size<AnalyzerTypeList>::value-1>::run(barcode_index);
		}
		return NULL;
	}
};


struct pipeline2
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;

	pipeline2(void* inptr, size_t index, bool eof)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof)
	{
//		std::cerr << "void*" << transfer_ptr_ << std::endl;
//		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
//		std::cerr << "pipeline2 start... " << std::endl;
	}
	template<class T>
	void operator()(T t)
	{
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_ );	
	}
};

struct pipeline3
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;
	size_t barcode_index_;
	pipeline3(void* inptr, size_t index, bool eof, size_t barcode_index=0)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof), barcode_index_ (barcode_index)
	{
//		std::cerr << "void*" << transfer_ptr_ << std::endl;
//		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
//		std::cerr << "pipeline2 start... " << std::endl;
	}
	template<class T>
	void operator()(T t)
	{
//		std::cerr << "ABB" << std::endl;
		
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_, barcode_index_ );	
		
//		std::cerr << "transfer ptr "<< transfer_ptr_ << std::endl;
	}
};

void report_progress(int p)
{
	std::cout << "Progress: " << p << " %. Mem usage: " ;
	process_mem_usage();
}


ThreadPool PipePool ( 8 );

int main(int argc, char** argv)
{

	std::string genome_fa;
	std::string genome_index_prefix;
	
	std::vector < std::vector <std::string> > fastq_file_vec({{ "/home/andy/andy/work_tailor/fastq/SRR529100.trim.fastq" }});
//	std::vector < std::vector <std::string> > fastq_file_vec({{ "/home/andy/andy/work_tailor/fastq/SRR529100.2000000.trim.fastq" }});
	std::vector < std::vector <uint64_t> > fastq_size_vec(0);
	
	int total_fastq = 18478204;
	int progress_pipeline1 = 70;
	int progress_pipeline2 = 30;
	
	if(argc > 2)
	{
		std::string genome (argv[2]);
		std::string cmd ("php pipeline/download.php ");
		cmd += genome;
		system (cmd.c_str());
		genome_fa = (genome+".fa");
		genome_index_prefix = genome;
	}
	else
	{
		genome_fa = 
		//(genome+"_random.fa");
		//("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
		("/mnt/godzilla/GENOME/db/hg19/hg19.fa");
		//("/mnt/godzilla/GENOME/db/mm9/mm9.fa");
		//("/run/shm/hg19/hg19.fa");
		
		genome_index_prefix = 
		//(genome);
		//("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//
		("/mnt/mammoth/jones/bwt_table/hg19/hg19");
		//("/mnt/godzilla/BOWTIE_INDEX/tailer/mm9");
		//("/run/shm/hg19/hg19");
		
	}
	
	
	PipelinePreparator<> PP (genome_fa, genome_index_prefix);
	std::vector <std::string> barcode_vec({"all_hg19"});
	
	if (boost::is_same <typename std::tuple_element<0, BarcodeType>::type, boost::mpl::string<'null'> >::value)
		;
	else
	{
		barcode_vec.clear();
		TupleToVector<BarcodeType, std::tuple_size <BarcodeType>::value-1>::ToDynamic (barcode_vec);
	}
	PP.GetBarcodeVec (barcode_vec);

	std::cerr<<"barcode vector: size "<<PipelinePreparator<>::gBarcode_vector_.size()<<'\n';
	for (auto& q : PipelinePreparator<>::gBarcode_vector_)
		std::cerr<<q<<'\n';

    PipelinePreparator<>::gDeviceParameter_.bs_basic_url_="https://api.cloud-hoth.illumina.com/";//"https://api.basespace.illumina.com/";
    PipelinePreparator<>::gDeviceParameter_.bs_version_="v1pre3/";
    PipelinePreparator<>::gDeviceParameter_.bs_tailing_str_="multipart=true";
    PipelinePreparator<>::gDeviceParameter_.bs_content_type_ = "Content-Type: application/txt";
    PipelinePreparator<>::gDeviceParameter_.bs_upload_content_type_  = "Content-Type: multipart/form-data";
    PipelinePreparator<>::gDeviceParameter_.bs_app_result_= "appresults/2538015/";//"appresults/7525518/";
    PipelinePreparator<>::gDeviceParameter_.bs_access_token_ = "x-access-token: 9ca77f5225404c448fbe57096640ec19";//"x-access-token: 7b88f5bc19c342f5937259692b0c3eed";

	if(argc > 1)
	{
		json_handler<> jh (argv[1], "");//("http://peat.jhhlab.tw/basespace/query_task_peat/file_list/bfb36a45cfa747d88b9b5d933c7c0259", "");
		jh.print(); 
		auto result = jh.get_info ();
		
		PipelinePreparator<>::gDeviceParameter_.bs_basic_url_=std::get<3>(result);//"https://api.basespace.illumina.com/";
		PipelinePreparator<>::gDeviceParameter_.bs_version_=std::get<0>(result);//"v1pre3/";
		PipelinePreparator<>::gDeviceParameter_.bs_app_result_=std::get<1>(result);//"appresults/7525518/";//2938939/";//2898896/";//1650649/";
		PipelinePreparator<>::gDeviceParameter_.bs_access_token_ = std::get<2>(result);//"x-access-token: 7b88f5bc19c342f5937259692b0c3eed";//ac84971a44bd43189403fe19c6f4f9fd";//fe7e579479fb40aa97a36643ba70915c";//4b2bf2c94ea34721ac01fc177111707f";
	std::cerr<<"json result "<<std::get<0>(result)<<'\t'<<std::get<1>(result)<<'\t'<<std::get<2>(result)<<'\n';
		PipelinePreparator<>::gDeviceParameter_.bs_tailing_str_="multipart=true";
		PipelinePreparator<>::gDeviceParameter_.bs_content_type_ = "Content-Type: application/txt";
		PipelinePreparator<>::gDeviceParameter_.bs_upload_content_type_  = "Content-Type: multipart/form-data";
	
		for (auto& q : jh.url_vec_)
			q.resize(1);
		for (auto& q : jh.size_vec_)
			q.resize(1);
		std::swap(fastq_file_vec, jh.url_vec_);
		std::swap(fastq_size_vec, jh.size_vec_);
	}
		
//	FileReaderWrapper<IoHandlerBaseSpaceDownload> frw (fastq_file_vec, fastq_size_vec);
	FileReaderWrapper<IoHandlerIfstream> frw (fastq_file_vec, fastq_size_vec);

	std::cerr << "INIT Memory usage: "; process_mem_usage();
	namespace mpl = boost::mpl;
	
	/// @brief pipeline session I
	/// @brief pipeline session I
	/// @brief pipeline session I

	typedef mpl::vector	< 
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						Adapter_Trimmer	>,
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*,
						std::map < int, std::vector< Sam<> > >*,
						Tailer_	>,
		mpl::vector < 	std::map <int, std::vector< Sam<> > >*,
						std::map < int, std::map < RawBed<>, uint16_t > >*,
						Pipeline_terminator	>
	> typelist;
	
	std::cerr << "Load index done. Memory usage: "; process_mem_usage();

	size_t ii=0, limit=10;
	std::vector <size_t> job_id;

	while (true)//	(ii!=limit)
	{
		auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
		auto eof_info = frw.run (ptr, 40000);
eof_info=true;		
		if( (ii & 15) == 0)
		{
			int progress = (int)  (((double) ii*40000/total_fastq)*(progress_pipeline1-10)) ;
			report_progress(progress);
		}
		
		BarcodeProcessor BP;
		BP (ptr);

		if (eof_info)//	(ii==(limit-1))
		{
			job_id.push_back (PipePool.JobPost(
				[ptr, eof_info, ii](){
				mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, true ) );
				}
				,job_id
			));
			//std::cerr << "final job_id " << job_id.back() << std::endl;
			break;
		}
		else
		{
			job_id.push_back (PipePool.JobPost(
				[ptr, eof_info, ii](){
			  	mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, false ) );		
				}
			));
		}
		//std::cerr << "Piplein I. Memory usage: "; process_mem_usage();
		++ii;
	}
	PipePool.FlushPool();
	std::cerr << "Pipeline session I Finish. Memory usage: "; process_mem_usage();
	
//	std::cerr << "ptr Sam_to_RawBed " << &(*Sam_to_RawBed::rawbed_map2_)  <<" Sam_to_RawBed::tmp " << Sam_to_RawBed::tmp << std::endl;
	
	for(auto &sample_read_count : Sam_to_RawBed::reads_mapped_count_)
	{
		std::cerr << "==============Mappability=================" << std::endl;
		std::cerr << "Total read: " << std::get<0>(sample_read_count.second) << std::endl;
		std::cerr << "Mapped read: " << std::get<1>(sample_read_count.second) << std::endl;
		std::cerr << "Unique read: " << std::get<2>(sample_read_count.second) << std::endl;
		std::cerr << "Multiple read: " << std::get<3>(sample_read_count.second) << std::endl;
		std::cerr << "-----------------------------------------" << std::endl;
	}
	

	/// @brief pipeline session II
	/// @brief pipeline session II
	/// @brief pipeline session II
	
	std::cerr << "E Memory usage: "; process_mem_usage();
		
	PP.ReleaseIndex();
	
	std::cerr << "Release Tailor indexer Memory usage: "; process_mem_usage();

	PP.Load ();
	
	std::cerr << "Load Genome. Memory usage: "; process_mem_usage();
	
	typedef mpl::vector	< 
		mpl::vector < 	std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Annotations	>,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Filters >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						AnalyzersC >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						void*, 
						Pipeline_terminator2>
	> typelist2;

//	RawBed Archive writting
//	----------------------------------------------------------------------------------
//	std::cerr<<"rawbed archive"<<'\n';
//	std::ofstream ofs ("/home/oman/work/pokemon/rawbed_archive_chrall_ACGT");
//	boost::archive::text_oarchive arc0 (ofs);
//	arc0 & ( *Sam_to_RawBed::rawbed_map2_);
//	ofs.close();
//	std::cerr<<"done archive"<<'\n';
//
//	for (auto& Q :*Sam_to_RawBed::rawbed_map2_)
//	{
//		std::cerr<<Q.first<<'\n';
//		for (auto& q : Q.second)
//			std::cerr<<q.first;
//	}
//	return 0;

	for (auto& content : *Sam_to_RawBed::rawbed_map2_)
	{
		std::cerr<<"raw_bed_map2_ "<<content.first<<'\t';

		PP.GetReadCountPerChr (content.second);
		for (auto& q: PipelinePreparator<>::gReadCountPerChr_)
			std::cerr << q <<'\n';


		std::cerr << "Pipeline II Memory usage: "; process_mem_usage();
		std::cerr << "content " << content.second.size() << std::endl;
		
		int iii=content.first;
		auto itr = content.second.begin();
		//int limit_run = 20;
		std::vector<size_t> flush_list (0);
		int index=0;
		double total_read_count = 0.0;
		double total_read_count_avg = 0.0;
		double total_read_count_uniq = 0.0;
		while (itr != content.second.end())//(true)
		{
			int idx=0;
			int idx_max = 400;
			
			if( (index & 255) == 0)
			{
				int progress = (int) (((double)index*idx_max/content.second.size())*(progress_pipeline2-5));
				report_progress(progress_pipeline1 + progress);
			}
			
			std::vector< AnnotationRawBed<> > *ptr_vector = new std::vector <AnnotationRawBed<> > ();
			
			ptr_vector->reserve(idx_max);
			while (idx<idx_max && itr!=content.second.end())
			{
				total_read_count += (double)(itr->first).reads_count_;
				total_read_count_avg += (double)(itr->first).reads_count_ / (double)(itr->first).multiple_alignment_site_count_;
				if((itr->first).multiple_alignment_site_count_ == 1)
				{
					total_read_count_uniq += (double)(itr->first).reads_count_;
				}
				ptr_vector->push_back (itr->first);
				++itr;
//				++idx;
                idx+=itr->first.reads_count_;
			}

			if(itr==content.second.end())
			{
				flush_list.push_back ( PipePool.JobPost(
					[index, ptr_vector, iii]()
					{
						mpl::for_each<typelist2>( pipeline3 ( (void*)ptr_vector, index, true, iii ) );
					}
					, flush_list
				) );
				break;
			}
			else
			{
				flush_list.push_back ( PipePool.JobPost(
					[index, ptr_vector, iii]()
					{
						mpl::for_each<typelist2>( pipeline3 ( (void*)ptr_vector, index, false, iii ) );
					}
				) );
			}
			++index;
		}
		std::cout << "total_read_count " << total_read_count << " total_read_count_avg " << total_read_count_avg << " total_read_count_uniq " << total_read_count_uniq << std::endl;
		std::cout << "index*400" << index*400 << std::endl;
		PipePool.FlushPool();
	}
	std::cerr << "Pipline session II finish. Memory usage: "; process_mem_usage();
	PP.ReleaseChr ();
	std::cerr << "chromosome deleted. Memory usage: "; process_mem_usage();
	std::cerr << "Analyzers finish" << std::endl;
	return 0;
}
