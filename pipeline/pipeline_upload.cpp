#include <iostream>
#include <cstdlib>
#include "../src/converter/sam2rawbed.hpp"
#include "../src/pipeline/pipeline_preparator.hpp"
#include "srna_pipeline_json_parser.hpp"
#include "progress_logger.hpp"
#include "../src/iohandler/basespace_def.hpp"

void process_mem_usage(std::ostream& yy)
{
	double vm_usage, resident_set;
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage	 = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
			   >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
			   >> utime >> stime >> cutime >> cstime >> priority >> nice
			   >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage	 = vsize / 1024.0;
   resident_set = rss * page_size_kb;
   
//   std::cout << "VM: " << (vm_usage/1024.0/1024.0) << "; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
   yy << "VM: " << (vm_usage/1024.0/1024.0) << "; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
}




int main (int argc, char** argv)
{
    std::cerr << "Memory usage: "; process_mem_usage(std::cerr);
	SRNAPipelineJsonParser Json_content (argv[1]);
	PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ = Json_content.analysis_prefix_name_;
	PipelinePreparator<>::gDeviceParameter_.bs_basic_url_ = Json_content.base_api_url_;//"https://api.cloud-hoth.illumina.com/";
	PipelinePreparator<>::gDeviceParameter_.bs_app_result_ = Json_content.appresult_href_.substr (7);//"appresults/2538015/";
	PipelinePreparator<>::gDeviceParameter_.bs_access_token_ = ("x-access-token: "+Json_content.access_token_);//9ca77f5225404c448fbe57096640ec19";
	PipelinePreparator<>::gDeviceParameter_.bs_version_ = "v1pre3/";
	PipelinePreparator<>::gDeviceParameter_.bs_tailing_str_ = "multipart=true";
	PipelinePreparator<>::gDeviceParameter_.bs_content_type_ = "Content-Type: application/txt";
	PipelinePreparator<>::gDeviceParameter_.bs_upload_content_type_ = "Content-Type: multipart/form-data";

	std::string genome_fa =Json_content.genome_path_;//("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
	std::string genome_index_prefix = Json_content.index_prefix_;

	PipelinePreparator<> PP;

	std::vector <std::string> barcode_vec ({"all"});
	if (Json_content.barcode_type_!=0)
		barcode_vec = Json_content.barcode_sequence_vec_;
	PP.GetBarcodeVec (barcode_vec);

	std::cerr<<"input barcode_type : input barcode_vector's size "<<Json_content.barcode_type_<<" "<<barcode_vec.size()<<std::endl;
	std::cerr<<"parsed barcode vector: size "<<PipelinePreparator<>::gBarcode_vector_.size()<<'\n';
	for (auto& q : PipelinePreparator<>::gBarcode_vector_)
		std::cerr<<q<<'\n';

    std::cerr << "Memory usage: "; process_mem_usage(std::cerr);
	std::vector<  std::shared_ptr <IoHandlerBaseSpace> > handler_vec;
	int barcode_index=0;
	for (auto& content:PipelinePreparator<>::gBarcode_vector_)//*Sam_to_RawBed::rawbed_map2_)
	{
		std::cout<<"Processing tar file of barcode_group: "<<PipelinePreparator<>::gBarcode_vector_[barcode_index]<<std::endl;
		std::string target_folder ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index]);
		std::string tar_name ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index]+"_ClouDNA-viewer.des3");
		std::string key ("5566");   
		std::string cmd ("/bin/tar -zcf - "+target_folder+" --exclude '*.bam' --exclude '*.bwg' --exclude '*.bai' | /usr/bin/openssl des3 -salt -k " + key +" | /bin/dd of=" + tar_name);

		int repeat=0;
		while (repeat<5)
		{
			//std::cout<<"running tar call #"<<repeat<<std::endl;
    		std::cerr << "Memory usage: "; process_mem_usage(std::cerr);
			auto yy = std::system ( cmd.c_str() );
    		std::cerr << "Memory usage: "; process_mem_usage(std::cerr);
			//std::cout<<"system call return value: "<<yy<<std::endl;
			std::cerr<<"system call return value: "<<yy<<std::endl;
			boost::filesystem::path tar_file (tar_name);
			if(boost::filesystem::exists(tar_file) )
				break;
			else
				++repeat;
		}
		if (repeat==5)
		{
			std::cout<<"packaging failed"<<std::endl;
			return -1;
		}
		std::cout<<"packaging completed"<<std::endl;


		std::ifstream fin (tar_name);
		std::string filename (PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index]+"_ClouDNA-viewer.des3");
		std::string output_path ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index]);
		DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
		file_writter_parameter.set_filename ( filename , output_path);
		handler_vec.emplace_back ( std::shared_ptr <IoHandlerBaseSpace>(std::make_shared <IoHandlerBaseSpace> (file_writter_parameter)));
		char* f2 = new char [4096+1];
		std::cerr<<"des3 file uploading "<<std::endl;
		while (true)
		{  
			fin.read (f2, 4096);
			handler_vec.back()->write(f2, 4096);
			if (fin.eof())
				break;
		}
		fin.close();
		delete f2;
		handler_vec.back()->bs_async_close();

		std::cout<<"file uploading of barcode group: "<<PipelinePreparator<>::gBarcode_vector_[barcode_index]<<std::endl;
		++barcode_index;
	}
    std::cerr << "Memory usage: "; process_mem_usage(std::cerr);
	for (auto&ptr: handler_vec)
		ptr->close();
    std::cerr << "Memory usage: "; process_mem_usage(std::cerr);
	std::cout<<"All file uploaded"<<std::endl;
	BS_pool.ResetPool();
}
