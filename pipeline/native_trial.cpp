#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_VECTOR_SIZE 200
#include <unistd.h>
#include <ios>
#include <fstream>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/string.hpp>
#include <boost/type_traits/add_pointer.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/map.hpp>
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>
#include "../src/file_reader.hpp"
//#include "../src/barcode_handler/barcode_handler_impl_static.hpp"
#include "../src/barcode_handler/barcode_handler_impl_dynamic.hpp"
#include "../src/trimmer/single_end_adapter_trimmer.hpp"
#include "../src/aligner/aligner.hpp"
#include "../src/converter/sam2rawbed.hpp"
#include "htslib/htslib/sam.h"
#include "../src/annotator/annotation_set.hpp"
#include "../src/annotator/annotation.hpp"
#include "../src/annotator/filter.hpp"
#include "../src/analyzer/analyzer.hpp"
#include "../src/pipeline/pipeline_preparator.hpp"
#include "../src/iohandler/iohandler.hpp"
#include "../src/pipeline/reader_wrapper.hpp"
#include "../src/format/json_handler.hpp"
#include "native_config.hpp"
#include "run_timer.hpp"
#include <cstdlib>

RunTimer <> timer;


void process_mem_usage(std::ostream& yy)
{
	double vm_usage, resident_set;
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage	 = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
			   >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
			   >> utime >> stime >> cutime >> cstime >> priority >> nice
			   >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage	 = vsize / 1024.0;
   resident_set = rss * page_size_kb;
   
//   std::cout << "VM: " << (vm_usage/1024.0/1024.0) << "; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
   yy << "VM: " << (vm_usage/1024.0/1024.0) << "; RSS: " << (resident_set/1024.0/1024.0) << " GB" << std::endl;
}


typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;
typedef SingleEndAdapterTrimmer < ParallelTypes::M_T, Fastq, TUPLETYPE, QualityScoreType::SANGER > Adapter_Trimmer;
typedef Aligner< Aligner_trait<> > Tailer_;
typedef Sam2RawBed < std::map <int, std::vector< Sam<> > >* > Sam_to_RawBed;

/// @brief vector 順序越前面權重越低，越後面越高
/// @brief tuple 0=>字串, 1=>is_filter;
/// @brief '^', '$', '%', 'R'
typedef boost::mpl::vector
<
  	 boost::mpl::vector< boost::mpl::string<'misc', '_RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
//	,boost::mpl::vector< boost::mpl::string<'Mt_', 'rRNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
//	,boost::mpl::vector< boost::mpl::string<'Mt_', 'tRNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'rRNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'sn', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
//	,boost::mpl::vector< boost::mpl::string<'sc', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
//	,boost::mpl::vector< boost::mpl::string<'srp', 'RNA'>, boost::mpl::int_<1>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'mi', 'RNA'>, boost::mpl::int_<0>, boost::mpl::char_<'='> >
	,boost::mpl::vector< boost::mpl::string<'linc', 'RNA'>, boost::mpl::int_<0>, boost::mpl::char_<'='> >
> Filter_Type_List;

typedef FilterWorker <std::vector< AnnotationRawBed<> >*, Filter_Type_List> Filters;

typedef AnnotationSet < 
		std::vector< AnnotationRawBed<> >,
		2,
		Annotation< FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
            ,AnnoTrait_mirna_3p5p
            ,AnnoIgnoreStrand::NO_IGNORE
            ,AnnoType::INTERSET
        >,
		Annotation<	FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
			,AnnoTrait_MGI
			,AnnoIgnoreStrand::NO_IGNORE
			,AnnoType::INTERSET
		>
> Annotations;

#include "typelist.hpp"

typedef AnalyzerC <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList_A> AnalyzersC;
typedef AnalyzerC <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList_B> AnalyzersD;
typedef AnalyzerC <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList_C> AnalyzersE;


std::mutex m1;
template<class InputType, class OutputType, class ObjectType>
struct run 
{
	OutputType operator()(InputType i,bool eof_)
	{
		std::cerr << "pipeline error " << std::endl;
		return 0;
	}
};

/// @brief Pipeline I
template<class InputType, class OutputType>
struct run < InputType, OutputType, Adapter_Trimmer>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Adapter_Trimmer " << std::endl;
		ParameterTraitSeat <QualityScoreType::SANGER> Qoo (g_adapter_seq);//("CGTATGCCGTCTTC");//("AGATCGGAAGAGCGG");
		std::vector<int> trim_pos;
//		int nthread = 1;	//18.84s	88%	
//		int nthread = 2;	//10.69s	193%
//		int nthread = 4;	//10.91s	193%
		int nthread = 8;	//10.59s	183%
		Adapter_Trimmer o (Qoo);

		for (auto& QQ : *ccc)
			o.Trim (ccc, nthread, trim_pos, QQ.first);

		//std::cerr << "done pipeline Adapter_Trimmer " << std::endl;
		return ccc;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Tailer_>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
		//std::cerr << "pipeline Tailer " << std::endl;
		Tailer_ o;
		//OutputType Q = new std::map < int, std::tuple<std::string, std::vector< Sam<> > >* > ();	
		OutputType Q =  new std::map <int, std::vector< Sam<> > > ();
		for (auto& QQ : *ccc)
		{
			auto gg = o.search(ccc, 8, 15, 1000, 72, QQ.first);
			//auto gg = o.search(ccc, 3, 18, 100, 200, QQ.first);
			Q->insert ({QQ.first, std::vector< Sam<> >()});	//avoid copy in the future

			std::swap ((*Q)[QQ.first], (*gg));
			delete gg;
			
			/// @brief count mapped reads for mappability 
			{
				std::lock_guard<std::mutex> lock (Sam_to_RawBed::map_insert_mutex_);
				auto &g_reads_mapped_count = Sam_to_RawBed::reads_mapped_count_[QQ.first];
				std::get<0>(g_reads_mapped_count) += std::get<0>(o.reads_map_count_);
				std::get<1>(g_reads_mapped_count) += std::get<1>(o.reads_map_count_);
				std::get<2>(g_reads_mapped_count) += std::get<2>(o.reads_map_count_);
				std::get<3>(g_reads_mapped_count) += std::get<3>(o.reads_map_count_);
				//Sam_to_RawBed::reads_mapped_count_[QQ.first] += o.reads_map_count_;
			}
			
		}
		delete ccc;
		//std::cerr << "done pipeline Tailer " << std::endl;
		return Q;
	}
};

class Pipeline_terminator
{
public:
	Pipeline_terminator()
	{}
};

std::mutex print_mutex;

template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator>
{
	OutputType operator()(InputType ccc, int map_index, bool eof_)
	{
//std::lock_guard <std::mutex> lg (print_mutex);
//{
//	std::cerr << "Pipeline I Memory usage: "; process_mem_usage(std::cerr);
//	std::cout << "Pipeline I Memory usage: "; process_mem_usage(std::cout);
//}
		//std::cerr << "pipeline Pipeline_terminator: I " << std::endl;
		Sam_to_RawBed o;
		auto raw_map = o.Convert2 (ccc, eof_);//o.Convert ( &(std::get<1>(*ccc)) );
		//std::cerr << "pipeline Pipeline_terminator: II " << std::endl;
		AnalyzerImpl< std::map <int, std::vector< Sam<> > >, TypeListUnsortedBam, AnalyzerTypes::ToBam> Analysis((*ccc));//TypeListUnsortedBam defined in typelist.hpp
		//std::cerr << "pipeline Pipeline_terminator: III " << std::endl;
		Analysis(map_index, eof_); 
		//std::cerr << "pipeline Pipeline_terminator: IV " << std::endl;
		delete ccc;
		OutputType ll;
		return ll;
		//std::cerr << "pipeline Pipeline_terminator: V " << std::endl;
	}
};

/// @brief Pipeline II
template<class InputType, class OutputType>
struct run < InputType, OutputType, Annotations>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
		std::vector<std::string> AnnoDBPath
		{
			 "/home/andy/db/MGI_mm9_no_cluster.bed"
//			,"/home/andy/db/MGI_mm9_no_cluster.bed"
		};
//		std::cout << "vector size of raw bed : " << ccc->size() << std::endl;
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		for(auto &annorawbed : *ccc)
		{
//			std::cout << annorawbed << std::endl;
			if(annorawbed.annotation_info_.size() == 0)
				continue;				
//			std::cout << "========================" << std::endl;
//			
//			for(auto &kk : annorawbed.annotation_info_)
//			{
//				for(auto &kkk : kk)
//				{
//					std::cout << kkk << std::endl;
//				}
//				
//			}
//			std::cout << "========================" << std::endl;
		}
		return ccc;		
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index)
	{
		Annotations annos;	
		annos.AnnotateAll(*ccc);
		return ccc;		
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, Filters>
{
	OutputType operator()(InputType ccc, bool eof_)
	{
//		std::cerr << "pipeline Filter " << std::endl;
		Filters o;
		return o.Filter (ccc);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index)
	{
		Filters o;
		return o.Filter (ccc);
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, AnalyzersC>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_)
	{
		AnalyzersC o;
		auto rr = o.run (ccc, index, eof_);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		AnalyzersC o;
		//std::cerr<<"run with barcode_index: "<<barcode_index<<'\n';
		o.run (ccc, index, eof_, barcode_index);
		return ccc;//NULL;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, AnalyzersD>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_)
	{
		AnalyzersD o;
		auto rr = o.run (ccc, index, eof_);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		AnalyzersD o;
		//std::cerr<<"run with barcode_index: "<<barcode_index<<'\n';
		o.run (ccc, index, eof_, barcode_index);
		return ccc;//NULL;
	}
};

template<class InputType, class OutputType>
struct run < InputType, OutputType, AnalyzersE>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_)
	{
		AnalyzersE o;
		auto rr = o.run (ccc, index, eof_);
	}

	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		AnalyzersE o;
		//std::cerr<<"run with barcode_index: "<<barcode_index<<'\n';
		o.run (ccc, index, eof_, barcode_index);
		return ccc;//NULL;
	}
};


class Pipeline_terminator2
{
public:
	Pipeline_terminator2()
	{}
};


template<class InputType, class OutputType>
struct run < InputType, OutputType, Pipeline_terminator2>
{
	OutputType operator()(InputType ccc, size_t index, bool eof_, size_t barcode_index )
	{
		delete ccc;

//std::lock_guard <std::mutex> lg (print_mutex);
//{
//	std::cerr << "Pipeline II Memory usage: "; process_mem_usage(std::cerr);
//	std::cout << "Pipeline II Memory usage: "; process_mem_usage(std::cout);
//}
		if (eof_)
		{
			AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::ClearContent ();
            AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_TYPELIST_GLOBAL_HETEROGENEITY, AnalyzerTypes::Heterogeneity>::ClearContent ();
			AnalyzerImpl<std::vector< AnnotationRawBed<> >*, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::ClearContent ();
			BwgWriter <std::vector< AnnotationRawBed<> >*, AnalyzerTypeList_C, boost::mpl::size<AnalyzerTypeList_C>::value-1>::run(barcode_index);

			AnalyzerImplInitC<std::vector< AnnotationRawBed<> >*>::analyzer_count_type_.clear();
		}
		return NULL;
	}
};


struct pipeline2
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;

	pipeline2(void* inptr, size_t index, bool eof)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof)
	{
//		std::cerr << "void*" << transfer_ptr_ << std::endl;
//		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
//		std::cerr << "pipeline2 start... " << std::endl;
	}
	template<class T>
	void operator()(T t)
	{
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_ );	
	}
};

struct pipeline3
{
	void* transfer_ptr_;
	size_t index_;
	bool eof_;
	size_t barcode_index_;
	pipeline3(void* inptr, size_t index, bool eof, size_t barcode_index=0)
		: transfer_ptr_ (inptr), index_(index), eof_ (eof), barcode_index_ (barcode_index)
	{
//		std::cerr << "void*" << transfer_ptr_ << std::endl;
//		std::cerr << "index & eof" << index_ <<'\t'<<eof_ << std::endl;
//		std::cerr << "pipeline2 start... " << std::endl;
	}
	template<class T>
	void operator()(T t)
	{
//		std::cerr << "ABB" << std::endl;
		typedef typename boost::mpl::at<T, boost::mpl::int_<0> >::type InputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<1> >::type OutputType;
		typedef typename boost::mpl::at<T, boost::mpl::int_<2> >::type ObjectType;
		
		run<InputType, OutputType, ObjectType> running;

		transfer_ptr_ = (void*) 
		running ( (InputType)transfer_ptr_, index_, eof_, barcode_index_ );	
//		std::cerr << "transfer ptr "<< transfer_ptr_ << std::endl;
	}
};


ThreadPool PipePool ( 8 );
#include "srna_pipeline_json_parser.hpp"
#include "progress_logger.hpp"
#include <stdlib.h> 

template <typename T>
void free (T& aa)
{
	T bb;
	bb.swap (aa);
};

int main(int argc, char** argv)
{
	GlobalPool.ChangePoolSize(26);

    timer.start_timer ("Initialize");

	SRNAPipelineJsonParser Json_content (argv[1]);
	g_adapter_seq = Json_content.adapter_sequence_;
	genome_fa =Json_content.genome_path_;//("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
	genome_index_prefix = Json_content.index_prefix_; //("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//

	PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ = Json_content.analysis_prefix_name_;
	PipelinePreparator<>::gDeviceParameter_.bs_basic_url_ = Json_content.base_api_url_;//"https://api.cloud-hoth.illumina.com/";//"https://api.basespace.illumina.com/";
	PipelinePreparator<>::gDeviceParameter_.bs_app_result_ = Json_content.appresult_href_.substr (7);//"appresults/2538015/";//"appresults/7525518/";
	PipelinePreparator<>::gDeviceParameter_.bs_access_token_ = ("x-access-token: "+Json_content.access_token_);//9ca77f5225404c448fbe57096640ec19";//"x-access-token: 7b88f5bc19c342f5937259692b0c3eed";
	PipelinePreparator<>::gDeviceParameter_.bs_version_ = "v1pre3/";
	PipelinePreparator<>::gDeviceParameter_.bs_tailing_str_ = "multipart=true";
	PipelinePreparator<>::gDeviceParameter_.bs_content_type_ = "Content-Type: application/txt";
	PipelinePreparator<>::gDeviceParameter_.bs_upload_content_type_ = "Content-Type: multipart/form-data";

	PipelinePreparator<> PP (genome_fa, genome_index_prefix);//, "output/");

	std::vector <std::string> barcode_vec ({"all"});
	if (Json_content.barcode_type_!=0)
		barcode_vec = Json_content.barcode_sequence_vec_;
	PP.GetBarcodeVec (barcode_vec);

	//std::cerr<<"input barcode_type : input barcode_vector's size "<<Json_content.barcode_type_<<" "<<barcode_vec.size()<<std::endl;
	//std::cerr<<"parsed barcode vector: size "<<PipelinePreparator<>::gBarcode_vector_.size()<<'\n';
	for (auto& q : PipelinePreparator<>::gBarcode_vector_)
		std::cerr<<q<<'\n';

	for (auto index=0; index!=barcode_vec.size(); ++index)
	{
		(*Sam_to_RawBed::rawbed_map2_)[index].reserve (10*1024*1024);
		(*Sam_to_RawBed::rawbed_map_tmp_)[index].reserve (2*1024*1024); 
	}

	fastq_file_vec.clear(), fastq_size_vec.clear();
	ProgressLogger plg;

	if (Json_content.app_session_name_.substr (0, 4) == "http")
	{
		typedef boost::mpl::map
		<   
		    boost::mpl::pair< IoHandlerGlobalParameter::FilteringStreamType, boost::iostreams::filtering_streambuf<boost::iostreams::input> >
		    , boost::mpl::pair< IoHandlerGlobalParameter::DeviceParameter, DeviceParameter >
		    , boost::mpl::pair< IoHandlerGlobalParameter::MutipleNumber, boost::mpl::int_<2> >
		    , boost::mpl::pair< IoHandlerGlobalParameter::DeviceBufferSize, boost::mpl::int_<8> >
		    , boost::mpl::pair< IoHandlerGlobalParameter::DevicePushbackSize, boost::mpl::int_<16> >
		    , boost::mpl::pair< IoHandlerGlobalParameter::BaseStreamType, std::istream >
		> IO_HANDLER_GLOBAL_SETTING;
		typedef SharedMemory<IO_HANDLER_GLOBAL_SETTING> SharedMemoryType;
		typedef boost::mpl::vector
		<   
		    boost::mpl::map
		    <   
		        boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::BasespaceDevice_download> >
		    >
		> IO_HANDLER_LIST;
		
		DeviceParameter dp;
		dp.bs_download_url_ = Json_content.app_session_name_;
		dp.bs_download_size_ = 0;     
		iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp);

		uint64_t total_read_count = 0;
		while(!bb.eof())
		{   
			std::string item;
			std::getline (bb, item);
	
			std::vector < std::string > split_temp;
			boost::split (split_temp, item, boost::is_any_of ("\t") );
			if (split_temp[0].size()==0)
				break;
	
			std::cerr<<"txt: "<<split_temp[0].size()<<'\t'<<split_temp[1].size()<<'\n';	
			std::cerr<<"txt: "<<split_temp[0]<<'\t'<<split_temp[1]<<'\n';	
			fastq_file_vec.push_back (std::vector<std::string>({split_temp[0]}));
			fastq_size_vec.push_back (std::vector<std::uint64_t>({std::stoull(split_temp[1])}));
			total_read_count += std::stoull(split_temp[1]);
		}
		plg.SetTotalCount (total_read_count);
	}
	else
	{
		for (auto&qq: Json_content.url_vec_)
			fastq_file_vec.push_back (std::vector<std::string>({qq}));
		for (auto&qq: Json_content.size_vec_)
			fastq_size_vec.push_back (std::vector<std::uint64_t>({qq}));
		plg.SetTotalCount (Json_content.total_read_count_);
	}

	FileReaderWrapper<IoHandlerBaseSpaceDownload> frw (fastq_file_vec, fastq_size_vec);


	std::cout << "INIT Memory usage: "; process_mem_usage(std::cout);

	namespace mpl = boost::mpl;
	
	/// @brief pipeline session I
	/// @brief pipeline session I
	/// @brief pipeline session I

	typedef mpl::vector	< 
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						std::map < int, std::vector< Fastq<TUPLETYPE> > >*, 
						Adapter_Trimmer	>,
		mpl::vector < 	std::map < int, std::vector< Fastq<TUPLETYPE> > >*,
						std::map < int, std::vector< Sam<> > >*,
						Tailer_	>,
		mpl::vector < 	std::map <int, std::vector< Sam<> > >*,
						void*,//std::map < int, std::map < RawBed<>, uint16_t > >*,
						Pipeline_terminator	>
	> typelist;
	
	std::cout << "Aligner index loaded. Memory usage: "; process_mem_usage(std::cout);

	size_t ii=0, limit=10;
	std::vector <size_t> job_id;

    timer.stop_timer ("Initialize");

	int sum_read_count=0, sum_read_count2=0;
	timer.start_timer ("pipeline_I");


	while (true)//	(ii!=limit)
	{
		int read_count_per_round = 80000;
		auto ptr = new std::map <int, std::vector < Fastq<TUPLETYPE> > >;
		auto eof_info = frw.run (ptr, read_count_per_round);

		plg.GetProgress (read_count_per_round);//((*ptr)[0].size());
		sum_read_count += (*ptr)[0].size();
		sum_read_count2 += read_count_per_round;

		BarcodeHandlerDynamic BP (Json_content.barcode_type_, Json_content.barcode_sequence_vec_);
		BP (ptr);

		if (eof_info)//	(ii==(limit-1))
		{
			job_id.push_back (PipePool.JobPost(
				[ptr, eof_info, ii](){
				mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, true ) );
				}
				,job_id
			));
			//std::cerr << "final job_id " << job_id.back() << std::endl;
			break;
		}
		else
		{
			job_id.push_back (PipePool.JobPost(
				[ptr, eof_info, ii](){
			  	mpl::for_each<typelist>( pipeline2( (void*)ptr, ii, false ) );		
				}
			));
		}
		//std::cerr << "Piplein I. Memory usage: "; process_mem_usage();
		++ii;
	}

	std::cerr<<"Phase I done barcode count "<<Sam_to_RawBed::rawbed_map2_->size()<<std::endl;
	std::cerr << "Pipeline session I done post. Memory usage: "; process_mem_usage(std::cerr);
	std::cout << "Session I terminating. Memory usage: "; process_mem_usage(std::cout);
	PipePool.FlushPool();
	BS_pool.FlushPool();
	std::cerr << "Pipeline session I Finish. Memory usage: "; process_mem_usage(std::cerr);
	std::cout << "Session I terminated. Memory usage: "; process_mem_usage(std::cout);

	PipePool.ChangePoolSize(26);

	std::cerr<<"actual fastq read count v.s. listed fastq read_count: "<<sum_read_count<<'\t'<<sum_read_count2<<'\n';
	std::cout<<"Effective fastq read count "<<sum_read_count<<std::endl;
	
	for(auto &sample_read_count : Sam_to_RawBed::reads_mapped_count_)
	{
		std::cout << "==============Mappability Summary=================" << std::endl;
		std::cout << "Total fastq read: " << std::get<0>(sample_read_count.second) << std::endl;
		std::cout << "Mapped fastq read: " << std::get<1>(sample_read_count.second) << std::endl;
		std::cout << "Unique fastq read: " << std::get<2>(sample_read_count.second) << std::endl;
		std::cout << "Multiple fastq read: " << std::get<3>(sample_read_count.second) << std::endl;
		std::cout << "--------------------------------------------------" << std::endl;
	}

	timer.stop_timer ("pipeline_I");


	/// @brief pipeline session II
	/// @brief pipeline session II
	/// @brief pipeline session II
	timer.start_timer ("pipeline_II");
	
	PP.ReleaseIndex();
	std::cout << "Aligner index released. Memory usage: "; process_mem_usage(std::cout);
	PP.Load ();
	std::cout << "Genome sequence loaded. Memory usage: "; process_mem_usage(std::cout);
	
	typedef mpl::vector	< 
		mpl::vector < 	std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Annotations	>,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						Filters >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						AnalyzersC >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						AnalyzersD >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						std::vector< AnnotationRawBed<> >*, 
						AnalyzersE >,
		mpl::vector <   std::vector< AnnotationRawBed<> >*, 
						void*, 
						Pipeline_terminator2>
	> typelist2;

//	RawBed Archive writting
//	----------------------------------------------------------------------------------
//	std::cerr<<"rawbed archive"<<'\n';
//	std::ofstream ofs ("/home/oman/work/pokemon/rawbed_archive_chrall_ACGT");
//	boost::archive::text_oarchive arc0 (ofs);
//	arc0 & ( *Sam_to_RawBed::rawbed_map2_);
//	ofs.close();
//	std::cerr<<"done archive"<<'\n';
//
//	for (auto& Q :*Sam_to_RawBed::rawbed_map2_)
//	{
//		std::cerr<<Q.first<<'\n';
//		for (auto& q : Q.second)
//			std::cerr<<q.first;
//	}
//	return 0;

	plg.AdvanceStage();
	uint32_t total_read_count=0;
	for (auto& content : *Sam_to_RawBed::rawbed_map2_)
		for (auto& element: content.second)
			total_read_count += element.reads_count_;
	plg.SetTotalCount (total_read_count);

	int barcode_index=0;
	for (auto& content : *Sam_to_RawBed::rawbed_map2_)
	{
		std::cout<<"Processing alignment reads of barcode_group: "<<PipelinePreparator<>::gBarcode_vector_[content.first]<<std::endl;
		std::cout<<"Raw alignment Reads count: "<<content.second.size()<<std::endl;
		std::cout<<"Memory usage: "; process_mem_usage(std::cout);

		int iii=content.first;
		auto itr = content.second.begin();
		//int limit_run = 20;
		std::vector<size_t> flush_list (0);
		int index=0;
		double total_read_count = 0.0;
		double total_read_count_avg = 0.0;
		double total_read_count_uniq = 0.0;
		while (itr != content.second.end())//(true)
		{
			int idx=0;
			int idx_max = 10000;//400;

			std::vector< AnnotationRawBed<> > *ptr_vector = new std::vector <AnnotationRawBed<> > ();

			ptr_vector->reserve(idx_max*2);

			while (idx<idx_max && itr!=content.second.end())
			{
				total_read_count += (double)(itr->reads_count_);
				total_read_count_avg += (double)(itr->reads_count_) / (double)(itr->multiple_alignment_site_count_);
				if(itr->multiple_alignment_site_count_ == 1)
				{
					total_read_count_uniq += (double)(itr->reads_count_);
				}
				ptr_vector->push_back (*itr);
				++itr;
				idx+=itr->reads_count_;
			}

			if(itr==content.second.end())
			{
				flush_list.push_back ( PipePool.JobPost(
							[index, ptr_vector, iii]()
							{
							mpl::for_each<typelist2>( pipeline3 ( (void*)ptr_vector, index, true, iii ) );
							}
							, flush_list
							) );
				break;
			}
			else
			{
				flush_list.push_back ( PipePool.JobPost(
							[index, ptr_vector, iii]()
							{
							mpl::for_each<typelist2>( pipeline3 ( (void*)ptr_vector, index, false, iii ) );
							}
							) );
			}
			++index;
			plg.GetProgress (ptr_vector->size());
		}

		std::cerr << "Pipline session II - barcode_index: "<<barcode_index<<" done job post Memory usage: "; process_mem_usage(std::cerr);
		std::cout << "total_read_count " <<std::fixed<< total_read_count << " total_read_count_avg " << total_read_count_avg << " total_read_count_uniq " << total_read_count_uniq << std::endl;
		std::cerr << "total run: " << index << std::endl;
		PipePool.ResetPool();
		++barcode_index;
		free (content.second);
		std::cout<<"Alignment reads of barcode_group: "<<PipelinePreparator<>::gBarcode_vector_[content.first]<<" prossess done. Memory usage: "; process_mem_usage(std::cout);
	}

	plg.AdvanceStage();
	plg.GetProgress (5566);

	timer.stop_timer ("pipeline_II");
	std::cerr << "Pipline session II finish. Memory usage: "; process_mem_usage(std::cerr);
	PP.ReleaseChr ();
	std::cerr << "chromosome deleted. Memory usage: "; process_mem_usage(std::cerr);
	std::cout << "Misson accomplished" << std::endl;
	timer.PrintAllTime();
	return 0;
}
