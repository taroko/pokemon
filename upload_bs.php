<?php
	
	
	$access_token = "9021f24ac063413cb93ac030fb76a46a";
	$filename = "/home/andy/andy/work_tailor/fastq/SRR529100.fastq.gz";
	
	$api_url = "https://api.cloud-hoth.illumina.com";
	
	$app_appsessionuri = "v1pre3/appsessions/3271304";
	$ref_sample_href = "v1pre3/samples/295295";
	
	$project_create = NULL;
	$appresult_create = NULL;
	
	$basename = basename($filename);
	
	
	$date = new DateTime();
	create_project("SmallRNAPipeline-proj-BC_3");
	create_appresult($date->format("Y-m-d H:i:s"), "upload_test", $ref_sample_href);

	$appresult = $appresult_create["Href"];
	//$appresult = "v1pre3/appresults/2469725";
	
	echo "==============================\n";
	echo "Appresult: $appresult";
	echo "==============================\n";
	
	// create file
	$result = "";
	while ($result == "")
	{
		$cmd = "curl -H \"x-access-token: $access_token\" -H \"Content-Type: application/octet-stream\" -X POST $api_url/$appresult/files?name=$basename\&multipart=true";
		echo $cmd."\n";
		$result = shell_exec($cmd);
	}
	echo $result;
	$result = json_decode($result, true);
	$file_url = $result["Response"]["Href"];
	
	$part_size = 24*1024*1024;
	// post file
	$file_size = filesize($filename);
	$part_num = ceil($file_size/($part_size));
	
	
	for($i=1;$i<=$part_num;$i++)
	{
		echo "\n===== parts $i =====\n";
		$seekgs = $part_size * ($i-1);
		$seekge = $part_size * $i;
		
		$get_size = $part_size;
		
		$fp = fopen($filename, "r");
		fseek($fp, $seekgs);
		$content = fread($fp, $get_size);
		fclose($fp);
		file_put_contents("/tmp/bs_tmp", $content);
		$result = "";
		while ($result == "")
		{
			$cmd = "curl -L -v -H \"x-access-token: $access_token\" -T /tmp/bs_tmp -X PUT $api_url/$file_url/parts/$i";
			echo $cmd."\n";
			$result = shell_exec($cmd);
		}
		echo $result;
	}

	// finish
	$result = "";
	while ($result == "")
	{
		$cmd = "curl -v -H \"x-access-token: $access_token\" -X POST $api_url/$file_url?uploadstatus=complete";
		echo $cmd."\n";
		$result = shell_exec($cmd);
	}
	echo $result;

	function create_project($name)
	{
		global $api_url, $access_token, $project_create;
		$post_data = Array(
			"name"					=>	$name
		);
		$options = Array(
			CURLOPT_URL => "$api_url/v1pre3/projects",
			CURLOPT_HTTPHEADER => Array("x-access-token: $access_token"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $post_data
		);
		
		$response = query_curl($options);
		$results = json_decode($response, true);
		
		if( !isset($results['Response']) )
		{
			die("Error! No result Response, app project create failure.");
		}
		//var_dump($results['Response']);
		$project_create = $results['Response'];
	}
	function create_appresult($name, $description, $ref_sample_href)
	{
		global $api_url, $access_token, $project_create, $app_appsessionuri, $appresult_create;
		
		if($project_create == NULL)
		{
			die("Error! No project created.");
		}
		if($app_appsessionuri == NULL)
		{
			die("Error! No app_appsessionuri.");
		}
		
		$post_data = Array(
			"Name"					=>	$name,
			"Description"			=>	$description,
			"HrefAppSession"		=>	$app_appsessionuri,
			"References"			=>	Array( Array("Rel" => "using", "HrefContent" => $ref_sample_href) )
		);
		$options = Array(
			CURLOPT_URL => "{$api_url}/{$project_create['HrefAppResults']}",
			CURLOPT_HTTPHEADER => Array("x-access-token: {$access_token}", "Content-Type: application/json"),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_VERBOSE => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => json_encode($post_data)
		);
		
		// require access_tocken
		$response = query_curl($options);
		$results = json_decode($response,true);
		//echo "$name $description <br>";
		//var_dump($results);
		
		if( !isset($results['Response']) )
		{
			die("Error! No result Response, app result create failure.");	
		}
		
		$appresult_create = $results['Response'];
		//var_dump($appresult_create);
	}
	
	function query_curl($opts)
	{
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		$response = curl_exec($ch); 
		curl_close($ch);
		return $response;
	}

?>