#ifndef UTILITY_HPP_
#define UTILITY_HPP_

/*
class cc1
{
public:
	cc1(){std::cerr << "cc1" << std::endl;};
	void me(){std::cerr << "cc1" << std::endl;};
};
class cc2
{
public:
	cc2(){std::cerr << "cc2" << std::endl;};
	void me(){std::cerr << "cc2" << std::endl;};
};
class cc3
{
public:
	cc3(){std::cerr << "cc3" << std::endl;};
	void me(){std::cerr << "cc3" << std::endl;};
};
*/
template<int N, typename T, typename... ARGS>
class ExtractARGS
{
public:
	typedef typename ExtractARGS<N-1, ARGS...>::Type Type;
};
template<typename T, typename... ARGS>
class ExtractARGS<0, T, ARGS...>
{
public:
	typedef T Type;
};
template<typename T>
class ExtractARGS<0, T>
{
public:
	typedef T Type;
};


#endif
