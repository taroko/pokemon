#ifndef SBWT_ASSEMBLY_HPP_
#define SBWT_ASSEMBLY_HPP_

#include <memory>
#include <vector>
#include "boost/serialization/vector.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/filesystem.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/ihandler/ihandler.hpp"
#include "../iohandler/basespace_def.hpp"


template <typename INPUTHANDLE=IoHandlerIfstream>  
class SbwtAssemblyPrehandler
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE; 
	typedef IHandler <ParallelTypes::NORMAL, Fastq, TUPLETYPE, INPUTHANDLE> Reader;//IoHandlerIfstream> Reader;

	std::vector< std::vector<std::string> > url_vec_;
	std::vector< std::vector<uint64_t> > size_vec_;
	std::map<int, std::vector< Fastq<TUPLETYPE> > >* dummy_ptr_;
	int current_source_;
	int read_count_;
	int archive_run_;
	std::string reads_archive_name_prefix_;
	Reader* reader_ptr_;

	std::vector<std::string> reads_archive_name_;
	std::vector<std::shared_ptr< std::ofstream> > reads_ofstream_list_;
	std::vector<std::shared_ptr< boost::archive::binary_oarchive> > reads_oarchive_list_;

public:
	SbwtAssemblyPrehandler (std::vector< std::vector <std::string> >& url_vec, const std::vector < std::vector <uint64_t> >& size_vec = {}, const std::string& reads_archive_name="reads_archive_part_" )
		: url_vec_ (url_vec)
		, size_vec_ (size_vec)
		, dummy_ptr_ (new  std::map <int, std::vector < Fastq<TUPLETYPE> > >)
		, current_source_ (0)
		, read_count_(0)	
		, archive_run_(0)	
	{
		SetReadsArchive (reads_archive_name);
		if (url_vec_.size()>0)
		{
			if (url_vec_.size()==size_vec_.size())
				reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_, size_vec_[current_source_]);
			else
				reader_ptr_ = new Reader (url_vec_.front(), dummy_ptr_);
			++current_source_;
		}
		else
		{
			std::cerr<<"url_vec wrong size "<<'\n';
			exit (1);
		}
	}	

	void SetReadsArchive(const std::string& reads_archive_name)
	{
		std::string path ("archive_folder/");
		boost::filesystem::path dir_output(path);
		boost::filesystem::create_directory(dir_output);
		reads_archive_name_prefix_ = path+reads_archive_name;
	}


	~SbwtAssemblyPrehandler ()
	{
		delete reader_ptr_;
		delete dummy_ptr_;
	}

	bool run (//std::map<int, std::vector< Fastq<TUPLETYPE> > >* ptr, 
			int run_count=1000)
	{
		int archive_index=0;
		while (true)//buffered_read_count<run_count)
		{
			bool eof_info = reader_ptr_->Read_NSkip (dummy_ptr_, run_count);

			reads_archive_name_.push_back (reads_archive_name_prefix_+std::to_string (archive_index));
			reads_ofstream_list_.push_back (std::make_shared<std::ofstream>(reads_archive_name_[archive_index], std::ios::binary));
			reads_oarchive_list_.push_back (std::make_shared<boost::archive::binary_oarchive> (*reads_ofstream_list_[archive_index]));
			*reads_oarchive_list_[archive_index] & (*dummy_ptr_)[0];

			read_count_+=(*dummy_ptr_)[0].size();
			++archive_index;

			if (eof_info && current_source_ < url_vec_.size())
			{
					delete reader_ptr_;
					if (url_vec_.size()==size_vec_.size())
						reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_, size_vec_[current_source_]);
					else
						reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_);
					++current_source_;
			}
		}
	}


};

#endif
