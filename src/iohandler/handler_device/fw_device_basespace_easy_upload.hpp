#ifndef FW_DEVICE_BASESPACE_EASY_UPLOAD_HPP_
#define FW_DEVICE_BASESPACE_EASY_UPLOAD_HPP_

#include <algorithm>// copy, min
#include <cassert>
#include <iosfwd>
#include <ios>// ios_base::beg
#include <mutex>
#include <future>
#include <string>// streamsize
#include <sstream>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include "../../thread_pool_update.hpp"
#include "fw_utility.hpp"
#include "../curl_impl/curl_utility.hpp"
#include "../iohandler.hpp"
#include "device_basespace_setting.hpp"


template<class SharedMemoryType, class ParaType>
class FDevice<SharedMemoryType, ParaType, FileDeviceType::BasespaceDeviceEasyUpload>
{
private:
	typedef boost::iostreams::stream_offset size_type;
	size_type pos_;
	SharedMemoryType &shared_memory_;
	uint32_t container_size_;
	int part_index_;

public:
	std::shared_ptr <FWBuffer> bs_buffer_;
	std::shared_ptr <std::string> upload_url_;
	size_t write_job_id_;
	bool is_closed_;
	bool written_;

	//typedef boost::iostreams::seekable_device_tag category;
	typedef boost::iostreams::sink_tag  category;
	typedef char char_type;
	typedef typename at<ParaType, typename BaseSpaceLocalParameter::CurlSendMaxLength, boost::mpl::int_<24*1024*1024> >::type CurlSendMaxLengthType;
	typedef typename at<ParaType, typename BaseSpaceLocalParameter::CurlSendLastLength, boost::mpl::int_<5*1024*1024> >::type CurlSendLastLengthType;

	FDevice(SharedMemoryType &sm)
		: pos_(0)
		, shared_memory_(sm)
		, container_size_(0)
		, part_index_(0)
		, bs_buffer_ (std::make_shared <FWBuffer> ())
		, upload_url_ (std::make_shared <std::string> ())
		, is_closed_(false)
		, written_ (false)
	{
		open();
	}

	void open()
	{}

	std::streamsize write(const char_type* upload_content, std::streamsize length)
	{
		bs_buffer_->write (upload_content, length);
		return length;
	}

	std::streamsize read(const char_type* download_content, std::streamsize length)
	{}

	boost::iostreams::stream_offset seek(boost::iostreams::stream_offset off, std::ios_base::seekdir way)
	{
		boost::iostreams::stream_offset next;
		if (way == std::ios_base::beg) 
		{
			next = off;
		} 
		else if (way == std::ios_base::cur) 
		{
			next = pos_ + off;
		} 
		else if (way == std::ios_base::end) 
		{
			next = container_size_ + off - 1;
		} 
		else 
		{
			throw std::ios_base::failure("bad seek direction");
		}
		// Check for errors
		if (next < 0 || next >= container_size_)
			throw std::ios_base::failure("bad seek offset");

		pos_ = next;
		return pos_;
	}
	
	~FDevice()
	{
//		std::cout << "BasespaceDevice close eof " << shared_memory_.flag_eof_ << std::endl;
		if(shared_memory_.flag_eof_)
		{
			if (is_closed_)
				;
			else
				close();
			BS_pool.FlushOne (write_job_id_);
		}
	}

	void close()
	{
		is_closed_=true;

		auto upload_url = shared_memory_.parameter.bs_basic_url_ + shared_memory_.parameter.bs_version_ + 
						  shared_memory_.parameter.bs_app_result_ + shared_memory_.parameter.bs_file_path_ + shared_memory_.parameter.bs_dir_path_;
		auto active_size = bs_buffer_->active_size();
		char* ka = new char [active_size+1];
		bs_buffer_->read (ka, active_size);
		if(bs_buffer_->eof())
		{
			bs_buffer_->clear();
			bs_buffer_->seekp(0);
			bs_buffer_->seekg(0);
		}
		write_job_id_ = BS_pool.JobPost (
		[ka, this, upload_url, active_size] 
		{
	    	for (auto ind=0; ind!=5; ++ind)
	    	{
	    	    CurlImpl <> curl_device (shared_memory_.parameter.bs_curl_config_set_);
	    	    curl_device.HeaderConfig (std::vector<std::string>({
	    	        shared_memory_.parameter.bs_access_token_,
					shared_memory_.parameter.bs_content_type_,
					}));
				curl_device.PostConfig ( (char*) upload_url.c_str() , ka, active_size);
	    	    auto code = curl_device.ExecuteCurl (); 
	    	    curl_device.CloseCurl();
	    	    auto bs_json_info = curl_device.GetWriteContent().str();
	
	    	    std::stringstream ss;
	    	    ss << curl_easy_strerror(code);
	
	    	    if (ss.str().find ("No error") == std::string::npos)
	    	        continue;
	    	    else
	    	        break;
	    	}  
			delete ka;	
		} );
	}
};
#endif
