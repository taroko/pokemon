#include <iostream>
#include <sstream>

#include <algorithm>// copy, min
#include <iosfwd>            
#include <string>// streamsize
#include <cassert>
#include <ios>// ios_base::beg
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/positioning.hpp>

#include "iohandler.hpp"
//#include "fw_device_ofstream.hpp"
//#include "fw_device_basespace.hpp"
//#include "fw_filter_gzip_compressor.hpp"
//#include "../file_reader.hpp"
#include "gtest/gtest.h"

typedef boost::mpl::map
<
	boost::mpl::pair< IoHandlerGlobalParameter::FilteringStreamType, boost::iostreams::filtering_streambuf<boost::iostreams::output> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DeviceParameter, DeviceParameter >
	, boost::mpl::pair< IoHandlerGlobalParameter::MutipleNumber, boost::mpl::int_<2> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DeviceBufferSize, boost::mpl::int_<8> >
	, boost::mpl::pair< IoHandlerGlobalParameter::DevicePushbackSize, boost::mpl::int_<16> >
	, boost::mpl::pair< IoHandlerGlobalParameter::BaseStreamType, std::ostream>

> IO_HANDLER_GLOBAL_SETTING;

typedef SharedMemory<IO_HANDLER_GLOBAL_SETTING> SharedMemoryType;

typedef boost::mpl::vector
<
	//boost::mpl::map
	//<
	//	boost::mpl::pair< IoHandlerGlobalParameter::FWType, FWGzip<SharedMemoryType> >
	//>,
	//boost::mpl::map
	//<
	//	boost::mpl::pair< IoHandlerGlobalParameter::FWType, CURLcd <SharedMemoryType> >
	//>//,
	//boost::mpl::map
	//<
	//	boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::GzipCompressFilter> >
	//>
	//,boost::mpl::map
	//<
	//	boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::Ofstream> >
	//>,
	boost::mpl::map
	<
		boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::BasespaceDeviceEasyUpload> >/*,
		boost::mpl::pair< BaseSpaceLocalParameter::CurlSendMaxLength, boost::mpl::int_<6*1024*1024> >,
		boost::mpl::pair< BaseSpaceLocalParameter::CurlSendLastLength, boost::mpl::int_<5*1024*1024> >*/
	>
> IO_HANDLER_LIST;

typedef boost::mpl::vector
<
	boost::mpl::map
	<
		boost::mpl::pair< IoHandlerGlobalParameter::DeviceType, boost::mpl::int_<FileDeviceType::Ofstream> >//,
	>
> IO_HANDLER_OFSTREAM_LIST;



#include "../format/fastq.hpp"

TEST (ofstream, easy_upload)
{
	DeviceParameter dp;
//	dp.bs_file_open_url_ = "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true";
//	dp.bs_file_open_url_ = "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&directory=output&multipart=true";
//	dp.bs_access_token_ = "x-access-token: 4b2bf2c94ea34721ac01fc177111707f"; 
//	dp.bs_basic_url_ = "https://api.basespace.illumina.com/";
//	dp.bs_content_type_ = "Content-Type: application/txt";
//	dp.bs_upload_content_type_	= "Content-Type: multipart/form-data";
	
	dp.bs_basic_url_ = "https://api.cloud-hoth.illumina.com/";
    dp.bs_version_ = "v1pre3/";
    dp.bs_app_result_ = "appresults/3205202/";//"appresults/7525518/";
	dp.bs_access_token_ = "x-access-token: 4bf327b54e89423dade578450a2b7bed";
	dp.set_bs_filename( "Table.mappability_2.tsv", "output/");
	dp.bs_content_type_ = "Content-Type: application/txt";//"Content-Type: application/octet-stream";

	std::stringstream ss;
	auto ptr = new std::map <int, std::vector < Fastq<> > >;

	namespace mpl = boost::mpl;

//	std::vector< Fastq<> > fqvec;// (10000000);
//	std::ifstream ifs ("/home/oman/work/srna_pipeline/fastq_archive_partial");
//	boost::archive::text_iarchive arci (ifs);
//	arci & fqvec;
//	ifs.close(); 
//	std::cerr<<"fqvec.size "<<fqvec.size()<<'\n';
//	for (auto ii=0; ii!=10; ++ii)//fqvec.size() ; ++ii)
//		ss << fqvec[ii];

	std::ifstream ifs ("/home/oman/work/srna_pipeline/output/sample-A/mappability/Table.mappability.tsv");
	ss << ifs.rdbuf();
	std::cerr<<"ss.size "<<ss.tellp()-ss.tellg()<<'\n';
	
	//std::cerr<<"a"<<'\n';
	iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp);
	//std::cerr<<"b"<<'\n';
	char* f2 = new char [5566];//[26214400];
	//std::cerr<<"c"<<'\n';
	ss.read (f2, 5566);//26214400);
	std::cerr<<"ss.tellp & tellg "<<ss.tellp()<<'\t'<<ss.tellg()<<'\n';
	//std::cerr<<"d"<<'\n';
	bb.write(f2, 5566);//32000);
	//std::cerr<<"e"<<'\n';
//	std::string aa (20000, 'a');
//	char *f2 = (char*) aa.c_str();//"ABCDEFG";
//	bb.write(f2,7);
	bb.bs_async_close();
	bb.close();
//	std::cout << "=====================D=======================" << std::endl;

	BS_pool.ResetPool();
};


/*
TEST (basespace, test1)
{
	DeviceParameter dp;
//	dp.bs_file_open_url_ = "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true";
	dp.bs_file_open_url_ = "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&directory=output&multipart=true";
	dp.bs_access_token_ = "x-access-token: 4b2bf2c94ea34721ac01fc177111707f"; 
	dp.bs_basic_url_ = "https://api.basespace.illumina.com/";
	dp.bs_content_type_ = "Content-Type: application/txt";
	dp.bs_upload_content_type_	= "Content-Type: multipart/form-data";

	std::stringstream ss;
	auto ptr = new std::map <int, std::vector < Fastq<> > >;

	namespace mpl = boost::mpl;
	std::vector< Fastq<> > fqvec;// (10000000);
	std::ifstream ifs ("/home/oman/work/pokemon/fastq_archive_partial");
	boost::archive::text_iarchive arci (ifs);
	arci & fqvec;
	ifs.close(); 
	std::cerr<<"fqvec.size "<<fqvec.size()<<'\n';

	for (auto ii=0; ii!=fqvec.size() ; ++ii)
		ss << fqvec[ii];
	std::cerr<<"ss.size "<<ss.tellp()-ss.tellg()<<'\n';
	
	//std::cerr<<"a"<<'\n';
	iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp);
	//std::cerr<<"b"<<'\n';
	char* f2 = new char [26214400];
	//std::cerr<<"c"<<'\n';
	ss.read (f2, 26214400);
	//std::cerr<<"d"<<'\n';
	bb.write(f2, 32000);
	//std::cerr<<"e"<<'\n';
//	std::string aa (20000, 'a');
//	char *f2 = (char*) aa.c_str();//"ABCDEFG";
//	bb.write(f2,7);
	bb.bs_async_close();
	bb.close();
//	std::cout << "=====================D=======================" << std::endl;

	BS_pool.ResetPool();
};
*/
void test()
{
	std::stringstream bb(std::stringstream::in|std::stringstream::out|std::stringstream::binary);
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	bb << "ABCD";
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	bb.clear();
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	char* tmp = new char[100];
	bb.read(tmp, 1);
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	std::cout.write(tmp, bb.gcount());
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	bb.clear();
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	bb.seekg(0);
	bb.seekp(0);
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;

	
	bb << "OKK";
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	bb.read(tmp, 10);
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	
	std::cout.write(tmp,bb.gcount());
	std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
}
/*
void test2()
{

	bool is_close = false;
	std::string tmptmp("gcount");
	boost::iostreams::example::container_device<std::string> device(tmptmp, is_close);
	std::stringstream parent_buffer;
	
	
	//parent_buffer << "ooooooooooooooooooooooooooooooooooo" << std::endl;
	

	boost::iostreams::filtering_streambuf<boost::iostreams::input> input;
	
	
	
	input.push( boost::iostreams::gzip_compressor() );
	input.push( device );
	
	
	std::istream tmpin(&input);
	
	int copy_size (100);
	
	char* tmp = new char[copy_size+1];
	
	
	input.set_auto_close(false);
	tmpin.read(tmp, copy_size);
	std::cout << "gcount" << tmpin.gcount() << " is_complete " << input.is_complete() << std::endl;
	std::cout << "gcount" << tmpin.gcount() << "tmpin eof" << tmpin.eof() << " is_complete " << input.is_complete() << std::endl;
	tmptmp += "gcount";
	
	
	std::ofstream aaa("aa.gz");
	aaa.write(  tmp, tmpin.gcount());
	
	//auto &bb = parent_buffer;
	//std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	//parent_buffer << "ooooooooooooooooooooooooooooooooooo" << std::endl;
	//std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	//parent_buffer.clear();
	tmpin.clear();
	std::cout << "gcount" << tmpin.gcount() << "tmpin eof" << tmpin.eof() << " is_complete " << input.is_complete() << std::endl;
	
	is_close = true;
	
	
	tmpin.read(tmp, copy_size);
	//input.auto_close();
	
	//std::cout << "gcount " << bb.gcount() << " " << bb.eof() << " " << bb.tellg() << " " << bb.tellp() << std::endl;
	std::cout << "gcount" << tmpin.gcount() << "tmpin eof" << tmpin.eof() << " is_complete " << input.is_complete() << std::endl;
	aaa.write(tmp, tmpin.gcount());
	std::cout << "gg" << std::endl;
	input.auto_close();
	//tmpin.close();
	aaa.close();
	std::cout << "bb" << std::endl;
}
*/

/*
int main(int argc, char** argv)
{
	
	std::string www;
	bool is_close = true;
	//ContainerDevice<std::string> device (www, is_close);

	//boost::iostreams::filtering_streambuf<boost::iostreams::output> output;
	
	//output.push( boost::iostreams::gzip_compressor() );
	//output.push( device );
	
	//std::ostream tmpin(&output);
	
	char *f1 = "ABCDEFG";
	
	//tmpin.write(f1, 7);
//	
//	std::cout << "www.size()" << www.size() << std::endl;
//	
//	tmpin.write(f1, 7);
//	
//	std::cout << "www.size()" << www.size() << std::endl;
//	output.set_auto_close(true);
//	output.reset();
//	
//	std::cout << "www.size()" << www.size() << " " << output.is_complete() << std::endl;
//	
//	std::ofstream oo("aa.gz");
//	oo.write(www.c_str(), www.size());
//	
	
	//test();
	//test2();
	
	//return 0;
	
//	DeviceParameter dp;
//	dp.bs_file_open_url_ = "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true";
//	dp.bs_access_token_ = "x-access-token: 4b2bf2c94ea34721ac01fc177111707f"; 
//	dp.bs_basic_url_ = "https://api.basespace.illumina.com/";
//	dp.bs_content_type_ = "Content-Type: application/gz";
//	
//	dp.filename = "aa.gz";
//	
//	iohandler<IO_HANDLER_GLOBAL_SETTING, IO_HANDLER_LIST>  bb(dp);
//
//	std::cout << "=====================A=======================" << std::endl;
//	bb.write(f1,7);
//	std::cout << "=====================B=======================" << std::endl;
//
//	char *f2 = "ABCDEFG";
//
//	bb.write(f2,7);
//	std::cout << "=====================C=======================" << std::endl;
//	bb.close();
//	std::cout << "=====================D=======================" << std::endl;
//	
//	std::ostream dd(&output);
//	dd.write(f1,7);
//	
//	aa.write(f1, 7);
//	aa.write(f1, 7);
	
//	std::stringstream aaa;
//	std::stringstream bbb;
//	aaa<<"111111111111111111111111\n";
//	bbb<<"222222222222222222222222\n";
//	std::cout <<"I" <<std::endl;
//	aa.open("aa");
//	std::cout <<"A" <<std::endl;
//	aa.push_buffer(aaa);
//	std::cout <<"B" <<std::endl;
//	aa.push_buffer(bbb);
//	std::cout <<"C" <<std::endl;
//	aa.close();
//	std::cout <<"D" <<std::endl;
	
//	return 0;
}
*/
