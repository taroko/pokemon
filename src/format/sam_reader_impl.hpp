/// @file Sam_reader_impl.hpp
/// @brief Specialized version of class FileReader_impl for Sam, for provide operation detail for getting a piece of Sam data from an ifstream object
/// @author C-Salt Corp.
#ifndef SAM_READER_IMPL_HPP_
#define SAM_READER_IMPL_HPP_
#include <bitset>
#include <cstdint>
#include <vector>
#include <fstream>
#include <boost/algorithm/string/split.hpp>
#include "sam.hpp"
#include "../tuple_utility.hpp"
#include "../wrapper_tuple_utility.hpp"
#include "../format_io_iterator.hpp"
#include "../file_reader_impl.hpp"
#include "../constant_def.hpp"
#include <boost/algorithm/string.hpp>
/// @brief Read Sam format data, including member functions: io_end (); get_next_entry (); get_all_entry (); read_bam(); write_bam()
/// @tparam Sam template template parameter Sam, indicating current FileReader_impl is a specilization form of the base FileReader_impl template class for Sam format
/// @tparam TUPLETYPE indicating Sam data format, defaulted as tuple < string, int, string, uint32_t, int, string, string, uint32_t, int, string >
template<class TUPLETYPE, SOURCE_TYPE STYPE> //Specialization for Sam
class FileReader_impl < Sam, TUPLETYPE, STYPE >
    : public DataSource < STYPE >
{
public:
typedef Sam<TUPLETYPE> format_type;
typedef TUPLETYPE tuple_type;
typedef SOURCE_TYPE source_type; 
};         

//Specialization for SAM
template <>//typename TUPLETYPE>
class FileReader_impl < Sam, //TUPLETYPE, 
    	std::tuple <
	        std::string, //QNAME
	        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
	        std::string, //RNAME
	        uint64_t, //POS
	        int, //MAPQ
	        std::string, //CIGAR
	        std::string, //RNEXT
	        uint64_t, //PNEXT
	        int64_t, //TLEN
	        std::string,//, //SEQ
	        std::string, //QUAL
			UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
//UserDefineContent 
//	        int, // NH
//	        std::string //TailSeq
				>,	
						SOURCE_TYPE::IFSTREAM_TYPE >
	: public DataSource < SOURCE_TYPE::IFSTREAM_TYPE >
{
public:
	std::string header_;

    typedef std::tuple <
        std::string, //QNAME
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
		UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
//UserDefineContent
//        int, // NH
//        std::string //TailSeq
			> TUPLETYPE; 

    size_t file_num_;
///@typedef io_iterator in format of formatIoIterator <Sam <TUPLETYPE>, FileReader_impl <Sam, TUPLETYPE> >
	typedef FormatIoIterator<
							Sam <TUPLETYPE>,
							FileReader_impl< Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
					   		> io_iterator;   // using specialized FormatIoIterator as iterator
/// @memberof FileReader_impl<Sam, TUPLETYPE>
/// @brief return a poniter pointing to a Sam <TUPLETYPE> with its eof_flag setted as true
/// @return shared_ptr < Sam <TUPLETYPE> > (new Sam <TUPLETYPE> (true) )
	io_iterator io_end ()
	{
		return io_iterator (std::shared_ptr < Sam <TUPLETYPE> > (new Sam <TUPLETYPE> (true) ) );
	};

    FileReader_impl ()
        : DataSource < SOURCE_TYPE::IFSTREAM_TYPE > ()
        , file_num_ (0)
		, header_("")
    {}

    FileReader_impl (std::vector<std::string>& file_path, std::vector <uint64_t> sizein = std::vector<uint64_t>(0) )
        : DataSource < SOURCE_TYPE::IFSTREAM_TYPE > (file_path, sizein)
        , file_num_ ( file_path.size() )
		, header_("")
    {}

/// @memberof FileReader_impl<Sam, TUPLETYPE>
/// @brief return an object of Sam <TUPLETYPE> by means of reading an std::ifstream& object
/// @param file_handle an std::ifstream& object 
/// @return Sam <TUPLETYPE> 
	Sam<TUPLETYPE> get_next_entry (size_t index)//std::ifstream& file_handle)
	{
		Sam<TUPLETYPE> result;
		std::string line_temp1, line_temp2;

//		std::ifstream file_handle ( file_name );
//		file_handle.peek();
//		if ( file_handle.eof() || file_handle.fail() || file_handle.bad() )
        this->file_handle [index] -> peek();
        if ( this->file_handle[index]->eof() || this->file_handle[index]->fail() || this->file_handle[index]->bad() )
		// The if (file_handle.fail() || file_handle.eof() || file_handle.bed() ) function detects ifstream fail/eof/bad flags, and 
		// return Sam <TUPLETYPE> with enabled eof_flag when any one of the aformentioned flags indicates true. 
		// Whenever the returned Sam <TUPLETYPE> has an enabled eof_flag, it is indicated that at least one of fail/eof/bad/ situations occurs
		{
			result.eof_flag = true;
			return result;
		}   
		while ( std::getline (*(this->file_handle[index]), line_temp1) )
		{
/// @brief The if (file_handle.fail() || file_handle.eof() || file_handle.bed() ) function detects ifstream fail/eof/bad flags, and return Sam <TUPLETYPE> with enabled eof_flag when any one of the aformentioned flags indicates true \n Whenever the returned Sam <TUPLETYPE> has an enabled elf_flag, it is indicated that at least one of fail/eof/bad/ situations occurs
			boost::algorithm::trim (line_temp1);
			if ( line_temp1.substr (0,1) == "@")
				header_ += line_temp1+' ';
//				line_temp2+=(line_temp1+' ');
			else
			{
				line_temp2+=line_temp1;//+=('\t'+line_temp1);
				std::vector < std::string > split_temp1;//, split_temp2;
				boost::split (split_temp1, line_temp2, boost::is_any_of ("\t") );

				if ( split_temp1.size() > 11 )//12 )
				{
					for (size_t x = 12; x!=split_temp1.size(); ++x)//13; x!=split_temp1.size(); ++x)
						split_temp1[11] += ('\t' + split_temp1[x]);//split_temp1[12] += (' ' + split_temp1[x]);
					split_temp1.resize(12);	//split_temp1.resize(13);
				}
				TUPLETYPE SAM_in1;//, SAM_in2;
				TupleUtility< TUPLETYPE, 11//std::tuple_size<TUPLETYPE>::value  
					>:: FillTuple (SAM_in1, split_temp1);
				int i;
				size_t pos1, pos2, pos3, pos4;
				pos1 = split_temp1[11].find ("NH:i:") + 5 ;

				if (pos1 != std::string::npos)
				{
					pos2 = split_temp1[11].find ("\t", pos1) ;
					i = std::stoi (split_temp1[11].substr (pos1, pos2-pos1));
				}
				pos3 = split_temp1[11].find ("TL:Z:") + 5 ;
				if (pos3 != std::string::npos)
					pos4 = split_temp1[11].find ("\n", pos3) ;
				std::get<11> (SAM_in1) =  UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >//UserDefineContent 
				(i, (split_temp1[11].substr (pos3, pos4-pos3)));
				result.data = SAM_in1;
				break;//return result;
			}
		}
std::cerr<<"obtained sam\n"<<result;
		return result;
	}

/// @memberof FileReader_impl<Sam, TUPLETYPE>
/// @brief receive a std::string of file_name, and accordingly read all Sam <TUPLETYPE> entries thereof and update the call by reference parameter of data_cluster to record the god Sam<TUPLETYPE> data entries
/// @param file_name, in format of std::string 
/// @param std::vector< Sam <TUPLETYPE> >, passed into get_all_entry function by reference, so as to keep the got Sam<TUPLETYPE> data entries
	//static void get_all_entry (std::string& file_name, std::vector< Sam<TUPLETYPE> >& data_cluster)
	//{
	//	std::ifstream file_handle ( file_name );
	//	while (true)
	//	{	
	//		auto x = get_next_entry (file_handle);
	//		if (x.eof_flag)
	//			break;
	//		data_cluster.push_back (x);
	//	}	
	//}

/// @memberof FileReader_impl<Sam, TUPLETYPE>
/// @brief return a char* of outFile name, wherein the outFile storges Sam<TUPLETYPE> format data converted from inFile
/// @param inFile in char* format, indicating to be read bam file
/// @param outFile in char* format, indicating converted sam file
/// @return char* == outFile
	//static char*  read_bam (char* inFile, char* outFile)
	//{
	//	std::string cmd = "/Users/obigbando/Downloads/bamtools/bin/bamtools convert -format sam -in ";
	//	cmd = cmd + inFile + " -out " + outFile;
	//	system ( cmd.c_str() );
	//	return outFile;
	//};

/// @memberof FileReader_impl<Sam, TUPLETYPE>
/// @brief return a char* of file name, wherein the outName storges bam format data converted from inName
/// @param inName in char* format, indicating to be read Sam file
/// @param outName in char* format, indicating converted bam file
/// @return std::string == outName
	//static char* write_bam (char* inFile, char* outFile)
	//{
	//	std::string cmd = "~/Downloads/samtools-0.1.18/samtools view -bS ";
	//	cmd = cmd + inFile + " > " + outFile;
	//	system ( cmd.c_str() );
	//	return outFile;
	//}
};

template<class TUPLETYPE, SOURCE_TYPE STYPE >//curl_gz & curl_plain specialization for FASTA
class FileReader_impl < Sam, TUPLETYPE, STYPE, curl_default_handle_mutex >
    : public DataSource < STYPE, curl_default_handle_mutex >
{
typedef Sam<TUPLETYPE> format_type;
typedef TUPLETYPE tuple_type;
typedef curl_default_handle_mutex curl_handle_type;

    size_t file_num_;
    bool end_;
    typedef FormatIoIterator<
                            Sam<TUPLETYPE>,
                            FileReader_impl<Sam, TUPLETYPE, STYPE, curl_default_handle_mutex >//SOURCE_TYPE::CURL_TYPE_GZ >
                            > io_iterator;
public:
    io_iterator io_end ()
    {
        return io_iterator (std::shared_ptr < Sam <TUPLETYPE> > (new Sam <TUPLETYPE> (true) ) );
    }

    FileReader_impl ()
        : DataSource < STYPE, curl_default_handle_mutex > ()
        , file_num_ (0)
        , end_ (false)
    {}

    FileReader_impl ( std::vector <std::string>& url, std::vector <uint64_t> gz_file_size )
        : DataSource < STYPE, curl_default_handle_mutex > ( url, gz_file_size )//, thread_count, thread_recv_volume )
        , file_num_ ( url.size() )
        , end_ (false)
    {}

	Sam<TUPLETYPE> get_next_entry (size_t& index)//std::ifstream& file_handle)
	{
		Sam<TUPLETYPE> result;
		std::string line_temp1, line_temp2;

        this->file_handle [index] -> peek();
        if ( this->file_handle[index]->eof() || this->file_handle[index]->fail() || this->file_handle[index]->bad() )
		// The if (file_handle.fail() || file_handle.eof() || file_handle.bed() ) function detects ifstream fail/eof/bad flags, and 
		// return Sam <TUPLETYPE> with enabled eof_flag when any one of the aformentioned flags indicates true. 
		// Whenever the returned Sam <TUPLETYPE> has an enabled eof_flag, it is indicated that at least one of fail/eof/bad/ situations occurs
		{
			result.eof_flag = true;
			return result;
		}   
		while ( std::getline (*(this->file_handle[index]), line_temp1) )
		{
/// @brief The if (file_handle.fail() || file_handle.eof() || file_handle.bed() ) function detects ifstream fail/eof/bad flags, and return Sam <TUPLETYPE> with enabled eof_flag when any one of the aformentioned flags indicates true \n Whenever the returned Sam <TUPLETYPE> has an enabled elf_flag, it is indicated that at least one of fail/eof/bad/ situations occurs
			boost::algorithm::trim (line_temp1);
			if ( line_temp1.substr (0,1) == "@")
				line_temp2+=(line_temp1+' ');
			else
			{
				line_temp2+=('\t'+line_temp1);
				std::vector < std::string > split_temp1;//, split_temp2;
				boost::split (split_temp1, line_temp2, boost::is_any_of ("\t") );
				for (size_t x = 11; x!=split_temp1.size(); ++x)
					split_temp1[10] += (' ' + split_temp1[x]);
				split_temp1.resize(11);
				TUPLETYPE SAM_in1;//, SAM_in2;
				TupleUtility< TUPLETYPE, std::tuple_size<TUPLETYPE>::value  >:: FillTuple (SAM_in1, split_temp1);
				result.data = SAM_in1;
				break;
			}
		}
		return result;
	}
};
#endif
