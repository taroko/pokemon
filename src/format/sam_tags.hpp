#ifndef SAM_TAGS_HPP_
#define SAM_TAGS_HPP_

#include <boost/mpl/string.hpp>
#include <boost/mpl/push_front.hpp>
#include <boost/tuple/tuple.hpp> 
#include <boost/preprocessor/repetition/enum_params.hpp> 
#include <boost/mpl/copy.hpp>

/**
 * @brief 原型 Tag format type，定義Sam格式的 TAGS 的型別，e.g., AM->int, NH->int, TL->string。然後 struct裡面一定要定義 VALUE_TYPE 的型別。
 * @tparam TAG_NAME_TYPE 之後會帶入 boost mpl string
 */
template<class TAG_NAME_TYPE>
struct TagFormatType
{};

/// @brief The smallest template-independent mapping quality of segments in the rest
template<> struct TagFormatType< boost::mpl::string<'AM'> >	{ typedef int VALUE_TYPE; };

/// @brief Number of reported alignments that contains the query in the current record
template<> struct TagFormatType< boost::mpl::string<'NH'> >	{ typedef int VALUE_TYPE; };

/// @brief Tailing sequence
template<> struct TagFormatType< boost::mpl::string<'TL'> >	{ typedef std::string VALUE_TYPE; };

/**
 * @brief 原型版本，Sam Format 第十三欄位自定Tag的型別，這邊使用Boost mpl string來定義，e.g.,boost::mpl::string<'NH'>，並且可以多項定義
 * @tparam TAG_TYPE boost::mpl::string<'XX'>，XX 為自定Tag。注意！TagFormatType也必須同時定義好。
 */
template<class TAG_TYPE, class... ARGS>
struct UserDefineTagsFormat
{
	/// @brief TAG_VALUE_TYPE 為此 TAG_TYPE(boost mpl string) 內容儲存的型別(int or string, ...)，為了宣告tuple內容型別，必須先由TagFormatType定義好。使用者一般不會用到。
	typedef typename TagFormatType<TAG_TYPE>::VALUE_TYPE TAG_VALUE_TYPE;
	
	/// @brief TUPLE_TYPE 為最終整個UserDefineTagsFormat的真正儲存型別，使用Tuple來儲存；在此還使用了靜態遞迴tuple_push_front來依序把 Tuple內容型別一個一個塞入
	typedef typename tuple_push_front<TAG_VALUE_TYPE, typename UserDefineTagsFormat<ARGS...>::TUPLE_TYPE >::type TUPLE_TYPE;
	
	/// @brief TAGS_ORI_TYPE 為 boost mpl vector 主要是用來儲存原始 TAG_TYPE(boost mpl string) 型別，也就是使用者定義的 tag型別，e.g., boost::mpl::string<'NH'>
	typedef boost::mpl::vector <TAG_TYPE, ARGS...> TAGS_ORI_TYPE;
	
	/// @brief 舉例來說如果使用者宣告：UserDefineTagsFormat< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
	/// @brief TUPLE_TYPE 的型別會是 tuple<int, std::string>
	/// @brief 而 TAGS_ORI_TYPE 的型別會是 boost::mpl::vector< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
	
};

/**
 * @brief 特化版本，靜態遞迴終止條件
 * @tparam TAG_TYPE boost::mpl::string<'XX'>，XX 為自定Tag。注意！TagFormatType也必須同時定義好。
 */
template<class TAG_TYPE>
struct UserDefineTagsFormat< TAG_TYPE >
{
	/// @brief TAG_VALUE_TYPE 為此 TAG_TYPE(boost mpl string) 內容儲存的型別(int or string, ...)，為了宣告tuple內容型別，必須先由TagFormatType定義好。使用者一般不會用到。
	typedef typename TagFormatType<TAG_TYPE>::VALUE_TYPE TAG_VALUE_TYPE;
	
	/// @brief TUPLE_TYPE 為最終整個UserDefineTagsFormat的真正儲存型別，使用Tuple來儲存；並且是第一個 Tuple，之後會被靜態遞迴依序擴展。
	typedef std::tuple< TAG_VALUE_TYPE > TUPLE_TYPE;
};

/**
 * @brief 原型版，將動態std::string tag與value，比對靜態boost::mpl::string，將值正確塞入tuple中。這邊使用靜態遞迴來依序塞入。 
 * @tparam int N，為目前 tuple 的內容型別次序，主要是給 std::get<N>使用，也是要將動態轉靜態的關鍵
 * @tparam TAG_TYPE 為 boost::mpl::string<'XX'>，sam第十三欄位自定型別的 tag。
 */
template<int N, class TAG_TYPE, class... ARGS>
struct AssignTuple
{
	/// @brief 取得 boost::mpl::string Tag 的 value 真正型別
	typedef typename TagFormatType<TAG_TYPE>::VALUE_TYPE TAG_VALUE_TYPE;
	
	/**
	 * @brief 將值塞入tuple中
	 * @tparam tag_type Tag的儲存內容型別
	 * @tparam tuple_type 儲存的 Tuple 完整型別
	 * @param tag_name sam format 第十三欄位的自定 tag 的名稱，e.g., NH, TL, ...，此function會一個一個對照TAG_TYPE（遞迴），對中Tuple內容的某個tag，則塞入
	 * @param tag_value 要存入的value type
	 * @param tuple 儲存的 tuple
	 * @return void
	 */
	template<class tag_type, class tuple_type>
	static void assign(std::string &tag_name, tag_type &tag_value, tuple_type & tuple)
	{
		/// @brief 靜態的boost mpl string TAG_TYPE 如果等於動態的string tag_name，就將值塞入, std::get<N>(tuple)
		/// @brief 否則就N+1（靜態遞迴），繼續找相同的 TAG
		if(boost::mpl::c_str<TAG_TYPE>::value == tag_name)
		{
			std::get<N>(tuple) = boost::lexical_cast< TAG_VALUE_TYPE > ( tag_value );
		}
		else
		{
			AssignTuple<N+1, ARGS...>::assign(tag_name, tag_value, tuple);
		}
	}
};

/**
 * @brief 特化版本，靜態遞迴終止條件
 * @tparam int N，為目前 tuple 的內容型別次序，主要是給 std::get<N>使用，也是要將動態轉靜態的關鍵
 * @tparam TAG_TYPE 為 boost::mpl::string<'XX'>，sam第十三欄位自定型別的 tag。
 */
template<int N, class TAG_TYPE>
struct AssignTuple < N, TAG_TYPE >
{
	typedef typename TagFormatType<TAG_TYPE>::VALUE_TYPE TAG_VALUE_TYPE;
	
	template<class tag_type, class tuple_type>
	static void assign(std::string &tag_name, tag_type &tag_value, tuple_type & tuple)
	{
		if(boost::mpl::c_str<TAG_TYPE>::value == tag_name)
		{
			std::get<N>(tuple) = boost::lexical_cast< TAG_VALUE_TYPE > ( tag_value ); 
		}
	}
};


/**
 * @brief 繼承tuple，主要來存放 sam format 第十三欄位的內容 
 * @tparam ARGS... 許多連率的 boost::mpl::string TAG, boost::mpl::string<'NH'>
 */
template<class... ARGS> 
struct UserDefineTags
	/// @brief UserDefineTagsFormat<ARGS...>::TUPLE_TYPE 為tuple，繼承tuple
	:public UserDefineTagsFormat<ARGS...>::TUPLE_TYPE
{
	typedef typename UserDefineTagsFormat<ARGS...>::TUPLE_TYPE TUPLE_TYPE;
	
	UserDefineTags()
	{}
	
	/// @brief 依序給予值的 tuple建構式
	template<class... args>
	UserDefineTags(const args&... args_list)
		:UserDefineTagsFormat<ARGS...>::TUPLE_TYPE(args_list...)
	{}
	
	UserDefineTags (TUPLE_TYPE &data_in)
		: UserDefineTagsFormat<ARGS...>::TUPLE_TYPE (data_in)
	{}
	UserDefineTags (TUPLE_TYPE &&data_in) 
		: UserDefineTagsFormat<ARGS...>::TUPLE_TYPE (std::move (data_in))
	{}
	
	/**
	 * @brief String2tag constructor，會將用tab隔開的文字，split後依序填入儲存的 tuple
	 * @param tags_str 用tab隔開的文字字串
	 */
	UserDefineTags(const std::string & tags_str)
	{
		std::vector<std::string> tmp_splits;
		boost::split( tmp_splits, tags_str, boost::is_any_of( "\t" ));
		for(auto iter(tmp_splits.begin()); iter != tmp_splits.end(); iter++ )
		{
			std::vector<std::string> tmp_splits2;
			boost::split( tmp_splits2, *iter, boost::is_any_of( ":" ));
			if(tmp_splits2.size() != 3)
			{
				std::cerr << "Error! '" << *iter << "' is not a sam tag format" << std::endl;
				continue;
			}
			AssignTuple<0, ARGS...>::assign(tmp_splits2[0], tmp_splits2[2], *this);
		}
	}
	
	/**
	 * @brief Vector of splited string constructor，傳入 split後的 vector begin and end，在依序填入tuple，可以避免上述複製 string
	 * @param begin splited vector iterator begin
	 * @param end splited vector iterator end
	 */
	UserDefineTags(const std::vector<std::string>::iterator &begin, const std::vector<std::string>::iterator &end)
	{
		for(auto iter(begin); iter != end; iter++ )
		{
			std::vector<std::string> tmp_splits2;
			boost::split( tmp_splits2, *iter, boost::is_any_of( ":" ));
			if(tmp_splits2.size() != 3)
			{
				std::cerr << "Error! '" << *iter << "' is not a sam tag format" << std::endl;
				continue;
			}
			AssignTuple<0, ARGS...>::assign(tmp_splits2[0], tmp_splits2[2], *this);
		}
	}
	
	
	template<int N, class TAG_TYPE, class tag_type_str>
	struct INTorSTRING
	{
		static void str(TUPLE_TYPE &tuple, std::string &out)
		{
			out += boost::mpl::c_str<TAG_TYPE>::value;
			out += boost::mpl::c_str<tag_type_str>::value;
			out += boost::lexical_cast< std::string >( std::get<N>(tuple) );
		}
	};
	template<int N, class TAG_TYPE>
	struct INTorSTRING< N, TAG_TYPE, boost::mpl::string<':Z:'> >
	{
		static void str(TUPLE_TYPE &tuple, std::string &out)
		{
			if(std::get<N>(tuple) != "")
			{
			  out += boost::mpl::c_str<TAG_TYPE>::value;
			  out += ":Z:";
			  out += std::get<N>(tuple);
			}
		}
	};
	
	
	/**
	 * @brief 原型版本，巢狀 struct，主要是要將tuple的內容與boost mpl string記得內容整合輸出為sam format第十三欄位（也同樣使用靜態遞迴）
	 * @tparam int N，為目前 tuple 的內容型別次序，主要是給 std::get<N>使用
	 * @tparam TAG_TYPE 為 boost::mpl::string<'XX'>，sam第十三欄位自定型別的 tag。
	 */
	template<int N, class TAG_TYPE, class... args>
	struct ToStr
	{
		typedef typename TagFormatType<TAG_TYPE>::VALUE_TYPE TAG_VALUE_TYPE;
		
		static void str(TUPLE_TYPE &tuple, std::string &out)
		{
			if(boost::lexical_cast< std::string >( std::get<N>(tuple) ) != "")
			{
				typedef boost::is_same<TAG_VALUE_TYPE, std::string> is_string_type;
				typedef typename boost::mpl::if_<is_string_type, boost::mpl::string<':Z:'>, boost::mpl::string<':i:'> >::type tag_type_str;
				
				out += boost::mpl::c_str<TAG_TYPE>::value;
				out += boost::mpl::c_str<tag_type_str>::value;
				out += boost::lexical_cast< std::string >( std::get<N>(tuple) );
				out += "\t";
			}
			ToStr<N+1, args...>::str(tuple, out);
		}
	};
	
	
	
	
	/**
	 * @brief 特化版本，巢狀 struct，主要是要將tuple的內容與boost mpl string記得內容整合輸出為sam format第十三欄位（也同樣使用靜態遞迴）
	 * @tparam int N，為目前 tuple 的內容型別次序，主要是給 std::get<N>使用
	 * @tparam TAG_TYPE 為 boost::mpl::string<'XX'>，sam第十三欄位自定型別的 tag。
	 */
	template<int N, class TAG_TYPE>
	struct ToStr<N, TAG_TYPE>
	{
		typedef typename TagFormatType<TAG_TYPE>::VALUE_TYPE TAG_VALUE_TYPE;
		static void str(TUPLE_TYPE &tuple, std::string &out)
		{
			typedef boost::is_same<TAG_VALUE_TYPE, std::string> is_string_type;
			typedef typename boost::mpl::if_<is_string_type, boost::mpl::string<':Z:'>, boost::mpl::string<':i:'> >::type tag_type_str;
			INTorSTRING<N, TAG_TYPE, tag_type_str>::str(tuple, out);
			//if(std::get<N>(tuple) != "")
			//{
			//	out += boost::mpl::c_str<TAG_TYPE>::value;
			//	out += boost::mpl::c_str<tag_type_str>::value;
			//	out += boost::lexical_cast< std::string >( std::get<N>(tuple) );
			//}
		}
	};
	
	void str (std::string& out)
    {
    	ToStr<0, ARGS...>::str(*this, out);
    	boost::trim(out);
    }
	
};
#endif
