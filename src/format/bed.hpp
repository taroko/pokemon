///@file bed.hpp
///@brief Provide definition for Bed Format. 
///@author C-Salt Corp.
#ifndef BED_HPP_
#define BED_HPP_
#include <cstdint>
#include <string>
#include <tuple>
#include <algorithm> 
#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include "is_tuple_type.hpp"

/**
 * @struct Bed
 * @brief This is a Bed struct, including member elements:
 *  static type (enum format_type);
 *  data (TUPLETYPE);
 *  eof_flag (bool)
 * This struct can store the Bed format data, provide constructor for move and copy.  We also overload the operator<< for print the Bed data.\n
 * @tparam TUPLETYPE defaulted as tuple < string, uint32_t, uint32_t >, for keeping track of chrom, start_position, and end_position \n
 */

template<class TUPLETYPE = std::tuple <std::string, uint32_t, uint32_t> >
struct Bed //three fields version
{
	static_assert( IsTupleType<TUPLETYPE>::value == true, "ARGUMENT IS NOT A TUPLE");

//Members
	/// @memberof struct Bed <TUPLETYPE>
	/// @brief a member element, having a data format capable of storing Bed data \n defaulted as tuple < string, uint32_t, uint32_t > 
	TUPLETYPE data;
	/// @memberof struct Bed <TUPLETYPE>
	/// @brief an enumerated static identifier, indicating aforementioned struct is in Bed<TUPLETYPE> format
	static const format_types type = format_types::BED;
	/// @memberof struct Bed <TUPLETYPE>
	/// @brief a bool flag, indicating whether the Bed data, got by Bed_reader's get_next_entry function, correponds to the eof of the read ifstream object 
	bool eof_flag;  //indicating whether file_handle reaches EOF 
	/// @typedef TupleType as a alias for TUPLETYPE 
	typedef TUPLETYPE TupleType;

//Constructors
	/// @brief Default constructor
 	/// @see TEST(Bed, default_constructor) unit test example
	Bed () 
		: data(), eof_flag( false )  //default constructor
	{}

	///	@brief Copy constructor
	/// @see TEST(Bed, copy_constructor) unit test example
	Bed ( const Bed& in ) 
        : data( in.data ), eof_flag( in.eof_flag )  //copy constructor
	{}

	/// @brief Assignment operator
	/// @see TEST(Bed, assignment_constructor) unit test example
	Bed& operator=( const Bed& in ) //Assignment operator
	{
		data = in.data;
		eof_flag = in.eof_flag;
		return *this;
	}

	/// @brief Move constructor
	/// @see TEST(Bed, move_constructor) unit test example
	Bed (Bed && other) 
        : data( std::move(other.data ) ), eof_flag ( std::move(other.eof_flag) )
	{}

	/// @brief Move assignment operator
	/// @see TEST(Bed, move_assignment) unit test example
	Bed& operator= (Bed && other)
	{
		data = std::move(other.data);
		eof_flag = std::move(other.eof_flag);
		return *this;
	}

	/// @brief Consructor with data_in, in TUPLETYPE format
	/// @see TEST(Bed, tupletype_copy_constructor) unit test example
	Bed (TUPLETYPE & data_in) 
        : data (data_in), eof_flag (false)
	{}

	/// @brief Move constructor with data_in, in format of TUPLETYPE
	/// @see TEST(Bed, tupletype_move_constructor) unit test example
	Bed (TUPLETYPE && data_in_move) 
        : data (std::move (data_in_move) ), eof_flag (false) //Move constructor for specific TUPLETYPE
	{}

	/// @brief Constructor with EofFlag, in bool type
	/// @see TEST(Bed, eof_constructor) unit test example
	Bed (bool EofFlag) 
        : eof_flag (EofFlag) 
    {} //end of file flag for bed_reader io_end()

//Functions
	/// @brief Overload operator << to facilitate lexical_cast, having Bed data s converted into a single std::string, so that ofstream operation can be easily achieved
	/// @see TEST(Bed, overload_smaller_operator) unit test example
	friend std::ostream& operator<< (std::ostream& out, const Bed& s)
	{
		std::string result;
		TupleUtility< TupleType, std::tuple_size< TupleType >::value >::PrintTuple(s.data, result, 1); ////calling the overload version for tab delimited data structure
//		std::replace(result.begin(),result.end(),'\n','\t');
        result.resize (result.size()-1);
        result+='\n';
		out << result;
		return out;
	}
	
	bool operator< (const Bed<TUPLETYPE>& input) const
	{
		return data < input.data;
	}

    friend class boost::serialization::access;
	/// @brief serialization implementation for Bed struct
	/// @see TEST(Bed, serialization) unit test example
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
        TupleUtility < TUPLETYPE, std::tuple_size<TUPLETYPE>::value >
            :: SerializeTuple (data, ar, version);
        ar & eof_flag;
    }
};
#endif
