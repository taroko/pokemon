#ifndef JSON_HANDLER_
#define JSON_HANDLER_
#include "curl/curl.h"
#include <string>
#include <iostream>
#include <sstream>
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include <tuple>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include "../iohandler/curl_impl/curl_utility.hpp"
#include "../iohandler/curl_impl/curl_impl.hpp"

template <typename FUNC=curl_default_handle >///_json>
struct json_handler
{
	std::string json_from_server_;
	std::stringstream ss_;
	boost::property_tree::ptree p_tree_;
	std::string basic_str_, token_, access_token_;//content_type_, 
	std::vector < std::vector <std::string> > url_vec_, name_vec_, upload_url_vec_;
	std::vector < std::vector <uint64_t> > size_vec_;
	std::vector <size_t> uid_vec_;

	json_handler (const std::string& json_in, const std::string& amend_name ="")
		: json_from_server_ (json_in)
//		, basic_str_ ("https://api.basespace.illumina.com/")
//		, content_type_ ("Content-Type: application/text")
	{
		get_json_from_server();
		parse_json_1 (amend_name);
	}

	CURLcode get_json_from_server (void)
	{
		CurlConfigSet<> dp;
		CurlImpl <> curl_device (dp);
		curl_device.GetConfig ( (char*) json_from_server_.c_str() );
		curl_device.WriteConfig (&(ss_), &call_back_operate_mutexless <FUNC> );
		auto code = curl_device.ExecuteCurl();

		int repeat = 0;
		while (repeat < 10)
		{
			if (code == CURLE_OK)
				break;
			else
			{
std::cerr<<"json failed: repeat "<<repeat<<'\n';
				std::chrono::milliseconds dura( 1000 );
				std::this_thread::sleep_for( dura );
				ss_.str(std::string());
				ss_.clear();
				CurlImpl <> curl_device (dp);
				curl_device.GetConfig ( (char*) json_from_server_.c_str() );
				curl_device.WriteConfig (&(ss_), &call_back_operate_mutexless <FUNC> );
				code = curl_device.ExecuteCurl();
std::cerr<<"code==CURLE_OK?? "<<(code==CURLE_OK)<<'\n';
				++repeat;
			}
		}

		boost::property_tree::json_parser::read_json ( ss_, p_tree_ );
		return code;
	}
/*
	CURLcode get_json_from_server (void)
	{
		CURLcode code (CURLE_FAILED_INIT);
		CURL* curl = curl_easy_init();
		curl_slist * pHeaders = NULL;
		if (curl)
		{
			if (   CURLE_OK == ( code = curl_easy_setopt ( curl, CURLOPT_URL, json_from_server_.c_str() ) ) 
				&& CURLE_OK == ( code = curl_easy_setopt ( curl, CURLOPT_WRITEFUNCTION, call_back_operate_mutexless <FUNC> ) )
				&& CURLE_OK == ( code = curl_easy_setopt ( curl, CURLOPT_WRITEDATA, &(ss_) ) )
				&& CURLE_OK == ( code = curl_easy_setopt ( curl, CURLOPT_NOPROGRESS, 1L) )
				&& CURLE_OK == ( code = curl_easy_setopt ( curl, CURLOPT_FOLLOWLOCATION, 1L) ) 
				)
				code = curl_easy_perform (curl);
			curl_easy_cleanup (curl);
		}
		boost::property_tree::json_parser::read_json ( ss_, p_tree_ );
		return code;
	}
*/
	void parse_json_1 (const std::string& amend_name)
	{
		token_ = p_tree_.get<std::string>("access_token"); 

		basic_str_ = p_tree_.get <std::string> ("platform") + "/"; 

		access_token_ = ("x-access-token: "+token_);
		boost::property_tree::ptree& pptree = p_tree_.get_child ("files");
		for (boost::property_tree::ptree::iterator it = pptree.begin(); it != pptree.end(); ++it)
		{
		    auto ttt = it->second; //


			url_vec_.emplace_back ( std::vector <std::string> 
				({
					basic_str_ + it->second.get <std::string> ("R1_content") + "/content?access_token=" + token_, // 4b2bf2c94ea34721ac01fc177111707f,
					basic_str_ + it->second.get <std::string> ("R2_content") + "/content?access_token=" + token_
				}) 
			);

			size_vec_.emplace_back ( std::vector <uint64_t> 
				({
					it->second.get <uint64_t> ("R1_size"),
					it->second.get <uint64_t> ("R2_size")
				}) 
			);

			name_vec_.emplace_back ( std::vector <std::string> 
				({
					it->second.get <std::string> ("R1_name"),
					it->second.get <std::string> ("R2_name")
				}) 
			);

			upload_url_vec_.emplace_back ( std::vector <std::string>
				({
					basic_str_ + p_tree_.get<std::string> ("HrefAppresult") + "/files?name=" + 	it->second.get <std::string> ("R1_name") + amend_name + "&multipart=true",
					basic_str_ + p_tree_.get<std::string> ("HrefAppresult") + "/files?name=" + 	it->second.get <std::string> ("R2_name") + amend_name + "&multipart=true",
				})
			);

			uid_vec_.emplace_back (it->second.get <size_t> ("uid") );
		}
	}

	std::tuple<std::string, std::string, std::string, std::string> get_info (void)
	{
		auto app_result = p_tree_.get<std::string> ("HrefAppresult");
		std::vector<std::string> split_vec;
		boost::split (split_vec, app_result, boost::is_any_of ("/") );
		//return 
		auto yy = std::make_tuple (split_vec[0]+"/", std::string(split_vec[1]+"/"+split_vec[2]+"/"), access_token_, basic_str_);
		std::cerr<<"json info "<<std::get<0>(yy)<<'\t'<<std::get<1>(yy)<<'\t'<<std::get<2>(yy)<<'\t'<<std::get<3>(yy)<<'\n';
		return yy;
	}

	void print (void)
	{
std::cerr<<"basic str "<<basic_str_<<std::endl;

std::cerr<<"url"<<'\n';
		for (auto& i : url_vec_)
			for (auto& j : i )
			std::cerr<<j<<'\n';
std::cerr<<"size"<<'\n';
		for (auto& i : size_vec_)
			for (auto& j : i )
			std::cerr<<j<<'\n';
std::cerr<<"name"<<'\n';
		for (auto& i : name_vec_)
			for (auto& j : i )
			std::cerr<<j<<'\n';
std::cerr<<"uid"<<'\n';
		for (auto& j : uid_vec_)
			std::cerr<<j<<'\n';
std::cerr<<"upload_url"<<'\n';
		for (auto& i : upload_url_vec_)
			for (auto& j : i )
				std::cerr<<j<<'\n';
	}

	void change_name (const std::string& amend)
	{
		for ( auto& i : name_vec_)
		{
			for (auto& j : i)
				j += amend;	 
		}
	}
};
#endif

