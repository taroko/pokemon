#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

#include "raw_bed.hpp"
#include "sam.hpp"
#include "../file_reader_impl.hpp"

int main (void)
{
    typedef std::tuple <
        std::string, //QNAME
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
        int, // NH
        std::string //TailSeq
                        > TUPLETYPE;

	std::string file_name ("../../unit_test/test.sam");
	std::ifstream file_handle (file_name);
	std::vector<std::string> QQ ({file_name});
	FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);

	size_t index=0;
	std::vector <Sam <TUPLETYPE> > AAA;
	std::vector < RawBed<> > BBB;
	std::map < RawBed<>, int > CCC;
	std::map < RawBed<>, uint16_t > DDD;

	const size_t RawBedSize = sizeof (RawBed<>);
	while (index!=5)
	{
		const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
//		BBB.push_back (RawBed <>(qq));
		RawBed <> yy (qq);
//		RawBed <>* Q = &yy;
//		std::cerr<<"test "<< sizeof (*Q)<<std::endl;
		++CCC[yy];
//		++CCC[RawBed <>(qq)];

		//uint16_t* ptr = 
		++ ( *( ( &( DDD[RawBed<>(qq)]) ) - 5 ) );	
//		uint16_t* ptr = (uint16_t*) ( &( DDD[RawBed<>(qq)]) );	
//		++(*(ptr-5));
		AAA.push_back (qq);
		++index;
	}

	for (auto itr=DDD.begin(); itr!=DDD.end(); ++itr)
		std::cerr<<(itr->first).reads_count_<<'\n';
	
	for (auto itr=CCC.begin(); itr!=CCC.end(); ++itr)
		std::cerr<<itr->second<<'\n';

};
	

//	std::sort (BBB.begin(), BBB.end());

	//for (auto i=0; i!=4; ++i)
	//{
	//	bool b1 = BBB[i] < BBB[i+1], b2 = BBB[i+1] < BBB[i];
	//	std::cerr<<"< overload "<<i<<"<"<<i+1<<" & "<<i+1<<"<"<<i<<'\t'<<b1<<'\t'<<b2<<'\n';
	//}
	//for (auto itr=CCC.begin(); itr!=CCC.end(); ++itr)
	//	std::cerr<<itr->second<<'\n';

