///@file Wig_reader_impl.hpp
///@brief Specialized version of class FileReader_impl for Wig, for providing operation detail for getting a piece of Wig data from an ifstream object
///@author C-Salt Corp.
#ifndef WIG_READER_IMPL_HPP_
#define WIG_READER_IMPL_HPP_
#include <cstdint>
#include <vector>
#include <fstream>
#include <boost/algorithm/string/split.hpp>
#include "wig.hpp"
#include "../tuple_utility.hpp"
#include "../wrapper_tuple_utility.hpp"
#include "../file_reader_impl.hpp"
#include <boost/algorithm/string.hpp>
/// @brief Read Wig format data, including member functions: io_end (); get_next_entry (); get_all_entry (); read_Bigwig(); write_Bigwig()
/// @tparam Wig template template parameter Wig indicating current FileReader_impl is a specilization form of the base FileReader_impl template class for Wig format
/// @tparam TUPLETYPE indicating Wig data format, defaulted as tuple < string, string, int, vector < tuple < uint32_t, double > > >
//Specialization for WIG
template <typename TUPLETYPE>
class FileReader_impl < Wig, TUPLETYPE >
{
public:
///@typedef io_iterator in format of formatIoIterator < Wig <TUPLETYPE>, FileReader_impl <Wig, TUPLETYPE> > 
	typedef FormatIoIterator<
						Wig <TUPLETYPE>,
						FileReader_impl < Wig, TUPLETYPE >
				   		> io_iterator;   // using specialized FormatIoIterator as iterator
/// @memberof FileReader_impl<Wig, TUPLETYPE>
/// @brief return a pointer pointing to a Wig <TUPLETYPE> with its eof_flag setted as true
/// @return shared_ptr < Wig <TUPLETYPE> > (new Wig <TUPLETYPE> (true) )
	io_iterator io_end ()
	{
		return io_iterator (std::shared_ptr < Wig <TUPLETYPE> > (new Wig <TUPLETYPE> (true) ) );
	};

/// @memberof FileReader_impl<Wig, TUPLETYPE>
/// @brief return an object of Wig <TUPLETYPE> by means of reading an std::ifstream& object
/// @param file_handle an std::ifstream& object 
/// @return Wig <TUPLETYPE> 
//template<class STRING = std::string>
//typedef STRING defaulted as std::string, could also implemented by iterator_range
	static Wig<TUPLETYPE> get_next_entry (std::ifstream& file_handle)
	{
		std::string line_temp, span("1"), chrom;
		std::string variable_string, fix_string;
		uint32_t start=0;
		int index = 0, step = 0;
		int data_count = 0;
		std::vector <std::string> parse_vector;
		Wig <TUPLETYPE> result;
//		std::ifstream file_handle(file_name);
/// @brief The if (file_handle.fail() || file_handle.eof() || file_handle.bed() ) function detects ifstream fail/eof/bad flags, and return Wig <TUPLETYPE> with enabled eof_flag when any one of the aformentioned flags indicates true \n Whenever the returned Wig <TUPLETYPE> has an enabled elf_flag, it is indicated that at least one of fail/eof/bad/ situations occurs
		if ( file_handle.fail() || file_handle.eof() || file_handle.bad() ) //fail
        // The if (file_handle.fail() || file_handle.eof() || file_handle.bed() ) function detects ifstream fail/eof/bad flags, and 
        // return Wig <TUPLETYPE> with enabled eof_flag when any one of the aformentioned flags indicates true. 
        // Whenever the returned Wig <TUPLETYPE> has an enabled eof_flag, it is indicated that at least one of fail/eof/bad/ situations occurs
		{
			result.eof_flag = true;
			return result;
		}
		while (std::getline (file_handle, line_temp) )
		{
			boost::algorithm::trim (line_temp);
			if (line_temp == "")
				continue;//	break;
			else if (line_temp.substr (0,5) == "track")
			{
				if ( index == 0) //indicating currently get a first track line
					parse_vector.push_back (line_temp);//track_info);
				else 
					break;
			}
			else if ( (line_temp.substr (0,9) == "fixedStep" ) || ( line_temp.substr (0,12) == "variableStep" ) )
			{
				if ( index == 0) //indicating currently get a first track line
				{
					std::vector <std::string> split_temp1, split_temp2;
					boost::split (split_temp1, line_temp, boost::is_any_of (" ") );
					for ( auto itr = split_temp1.begin(); itr != split_temp1.end(); ++itr ) 
					{
						boost::split (split_temp2, *itr, boost::is_any_of ("=") );
						if (split_temp2 [0]  == "chrom")
						{
							chrom = split_temp2 [1];
							if (parse_vector.size()==0)		//check for no track line situation, occurring in bigwigTowig conversion
							{											
								parse_vector.push_back ("track");
								parse_vector.push_back (split_temp2 [1]); //chrom);
							}
							else							//normal situation, track_line has been pushed_back
								parse_vector.push_back (split_temp2 [1]); //chrom);
						}
						else if (split_temp2 [0] == "span")
						{
							span = split_temp2 [1];
							parse_vector.push_back (split_temp2 [1]);
						}
						else if (split_temp2 [0] == "start")
							start = boost::lexical_cast <uint32_t> (split_temp2 [1]);	//fixedstep only, need to be parsed to obtain our start_info format
						else if ( split_temp2 [0] =="step")
							step = boost::lexical_cast <int> (split_temp2 [1] );	//fixedstep only, need to be parsed to obtain our start_info format
						else 
							;
					}
					if (parse_vector.size() == 2)
						parse_vector.push_back (span);
				}
				else if (index != 0) // meeting next declaration line 
					break;
				else
					;
			}
			else 
			{
 				if (step == 0)		//indicating variableStep
				{
					std::vector <std::string> split_temp3;
					boost::split (split_temp3, line_temp, boost::is_any_of (" \t") ); 
					variable_string += '*';
					variable_string.append (split_temp3 [0]);	//start 
					variable_string += '#';
					variable_string.append (split_temp3 [1]);	//value
				}
				else	//indicating fixedStep
				{
					fix_string += '*';
					std::string start_parsed = boost::lexical_cast <std::string> (start + (data_count * step));	//parsing start info for fixedstep format
					fix_string.append (start_parsed);
					fix_string += '#';
					fix_string.append (line_temp); 
					++data_count;
				}
				++index; 
			}
		}
		if (data_count == 0)	//indicating variable string
			parse_vector.push_back (variable_string);
		else	//indicating fix string
			parse_vector.push_back (fix_string);
		TupleUtility <TUPLETYPE, 4>::FillTuple (result.data, parse_vector);
		file_handle.seekg( static_cast<long long int>(file_handle.tellg()) - static_cast<long long int>(line_temp.length()) -1 );
		return result;
	};

/// @memberof FileReader_impl<Wig, TUPLETYPE>
/// @brief receive a std::string of file_name, and accordingly read all Wig <TUPLETYPE> entries thereof and update the call by reference parameter of data_cluster to record the got Wig <TUPLETYPE> data entries
/// @param file_name, in format of std::string 
/// @param std::vector< Wig <TUPLETYPE> >, passed into get_all_entry function by reference, so as to keep the got Wig<TUPLETYPE> data entries
	static void get_all_entry (std::string& file_name, std::vector< Wig<TUPLETYPE> >& data_cluster)
	{
		std::ifstream file_handle (file_name);
		while (true)
		{
			auto x = get_next_entry ( file_handle );
			if (x.eof_flag)
				break;
			data_cluster.push_back (x);
		}
	}

/// @memberof FileReader_impl<Wig, TUPLETYPE>
/// @brief return a char* of outFile name, wherein the outFile storges wig format data converted from inFile
/// @param inFile in char* format, indicating to be read bigwig file
/// @param outFile in char* format, indicating converted wig file
/// @return char* == outFile
	static char* read_bigwig (char* inFile, char* outFile)
	{
		std::string s0 = "/Users/obigbando/bin/x86_64/bigWigToWig ";
		s0 = s0 + inFile + " " + outFile;
		system ( s0.c_str() );
		return outFile;
	};

/// @memberof FileReader_impl<Wig, TUPLETYPE>
/// @brief return a string of file name, wherein the outName storges bigwig format data converted from inName
/// @param inName in char* format, indicating to be read wig file
/// @param outName in char* format, indicating converted bigwig file
/// @return std::string == outName
	static char* write_bigwig (char* inName, char* chromSizes, char* outName)
	{
		std::string s0 = "/Users/obigbando/bin/x86_64/wigToBigWig ";
		s0 = s0 + inName + " " + chromSizes + " " + outName;
		system ( s0.c_str() );
		return outName;
	};
};

#endif
