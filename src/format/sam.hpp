/// @file sam.hpp
/// @brief Provide definition for Sam Format
#ifndef SAM_HPP_
#define SAM_HPP_
#include <tuple>
#include <string>
#include <cstdint>
#include <vector>
#include <bitset>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/mpl/string.hpp>
#include <boost/mpl/push_front.hpp>
#include <boost/tuple/tuple.hpp> 
#include <boost/preprocessor/repetition/enum_params.hpp> 
#include <boost/mpl/copy.hpp> 
#include <boost/algorithm/string/detail/trim.hpp>

#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "sam_tags.hpp"
#include "is_tuple_type.hpp"


/// @brief 即將刪除
struct UserDefineContent
{
    int nh_count_;
    std::string tail_seq_;

    UserDefineContent (void)
        : nh_count_(0), tail_seq_("")
    {
    	//std::string aa = "TL:Z:TG\tNH";
		//UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> > ll(aa);
		//std::cout << std::get<0>(ll) << std::get<1>(ll) << std::endl;
    }
  
	UserDefineContent ( int nhin, std::string tailseq )
        : nh_count_(nhin), tail_seq_ (tailseq)
    {}

//    friend std::ostream& operator << (std::ostream& out, const UserDefineContent& cc)
//	std::ostream& operator() (std::ostream& out, const UserDefineContent& cc)
	void str (std::string& out)
    {
		out+= ("NH:i:"+ std::to_string(nh_count_));
		if (! tail_seq_.empty() )//(!sam._tailSeq.empty ())
			out += ("\tTL:Z:"+tail_seq_);
//		out << '\n';
//		return out;
    }
};




/**
 * @struct Sam
 * @brief This is a Sam struct, including member elements:
 *  static type (enum format_type);
 *  data (TUPLETYPE);
 *  eof_flag (bool)
 * This struct can store the Sam format data, provide constructor for move and copy. We also overload the operator<< for print the fasta's data.\n
 */ 
/// @tparam TUPLETYPE defaulted as tuple < string, int, string, uint32_t, int, string, string, uint32_t, int, string >,indicate type of member element: data, \n
template < class TUPLETYPE =
			std::tuple <
//						std::string, //header
						std::string, //QNAME
						int, //SAM_FLAG, //FLAG
						std::string, //RNAME
						uint64_t, //POS
						int, //MAPQ
						std::string, //CIGAR
						std::string, //RNEXT
						uint64_t, //PNEXT
						int64_t, //TLEN
						std::string, //SEQ
						std::string, //QUAL
						UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
						//UserDefineContent
//						int, // NH
//						std::string //TailSeq
						>
//		, class UDT_HANDLE = UserDefineContent
		 >
struct Sam
{
	static_assert( IsTupleType<TUPLETYPE>::value == true, "ARGUMENT IS NOT A TUPLE");
	TUPLETYPE data;//_f, data_b;


	static const format_types type = format_types::SAM;
	bool eof_flag;
	typedef TUPLETYPE TupleType;
	//default constructor
	Sam ()	//empty construct - can't pass unit_test
		: data ( TUPLETYPE() ), eof_flag (false) //_f ( TUPLETYPE() ), data_b ( TUPLETYPE() ), eof_flag (false)
	{}
	//Copy constructor
	Sam (const Sam<>& Sm)	
		: data (Sm.data), eof_flag (Sm.eof_flag)
	{}
	//Assignment operator
	Sam<>& operator=( const Sam<>& other)
	{
//		static_assert( is_tuple_type<decltype(other.data)>::value == true, "assignment's argument is not a tuple");
		eof_flag = other.eof_flag;
		data = other.data;
		return *this;
	}
	Sam (TUPLETYPE &data_in)
		: data (data_in), eof_flag (false) 
	{}
	Sam (TUPLETYPE &&data_in) 
		: data (std::move (data_in)), eof_flag (false) 
	{}
	//Move constructor
	Sam (Sam<> && other) 
		: data (std::move(other.data)), eof_flag (std::move(other.eof_flag))
	{}
	//Move assignment operator
	Sam<>& operator= (Sam<> && other)
	{
		data = std::move(other.data);
		eof_flag = std::move (other.eof_flag);
		return *this;
	}
	Sam (bool EofFlag) 
		: data ( TUPLETYPE() ),  eof_flag (EofFlag) 
	{}
	//String (copy) constructor
	Sam(std::string &line)
		:data( TUPLETYPE() ), eof_flag (false)
	{
		line2sam(line, data);
	}
	//String (move) constructor
	Sam(std::string &&line)
		:data( TUPLETYPE() ), eof_flag (false)
	{
		line2sam(line, data);
	}
	
	void line2sam(std::string & line, TUPLETYPE & tuple)
	{
		std::vector<std::string> tmp_splits;
		boost::split( tmp_splits, line, boost::is_any_of( "\t" ));

		tuple = 
		std::move(
			std::make_tuple
			(
				boost::lexical_cast< std::string > (tmp_splits[0]), //QNAME
	            boost::lexical_cast< int > (tmp_splits[1]), //SAM_FLAG, //FLAG
	            boost::lexical_cast< std::string > (tmp_splits[2]), //RNAME
	            boost::lexical_cast< uint64_t > (tmp_splits[3]), //POS
	            boost::lexical_cast< int > (tmp_splits[4]), //MAPQ
	            boost::lexical_cast< std::string > (tmp_splits[5]), //CIGAR
	            boost::lexical_cast< std::string > (tmp_splits[6]), //RNEXT
	            boost::lexical_cast< uint64_t > (tmp_splits[7]), //PNEXT
	            boost::lexical_cast< int64_t > (tmp_splits[8]), //TLEN
	            boost::lexical_cast< std::string > (tmp_splits[9]), //SEQ
	            boost::lexical_cast< std::string > (tmp_splits[10]), //QUAL
				UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >(tmp_splits.begin()+11, tmp_splits.end())
			)
		
		);
		/*
		UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> > bbb(tmp_splits.begin()+11, tmp_splits.end()), ccc;
		ccc = bbb;
		std::cout <<  "ssstrV " << tmp_splits[11] << " " << tmp_splits.size() << std::endl;
		std::cout <<  "AA " << std::get<0>( std::get<11>(tuple) ) << " " << std::get<1>( std::get<11>(tuple) ) << std::endl;
		std::cout <<  "BB " << std::get<0>( bbb ) << " " << std::get<1>( bbb ) << std::endl;
		std::cout <<  "CC " << std::get<0>( bbb ) << " " << std::get<1>( bbb ) << std::endl;
		*/
	}
	
	friend std::ostream& operator<< (std::ostream& out, Sam& s)
	{
				
		std::string result;
		TupleUtility< TupleType , 11 >//std::tuple_size< TupleType >::value >
			::PrintTuple(s.data, result, 1);	//calling the overload version for tab delimited data structure
		//result.resize (result.size()-1);
		(std::get<11> (s.data)).str (result);//UserDefineContent 
		result+='\n';
		out << result;
		return out;
	}

	std::string str()
	{
//		std::string result;
//		TupleUtility< TupleType , std::tuple_size< TupleType >::value >::PrintTuple(data, result, 1);	//calling the overload version for tab delimited data structure
//		result.resize (result.size()-1);

		std::string result;
		TupleUtility< TupleType , 11 >//std::tuple_size< TupleType >::value >
			::PrintTuple(data, result, 1);	//calling the overload version for tab delimited data structure
		(std::get<11> (data)).str (result);//UserDefineContent 

//		result+='\n';	//result in an extra \n when we do bam -> sam conversion using sametool. samtool will automatically add an \n (speculation only)
		return result;
	}

//	friend std::ostream& operator<< (std::ostream& os, const Sam& sam) 
//	{
//		os << sam.QNAME << '\t'
//			<< sam.FLAG.to_ulong() << '\t'
//			<< sam.RNAME << '\t'
//			<< sam.POS << '\t'
//			<< sam.MAPQ << '\t'
//			<< sam.CIGAR << '\t'
//			<< sam.RNEXT << '\t'
//			<< sam.PNEXT << '\t'
//			<< sam.TLEN << '\t'
//			<< sam.SEQ << '\t'
//			<< sam.QUAL << '\t'
//			<< "NH:i:" << sam._NH;
//		if (!sam._tailSeq.empty ())
//			os << "\tTL:Z:" << sam._tailSeq;
//		os << '\n';
//		return os;
//	}


	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version)
	{
		TupleUtility < TUPLETYPE, std::tuple_size<TUPLETYPE>::value >
			:: SerializeTuple (data, ar, version);
		ar & eof_flag;
	}
};
#endif
