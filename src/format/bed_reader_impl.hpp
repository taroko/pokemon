///@file bed_reader_impl.hpp
///@brief Specialized version of class FileReader_impl for Bed, for provide operation detail for getting a piece of Bed data from an ifstream object
///@author C-Salt Corp.
#ifndef BED_READER_IMPL_HPP_
#define BED_READER_IMPL_HPP_
#include <boost/algorithm/string/split.hpp>
#include <fstream>
#include "bed.hpp"
#include "../tuple_utility.hpp"
#include "../file_reader_impl.hpp"
#include "boost/algorithm/string/trim.hpp"
/// @brief Read Bed format data, including member functions: io_end (); get_next_entry (); get_all_entry (); read_Bigbed(); write_Bigbed()
/// @tparam Bed template template parameter Bed indicating current FileReader_impl is a specilization form of the base FileReader_impl template class for Bed format
/// @tparam TUPLETYPE indicating Bed data format, defaulted as tuple < string, uint32_t, uint32_t >
//Specialization for BED


template< class TUPLETYPE, SOURCE_TYPE STYPE>//Specialization for FASTQ
class FileReader_impl <Bed, TUPLETYPE, STYPE>
    : public DataSource < STYPE >
{
    public:
        typedef Bed<TUPLETYPE> format_type;
        typedef TUPLETYPE tuple_type;
        typedef SOURCE_TYPE source_type;
};


template <typename TUPLETYPE>
//class FileReader_impl < Bed, TUPLETYPE >
class FileReader_impl < Bed, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE > //edit by jones
    : public DataSource < SOURCE_TYPE::IFSTREAM_TYPE >
{
public:
    size_t file_num_;
    typedef Bed<TUPLETYPE> format_type;
	typedef TUPLETYPE TupleType;
///@typedef io_iterator in format of formatIoIterator < Bed <TUPLETYPE>, FileReader_impl <Bed, TUPLETYPE> > 
	typedef FormatIoIterator< 
						Bed <TUPLETYPE>, 
						FileReader_impl < Bed, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >//, SOURCE_TYPE::IFSTREAM_TYPE > 
//					   SOURCE_TYPE::IFSTREAM_TYPE //edit by jones
					   > io_iterator;	/// using specialized FormatIoIterator as iterator
/// @memberof FileReader_impl<Bed, TUPLETYPE>
/// @brief return a pointer pointing to a Bed <TUPLETYPE> with its eof_flag setted as true
/// @return shared_ptr < Bed <TUPLETYPE> > (new Bed <TUPLETYPE> (true) )
	io_iterator io_end ()
	{
		return io_iterator (std::shared_ptr < Bed <TUPLETYPE> > (new Bed <TUPLETYPE> (true) ) );
	}

    FileReader_impl ()
        : DataSource < SOURCE_TYPE::IFSTREAM_TYPE > ()
        , file_num_ (0)
    {}

    FileReader_impl (std::vector<std::string>& file_path, std::vector <uint64_t> sizein = std::vector<uint64_t>(0) )
        : DataSource < SOURCE_TYPE::IFSTREAM_TYPE > (file_path, sizein)
        , file_num_ ( file_path.size() )
    {}

/// @memberof FileReader_impl<Bed, TUPLETYPE>
/// @brief return an object of Bed <TUPLETYPE> by means of reading an std::ifstream& object; \n
/// @param file_handle an std::ifstream& object 
/// @return Bed <TUPLETYPE> 
	template<class STRING = std::string>
//	typedef std::string STRING; //typedef STRING defaulted as std::string, could also implemented by iterator_range
	Bed<TUPLETYPE> get_next_entry (size_t index)//(std::ifstream& file_handle)
	{
		std::string line_temp;
 		bool return_flag = false;
		Bed<TUPLETYPE> result_element;
		this -> file_handle [index] -> peek();
		if ( (this -> file_handle[index]) -> fail() || (this -> file_handle[index]) -> eof() || (this -> file_handle[index]) -> bad()  ) //fail
		{
			result_element.eof_flag = true;
			return result_element;
		}
		std::getline ( *(this -> file_handle[index]), line_temp);
		boost::trim (line_temp);
		std::vector < STRING > split_temp;
		boost::split (split_temp, line_temp, boost::is_any_of ("\t") );
		TupleUtility< TUPLETYPE, std::tuple_size<TUPLETYPE>::value >:: FillTuple(result_element.data, split_temp);
		return result_element;
	}

/// @memberof FileReader_impl<Bed, TUPLETYPE>
/// @brief receive a std::string of file_name, and accordingly read all Bed <TUPLETYPE> entries thereof and update the call by reference parameter of data_cluster to record the got Bed <TUPLETYPE> data entries
/// @param file_name in format of std::string 
/// @param data_cluster passed into get_all_entry function by reference, so as to keep the got Bed <TUPLETYPE> data entries
/*
	static void get_all_entry (std::string& file_name, std::vector< Bed<TUPLETYPE> >& data_cluster )
	{
		std::ifstream file_handle (file_name);
		while ( true )
		{
			auto x = get_next_entry (file_handle);
			if (x.eof_flag == 1)
				break;
			data_cluster.push_back ( x );
		}
	}
*/

/// @memberof FileReader_impl<Bed, TUPLETYPE>
/// @brief return a string of outFile name, wherein the outFile storges Bed format data converted from inFile
/// @param inFile in char* format, indicating to be read Bigbed file
/// @param outFile in char* format, indicating converted Bed file
/// @return char* == outFile
/*
	static std::string read_Bigbed (char *inFile, char *outFile) 
	{
		std::string s0 = "/Users/obigbando/bin/x86_64/bigBedToBed ";
		s0 = s0 + inFile + ' ' + outFile;
		system (s0.c_str() );
		return outFile; 
	}
*/

/// @memberof FileReader_impl<Bed, TUPLETYPE>
/// @brief return a string of file name, wherein the outName storges Bigbed format data converted from inName
/// @param inName in char* format, indicating to be read Bed file
/// @param chromSizes providing chrom size info
/// @param outName in char* format, indicating converted Bigbed file
/// @return std::string == outName
/*
 	static std::string write_Bigbed ( char *inName, char *chromSizes, char *outName )
	{
		std::string s0 = "/Users/obigbando/bin/x86_64/bedToBigBed ";
		s0 = s0 + inName + ' ' + chromSizes + ' ' + outName;
		system ( s0.c_str() );
		return outName;
	}
*/
};

#endif
