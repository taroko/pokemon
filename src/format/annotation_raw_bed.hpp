/**
 *  @file annotation_raw_bed.hpp
 *  @brief provide the data structure keeping track of post alignment information, which is derived from raw_bed, but with extra member variables of chromosome (std::string), strand (char), end position (uint32_t), and annotation informatoin (vector<string>).
 *  @author C-Salt Corp.
 */
#ifndef ANNOTATION_RAW_BED_HPP_
#define ANNOTATION_RAW_BED_HPP_
#include "../constant_def.hpp"
#include "raw_bed.hpp"

#include "../pipeline/pipeline_preparator.hpp"

//std::map <std::string, std::string> g_genometable ({{ std::string ("chrX"), std::string (500000000, 'A')} ,});

/**
 * @struct AnnotationRawBed
 * @brief provide a structure keeping track of post annotation information.  It is derived from RawBed, but with some extra member variables such as chromosome (std::string), strand (char), end position (uint32_t), and annotation informatoin (vector<string>)
 * @tparam TUPLETYPE defaulted as tuple < string, int, string, uint64_t, int, string, string, uint64_t, int64_t, string, std::string, int, std::string >,indicate type of the corresponding sam data
 */
template <typename TUPLETYPE
			= std::tuple <
                        std::string, //QNAME
                        int, //SAM_FLAG, //FLAG
                        std::string, //RNAME
                        uint64_t, //POS
                        int, //MAPQ
                        std::string, //CIGAR
                        std::string, //RNEXT
                        uint64_t, //PNEXT
                        int64_t, //TLEN
                        std::string, //SEQ
                        std::string, //QUAL
                        UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
                        //UserDefineContent
                        >
		>
struct AnnotationRawBed
	: public RawBed <TUPLETYPE>
{
/** 
 * @brief used to represent the strand information
 */
	char strand_;
/** 
 * @brief used to represent the end information of the read
 */
	uint32_t end_;
/** 
 * @brief used to represent the chromosome information of the read
 */
	std::string chromosome_;
/** 
 * @brief used to represent the annotation information of the read
 */
	std::vector< std::vector <std::string> > annotation_info_;

/**
 * @brief used to indiate whether the current AnnotationRawBed data have been filtered, indicating value of 1, or not, indicating value of 0
 */
	size_t is_filtered_;

/** 
 * @brief Default constructor
 */
	AnnotationRawBed (void)
		: RawBed<TUPLETYPE>()
	{}

/** 
 * @brief constructor with input RawBed<TUPLETYPE> data
 */
	AnnotationRawBed (const RawBed<TUPLETYPE>& rawin)//(const Sam<TUPLETYPE>& samin)
		: RawBed<TUPLETYPE>(rawin)
		, strand_ (this->GetStrand (this->chr_idx_))
		, end_ (this->start_ + this->length_)
		, chromosome_ (this->GetChr (this->chr_idx_) )//( this->chr_prefix_ + (GetChr (this->chr_idx_) ) )
		, annotation_info_ (0)
		, is_filtered_ (0)
	{}

/** 
 * @brief operator<< overload
 */
    friend std::ostream& operator<< (std::ostream& out, const AnnotationRawBed<TUPLETYPE>& target)
    {
//		out << target.GetChr (target.chr_idx_) <<'\t'<< target.GetStrand (target.chr_idx_) <<'\t';
		out << target.chromosome_ <<'\t'<< target.GetStrand (target.chr_idx_) <<'\t';
        out<<target.start_                      <<'\t'
        <<target.length_                        <<'\t'
        <<(int)target.tail_length_              <<'\t'
        <<target.multiple_alignment_site_count_ <<'\t'
        <<target.reads_count_                   <<'\t'
        <<target.tail_mismatch_                 <<'\t'
        <<"is_filtered_ " << target.is_filtered_<<'\n';
		out << "annotation content " << target.strand_ << '\t'<<target.end_<<'\t'<<target.chromosome_<<'\n';
		size_t ii=0, jj=0;
		for (auto& q : target.annotation_info_)
		{
			out<<"level 1, db id # "<< ii << "\tAnno size " << q.size() <<'\n';
			for (auto& qq : q)
			{
				out<<"level 2, db_depth # "<<jj<<'\t';
				out<<qq<<'\t';
				++jj;
			}
			out<<'\n';
			++ii;
		}
        return out;
	}

	inline std::string ToSam (void)
	{
		if( this->start_+this->length_ > (PipelinePreparator<>::gGenometable_[chromosome_]).size())
			return "";
		std::stringstream out;
		//having rawbed with multiple read counts recovered with 
		//its corresponding read counts
		for (auto repeat=0; repeat!=this->reads_count_; ++repeat) 
		{
			//parse columns[1-6]
			if (strand_=='+')
				out <<"rd"<<this->start_<< "_" << repeat <<'\t'<<"0"<<'\t'<<chromosome_<<'\t'
					<< this->start_<<'\t'<<"0"<<'\t'<<(int)(this->length_-this->tail_length_)<<"M"<<(int)this->tail_length_<<"S"<<'\t';
			else if (strand_=='-')
				out <<"rd"<<this->start_<< "_" << repeat <<'\t'<<"16"<<'\t'<<chromosome_<<'\t'
					<< this->start_<<'\t'<<"0"<<'\t'<<(int)this->tail_length_<<"S"<<(int)(this->length_-this->tail_length_)<<"M"<<'\t';		
			//parse columns[7-10]
			out <<"*"<<'\t'<<"0"<<'\t'<<"0"<<'\t'<< getSamSeq()<<'\t'<<"*"<<'\t';
			//parse column[12]
			out << "NH:i:"<< this->multiple_alignment_site_count_;
			if (this->tail_length_!=0)
	        	out << "\tTL:Z:" << getTail() <<'\n';
			else
				out << '\n';
		}
		std::string tmp = out.str();
		tmp.pop_back();
		return tmp;
	}

	inline std::string getReadSeq()
	{
		std::string read_seq = (PipelinePreparator<>::gGenometable_[chromosome_]).substr (this->start_-1, this->length_ - this->tail_length_);//start_ is recorded as 1 base
		std::string tail = getTail();
		
		std::transform(read_seq.begin(), read_seq.end(), read_seq.begin(), ::toupper );
		
		if(strand_ == '-')
		{
			std::transform(read_seq.begin(), read_seq.end(), read_seq.begin(), [this](char c){return this->complement(c);} );
			std::reverse(read_seq.begin(), read_seq.end());
		}
		read_seq += getTail();
		return std::move(read_seq);
	}

	inline std::string getSamSeq()
	{
		std::string read_seq = (PipelinePreparator<>::gGenometable_[chromosome_]).substr (this->start_-1, this->length_ - this->tail_length_);//start_ is recorded as 1 base
		std::string tail = getTail();
		
		std::transform(read_seq.begin(), read_seq.end(), read_seq.begin(), ::toupper );
		
		if(strand_ == '-')
		{
			std::transform(tail.begin(), tail.end(), tail.begin(), [this](char c){return this->complement(c);} );
			std::reverse(tail.begin(), tail.end());
			read_seq = tail + read_seq;
		}
		else
			read_seq += getTail();
		return std::move(read_seq);
	}

	inline std::string getTail(void)
	{
		std::string tail = "";
		if (this->tail_length_!=0)
		{
        	std::map <int, char> mtable ({ {0,'A'}, {1,'C'}, {2,'G'}, {3,'T'} });
	        for (auto index=this->tail_length_-1; index>=0; --index)
			{
    	        auto kk = (this->tail_mismatch_ >> (2*index) ) % 4;	
    	        tail.push_back ( mtable [(int)kk] );	
			}
		}
		return std::move(tail);
	}

	inline char complement(char c)
	{
		switch (c) {
			case 'A': c = 'T'; break;
			case 'T': c = 'A'; break;
			case 'C': c = 'G'; break;
			case 'G': c = 'C'; break;
			default :
				std::cerr << "Error this char is not A,T,C,G" << std::endl;
				exit(1);
		}
		return c;
	}
};

#endif
