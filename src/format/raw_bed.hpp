/**
 *  @file raw_bed.hpp
 *  @brief provide the data structure keeping track of post alignment information 
 *  @author C-Salt Corp.
 */
#ifndef RAW_BED_HPP_
#define RAW_BED_HPP_
#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <tuple>
#include "boost/lexical_cast.hpp"
#include "../constant_def.hpp"
#include "sam.hpp"
#include "boost/multiprecision/cpp_int.hpp"
/**
 * @struct RawBed
 * @brief provide a structure keeping track of the minimum information, with sizeof 16 bytes, for a piece of sam data
 * @tparam TUPLETYPE defaulted as tuple < string, int, string, uint64_t, int, string, string, uint64_t, int64_t, string, std::string, int, std::string >,indicate type of the corresponding sam data
 */
template <typename TUPLETYPE
			= std::tuple <
                        std::string, //QNAME
                        int, //SAM_FLAG, //FLAG
                        std::string, //RNAME
                        uint64_t, //POS
                        int, //MAPQ
                        std::string, //CIGAR
                        std::string, //RNEXT
                        uint64_t, //PNEXT
                        int64_t, //TLEN
                        std::string, //SEQ
                        std::string, //QUAL
                        UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
                        //UserDefineContent
                        >
		>
struct RawBed
{


	static std::map <std::string, uint8_t> gChrRefMap_;
/** 
 * @brief used to record chromosome prefix, e.g. "chr"
 */
	static const char* chr_prefix_;// = "chr";
/** 
 * @brief used to record a conversion table to do A/C/G/T -> 00/01/10/11 conversion
 */
	static const std::array <int, 256> mtable_;	//used to do A/C/G/T -> 00/01/10/11 conversion
/** 
 * @brief used to record a coded char to identify chromosome and strand information of the read.  1-32 正股, 33-64 反股, 大寫正股, 小寫反股，沒有正反股以大寫為主
 */
	//char chr_idx_; // chr_idx = 1, //1-32 正股, 33-64 反股, 大寫正股, 小寫反股，沒有正反股以大寫為主
	uint8_t chr_idx_;	//coding according to gChromosome_map_: 1~127 forward strand; 128~255 backward strand
/** 
 * @brief used to record tail sequence length
 */
	uint8_t tail_length_; //1B
/** 
 * @brief used to record sequence length
 */
	uint16_t length_; //2B
/** 
 * @brief used to record multiple alignment sites of the read
 */
	uint16_t multiple_alignment_site_count_; //2B, i.e. the k value indicating how many alignment sites that the read is corresponding to 
	
	uint16_t tail_dummy_; //2B could be used for mismatch information in the future
	
/** 
 * @brief used to record how many times an identical read has been received.  This value will be updated whenever an identical read has been inserted
 */
	uint32_t reads_count_; //4B, i.e. the n value indicating how many input fastq read shared the same sequence 
/** 
 * @brief used to record start position of the read
 */
	uint32_t start_; //4B
/** 
 * @brief used to record encoded tail_mismatch_ sequence
 */
	
//	uint64_t tail_mismatch_; //8B
	boost::multiprecision::uint128_t tail_mismatch_;
	

/** 
 * @brief Default constructor
 */
	RawBed (void)
	{}

/** 
 * @brief Constructor based on input Sam<TUPLETYPE>
 */
	RawBed (const Sam<TUPLETYPE>& samin)
		: chr_idx_ (GetChr (std::get<1>(samin.data), std::get<2>(samin.data) ))
		, start_ ( std::get<3> (samin.data) )
		, length_ ( std::get<9> (samin.data).size() )
		, tail_length_ ( (uint8_t)( std::get<1>( std::get<11>(samin.data) ).size() ) )
		, multiple_alignment_site_count_ ( std::get<0>( std::get<11>(samin.data) ) )
		, reads_count_(0)
		, tail_mismatch_( ParseTailInfo ( std::get<1>( std::get<11>(samin.data) ) ) )
	{}

	uint8_t GetChr (int flag, const std::string& chrstr) const
	{
//		std::cerr<<"flag : chrstr : chr_idx_ "<<flag<<'\t'<<chrstr<<'\t'<<(int)chr_idx_<<'\n';
		if (flag==0)
		{
			uint8_t temp = gChrRefMap_[chrstr];
//			std::cerr<<(int)temp<<'\n';
			return temp;
		}
		else
		{
			uint8_t temp = (uint8_t)((int) gChrRefMap_[chrstr]+128) ;
//			std::cerr<<(int)temp<<'\n';
			return temp;
		}
	}

	std::string GetChr (uint8_t chr_idx) const
	{
		auto itr = gChrRefMap_.begin();
		if (chr_idx<128)
			std::advance (itr, chr_idx_);
		else 
			std::advance (itr, chr_idx_-128);
		return itr->first;
	}

    char GetStrand (uint8_t chr_idx) const
    {                                                                                                                                                                                     
        if (chr_idx < 128)
            return '+';
        else
            return '-';
	}
/** 
 * @brief Constructor based on input RawBed<TUPLETYPE>
 */
    RawBed (const RawBed<TUPLETYPE>& rawin)
		: chr_idx_ ( rawin.chr_idx_)
		, start_ ( rawin.start_)
		, length_ ( rawin.length_)
		, tail_length_ ( rawin.tail_length_)
		, multiple_alignment_site_count_ (rawin.multiple_alignment_site_count_)
		, reads_count_(rawin.reads_count_)
		, tail_mismatch_( rawin.tail_mismatch_)
	{}

/** 
 * @brief operator< overload to facilitate sorting of RawBed format
 */
	bool operator< (const RawBed<TUPLETYPE>& input) const
	{
/*		int temp1 = (int)chr_idx_, temp2 = (int)input.chr_idx_;
		if ( (97<=chr_idx_ && chr_idx_<=122) || 
			 (33<=chr_idx_ && chr_idx_<=64) )
			temp1-=32;
		if ( (97<=input.chr_idx_ && input.chr_idx_<=122) || 
			 (33<=input.chr_idx_ && input.chr_idx_<=64) )
			temp2-=32;
		
		//if(temp1 < 10)
		//	temp1 = temp1*10 - 1;
		//if(temp2 < 10)
		//	temp2 = temp2*10 - 1;
		if(temp1 > 64) temp1 += 999000;
		if(temp2 > 64) temp2 += 999000;
		
		if (std::to_string(temp1) < std::to_string(temp2))
			return true;
		else if (std::to_string(temp1) > std::to_string(temp2))
			return false;
*/
		uint8_t temp1=chr_idx_, temp2=input.chr_idx_;
		if (chr_idx_ >= 128)
			temp1 = chr_idx_ - 128;
		if 	(input.chr_idx_ >=128)
			temp2 = input.chr_idx_-128;
		if (temp1 < temp2)
			return true;
		else if (temp2 < temp1)
			return false;
		else
		{
			if ( start_ < input.start_ )
				return true;
			else if (input.start_ < start_)
				return false;
			else
			{
				if (length_ < input.length_)
					return true;
				else if (input.length_ < length_)
					return false;
				else
				{
					if (tail_length_ < input.tail_length_)
						return true;
					else if ( input.tail_length_ < tail_length_)
						return false;
					else
					{
						if (tail_mismatch_ < input.tail_mismatch_)
							return true;
						else //if ( input.tail_mismatch < tail_mismatch )
							return false;
					}
				}
			}
		}
	}

/** 
 * @brief operator<< overload 
 */
	friend std::ostream& operator<< (std::ostream& out, const RawBed<TUPLETYPE>& target)
    {
		auto itr = gChrRefMap_.begin();
		if (target.chr_idx_<128)
		{
			std::advance (itr, target.chr_idx_);
        	out <<itr->first<<"\t+\t";
		}
		else
		{
			std::advance (itr, target.chr_idx_-128);
        	out <<itr->first<<"\t-\t";
		}
/*
        if( target.chr_idx_>=1 && target.chr_idx_<=32 )
            out << (int)target.chr_idx_ <<"\t+\t";
        else if( target.chr_idx_>=33 && target.chr_idx_<=64 )
            out << (int)target.chr_idx_-32 <<"\t-\t";
        else if( target.chr_idx_>=65 && target.chr_idx_<=90 )
            out << target.chr_idx_ <<"\t+\t";
        else if( target.chr_idx_>=97 && target.chr_idx_<=122)
            out << (char)(target.chr_idx_ -32) <<"\t-\t";
*/
        out<<target.start_                      <<'\t'
        <<target.length_                        <<'\t'
        <<(int)target.tail_length_              <<'\t'
        <<target.multiple_alignment_site_count_ <<'\t'
        <<target.reads_count_                   <<'\t'
        <<target.tail_mismatch_                 <<'\n';
		return out;
    }


    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {  
//		ar & chr_prefix_;
//		ar & mtable_;
		ar & chr_idx_; 
		ar & tail_length_; 
		ar & length_; 
		ar & multiple_alignment_site_count_; 
		ar & reads_count_; 
		ar & start_; 
		ar & tail_mismatch_;
	}

private:
/** 
 * @brief parsing the strand and chromsome information and accordingly return a coded char to represent both chromosome and strand information
 */
	char ParseChrInfo (std::string Q, int flag)
	{
		if (Q.size()>1)
		{
			if( (int)Q[0]>=48 && (int)Q[0]<=57 )    //double digit
			{
				if ( (flag >> 4) % 2 )	//determine whether the current alignment is from negative strand, i.e. 5th bit of sam flag is set or not
					return (char) ( std::stoi (Q) + 32 );	//if so, has the char's value shifted up 32, ranging from 33 to 64 to represent reverse strand 
				else
					return (char) std::stoi (Q);	//if not, left the char's value unchanged
			}
			else	// multiple chararacter situation, will be removed after upstream table establishment is renovated
			{
				if ( (flag >> 4) % 2 )	//determine whether the current alignment is from negative strand, i.e. 5th bit of sam flag is set or not
					return (char) ( std::stoi (Q) + 32 );	//if so, has the char's value shifted up 32 to use lowercase letter to signify reverse strand
				else
					return Q[0];
			}
		}
		else if (Q.size()==1)
		{
			if ((int)Q[0]>=48 && (int)Q[0]<=57) //single digit
			{
				if ( (flag >> 4) % 2 )
					return (char) ( std::stoi (Q) + 32 );
				else
					return (char) std::stoi (Q);
			}
			else
			{
				if ( (flag >> 4) % 2 )
					return Q[0]+32;
				else
					return Q[0];
			}
		}
	}

/** 
 * @brief parsing the tail sequence and return a coded uint32_t to represent the tail sequence
 */
	uint64_t ParseTailInfo (std::string tailseq)
	{
		uint64_t tmp_cs=0;
//std::cerr<<"parsetail info "<<tailseq<<'\t'<<tail_length_<<'\t'<<sizeof(tail_mismatch_)*8<<'\t'<<(sizeof(tail_mismatch_)*8/2)<<'\n';
		if (tail_length_ !=0 && tail_length_ <= (sizeof(tail_mismatch_)*8/2))//tail_size_)//16)	
		//do tailseq parsing to convert tailseq, with maximum length of 16 letters, coded into a uint32_t
		{
			tailseq.resize (tail_length_);
			for(INTTYPE i(0); i < tail_length_; ++i)
				tmp_cs += mtable_[ tailseq [i] ] * std::pow(4,(tail_length_-1-i)) ;
//			std::cerr<<"tail seq "<<tailseq<<'\t'<<tmp_cs<<'\n';
			return tmp_cs;
		}
		else
			return tmp_cs;
	}

/** 
 * @brief counting the length of chromosome information
 */
    int ParseChrInfoImpl (char* c)
	{
		int i = 0;
		while (*(c+i) != '\0')
			++i;
		return i;
	}
};


template <typename TUPLETYPE>
const char* 
RawBed <TUPLETYPE>::chr_prefix_ = "chr";

template <typename TUPLETYPE>
const std::array<int, 256> 
RawBed <TUPLETYPE>::mtable_ = 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

template <typename TUPLETYPE>
std::map <std::string, uint8_t> 
RawBed <TUPLETYPE>::gChrRefMap_;


#endif
