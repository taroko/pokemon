#ifndef ABWT_SAM_HPP_
#define ABWT_SAM_HPP_

/* definition of class HANB_SAM */
class HANB_SAM 
{
public:
	enum SAM_FLAG {
		MAPPED = 0,
		PAIRED_END = 1,
		EACH_END_ALIGNED = 2,
		UNMAPPED = 4,
		NEXT_UNMAPPED = 8,
		REVERSE_COMPLEMENTED = 16,
		NEXT_REVERSE_COMPLEMENTED = 32,
		FIRST_SEG = 64,
		SECOND_SEG = 128,
		SECONDARY_ALIGNMENT = 256,
		NOT_PASSING_QUALITY = 512,
		PCR_DUP = 1024,
		FLAG_SIZE = 11
	};
private:
	std::string QNAME = "";
	std::bitset<SAM_FLAG::FLAG_SIZE> FLAG;
	std::string RNAME = "";
	INTTYPE POS = 0;
	int MAPQ = 255;
	std::string CIGAR = "";
	std::string RNEXT = "";
	INTTYPE PNEXT = 0;
	int64_t TLEN = 0;
	std::string SEQ = "";
	std::string QUAL = "";
	INTTYPE _NH = 0;
	std::string _tailSeq = "";
//	std::unordered_map<std::string, std::string> OPTIONAL_FIELDS {};

public:
	/**! default constructor **/
	HANB_SAM () = default ;
	/** ctor from individuals*/
	HANB_SAM (std::string&& _QNAME, SAM_FLAG _FLAG, std::string&& _RNAME, INTTYPE _POS, int _MAPQ, std::string&& _CIGAR, std::string&& _RNEXT, INTTYPE _PNEXT, int64_t _TLEN, const std::string& _SEQ, std::string&& _QUAL, INTTYPE NH, std::string&& tailSeq = "")
		:
		QNAME {_QNAME},
		FLAG {_FLAG},
		RNAME {_RNAME},
		POS {_POS},
		MAPQ {_MAPQ},
		CIGAR {_CIGAR},
		RNEXT {_RNEXT},
		PNEXT {_PNEXT},
		TLEN {_TLEN},
		SEQ {_SEQ},
		QUAL {_QUAL},
		_NH {NH},
		_tailSeq {tailSeq}
		{ }
		/**! copy ctor **/
		HANB_SAM (const HANB_SAM&) = default;
		/**! move ctor **/
		HANB_SAM (HANB_SAM&&) = default;
		//	/**! lvalue assignment operator **/
		//	HANB_SAM& operator=(const HANB_SAM& other) {
		//		if (this == &other) return *this;
		//		QNAME = other.QNAME;
		//		FLAG  = other.FLAG;
		//		RNAME = other.RNAME;
		//		POS   = other.POS;
		//		MAPQ  = other.MAPQ;
		//		CIGAR = other.CIGAR;
		//		RNEXT = other.RNEXT;
		//		PNEXT = other.PNEXT;
		//		TLEN  = other.TLEN;
		//		SEQ   = other.SEQ;
		//		QUAL  = other.QUAL;
		//		OPTIONAL_FIELDS = other.OPTIONAL_FIELDS;
		//		return *this;
		//	}
		/**! rvalue assignment operator **/
		HANB_SAM& operator=(HANB_SAM&& other) {
			if (this != &other) {
				QNAME.swap (other.QNAME);
				FLAG = other.FLAG;
				RNAME.swap (other.RNAME);
				POS   = other.POS;
				MAPQ  = other.MAPQ;
				CIGAR.swap (other.CIGAR);
				RNEXT.swap (other.RNEXT);
				PNEXT = other.PNEXT;
				TLEN  = other.TLEN;
				SEQ.swap (other.SEQ);
				QUAL.swap (other.QUAL);
				_NH = other._NH;
				_tailSeq.swap (other._tailSeq);
				other.~HANB_SAM ();
			}
			return *this;
		}
		/**! destructor **/
		~HANB_SAM () = default;

		template <typename T>
		bool checkFlag (T f) const {
			return FLAG.test (f);
		}

		template <typename T, typename ... Args>
		bool checkFlag(T f, Args ... rest) const {
			if (sizeof ...(rest))
				return FLAG.test(f) && checkFlag (rest...);
			return FLAG.test (f);
		}
		// print out the sequence of SEQ
		int get_size () const {
			return SEQ.size ();
		}
		// output
		friend std::ostream& operator<< (std::ostream& os, const HANB_SAM& sam) {
			os << sam.QNAME << '\t'
					<< sam.FLAG.to_ulong() << '\t'
					<< sam.RNAME << '\t'
					<< sam.POS << '\t'
					<< sam.MAPQ << '\t'
					<< sam.CIGAR << '\t'
					<< sam.RNEXT << '\t'
					<< sam.PNEXT << '\t'
					<< sam.TLEN << '\t'
					<< sam.SEQ << '\t'
					<< sam.QUAL << '\t'
					<< "NH:i:" << sam._NH;
			if (!sam._tailSeq.empty ())
				os << "\tTL:Z:" << sam._tailSeq;
			os << '\n';
			return os;
		}
};

#endif
