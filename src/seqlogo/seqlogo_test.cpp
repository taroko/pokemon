#include <cmath>
#include <string>
#include <iostream>
#include <vector>
#include <cassert>


//
//int main (void)
//{
//	image QQ1 (768, 1024);
//
//	std::string strin (
//	"-----"
//	"  *  "
//	" *** "
//	"*   *"
//	"-----"
//	);
//	image QQ2 (strin, 255, 255, 0);	
//
//	for (auto& chr: QQ2.buffer_)
//		std::cerr<<chr<<' ';
//	
//};
//

#include "seqlogo.hpp"
//#include "savejpeg.h"
#include <stdio.h>
#include <string.h>
int main(void)
{
	double pwm[4 * 7] = { 0.17, 0.17, 0.10, 0.56,
		0.80, 0.00, 0.20, 0.00,
		0.00, 0.00, 1.00, 0.00,
		0.50, 0.40, 0.00, 0.10,
		0.10, 0.90, 0.00, 0.00,
		0.90, 0.03, 0.03, 0.04,
		0.00, 0.30, 0.30, 0.40 };
	int i, ii;
	// int *heights;
	// unsigned char *t;
	int err;

	SequenceLogo QQ;

	auto heights = QQ.getheightsfrompwm(pwm, 7, 80);
	for(i=0;i<4;i++)
	{
		for(ii=0;ii<7;ii++)
			printf("% 4d", heights[ii*4+i]);
		printf("\n");
	}
	auto t = QQ.seqlogo(pwm, 7, 7*40, 128);

	unsigned char* gg = new unsigned char [t.size()+1];
	int idx=0;
	for (auto chr: t)
	{
		gg[idx] = (unsigned char)chr;
		++idx;
	}
//	savejpeg("test3.jpeg", gg, 7*40, 128);
	QQ.ExportJPEG ("test3.jpeg", gg, 7*40, 128);
}
