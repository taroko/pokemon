
#include <cmath>
#include <string>
#include <iostream>
#include <vector>
#include <cassert>
//
///*********************************************************************************************
//
//                                         MAKESEQLOGO
//                                         
//                             Generate sequence logos for DNA motifs.
//                             
//    Seqlogos were invented by Schneider and Stephens 
//    
//    Schneider TD, Stephens RM. 1990. Sequence Logos: A New Way to Display Consensus Sequences. 
//    Nucleic Acids Res. 18:6097-6100
//    
//    Code by Malcolm Mclean, Weizmann Institute of Science, Israel                        
//
//**********************************************************************************************/
//#include <stdlib.h>
//#include <string.h>
//#include <math.h>
//#include <assert.h>
///* You can edit these letters to add symbols or use a different font.
//
//   Make each letter a single string, with the first line all '-' to indicate the letter 
//   width. Letters don't have to be the same dimensions - the code will resize them */
//
//// alphabet in string
//const std::string charA = (
//"-------------------------------"       
//"                               "       
//"             **                "       
//"             **                "       
//"            ***                "       
//"            ****               "       
//"            ****               "       
//"           ******              "       
//"           ** ***              "       
//"           *  ***              "       
//"          **   ***             "       
//"          *    ***             "       
//"         **    ****            "       
//"         **     ***            "       
//"         *      ***            "       
//"        **      ****           "       
//"        **       ***           "       
//"       **************          "       
//"       **        ****          "       
//"      **          ***          "       
//"      **          ****         "       
//"     **           ****         "       
//"     **            ****        "       
//"    ***            ****        "       
//"   ******        ********      "       
//"                               "       
//"                               ");       
//const std::string charC = (
//"-------------------------------"                               
//"                               "       
//"          *********   *        "       
//"        ****     ******        "       
//"       ***         ****        "       
//"      ***           ***        "       
//"     ***             **        "       
//"     ***             **        "       
//"    ***               *        "       
//"    ***               *        "       
//"   ****                        "       
//"   ****                        "       
//"   ****                        "       
//"   ****                        "       
//"   ****                        "       
//"   ****                        "       
//"   ****                        "       
//"    ***                        "       
//"    ***                        "       
//"     ***                       "       
//"     ***              *        "       
//"      ***            **        "       
//"       ***          **         "       
//"        ****     ****          "       
//"          *********            "       
//"                               "       
//"                               ");       
//const std::string charG = (   
//"-------------------------------" 
//"                               "
//"          *********  **        "      
//"        ****    *******        "      
//"       ***         ****        "       
//"      ***           ***        "       
//"     ***             **        "       
//"     ***             **        "       
//"    ***               *        "       
//"    ***                        "       
//"   ****                        "       
//"   ****                        "       
//"   ****                        "       
//"   ****          ********      "       
//"   ****            *****       "       
//"   ****             ***        "       
//"   ****             ***        "       
//"    ***             ***        "       
//"    ***             ***        "       
//"     ***            ***        "       
//"     ***            ***        "       
//"      ***           ***        "       
//"       ***          ***        "       
//"        ****      ****         "       
//"          **********           "       
//"                               "       
//"                               ");       
//const std::string charT = (
//"-------------------------------"                    
//"                               "                                            
//"   *******************         "       
//"   ****    ***    ****         "       
//"   **      ***      **         "       
//"   **      ***      **         "       
//"   *       ***       *         "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"           ***                 "       
//"          *****                "       
//"        *********              "       
//"                               "       
//"                               "); 
//
//// colours for the letters 
//const unsigned char col_A[3] = {0, 255, 0};
//const unsigned char col_C[3] = {0, 0, 255};
//const unsigned char col_G[3] = {150, 150, 0};  
//const unsigned char col_T[3] = {255, 0, 0}; 
//
//#define clamp(x, low, high) ((x) < (low) ? (low) : (x) > (high) ? (high) : (x))
//

int DefaultGrayLevel=255;
struct image
{
	int width_;
	int height_;
	std::vector<unsigned char> buffer_;

	image (void)
		: width_ (0), height_ (0), buffer_ ()
	{}

	image(int width, int height)
		: width_ (width), height_ (height), buffer_ (width_*height_*3, DefaultGrayLevel)
	{}

	image (std::string strin, int red, int green, int blue)//static IMAGE *stringtoimage(char *str, int r, int g, int b);
		: buffer_ (strin.size()*3, DefaultGrayLevel)
	{
		width_ = 0;
		for(auto i=0; strin[i] == '-';i++)
			++width_;
		height_ = strin.size()/width_;

		for(auto pos_y=0; pos_y<height_; ++pos_y)
			for(auto pos_x=0; pos_x<width_; ++pos_x)
				if(strin[pos_y*width_+pos_x] == '*')
					SetPixel(pos_x, pos_y, red, green, blue);
	}

	void SetPixel (int x, int y, int r, int g, int b)//(IMAGE *img, int x, int y, int r, int g, int b)
	{
		if(x >= 0 && x < width_ && y >= 0 && y < height_)
		{
			buffer_[(y*width_+x)*3] = r;
			buffer_[(y*width_+x)*3+1] = g;
			buffer_[(y*width_+x)*3+2] = b;
		}
	}

	void SetPixelValue (int x, int y, char r, char g, char b)//(IMAGE *img, int x, int y, unsigned char *col)
	{
		SetPixel(x, y, r, g, b);
	}

	void SetSize (int width, int height)
	{
		width_ = width;
		height_ = height;
		std::vector<unsigned char> tmp (width_*height_*3, DefaultGrayLevel);
		tmp.swap (buffer_);
	}

	void GetBufferPtr (unsigned char* return_ptr)
	{
		int idx=0;
		for (auto item: buffer_)
			return_ptr[idx] = item;
	}
};

//
//int main (void)
//{
//	image QQ1 (768, 1024);
//
//	std::string strin (
//	"-----"
//	"  *  "
//	" *** "
//	"*   *"
//	"-----"
//	);
//	image QQ2 (strin, 255, 255, 0);	
//
//	for (auto& chr: QQ2.buffer_)
//		std::cerr<<chr<<' ';
//	
//};
//

struct SequenceLogo
{
	// alphabet in string
	const std::string charA = (
			"-------------------------------"       
			"                               "       
			"             **                "       
			"             **                "       
			"            ***                "       
			"            ****               "       
			"            ****               "       
			"           ******              "       
			"           ** ***              "       
			"           *  ***              "       
			"          **   ***             "       
			"          *    ***             "       
			"         **    ****            "       
			"         **     ***            "       
			"         *      ***            "       
			"        **      ****           "       
			"        **       ***           "       
			"       **************          "       
			"       **        ****          "       
			"      **          ***          "       
			"      **          ****         "       
			"     **           ****         "       
			"     **            ****        "       
			"    ***            ****        "       
			"   ******        ********      "       
			"                               "       
			"                               ");       
	const std::string charC = (
			"-------------------------------"                               
			"                               "       
			"          *********   *        "       
			"        ****     ******        "       
			"       ***         ****        "       
			"      ***           ***        "       
			"     ***             **        "       
			"     ***             **        "       
			"    ***               *        "       
			"    ***               *        "       
			"   ****                        "       
			"   ****                        "       
			"   ****                        "       
			"   ****                        "       
			"   ****                        "       
			"   ****                        "       
			"   ****                        "       
			"    ***                        "       
			"    ***                        "       
			"     ***                       "       
			"     ***              *        "       
			"      ***            **        "       
			"       ***          **         "       
			"        ****     ****          "       
			"          *********            "       
			"                               "       
			"                               ");       
	const std::string charG = (   
			"-------------------------------" 
			"                               "
			"          *********  **        "      
			"        ****    *******        "      
			"       ***         ****        "       
			"      ***           ***        "       
			"     ***             **        "       
			"     ***             **        "       
			"    ***               *        "       
			"    ***                        "       
			"   ****                        "       
			"   ****                        "       
			"   ****                        "       
			"   ****          ********      "       
			"   ****            *****       "       
			"   ****             ***        "       
			"   ****             ***        "       
			"    ***             ***        "       
			"    ***             ***        "       
			"     ***            ***        "       
			"     ***            ***        "       
			"      ***           ***        "       
			"       ***          ***        "       
			"        ****      ****         "       
			"          **********           "       
			"                               "       
			"                               ");       
	const std::string charT = (
			"-------------------------------"                    
			"                               "                                            
			"   *******************         "       
			"   ****    ***    ****         "       
			"   **      ***      **         "       
			"   **      ***      **         "       
			"   *       ***       *         "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"           ***                 "       
			"          *****                "       
			"        *********              "       
			"                               "       
			"                               "); 

	// colours for the letters 
	unsigned char col_A[3] = {0, 255, 0};
	unsigned char col_C[3] = {0, 0, 255};
	unsigned char col_G[3] = {150, 150, 0};  
	unsigned char col_T[3] = {255, 0, 0}; 

	image seq_logo_image_;

	SequenceLogo (void)
		: seq_logo_image_ ()
	{}

	unsigned char ClampValue (double value, unsigned char floor, unsigned char ceiling)
	{
		if (value < floor)
			return (double) floor; 
		else if (value > ceiling)
			return (double) ceiling;
		else
			return value;
	}; 


/*
  get index of minimum value
*/
	int mini(int *v, int N)
	{
		int answer = 0;
		int i;
		for(i=0;i<N;i++)
			if(v[i] < v[answer])
				answer = i;
		return answer;
	}

/*
  get the overlap between two intervals
    a1 - low value for a
    a2 - high value for a
    b1 - low value for b
    b2 - high value for b
  Returns: the overlap
*/
	double overlap(double a1, double a2, double b1, double b2)
	{
		double start, end;

		if(a1 < b1)
			start = b1;
		else
			start = a1;
		if(a2 > b2)
			end = b2;
		else
			end = a2;
		return end - start;
	}

/*
  resize algorithm
  
  Params: spr - the original sprite
          width - original width
          height - original height
          newwidth - width wanted
           newheight - height wanted
  Returns: new sprite
  Notes: should be used for shrinking rather than stretching as does pixel-based sampling
    instead of bilinear interpolation
*/
	std::vector<unsigned char> shrinkasprite(std::vector<unsigned char> spr, int width, int height, int newwidth, int newheight)
	{
		std::vector<unsigned char> answer (newwidth * newheight * 3, DefaultGrayLevel);
		double deltax, deltay;
		double x, y;
		int xi, yi;
		double xfrac, yfrac, frac;
		int i, ii;
		double red, green, blue;
		double tot;

		deltax = ((double)width) / newwidth;
		deltay = ((double)height) / newheight;

		for(i=0, y = 0.0;i<newheight;i++, y += deltay)
			for(ii=0, x = 0.0;ii<newwidth;ii++, x += deltax)
			{
				red = 0.0;
				green = 0.0;
				blue = 0.0;
				tot = 0.0;

				yi = std::floor(y);
				do
				{
					yfrac = overlap(yi, yi+1, y, y + deltay);
					xi = std::floor(x);
					do
					{
						xfrac = overlap(xi, xi+1, x, x + deltax);
						frac = xfrac * yfrac;
						tot += frac;
						red += spr[(yi*width+xi)*3] * frac;
						green += spr[(yi*width+xi)*3+1] * frac;
						blue += spr[(yi*width+xi)*3+2] * frac;
						xi++;	  
					} while(xi < x + deltax && xi < width);
					yi++;
				} while(yi < y + deltay && yi < height);

				red = ClampValue(red/tot, 0, 255);
				green = ClampValue(green/tot, 0, 255);
				blue = ClampValue(blue/tot, 0, 255);
				answer[(i*newwidth+ii) * 3] = (unsigned char) red;
				answer[(i*newwidth+ii) * 3 + 1] = (unsigned char) green;
				answer[(i*newwidth+ii) * 3 + 2] = (unsigned char) blue;
			} 
		return answer;  
	}

/*
  Paste letter intot he logo
  Params: img - the logo
          x, y - co-ordiantes of top left
          width, height - height ans width of letter
*/
	//void pasteletter(IMAGE *img, int x, int y, int width, int height, IMAGE *glyph)
	void pasteletter(int x, int y, int width, int height, image& glyph)
	{
		int i, ii;
		auto temp = shrinkasprite(glyph.buffer_, glyph.width_, glyph.height_, width, height);
		for(i=0;i<height;i++)
			for(ii=0;ii<width;ii++)
				seq_logo_image_.SetPixelValue(x+ii, y+i, temp[((i*width) + ii)* 3], temp[((i*width) + ii)* 3 +1], temp[((i*width) + ii)* 3+2]);
	}

/*
  Shannon entropy of a vector 
*/
	static double entropy(double *x, int N)
	{
		int i;
		double answer = 0;
		double tot = 0;

		for(i=0;i<N;i++)
			tot += x[i];

		for(i=0;i<N;i++)
			if(x[i] > 0)
				answer -= x[i]/tot * log(x[i]/tot);

		return answer;
	}

/*
  get the heights of the letters
  Params: pwm - the position weight matrix
          N - number of bases
          maxheight - the maxmimu height of a letter
  Returns: matrix of letter heights
  Notes: each letter has a height scaled by the entropy 
*/
	int* getheightsfrompwm(double *pwm, int Nbases, int maxheight)
	{
		double *heights;
		int *answer;
		//std::vector<int> answer (4*Nbases);
		int i, ii;
		double ent;

		  answer = malloc(4*Nbases*sizeof(int));
		  if(!answer)
		      return 0;
		for(i=0;i<Nbases;i++)
		{
			ent = 2.0 - entropy(&pwm[i*4], 4)/log(2);
			for(ii=0;ii<4;ii++)
				answer[i*4+ii] = (int) (pwm[i*4+ii] * maxheight/2 * ent + 0.5);
		}

		return answer;
	}

/*
  Get the logo
  Params: heights - heights from the getheightsfrompwm() fucntion
          Nbases - number of bases in the pwm
          width - logo width, in pixels (eg 16 per base)
          height - logo height in pixels (eg 128)
  Returns: malloced 24-bit rgb buffer
  Notes: heights is destroyed
*/
	std::vector<unsigned char> getlogofromheights(int* heights, int Nbases, int width, int height)
	{
		int i, ii, basei;
		//  IMAGE *img;
		int y;
		//  image *glyphs[4];
		std::vector<image> glyphs //(4);
		{
			image(charA, col_A[0], col_A[1], col_A[2]),
				image(charC, col_C[0], col_C[1], col_C[2]),
				image(charG, col_G[0], col_G[1], col_G[2]),
				image(charT, col_T[0], col_T[1], col_T[2])
		};
		int colwidth;
		int xpos;
		//  unsigned char *answer;

		colwidth = width/Nbases;  /* width of each column */

		seq_logo_image_.SetSize (width, height); 
//std::cerr<<"seq_log_image_.size "<<seq_logo_image_.buffer_.size()<<'\n';

		//  image img = image(width, height);

		//  if(!img || !glyphs[0] || !glyphs[1] || !glyphs[2] || !glyphs[3])
		//    goto outofmemory;
		for(i=0;i<Nbases;i++)
		{
			y = height;
			for(ii=0;ii<4;ii++)
			{
				//basei = mini(heights, 4 * i, 4);
				basei = mini(heights+ 4 * i,4);
				y -= heights[4*i+basei];
				xpos = (width * i)/Nbases; 
				pasteletter(xpos, y, colwidth, heights[4*i+basei], glyphs[basei]);
				heights[4*i+basei] = 10000;
			}
		}

		//  for(i=0;i<4;i++)
		//      killimage(glyphs[i]);

		auto answer = seq_logo_image_.buffer_;
		//  free(img);

		return answer.GetBufferPtr ();

		//  outofmemory:
		//    killimage(img);
		//    for(i=0;i<4;i++)
		//      killimage(glyphs[i]);
		//    return 0;
	}

/*
  Set the colours to be used for the seq logo
  Params: letter - ACGT
          red, green, blue - 25 bit rgb values
  Notes: letter colour is held as global state  
*/
	void setseqlogocolor(char letter, unsigned char red, unsigned char green, unsigned char blue)
	{
		switch(letter)
		{
			case 'A':
				col_A[0] = red;
				col_A[1] = green;
				col_A[2] = blue;
				break;
			case 'C':
				col_C[0] = red;
				col_C[1] = green;
				col_C[2] = blue;
				break;
			case 'G':
				col_G[0] = red;
				col_G[1] = green;
				col_G[2] = blue;
				break;
			case 'T':
				col_T[0] = red;
				col_T[1] = green;
				col_T[2] = blue;
				break;
			default:
				assert(letter == 'A' || letter == 'C' || letter == 'G' || letter == 'T');
				break;
		}
	}

/*
  Create a sequence logo for a position-specific weight matirx
    Params: pwm - the position-specific weight matrix, Nbases *4, columns minor, order ACGT
            Nbases - the numenr of bases in the pwm
            width - width of the logo , in pixels (e.g. Nbases * 16)
            height - height of the logo, in pixels (eg 128)
    Returns: malloced rgb 24 bit buffer contianing logo, width * height * 3 
    Notes: pwm columns must sum to unity. Columns go pwm[0], pwm[1], pwm[2], pwm[3].          
*/
	std::vector<unsigned char> seqlogo(double *pwm, int Nbases, int width, int height)
	{
		int logoheight;
		int i;
		double colweight;
		std::vector<unsigned char> answer;

		for(i=0;i<Nbases * 4;i++)
			assert(pwm[i] >= 0.0);
		for(i=0;i<Nbases;i++)
		{
			colweight = pwm[i*4] + pwm[i*4+1] + pwm[i*4+2] + pwm[i*4+3];
			assert(colweight >= 0.99 && colweight <= 1.0);
		}  
		logoheight = (height * 3)/4;
		auto heights = getheightsfrompwm(pwm, Nbases, logoheight);

		answer = getlogofromheights(heights, Nbases, width, height);
		free(heights);

		return answer;
	}

};



#include "savejpeg.h"
#include <stdio.h>
#include <string.h>
int main(void)
{
  double pwm[4 * 7] = { 0.17, 0.17, 0.10, 0.56,
                        0.80, 0.00, 0.20, 0.00,
                        0.00, 0.00, 1.00, 0.00,
                        0.50, 0.40, 0.00, 0.10,
                        0.10, 0.90, 0.00, 0.00,
                        0.90, 0.03, 0.03, 0.04,
                        0.00, 0.30, 0.30, 0.40 };
 int i, ii;
// int *heights;
// unsigned char *t;
 int err;

 SequenceLogo QQ;

 auto heights = QQ.getheightsfrompwm(pwm, 7, 80);
 for(i=0;i<4;i++)
 {
   for(ii=0;ii<7;ii++)
      printf("% 4d", heights[ii*4+i]);
   printf("\n");
 }
auto t = QQ.seqlogo(pwm, 7, 7*40, 128);

unsigned char* gg = new unsigned char [t.size()+1];
int idx=0;
for (auto chr: t)
{
	gg[idx] = (unsigned char)chr;
	++idx;
}
    savejpeg("test3.jpeg", gg, 7*40, 128);

}
