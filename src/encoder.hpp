/// @file encoder.hpp
/// @brief achieve conding (compression) scheme with tempalte class specialization over enum paramerter compression_type 
/// @author C-Salt Corp.
#ifndef ENCODER_HPP_
#define ENCODER_HPP_
#include "compression.hpp"
/// @class Encoder
/// @brief Include template member function encode
/// @tparam compression_type non-type enum parameter, indicating current compression scheme
template <int N, CompressType comp_type>
class Encoder
{
public:
/// @typedef value_type for current design, three different value_type, i.e. std::string, NBitCompress<TraitClass<N, CompressType::TWO_BITS_> >, and NBitCompress< TraitClass<N, CompressType::TWO_BITS_MASK> >, defined in compreess_trait<comp_type> class, are provided.  Other value_type may be defined for further encode function implementation, e.g. protein Encoder with 6 or 8 bits.
	typedef typename Compress_Trait<N, comp_type>::value_type value_type;	/// use the Compresson_Trait as internal implementation
/// @memberof Encoder<compression_type comp_type>
/// @brief an encode function for obtaining a value_type, in format of std::string or NBitCompress types, in response to a to-be-encoded data of t, wherein t is, for example, has a format of std::string 
/// @tparam t type parameter
/// @return value_type, e.g.in format of std::string or NBitCompress
	template <typename T>
		value_type encode (const T& t)	/// directly use the constructor to achieve the conversion (compression)
		{
			return value_type (t);
		}
};

#endif
