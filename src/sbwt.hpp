/**
 *  @file sbwt.hpp
 *  @brief 短版本 BWT 演算法實作 \n
 *  @author C-Salt Corp.
 *  
 */
#ifndef SBWT_HPP_
#define SBWT_HPP_
#include <algorithm>
#include <bitset>
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <fstream>
#include <functional>
#include <iostream>
#include <list>
#include <locale>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <string>
#include <tuple>
#include <utility>
//#include <unordered_map>
#include <boost/ref.hpp>
#include <ctime>
#include <cmath>
#include <memory>
#include "compression/abit.hpp"
#include "compression/jbit.hpp"
//#include "thread_pool.hpp"
//#include "difference_cover.hpp"
#include "constant_def.hpp"
#include "mkq_sort.hpp"
#include "difference_cover.hpp"
#include "split_sort.hpp"
#include "mkq_sort.hpp"
#include "abwt_table.hpp"
#include "abwt_search.hpp"

#include "boost/serialization/vector.hpp"
#include "boost/serialization/utility.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/unordered_map.hpp"
//#include <boost/archive/binary_iarchive.hpp>
//#include <boost/archive/binary_oarchive.hpp>

//#define INTTYPE uint64_t

template< class SeqType>
class SBWT
{};

template<>
class SBWT< ABSequence<> >
{

public:
	
	/// @brief 定義 SeqType 為 ABSequence
	typedef ABSequence<> SeqType;
	
	/// @brief 定義 TableType 為儲存bwt location (int)的容器
	typedef std::vector< INTTYPE > TableType;
	
	
	/// @brief 定義主體sort suffix 的 sort type \n
	/// 這邊使用 multikey quick sort 
	typedef Multikey_quicksort
					<	SeqType,
						TableType,
						Record_rank_disable,
						Compare_default,
						Sort_small_n_enable,
						//Sort_big_n_disable
						Bucket_sort
					>	MainSortType;
	
	/// @brief 原始序列儲存
	SeqType &seq;
	
	/// @brief 排序 All Suffix後，儲存每個Suffix的第一個字在原始sequence的位置(INTTYPE) \n
	/// 此時 int 對應到 seq 的字元應該是： $AAAA...CC...G...TT... \n
	/// 而前一個字元(int -1)對應到的字元則為 BWT \n
	TableType seq_table;
	
	/// @brief ABWT_table 為處理seq_table的class，主要負責產生所有在 search所需要的表 \n
	/// @brief 像是： bwt(char), o table, occ table, location table 等等，和一些加速的 table
	ABWT_table abwtt;
	
	/// @brief 紀錄原始sequence的總長度
	INTTYPE len_seq; 
	
	/// @brief difference cover 的參數，為2的倍數，關係到dcs的大小，速度
	INTTYPE sort_length; 
	
	/// @brief 排序時，分割每群的大小 \n
	/// @brief 在排序時，是分成好幾群，依序排序，可以有效減少記憶體用量，也可以多cpu執行
	INTTYPE split_size;
	
	/// @brief 記錄時間
	clock_t start, stop;
	
	/**
	 * @memberof class ABWT< ABSequence<std::string> >
	 * @brief 建構式與程式主體
	 * @param sequence ABSequence<std::string> 原始待排序序列
	 * @param limit_sort_length INTTYPE sort 前 n 個字
	 * @param interval_size INTTYPE 在建abwt表時所使用的間隔參數，越小越快，記憶體使用越大
	 */
	SBWT (SeqType &sequence, long limit_sort_length=512, long interval_size=64)
		:seq(sequence), len_seq(seq.size()) , sort_length(limit_sort_length), abwtt(interval_size), split_size(30000000)
	{		
		//part 1		
		//no dcs 
		
		//part 2
		
		
		start = clock();	

		// sort suffix using split sort
		std::cout << "split_size : " << split_size << std::endl;
		//new split sort obj
				
		Split_sort<	SeqType, std::vector< INTTYPE >, MainSortType > split_sort (seq, sort_length);
		//setting
		split_sort.getTableV = [](INTTYPE& a){
			return a;
		};
		split_sort.dcs_compare = [](const INTTYPE a, const INTTYPE b){return true;};
		
		//start split sort
		split_sort.split_by_size_init(split_size);
		
		// copy sorted result to seq_table(bwt)
		//seq_table.reserve( seq.size() );
		//seq_table.swap(split_sort.Results);
		std::vector<std::string> filenames = split_sort.archive_name;
		
		stop = clock();
		std::cout << "===================================" << std::endl;
		std::cout << "Create BWT, time:" << double(stop - start) / CLOCKS_PER_SEC << std::endl;
		std::cout << "===================================" << std::endl;
		
		//std::vector<std::string> filenames;
		//for(int i=0; i <= 231; i++)
		//{
		//	filenames.push_back("split_" + std::to_string(i));
		//}
		
		
		//part 3
		
		start = clock();
		// save abwt table
		abwtt.createAllTable(seq, filenames);
		//abwtt.createAllTable(seq, seq_table);
		abwtt.saveBWT("t_sbwt.bwt");
		abwtt.saveTable("t_stable.bwt");
		abwtt.saveSEQ("t_sseq.bwt", seq);
		
		stop = clock();
		std::cout << "===================================" << std::endl;
		std::cout << "Create abwt Table, time:" << double(stop - start) / CLOCKS_PER_SEC << std::endl;
		std::cout << "===================================" << std::endl;
		
		// part 4 print SA
		/*
		for(auto i(0);i<seq_table.size();i++)
		{
			for(auto j(0); j<seq_table.size(); j++)
			{
				
				auto idx = (seq_table[ i ]+j)%seq.size();
				std::cerr << seq[ idx ];
			}
			std::cerr << std::endl;
		}
		*/

		split_sort.clean_up ();

		return ;
		
	}
	
	
};


#endif
