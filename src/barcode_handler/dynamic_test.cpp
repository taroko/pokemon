#include "barcode_handler_impl_dynamic.hpp"
#include <iostream>
#include <map>
#include <vector>
#include <string>
int main (void)
{
	std::vector<std::string> bq ({"AA", "GC"});
	BarcodeHandlerDynamic BD (3,bq);

	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE;
	std::vector< Fastq<TUPLETYPE> > QQ;
	QQ.push_back (std::make_tuple (std::string("@1"), std::string("TACGTAACGT"), std::string("+"), std::string(")))))(((((")));
	QQ.push_back (std::make_tuple (std::string("@2"), std::string("AACGTAACGA"), std::string("+"), std::string(")))))(((((")));
	QQ.push_back (std::make_tuple (std::string("@3"), std::string("CACGTAACGC"), std::string("+"), std::string(")))))(((((")));
	QQ.push_back (std::make_tuple (std::string("@4"), std::string("GACGTAACGG"), std::string("+"), std::string(")))))(((((")));
	std::map <int,  std::vector< Fastq<TUPLETYPE> > > mp 
	({{ 0, QQ }});


	std::cerr<<"original content: "<<'\n';
	int ind=0;
	for (auto& i : mp)
	{
		std::cerr<<"barcode_group: "<<ind<<'\n';
		for (auto& itm : mp[ind])
			std::cerr<<itm<<'\n';
		++ind;
	}
	std::cerr<<"============================"<<'\n';


	BD (&mp); 

	ind=0;
	for (auto& i : mp)
	{
		std::cerr<<"barcode_group: "<<ind<<'\n';
		for (auto& itm : mp[ind])
			std::cerr<<itm<<'\n';
		++ind;
	}
};
