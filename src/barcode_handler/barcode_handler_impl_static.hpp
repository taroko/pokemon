#include <iostream>
#include <tuple>
#include "boost/algorithm/string.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/mpl/map.hpp"
#include "boost/mpl/string.hpp"
#include "boost/type_traits.hpp"
#include "../format/fastq.hpp"
#include "../file_reader.hpp"

template<BarcodeHandleScheme BARCODE_SCHEME, typename BARCODE_TYPE,// = boost::mpl::void_, 
			template <typename> class FORMAT=Fastq, typename TUPLETYPE=std::tuple<std::string, std::string, std::string, std::string> >
class BarcodeHandler
{
public:
    BarcodeHandler (void)
    {}
	typedef boost::mpl::int_<std::tuple_size <BARCODE_TYPE>::value-1> BARCODE_TUPLE_SIZE;
    std::map < int, std::vector< Fastq<TUPLETYPE> > >* operator() (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)
    {
		if (IsTupleType<BARCODE_TYPE>::value && !(boost::is_same <typename std::tuple_element <0, BARCODE_TYPE>::type, boost::mpl::string<'null'> >::value) )
		//(! boost::is_same<BARCODE_TYPE, boost::mpl::void_>::value )
		{
			std::swap ( (*result2)[0], (*result2)[-1]);
			for (size_t itr=0; itr!=(*result2)[-1].size(); ++itr)
				BarcodeFormatType< boost::mpl::int_<0> >::ScanBarcodeSeq ((*result2)[-1][itr], result2);
			//if ( (*result2).size()==2)//no barcode distribution at all	
			//	std::swap ( (*result2)[0], (*result2)[-1]);
			(*result2).erase ((*result2).begin());
		}
		else
			;//return result2;
		return result2;
    }

	template <class N, typename DUMMY=char>
	struct BarcodeFormatType 
	{
		typedef typename std::tuple_element<N::value, BARCODE_TYPE>::type ListedType;
		static void ScanBarcodeSeq (FORMAT<TUPLETYPE>& content, std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)
		{
			int start_site=0;
			if (BARCODE_SCHEME)	//three prime barcode
				start_site = content.getSeq().size()-boost::mpl::size<ListedType>::value;
			if ( boost::mpl::c_str <ListedType>::value == content.getSeq().substr (start_site, boost::mpl::size<ListedType>::value) )
			{
				std::get<1>(content.data) = content.getSeq().replace (start_site, boost::mpl::size<ListedType>::value, "");
				std::get<3>(content.data) = content.getQuality().replace (start_site, boost::mpl::size<ListedType>::value, "");
//				std::cerr<<"CURRENT N: "<<N<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
				(*result2)[N::value].push_back (std::move (content) );
			}
			else
			{
//				std::cerr<<"CURRENT N: "<<N<<"boost::mpl::c_str<TAG_TYPE>::value != tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\nsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
				BarcodeFormatType <boost::mpl::int_<N::value+1>, DUMMY>::ScanBarcodeSeq (content, result2);
			}
		}
	};

	template <typename DUMMY>
	//struct BarcodeFormatType <std::tuple_size <BARCODE_TYPE>::value-1, DUMMY>
	struct BarcodeFormatType <BARCODE_TUPLE_SIZE, DUMMY>
	{
		typedef typename std::tuple_element<std::tuple_size <BARCODE_TYPE>::value-1, BARCODE_TYPE>::type ListedType;
		static void ScanBarcodeSeq (FORMAT<TUPLETYPE>& content, std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)
		{
			int start_site=0;
			if (BARCODE_SCHEME)	//three prime barcode
				start_site = content.getSeq().size()-boost::mpl::size<ListedType>::value;
			if ( boost::mpl::c_str <ListedType>::value == content.getSeq().substr (start_site, boost::mpl::size<ListedType>::value) )
			{
				std::get<1>(content.data) = content.getSeq().replace (start_site, boost::mpl::size<ListedType>::value, "");
				std::get<3>(content.data) = content.getQuality().replace (start_site, boost::mpl::size<ListedType>::value, "");
//				std::cerr<<"CURRENT N: "<<std::tuple_size <BARCODE_TYPE>::value-1<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
				(*result2)[std::tuple_size <BARCODE_TYPE>::value-1].push_back (std::move (content) );
			}
			else
			{
//				std::cerr<<"CURRENT N: "<<std::tuple_size <BARCODE_TYPE>::value-1<<"boost::mpl::c_str<TAG_TYPE>::value == tag_name\t" <<
//					boost::mpl::c_str< ListedType >::value << "\tsize of barcode: "<<boost::mpl::size<ListedType>::value<<'\n';
			}
		}
	};
};

