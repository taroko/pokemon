#include <iostream>
#include <tuple>
#include "boost/algorithm/string.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/mpl/map.hpp"
#include "boost/mpl/string.hpp"
#include "boost/type_traits.hpp"
#include "../format/fastq.hpp"
#include "../file_reader.hpp"

//template<BarcodeHandleScheme BARCODE_SCHEME, typename BARCODE_TYPE,// = boost::mpl::void_, 
//			template <typename> class FORMAT=Fastq, typename TUPLETYPE=std::tuple<std::string, std::string, std::string, std::string> >
class BarcodeHandlerDynamic
{
	int barcode_scheme_;
	std::vector<std::string>& barcode_vec_;
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE ;

public:
    BarcodeHandlerDynamic (int barcode_scheme, std::vector<std::string>& barcode_vec)
		: barcode_scheme_ (barcode_scheme)
		, barcode_vec_ (barcode_vec)
    {}

    std::map < int, std::vector< Fastq<TUPLETYPE> > >* operator() (std::map < int, std::vector< Fastq<TUPLETYPE> > >* result2)
	{
		if (barcode_scheme_ == 0)
			;
		else	
		{
			std::swap ( (*result2)[0], (*result2)[-1]);
			int barcode_index=0;	

			for (auto& barcode_seq: barcode_vec_)
			{
				int barcode_length = barcode_seq.size();
				int start_site = 0;
//				if (barcode_scheme_==3)
//					start_site = (*result2)[-1].front().getSeq().size()-barcode_length;//boost::mpl::size<ListedType>::value;
				for (auto& content: (*result2)[-1])
				{
					if (barcode_scheme_==3)
						start_site = content.getSeq().size()-barcode_length;//boost::mpl::size<ListedType>::value;

					if (start_site<0 || start_site>=content.getSeq().size() || barcode_length<0)
						continue;

					if ( barcode_seq == content.getSeq().substr (start_site, barcode_length) )
					{
						Fastq<TUPLETYPE> tmp;
						std::get<0>(tmp.data) = std::get<0>(content.data);
						std::get<1>(tmp.data) = content.getSeq().replace (start_site, barcode_length, "");
						std::get<2>(tmp.data) = content.getName2();
						std::get<3>(tmp.data) = content.getQuality().replace (start_site, barcode_length, "");
						(*result2)[barcode_index].push_back (std::move (tmp) );
					}
				}
				++barcode_index;
			}
			(*result2).erase ((*result2).begin());
		}
		return result2;
	}
};

