///@file mpi_job_creator.hpp
///@brief A functor wrapper, for wrapping user provided functor and having it equipped with serialization functionality, so as to support MPI operation
///@author C-Salt Corp.
#ifndef MPI_JOB_CREATOR_HPP
#define MPI_JOB_CREATOR_HPP
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "tuple_utility.hpp"
#include <vector>
#include <tuple>
#include <fstream>
#include <string>
#include <iostream>
#include <functional>
#include "boost/ref.hpp"

/// @class IsReferenceWrapper template class
/// @brief served as a simple type verifier, capable of providing a bool indicating that a to-be-tested object corresponds to reference types (such as std::reference_wrapper or boost::reference_wrapper) or non-reference type.  
/// @tparam T a type parameter
template<typename T>
class IsReferenceWrapper	//provide a scheme with a value indicating true in response reference wrapper objects (boost or std), and indicating false otherwise 
{
public:
/// @memberof value corresponds to the value of false by default
	static const bool value = false;
};

/// @brief specialized form of the IsReferenceWrapper, with the to-be-tested object specialized as the type of std::reference_wrapper<T>
template<typename T>
class IsReferenceWrapper< std::reference_wrapper<T> >
{
public:
/// @memberof value corresponds to the value of true 
	static const bool value = true;
};

template<typename T>
/// @brief specialized form of the IsReferenceWrapper, with the to-be-tested object specialized as the type of boost::reference_wrapper<T>
class IsReferenceWrapper< boost::reference_wrapper<T> >
{
public:
/// @memberof value corresponds to the value of true 
	static const bool value = true;
};

/// @class SerializedFunctor_base template class
/// @brief a basic implementation class for generic functor wrapper.  Variadic template parameter Args... represents possible arguments and return value of the template parameter FUNCTOR. 
/// @tparam FUNCTOR representing the to-be-wrapped functor's type.  Though in the current usage, std::bind will be applied to the functor in the JobSerializer class.  In other words, FUNCTOR has the type of std::function<RETURN(void)> for the current usage.
/// @tparam Args defined in variadic form, so as to represent possible arguments of FUNCTOR
template<class FUNCTOR, class... Args> //PARAM_TUPLE>
class SerializedFunctor_base
// A class with a FUNCTOR f_ and its parameter tuple PARAM_TUPLE p_.  The class is capable of
//	1) executing the FUNCTOR f_; and
//	2) serializing the PARAM_TUPLE p_
// if any elements of the PARAM_TUPLE p_ is provided by  reference, i.e. capable of holding operation result of the FUNCTOR f_, the SerializedFunctor_base can obtain 
// and serialize the FUNCTOR f_'s result.  As such, mpi compatibility can be guaranteed by using the SerializedFUnctor_base object
// if otherwise, i.e. the operation result of the FUNCTOR f_ is returned via return value, the inherited class SerializedFunctor will provide a member element of 
// RETURN r_ to catch and record the return value in SerializedFunctor data structure 
{
	friend class boost::serialization::access;
public:
	FUNCTOR f_;
	std::tuple < Args... > p_;//	PARAM_TUPLE p_;
	typedef std::tuple <Args...> PARAM_TUPLE;
	SerializedFunctor_base (FUNCTOR& f, Args... args)//PARAM_TUPLE& p)
		: f_(f), p_(args...)
	{}
	SerializedFunctor_base (const SerializedFunctor_base& aa)
		: f_(aa.f_), p_(aa.p_)
	{}
	SerializedFunctor_base& operator= (const SerializedFunctor_base& aa)
	{
		f_ = aa.f_;
		p_ = aa.p_;
		return *this;
	}
	SerializedFunctor_base (SerializedFunctor_base&& aa)
		: f_(std::move (aa.f_)), p_(std::move(aa.p_))
	{std::cerr<<"move construct SerializedFunctor_base"<<std::endl;}
	void operator= (SerializedFunctor_base&& aa)
	{
		f_ = std::move(aa.f_);
		p_ = std::move(aa.p_);
		std::cerr<<"move assign"<<std::endl;
	}
/// @memberof SerializedFunctor_base capable of executing the incoming functor.  It is noted that value return schemes, either by means of directly return value or by means of altering call by reference parameter, is not defined in the current base class
	void operator() ( void )
	// A member function to provide an interface to execute the FUNCTOR f_
	{
		f_();
	}
/// @memberof SerializedFunctor_base for achieving the serialization operation for the member variable of p_, i.e. the parameters of the to-be-wrapped functor f_
	template < typename Archive >
	void serialize (Archive& arc, const unsigned int version)
	// A template serialize function, capable of having each and every one of the elements of the PARAM_TUPLE p_ 
	// serialized into or from the Archive arc.
	// template struct Serial_Imp is employed to provide partial specialization capability, so as to achive static recursion accordingly.
	// Totally 5 embodiments, including a generic form and four partially specialized forms, are presented.
	{
		size_t const size = std::tuple_size<PARAM_TUPLE>::value;
		Serial_Imp < 
					Archive, 
					size-1,
					IsReferenceWrapper <  typename std::tuple_element<size-1, PARAM_TUPLE>::type  >::value 
	// IsReferenceWrapper class is employed here to determine whether any of the parameters is passed in reference-wrapper.
	// If so, the reference wrapper must be taken off before serialization is applied.  As such, different serial_Imp implementations are applied to respectively hadle the different serailization job
				>::serialize_base ( arc, p_);
	}

/// @brief serialization implementation for each elmenet of the PARAM_TUPLE, i.e. the parameters p_ of the to-be-wrapped functor f_
	template < typename Archive, int ParaCount, bool IsRefWrapped >
	struct Serial_Imp
	{};
	
/// @brief specialized form of the Serial_Imp, with the the bool B specialized as false
	template < typename Archive, int ParaCount>
	struct Serial_Imp< Archive, ParaCount, false >
	{
		static void serialize_base (Archive& arc, PARAM_TUPLE p_)
		{
			Serial_Imp < Archive, ParaCount-1, IsReferenceWrapper <  typename std::tuple_element<ParaCount-1, PARAM_TUPLE>::type  >::value >::serialize_base (arc, p_);
			arc & std::get<ParaCount>(p_);
		} 
	};
	
/// @brief specialized form of the Serial_Imp, with the the bool B specialized as true
	template < typename Archive, int ParaCount>
	struct Serial_Imp< Archive, ParaCount, true >
	{
		static void serialize_base (Archive& arc, PARAM_TUPLE p_)
		{
			Serial_Imp < Archive, ParaCount-1, IsReferenceWrapper <  typename std::tuple_element<ParaCount-1, PARAM_TUPLE>::type  >::value >::serialize_base (arc, p_);
			arc & std::get<ParaCount>(p_).get();
		} 
	};
	
/// @brief specialized form of the Serial_Imp, with the the int N and the bool B respectively specialized as 0 and false
	template < typename Archive >
	struct Serial_Imp< Archive, 0, false >
	{
		static void serialize_base (Archive& arc, PARAM_TUPLE p_)
		{
			arc & std::get<0>(p_);
		}
	};
	
/// @brief specialized form of the Serial_Imp, with the the int N and the bool B respectively specialized as 0 and true
	template < typename Archive >
	struct Serial_Imp< Archive, 0, true >
	{
		static void serialize_base (Archive& arc, PARAM_TUPLE p_)
		{
			arc & std::get<0>(p_).get();
		}
	};
};

/// @brief a class capable of having incoming functor, with any type, equipped with proper serialization implementation.  The SerializedFunctor is inherited from the SerializedFunctor_base, and further include a memeber element of r_, for catching the result of the to-be-wrapped functor
/// tparam RETURN representing the type of the return value.
template<class FUNCTOR, class RETURN, class... Args > //PARAM_TUPLE>
class SerializedFunctor : public SerializedFunctor_base<FUNCTOR, Args... >//PARAM_TUPLE>
{
public:
	typedef std::tuple <Args...> PARAM_TUPLE;
	SerializedFunctor (FUNCTOR& f, Args... args)//PARAM_TUPLE& p)
		: SerializedFunctor_base<FUNCTOR, Args...> (f, args...)//PARAM_TUPLE>(f, p)
	{}
/// @memberof SerializedFunctor having the type of RETURN, and employed to catch the execution result of the functor f_.
	RETURN r_;
/// @memberof SerializedFunctor capable of employing the member variable r_ to catch the executing result of the functor f_.  In other words, the value return scheme achieved by return value returing is accordingly achieved.
	void operator() ( void )
	{
		r_ = this->f_();
	}
/// @brief serialization implementation for the member variable of r_.
	template < typename Archive >
	void serialize (Archive& arc, const unsigned int version)
	// A template serialize function, capable of having each and every one of the elements of the PARAM_TUPLE p_ 
	// serialized into or from the Archive arc.
	// template struct Serial_Imp is employed to provide partial specialization capability, so as to achive static recursion accordingly.
	// Totally 5 embodiments, including a generic form and four partially specialized forms, are presented.
	{
		arc & r_;
		::SerializedFunctor_base<FUNCTOR, Args...>::serialize(arc, version);
	}
};

/// @brief specialized form of the SerializedFunctor class, with the template parameter RETURN specialized to be void.
template<class FUNCTOR, class... Args> //PARAM_TUPLE>
class SerializedFunctor <FUNCTOR, void, Args...> : public SerializedFunctor_base<FUNCTOR, Args...>
{
public:
/// @memberof SerializedFunctor has the sample implementation as that of the base class SerializedFunctor_base, for achieving the serialization operation for the member variable of p_, i.e. the parameters of the to-be-wrapped functor f_
	SerializedFunctor (FUNCTOR& f, Args... args)
		: SerializedFunctor_base<FUNCTOR, Args...>(f, args...)
	{}
};

/// @brief a class capable of having any incoming functor equipped with serialization implementation. 
/// tparam RETURN representing the type of the return value.
template<class RETURN>
class JobSerializer
{
public:
/// @memberof JobSerializer capable of create a newly created functor, with full serialization capability and in the type of SerializedFunctor < std::function<RETURN()>, RETURN, Args... >, based on any type of incoming functor f.
/// @tparam FUNCTOR representing the to-be-wrapped functor's type.  
/// @tparam Args defined in variadic form, so as to represent possible arguments of FUNCTOR
/// @return a functor with the same functionality as the incoming functor f, but with full serialization capability.  The return value is in type of SerializedFunctor < std::function<RETURN()>, RETURN, Args... >
	template<class FUNCTOR, typename ...Args>
	static SerializedFunctor< std::function< RETURN()>, RETURN, Args... > create (FUNCTOR f, Args... args)
	{	
		std::function<RETURN ()> func = std::bind( f, args...);
		SerializedFunctor<  std::function<RETURN ()> , RETURN , Args... > BoundFunctor ( func, args... );
		return BoundFunctor;
	} 
};

#endif
