#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include "gtest/gtest.h"
//template <typename T>
class BMSearch
{
	std::string pattern, txt;
	size_t m, n;
	std::map<char, size_t> bad_char_map;
	std::map<size_t, size_t> kmp_map, good_sufx_map;
public:
	BMSearch ( std::string& str_pattern, std::string& str_txt)
		: pattern (str_pattern), txt (str_txt)
		, m (pattern.size()), n (txt.size())
	{
		badmatchprocess();// (str_txt, str_pattern);
		kmpprocess();
		goodsufxprocess();
	}

	void badmatchprocess(void)
	{
		for (int i=0; i < m; i++)
			bad_char_map[pattern[i]] = i;
	}

	void kmpprocess (void) 
	{
	//having kmp_map[i] indicating the start position index of the widest border of the suffix taken from position i.
	//having good_sufx_map[i] indicating the shift distance when pattern[i] is matched, while pattern[i-1] is not mismatched.
	//in an example that the pattern has the value of "abbabab", the index i and kmp_map[i] are mapped as followed:
	// i				: 0 1 2 3 4 5 6 7 
	// pattern 			: a b b a b a b
	// kmp_map[i]		: 5 6 4 5 6 7 7 8
	// good_sufx_map[i]	: 0 0 0 0 2 0 4 1 
		size_t i = m, j = m+1;
		kmp_map [i] = j;
		while (i>0)
		{
			while (j<=m && pattern[i-1] != pattern[j-1])
			{
				if (good_sufx_map[j]==0) 
					good_sufx_map[j]=j-i;
				j=kmp_map[j];
			}
			i--; j--;
			kmp_map[i]=j;
		}
	}

	void goodsufxprocess (void)
	{
	// having the un-valued element of the good_sufx_map filled with value of kmpprocess[0], i.e. the starting position of the widest border
		size_t i, j;
		j = kmp_map[0];
		for (i=0; i<=m; i++)
		{
			if (good_sufx_map[i]==0) 
				good_sufx_map[i]=j;
			if (i==j)
				j=kmp_map[j];
		}
	}

	int bmSearch (std::vector<int>& found_vec)
	{
		size_t i=0;
		int j=m-1, found=-100;
		while ( i<=n-m )
		{
			std::cerr<<"current i, j, found "<<i<<'\t'<<j<<'\t'<<found<<std::endl;
			while (j>=0 && pattern[j]==txt[i+j]) 
				j--;
std::cerr<<"matching till i, j "<<i<<'\t'<<j<<std::endl;
			if (j<0)
			{
				found = i;
				found_vec.push_back (found);
				i += good_sufx_map[0];
				j = m-1;
			}
			else if (j == m-1) 
			{
std::cerr<<"initially not matched\ni, j, good_sufx_map[j+1], and bad_char_map[txt[i+j]] "<<i<<'\t'<<j<<'\t'<<good_sufx_map[j+1]<<'\t'<<bad_char_map[txt[i+j]]<<std::endl;
				if (bad_char_map.find (txt[i+j])->second == 0)
//				if (bad_char_map.find (txt[i+j])==bad_char_map.end())
				{
std::cerr<<"bad_char_map search not found "<<std::endl;
					i+=m;
std::cerr<<"udated i, current n-m "<<i<<'\t'<<n-m<<std::endl;
				}
				else
				{
std::cerr<<"bad_char_map searched "<<std::endl;
					std::cerr<<"txt[i+j] "<<i+j<<'\t'<<txt[i+j]<<'\t'<<bad_char_map[txt[i+j]]<<std::endl;
					i += j-bad_char_map[txt[i+j]];
std::cerr<<"udated i, current n-m "<<i<<'\t'<<n-m<<std::endl;
				}
			}
			else if (j < m-1) 
			{
std::cerr<<"partial match\ni, j, good_sufx_map[j+1], and bad_char_map[txt[i+j]] "<<i<<'\t'<<j<<'\t'<<good_sufx_map[j+1]<<'\t'<<bad_char_map[txt[i+j]]<<std::endl;
				i += good_sufx_map [j+1];
std::cerr<<"udated i, current n-m "<<i<<'\t'<<n-m<<std::endl;
				j = m-1;
			}
		}
std::cerr<<"end while, with i, found "<<i<<'\t'<<found<<std::endl;
		if (found!=-100)
			return found;
		else
			return -1;
	}

	int bmSearch (void)
	{
		size_t i=0;
		int j=m-1, found=-100;
		while ( i<=n-m )
		{
			while (j>=0 && pattern[j]==txt[i+j]) 
				j--;
			if (j < 0)
			{
				found = i;
				i += good_sufx_map[0];
				j = m-1;
			}
			else if (j == m-1) 
				i += j-bad_char_map[txt[i+j]];
			else if (j < m-1) 
			{
				i += good_sufx_map [j+1];
				j = m-1;
			}
		}
		if (found!=-100)
			return found;
		else
			return -1;
	}

	void Print ()
	{
		std::cerr<<"obtained bad_char_map, with size of "<<bad_char_map.size()<<std::endl;
		std::for_each (bad_char_map.begin(), bad_char_map.end(),
		[] (const std::pair<char, int>& Q)
		{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;});
		
		std::cerr<<"obtained kmp_map, with size of "<<kmp_map.size()<<std::endl;
		std::for_each (kmp_map.begin(), kmp_map.end(),
		[] (const std::pair<size_t, size_t>& Q)
		{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;});
	
		std::cerr<<"obtained good_sufx_map, with size of "<<good_sufx_map.size()<<std::endl;
		std::for_each (good_sufx_map.begin(), good_sufx_map.end(),
		[] (const std::pair<size_t, size_t>& Q)
		{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;});
	}
};
