#include <iostream>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <set>
#include <map>
#include "gtest/gtest.h"
//#include "bm_test.hpp"
#include <fstream>
#include <random>
template <size_t MISMATCH_COUNT>
class UkkonenMismatch
{
public:
	std::string pattern, txt;
	size_t m, n, length;
	std::string alphabet;
//	std::string alphabet;
	std::unordered_map<size_t, std::unordered_map<char, size_t> >Ukx_map;//	std::map<char, std::vector<size_t> >Ukx_map;
public:
	UkkonenMismatch (std::string& str_pattern, std::string& str_txt)
		: pattern (str_pattern), txt (str_txt)
		, m (pattern.size()), n (txt.size()), length (MISMATCH_COUNT + 1)
//		, alphabet ("acgtuACGTU")
	{
//std::cerr<<"m, n "<<m<<'\t'<<n<<std::endl;
		for (size_t i=0; i!=txt.length(); ++i)
		{
			if (alphabet.find(txt[i],0)==std::string::npos)
			alphabet.push_back (txt[i]);
		}

        for (size_t i=0; i!=pattern.length(); ++i)
        {
            if (alphabet.find(pattern[i],0)==std::string::npos)
            alphabet.push_back (pattern[i]);
        }
        UkxProcess();
    }

    void UkxProcess (void)
    {
        for (size_t i=0; i!=alphabet.size(); ++i)
        {
            for (auto j=m-1; j>=m-MISMATCH_COUNT-1; --j)
                Ukx_map[j][alphabet[i]] = m-MISMATCH_COUNT;
        }

        for (size_t j=0; j!=length; ++j)
        {
            for (size_t i=0; i < m; i++)
            {
                if (m-1 > i+j)
                    Ukx_map[m-j-1][pattern[i]]=m-i-1-j;
            }
        }
    }

	void Ukkonen (std::vector<int>& found_vec)
	{
		size_t temp;
//		size_t i=0;
		size_t j=m-1;//, found=-100;
		size_t distance;
		size_t y = m-MISMATCH_COUNT;
		while ( j <= n-1) //n+MISMATCH_COUNT-1 )
		{
//std::cerr<<"current j , current n-1"<<'\r';//j<<'\t'<<n-1<<std::endl;
			int i=m-1, h=j;
			size_t neq=0;
			distance = y;
			while (i>=0 && neq<=MISMATCH_COUNT)
			{
//std::cerr<<"current i and neq and distance "<<i<<'\r';//<<neq<<'\t'<<distance<<std::endl;
				if ( i>=y-1)
				{
					temp = Ukx_map[i][txt[h]];
					if ( temp < distance)
						distance = temp;
//std::cerr<<"i, , txt[h], updating distance "<<i<<'\t'<<h<<'\t'<<txt[h]<<'\t'<<Ukx_map[i][txt[h]]<<'\t'<<distance<<std::endl;
				}
				if (txt[h] != pattern[i])
				{
//std::cerr<<"txt[h] != pattern[i], with h, i, txt[h], pattern[i], neq "<<h<<'\t'<<i<<txt[h]<<'\t'<<pattern[i]<<'\t'<<neq<<std::endl;
					++neq;
				}
				--i, --h;
			}
//std::cerr<<"end while with neq and distance "<<neq<<'\t'<<distance<<std::endl; 
			if (neq<=MISMATCH_COUNT)
			{
//std::cerr<<"found at j "<<j<<std::endl;
				found_vec.push_back (j);
			}
			j+=distance;
		}
	}

	void linear_mismatch (std::vector<int>& found_vec)
	{
		size_t i=0;
		int jj=m-1, found=-100;
		while ( i<=n-m )
		{
//std::cerr<<"current i "<<'\r';
			size_t mmc = MISMATCH_COUNT;
			int j=jj;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
//std::cerr<<"current j and neq and distance "<<'\r';
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
			{
				found = i+jj;
				found_vec.push_back (found);
				++i;
			}   
			else
				++i;
		}
	}

	void Print ()
	{
		std::cerr<<"pattern, with size of "<<pattern.size()<<'\t'<<pattern<<std::endl;
		std::cerr<<"txt, with size of "<<txt.size()<<'\t'<<txt<<std::endl;

		std::cerr<<"obtained alphabet, with size of "<<alphabet.size()<<std::endl;
		std::for_each ( alphabet.begin(), alphabet.end(),
			[] (const char& Q)
			{ std::cerr<<Q<<std::endl; });
	
		std::cerr<<"obtained Ukx_map, with size of "<<Ukx_map.size()<<std::endl;
		std::for_each ( Ukx_map.begin(), Ukx_map.end(),
			[] (const std::pair<size_t, std::unordered_map<char, size_t> >& Q)
			{ std::cerr<<Q.first<<'\n'; 
			std::for_each (Q.second.begin(), Q.second.end(),
				[] (const std::pair<char, size_t>& q)
				{ std::cerr<<q.first<<'\t'<<q.second<<std::endl; });
			std::cerr<<std::endl;
			});
	}
};

void get_sample (std::string& pattern, std::string& txt, std::vector<size_t>& result)
{
	std::cerr<<pattern<<'\n'<<txt.size()<<'\n'<<result.size()<<std::endl;
	size_t pos = 0;
	while (1)
	{
		size_t i = txt.find (pattern, pos);
std::cerr<<"currnet i "<<i<<std::endl;
		if (i==std::string::npos)
			break;
		else
		{
			result.push_back (i);
			pos=i+1;
		}
	}
}

//std::vector <std::string> pattern_vec;
std::vector <std::string> txt;
std::string pattern ("ABCDEFGHIJKLMNOPQRSTUV");

TEST (random_gen, string)//(bm_mismatch, test)
{
//	std::string pattern("ACGTANG"), txt ("ABCDEFGACGT");
//	std::string pattern("AAGTCGTAAC"), txst ("ABCDEFGAAGTCGTAAC");
	std::ifstream ofs ("/Users/obigbando/Downloads/human_chr/chrY.fa");
	std::string s2;
	int i =0;
	std::getline (ofs, s2);
	while (i<10000)//(!ofs.eof())//(i<1187473)//100000)
	{
//		std::cerr<<i<<'\r';
		std::getline (ofs,s2) ;
		txt.push_back (s2);
//		txt+=s2;
		++i;
	}
//std::cerr<<'\n'<<'\n'<<std::endl;
}

TEST (compare, linear)
{
	for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
	{
//		std::cout<<"current pattern "<<*itr<<std::endl;
//		BMSearch qq (*itr, txt);
		UkkonenMismatch<2> q (pattern, *itr);
		std::vector<int> found_vec0;
		q.linear_mismatch (found_vec0);
	}
}

TEST (Ukkonen1, UKKONEN)
{
	for (auto itr=txt.begin(); itr!=txt.end(); ++itr)
	{
		std::cout<<"current pattern "<<*itr<<std::endl;
		UkkonenMismatch<2> q (pattern, *itr);
		q.Print();
		std::vector<int> found_vec0;
		q.Ukkonen (found_vec0);
	}
}


/*
TEST (bm, test)
{
//  std::string pattern ("abbabab"), txt ("abcdefghijklmnopqrstuvwxyzabbabababcdefghijklmnopqrstuvwxyzabbabab");	
//	std::string pattern ("abbabac"), txt ("abcdefghijklmnopqrstuvwxyzabbabababcdefghijklmnopqrstuvwxyzabbabab");	
//	std::string pattern ("AAGTCGTAAC"), txt("AAGTCGTAACAACTGTTAACTTGCGACTAGAAGTCGTAAC");

	std::ifstream ofs ("/Users/obigbando/Downloads/human_chr/chrY.fa");
	std::string txt, s2;
	std::getline (ofs,s2) ;
	int i =0;
	while (!ofs.eof())
	{
		std::cerr<<i<<'\r';
		std::getline (ofs,s2) ;
		txt+=s2;
		++i;
	}
	std::string pattern ("aTCCCAGGACCAAACCCATCTATATTCTGTAACCCGAAACCTCAAAGccg");//, txt("acgtacggaggtccgtaaaaccccggggttttacgtacgt");

//	std::cerr<<"txt.substr(12799, 16496, 95875 "<< txt.substr (12749, 50) <<'\n' <<txt.substr (16446, 50) <<'\n' <<txt.substr(95825, 50)<<std::endl;
//	std::vector<size_t> sample;
//	get_sample (pattern, txt, sample);

	UkkonenMismatch<2> qq (pattern, txt);
//	qq.Print();
	std::vector<int>vec;
	qq.Ukkonen (vec);
	std::cerr<<"vec's content"<<std::endl;
	std::for_each (vec.begin(), vec.end(),
		[&] (const int& q)
		{ std::cerr<<q<<std::endl;
			for (auto i=0; i!=50; ++i)
			{
				int ii=0;
				if (txt.substr(q-50, 50)[i] != pattern[i])
					++ii;
				ASSERT_TRUE (ii<=2);
			}
		});

	std::vector<int>vec_l;
	qq.linear_mismatch (vec_l);
	std::cerr<<"vec_l's content"<<std::endl;
	std::for_each (vec_l.begin(), vec_l.end(),
		[&] (const int& q)
		{ std::cerr<<q<<std::endl;
			for (auto i=0; i!=50; ++i)
			{
				int ii=0;
				if (txt.substr(q-50, 50)[i] != pattern[i])
					++ii;
				ASSERT_TRUE (ii<=2);
			}
		});

	EXPECT_EQ (vec, vec_l);

}
*/
