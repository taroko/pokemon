#include <array>
#include <cstdint>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <map>
#include <set>
//#include "gtest/gtest.h"
//#include "bm_mismatch_update.cpp"
#include "boost/mpl/string.hpp"
#include "boost/mpl/char.hpp"

/* 
// dynamic solotion 
template <size_t MISMATCH_COUNT, size_t MATCH_COUNT>
class FAAST
{
public:
	std::string pattern, txt;
	size_t m, n, length;
//	std::set<char> alphabet;
	std::string alphabet;
	std::map<size_t, std::unordered_map<char, std::set<size_t> > >Ukx_map;
	std::unordered_map<size_t, std::unordered_map<std::string, size_t > >Vkx_map;
	std::unordered_map<std::string, size_t> Dkx_map;
public:

	FAAST (std::string& str_pattern, std::string& str_txt)
		: pattern (str_pattern), txt (str_txt)
		, m (pattern.size()), n (txt.size()), length (MISMATCH_COUNT + MATCH_COUNT)
	{
		for (auto i=0; i!=txt.length(); ++i)
		{
			if (alphabet.find(txt[i],0)==std::string::npos)
			alphabet.push_back (txt[i]);
		}
		for (auto i=0; i!=pattern.length(); ++i)
		{
			if (alphabet.find(pattern[i],0)==std::string::npos)
			alphabet.push_back (pattern[i]);
		}
		UkxProcess();
		VkxProcess();
	}

	FAAST (std::string& str_pattern, std::string& str_txt, int GG)
		: pattern (str_pattern), txt (str_txt)
		, m (pattern.size()), n (txt.size()), length (MISMATCH_COUNT + MATCH_COUNT)
	{
		for (auto i=0; i!=txt.length(); ++i)
		{
			if (alphabet.find(txt[i],0)==std::string::npos)
			alphabet.push_back (txt[i]);
		}
		for (auto i=0; i!=pattern.length(); ++i)
		{
			if (alphabet.find(pattern[i],0)==std::string::npos)
			alphabet.push_back (pattern[i]);
		}
		std::vector<std::string> qq;
		pattern_generate (qq);
	}

	void UkxProcess (void)
	{
//		for (size_t i=0; i!=alphabet.size(); ++i)
//		{
//			for (auto j=m; j>m-MISMATCH_COUNT-MATCH_COUNT; --j)
//				Ukx_map[j][alphabet[i]].insert(m-MISMATCH_COUNT);
//		}
		for (size_t j=0; j!=MISMATCH_COUNT+MATCH_COUNT; ++j)
		{
			for (size_t i=0; i < m; i++)
			{
				if (m-1 > i+j)
					Ukx_map[m-j][pattern[i]].insert (m-i-1-j);
			}
		}
	}

	void pattern_generate (std::vector<std::string>& count_vec)
	{	
		count_vec.reserve (pow(alphabet.size(), MISMATCH_COUNT+MATCH_COUNT));
		std::string temp (MISMATCH_COUNT+MATCH_COUNT, alphabet[0]);
		pat<MISMATCH_COUNT+MATCH_COUNT-1>::pattern_fill (temp, count_vec, alphabet);
	}

	template <size_t INDEX, typename T = int>
	struct pat
	{
		static void pattern_fill (std::string& temp, std::vector<std::string>& count_vec, std::string& alpha )
		{
			std::for_each (alpha.begin(), alpha.end(), 
				[&] (const char& q)
				{ temp[INDEX] = q;
				pat<INDEX-1, T>::pattern_fill (temp, count_vec, alpha); });
		}
	};

	template <typename T>
	struct pat <0, T>
	{
		static void pattern_fill (std::string& temp, std::vector<std::string>& count_vec, std::string& alpha )
		{
			std::for_each (alpha.begin(), alpha.end(),
				[&] (const char& q)
				{ 	temp[0] = q;
					count_vec.push_back (temp); });
		}
	};

	void VkxProcess (void)
	{		
		std::vector<std::string> count_vec;
		pattern_generate (count_vec);
//		std::for_each (count_vec.begin(), count_vec.end(),
//		[&] (const std::string& Q)
//		{
		for (auto itr=count_vec.begin(); itr!=count_vec.end(); ++itr)
		{
//std::cerr<<"current *itr "<<*itr<<std::endl;
			for (size_t l=1; l<=m-MISMATCH_COUNT; ++l)// std::unordered_map<size_t, std::unordered_map<std::string, size_t > >Vkx_map;
			{
//std::cerr<<"continued "<<std::endl;
				//Vkx_map[l][*itr] = 0;
				for (size_t i=m-MISMATCH_COUNT-MATCH_COUNT+1; i<=m; ++i)
				{
					if (Ukx_map[i][(*itr)[i-MISMATCH_COUNT-MATCH_COUNT-1]].find (l) != Ukx_map[i][(*itr)[i-MISMATCH_COUNT-MATCH_COUNT-1]].end())
						++Vkx_map[l][*itr];
				}
//std::cerr<<"MatcH_COUNT, m-MISMATCH_COUNT, l "<<MATCH_COUNT<<'\t'<<m-MISMATCH_COUNT<<'\t'<<l<<std::endl;
				auto min = m-MISMATCH_COUNT-l;
				if (MATCH_COUNT < min)
					min = MATCH_COUNT;
				if (Vkx_map[l][*itr] >= min)
				{
					Dkx_map[*itr] = l;
//std::cout<<"Dkx_map["<<*itr<<"] "<<l<<std::endl;
//std::cerr<<"min Vkx_map[l][*itr] Dkx_map[*itr] "<<min<<'\t'<<Vkx_map[l][*itr]<<'\t'<<l<<'\t'<<Dkx_map[*itr]<<std::endl;
					break;
				}
			}
		}
	}

	void faast_search (std::vector<size_t>& found_vec)
	{
		size_t i=0;
		int jj=m, found=-100;
		while ( i<=n-m )
		{
			size_t mmc = MISMATCH_COUNT;
			int j=jj-1;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
				found_vec.push_back (i+jj-1);//(found);
			i+=Dkx_map[txt.substr(jj+i-length, length)];
		}
	}

	void linear_mismatch (std::vector<size_t>& found_vec)
	{   
		size_t i=0,  jj=m-1;
		int found=-100;
		while ( i<=n-m )
		{
//std::cerr<<"current i "<<'\r';
			size_t mmc = MISMATCH_COUNT;
			int j=jj;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
//std::cerr<<"current j and neq and distance "<<'\r';
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
			{
				found = i+jj;
				found_vec.push_back (found);
				++i;
			}
			else
				++i;
		}
	}

	void Print (void)
	{
		std::for_each ( Ukx_map.begin(), Ukx_map.end(),
			[] (const std::pair<size_t, std::unordered_map<char, std::set<size_t> > >& Q)
			{ std::cerr<<Q.first<<'\t'; 
			std::for_each (Q.second.begin(), Q.second.end(),
				[] (const std::pair<char, std::set<size_t> >& q)
				{ std::cerr<<q.first<<'\t'; 
					std::for_each (q.second.begin(), q.second.end(),
					[] (const size_t& qq)
					{ std::cerr<<qq<<' '; });
				});
			std::cerr<<std::endl;
			});

		std::cerr<<Vkx_map.size()<<std::endl;
		std::for_each (Vkx_map.begin(), Vkx_map.end(),
			[&] (const std::pair<size_t, std::unordered_map<std::string, size_t> >& Q)
			{ 
			std::cerr<<Q.first<<'\n'<<Q.second.size()<<std::endl;
			std::for_each (Q.second.begin(), Q.second.end(),
				[&] (const std::pair <std::string, size_t>& q)
				{ std::cerr<<q.first<<'\t'<<q.second<<std::endl;});
			});

		std::for_each (Dkx_map.begin(), Dkx_map.end(),
			[&] (const std::pair<std::string, size_t>& Q)
			{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;});
	}
};
*/

template <size_t MISMATCH_COUNT>
class LinearSearch
{
	std::string pattern, txt;
	size_t m, n, length;
	std::string alphabet;
public:
	LinearSearch (std::string& str_pattern, std::string& str_txt)
		: pattern (str_pattern), txt (str_txt)
		, m (pattern.size()), n (txt.size()), length (MISMATCH_COUNT + 1)
	{
		for (size_t i=0; i!=txt.length(); ++i)
		{
			if (alphabet.find(txt[i],0)==std::string::npos)
			alphabet.push_back (txt[i]);
		}   
		for (size_t i=0; i!=pattern.length(); ++i)
		{
			if (alphabet.find(pattern[i],0)==std::string::npos)
			alphabet.push_back (pattern[i]);
		}
	}

	void linear_mismatch (std::vector<size_t>& found_vec)
	{
		size_t i=0, jj=m-1;
		int found=-100;
		while ( i<=n-m )
		{
//std::cerr<<"current i "<<'\r';
			size_t mmc = MISMATCH_COUNT;
			int j=jj;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
//std::cerr<<"current j and neq and distance "<<'\r';
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
			{
				found = i+jj;
				found_vec.push_back (found);
				++i;
			}
			else
				++i;
		}
	}
};

template <size_t ALPHASIZE, size_t INDEX>
struct power
{
	static size_t const value = power <ALPHASIZE, INDEX-1> ::value * ALPHASIZE;
};

template <size_t ALPHASIZE>
struct power <ALPHASIZE, 0>
{
	static size_t const value = 1;
};

constexpr const char alphabet[11]= "acgtACGNTU";

template <size_t MISMATCH_COUNT, size_t MATCH_COUNT, size_t INDEX, size_t COUNT, size_t ALPHASIZE>
struct pattern_trait
{
	typedef typename boost::mpl::push_back< 
		typename pattern_trait <
			MISMATCH_COUNT, MATCH_COUNT, INDEX-1, COUNT-power <ALPHASIZE,INDEX>::value*(COUNT / power<ALPHASIZE, INDEX>::value), ALPHASIZE
			>::qq, 
			boost::mpl::char_<alphabet[COUNT / power<ALPHASIZE, INDEX>::value]>   
	>::type qq;	
//recursively add wrapper mpl::pusk_back from the first level, i.e. the #INDEX pattern_trait, to the one before last level, i.e. the #1 pattern_trait, so that a form
//of mpl::push_back < mpl::push_back < mpl::push_back <... pattern_trait#INDEX-2>:: type,  pattern_trait #INDEX-1>::type, pattern_trait #INDEX> :: type is formed
};

template <size_t MISMATCH_COUNT, size_t MATCH_COUNT, size_t COUNT, size_t ALPHASIZE>
struct pattern_trait <MISMATCH_COUNT, MATCH_COUNT, 0, COUNT, ALPHASIZE>
{
	typedef typename boost::mpl::push_back<
      boost::mpl::string<>,   // The empty string
      boost::mpl::char_<alphabet[COUNT % ALPHASIZE]>
    >::type qq;
//providing the end condition for the mpl::push_back typedef, wherein when the recursion goes down to the last level, i.e. the #0 pattern trait, the corresponding
//qq is defined to include the empty string and the last char.
};


//#include <boost/mpl/vector_c.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/for_each.hpp>
template <size_t MISMATCH_COUNT, size_t MATCH_COUNT, size_t INDEX, size_t COUNT, size_t ALPHASIZE>
struct traverse
{
	typedef typename boost::mpl::push_back<
		typename traverse < MISMATCH_COUNT, MATCH_COUNT, INDEX, COUNT-1, ALPHASIZE > :: qqq, 
		typename pattern_trait <MISMATCH_COUNT, MATCH_COUNT, INDEX, COUNT, ALPHASIZE>::qq
	>::type qqq;
};

template <size_t MISMATCH_COUNT, size_t MATCH_COUNT, size_t INDEX, size_t ALPHASIZE>
struct traverse <MISMATCH_COUNT, MATCH_COUNT, INDEX, 0, ALPHASIZE>
{
	typedef typename boost::mpl::push_back<
		boost::mpl::vector<>,
		typename pattern_trait <MISMATCH_COUNT, MATCH_COUNT, INDEX, 0, ALPHASIZE>::qq
	>::type qqq;
};

struct value_printer
{
	template< typename U > void operator() (U x)
	{
		std::cerr<<boost::mpl::c_str<U>::value<<std::endl;
	}
};

int main (void)
{
	boost::mpl::for_each < traverse<1,1, 1, 99, 10>::qqq > (value_printer() ); 
}


