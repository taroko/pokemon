#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdint>
#include <cmath>
#include <array>
#include <string>
#include <unordered_map>
#include <map>
#include <set>
#include <deque>
#include <random>
#include "gtest/gtest.h"
#include "boost/unordered_map.hpp" 

template <size_t MISMATCH_COUNT, size_t MATCH_COUNT>
class FAAST
{
public:
	std::string pattern;
	size_t pattern_length, txt_length, mask_length;
	std::string alphabet;
	boost::unordered_map<size_t, boost::unordered_map<int, std::set<size_t> > >UkxMap;
	mutable std::vector<std::pair<int, std::string> > MapTable;
	mutable boost::unordered_map <char, int> ReverseAlphabet;
	mutable std::vector<int> MapTableRecord;
public:
	FAAST (std::string& str_pattern)
		: pattern (str_pattern)
		, pattern_length (pattern.size()), mask_length (MISMATCH_COUNT + MATCH_COUNT)
		, alphabet ("ACGNT")
	{
 		GetReverseAlphabet();
		GetUkxMap();
		GetMapTable();
	}

    FAAST (std::string& str_pattern, int i)	//for compare
        : pattern (str_pattern)//, txt (str_txt)
        , pattern_length (pattern.size()), mask_length (MISMATCH_COUNT + MATCH_COUNT)
        , alphabet ("ACGNT")
    {}
	
	void GetReverseAlphabet(void)
	{//ReverseAlphabet generation
		int index=0;
		std::for_each (alphabet.begin(), alphabet.end(),
		[&] (const char& q)
		{ ReverseAlphabet.insert (std::make_pair (q, index));
			++index; 
		});
	}

	void GetUkxMap (void)
	{//UkxMap
		for (size_t a=0; a!=alphabet.size(); ++a)
		{
			for (auto i=pattern_length; i>=pattern_length-MISMATCH_COUNT-MATCH_COUNT+1; --i)
		  		UkxMap[i][a].insert(pattern_length-MISMATCH_COUNT);
 		}

		for (size_t i=pattern_length; i>=pattern_length-MISMATCH_COUNT-MATCH_COUNT+1; --i)
		{
			for (size_t s=i-1; s>=1; --s)
			{
				if (i-s < pattern_length-MISMATCH_COUNT)
					UkxMap[i][ReverseAlphabet[pattern[s]]].insert (i-s);
			}	
		}
	}

	void GetMapTable (void)
	{
//StrVec generation
		boost::unordered_map<size_t, boost::unordered_map<int, size_t > >VkxMap;
		std::vector<std::string> StrVec;
		pattern_generate (StrVec);

//MapTable initialization
		size_t vecsize = pow(16, MISMATCH_COUNT+MATCH_COUNT+1);
		MapTable.resize (vecsize);
		int value_indicator = -5566;

		std::for_each (StrVec.begin(), StrVec.end(),
		[&] (const std::string& q)
		{
			int result = 0;
			for (auto i = 0; i != q.size(); ++i)
				result += (ReverseAlphabet[q[i]] << (i<<2));//	result+=ReverseAlphabet[q[i]] *pow(alphabet.size(), i);
			MapTable[result]= {value_indicator, q};

			MapTableRecord.push_back (result);
		});

//MapTable 
        for (auto itr=MapTableRecord.begin(); itr!=MapTableRecord.end(); ++itr)
//		for (auto itr=0; itr!=vecsize; ++itr)
		{
/*
//			if (MapTable[itr].first!=value_indicator)//-5566)
			if (MapTableRecord.find (itr) == MapTableRecord.end())	//not verified yet
				continue;
*/
			for (size_t l=1; l<=pattern_length-MISMATCH_COUNT; ++l)// boost::unordered_map<size_t, boost::unordered_map<std::string, size_t > >VkxMap;
			{
				VkxMap[l][MapTable[*itr].first] = 0 ; //use insert for instead	//takes 4 secs instead!! 
//				VkxMap[l].insert (std::make_pair (MapTable[itr].first, 0));	//takes 16 secs under FAAST<2,3,> situation; emplace doesn't do any help !!
//				VkxMap[l].insert ({MapTable[itr].first, 0});					//takes 16 secs under FAAST<2,3,> situation; emplace dosen't do any help !!

				for (size_t i=pattern_length; i>=pattern_length-MISMATCH_COUNT-MATCH_COUNT+1; --i)
				{
					if (UkxMap[i][ReverseAlphabet[MapTable[*itr].second[MISMATCH_COUNT+MATCH_COUNT-1-pattern_length+i]]].find (l) != 
							UkxMap[i][ReverseAlphabet[MapTable[*itr].second[MISMATCH_COUNT+MATCH_COUNT-1-pattern_length+i]]].end())
						VkxMap[l][MapTable[*itr].first]+=1;
				}

				if (VkxMap[l][MapTable[*itr].first] >= std::min (MATCH_COUNT, pattern_length-MISMATCH_COUNT-l))
				{// make modification on original faast since we found that if we have MapTable[*itr]-1 when l>1, then we can have correct answer
					if (l>1)
						MapTable[*itr].first = l-1;
					else
						MapTable[*itr].first = l;
					break;
				}
			}
		}
	}

	bool find//faast_search 
			(std::string& txt, std::vector<size_t>& found_vec)//, int i)
	{
		txt_length = txt.size();
		bool return_value = false;

		size_t j = pattern_length ;
		while (j<=txt_length)
		{
			int h = j, i=pattern_length, e=0;
			while (i>0 && e<=MISMATCH_COUNT)
			{
				if (txt[h-1]!=pattern[i-1])
					++e;
				--i, --h;
			}
			if (e <= MISMATCH_COUNT)
			{
				found_vec.push_back (j-1);
				return_value = true;
			}
			int result=0;
			for (auto i=0; i!=mask_length; ++i)
//				result+=ReverseAlphabet[txt[j-length+i]] *pow(alphabet.size(), i);
				result += (ReverseAlphabet[ txt[j-mask_length+i] ] << (i<<2));

			j+=MapTable[result].first;
		} 
		return return_value;
	}

	void pattern_generate (std::vector<std::string>& StrVec)
	{	
		StrVec.reserve (pow(alphabet.size(), MISMATCH_COUNT+MATCH_COUNT));
		std::string r (MISMATCH_COUNT+MATCH_COUNT, '1');
		std::deque<std::string> pp;
		GetCombination (alphabet, 0, r, 0, alphabet.size()-MISMATCH_COUNT-MATCH_COUNT, pp);

		std::string repetitive_str;
		std::for_each (alphabet.begin(), alphabet.end(),
			[&] (const char& q)
			{( repetitive_str += ( std::string (MISMATCH_COUNT+MATCH_COUNT, q) ) ); } );

//std::cerr<<"repetitive_str "<<repetitive_str<<std::endl;
		std::string r2 (MISMATCH_COUNT+MATCH_COUNT, '1');
		GetCombination (repetitive_str, 0, r2, 0, alphabet.size()*(MISMATCH_COUNT+MATCH_COUNT)-MISMATCH_COUNT-MATCH_COUNT, pp);

		std::set<std::string> Q (pp.begin(), pp.end());

		std::for_each (Q.begin(), Q.end(),
		[&] (std::string q)
		{
//std::cerr<<"current combination "<<q<<std::endl;
//size_t i=1;	
			std::sort (q.begin(), q.end());
			StrVec.push_back (q);
			while ( std::next_permutation (q.begin(), q.end()) )
			{
//std::cerr<<i<<"th permutation "<<q<<std::endl;
//++i;
				StrVec.push_back (q);
			}
		});
	}

	void GetCombination (std::string target_str, size_t target_column, std::string result, size_t result_column, int loop, std::deque<std::string>& container)
	{
		int r_size = result.size(), localloop = loop, local_target_column = target_column;
	
		//A different combination is out
		if (result_column > r_size-1)//(r_column>(r_size-1))
		{
			container.push_back (result);
			return;
		}
		//===========================
	
		for(auto i=0; i<=loop; ++i)
		{
			auto it1 = result.begin();//rbegin;
			for(auto cnt=0; cnt<result_column; ++cnt)
				++it1;
			auto it2 = target_str.begin();//nbegin;
			for(auto cnt2=0; cnt2<target_column+i; ++cnt2)
				++it2;
			*it1=*it2;
	
			++local_target_column;
			GetCombination(target_str, local_target_column, result, result_column+1, localloop, container);
			--localloop;
		}
	}	

	bool linear_mismatch (std::string& txt, std::vector<size_t>& found_vec)
	{   
		txt_length = txt.size();
		if (txt_length<pattern_length)
			return false;
		
		bool return_value=false;
		size_t i=0,  jj=pattern_length-1;
		int found=-100;
		while ( i<=txt_length-pattern_length )
		{
//std::cerr<<"current i "<<'\r';
			size_t mmc = MISMATCH_COUNT;
			int j=jj;
			while ( j>=0 && (pattern[j]==txt[i+j] || mmc!=0) )
			{
//std::cerr<<"current j and neq and distance "<<'\r';
				if (pattern[j]!=txt[i+j])
					--mmc;
				--j;
			}
			if (j < 0)
			{
				found = i+jj-pattern_length;
				found_vec.push_back (found);
				++i;
				return_value=true;
			}
			else
				++i;
		}

//		found_vec.push_back (txt_length-1);

		return return_value;
	}

	void Print (void)
	{
		std::cerr<<"alphabet "<<std::endl;
		std::cerr<<alphabet<<std::endl;

		std::cerr<<"UkxMap "<<std::endl;
		std::for_each ( UkxMap.begin(), UkxMap.end(),
			[&] (const std::pair<size_t, boost::unordered_map<int, std::set<size_t> > >& Q)
			{ std::cerr<<Q.first<<'\t'; 
			std::for_each (Q.second.begin(), Q.second.end(),
				[&] (const std::pair<int, std::set<size_t> >& q)
				{ std::cerr<<alphabet[q.first]<<'\t';
					std::cerr<<"q.first "<<q.first<<std::endl; 
					std::for_each (q.second.begin(), q.second.end(),
					[] (const size_t& qq)
					{ std::cerr<<qq<<' '; });
				});
			std::cerr<<std::endl;
			});

		std::cerr<<"MapTable "<<std::endl;
		std::for_each (MapTable.begin(), MapTable.end(),
			[&] (const std::pair<int, std::string>& Q)
			{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;});
	}

    void clearMapTable (void)
    {
        std::for_each (MapTableRecord.begin(), MapTableRecord.end(),
            [&] (const int& Q)
            {   MapTable[Q] = {0, 0}; 
                MapTableRecord.clear(); 
            });
    }
};

