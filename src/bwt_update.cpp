#ifndef BWT_HPP_
#define BWT_HPP_
#include "compression/n_bit_compress_update.hpp"
#include <fstream>
#include <map>
#include <utility>
#include <list>
#include <random>
#include <algorithm>
#include <functional>
#include <boost/ref.hpp>
#include <boost/bind.hpp>
#include "thread_pool.hpp"
#include "constant_def.hpp"
//#include "mpi_pool.hpp"
//#include "mpi_job_creator.hpp"
#include "gtest/gtest.h"

template <typename T>
struct compare_NBC
{
	NBitCompress<T>& content;
	uint64_t content_len;
	compare_NBC (NBitCompress<T>& Seq)
		: content (Seq)
		, content_len (content.GetSize()/T::bit_count)
	{}	
	bool operator() (const uint64_t index_1, const uint64_t index_2)
	{
//std::cerr<<"content_len, index_1 and NB2 "<<content_len<<'\t'<<index_1<<'\t'<<index_2<<std::endl;
		if (index_1 >= content_len || index_2 >= content_len)
		{
			//std::cerr<<"content_len, index_1 and NB2 "<<content_len<<'\t'<<index_1<<'\t'<<index_2<<std::endl;
			std::cerr<<"what the fuck"<<std::endl;
			return false;
		}
		if (index_1==index_2)
			return false;
		uint64_t index=0;
		uint64_t len_1 = content_len - index_1;
		uint64_t len_2 = content_len - index_2;
		uint32_t NB1_Except_count = 0, NB2_Except_count = 0;
		while( index+index_1 < content_len && index+index_2 < content_len )
		{
			char chr1 = content[index+index_1];
			NB1_Except_count = content.Except_count;
			char chr2 = content[index+index_2];
			NB2_Except_count = content.Except_count;
  			if (chr1 < chr2)
				return true;
			else if (chr1 > chr2)
				return false;
			else if (NB1_Except_count>0 && NB2_Except_count>0)
			{
				if (NB1_Except_count > NB2_Except_count)
					index + NB2_Except_count;
				else
					index + NB1_Except_count;

		   		chr1 = content[index+index_1];
				chr2 = content[index+index_2];

				if (chr1 < chr2)
					return true;
				else if (chr1 > chr2)
					return false;
			}
			index++;
		}
		if (index_1 < index_2)
			return false;
		else
			return true;
	}
};

//Generic Version
template <ParallelTypes ParaType, typename T, uint64_t ModeValue >//= 3095693984> //3095693983 + 1 ($)
class BWT 
{};

//MPI Version
/*
template <typename T, uint64_t ModeValue >//= 3095693984> //3095693983 + 1 ($)
class BWT <ParallelTypes::M_P_I, T, ModeValue>
{
public:
//private:
	MPIPool* ptr_to_MPIPool;
	uint16_t Num_sort_Od1;
	NBitCompress<T> content;
	std::vector< std::pair< uint64_t, NBitCompress<T> > > random_sort_temp;
	std::map < int, std::vector < std::pair<uint64_t, NBitCompress<T> > > >* result;
public:
	BWT ( NBitCompress<T>& Seq, 
			uint16_t Num, 
			std::map < int, std::vector < std::pair<uint64_t, NBitCompress<T> > > >* ptr,
			MPIPool* tp )
		: content (Seq)
		, Num_sort_Od1 (Num)
		, result (ptr)
		, ptr_to_MPIPool (tp)
		, random_sort_temp ()
	{
std::cerr<<"BWT array processing"<<std::endl;
		random_sort_temp.reserve (Num_sort_Od1);
		std::linear_congruential_engine < uint64_t, 2, 19, ModeValue > RandomGen;
		uint64_t a=43;
		RandomGen.seed (a);

		for (auto index=0; index!=Num_sort_Od1; ++index)
		{
			RandomGen.discard (11);
			NBitCompress<T> x;
			auto i = RandomGen();
			Seq.GetSubNBitCompress (i, x);
std::cerr<<"i "<<i<<'\r';
			random_sort_temp.emplace_back (std::make_pair(i, x));
		}
std::cerr<<"done random selecting"<<std::endl;
		std::sort (random_sort_temp.begin(), random_sort_temp.end(), compare<T>);
		random_sort_temp.erase (std::adjacent_find (random_sort_temp.begin(), random_sort_temp.end()));
std::cerr<<"randomly sorted"<<std::endl;
	 }	

	void SortOd2_impl (int index, std::vector < std::pair<uint64_t, NBitCompress<T> > >& tempOd2)
	{
tempOd2.reserve (10000);
std::cerr<<"sorting index #"<<index<<'\r';
		if (index==0)//			SortObject< 0 > :: sort_impl (tempOd2);
		{
std::cerr<<"index==0"<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
std::cerr<<"in for loop #"<<i<<'\r';
				NBitCompress<T> temp;
				content.GetSubNBitCompress (i, temp);
				if (i==random_sort_temp[0].first)
					continue;
				if (compare_NBC<T>(temp, random_sort_temp.begin()->second))
					tempOd2.emplace_back (std::make_pair(i, temp));
			}
			std::sort (tempOd2.begin(), tempOd2.end(), compare<T>); 
		}
		else if (index==random_sort_temp.size())
		{
std::cerr<<"index==end"<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
std::cerr<<"in for loop #"<<i<<'\r';
				NBitCompress<T> temp;
				content.GetSubNBitCompress (i, temp);
				if (i==random_sort_temp.back().first)
					continue;
				if (compare_NBC<T>(random_sort_temp.back().second, temp))
					tempOd2.emplace_back (std::make_pair(i, temp));
			}	
			std::sort (tempOd2.begin(), tempOd2.end(), compare<T>); 
		}
		else
		{
std::cerr<<"index!0 && !=end"<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
std::cerr<<"in for loop #"<<i<<'\r';
				NBitCompress<T> temp;
				content.GetSubNBitCompress (i, temp);
				if (i==random_sort_temp[index-1].first || i==random_sort_temp[index].first)
					continue;
				if (compare_NBC<T> (random_sort_temp[index-1].second, temp) &&
					compare_NBC<T> (temp, random_sort_temp[index].second))
					tempOd2.emplace_back (std::make_pair(i, temp));
			}	
			std::sort (tempOd2.begin(), tempOd2.end(), compare<T>); 
		}
	}

	void SortOd2 ()//(std::map < int, std::vector < std::pair<uint64_t, NBitCompress<T> > > >* result)
	{
		std::vector < boost::shared_ptr<boost::mpi::packed_iarchive> > arc_temp;
		for ( auto index=0; index<=random_sort_temp.size(); ++index)
		{
			auto x = JobSerializer<void> :: create ( 
				std::bind (
					&BWT::SortOd2_impl, this,
					std::placeholders::_1,
					std::placeholders::_2 
							),
				index, 
				std::ref((*result)[index])
							);
			arc_temp.push_back ( ptr_to_MPIPool -> JobPost ( x ) );
		}			   
		ptr_to_MPIPool->FlushPool();

		if ( ptr_to_MPIPool->world.rank() == 0)
		{
			for ( auto index=0; index<=random_sort_temp.size(); ++index)
			{
				auto x = JobSerializer<void> :: create (
					std::bind (
						&BWT::SortOd2_impl, this,
						std::placeholders::_1,
						std::placeholders::_2
								),
					index,
					std::ref((*result)[index])
								);
				*(arc_temp[index]) >> x;
				(*result)[index] = std::get<1>(x.p_).get();
			}
		}
	}

	void GetLFStr_impl (int index, std::string& sstr)
	{
		std::for_each ( (*result)[index].begin(), (*result)[index].end(),
			[&] (const std::pair<uint64_t, NBitCompress<T> >& Q)
			{	if (Q.first==0) sstr += content[content.GetSize()/T::bit_count-1];
				else sstr += content [Q.first-1];
			});
//		std::cerr<<"for index "<<index<<" str is obtained as "<<sstr<<std::endl;
		if (index < random_sort_temp.size())
		{
			if (random_sort_temp[index].first==0)
				sstr += content [(content.GetSize()/T::bit_count)-1];
			else
				sstr += content [random_sort_temp[index].first-1];
		}
	}

	void GetLFStr (std::string& LFString)
	{
		std::vector < boost::shared_ptr<boost::mpi::packed_iarchive> > arc_temp;
		std::map <int, std::string> str_temp;
		for (auto index=0; index!=result->size(); ++index)
		{
			auto x = JobSerializer<void> :: create (
				std::bind (
					&BWT :: GetLFStr_impl, 
					std::placeholders::_1,
					std::placeholders::_2
							), 
				index,
				std::ref(str_temp[index])
							);
			arc_temp.push_back ( ptr_to_MPIPool -> JobPost ( x ) );
		}
		ptr_to_MPIPool->FlushPool();
		if ( ptr_to_MPIPool->world.rank() == 0)
		{
			for (auto index=0; index!=result->size(); ++index)
			{
				auto x = JobSerializer<void> :: create ( 
					std::bind (
						&BWT :: GetLFStr_impl,
						std::placeholders::_1,
						std::placeholders::_2
								),
					index,
					std::ref(str_temp[index])
								);
				*(arc_temp[index]) >> x;
				str_temp[index] = std::get<1>(x.p_).get();
			}
			for (auto i=0; i!=result->size(); ++i)
				LFString+=str_temp[i];
		}
	}

	void Print_random_sort (void)
	{
		std::for_each (random_sort_temp.begin(), random_sort_temp.end(),
			[&] (const std::pair <uint64_t, NBitCompress<T> >& Q)
			{ std::cerr<< Q.first << '\t' << Q.second.GetSize()/T::bit_count <<std::endl;});
	}

	void Print_result (void)
	{
		std::for_each (result->begin(), result->end(),
			[] ( std::pair <int, std::vector <std::pair <uint64_t, NBitCompress<T> > > > Q)
			{ std::cerr<< "\n\titem of "<<Q.first << '\n';
				std::for_each (Q.second.begin(), Q.second.end(),
					[&] (std::pair<uint64_t, NBitCompress<T> >& q)
					{ std::cerr<<q.first<<'\n'<<q.second.MakeSeqString()<<std::endl;});
			});
	}
};


//MT Version
template <typename T, uint64_t ModeValue >//= 3095693984> //3095693983 + 1 ($)
class BWT <ParallelTypes::M_T, T, ModeValue>
{
public:
//private:
	uint16_t Num_sort_Od1;
	NBitCompress<T> content;
	std::vector< uint64_t > random_sort_temp;
	std::map < int, std::vector < uint64_t > >* result;
	compare_NBC <T> IsSmallerThan;
	ThreadPool* ptr_to_GlobalPool;

public:
	BWT ( NBitCompress<T>& Seq, 
			uint16_t Num, 
			std::map < int, std::vector <uint64_t> >* ptr, 
			ThreadPool* tp = &GlobalPool  )
		: content (Seq)
		, IsSmallerThan (content)
		, Num_sort_Od1 (Num)
		, result (ptr)
		, random_sort_temp ()
		, ptr_to_GlobalPool (tp)
	{
std::cerr<<"BWT array processing"<<std::endl;
		random_sort_temp.reserve (Num_sort_Od1);
		std::linear_congruential_engine < uint64_t, 2, 19, ModeValue > RandomGen;
		uint64_t a=43;
		RandomGen.seed (a);
		for (auto index=0; index!=Num_sort_Od1; ++index)
		{
			RandomGen.discard (10);
			uint64_t i = RandomGen();
			std::cerr<<"i "<<i<<'\t';
			random_sort_temp.push_back (i);//(std::make_pair(i, x));
		}
std::cerr<<"done random selecting"<<std::endl;
		std::sort (random_sort_temp.begin(), random_sort_temp.end(), IsSmallerThan);

		std::for_each (random_sort_temp.begin(), random_sort_temp.end(),
			[&] (const uint64_t& Q)
			{ std::cerr<<Q<<std::endl;});
	}

	void SortOd2_impl (int index, std::vector <uint64_t>& tempOd2)
	{
		tempOd2.reserve (10000);
std::cerr<<"sorting index #"<<index<<'\n';
		if (index==0)
		{
//std::cerr<<"index==begin"<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
				if ( IsSmallerThan (i, random_sort_temp.front()) )
					tempOd2.push_back (i);
			}
//std::cerr<<"tempOd2's size "<<tempOd2.size()<<std::endl;
			std::sort (tempOd2.begin(), tempOd2.end(), IsSmallerThan); 
		}
		else if (index==random_sort_temp.size())
		{
//std::cerr<<"index==end"<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
				if ( IsSmallerThan (random_sort_temp.back(), i) )
					tempOd2.push_back (i);
			}	
//std::cerr<<"tempOd2's size "<<tempOd2.size()<<std::endl;
			std::sort (tempOd2.begin(), tempOd2.end(), IsSmallerThan); 
		}
		else
		{
//std::cerr<<"index!0 && !=end"<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
				if ( IsSmallerThan (random_sort_temp[index-1], i) &&
					IsSmallerThan (i, random_sort_temp[index]))
					tempOd2.push_back (i);
			}	
//std::cerr<<"tempOd2's size "<<tempOd2.size()<<std::endl;
			std::sort (tempOd2.begin(), tempOd2.end(), IsSmallerThan); 
		}
	}

	void SortOd2 ()//(std::map < int, std::vector < std::pair<uint64_t, NBitCompress<T> > > >* result)
	{
//std::cerr<<"random_sort_temp.size() "<<random_sort_temp.size()<<std::endl;
		for ( auto index=0; index<=random_sort_temp.size(); ++index)
		{
//std::cerr<<"job posting #"<<index<<std::endl;
			ptr_to_GlobalPool -> JobPost ( boost::bind
				( &BWT::SortOd2_impl, this, index, boost::ref( (*result)[index] ) ) );  
		}
//std::cerr<<"done post"<<std::endl;
		ptr_to_GlobalPool->FlushPool(); // since the Read_impl, implemented with the get_next_entry function of each FileRea
	}

	void GetLFStr_impl (int index, std::string& sstr)
	{
		std::for_each ( (*result)[index].begin(), (*result)[index].end(),
			[&] (const uint64_t& Q)
			{	if (Q==0) sstr += content[content.GetSize()/T::bit_count-1];
				else sstr += content [Q-1];
			});
//		std::cerr<<"for index "<<index<<" str is obtained as "<<sstr<<std::endl;
		if (index < random_sort_temp.size())
		{
			if (random_sort_temp[index]==0)
				sstr += content [(content.GetSize()/T::bit_count)-1];
			else
				sstr += content [random_sort_temp[index]-1];
		}
	}

	void GetLFStr (std::string& LFString)
	{
		std::map <int, std::string> str_temp;
		for (auto index=0; index!=result->size(); ++index)
		{
			ptr_to_GlobalPool -> JobPost ( boost::bind
				( &BWT::GetLFStr_impl, this, index, boost::ref( str_temp[index] ) ) );  
		}
		ptr_to_GlobalPool->FlushPool(); // since the Read_impl, implemented with the get_next_entry function of each FileRea
		for (auto i=0; i!=result->size(); ++i)
			LFString+=str_temp[i];
	}

	void Print_random_sort (void)
	{
		std::for_each (random_sort_temp.begin(), random_sort_temp.end(),
		[&] (const uint64_t Q)
		{	std::cerr<<Q<<'\n';
			NBitCompress<T> temp; 
			content.GetSubNBitCompress(Q, temp);
			std::cerr<<temp.MakeSeqString()<<std::endl;
		});	
	}

	void Print_result (void)
	{
		std::for_each (result->begin(), result->end(),
			[&] ( std::pair <int, std::vector <uint64_t> > Q)
			{ std::cerr<< "\n\titem of "<<Q.first << '\n';
				std::for_each (Q.second.begin(), Q.second.end(),
					[&] (uint64_t& q)
					{ std::cerr<<q<<std::endl;
					NBitCompress<T> temp;
					content.GetSubNBitCompress (q, temp);
					std::cerr<<temp.MakeSeqString()<<std::endl;
					});
			});
	}
};
*/

// Sequential Version
template <typename T, uint64_t ModeValue >//= 3095693984> //3095693983 + 1 ($)
class BWT <ParallelTypes::NORMAL, T, ModeValue>
{
public:
//private:
	uint16_t Num_sort_Od1;
	NBitCompress<T>& content;
	std::vector< uint64_t > random_sort_temp;
	std::map < int, std::vector < uint64_t > >* result;
	compare_NBC <T> IsSmallerThan;
	ThreadPool* ptr_to_GlobalPool;

public:
	BWT ( NBitCompress<T>& Seq, 
			uint16_t Num, 
			std::map < int, std::vector <uint64_t> >* ptr) 
		: content (Seq)
		, IsSmallerThan (content)
		, Num_sort_Od1 (Num)
		, result (ptr)
		, random_sort_temp ()
//		, ptr_to_GlobalPool (tp)
	{
std::cerr<<"BWT array processing"<<std::endl;
		random_sort_temp.reserve (Num_sort_Od1);
		std::linear_congruential_engine < uint64_t, 2, 19, ModeValue > RandomGen;
		uint64_t a=43;
		RandomGen.seed (a);
		for (auto index=0; index!=Num_sort_Od1; ++index)
		{
			RandomGen.discard (10);
			uint64_t i = RandomGen();
			std::cerr<<"i "<<i<<'\r';
			random_sort_temp.push_back (i);//(std::make_pair(i, x));
		}
std::cerr<<"done random selecting"<<std::endl;
		std::sort (random_sort_temp.begin(), random_sort_temp.end(), IsSmallerThan);

		std::for_each (random_sort_temp.begin(), random_sort_temp.end(),
			[&] (const uint64_t& Q)
			{ if (Q>ModeValue) std::cerr<<"FUCK"<<std::endl;
				else std::cerr<<Q<<'\t';
//			NBitCompress<T> temp;
//			content.GetSubNBitCompress (Q, temp);
//			std::cerr<<temp.MakeSeqString()<<std::endl;
			});
	}

	void SortOd2_impl (int index, std::vector <uint64_t>& tempOd2)
	{
		tempOd2.reserve (60000);
std::cerr<<"sorting index #"<<index<<'\n';
		if (index==0)
		{
std::cerr<<"index==begin, with ModeValue= "<<ModeValue<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
				if ( IsSmallerThan (i, random_sort_temp.front()) )
				{
					std::cerr<<"obtained i "<<i<<'\t';
					tempOd2.push_back (i);
				}
			}
		}
		else if (index==random_sort_temp.size())
		{
std::cerr<<"index==end, with ModeValue= "<<ModeValue<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
				if ( IsSmallerThan (random_sort_temp.back(), i) )
				{
					std::cerr<<"obtained i "<<i<<'\t';
					tempOd2.push_back (i);
				}
			}	
		}
		else
		{
std::cerr<<"index!0 && !=end, with ModeValue "<<ModeValue<<'\n';
			for (auto i=0; i!=ModeValue; ++i)
			{
				if ( IsSmallerThan (random_sort_temp[index-1], i) &&
					IsSmallerThan (i, random_sort_temp[index]))
				{
					std::cerr<<"obtained i "<<i<<'\t';
					tempOd2.push_back (i);
				}
			}	
		}
		std::cout<<"tempOd2's size "<<tempOd2.size()<<std::endl;
		std::for_each (tempOd2.begin(), tempOd2.end(),
			[] (uint64_t& Q)
			{ if (Q>=ModeValue) std::cerr<<"WHAT A SUCKER"<<std::endl;});
		//	std::cerr<<Q<<'\n';
	//		});
		std::sort (tempOd2.begin(), tempOd2.end() , IsSmallerThan); 
	}

	void SortOd2 ()//(std::map < int, std::vector < std::pair<uint64_t, NBitCompress<T> > > >* result)
	{
		for ( auto index=0; index<=random_sort_temp.size(); ++index)
//		int index=0;
		{
			std::cerr<<"handling SortOd2 for "<<index<<'\t'<<random_sort_temp[index]<<std::endl;
			SortOd2_impl (index, boost::ref((*result)[index] ) ) ;
		}
	}

	void GetLFStr_impl (int index, std::string& sstr)
	{
		std::for_each ( (*result)[index].begin(), (*result)[index].end(),
			[&] (const uint64_t& Q)
			{	if (Q==0) sstr += content[content.GetSize()/T::bit_count-1];
				else sstr += content [Q-1];
			});
//		std::cerr<<"for index "<<index<<" str is obtained as "<<sstr<<std::endl;
		if (index < random_sort_temp.size())
		{
			if (random_sort_temp[index]==0)
				sstr += content [(content.GetSize()/T::bit_count)-1];
			else
				sstr += content [random_sort_temp[index]-1];
		}
	}

	void GetLFStr (std::string& LFString)
	{
		std::map < int, std::string > str_temp; 
		for (auto index=0; index!=result->size(); ++index)
			GetLFStr_impl (index, str_temp[index]);
		for (auto i=0; i!=result->size(); ++i)
			LFString+=str_temp[i];
	}

	void Print_random_sort (void)
	{
		std::for_each (random_sort_temp.begin(), random_sort_temp.end(),
		[&] (const uint64_t Q)
		{	std::cerr<<Q<<'\n';
			NBitCompress<T> temp; 
			content.GetSubNBitCompress(Q, temp);
			std::cerr<<temp.MakeSeqString()<<std::endl;
		});	
	}

	void Print_result (void)
	{
		std::for_each (result->begin(), result->end(),
			[&] ( std::pair <int, std::vector <uint64_t> > Q)
			{ std::cerr<< "\n\titem of "<<Q.first << '\n';
				std::for_each (Q.second.begin(), Q.second.end(),
					[&] (uint64_t& q)
					{ std::cerr<<q<<std::endl;
					NBitCompress<T> temp;
					content.GetSubNBitCompress (q, temp);
					std::cerr<<temp.MakeSeqString()<<std::endl;
					});
			});
	}
};

#endif


TEST(basic, Normal)
//int main (void)
{
	std::string s1 ("ZZZYYYXXXWWWVVVUUUTTTSSSRRRQQQPPPOOONNNMMMLLLKKKJJJIIIHHHGGGFFFEEEDDDCCCBBBAAA$");
	NBitCompress <CompressRuleTrait<2, CompressType::N_BITS> > input (s1);
//	compare_NBC<CompressRuleTrait<2, CompressType::N_BITS> > compare (input);
	input.Printer();

	std::map<int, std::vector < uint64_t > > sort_result, sort_resultM;
//	BWT<ParallelTypes::NORMAL, CompressRuleTrait<2, CompressType::N_BITS>, 27> Q (input, 4, &sort_result);
	BWT<ParallelTypes::NORMAL, CompressRuleTrait<2, CompressType::N_BITS>, 79> Q (input, 4, &sort_result);
	Q.SortOd2();
	Q.Print_result();
	std::string LFSTRING;
	Q.GetLFStr(LFSTRING);
//	std::cerr<<"got LFSTRING "<<LFSTRING<<std::endl;
/*
	BWT<ParallelTypes::M_T, CompressRuleTrait<2, CompressType::N_BITS>, 79> QM (input, 4, &sort_resultM);
//	QM.ptr_to_GlobalPool -> ChangePoolSize (2);
	QM.SortOd2 ();//(&sort_result);
	QM.Print_result();
	EXPECT_EQ (sort_result, sort_resultM);
	std::string LFSTRING2;
	QM.GetLFStr(LFSTRING2);
//	std::cerr<<"got LFSTRING "<<LFSTRING<<std::endl;
	EXPECT_EQ (LFSTRING, LFSTRING2);
*/
}

#include <fstream>
TEST(chrY, Normal)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s1, s2;
	std::getline (ofs, s2);
	std::cerr<<"constructing str of chrY"<<std::endl;
	int i =0;
	while(i!=500)//
	{
		std::getline (ofs,s2) ;
		++i;
	}
	int j=0;
	while(j!=1000)//
	{
		std::getline (ofs,s2) ;
		s1+=s2;
		++j;
	}

//	std::string s1 ("ZZYYXXWWVVUUTTSSRRQQPPOONNMMLLKKJJIIHHGGFFEEDDCCBBAA$");
	std::cerr<<"chrY constructed"<<std::endl;
	std::cerr<<s1<<std::endl;
	std::cerr<<"with size "<<s1.size()<<std::endl;

	NBitCompress <CompressRuleTrait<2, CompressType::N_BITS> > input (s1);
	std::map<int, std::vector < uint64_t > > sort_result;
	std::cerr<<"1st order sorting"<<std::endl;
	BWT<ParallelTypes::NORMAL, CompressRuleTrait<2, CompressType::N_BITS>, 50000> QQ (input, 50, &sort_result);	
//	BWT<ParallelTypes::NORMAL, CompressRuleTrait<2, CompressType::N_BITS>, 53> QQ (input, 4, &sort_result);
	std::cerr<<"1st order sorted"<<std::endl;
	std::cerr<<"2nd order sorting"<<std::endl;
	QQ.SortOd2 ();
	std::cerr<<"2nd order sorted"<<std::endl;

	QQ.Print_result();

	std::string LFSTRING;
	QQ.GetLFStr (LFSTRING);
	std::cerr<<LFSTRING<<std::endl;;

	std::ofstream ofs1 ("/Users/obigbando/Documents/work/test_file/model.fa");
	ofs1 << s1;
	ofs1.close();
	std::ofstream ofs2 ("/Users/obigbando/Documents/work/test_file/test_serial.fa");
	ofs2 << LFSTRING;
	ofs2.close();
}
/*
TEST(chrY, MT)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s1, s2;
	std::getline (ofs, s2);
	std::cerr<<"constructing str of chrY"<<std::endl;
	int i =0;
	while(i!=500)//
	{
		std::getline (ofs,s2) ;
		++i;
	}
	int j=0;
	while(j!=1000)//
	{
		std::getline (ofs,s2) ;
		s1+=s2;
		++j;
	}

//	std::string s1 ("ZZYYXXWWVVUUTTSSRRQQPPOONNMMLLKKJJIIHHGGFFEEDDCCBBAA$");
	std::cerr<<"chrY constructed"<<std::endl;
	std::cerr<<"with size "<<s1.size()<<std::endl;

	NBitCompress <CompressRuleTrait<2, CompressType::N_BITS> > input (s1);
	std::map<int, std::vector < uint64_t > > sort_resultM;
	std::cerr<<"1st order sorting"<<std::endl;

//	BWT<ParallelTypes::M_T, CompressRuleTrait<2, CompressType::N_BITS>, 53> QM (input, 4, &sort_resultM);
	BWT<ParallelTypes::M_T, CompressRuleTrait<2, CompressType::N_BITS>, 50000> QM (input, 250, &sort_resultM);	
//	QM.ptr_to_GlobalPool -> ChangePoolSize(1);
	std::cerr<<"1st order sorted"<<std::endl;
	std::cerr<<"2nd order sorting"<<std::endl;
	QM.SortOd2 ();
	std::cerr<<"2nd order sorted"<<std::endl;

	std::string LFSTRING;
	QM.GetLFStr (LFSTRING);
	std::cerr<<LFSTRING<<std::endl;

	std::ofstream ofs2 ("/Users/obigbando/Documents/work/test_file/test_MT.fa");
	ofs2 << LFSTRING << std::endl;;
	ofs2.close();

	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/test_serial.fa");
	std::string test;
	std::getline (ifs, test);
	EXPECT_EQ (test, LFSTRING);
}
*/
// Speed: for 100 lines of ChrY (with string size of 5K, 1st random sort 50), serial SortOd2 & GetLFStr takes 5 min on Mac
// Speed: for 100 lines of ChrY (with string size of 5K, 1st random sort 50), MT SortOd2 & GetLFStr takes 2.2 sec on Mac
