///@file parser.hpp
///@brief parser main part, designed as a derived class from FilerReader and Encoder 
///@author C-Salt Corp.
#ifndef PARSER_HPP_
#define PARSER_HPP_
#include <deque>
#include <map>
#include <unordered_map>
#include "constant_def.hpp"
#include "file_reader.hpp"
#include "encoder.hpp"
/// @class FileParser
/// @brief Implemented with Policy technique, wherein FileReader and Encoder classes are instantiated according to their corresponding template parameters \n FileParser accordingly succeed the file-reading and compressing functionlity of FileReader and Encoder classes via multiple classes inheritance
/// @tparam ParallelType non-type enum parameter, indicating current parallel processing scheme
/// @tparam FORMAT template template parameter, indicating what formats, e.g. one of the Bed (BigBed), FastA, FastQ, Sam (Bam), or Wig respectively defined as a template classes, the FileParser is going to parse.
/// @tparam TUPLETYPE type parameter, indicating the data structures held by FORMAT
/// @tparam CompressType non-type enum parameter, indicating current compression scheme
/// @tparam CONTAINER template template parameter, indicating what kind of container the FileParser will be inherited from
/// @tparam CompressBitCount non-type parameter, indicating how many bits the compression scheme of the Encoder is going to take.
template< ParallelTypes ParaType, 
			template < typename > class FORMAT, 
			typename TUPLETYPE, 
			CompressType CompType,
			template < typename... > class CONTAINER,
			int CompressBitCount
		 >
class FileParser 
	: public FileReader < ParaType, FORMAT, TUPLETYPE >
	, public Encoder < CompressBitCount, CompType >
	, public CONTAINER < FORMAT<TUPLETYPE> >
{
private:
	std::map <int, std::vector<FORMAT<TUPLETYPE> > > content;	//a dummy member element, which is firstly default constructed, then its address is applied to have the FileReader initialized.
public:
	FileParser ()
        : FileReader < ParaType, FORMAT, TUPLETYPE > ( &content )	
		, content ()
		//though it seems unreasonable to pass an object's address for constructing before that object's initialization, 
		//the memory allocation is actually done right after the call of FileParser's constructor.  As such, passing 
		//the already memory allocated object content's address for initualization of FileReader is totally legitimate, 
		//even the content is still not initialized.  
		//If not the address, but the value of content is employed for initialiation of FileReader, the aforementioned scheme will lead to undefined situation
    {}
	void Parse (void)
	{
		/*please write your code for parsing data*/
		//auto x = this->read();
		//std::for_each (	this->result->begin(), this->result->end(),
		//				[this] (const std::pair<int, FORMAT<TUPLETYPE> >& Q)
		//				{ this->push_back (Q.second);}
		//				);
	}
};

/// @brief specialized form of the FileReader, with the template template parameter of CONTAINER specialized to be std::map container
template< ParallelTypes ParaType, 
			template < typename > class FORMAT, 
			typename TUPLETYPE, 
			CompressType CompType,
			int CompressBitCount
		>	
class FileParser < ParaType, FORMAT, TUPLETYPE, CompType, std::map, CompressBitCount >
//since map cotainer, such as normal map and newly added unordered_map takes elements of std::pair type, rather than the fundamental type taken by the rest of std container, we can't properly have the map pcontainer properly covered with the generic form of CONTAINER, so that the corresponding specialization forms are provided. 
	: public FileReader < ParaType, FORMAT, TUPLETYPE >
	, public Encoder < CompressBitCount, CompType >
	, public std::map < int, FORMAT<TUPLETYPE> >
{
private:
	std::map <int, std::vector< FORMAT<TUPLETYPE> > > content;	//a dummy member element, which is firstly default constructed, then its address is applied to have the FileReader initialized.
public:
	FileParser ()
        : FileReader < ParaType, FORMAT, TUPLETYPE > ( &content )
		, content ()
    {}
	void Parse (void)
	{
		/*please write your code for parsing data*/
		//auto x = this->read();
		//std::for_each (	this->result->begin(), this->result->end(),
		//				[this] (const std::pair<int, FORMAT<TUPLETYPE> >& Q)
		//				{ this->push_back (Q.second);}
		//				);
	}
};

/// @brief specialized form of the FileReader, with the template template parameter of CONTAINER specialized to be std::unordered_map container
template< ParallelTypes ParaType, 
			template < typename > class FORMAT, 
			typename TUPLETYPE, 
			CompressType CompType,
			int CompressBitCount
		>
class FileParser < ParaType, FORMAT, TUPLETYPE, CompType, std::unordered_map, CompressBitCount >
	: public FileReader < ParaType, FORMAT, TUPLETYPE >
	, public Encoder < CompressBitCount, CompType >
	, public std::unordered_map < int, FORMAT<TUPLETYPE> >
{
private:
	std::map <int, std::vector< FORMAT<TUPLETYPE> > > content;	//a dummy member element, which is firstly default constructed, then its address is applied to have the FileReader initialized.
public:
	FileParser ()
        : FileReader < ParaType, FORMAT, TUPLETYPE > ( &content )
		, content ()
    {}
	void Parse (void)
	{
		/*please write your code for parsing data*/
		//auto x = this->read();
		//std::for_each (	this->result->begin(), this->result->end(),
		//				[this] (const std::pair<int, FORMAT<TUPLETYPE> >& Q)
		//				{ this->push_back (Q.second);}
		//				);
	}
};
#endif
