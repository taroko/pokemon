/**
 *  @file annotation.hpp
 *  @brief 組裝 Aligner
 *  @author JHH Corp.
 */
#ifndef ANNOTATION_HPP_
#define ANNOTATION_HPP_
#include <vector>
#include <string>
#include <initializer_list>
#include <functional>
#include <iostream>
#include <map>
#include <tuple>
#include <set>
#include <algorithm>
//#include "../../pokemon.old/pokemon/src/file_reader_impl.hpp"
//#include "../../pokemon.old/pokemon/src/format/bed.hpp"
//#include <boost/type_traits/is_same.hpp>
#include <boost/type_traits.hpp>
#include "../file_reader_impl.hpp"
#include "../format/bed_reader_impl.hpp"
#include "../format/bed.hpp"
#include "../format/annotation_raw_bed.hpp"

template<int I>
class IntType{};

template<class TUPLE, class VECTOR, int Start, int End, class bool_type = boost::false_type>
class tuple2vector
{
public:
	tuple2vector(const TUPLE &t, VECTOR &v)
	{
		v.push_back( std::get<Start>(t) );
		typedef typename boost::is_same<IntType<Start>, IntType<End-1>>::type is_same_type;
		//if(Start != End)
		//{
		tuple2vector<TUPLE,VECTOR,Start+1,End, is_same_type> tmp(t,v);
		//}
	}
};

template<class TUPLE, class VECTOR, int Start, int End>
class tuple2vector<TUPLE, VECTOR, Start, End, boost::true_type>
{
public:
	tuple2vector(const TUPLE &t, VECTOR &v){}
};


/**
 * @brief Annotation 通用版本
 * @tparam PARSER_TYPE File parser for read bed to database
 * @tparam ANNOTRAIT 其他相關設定，目前只有 db file path
 * @tparam IGNORE_STRAND 是否忽略 strand
 * @tparam ANNOTYPE Annotation type, e.g., interset 特化 annotation 的實作
 */

template <	class PARSER_TYPE,
			class ANNOTRAIT,
			int IGNORE_STRAND,
			int ANNOTYPE
			>
struct Annotation
{};


/**
 * @brief Interset 特化版本， read 只要有交集到 gene 的 1 base 就會被 annotate
 * @tparam PARSER_TYPE File parser for read bed to database
 * @tparam ANNOTRAIT 其他相關設定，目前只有 db file path
 * @tparam IGNORE_STRAND (enum) 是否忽略 strand，strand 有 + - .
 * @tparam AnnoType::INTERSET (enum) Annotation type, e.g., intersrt 特化 annotation 的實作
 */
template <  class PARSER_TYPE,
			class ANNOTRAIT,
			int IGNORE_STRAND
			>
struct Annotation < PARSER_TYPE, ANNOTRAIT, IGNORE_STRAND, AnnoType::INTERSET>
	:public ANNOTRAIT
{
	
	/// @brief database BED 完整 type，包含 tuple
	typedef typename PARSER_TYPE::format_type BED_TYPE;
	
	/// @brief database BED 的 Tuple type
	typedef typename PARSER_TYPE::TupleType BED_TUPLE_TYPE;
	
	/// @brief 用來記錄 chromosome 中，最常的 gene，可用來做比對的加速
	std::map < std::string, uint32_t > max_length;
	
	/// @brief 記錄的 Database
	static std::map < BED_TYPE, uint32_t> gdatabase;
	
	static std::mutex gAnnotation_db_mutex;

	static bool gdb_established;

	/// @memberof Annotation < PARSER_TYPE, ANNOTRAIT, IGNORE_STRAND, AnnoType::INTERSET>
	Annotation()
	{
		std::lock_guard<std::mutex> lock(gAnnotation_db_mutex);
		{
			if (!gdb_established)
			{
				create_db_from_file ();
				gdb_established = true;
			}
			else
				;
		}
	}

	/**
	 * @fn create_db_from_file
	 * @brief 建構 database, 用 parser 讀取檔案，然後塞入 database，順便記錄chromosome 內 gene 最大長度
	 * @return void
	 */
	inline void create_db_from_file ()
	{
		
		//std::cerr << "create_db_from_file: " << this->file_path << std::endl;
		
		/// @brief 建構 file parser
		/// @brief this->file_path 是繼承 ANNOTRAIT 得到
		std::vector<std::string> file_list({this->file_path});
		PARSER_TYPE db_file_reader(file_list);
		
		while ( 1 )
		{
			/// @brief type of db_bed is Bed< std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string> >
			auto db_bed = db_file_reader.get_next_entry ( 0 );

			if ( db_bed.eof_flag )		
			{
				break;	
			}
			else;
			
			/// @brief insert bed to database
			gdatabase[ db_bed ] = 1;

			std::string &tmp_db_chr = std::get<0>(db_bed.data);
			uint32_t tmp_db_length = std::get<2>(db_bed.data) - std::get<1>(db_bed.data);
			
			/// @brief record max gene length for quick stop
			if ( max_length [ tmp_db_chr ] < ( tmp_db_length ) )
			{
				max_length [ tmp_db_chr ] = ( tmp_db_length );
			}
			else;
		}
		

	}
	
	/**
	 * @brief 此 Annotate 為 interset 功能，只要有 read 交集 gene, read 的 annotation_info_ 會 push_back anno string
	 * @tparam PARSER_TYPE File parser for read bed to database
	 * @param read 為要搜尋 database的read, 必須有 member variable: chromosome_, start_, end_, strand_, annotation_info_
	 * @return int 回傳對中的 gene 數目
	 */
	inline int Annotate ( AnnotationRawBed<>& read , int db_idx)
	{
	
		int annotation_number = 0;
		
		//auto db_iter = database.lower_bound( BED_TYPE ( std::make_tuple(read.chromosome_, read.end_, 0, read.strand_, "", "") ) );
		BED_TUPLE_TYPE tmp_tuple;
		std::get<0>(tmp_tuple) = read.chromosome_;
		std::get<1>(tmp_tuple) = read.end_;
		std::get<2>(tmp_tuple) = 0;
		
		auto db_iter = gdatabase.lower_bound( BED_TYPE ( std::move(tmp_tuple) ) );
		
		/// @brief lower_bound search 是搜尋到 read_end <= 
		/// @brief 所以在一般情況下要先往上一個指標
		//if(db_iter != database.begin() && std::get<1>( (db_iter->first).data)  != read.end_ )
		//	--db_iter;
		
		
		auto gene_start = std::get<1>( (db_iter->first).data );
		auto gene_end = std::get<2>( (db_iter->first).data );
		long tmp_gene_max = (long)(read.start_) - (long)(gene_start)+1 ;
		
		auto max_gene_length = max_length[read.chromosome_];
		
		/// @brief 如果 gene長度已經大於最大 gene長度則終止（加速用）
		while(tmp_gene_max <= max_gene_length )
		{
			/// @brief chromosome 不對，則已經高過範圍，直接終止
			if(read.chromosome_ != std::get<0>( (db_iter->first).data ) )
				break;
			
			/// @brief 檢查 strand
			bool is_strand_ok (true);
			if(IGNORE_STRAND == AnnoIgnoreStrand::NO_IGNORE)
			{
				char gene_strand = std::get<3>( (db_iter->first).data );
				if( gene_strand != '.' && read.strand_ != gene_strand)
				{
					is_strand_ok = false;
				}
			}
			
			gene_start = std::get<1>( (db_iter->first).data );
			gene_end = std::get<2>( (db_iter->first).data );
			tmp_gene_max = (long)(read.start_) - (long)(gene_start)+1 ;
			
			/// @brief 第一個條件是 gene and read start or and ，第二個條件是 strand 條件是否有符合
			if ( ! (read.end_ < gene_start || read.start_ > gene_end) && is_strand_ok)
			{	
				tuple2vector<BED_TUPLE_TYPE, std::vector<std::string>, 4, std::tuple_size< BED_TUPLE_TYPE >::value> (  (db_iter->first).data , read.annotation_info_[db_idx] );
				++annotation_number;
			}
			if( db_iter == gdatabase.begin() )
					break ;
			--db_iter;
		}
		///如果 annotation_number = 0，記錄之 
		return annotation_number;
	}
	
};


template < class PARSER_TYPE, class ANNOTRAIT, int IGNORE_STRAND >
std::map < typename PARSER_TYPE::format_type, uint32_t> 
Annotation < PARSER_TYPE, ANNOTRAIT, IGNORE_STRAND, AnnoType::INTERSET> :: gdatabase;

template < class PARSER_TYPE, class ANNOTRAIT, int IGNORE_STRAND >
std::mutex 
Annotation < PARSER_TYPE, ANNOTRAIT, IGNORE_STRAND, AnnoType::INTERSET> :: gAnnotation_db_mutex;

template < class PARSER_TYPE, class ANNOTRAIT, int IGNORE_STRAND >
bool 
Annotation < PARSER_TYPE, ANNOTRAIT, IGNORE_STRAND, AnnoType::INTERSET> :: gdb_established = false;


//bool operator<(const Bed < std::tuple<std::string, uint32_t, uint32_t, std::string, std::string, std::string>> &lhs
//				,const Bed < std::tuple<std::string, uint32_t, uint32_t, std::string, std::string, std::string>> &rhs);
//{
//
//	return true;
//}



#endif
