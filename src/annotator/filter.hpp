/**
 *  @file filter.hpp
 *  @brief 過濾 annotation 後的結果
 *  @author C-Salt Corp.
 */
#ifndef FILTER_HPP_
#define FILTER_HPP_
#include <boost/regex.hpp>
#include <boost/mpl/size.hpp>

/**
 * @struct FilterMatchTrait
 * @brief used to define the filter-rule policy that is going to applied on the vector < AnnotationRawBed<> >.  Currently, six different filter rules have been provided
 * @tparam MatchType, a non-type parameter of char, specializaed and used to indicate which filter rules is going to be applied.
 */
template <char MatchType, class str>
struct FilterMatchTrait
{};

/**
 * @brief specialized version for FilterMatchTrait with non-type template parameter MatchType specialized to be '=', indicating an exact string matching scheme is applied for annotation filtering
 */
template <class str>
struct FilterMatchTrait <'=', str>
{
/**
 * @memberof FilterMatchTrait<'='>
 * @brief provide the rule to tell whether anno, an annotation string of a queried AnnotationRawBed<> data, is exactly the same as filter_str, a user defined string for annotation string filtering 
 * @param anno string from the member variable annotation_info_ of the queried AnnotationRawBed<>
 * @param filter_str user defined string for annotation string filtering
 */
 	
	inline bool operator() (const std::string& anno, const std::string& filter_str)
	{
		if ( anno == filter_str )
		{
			return true;
		}
		return false;
	}
	static inline bool Match (const std::string& anno)
	{
		if ( anno == boost::mpl::c_str<str>::value )
		{
			return true;
		}
		return false;
	}
};

/**
 * @brief specialized version for FilterMatchTrait with non-type template parameter MatchType specialized to be '^', indicating a prefix string matching scheme is applied for annotation filtering
 */
template <class str>
struct FilterMatchTrait <'^', str>
{
/**
 * @memberof FilterMatchTrait<'^'>
 * @brief provide the rule to tell whether a prefix of anno is exactly the same as filter_str
 * @param anno string from the member variable annotation_info_ of the queried AnnotationRawBed<>
 * @param filter_str user defined string for annotation string filtering
 */
 	
	inline  bool operator() (const std::string& anno, const std::string& filter_str)
	{
		if ( anno.rfind(filter_str, filter_str.length()) != std::string::npos )
		{
			return true;
		}
		return false;
	}
	
	static inline  bool Match (const std::string& anno)
	{
		if ( anno.rfind(boost::mpl::c_str<str>::value, boost::mpl::size<str>::value) != std::string::npos )
		{
			return true;
		}
		return false;
	}
};

/**
 * @brief specialized version for FilterMatchTrait with non-type template parameter MatchType specialized to be '$', indicating a prefix string matching scheme is applied for annotation filtering
 */
template <class str>
struct FilterMatchTrait <'$', str>
{
/**
 * @memberof FilterMatchTrait<'$'>
 * @brief provide the rule to tell whether a suffix of anno is exactly the same as filter_str
 * @param anno string from the member variable annotation_info_ of the queried AnnotationRawBed<>
 * @param filter_str user defined string for annotation string filtering
 */
 	
	inline bool operator() (const std::string& anno, const std::string& filter_str)
	{
		if ( anno.find(filter_str, anno.length() - filter_str.length()) != std::string::npos )
		{
			return true;
		}
		return false;
	}
	
	static inline bool Match (const std::string& anno)
	{
		if ( anno.find(boost::mpl::c_str<str>::value, anno.length() - boost::mpl::size<str>::value) != std::string::npos )
		{
			return true;
		}
		return false;
	}
};

/**
 * @brief specialized version for FilterMatchTrait with non-type template parameter MatchType specialized to be '%', indicating a substr matching scheme is applied for annotation filtering
 */
template <class str>
struct FilterMatchTrait <'%', str>
{
/**
 * @memberof FilterMatchTrait<'%'>
 * @brief provide the rule to tell whether filter_str can be found in anno
 * @param anno string from the member variable annotation_info_ of the queried AnnotationRawBed<>
 * @param filter_str user defined string for annotation string filtering
 */
 	
	inline bool operator() (const std::string& anno, const std::string& filter_str)
	{
		if ( anno.find(filter_str) != std::string::npos )
		{
			return true;
		}
		return false;
	}
	
	static inline bool Match (const std::string& anno)
	{
		if ( anno.find(boost::mpl::c_str<str>::value) != std::string::npos )
		{
			return true;
		}
		return false;
	}
};

/**
 * @brief specialized version for FilterMatchTrait with non-type template parameter MatchType specialized to be 'R', indicating a regular expression matching scheme is applied for annotation filtering
 */
template <class str>
struct FilterMatchTrait <'R', str>
{
/**
 * @memberof FilterMatchTrait<'R'>
 * @brief provide the rule to tell whether a regular expressed filter_str filtering criterion is met in anno
 * @param anno string from the member variable annotation_info_ of the queried AnnotationRawBed<>
 * @param filter_str user defined string for annotation string filtering
 */
 	
	inline bool operator() (const std::string& anno, const std::string& filter_str)
	{
		boost::regex reg_pattern(filter_str);
		if ( boost::regex_search(anno, reg_pattern) )
		{
			return true;
		}
		return false;
	}
	
	static inline bool Match (const std::string& anno)
	{
		boost::regex reg_pattern(boost::mpl::c_str<str>::value);
		if ( boost::regex_search(anno, reg_pattern) )
		{
			return true;
		}
		return false;
	}
};

/**
 * @struct FilterImpl
 * @brief used to define the filter-rule policy that is going to applied on the vector < AnnotationRawBed<> >.  Currently, six different filter rules have been provided
 * @tparam MatchType, a non-type parameter of char, specializaed and used to indicate which filter rules is going to be applied.
 */
template<class INPUT_TYPE>
struct FilterImpl
{
/**
 * @memberof FilterImpl <INPUT_TYPE>
 * @brief keep track of the std::vector< AnnotationRawBed<> >* data structure, which is going to be filtered in the following operator() overloading function
 */
	INPUT_TYPE in;

/**
 * @memberof FilterImpl <INPUT_TYPE>
 * @brief constructor with an INPUT_TYPE structure
 */
	FilterImpl(INPUT_TYPE i)
		: in (i)
	{}

/**
 * @memberof FilterImpl <INPUT_TYPE>
 * @brief default constructor for testing
 */
	FilterImpl()
	{}

/**
 * @memberof FilterImpl <INPUT_TYPE>
 * @brief operator() overloading to set the is_filtered_ flag for each piece of the AnnotationRawBed<> data  
 * @tparam FILTER_TYPELIST provide a FILTER_TYPELIST, provide a series of boost::mpl::vector typelists, each of which provides a type list set including a mpl::string, a mpl::int, and a mpl::char for filtering operation.
 */
	template<class FILTER_TYPELIST>//T>
	void operator()(FILTER_TYPELIST t)
	{
		typedef typename boost::mpl::at<FILTER_TYPELIST, boost::mpl::int_<0> >::type FilterQueryString;
		typedef typename boost::mpl::at<FILTER_TYPELIST, boost::mpl::int_<1> >::type FilterValue;
		typedef typename boost::mpl::at<FILTER_TYPELIST, boost::mpl::int_<2> >::type FilterMatchScheme;
		typedef typename boost::mpl::at<FILTER_TYPELIST, boost::mpl::int_<3> >::type DBIdxType;
		typedef typename boost::mpl::at<FILTER_TYPELIST, boost::mpl::int_<4> >::type DBDepthType;
		typedef typename boost::mpl::if_ < boost::is_same<DBIdxType, boost::mpl::void_>, boost::false_type, DBIdxType >::type DBIdxType2;
		typedef typename boost::mpl::if_ < boost::is_same<DBDepthType, boost::mpl::void_>, boost::false_type, DBDepthType >::type DBDepthType2;
		
		const int value_for_is_filter_flag = FilterValue ();
		const char MatchType = FilterMatchScheme::value;
		
		Filter < FilterMatchTrait< MatchType, FilterQueryString> > (in, value_for_is_filter_flag, DBIdxType2::value, DBDepthType2::value);
	}

	// 跑特定db特定depth
	template<class MATCHTYPE>
	inline void Filter (INPUT_TYPE in, const int value_for_is_filter_flag, const int db_idx, const int db_depth)
	{
		for(auto &anno_rawbed : *in)
		{
			// 如果db size為0 ，就filter掉
			if(anno_rawbed.annotation_info_.size() == 0)
			{
				anno_rawbed.is_filtered_ = value_for_is_filter_flag;
				continue;
			}
			
			// 如果 annotation size =0，就filter掉
			if(anno_rawbed.annotation_info_[db_idx].size() == 0)
			{
				anno_rawbed.is_filtered_ = value_for_is_filter_flag;
				continue;
			}
				
			auto &anno = anno_rawbed.annotation_info_[db_idx][db_depth];
			if( MATCHTYPE::Match (anno) )
			{
				anno_rawbed.is_filtered_ = value_for_is_filter_flag;
			}
		}
	}

	// 跑特定db
	template<class MATCHTYPE>
	inline void Filter (INPUT_TYPE in, const int value_for_is_filter_flag, const int db_idx, const bool dummy_bool)
	{
		for(auto &anno_rawbed : *in)
		{
			// 如果db size為0 ，就filter掉
			if(anno_rawbed.annotation_info_.size() == 0)
			{
				anno_rawbed.is_filtered_ = value_for_is_filter_flag;
				continue;
			}
			auto &annos = anno_rawbed.annotation_info_[db_idx];
			
			// 如果 annotation size =0，就filter掉
			if(annos.size() == 0)
				anno_rawbed.is_filtered_ = value_for_is_filter_flag;
			
			for (auto &anno : annos) //annos => vector<string>
			{
				if( MATCHTYPE::Match (anno) )
				{
					anno_rawbed.is_filtered_ = value_for_is_filter_flag;
				}
			}
		}
	}

	// 全跑
	template<class MATCHTYPE>
	inline void Filter (INPUT_TYPE in, const int value_for_is_filter_flag, const bool dummy_bool_1, const bool dummy_bool_2)
	{
		//std::cerr << "in size " << in->size() << std::endl;
		for(auto &anno_rawbed : *in)
		{
			// 如果db size為0 ，就filter掉
			if(anno_rawbed.annotation_info_.size() == 0)
				anno_rawbed.is_filtered_ = value_for_is_filter_flag;
			
			for (auto &annos : anno_rawbed.annotation_info_)
			{
				// 如果 annotation size =0，就filter掉
				if(annos.size() == 0)
				{
					anno_rawbed.is_filtered_ = value_for_is_filter_flag;
					//std::cerr << anno_rawbed << std::endl;					
				}
					
				
				for (auto &anno : annos) //annos => vector<string>
				{		
					if( MATCHTYPE::Match (anno) )
					{
						anno_rawbed.is_filtered_ = value_for_is_filter_flag;
					}
				}
			}
		}
	}
};

/**
 * @struct FilterWorker
 * @brief carry out filter operation with mpl::typelist implementation
 * @tparam INPUT_TYPE represent the input type, designed to be std::vector< AnnotationRawBed<> >*
 * @tparam FILTER_TYPELIST define a boost::mpl::vector typelist packing a series of boost::mpl::vector typelists, each of which provides a type list set including a mpl::string, a mpl::int, and a mpl::char for filtering operation
 */
template<class INPUT_TYPE, class FILTER_TYPELIST>
class FilterWorker
{
public:
/**
 * @memberof FilterWorker <INPUT_TYPE, FILTER_TYPELIST>
 * @brief constructor
 */
	FilterWorker()
	{}
	
/**
 * @memberof FilterWorker <INPUT_TYPE, FILTER_TYPELIST>
 * @brief povide the interface function to pass in the FILTER_TYPELIST typelist and run the FilterImpl<INPUT_TYPE> functor for each set of boost::mpl::vector packed sub-typelist carried in the FILTER_TYPELIST typelist
 * @param in has the type of std::vector< AnnotationRawBed<> >* to carry the to be filtered AnnotationRawBed<> data
 */
	void run (INPUT_TYPE in) 
	{
		boost::mpl::for_each<FILTER_TYPELIST> ( FilterImpl<INPUT_TYPE>( in ) );
	}

	INPUT_TYPE Filter (INPUT_TYPE in) 
	{
		boost::mpl::for_each<FILTER_TYPELIST> ( FilterImpl<INPUT_TYPE>( in ) );
		return in;
	}
};

#endif
