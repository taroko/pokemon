#include "../iohandler/iohandler.hpp"
#include "../iohandler/ihandler/ihandler.hpp"

/*
enum FileSource
{
	Local,
	URL
};

template <int FileSource>
struct FileReaderWrapper
{};
template <>
struct FileReaderWrapper <FileSource::URL>
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE; 
	typedef IHandler <ParallelTypes::NORMAL, Fastq, TUPLETYPE, IoHandlerBaseSpaceDownload> Reader;

	std::vector< std::vector<std::string> > url_vec_;
	std::vector< std::vector<uint64_t> > size_vec_;
	Reader* reader_ptr_;
	std::map<int, std::vector< Fastq<TUPLETYPE> > >* dummy_ptr_;
	int current_source_;

	FileReaderWrapper (std::vector< std::vector <std::string> >& url_vec, const std::vector < std::vector <uint64_t> >& size_vec = {} )
		: url_vec_ (url_vec)
		, size_vec_ (size_vec)
		, dummy_ptr_ (new  std::map <int, std::vector < Fastq<TUPLETYPE> > >)
		, current_source_ (0)
	{
		
		if (url_vec_.size()>0)
		{
			if (url_vec_.size()==size_vec_.size())
				reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_, size_vec_[current_source_]);
			else
				reader_ptr_ = new Reader (url_vec_.front(), dummy_ptr_);
			++current_source_;
		}
		else
		{
			std::cerr<<"url_vec wrong size "<<'\n';
			exit (1);
		}
	}	
	
	~FileReaderWrapper ()
	{
		delete reader_ptr_;
		delete dummy_ptr_;
	}

	bool run (std::map<int, std::vector< Fastq<TUPLETYPE> > >* ptr, int get_count=1)
	{
		bool eof_info = reader_ptr_->Read_NSkip (ptr, get_count);
//std::cerr<<"internal eof && current size_: vec.size "<<eof_info<<'\t'<<current_source_<<'\t'<<url_vec_.size()<<'\n';
		if (eof_info)
		{
			if (current_source_ < url_vec_.size())
			{
				delete reader_ptr_;
				if (url_vec_.size()==size_vec_.size())
					reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_, size_vec_[current_source_]);
				else
					reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_);
				++current_source_;
				return false;
			}
			else
				return true;
		}
		else
			return false;
	}
};

//struct FileReaderWrapperLocal
*/
template <typename INPUTHANDLE>
struct FileReaderWrapper //<FileSource::Local>
{
	typedef std::tuple <std::string, std::string, std::string, std::string> TUPLETYPE; 
	typedef IHandler <ParallelTypes::NORMAL, Fastq, TUPLETYPE, INPUTHANDLE> Reader;//IoHandlerIfstream> Reader;

	std::vector< std::vector<std::string> > url_vec_;
	std::vector< std::vector<uint64_t> > size_vec_;
	Reader* reader_ptr_;
	std::map<int, std::vector< Fastq<TUPLETYPE> > >* dummy_ptr_;
	int current_source_;

	FileReaderWrapper (std::vector< std::vector <std::string> >& url_vec, const std::vector < std::vector <uint64_t> >& size_vec = {} )
		: url_vec_ (url_vec)
		, size_vec_ (size_vec)
		, dummy_ptr_ (new  std::map <int, std::vector < Fastq<TUPLETYPE> > >)
		, current_source_ (0)
	{
		
		if (url_vec_.size()>0)
		{
			if (url_vec_.size()==size_vec_.size())
				reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_, size_vec_[current_source_]);
			else
				reader_ptr_ = new Reader (url_vec_.front(), dummy_ptr_);
			++current_source_;
		}
		else
		{
			std::cerr<<"url_vec wrong size "<<'\n';
			exit (1);
		}
	}	
	
	~FileReaderWrapper ()
	{
		delete reader_ptr_;
		delete dummy_ptr_;
	}

	bool run (std::map<int, std::vector< Fastq<TUPLETYPE> > >* ptr, int get_count=1)
	{
		bool eof_info = reader_ptr_->Read_NSkip (ptr, get_count);
//std::cerr<<"internal eof && current size_: vec.size "<<eof_info<<'\t'<<current_source_<<'\t'<<url_vec_.size()<<'\n';
		if (eof_info)
		{
			if (current_source_ < url_vec_.size())
			{
				delete reader_ptr_;
				if (url_vec_.size()==size_vec_.size())
					reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_, size_vec_[current_source_]);
				else
					reader_ptr_ = new Reader (url_vec_[current_source_], dummy_ptr_);
				++current_source_;
				return false;
			}
			else
				return true;
		}
		else
			return false;
	}
};
