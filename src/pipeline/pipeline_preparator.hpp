#ifndef GENOME_PREPARATOR_HPP_
#define GENOME_PREPARATOR_HPP_

#include <fstream>
#include <map>
#include <string>
#include "../file_reader.hpp"
#include "../aligner/aligner.hpp"
#include "../iohandler/iohandler.hpp"
//#include "../file_writer/file_writter.hpp"
#include "../format/raw_bed.hpp"

template < typename ALIGNER_TYPE = Aligner< Aligner_trait<> > >
class PipelinePreparator
{
public:
	typedef std::tuple <std::string, std::string> TUPLETYPE;
	typedef FileReader_impl < Fasta, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE > ReaderType;

	std::vector<std::string> genome_file_path_;
	std::string genome_repository_path_;
	static std::string gChromosome_length_path_;

	ReaderType Reader_;
	static std::map <std::string, std::string> gGenometable_;
	static std::string gBam_Header_;
	static std::vector<std::string > gChromosome_vector_;
	static DeviceParameter gDeviceParameter_;
	static std::vector <std::string> gBarcode_vector_;
	static std::map <std::string, uint8_t> gChromosome_map_;
	static std::vector<uint32_t> gReadCountPerChr_;

	PipelinePreparator (void)//std::string& genome_file_path, std::string& index_path)
//		: genome_file_path_ ({genome_file_path})
//		, genome_repository_path_ (index_path)//( genome_file_path.substr (0, genome_file_path.rfind('/') ) + '/' )
//		, Reader_ (genome_file_path_)
	{
//		gChromosome_length_path_ = (genome_repository_path_ + ".chrLen");
		//std::cerr<<"length path "<<gChromosome_length_path_<<'\n';
//		ALIGNER_TYPE aligner;
//		aligner.load_table (index_path);
//		gBam_Header_ = aligner.get_sam_header_string();
//std::cerr<<"PP:: gBam_Header "<<gBam_Header_<<'\n';
//		gChromosome_vector_ = aligner.get_chromosome_string();
//		SetChrmap ();
 //		RawBed <>::gChrRefMap_ = gChromosome_map_ ;
		//std::cerr<<"print gChrRefMap_ "<<'\n';
		//for (auto& q : RawBed <>::gChrRefMap_)
		//	std::cerr<<"strand "<<q.first<<'\t'<<q.second<<'\n';
	}



	PipelinePreparator (std::string& genome_file_path, std::string& index_path)
		: genome_file_path_ ({genome_file_path})
		, genome_repository_path_ (index_path)//( genome_file_path.substr (0, genome_file_path.rfind('/') ) + '/' )
		, Reader_ (genome_file_path_)
	{
		gChromosome_length_path_ = (genome_repository_path_ + ".chrLen");
		//std::cerr<<"length path "<<gChromosome_length_path_<<'\n';
		ALIGNER_TYPE aligner;
		aligner.load_table (index_path);
		gBam_Header_ = aligner.get_sam_header_string();
std::cerr<<"PP:: gBam_Header "<<gBam_Header_<<'\n';
		gChromosome_vector_ = aligner.get_chromosome_string();
		SetChrmap ();
 		RawBed <>::gChrRefMap_ = gChromosome_map_ ;
		//std::cerr<<"print gChrRefMap_ "<<'\n';
		//for (auto& q : RawBed <>::gChrRefMap_)
		//	std::cerr<<"strand "<<q.first<<'\t'<<q.second<<'\n';
	}

	void SetChrmap (void)
	{
//		std::vector <int> strand_flag ({0, 16});
//		for (auto& flag : strand_flag)
		{
			int idx=0;
			for (auto& chr : gChromosome_vector_)
			{
				gChromosome_map_[chr]=(uint8_t) idx;
				++idx;
			}
		}
	}


	PipelinePreparator (std::string& genome_file_path, std::string& index_path, int a)	//for pipeline_2 test
		: genome_file_path_ ({genome_file_path})
		, genome_repository_path_ ( genome_file_path.substr (0, genome_file_path.rfind('/') ) + '/' )
		, Reader_ (genome_file_path_)
	{
		gChromosome_length_path_ = (genome_repository_path_ + "chrLen");
		gBam_Header_ = ("@HD\tVN:1.0\tSO:unsorted\n@SQ\tSN:chr1\tLN:225280621\n@SQ\tSN:chr10\tLN:131314738\n@SQ\tSN:chr11\tLN:131129516\n@SQ\tSN:chr12\tLN:130481393\n@SQ\tSN:chr13\tLN:95589878\n@SQ\tSN:chr14\tLN:88289540\n@SQ\tSN:chr15\tLN:81694766\n@SQ\tSN:chr16\tLN:78884753\n@SQ\tSN:chr17\tLN:77795210\n@SQ\tSN:chr18\tLN:74657229\n@SQ\tSN:chr19\tLN:55808983\n@SQ\tSN:chr2\tLN:238204518\n@SQ\tSN:chr20\tLN:59505520\n@SQ\tSN:chr21\tLN:35106642\n@SQ\tSN:chr22\tLN:34894545\n@SQ\tSN:chr3\tLN:194797135\n@SQ\tSN:chr4\tLN:187661676\n@SQ\tSN:chr5\tLN:177695260\n@SQ\tSN:chr6\tLN:167395066\n@SQ\tSN:chr7\tLN:155353663\n@SQ\tSN:chr8\tLN:142888922\n@SQ\tSN:chr9\tLN:120143431\n@SQ\tSN:chrX\tLN:151100560\n@SQ\tSN:chrY\tLN:25653566\n");
		SetChrmap ();
 		RawBed <>::gChrRefMap_ = gChromosome_map_ ;
	}

	void ReleaseIndex(void)
	{
		typename Aligner_trait<>::AlignerTableType kk;
		std::swap(Aligner< Aligner_trait<> >::aligner_table_, kk);
	}

	void Load (void)
	{
		std::ofstream outfile (gChromosome_length_path_);
		while (true)
		{
			auto chr_content = Reader_.get_next_entry (0);
			if (chr_content.eof_flag)
				break;
			gGenometable_.insert ( std::make_pair (chr_content.getName(), chr_content.getSeq()) );
//			gGenometable_.emplace ( std::make_pair (chr_content.getName(), chr_content.getSeq()) );
			outfile << chr_content.getName()<<'\t'<<chr_content.getSeq().size()<<'\n';
//std::cerr<<"name : length "<<chr_content.getName()<<":"<<chr_content.getSeq().size()<<std::endl;
			//gBam_Header_+=("@SQ\tSN:"+chr_content.getName()+"\tLN:"+std::to_string(chr_content.getSeq().size())+'\n');
		}
		outfile.close();
//std::cerr<<"genome_file_path_ : gChromosome_length_path_ "<< genome_file_path_.front() << " : " <<gChromosome_length_path_<<'\n';
//std::cerr<<"bam_header : \n"<< gBam_Header_ <<'\n';
	}	

	void ReleaseChr (void)
	{
		gGenometable_.clear();
	}

	void GetBarcodeVec (std::vector<std::string>& vecin)
	{
		gBarcode_vector_ = vecin;
	}

	template <typename INPUT_TYPE>
	void GetReadCountPerChr (INPUT_TYPE& rawbed_map)
	{
		gReadCountPerChr_.clear();

		std::string current_chr = rawbed_map.begin()->first.GetChr (rawbed_map.begin()->first.chr_idx_);
		uint32_t count = 0;
		for (auto& item: rawbed_map)
		{
			if (item.first.GetChr (item.first.chr_idx_) == current_chr)
				count+=item.first.reads_count_;
			else
			{
				gReadCountPerChr_.push_back (count);
				count = item.first.reads_count_;
				current_chr = item.first.GetChr (item.first.chr_idx_);
			}
		}
		gReadCountPerChr_.push_back (count);
	}
};

template <typename ALIGNER_TYPE>
std::map <std::string, std::string> 
PipelinePreparator<ALIGNER_TYPE> ::gGenometable_;

template <typename ALIGNER_TYPE>
std::string 
PipelinePreparator<ALIGNER_TYPE>::gBam_Header_;

template <typename ALIGNER_TYPE>
std::string 
PipelinePreparator<ALIGNER_TYPE>::gChromosome_length_path_;

template <typename ALIGNER_TYPE>
std::vector<std::string >
PipelinePreparator<ALIGNER_TYPE>::gChromosome_vector_ ({
				"chr1",
				"chr10",
				"chr11",
				"chr12",
				"chr13",
				"chr14",
				"chr15",
				"chr16",
				"chr17",
				"chr18",
				"chr19",
				"chr2",
				"chr20",
				"chr21",
				"chr22",
				"chr3",
				"chr4",
				"chr5",
				"chr6",
				"chr7",
				"chr8",
				"chr9",
				"chrX",
				"chrY"});

template <typename ALIGNER_TYPE>
DeviceParameter
PipelinePreparator<ALIGNER_TYPE>::gDeviceParameter_;

template <typename ALIGNER_TYPE>
std::vector<std::string>
PipelinePreparator<ALIGNER_TYPE>::gBarcode_vector_;

template <typename ALIGNER_TYPE>
std::map <std::string, uint8_t>
PipelinePreparator<ALIGNER_TYPE>::gChromosome_map_;

template <typename ALIGNER_TYPE>
std::vector <uint32_t>
PipelinePreparator<ALIGNER_TYPE>::gReadCountPerChr_;


#endif
