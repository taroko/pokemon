#ifndef ANALYZER_SEQUENCE_LOGO_HPP_
#define ANALYZER_SEQUENCE_LOGO_HPP_

#include <cassert>
#include <functional>

#include "../constant_def.hpp"
#include "../tuple_utility.hpp"

#include "analyzer_utility.hpp"
#include "analyzer_policy.hpp"
//#include "analyzer_printer_old.hpp"

/**
 * @brief Analyzer 此class為提供計算Langth distribution的Analyzer參數
 * @tparam ANALYZER_TYPE 特化為 LengthDistribution
 */
template<>
class AnalyzerParameter<AnalyzerTypes::SequenceLogo>
{
public:
	/** 
	 * @brief 定義 length distribution參數有哪些
	 */
	
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<1> FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定db。
	typedef boost::mpl::int_<2> DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef boost::mpl::int_<3> DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef boost::mpl::int_<4> DbDepthNameType;
	
//	/// @brief GetReadLengthClass 決定取得 anno raw bed length 的方法，也就是length distribution 的 length，預設回傳 read length，也可以自定回傳其他數值
//	typedef boost::mpl::int_<5> GetReadLengthClass;
//	
//	/// @brief CalReadCountClass 決定取得 anno raw bed count 的方法，也就是length distribution 的 value，預設回傳 read count，也可以自定回傳其他數值
//	typedef boost::mpl::int_<6> CalReadCountClass;
	
	/// @brief GetReadSeqClass 決定取得 anno raw bed sequence 的方法，也就是read sequence 的 content，預設回傳 空字串，也可以自定回傳其他數值
	typedef boost::mpl::int_<5> GetReadSeqClass;
//	typedef boost::mpl::int_<8> Printer;
//	typedef boost::mpl::int_<-1> ReturnType;
	typedef boost::mpl::int_<6> DbDepth2NameType;
};


class ANALYZER_SEQUENCE_LOGO_GLOBAL {};


template<class INPUT_TYPE>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>
{
public:
	//typedef std::map<int, std::map<std::tuple<int,int,int,int,std::string,std::string>, std::tuple<double, double, double> > > OutPutType ;
	typedef std::map<std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > >//std::tuple<double, double, double> > > 
	OutPutType;	
	/// @brief 因為每個run為mt，所以最後要整合進一個 global 的資料結構
	static std::vector<OutPutType> gOutSet_;
	
	static std::vector< std::vector<OutPutType> > gTmpOutSet_;
	
	static std::vector<std::string> gOutNamePrefix_;
	
	static std::vector<std::mutex> gOutMutex_;

	template <typename T>
	static void free (T& aa)
	{
		T bb;
		bb.swap (aa);
	}
	
	static void ClearContent (void)
	{
		gOutSet_.clear();
		free(gOutSet_);
		gOutNamePrefix_.clear();
		free(gOutNamePrefix_);
		gTmpOutSet_.clear();
		free(gTmpOutSet_);
		gOutSet_.resize(200);
		gOutNamePrefix_.resize(200);
		gTmpOutSet_.resize(200);
	}
};



/**
 * @brief Analyzer 的實作，此為計算 Length distribation。幾乎範型的可以計算所有 length distribution，可以用參數設定要的長度分布
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::map<boost::mpl::pair<KEY, VALUE> >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，此為 Length distribation 特化
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>
	:public AnalysisLenDistImpl<INPUT_TYPE>
{
public:
	typedef typename std::iterator_traits<INPUT_TYPE>::value_type::value_type READ_TYPE;
	
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::SequenceLogo> AnaPara;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉"filter tag"=1，0=>去掉 "filter tag"=0。每一個AnalyzerParameter都應該要有此參數
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::FilterType, boost::mpl::int_<1> >::type FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定db。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbIndexType, boost::mpl::int_<-2> >::type DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthType, boost::mpl::int_<-2> >::type DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，此轉為 "-1"，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthNameType, boost::mpl::string<'-1'> >::type DbDepthNameType;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepth2NameType, boost::mpl::string<'-3'> >::type DbDepth2NameType;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::GetReadSeqClass, GetReadFirstNLastComposition<0, 0, 15> >::type GetReadSeqClass;
	//typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Printer, LenDistSeqPrinter >::type PrinterClass;
//	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::ReturnType >::type ReturnType;
	
	/// @brief 記錄此run為第N個使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	int this_analyzer_count_;

	const int length_ = GetReadSeqClass::LENGTH_TYPE::value;
	
	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	/** 
	 * @brief 輸出的資料型別
	 * \n output vector {read_length, tuple{sys, filter, db_idx, db_depth, db_depth_name}, std::array< tuple<double, double, double, double> >} 
	 * \n sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * \n sys 是因為db_index, db_depth "空"沒辦法記錄。ps. 自從空改成 -2後，應該就可以不需要 sys了，但目前還有待修改...
	 */
	typedef std::map<std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > >//std::tuple<double, double, double> > > 
		OutPutType ;
	/// @brief 輸出的資料
	OutPutType out_set_;
	
	/// @brief 因為每個run為mt，所以最後要整合進一個 global 的資料結構
	static std::vector<OutPutType> &gOutSet_;
	static std::vector<std::vector<OutPutType> > &gTmpOutSet_;
	static std::vector<std::string> &gOutNamePrefix_;
	static std::vector<std::mutex> &gOutMutex_;
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)
		, this_analyzer_count_(0)
		, AnalysisLenDistImpl<INPUT_TYPE>(
			std::bind(
				&AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>::CalOutput
				, this
				, std::placeholders::_1
				, std::placeholders::_2
				, std::placeholders::_3
				, std::placeholders::_4
				, std::placeholders::_5
				, std::placeholders::_6
			)
		)
	{}
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl()
	{}


	void OutSetPrinter (OutPutType& ii)
	{
//typedef std::map<std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double> > >//std::tuple<double, double, double> > > 
		for (auto& item: ii)
		{
			std::cerr<<"key tuple\n";
			TupleUtility<std::tuple<int,int,int,int,std::string>, 5>::PrintTuple (std::cerr, item.first);
			for (auto& tuple_item: item.second)
			{
				std::cerr<<"tuple in vec \n";
				TupleUtility<std::tuple<double, double, double, double, double>, 5>::PrintTuple (std::cerr, tuple_item);
			}
		}
	}
	
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief 主要執行Analyzer的對外介面
	 * @param this_analyzer_count 第N個 使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	 * @return void
	 */
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		this_analyzer_count_ = this_analyzer_count;

		this->Analysis (in, FilterType::value, DbIndexType::value, DbDepthType::value, boost::mpl::c_str<DbDepthNameType>::value, boost::mpl::c_str<DbDepth2NameType>::value);
		{
			std::lock_guard<std::mutex> lock(gOutMutex_[this_analyzer_count+1]);
			gTmpOutSet_[this_analyzer_count_].push_back(out_set_);
		}
			
		if (eof_flag)
		{
			int i=0;
			for(auto &tmp_out_set : gTmpOutSet_[this_analyzer_count_])
			{
				MapSum(gOutSet_[this_analyzer_count_], tmp_out_set);
				i++;
			}
			
			std::string db_depth_name_str (boost::mpl::c_str<DbDepthNameType>::value);
			std::string db_depth2_name_str (boost::mpl::c_str<DbDepth2NameType>::value);
			db_depth_name_str = convert_name(db_depth_name_str, 1);
			db_depth2_name_str = convert_name(db_depth2_name_str, 2);
 
			//SequenceLogo.Normal.full.GMPM
			
			std::string filename_prefix("SequenceLogo.");
			filename_prefix += boost::mpl::c_str< typename GetReadSeqClass::NAME_TYPE >::value;
//			filename_prefix += ".";
//			filename_prefix += boost::mpl::c_str< typename GetReadLengthClass::NAME_TYPE >::value;
//			filename_prefix += ".";
//			filename_prefix += boost::mpl::c_str< typename CalReadCountClass::NAME_TYPE >::value;
			
			filename_prefix += ".";
			filename_prefix += db_depth_name_str;
			filename_prefix += ".";
			filename_prefix += db_depth2_name_str;
			gOutNamePrefix_[this_analyzer_count] = filename_prefix;
std::cerr<<"name "<<filename_prefix<<'\n';
			for (auto& target_pair: gOutSet_[this_analyzer_count_])	// target_pair = miRNA/biotype name : vector < tuple <doublex4> >
				for (auto& chr_count: target_pair.second)	// chr_count = tuple <doublex4>
				{
					std::vector< double > count_vec ({std::get<0>(chr_count), std::get<1>(chr_count), std::get<2>(chr_count), std::get<3>(chr_count)});
					double total_count =0.0;
					for (auto& count: count_vec)
						total_count += count;

					double entropy = 0.0;
					for (auto& count: count_vec)
						if (count > 0)
							entropy -= count / total_count * log2 (count/total_count);
					double ent = 2.0 - entropy;
//					double maxheight = 2.0;
					std::get<0>(chr_count) = count_vec[0] / total_count;// * ent;//* maxheight / 2 ;// + 0.5;
					std::get<1>(chr_count) = count_vec[1] / total_count;// * ent;//* maxheight / 2 ;// + 0.5;
					std::get<2>(chr_count) = count_vec[2] / total_count;// * ent;//* maxheight / 2 ;// + 0.5;
					std::get<3>(chr_count) = count_vec[3] / total_count;// * ent;//* maxheight / 2 ;// + 0.5;
					std::get<4>(chr_count) = ent;
					//ent = 2.0 - entropy(&pwm[i*4], 4)/log(2);
					//for(ii=0;ii<4;ii++)
					//	answer[i*4+ii] = (int) (pwm[i*4+ii] * maxheight/2 * ent + 0.5);
				}
		}
		return (void*) &in;//NULL;
	}

	std::string convert_name(std::string str, int type)
	{
		if(type == 1)
		{
			if(str == "-1")
				return "biotype";
			else if(str == "-2")
				return "merged";
			else if(str == "-3")
				return "no_biotype";
			else
			{
				std::replace (str.begin(), str.end(), '.', '_');
				return str;
			}
		}
		else
		{
			if(str == "-1")
				return "detail";
			else if(str == "-2")
				return "merged_detail";
			else if(str == "-3")
				return "no_detail";
			else
			{
				std::replace (str.begin(), str.end(), '.', '_');
				return str;
			}
		}
	}
		
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是簡化重複程式碼，此部份主要負責 insert into map of output
	 * @tparam READ_TYPE 自動決定 anno rawbed data type
	 * @param anno_rawbed read 的資料結構，詳情請看 annotation_raw_bed.hpp
	 * @param sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void CalOutput (READ_TYPE &anno_rawbed ,const int sys, const int filter, const int db_index, const int db_depth, const char* db_depth_name)
	{
		//std::cerr << "CalOutput: " << filter << "\t" << db_index << "\t" << db_depth << "\t" << db_depth_name << std::endl;
		//std::cerr << "----------------------------\nAnnoRawBed: " << anno_rawbed << "\n----------------------------\n";
		
//		int read_length = GetReadLengthClass::GetReadLength(anno_rawbed);
//		double count = CalReadCountClass::CalReadCount(anno_rawbed);
		std::string read_seq = GetReadSeqClass::GetReadSeq(anno_rawbed);

		if (read_seq=="")
			return;

		std::string db_depth_name_1 (db_depth_name);//, strlen (db_depth_name));
		std::replace (db_depth_name_1.begin(), db_depth_name_1.end(), '.', '_');

		out_set_[std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1)].resize (length_);//sequence_length_);
		int pos_index=0;
		for (auto chr: read_seq)
		{
			switch (chr)
			{  
				case 'A':
					++std::get<0> (out_set_[std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1)][pos_index]);break;
				case 'C':
					++std::get<1> (out_set_[std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1)][pos_index]);break;
				case 'G':
					++std::get<2> (out_set_[std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1)][pos_index]);break;
				case 'T':
					++std::get<3> (out_set_[std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1)][pos_index]);break;
			}
			++pos_index;
		}
	}
	
	void MapSum(OutPutType &map_sum, OutPutType &map_item)
	{
		map_sum += map_item;
	}
};


template<class INPUT_TYPE>
std::vector< std::map< std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > > >//std::tuple<double, double, double> > > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutSet_(200);

template<class INPUT_TYPE>
std::vector< std::vector< std::map< std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > >  > >//std::tuple<double, double, double> > > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gTmpOutSet_(200);


template<class INPUT_TYPE>
std::vector< std::string > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutNamePrefix_(200);

template<class INPUT_TYPE>
std::vector<std::mutex>
AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutMutex_(200);


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector< std::map< std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > > >&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>::gOutSet_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector < std::vector< std::map< std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > > > >&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>::gTmpOutSet_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gTmpOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector< std::string > &
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>::gOutNamePrefix_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutNamePrefix_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector<std::mutex> &
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>::gOutMutex_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutMutex_;

#endif
