#ifndef ANALYZER_PRINTER_HETEROGENEITY_HPP_
#define ANALYZER_PRINTER_HETEROGENEITY_HPP_

#include <fstream>
#include <array>
#include <boost/filesystem.hpp>
#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"
#include "boost/algorithm/string/split.hpp"

template<>
class AnalyzerParameter<AnalyzerTypes::HeterogeneityPrinter>
{
public:
	/** 
	 * @brief 定義 length distribution參數有哪些
	 */
	
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	typedef boost::mpl::int_<1> AnnoIdx;
	typedef boost::mpl::int_<2> IoHandlerType;
	typedef boost::mpl::int_<3> XSort; 
};


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::HeterogeneityPrinter>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::HeterogeneityPrinter> AnaPara;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::AnnoIdx, boost::mpl::int_<0> >::type AnnoIdx;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType; 
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::XSort, boost::mpl::bool_<true> >::type XSort; 

	std::string output_path_;
	/// @brief 輸入的資料
	INPUT_TYPE &in;

    typedef std::map < std::tuple<int, int, std::string>, std::vector<double> > OutPutType;
	std::vector <OutPutType>& OutSet_;
	//map[file_name, map[miRNA_name, vector<double>]]
	std::map <std::string, std::map < std::string, std::vector<double> > > result_;
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)
		, output_path_("")
        , OutSet_(AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_HETEROGENEITY, AnalyzerTypes::Heterogeneity>::gresult_)
	{}
	
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		if(eof_flag)
		{
			output_path_ = std::string("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+ PipelinePreparator<>::gBarcode_vector_[barcode_index] + std::string("/heterogeneity/"));
			mkdir();
			auto content = OutSet_[AnnoIdx::value];
			getresult (content);

			for (auto& content: result_)	//pair < file_name, map <miRNA, std::vector<double> > >
			{
				std::string filename = content.first;
				DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
				file_writter_parameter.set_filename(filename , output_path_);
				std::shared_ptr <IoHandlerType> out_ (std::make_shared <IoHandlerType> (file_writter_parameter));

				std::cerr << output_path_ + filename << std::endl;

				*(out_) << "====================" << std::endl;
				*(out_) << filename << std::endl;
				*(out_) << "--------------------" << std::endl;



				std::vector<std::pair <std::string, double> > all_x_title;

				for (auto &miRNA_pair : content.second)		//content.second => map <miRNA, std::vector<double> >
				{   
					double hetero_sum = 0.0;
					for (auto item: miRNA_pair.second)
						hetero_sum += item;
					all_x_title.push_back ({miRNA_pair.first, hetero_sum});
				}

				if (XSort::value==true)
				{   
					std::sort(all_x_title.begin(), all_x_title.end(),
						[](const std::pair<std::string, double> &a, const std::pair<std::string, double> &b)
						{ return (a.second) > (b.second); } );
				}

				*(out_) << "MiRNA Name\t5' head\t5' tail\t3' head\t3' tail\n";
				for (auto& name_content: all_x_title)
					*(out_) << name_content.first << '\t'<< (content.second[name_content.first])[0] <<'\t'
							<< (content.second[name_content.first])[1] << '\t'<<(content.second[name_content.first])[2] << '\t' <<(content.second[name_content.first])[3] << '\n';
				*(out_) << "--------------------" << std::endl;
				out_->close();
			}
		}
		return (void*) &in;
	}

	void mkdir()
	{
		boost::filesystem::path dir_output("output");
		boost::filesystem::create_directory(dir_output);
		
		boost::filesystem::path dir_analyzer(output_path_);
		boost::filesystem::create_directory(dir_analyzer);
	}

	void getresult (OutPutType& output)
	{
		for (auto& tuple_pair: output)
		{
			std::string filename ("Table.heterogeneity.");
			filename += std::to_string(2) + ".";//sys
			filename += std::to_string(std::get<0>(tuple_pair.first)) + ".";//filter
			filename += std::to_string(std::get<1>(tuple_pair.first)) + ".";//db_idx
			filename += std::to_string(0) + ".tsv";//db_depth
			result_[filename][std::get<2>(tuple_pair.first)]=tuple_pair.second;
		}
	}
};
#endif
