#ifndef ANALYZER_PRINTER_MAPPABILITY_HPP_
#define ANALYZER_PRINTER_MAPPABILITY_HPP_

#include <fstream>
#include <array>
#include <boost/filesystem.hpp>
#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"
#include "boost/algorithm/string/split.hpp"
#include "../converter/sam2rawbed.hpp"
template<>
class AnalyzerParameter<AnalyzerTypes::MappabilityPrinter>
{
public:
	/** 
	 * @brief 定義 length distribution參數有哪些
	 */
	
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
/*	
	/// @brief 
	typedef boost::mpl::int_<1> AnnoIdx;
	typedef boost::mpl::int_<2> Xaxis;
	typedef boost::mpl::int_<3> Yaxis;
	typedef boost::mpl::int_<4> Zaxis;
	typedef boost::mpl::int_<5> Xlimit;
	typedef boost::mpl::int_<6> Ylimit;
	typedef boost::mpl::int_<7> Zlimit;
	typedef boost::mpl::int_<8> Len;
	typedef boost::mpl::int_<9> Anno;
	typedef boost::mpl::int_<10> Seq;
	typedef boost::mpl::int_<11> PrefixName;
*/	
	typedef boost::mpl::int_<12> IoHandlerType;
};


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::MappabilityPrinter>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::MappabilityPrinter> AnaPara;
/*
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::AnnoIdx, boost::mpl::int_<0> >::type AnnoIdx;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Xaxis, boost::mpl::pair< AnaPara::Len, boost::mpl::string<'-1'> > >::type Xaxis;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Yaxis, boost::mpl::pair< AnaPara::Anno, boost::mpl::string<'-1'> > >::type Yaxis;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Zaxis, boost::mpl::pair< AnaPara::Seq, boost::mpl::string<'-2'> > >::type Zaxis;	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Xlimit, boost::mpl::int_<0> >::type Xlimit;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Ylimit, boost::mpl::int_<0> >::type Ylimit;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Zlimit, boost::mpl::int_<0> >::type Zlimit;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::PrefixName, boost::mpl::string<'Tab', 'le'> >::type PrefixName;
*/	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType; 

//	typedef std::vector< std::map<int, std::map<std::tuple<int,int,int,int,std::string,std::string>, std::tuple<double, double, double> > > > gOutSetType ;
//	std::tuple <INTTYPE, INTTYPE, INTTYPE, INTTYPE> &gOutSet;	
//	std::vector<std::string> &gOutNamePrefix;
	std::string output_path;
	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)
		, output_path("")
	{}
	
	
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		if(eof_flag)
		{
			output_path = std::string("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+ PipelinePreparator<>::gBarcode_vector_[barcode_index] + std::string("/mappability/"));
			auto gOutSet = Sam2RawBed < std::map <int, std::vector< Sam<> > >* >::reads_mapped_count_[barcode_index];
			//Analysis ();
			mkdir();
			
			std::string filename = ("Table.mappability.tsv");//get_filename(dim_z_first, unit + ".tsv");
			DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
			file_writter_parameter.set_filename( filename , output_path);

			std::shared_ptr <IoHandlerType> out_ (std::make_shared <IoHandlerType> (file_writter_parameter));

			std::cerr << output_path + filename << std::endl;

			*(out_) << "====================" << std::endl;
			*(out_) << filename << std::endl;
			*(out_) << "--------------------" << std::endl;
			*(out_) << "Read count\n"<<"Total\tMapped\tUnique\tMultiple\n";
			*(out_) << std::get<0> (gOutSet)<<'\t'<<std::get<1>(gOutSet)<<'\t'<<std::get<2>(gOutSet)<<'\t'<<std::get<3>(gOutSet)<<'\n';

			*(out_) << "Mappability\n"<<"Overall\tUnique\tMultiple\n";
			out_->precision(3);
			*(out_) << (double) std::get<1>(gOutSet)/std::get<0>(gOutSet)<<'\t'
					<< (double) std::get<2>(gOutSet)/std::get<0>(gOutSet)<<'\t'
					<< (double) std::get<3>(gOutSet)/std::get<0>(gOutSet)<<'\n';
			*(out_) << "--------------------" << std::endl;
			out_->close();
		}
		return (void*) &in;
	}

	void mkdir()
	{
		boost::filesystem::path dir_output("output");
		boost::filesystem::create_directory(dir_output);
		
		boost::filesystem::path dir_analyzer(output_path);
		boost::filesystem::create_directory(dir_analyzer);
	}
};
#endif
