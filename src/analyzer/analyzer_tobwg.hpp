/**
 *  @file analyzer_tobwg.hpp
 *  @brief analyzer - to bigwig conversion
 *  @author C-Salt Corp.
 */
#ifndef ANALYZER_TOBWG_HPP_
#define ANALYZER_TOBWG_HPP_
#include <algorithm>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/string.hpp>
#include <boost/type_traits/add_pointer.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/map.hpp>
#include "../tuple_utility.hpp"
#include "../constant_def.hpp"
#include "../converter/annrawbed2bwg.hpp"
#include "analyzer.hpp"
extern "C"
{
#include "../../pipeline/kent/src/inc/common.h"
#include "../../pipeline/kent/src/inc/obscure.h"
#include "../../pipeline/kent/src/inc/linefile.h"
#include "../../pipeline/kent/src/inc/hash.h"
#include "../../pipeline/kent/src/inc/options.h"
#include "../../pipeline/kent/src/inc/bigWig.h"
#include "../../pipeline/kent/src/inc/bwgInternal.h"
#include "../../pipeline/kent/src/inc/zlibFace.h"
#include "../../pipeline/kent/src/inc/linefile.h"
#include "../../pipeline/kent/src/inc/errabort.h"
#include "../../pipeline/kent/src/inc/sqlNum.h"
#include "../../pipeline/kent/src/inc/sig.h"
#include "../../pipeline/kent/src/inc/bPlusTree.h"
#include "../../pipeline/kent/src/inc/cirTree.h"
#include "../../pipeline/kent/src/inc/bbiFile.h"
}
#define bwgSectionHeaderSize 24 
#include "analyzer_createbwg.hpp"
#include "../iohandler/basespace_def.hpp"

/**
 * @brief Analyzer 此class為提供轉換bwg檔的Analyzer參數
 * @tparam ANALYZER_TYPE 特化為 ToBwg
 */
template<>
class AnalyzerParameter<AnalyzerTypes::ToBwg>
{
public:
	/** 
	 * @brief 定義ToBam參數有哪些
	 */
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數 
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數 
	typedef boost::mpl::int_<1> FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定db。 
	typedef boost::mpl::int_<2> DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定第N個 annotation。 
	typedef boost::mpl::int_<3> DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。 
	typedef boost::mpl::int_<4> DbDepthNameType;

	/// @brief ValueEstimateClass 決定wig value的數值應該如何計算。 
	typedef boost::mpl::int_<5> ValueEstimateClass;

    typedef boost::mpl::int_<6> IoHandlerType;
};

//============Policy for ValueEstimateClass=================
/**
 * @brief estimate coverage and return it as wig's value content
 */
struct ValueEstimateDefault
{
	template <class READ_TYPE> 
	static std::pair<std::string, uint32_t> GetValue (READ_TYPE& in, uint32_t wig_span, std::vector<float>& value)
	{
		value.resize ((size_t) in.end_/wig_span - (size_t) in.start_/wig_span + 1 );
		value.front() = (float) ( wig_span - (in.start_%wig_span) ) / wig_span;
		if (value.size() > 1)
		{
			value.back() = (float) (in.end_%wig_span) / wig_span;
			for (auto itr=value.begin()+1; itr!=value.end()-1; ++itr)
				*itr = 1.0;
			if (value.back()==0)
				value.resize (value.size()-1);
		}
		return { in.chromosome_, ((uint32_t) in.start_ / wig_span) };
	}
};

/**
 * @brief A dummy class for implementation of a specialization form of AnalyzerImpl class.
 */
class ANALYZER_TYPELIST_GLOBAL_TOBWG {};

template <typename INPUT_TYPE, typename MapType, int A>
struct BwgWriterImpl
{
    static void run (int index) {};
};

template <typename INPUT_TYPE, typename MapType>
struct BwgWriterImpl <INPUT_TYPE, MapType, AnalyzerTypes::ToBwg>
{
    static void run (int index) 
    {
        AnalyzerImpl<INPUT_TYPE, MapType, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::WriteBwg (index);
        AnalyzerImpl<INPUT_TYPE, MapType, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::ClearContent();
    };
};

template <typename INPUT_TYPE, typename TYPELIST, int N>
struct BwgWriter
{
    typedef typename boost::mpl::at <TYPELIST, boost::mpl::int_<N> >::type MapType;
    typedef typename boost::mpl::at <MapType, boost::mpl::int_<0> >::type AnalyzerType;
    
    static void run (int index)
    {
        BwgWriter<INPUT_TYPE, TYPELIST, N-1>::run(index);
        BwgWriterImpl <INPUT_TYPE, MapType, AnalyzerType::value>::run(index);
    }
};

template <typename INPUT_TYPE, typename TYPELIST>
struct BwgWriter <INPUT_TYPE, TYPELIST, 0>
{
    typedef typename boost::mpl::at <TYPELIST, boost::mpl::int_<0> >::type MapType;
    typedef typename boost::mpl::at <MapType, boost::mpl::int_<0> >::type AnalyzerType;

    static void run (int index)
    {
        BwgWriterImpl <INPUT_TYPE, MapType, AnalyzerType::value>::run(index);
    }
};

/**
 * @brief A specialized AnalyzerImpl class with template parameter ANALYZER_TYPELIST specialized as ANALYZER_TYPELIST_GLOBAL_TOBWG.  It is used to hold the actual implementation of static objects whose aliases are later used in the instantiations of the class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>.  A static clear function is used to manage the clear operation for all of the static members.
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>
{
public:
	typedef std::map <std::string, std::shared_ptr < std::map < std::string, std::map < uint32_t, float > > > > OutputItemType; 
	typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaPara;
    typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType;

	static std::map< int, OutputItemType > gOutSet_;
	static int gCurrentPipelineIndex_;
	static std::vector <int> flag_;
	static std::map <std::string, std::map < std::string, std::map <uint32_t, float> > > gBuf_;
	static std::map <std::string, std::map < std::string, std::vector < std::vector <bwgVariableStepPacked> > > > gBufkk_;
	static std::map < std::string, bwgSection* > gbwgSection_;

    static void ClearContent (void)
	{
		gOutSet_.clear();
		free(gOutSet_);
		gCurrentPipelineIndex_=0;
		flag_.clear();
		free(flag_);
		gBuf_.clear();
		free(gBuf_);
		gBufkk_.clear();
		free(gBufkk_);
		for (auto& Q : gbwgSection_)
			delete Q.second;
		gbwgSection_.clear();
		free(gbwgSection_);
    }

	template <typename T>
	static void free (T& aa)
	{
		T bb;
		bb.swap (aa);
	}

	static void WriteBwg (int barcode_index)
	{
		CreateBwg<IoHandlerType> QQ (&gbwgSection_);
		QQ.bigWigFileCreate((char*)(PipelinePreparator<>::gChromosome_length_path_.c_str()), barcode_index );
	}
};

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < int, std::map < std::string, std::shared_ptr < std::map < std::string, std::map < uint32_t, float> > > > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gOutSet_;
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
int 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gCurrentPipelineIndex_=0;
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector <int> 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::flag_(0);
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map <std::string, std::map < std::string, std::map <uint32_t, float> > >	
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gBuf_;
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map <std::string, std::map < std::string, std::vector < std::vector <bwgVariableStepPacked> > > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gBufkk_;
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < std::string, bwgSection* > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gbwgSection_;


/**
 * @brief Analyzer 的實作，此為產生bwg檔的特化版本。
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed<>>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::vector<boost::mpl::map<boost::mpl::pair<KEY, VALUE> > >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，此為ToBwg 特化
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>
	:public AnalysisLenDistImpl<INPUT_TYPE>
{
public:
	typedef typename std::iterator_traits<INPUT_TYPE>::value_type::value_type READ_TYPE;

	/// @brief 此為要使用的 analyzer parameter  
	typedef AnalyzerParameter <AnalyzerTypes::ToBwg> AnaPara;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數 
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::FilterType, boost::mpl::int_<1> >::type FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定db。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbIndexType, boost::mpl::int_<-2> >::type DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定第N個 annotation。 
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthType, boost::mpl::int_<-2> >::type DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，此轉為 "-1"，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。 
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthNameType, boost::mpl::string<'-1'> >::type DbDepthNameType;

	/// @brief ValueEstimateDefault 決定wig的value要如何計算 
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::ValueEstimateClass, ValueEstimateDefault >::type ValueEstimateClass;
	
	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	/** 
	 * @brief 輸出的資料型別 \n output map[file_name, AnnRawBed2Bwg<ValueEstimateClass>] 
	 */
	typedef std::map<std::string, std::shared_ptr < AnnRawBed2Bwg<ValueEstimateClass> > > OutPutType;
	
	/// @brief 輸出的資料 
	OutPutType out_set_;
	
	/// @brief a buffer to hold the achieved pipeline result, as a pipeline index to its corresponding map [file_name, shared_ptr of map [chromosome, map[tile, wig_value]]].  The result will be temporary buffered in gOutSetAnnoRawBed2Bwg_ till the results corresponding to all of the precending pipeline indexes have been achieved.  Then the result will be merged into gBuf_ object. 
	typedef std::map <std::string, std::shared_ptr < std::map < std::string, std::map < uint32_t, float > > > > OutputItemType; 

	/// @brief a static buffer to hold the result obtained from each of the posted tobwg pipelines, wherein the pipe_index is used as key 
	static std::map< int, OutputItemType >& gOutSet_;
	
	/// @brief a global mutex object for establishing lock_guard object for merging results coming from each of the posted tobwg pipelines 
	static std::mutex gOutMutex_;

	/// @brief a sequential index, keeping record till which pipe line index that the results have been achieved and been merged into gBuf_ object.  The design is a must be since the merging operation of each tobwg pipelines must be achieved in the order of the pipeline index.  
	static int& gCurrentPipelineIndex_;
	
	/// @brief the object to hold the merged result.  The upload operation or file writing operation will be conducted based on the merged result stored in gBuf_ element, in a type of  map [file_name, map [chromosome, map [tile, wig_value]]]
	static std::map <std::string, std::map < std::string, std::map <uint32_t, float> > >& gBuf_;

	/// @brief a cluster of flags indicating which pipeline has been achieved and bufferred into the static gBuf_ object
	static std::vector <int>& flag_;

    uint32_t wig_step_, wig_span_;
    
    int this_pipe_index;

	AnnRawBed2Bwg<ValueEstimateClass> dummy_device_;

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)//, this_analyzer_count_(0)
		, dummy_device_ (5566, "123")
		, AnalysisLenDistImpl<INPUT_TYPE>(
			std::bind(
				&AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::CalOutput
				, this
				, std::placeholders::_1
				, std::placeholders::_2
				, std::placeholders::_3
				, std::placeholders::_4
				, std::placeholders::_5
				, std::placeholders::_6
			)
		)
	{}

	~AnalyzerImpl(void)
	{}

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>
	/// @brief AnalyzerImpl 建構子
//	AnalyzerImpl()
//	{}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>
	 * @brief main interface function to handle to wig/bwg transform.  
	 * @return void
	 */
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, size_t barcode_index)//, char* chrsize)
	{	
		this_pipe_index = pipe_index;
		if(this_analyzer_count == 0)
		{
			/// @brief input, filter = each, db_idx = -1, db_depth=0, db_depth_value, sys
			this->Analysis (in, -1, -1, 0, "-2", "-3", 0);
			this->Analysis (in, 1, -1, 0, "-2", "-3", 0);
			this->Analysis (in, 0, -1, 0, "-2", "-3", 0);
		}
		this->Analysis (in, FilterType::value, DbIndexType::value, DbDepthType::value, boost::mpl::c_str<DbDepthNameType>::value, "-3"); 
//		if (out_set_.size()==0)
//			return (void*) NULL;
		wig_step_ = dummy_device_.wig_step_;//out_set_.begin()->second->wig_step_;
		wig_span_ = dummy_device_.wig_span_;//out_set_.begin()->second->wig_span_;
		{
			std::lock_guard<std::mutex> lock(gOutMutex_);
			CopyToBuf (gOutSet_[pipe_index], out_set_);
			if ( pipe_index >= flag_.size() )
				flag_.resize (pipe_index+1);
			flag_[pipe_index]=7;
			Merge (pipe_index, eof_flag);
			if (eof_flag)
			{
				Merge2 ();
				//Printkk();
				WritebwgSection ();
				//PrintSection();
			}
		}
		return (void*) NULL;//in;//&gbwgSection_;
	}

	static void ClearContent(void)
	{
		gOutSet_.clear();
		gCurrentPipelineIndex_=0;
		flag_.clear();
		gBuf_.clear();
		gBufkk_.clear();
		for (auto& Q : gbwgSection_)
			delete Q.second;
		gbwgSection_.clear();
	}

	static std::map <std::string, 	//file_name	item
					std::map < std::string, 	//chromosome	item1
						std::vector < std::vector <bwgVariableStepPacked> >//bwgsection	item2	
			> >& gBufkk_;

	void Merge2 (void)
	{
		size_t max_item_count = 65536;
		for (auto& item : gBuf_)
		{
			for (auto& item1 : item.second)
			{
				size_t total_size = item1.second.size();
				size_t section_count;
				if (total_size%max_item_count!=0)
					section_count = (size_t)(total_size/max_item_count) + 1;
				else
					section_count = (size_t)(total_size/max_item_count);
				gBufkk_[item.first][item1.first].resize (section_count);
				auto itr = item1.second.begin();
				for (auto section_index=0; section_index!=section_count; ++section_index, total_size-=max_item_count)
				{
					gBufkk_[item.first][item1.first][section_index].resize(min (max_item_count, total_size));
					for (auto idx=0; idx!= min (max_item_count, total_size); ++idx, ++itr)
					{
						gBufkk_[item.first][item1.first][section_index][idx].start = itr->first* wig_span_;//(out_set_.begin()->second)->wig_span_;//out_set_[item.first]->wig_span_;
						gBufkk_[item.first][item1.first][section_index][idx].val = itr->second;
					}
				}
			}
		}
		gOutSet_.clear();
		gBuf_.clear();
	}

	void Printkk (void)
	{
		for (auto& item0 : gBufkk_)
		{
			for (auto& item1 : item0.second)
			{
				std::cerr<<"file : chr "<<item0.first<<':'<<item1.first<<'\n';
				std::cerr<<"variable content "<<'\n';
				size_t indx=0;
				for (auto& item2 : item1.second)
				{
					std::cerr<<"section #"<<indx<<'\n';
					for (auto& item3 : item2)
						std::cerr<<item3.start<<":"<<item3.val<<'\t';
					++indx;
					std::cerr<<'\n';
				}
			}
		}
	}

	void Printkk (std::string&& file_name)
	{
	std::ofstream qq (file_name);
		for (auto& item0 : gBufkk_)
		{
			for (auto& item1 : item0.second)
			{
				qq<<"file : chr "<<item0.first<<':'<<item1.first<<'\n';
				qq<<"variable content "<<'\n';
				size_t indx=0;
				for (auto& item2 : item1.second)
				{
					qq<<"section #"<<indx<<'\n';
					for (auto& item3 : item2)
						qq<<item3.start<<":"<<item3.val<<'\t';
					++indx;
					qq<<'\n';
				}
			}
		}
	qq.close();
	}

	static std::map < std::string, //file_name
						bwgSection* >& gbwgSection_;

	void WritebwgSection (void)
	{
		for ( auto& item : gBufkk_ )
		{
			size_t section_count = 0;
			for ( auto& item1 : item.second)
				section_count += item1.second.size();
			gbwgSection_[item.first] = new bwgSection [section_count];
			size_t idx=0;
			for (auto& item1 : item.second)
			{
				for (auto& item2 : item1.second)
				{
					if (idx == section_count-1)
						gbwgSection_[item.first][idx].next = NULL;
					else
						gbwgSection_[item.first][idx].next = &(gbwgSection_[item.first][idx+1]);
					gbwgSection_[item.first][idx].chrom = (char*) item1.first.c_str();
					gbwgSection_[item.first][idx].type = bwgSectionType::bwgTypeVariableStep;
					gbwgSection_[item.first][idx].itemStep = 0;
					gbwgSection_[item.first][idx].itemSpan = wig_span_;//out_set_.begin()->second->wig_span_;//out_set_[item.first]->wig_span_; 
					gbwgSection_[item.first][idx].chromId = 0;
					gbwgSection_[item.first][idx].fileOffset = 0;
					gbwgSection_[item.first][idx].start = (int) (item2.begin()->start);
					gbwgSection_[item.first][idx].end = (int) (item2.rbegin()->start);
					gbwgSection_[item.first][idx].items.variableStepPacked = (bwgVariableStepPacked*) (&item2[0]);
					gbwgSection_[item.first][idx].itemCount = item2.size();
					++idx;
				}
			}
		}
	}

	void PrintSection (void)
	{
		for ( auto& item : gBufkk_ )
		{
			std::cerr<<"file_name : chromosome_count "<<item.first<<":"<<item.second.size()<<'\n';
			size_t section_count = 0;
			for ( auto& item1 : item.second)
				std::cerr<<"chr : section_count "<<item1.first<<" : "<<item1.second.size()<<'\n';
			std::cerr<<"total section_count for file "<<item.first<<" : "<<section_count<<'\n';

			size_t idx=0;
			for (auto& item1 : item.second)
			{
				for (auto& item2 : item1.second)
				{
					std::cerr<<"section-idx : chr : idx "<<section_count<<" : "<<item1.first.c_str()<<" : "<<idx<<'\n';
std::cerr<<"next ptr "<<'\n';
					std::cerr<<"section["<<item.first<<"]["<<idx<<"].next : "<<gbwgSection_[item.first][idx].next<<'\n';
					std::cerr<<"section["<<item.first<<"]["<<idx+1<<"] : "<<&gbwgSection_[item.first][idx+1]<<'\n';

					std::cerr<<"start : end "<<gbwgSection_[item.first][idx].start<<" : "<<gbwgSection_[item.first][idx].end<<'\n';
					std::cerr<<"value : "<<'\n';
					for (auto ii=0; ii!=item2.size(); ++ii)
						std::cerr<<(gbwgSection_[item.first][idx].items.variableStepPacked+ii)->start<<":"<<(float) ((gbwgSection_[item.first][idx].items.variableStepPacked+ii)->val)<<"  ";
					std::cerr<<'\n';
					std::cerr<<"wig_step : wig_span : item_count "<<wig_step_<<":"<<wig_span_<<":"<<item2.size()<<'\n';
//					std::cerr<<"wig_step : wig_span : item_count "<<out_set_[item.first]->wig_step_<<":"<<out_set_[item.first]->wig_span_<<":"<<item2.size()<<'\n';
					std::cerr<<"actuall item_count "<< gbwgSection_[item.first][idx].itemCount<<'\n';
					++idx;
				}
			}
		}
	}

	void PrintSection (std::string&& file_name)
	{
	std::ofstream qq (file_name);
		for ( auto& item : gBufkk_ )
		{
			qq<<"file_name : chromosome_count "<<item.first<<":"<<item.second.size()<<'\n';
			size_t section_count = 0;
			for ( auto& item1 : item.second)
				qq<<"chr : section_count "<<item1.first<<" : "<<item1.second.size()<<'\n';
			qq<<"total section_count for file "<<item.first<<" : "<<section_count<<'\n';

			size_t idx=0;
			for (auto& item1 : item.second)
			{
				for (auto& item2 : item1.second)
				{
					qq<<"section-idx : chr : idx "<<section_count<<" : "<<item1.first.c_str()<<" : "<<idx<<'\n';
qq<<"next ptr "<<'\n';
					qq<<"section["<<item.first<<"]["<<idx<<"].next : "<<gbwgSection_[item.first][idx].next<<'\n';
					qq<<"section["<<item.first<<"]["<<idx+1<<"] : "<<&gbwgSection_[item.first][idx+1]<<'\n';

					qq<<"start : end "<<gbwgSection_[item.first][idx].start<<" : "<<gbwgSection_[item.first][idx].end<<'\n';
					qq<<"value : "<<'\n';
					for (auto ii=0; ii!=item2.size(); ++ii)
						qq<<(gbwgSection_[item.first][idx].items.variableStepPacked+ii)->start<<":"<<(float) ((gbwgSection_[item.first][idx].items.variableStepPacked+ii)->val)<<"  ";
					qq<<'\n';
					qq<<"wig_step : wig_span : item_count "<<wig_step_<<":"<<wig_span_<<":"<<item2.size()<<'\n';
//					qq<<"wig_step : wig_span : item_count "<<out_set_[item.first]->wig_step_<<":"<<out_set_[item.first]->wig_span_<<":"<<item2.size()<<'\n';
					qq<<"actuall item_count "<< gbwgSection_[item.first][idx].itemCount<<'\n';
					++idx;
				}
			}
		}
	qq.close();
	}

	void CopyToBuf (OutputItemType& map_sum, OutPutType &map_item)
	{
		for (auto& item : map_item)	
			map_sum[item.first] = item.second->GetItem();
	}

	void Merge (size_t pipe_index, bool eof_flag) 
	{
		int jump = gCurrentPipelineIndex_;
		for (auto ind=gCurrentPipelineIndex_; ind!=flag_.size(); ++ind)
		{
			if (flag_[ind]==7)
				++jump;
			else
				break;
		}

		//std::cerr<<"Mergeing "<<gCurrentPipelineIndex_<<" : "<<jump<<'\n';

		for (int qqq=gCurrentPipelineIndex_; qqq!=jump; ++qqq)
		{
			for (auto& item0 : (gOutSet_[qqq]) )//.back()) )
			{
				for (auto& item1 : (*item0.second)) 
				{
					auto first_ptr = item1.second.begin();
					auto last_ptr = item1.second.rbegin();
					uint32_t last_index = gBuf_[item0.first][item1.first].size ();
					if ( last_index==0 )
						for (auto& item2 : item1.second)
							gBuf_[item0.first][item1.first].insert (item2);//[item2.first] = item2.second;
					else if ((first_ptr->first) > (gBuf_[item0.first][item1.first].rbegin())->first) 
						for (auto& item2 : item1.second)
							gBuf_[item0.first][item1.first].insert (item2);//[item2.first] = item2.second;
					else	//merge issue
					{
						uint32_t current_last = (gBuf_[item0.first][item1.first].rbegin())->first;
						//std::cerr<<" overlap : 1, last_index-1 : first_ptr->first "<<last_index-1<<":"<<first_ptr->first<<'\n';
						auto overlap_ptr = item1.second.upper_bound (current_last);//(last_index-1);
						//std::cerr<<" overlap : 2 from to "<<first_ptr->first<<":"<<from_pos<<" till : "<<end_pos<<'\n';
						//size_t index=0;
						for (auto itr=item1.second.begin(); itr!=overlap_ptr; ++itr)
						{
							auto intersect_ptr = gBuf_[item0.first][item1.first].find (itr->first);
							if ( intersect_ptr != gBuf_[item0.first][item1.first].end() )
								gBuf_[item0.first][item1.first][intersect_ptr->first] += itr->second;
							else
								gBuf_[item0.first][item1.first].insert (*itr);
						}
						for (auto ptr = overlap_ptr; ptr!=item1.second.end(); ++ptr)
							gBuf_[item0.first][item1.first].insert (*ptr);
					}
				}
			}	
			gOutSet_[qqq].clear();
		}
		gCurrentPipelineIndex_=jump;
		//std::cerr<<"Done Merge "<<gCurrentPipelineIndex_<<" : "<<jump<<'\n';
	}
	
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是簡化重複程式碼，此部份主要負責establish AnnRawBed2Bwg<ValueEstimateClass> objects for handling anno_rawbed -> bwg conversion for each of the to be converted bwg files
	 * @tparam READ_TYPE 自動決定 anno rawbed data type
	 * @param anno_rawbed read 的資料結構，詳情請看 annotation_raw_bed.hpp
	 * @param sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	inline void CalOutput (READ_TYPE &anno_rawbed ,const int sys, const int filter, const int db_index, const int db_depth, const char* db_depth_name)
	{
		std::stringstream name;
//		name <<filter<<db_index<<db_depth<<db_depth_name<<".bwg";
		std::string name_temp (db_depth_name);
		while (true)
		{
			auto pos = name_temp.find (' ');
			if (pos==std::string::npos)
				break;
			else
				name_temp[pos]='_';
		}
		if (name_temp.size()==0)
			name <<"BWG."<<sys<<"."<<filter<<"."<<db_index<<"."<<db_depth<<"."<<'_'<<".bwg";	//output bam file name construction
		else
			name <<"BWG."<<sys<<"."<<filter<<"."<<db_index<<"."<<db_depth<<"."<<name_temp<<".bwg";	//output bam file name construction
		auto name_str = name.str();

		if (out_set_.find (name_str) != out_set_.end() )
			(out_set_[name_str])->run (anno_rawbed);
		else 
		{
			out_set_.insert ( {name_str, std::make_shared < AnnRawBed2Bwg<ValueEstimateClass> > (this_pipe_index, name_str)} );
			(out_set_[name_str])->run (anno_rawbed);
		}
	}
};

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::mutex
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::gOutMutex_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < int, std::map < std::string, std::shared_ptr < std::map < std::string, std::map < uint32_t, float> > > > >&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::gOutSet_//;
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
int&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::gCurrentPipelineIndex_//=0;
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gCurrentPipelineIndex_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector <int>&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::flag_//(0);
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::flag_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map <std::string, std::map < std::string, std::map <uint32_t, float> > >&	
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::gBuf_//;
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gBuf_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map <std::string, std::map < std::string, std::vector < std::vector <bwgVariableStepPacked> > > >&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::gBufkk_//;
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gBufkk_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < std::string, bwgSection* >&
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg>::gbwgSection_//;
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBwg, ANALYZER_TYPELIST_GLOBAL_TOBWG>::gbwgSection_;


template<class INPUT_TYPE>
class AnalyzerImplInitToBwg
{
public:

	INPUT_TYPE in;
	size_t pipe_index_;
	bool eof_flag_;
//	char* chrsize_;
	std::map<int,int> analyzer_count_type_;

    //typedef std::map <std::string, std::map <std::string, std::map <uint32_t, float> > > 
	typedef std::map < std::string,	bwgSection* >
	OutputItemType;

	static OutputItemType* gPtr_;

	
	AnalyzerImplInitToBwg(INPUT_TYPE i)
		: in (i)
	{}

	AnalyzerImplInitToBwg()
	{}

	template<class ANALYZER_TYPELIST>
	void 
	operator()(ANALYZER_TYPELIST t)
	{
		//取得 analyzer type
		typedef typename boost::mpl::at<ANALYZER_TYPELIST, boost::mpl::int_<0> >::type AnalyzerType;
		AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerType::value> Analysis(in);
		gPtr_ = (OutputItemType*) Analysis(analyzer_count_type_[AnalyzerType::value], pipe_index_, eof_flag_);//, chrsize_);	
        ++analyzer_count_type_[AnalyzerType::value];
	}

	AnalyzerImplInitToBwg(INPUT_TYPE i, size_t pipe_index, bool eof_flag)//, char* chrsize)
		: in (i), pipe_index_ (pipe_index), eof_flag_ (eof_flag)//, chrsize_(chrsize)
	{}
};

template<class INPUT_TYPE>
std::map < std::string,	bwgSection* >*
AnalyzerImplInitToBwg <INPUT_TYPE>::gPtr_;


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerToBwg
{
public:
	typedef std::map < std::string,	bwgSection* >
	OutputItemType;
	static OutputItemType* AnalyzerToBwgPtr_;

	OutputItemType*
	run (INPUT_TYPE in, size_t pipe_index, bool eof_flag)//, char* chrsize) 
	{
		boost::mpl::for_each<ANALYZER_TYPELIST> ( AnalyzerImplInitToBwg<INPUT_TYPE>( in, pipe_index, eof_flag) );
		AnalyzerToBwgPtr_ = AnalyzerImplInitToBwg<INPUT_TYPE>::gPtr_;
		return AnalyzerImplInitToBwg<INPUT_TYPE>::gPtr_;
	}
};

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < std::string,	bwgSection* >*
AnalyzerToBwg <INPUT_TYPE, ANALYZER_TYPELIST>::AnalyzerToBwgPtr_;

#endif
