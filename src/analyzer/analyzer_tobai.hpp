/**                                                                                                                                                                                     
 *  @file analyzer_tobai.hpp
 *  @brief achieve bai file written for the analyzer 
 *  @author C-Salt Corp.
 */
#ifndef ANALYZER_TOBAI_HPP_
#define ANALYZER_TOBAI_HPP_
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <map>
#include <tuple>
#include <utility>
#include <vector>
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"

/**
 * @brief Class handling bai file written
 */
template <typename OUTPUTHANDLE>
class ToBai
{
   	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int, int, int> > > > BamIndexType;
	std::map <int, BamIndexType>* index_ptr_;
	std::vector <int> carryin_bin_;// ({0, 9, 73, 585, 4681, 37450});
	int barcode_index_;
	std::map < std::string, 	//file_name	item1
		std::map < std::string, 	//chromosome	item2
			std::map < int, 	//bin	item3
				std::vector < std::tuple <int, int, int, int> > > > > // min_coffset, max_coffset, min_uoffset, max_uoffset
					bai_bin_;

	std::map < std::string, 	//file_name 
		std::map < std::string, 	//chromosome
			std::map <int,	//tile
				std::tuple <uint64_t, int, int> > > > //ioffset, coffset, uoffset
					bai_intv_;	

	std::map <std::string, std::map<std::string, uint32_t> >& ReadCount_;
	std::map <std::pair<std::string,std::string>, std::map<int, int> >& cross_board_bin_;

public:
	ToBai ( std::map <int, BamIndexType>* indexptr, int barcode_in, std::map <std::string, std::map<std::string, uint32_t> >& rcin, 
			std::map<std::pair<std::string,std::string>, std::map <int, int> >& crmap )
			: index_ptr_ (indexptr)
			, carryin_bin_ ({0, 8, 72, 584, 4680, 37450})//({0, 9, 73, 585, 4681, 37450})	
			, barcode_index_ (barcode_in)
			, ReadCount_ (rcin)
			, cross_board_bin_ (crmap)
		{
			GetBinInfo();
//std::string file_path ("output/sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index_]+"/bam/");
//			PrintBinInfo(file_path);
			GetIndexInfo();
//			PrintIndexInfo(file_path);
			WriteBaiFw();
		}

	void PrintBinInfo (std::string& file_path)
	{
		std::stringstream ss;
		ss << file_path << "bin_info";
		std::ofstream bininfo (ss.str());
		for (auto& item1 : bai_bin_)
		{
			bininfo<<"file_name "<<item1.first<<'\n';
			for (auto& item2 : item1.second)
			{
				bininfo<<"=========chr "<<item2.first<<"=========\n";
				for (auto& item3 : item2.second)
				{
					bininfo<<"#bin "<<item3.first<<'\n';
					for (auto& yy : item3.second)
					{
						bininfo<<"original: "<<std::get<0>(yy)<<'\t'<<std::get<1>(yy)<<'\t'<< std::get<2>(yy)<<'\t'<< std::get<3>(yy)<<'\n';
						bininfo<<"chunk beg_ "<<(size_t) (std::get<0>(yy)*pow(2,16)+std::get<2>(yy))<<'\n';
						bininfo<<"chunk end_ "<<(size_t) (std::get<1>(yy)*pow(2,16)+std::get<3>(yy))<<'\n';
						bininfo<<"min coffset & uoffset "<<std::get<0>(yy)<<'\t'<<std::get<2>(yy)<<'\n';
						bininfo<<"max coffset & uoffset "<<std::get<1>(yy)<<'\t'<<std::get<3>(yy)<<'\n';
					}
				}
			}
		}
		bininfo.close();
	}

	void PrintIndexInfo (std::string& file_path)
	{
		std::stringstream ss;
		ss << file_path << "index_info";
		std::ofstream indexinfo (ss.str());
		for (auto& item1 : bai_intv_)
		{
			indexinfo<<"file_name "<<item1.first<<'\n';
			for (auto& item2 : item1.second)
			{
				indexinfo<<"---------chromosome_ "<<item2.first<<"---------\n";
				for (auto& item3 : item2.second)
					indexinfo<<"tile "<<item3.first<<" : "<<std::get<0>(item3.second)<<'\t'<<std::get<1>(item3.second)<<'\t'<<std::get<2>(item3.second)<<'\n';
			}
		}
	}

private:
	int reg2bins(int beg, int end, std::vector <int>& list)/* calculate the list of bins that may overlap with region [beg,end) (zero-based) */
	{   
		int i = 0, k;
		--end;
		list.push_back (0);//[i++] = 0;
		for (k = 1 + (beg>>26); k <= 1 + (end>>26); ++k) list.push_back (k);//[i++] = k;
		for (k = 9 + (beg>>23); k <= 9 + (end>>23); ++k) list.push_back (k);//[i++] = k;
		for (k = 73 + (beg>>20); k <= 73 + (end>>20); ++k) list.push_back (k);//[i++] = k;
		for (k = 585 + (beg>>17); k <= 585 + (end>>17); ++k) list.push_back (k);//[i++] = k;
		for (k = 4681 + (beg>>14); k <= 4681 + (end>>14); ++k) list.push_back (k);//[i++] = k;
		return i;
	}

	int reg2bin(int beg, int end)
	{
		--end;
		if (beg>>14 == end>>14) return ((1<<15)-1)/7 + (beg>>14);
		if (beg>>17 == end>>17) return ((1<<12)-1)/7 + (beg>>17);
		if (beg>>20 == end>>20) return ((1<<9)-1)/7 + (beg>>20);
		if (beg>>23 == end>>23) return ((1<<6)-1)/7 + (beg>>23);
		if (beg>>26 == end>>26) return ((1<<3)-1)/7 + (beg>>26);
		return 0;
	}

//std::map <pipe, std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > > BamIndexType;
//			pipe_vs_name
//									file_name_pair
//																				chr_tile_uoffset

	std::map <std::string, std::map < std::string, std::map <int, std::tuple <int, int, int, int, int, int> > > > IndexMerged_;
																		 //coff_min, coff_max, uoff_min, off_max, start, end						
	void Merge (void)
	{
		for (auto& pipe_vs_name : *index_ptr_)
		{
			for (auto& file_name_pair : pipe_vs_name.second)
			{
				auto& file_name = file_name_pair.first;
				auto& coffset = std::get<0>(file_name_pair.second);

				for (auto& chr_tile_uoffset : std::get<1>(file_name_pair.second) )
				{
					auto& merge_map = IndexMerged_[file_name][chr_tile_uoffset.first.first];
					merge_map.insert ( std::make_pair (
								chr_tile_uoffset.first.second, 
								std::make_tuple (coffset.first + std::get<4>(chr_tile_uoffset.second), 
												 coffset.first + coffset.second , 
												 std::get<0>(chr_tile_uoffset.second), 
												 std::get<1>(chr_tile_uoffset.second),
												 std::get<2>(chr_tile_uoffset.second), 
												 std::get<3>(chr_tile_uoffset.second) ) ) );
				}
			}
		}
		for (auto& name_pair: IndexMerged_)
		{
			for (auto chr_itr=name_pair.second.begin(); chr_itr!=name_pair.second.end(); ++chr_itr)//(auto& chr_pair: name_pair.second)
			{

				auto chr_itr_next = chr_itr;
				std::advance (chr_itr_next, 1);	

				for (auto itr=chr_itr->second.begin(); itr!=chr_itr->second.end(); ++itr)//(auto& tile_pair: chr_pair.second)
				{
					auto itr_next = itr;
					std::advance (itr_next, 1);

					if (itr_next == chr_itr->second.end() )
					{
						if (chr_itr_next == name_pair.second.end() )
						{
							std::get<1>(itr->second)=std::get<0>(index_ptr_->rbegin()->second[name_pair.first]).first+
													 std::get<0>(index_ptr_->rbegin()->second[name_pair.first]).second+28;
							std::get<3>(itr->second)=0;
						}
						else //if (itr_next = name_pair.second.end() )
						{
							std::get<1>(itr->second)=std::get<0>(chr_itr_next->second.begin()->second);
							std::get<3>(itr->second)=std::get<2>(chr_itr_next->second.begin()->second);
						}
					}
					else
					{
						std::get<1>(itr->second)=std::get<0>(itr_next->second);
						std::get<3>(itr->second)=std::get<2>(itr_next->second);
					}
				}
			}
		}
	}

	void GetBinInfo (void)
	{
		Merge();
		for (auto& name_pair: IndexMerged_)
		{
			for (auto& chr_pair: name_pair.second)
			{
				for (auto& tile_pair: chr_pair.second)
				{
					std::vector <int> QQ;
					reg2bins (std::get<4>(tile_pair.second), std::get<5>(tile_pair.second), QQ );
					for (auto& yy : QQ)
					{

						auto& cbb = cross_board_bin_[std::make_pair (name_pair.first, chr_pair.first)];
						if (yy<4681 && cbb.find (yy)==cbb.end() )
							continue;

						if (bai_bin_[name_pair.first][chr_pair.first][yy].size()==0)
						{
							bai_bin_[name_pair.first][chr_pair.first][yy].resize (1);
							std::get<0> (bai_bin_[name_pair.first][chr_pair.first][yy][0]) = (std::get<0>(tile_pair.second));//min_coffset
							std::get<2> (bai_bin_[name_pair.first][chr_pair.first][yy][0]) = std::get<2>(tile_pair.second);//min_uoffset
							std::get<1> (bai_bin_[name_pair.first][chr_pair.first][yy][0]) = (std::get<1>(tile_pair.second));//max_coffset
							std::get<3> (bai_bin_[name_pair.first][chr_pair.first][yy][0]) = std::get<3>(tile_pair.second);//max_uoffset
						}
						else
						{
							std::get<1> (bai_bin_[name_pair.first][chr_pair.first][yy][0]) = (std::get<1>(tile_pair.second));//max_coffset
							std::get<3> (bai_bin_[name_pair.first][chr_pair.first][yy][0]) = std::get<3>(tile_pair.second);//max_uoffset
						}
					}
				}
			}
		}
//	std::map < std::string, 	//file_name	item1
//		std::map < std::string, 	//chromosome	item2
//			std::map < int, 	//bin	item3
//				std::vector < std::tuple <int, int, int, int> > > > > // min_coffset, max_coffset, min_uoffset, max_uoffset
//					bai_bin_;

		for (auto& item1 : bai_bin_)
		{
			int index=0;
			for (auto& item2 : item1.second)	
				// add last bin #37450 content, which always includes two bins, 
				// wherein the 1st bin having content corresponding to bin #0 and the 2nd bin having zero content
			{
				if (item2.second.empty())
					continue;

				auto itrf = item2.second.begin();
				auto itr = item2.second.rbegin();
				//std::cerr<<"itr->second.size() "<<itr->second.size()<<'\n';
				if (itr->second.empty())
					continue;
//std::cerr<< "TUPLE content: "<<std::get<0>((itr->second)[0])<<" "<<std::get<2>((itr->second)[0])<<std::endl;
//std::cerr<<std::get<1>((itr->second)[0])<<'\t'<<std::get<3>((itr->second)[0])<<std::endl;
//std::cerr<<ReadCount_[item1.first][item2.first]<<std::endl;

				if (index=0)
				{
					item2.second[37450].push_back (std::make_tuple (
							std::get<0>((itrf->second)[0]),//std::get<0>(item2.second[0][0]),//std::get<1>(item2.second[0][0]), 
							std::get<1>((itr->second)[0]), 
							0,//std::get<3>(item2.second[0][0])
							std::get<3>((itr->second)[0])));
				}
				else
				{
					item2.second[37450].push_back (std::make_tuple (
							std::get<0>((itrf->second)[0]),//std::get<0>(item2.second[0][0]),//std::get<1>(item2.second[0][0]), 
							std::get<1>((itr->second)[0]), 
							std::get<2>((itrf->second)[0]),//std::get<2>(item2.second[0][0]),//std::get<3>(item2.second[0][0])
							std::get<3>((itr->second)[0]) 
							));
				}
				item2.second[37450].push_back (std::make_tuple (0, 0, ReadCount_[item1.first][item2.first], 0));//(std::make_tuple (0, 0, 0, 0));
				++index;
			}
		}
	}

	void GetIndexInfo(void)
	{
		for (auto& item1 : *index_ptr_)
			for (auto& item2 : item1.second)
				for (auto& item3 : std::get<1>(item2.second) )
				{
					bai_intv_[item2.first][item3.first.first][0] = std::make_tuple ( 0, 0, 0 );
					bai_intv_[item2.first][item3.first.first].insert ({item3.first.second, std::make_tuple (
							(((uint64_t)((std::get<0>(item2.second).first+std::get<4> (item3.second)))  << 16) + std::get<0>(item3.second)), 
							std::get<0>(item2.second).first, 
							std::get<0>(item3.second) )});
				}
		for (auto& item1 : bai_intv_)
		// filling missing tile elements into bai_intv_[file_name][chromosome] 
		{
			for (auto& item2 : item1.second)
			{
				for (auto ii=++item2.second.begin(); ii!=item2.second.end(); ++ii)
				{
					int tail = ii->first;
					int head = (--ii)->first;
					++ii;
					for (auto idx=head+1; idx!=tail; ++idx)
						bai_intv_[item1.first][item2.first][idx] = bai_intv_[item1.first][item2.first][head];
				}
			}
		}
	}

	void WriteBaiFw (void)//std::string& file_path)
	{
		std::vector <std::shared_ptr <OUTPUTHANDLE> > fvec;
		//std::cerr<<"bai_bin_.size "<<bai_bin_.size()<<'\n';

		for (auto& item0 : bai_bin_)
		{
		    DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
			std::string output_path ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index_]+"/bam/");
			std::string output_name (item0.first+".bai");
			//std::cerr<<"filename & path "<<item0.first<<'\t'<<output_path<<'\n';
			file_writter_parameter.set_filename( output_name , output_path);
			fvec.push_back (std::make_shared<OUTPUTHANDLE> (file_writter_parameter) );

			char* magic = "BAI\1";
			int32_t n_ref = PipelinePreparator<>::gChromosome_vector_.size();//item0.second.size();	
			fvec.back()->write(magic, 4);
			fvec.back()->write(reinterpret_cast<const char *>(&n_ref), sizeof(n_ref));
			int iiii=0;

			for (auto& item1 : PipelinePreparator<>::gChromosome_vector_)
			{

//int kkk=0;
//int32_t n_bin = item0.second[item1].size()-8;
				int32_t n_bin = item0.second[item1].size();
				fvec.back()->write(reinterpret_cast<const char *>(&n_bin), sizeof(n_bin));
				for (auto& item2 : item0.second[item1])//item1.second)
				{
//if (kkk<8)
//{
//	++kkk;
//	continue;
//}
					uint32_t bin = item2.first;
					int32_t n_chunk = item2.second.size();
					fvec.back()->write (reinterpret_cast<const char *>(&bin), sizeof(bin));
					fvec.back()->write (reinterpret_cast<const char *>(&n_chunk), sizeof(n_chunk));

					for (auto &item3: item2.second)
					{
						uint64_t chunk_beg = (uint64_t)(std::get<0>(item3)*pow(2,16)+std::get<2>(item3));
						uint64_t chunk_end = (uint64_t)(std::get<1>(item3)*pow(2,16)+std::get<3>(item3));
						fvec.back()->write (reinterpret_cast<const char *>(&chunk_beg), sizeof(chunk_beg));
						fvec.back()->write (reinterpret_cast<const char *>(&chunk_end), sizeof(chunk_end));
					}
				}
				int32_t n_intv = bai_intv_[item0.first][item1].size();
				fvec.back()->write (reinterpret_cast<const char *>(&n_intv), sizeof(n_intv));

				for (auto& qq : bai_intv_[item0.first][item1])
				{
					uint64_t ioffset = std::get<0>(qq.second);
					fvec.back()->write (reinterpret_cast<const char *>(&ioffset), sizeof(ioffset));
				}	
				++iiii;
			}
			fvec.back()->bs_async_close();
		}
		for (auto& q : fvec)
			q->close();
	}
};

#endif
