#ifndef ANALYZER_PRINTER_SEQUENCE_LOGO_HPP_
#define ANALYZER_PRINTER_SEQUENCE_LOGO_HPP_

#include <fstream>
#include <array>
#include <boost/filesystem.hpp>
#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"
#include "boost/algorithm/string/split.hpp"
//#include "analyzer_utility.hpp"
//#include "analyzer_policy.hpp"
//#include "analyzer_length_distribution.hpp"

template<>
class AnalyzerParameter<AnalyzerTypes::SequenceLogoPrinter>
{
public:
	/** 
	 * @brief 定義 length distribution參數有哪些
	 */
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	/// @brief 
	typedef boost::mpl::int_<1> AnnoIdx;
	
//	typedef boost::mpl::int_<2> Xaxis;
	
//	typedef boost::mpl::int_<3> Yaxis;
	
//	typedef boost::mpl::int_<4> Zaxis;
	
//	typedef boost::mpl::int_<5> Xlimit;
	
//	typedef boost::mpl::int_<6> Ylimit;
	
//	typedef boost::mpl::int_<7> Zlimit;
	
//	typedef boost::mpl::int_<8> Len;

//	typedef boost::mpl::int_<9> Anno;

//	typedef boost::mpl::int_<10> Seq;

	typedef boost::mpl::int_<11> PrefixName;

	typedef boost::mpl::int_<12> IoHandlerType;

//	typedef boost::mpl::int_<13> XSort;

//	typedef boost::mpl::int_<14> YSort;

//	typedef boost::mpl::int_<15> ZSort;
};


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogoPrinter>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::SequenceLogoPrinter> AnaPara;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::AnnoIdx, boost::mpl::int_<0> >::type AnnoIdx;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType; 
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::PrefixName, boost::mpl::string<'Tab', 'le'> >::type PrefixName;
    typedef std::map<std::tuple<int,int,int,int,std::string>, std::vector< std::tuple <double, double, double, double, double> > > OutSetType;
	std::map <std::string, std::vector<double> > result_ratio_;

//	map[miRNA_name, vector<double>]
	std::map <std::string, std::vector< std::tuple<double, double, double, double, double> > > result_;
	std::vector <OutSetType> &OutSet_;
	std::vector<std::string> &OutNamePrefix;
	std::string output_path_;

	/// @brief 輸入的資料
	INPUT_TYPE &in;

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::SequenceLogo>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)
		  , OutSet_(AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutSet_)
		  , OutNamePrefix(AnalyzerImpl<INPUT_TYPE, ANALYZER_SEQUENCE_LOGO_GLOBAL, AnalyzerTypes::SequenceLogo>::gOutNamePrefix_)
		  , output_path_("")
	{}
	
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		if(eof_flag)
		{
			output_path_ = std::string("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+ PipelinePreparator<>::gBarcode_vector_[barcode_index] + std::string("/sequence_logo/"));
			mkdir();
			getresult (OutSet_[AnnoIdx::value]);
			Output ("ppm");//("pwm");
			Output ("pwm");//("height_matrix");
		}
		return (void*) &in;
	}

	void Output (const std::string& tail_str)
	{
		for (auto& content: result_)	//pair < file_name, map <miRNA, std::vector<double> > >
		{
			std::string filename = content.first + "." + tail_str;
			DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
			file_writter_parameter.set_filename(filename , output_path_);
			std::shared_ptr <IoHandlerType> out_ (std::make_shared <IoHandlerType> (file_writter_parameter));

			std::cerr << output_path_ + filename << std::endl;

			*(out_) << "====================" << std::endl;
			*(out_) << filename << std::endl;
			*(out_) << "--------------------" << std::endl;

			*(out_) << "Name\tA\tC\tG\tT\n";
			int pos_idx=1;
			for (auto& pos_tuple: content.second)
			{
				*(out_) << std::setprecision(3) << std::fixed << std::to_string (pos_idx) << '\t'
						<< std::get<0> (pos_tuple)*result_ratio_[filename][pos_idx-1] <<'\t' 
						<< std::get<1> (pos_tuple)*result_ratio_[filename][pos_idx-1] <<'\t' 
						<< std::get<2> (pos_tuple)*result_ratio_[filename][pos_idx-1] <<'\t'
						<< std::get<3> (pos_tuple)*result_ratio_[filename][pos_idx-1] <<'\n';
				++pos_idx;
			}
			*(out_) << "--------------------" << std::endl;
			out_->close();
		}
	}

	void mkdir()
	{
		boost::filesystem::path dir_output("output");
		boost::filesystem::create_directory(dir_output);
		
		boost::filesystem::path dir_analyzer(output_path_);
		boost::filesystem::create_directory(dir_analyzer);
	}

    void getresult (OutSetType& output)
    {   
        std::string NamePrefix = OutNamePrefix[AnnoIdx::value];
        for (auto& tuple_pair: output)
        {
            std::string filename ("Table.");//.sequence_logo.");
			filename += NamePrefix + '.';
            filename += std::to_string(std::get<0>(tuple_pair.first)) + ".";//sys
            filename += std::to_string(std::get<1>(tuple_pair.first)) + ".";//filter
            filename += std::to_string(std::get<2>(tuple_pair.first)) + ".";//db_idx
            filename += std::to_string(std::get<3>(tuple_pair.first)) + ".";//db_depth
			filename += std::get<4>(tuple_pair.first);
            result_.insert ({filename, tuple_pair.second});

			auto str_ppm = filename + ".ppm";//".pwm";
			result_ratio_[str_ppm] = std::vector<double> (tuple_pair.second.size(), 1.0);
			auto str_pwm = filename + ".pwm";//".height_matrix";
			for (auto& tuple_content: tuple_pair.second)
				result_ratio_[str_pwm].push_back (std::get<4>(tuple_content));
        }
    } 
};
#endif
