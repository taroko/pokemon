#ifndef ANALYZER_LENGTH_DISTRIBUTION_HPP_
#define ANALYZER_LENGTH_DISTRIBUTION_HPP_

#include <cassert>
#include <functional>

#include "../constant_def.hpp"
#include "../tuple_utility.hpp"

#include "analyzer_utility.hpp"
#include "analyzer_policy.hpp"
//#include "analyzer_printer_old.hpp"

/**
 * @brief Analyzer 此class為提供計算Langth distribution的Analyzer參數
 * @tparam ANALYZER_TYPE 特化為 LengthDistribution
 */
template<>
class AnalyzerParameter<AnalyzerTypes::LengthDistribution>
{
public:
	/** 
	 * @brief 定義 length distribution參數有哪些
	 */
	
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<1> FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定db。
	typedef boost::mpl::int_<2> DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef boost::mpl::int_<3> DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef boost::mpl::int_<4> DbDepthNameType;
	
	/// @brief GetReadLengthClass 決定取得 anno raw bed length 的方法，也就是length distribution 的 length，預設回傳 read length，也可以自定回傳其他數值
	typedef boost::mpl::int_<5> GetReadLengthClass;
	
	/// @brief CalReadCountClass 決定取得 anno raw bed count 的方法，也就是length distribution 的 value，預設回傳 read count，也可以自定回傳其他數值
	typedef boost::mpl::int_<6> CalReadCountClass;
	
	/// @brief GetReadSeqClass 決定取得 anno raw bed sequence 的方法，也就是read sequence 的 content，預設回傳 空字串，也可以自定回傳其他數值
	typedef boost::mpl::int_<7> GetReadSeqClass;
	
	typedef boost::mpl::int_<8> Printer;

	typedef boost::mpl::int_<-1> ReturnType;
	
	typedef boost::mpl::int_<9> DbDepth2NameType;
};


class ANALYZER_TYPELIST_GLOBAL_LENDIST {};


template<class INPUT_TYPE>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>
{
public:
	typedef std::map<int, std::map<std::tuple<int,int,int,int,std::string,std::string>, std::tuple<double, double, double> > > OutPutType ;
	
	/// @brief 因為每個run為mt，所以最後要整合進一個 global 的資料結構
	static std::vector<OutPutType> gOutSet_;
	
	static std::vector< std::vector<OutPutType> > gTmpOutSet_;
	
	static std::vector<std::string> gOutNamePrefix_;
	
	static std::vector<std::mutex> gOutMutex_;
	
	template <typename T>
	static void free( T & t ) {
		T tmp;
		t.swap( tmp );
	}
	
	static void ClearContent (void)
	{
		//gOutSet_.clear();
		//gOutNamePrefix_.clear();
		//gTmpOutSet_.clear();
		free(gOutSet_);
		free(gOutNamePrefix_);
		free(gTmpOutSet_);
		gOutSet_.resize(200);
		gOutNamePrefix_.resize(200);
		gTmpOutSet_.resize(200);
	}
};



/**
 * @brief Analyzer 的實作，此為計算 Length distribation。幾乎範型的可以計算所有 length distribution，可以用參數設定要的長度分布
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::map<boost::mpl::pair<KEY, VALUE> >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，此為 Length distribation 特化
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	:public AnalysisLenDistImpl<INPUT_TYPE>
{
public:
	typedef typename std::iterator_traits<INPUT_TYPE>::value_type::value_type READ_TYPE;
	
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::LengthDistribution> AnaPara;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉"filter tag"=1，0=>去掉 "filter tag"=0。每一個AnalyzerParameter都應該要有此參數
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::FilterType, boost::mpl::int_<1> >::type FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定db。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbIndexType, boost::mpl::int_<-2> >::type DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthType, boost::mpl::int_<-2> >::type DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，此轉為 "-1"，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthNameType, boost::mpl::string<'-1'> >::type DbDepthNameType;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepth2NameType, boost::mpl::string<'-3'> >::type DbDepth2NameType;
	
	/// @brief GetReadLengthClass 決定取得 anno raw bed length 的方法，也就是length distribution 的length，預設回傳 read length，也可以自定回傳其他數值
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::GetReadLengthClass, GetReadLengthDefault >::type GetReadLengthClass;
	
	/// @brief CalReadCountClass 決定取得 anno raw bed count 的方法，也就是length distribution 的 value，預設回傳 read count，也可以自定回傳其他數值
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::CalReadCountClass, CalReadCountDefault >::type CalReadCountClass;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::GetReadSeqClass, GetReadSeqDefault >::type GetReadSeqClass;
	
	//typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Printer, LenDistSeqPrinter >::type PrinterClass;

	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::ReturnType >::type ReturnType;
	
	/// @brief 記錄此run為第N個使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	int this_analyzer_count_;
	
	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	/** 
	 * @brief 輸出的資料型別
	 * \n output vector {read_length, tuple{sys, filter, db_idx, db_depth, db_depth_name, ReadSeqinfo}, tuple< read_number, all ppm, filter ppm >} 
	 * \n sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * \n sys 是因為db_index, db_depth "空"沒辦法記錄。ps. 自從空改成 -2後，應該就可以不需要 sys了，但目前還有待修改...
	 */
	typedef std::map<int, std::map<std::tuple<int,int,int,int,std::string,std::string>, std::tuple<double, double, double> > > OutPutType ;
	/// @brief 輸出的資料
	OutPutType out_set_;
	
	/// @brief 因為每個run為mt，所以最後要整合進一個 global 的資料結構
	static std::vector<OutPutType> &gOutSet_;
	
	static std::vector<std::vector<OutPutType> > &gTmpOutSet_;
	static std::vector<std::string> &gOutNamePrefix_;
	
	static std::vector<std::mutex> &gOutMutex_;
	
	//
	// std::bind(&AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>::CalOutput, this)
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)
		, this_analyzer_count_(0)
		, AnalysisLenDistImpl<INPUT_TYPE>(
			std::bind(
				&AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>::CalOutput
				, this
				, std::placeholders::_1
				, std::placeholders::_2
				, std::placeholders::_3
				, std::placeholders::_4
				, std::placeholders::_5
				, std::placeholders::_6
			)
		)
	{}
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl()
	{}
	
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief 主要執行Analyzer的對外介面
	 * @param this_analyzer_count 第N個 使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	 * @return void
	 */
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		this_analyzer_count_ = this_analyzer_count;
		//Analysis<GetReadLengthClass, CalReadCountClass> (in, FilterType::value, DbIndexType::value, DbDepthType::value, DbDepthNameType::value);
		if(this_analyzer_count == 0)
		{
			/// @brief input, filter = each, db_idx = -1, db_depth=0, db_depth_value, sys
			this->Analysis (in, -1, -1, 0, "-2", "-3", 0);
			this->Analysis (in, 1, -1, 0, "-2", "-3", 0);
			this->Analysis (in, 0, -1, 0, "-2", "-3", 0);
		}
		this->Analysis (in, FilterType::value, DbIndexType::value, DbDepthType::value, boost::mpl::c_str<DbDepthNameType>::value, boost::mpl::c_str<DbDepth2NameType>::value);


		{
			std::lock_guard<std::mutex> lock(gOutMutex_[this_analyzer_count+1]);
			//gTmpOutSet_[this_analyzer_count_].push_back(out_set_);
			MapSum(gOutSet_[this_analyzer_count_], out_set_);
		}
			
		//std::cerr << "gOutSet_[this_analyzer_count_] *" << &gOutSet_[this_analyzer_count_] << std::endl;
		//std::cerr << "this_analyzer_count_ " << this_analyzer_count_ << std::endl;
		
		//if 最後一個步驟
		if (eof_flag)
		{
			//int i=0;
			//for(auto &tmp_out_set : gTmpOutSet_[this_analyzer_count_])
			//{
			//	MapSum(gOutSet_[this_analyzer_count_], tmp_out_set);
			//	i++;
			//}
			
			std::string db_depth_name_str (boost::mpl::c_str<DbDepthNameType>::value);
			std::string db_depth2_name_str (boost::mpl::c_str<DbDepth2NameType>::value);
			db_depth_name_str = convert_name(db_depth_name_str, 1);
			db_depth2_name_str = convert_name(db_depth2_name_str, 2);
 
			//lendist.Normal.full.GMPM
			
			std::string filename_prefix("lendist.");
			filename_prefix += boost::mpl::c_str< typename GetReadSeqClass::NAME_TYPE >::value;
			filename_prefix += ".";
			filename_prefix += boost::mpl::c_str< typename GetReadLengthClass::NAME_TYPE >::value;
			filename_prefix += ".";
			filename_prefix += boost::mpl::c_str< typename CalReadCountClass::NAME_TYPE >::value;
			
			filename_prefix += ".";
			filename_prefix += db_depth_name_str;
			filename_prefix += ".";
			filename_prefix += db_depth2_name_str;
			gOutNamePrefix_[this_analyzer_count] = filename_prefix;
		}
		return (void*) &in;//NULL;
	}
	std::string convert_name(std::string str, int type)
	{
		if(type == 1)
		{
			if(str == "-1")
				return "biotype";
			else if(str == "-2")
				return "merged";
			else if(str == "-3")
				return "no_biotype";
			else
			{
				std::replace (str.begin(), str.end(), '.', '_');
				return str;
			}
		}
		else
		{
			if(str == "-1")
				return "detail";
			else if(str == "-2")
				return "merged_detail";
			else if(str == "-3")
				return "no_detail";
			else
			{
				std::replace (str.begin(), str.end(), '.', '_');
				return str;
			}
		}
	}
	void printer()
	{
		std::string filename_prefix("");
		filename_prefix += boost::mpl::c_str< typename GetReadSeqClass::NAME_TYPE >::value;
		filename_prefix += ".";
		filename_prefix += boost::mpl::c_str< typename GetReadLengthClass::NAME_TYPE >::value;
		filename_prefix += ".";
		filename_prefix += boost::mpl::c_str< typename CalReadCountClass::NAME_TYPE >::value;
		
		//std::cerr << "seq depth filter " << std::get<0>(gOutSet_[0][-1][std::make_tuple(0,1,0,0,"","")]) << std::endl;
		//std::cerr << "seq depth all" << std::get<0>(gOutSet_[0][-1][std::make_tuple(0,-1,0,0,"","")]) << std::endl;
		
		//PrinterClass::TsvPrinter2(gOutSet_[this_analyzer_count_], filename_prefix, 
		//	std::get<0>(gOutSet_[0][-1][std::make_tuple(0,1,0,0,"","")]),
		//	std::get<0>(gOutSet_[0][-1][std::make_tuple(0,-1,0,0,"","")])
		//);
	}
	
		
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是簡化重複程式碼，此部份主要負責 insert into map of output
	 * @tparam READ_TYPE 自動決定 anno rawbed data type
	 * @param anno_rawbed read 的資料結構，詳情請看 annotation_raw_bed.hpp
	 * @param sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void CalOutput (READ_TYPE &anno_rawbed ,const int sys, const int filter, const int db_index, const int db_depth, const char* db_depth_name)
	{
		//std::cerr << "CalOutput: " << filter << "\t" << db_index << "\t" << db_depth << "\t" << db_depth_name << std::endl;
		//std::cerr << "----------------------------\nAnnoRawBed: " << anno_rawbed << "\n----------------------------\n";
		
		int read_length = GetReadLengthClass::GetReadLength(anno_rawbed);
		double count = CalReadCountClass::CalReadCount(anno_rawbed);
		std::string read_seq = GetReadSeqClass::GetReadSeq(anno_rawbed);

		std::string db_depth_name_1 (db_depth_name);//, strlen (db_depth_name));
		std::replace (db_depth_name_1.begin(), db_depth_name_1.end(), '.', '_');


if(this_analyzer_count_ == 0 && read_seq!="" )
{
	std::get<0> ( out_set_[-1][std::make_tuple(sys,filter,db_index,db_depth,"", "")] ) += count;
	std::get<0> ( out_set_[read_length][std::make_tuple(sys,filter,db_index,db_depth,"", "")] ) += count;
}
		std::get<0> ( out_set_[-1][std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1, read_seq)] ) += count;
		std::get<0> ( out_set_[read_length][std::make_tuple(sys,filter,db_index,db_depth,db_depth_name_1, read_seq)] ) += count;
	}
	
	void MapSum(OutPutType &map_sum, OutPutType &map_item)
	{
		map_sum += map_item;
/*
		//第一層 map<len, map<tuple, tuple> >
		for(auto &map_item_pair : map_item)
		{
			//第二層 map<tuple,tuple>
			auto &map_item_pair_item =  map_item_pair.second;
			
			for(auto &map_item_pair_item_pair : map_item_pair_item)
			{
				if(std::get<0>(map_item_pair_item_pair.second) != 0)
				std::get<0>(map_sum[map_item_pair.first][map_item_pair_item_pair.first]) += std::get<0>(map_item_pair_item_pair.second);
			}	
		}
*/
	}
	
	
};


template<class INPUT_TYPE>
std::vector< std::map<int, std::map<std::tuple<int,int,int,int,std::string, std::string>, std::tuple<double, double, double> > > > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutSet_(200);

template<class INPUT_TYPE>
std::vector< std::vector< std::map<int, std::map<std::tuple<int,int,int,int,std::string, std::string>, std::tuple<double, double, double> > > > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gTmpOutSet_(200);


template<class INPUT_TYPE>
std::vector< std::string > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutNamePrefix_(200);

template<class INPUT_TYPE>
std::vector<std::mutex>
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutMutex_(200);


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector< std::map<int, std::map<std::tuple<int,int,int,int,std::string, std::string>, std::tuple<double, double, double> > > > &
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>::gOutSet_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector< std::vector< std::map<int, std::map<std::tuple<int,int,int,int,std::string, std::string>, std::tuple<double, double, double> > > > > &
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>::gTmpOutSet_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gTmpOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector< std::string > &
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>::gOutNamePrefix_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutNamePrefix_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector<std::mutex> &
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>::gOutMutex_
= AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutMutex_;

#endif
