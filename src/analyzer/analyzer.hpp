/**
 *  @file analyzer.hpp
 *  @brief analyzer 計算 filter 後的結果
 *  @author C-Salt Corp.
 */
#ifndef ANALYZER_HPP_
#define ANALYZER_HPP_

#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include <mutex>
#include <set>

#include <boost/type_traits.hpp>
#include <boost/type_traits/add_pointer.hpp>
#include <boost/mpl/char.hpp>
#include <boost/mpl/string.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/map.hpp>
#include <boost/mpl/push_back.hpp>
#include <boost/mpl/list.hpp>
#include <boost/mpl/if.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/for_each.hpp>



#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "../pipeline/pipeline_preparator.hpp"

#include "analyzer_utility.hpp"
#include "analyzer_policy.hpp"


/**
 * @brief Analyzer 的實作，此為範型版本，用來特化
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::vector<boost::mpl::map<boost::mpl::pair<KEY, VALUE> > >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，為constant_def 內的 AnalyzerTypes
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST, int ANALYZER_TYPE, class... ARGS>
class AnalyzerImpl
{};

/**
 * @brief Analyzer是以type list的形態出現，此class為Analyzer boost::mpl::for_each所直接執行的，可以當做一個中介產物
 * @tparam INPUT_TYPE 通常為vector<AnnoRawBed>
 */
template<class INPUT_TYPE>
class AnalyzerImplInitC
{
public:
	INPUT_TYPE in;
	
	/// @brief pipe_index_ 第N輪呼叫的次序，原始數據會被分群後的順序。目的是要在平行跑完後，最終能夠還原順序，是由外面傳進來的
    size_t pipe_index_;
    
    /// @brief eof_flag_ 是否為最後一輪（也就是最後一次呼叫），由外面傳進來 
    bool eof_flag_;	
    
    /// @brief barcode_index_ smaple在分完Barcode後，所記錄的barcode次序
    size_t barcode_index_;
    
    //keep track of information of how many times, the mapped value, a certain kind of AnalyzerType, the key value, has been involved in the current ANALYZER_TYPELIST
	/// @brief analyzer_count_type_ ，key為不同的 analyzer，value為某analyzer被run了幾次（這邊與第幾輪無關，僅僅是pipeline的順序次數）
	static std::map <int, std::map<int,int> > analyzer_count_type_; 
	static std::mutex count_mutex_;	

	/// @brief Global儲存的指標，主要是可以讓外面取得到特定物件
	static void* gPtr_;
	
	AnalyzerImplInitC(INPUT_TYPE i)
		: in (i)
	{}

	~AnalyzerImplInitC()
	{}

	AnalyzerImplInitC()
	{}

    AnalyzerImplInitC(INPUT_TYPE i, size_t pipe_index, bool eof_flag, size_t barcode_index)
        : in (i), pipe_index_ (pipe_index), eof_flag_ (eof_flag), barcode_index_ (barcode_index)
    {
		{
//		std::lock_guard <std::mutex> lg (count_mutex_);
//		analyzer_count_type_.clear();
		}
	}

	/// @tparam ANALYZER_TYPELIST type list 通常型別為 boost::mpl::map，負責記錄使用者訂定的 analyzer
	template <class ANALYZER_TYPELIST>
	void operator()(ANALYZER_TYPELIST t)
	{
		/// @breif 取得 analyzer type (enum)
		typedef typename boost::mpl::at<ANALYZER_TYPELIST, boost::mpl::int_<0> >::type AnalyzerType;
		/// @brief 建構 analyzer
		AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerType::value> Analysis(in);
//		gPtr_ = 
		int yy;
		{
		std::lock_guard <std::mutex> lg (count_mutex_);
		yy = analyzer_count_type_[pipe_index_][AnalyzerType::value];
		++analyzer_count_type_[pipe_index_][AnalyzerType::value];
//std::cerr<<"current pipe_index : analyzer_type :  yy "<<pipe_index_<<'\t'<<AnalyzerType::value<<'\t'<<yy<<'\n';
		}
Analysis(yy, pipe_index_, eof_flag_, barcode_index_);	
	}
};

/// @brief 初始化 static 物件
template<class INPUT_TYPE>
void*
AnalyzerImplInitC<INPUT_TYPE>::gPtr_ = (void*) NULL;

template<class INPUT_TYPE>
std::map <int, std::map<int,int> >
AnalyzerImplInitC<INPUT_TYPE>::analyzer_count_type_;

template<class INPUT_TYPE>
std::mutex 
AnalyzerImplInitC<INPUT_TYPE>::count_mutex_;

/**
 * @brief Analyzer 的主要介面，要將 type list 做 boost mpl for_each，依序把不同的 analyzer做完
 * @tparam INPUT_TYPE 通常為vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST type list 通常型別為 boost::mpl::map，負責記錄使用者訂定的 analyzer
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerC
{
public:
	~AnalyzerC()
	{}
	
	/**
	 * @brief 外面可以呼叫此介面，開始執行 analyzer
	 * @param pipe_index_ 第N輪呼叫的次序，原始數據會被分群後的順序。目的是要在平行跑完後，最終能夠還原順序
	 * @param eof_flag_ 是否為最後一輪（也就是最後一次呼叫）
	 * @return 回傳 AnalyzerImplInit 的static指標，主要是給予外面使用一些物件的能力
	 */
	void* run (INPUT_TYPE in, size_t pipe_index, bool eof_flag) 
	{
		boost::mpl::for_each<ANALYZER_TYPELIST> ( AnalyzerImplInitC<INPUT_TYPE>( in, pipe_index, eof_flag) );
		return AnalyzerImplInitC<INPUT_TYPE>::gPtr_;
	}

	void* run (INPUT_TYPE in, size_t pipe_index, bool eof_flag, size_t barcode_index) 
	{
		boost::filesystem::path dir("output/");
		boost::filesystem::create_directory(dir);
//		auto output_path = std::string("output/sample-") + std::to_string(barcode_index) + std::string("/");
		auto output_path = std::string("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+ PipelinePreparator<>::gBarcode_vector_[barcode_index] + std::string("/"));
		boost::filesystem::path dir_analyzer(output_path);
		boost::filesystem::create_directory(dir_analyzer); 

		boost::mpl::for_each<ANALYZER_TYPELIST> ( AnalyzerImplInitC<INPUT_TYPE>( in, pipe_index, eof_flag, barcode_index) );
		return AnalyzerImplInitC<INPUT_TYPE>::gPtr_;
	}
};

/**
 * @brief Analyzer 有許多不同的功能，此 class 是定義不同功能，所提供的不同參數。目的是讓使用者在使用參數時，要指定一個參數名，方便理解程式
 * @tparam ANALYZER_TYPE constant_def 內 AnalyzerTypes 的 type，用來特化不同的 Analyzer功能
 */

template<int ANALYZER_TYPE>
class AnalyzerParameter
{
public:
	/** 
	 * @brief 定義參數有哪些
	 */
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
};



template<class INPUT_TYPE>
class AnalysisLenDistImpl
{
public:
	typedef typename std::iterator_traits<INPUT_TYPE>::value_type::value_type READ_TYPE;
	typedef std::function<void(READ_TYPE&, const int, const int , const int , const int , const char* )> CAL_OUTPUT_TYPE;
	
	CAL_OUTPUT_TYPE CalOutput;
	AnalysisLenDistImpl( CAL_OUTPUT_TYPE cal_output)
		:CalOutput(cal_output)
	{}
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是判斷此 read 是不是要記錄
	 * @param is_filter Filter後 read(anno_rawbed)內部所記錄的 filter tag
	 * @param set_filter 使用者參數所設定的 filter tag (要filter 哪個 tag)，配合 is_filter 來決定 read 是否納入計算。-1 表示全部納入計算，0 表示去掉 tag=0，1表示去掉tag=1
	 * @return bool 回傳是否 filter掉（不納入計算）
	 */
	inline bool IsFilter(const int is_filter, const int set_filter)
	{
		if(set_filter == -1)
		{
			return false;
		}
		else
		{
			if(is_filter == set_filter)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	/**
	 * @brief Analysis實作，對 read做計算，記入進 output
	 * @param in 輸入的資料
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void Analysis(INPUT_TYPE in, const int filter, const int db_index, const int db_depth, const char* db_depth_name, const char* db_depth_name2, int sys = 2)
	{
		for(auto &anno_rawbed : *in)
		{	
			if(IsFilter(anno_rawbed.is_filtered_, filter))
				continue;
			
			for(int db_idx(0); db_idx != anno_rawbed.annotation_info_.size(); ++db_idx)
			{
				bool is_count (false);
				
				int check_db_idx = get_key(db_index, db_idx);
				if(check_db_idx == -3)
					continue;
				
				for(int db_dep(0); db_dep != anno_rawbed.annotation_info_[db_idx].size(); ++db_dep)
				{
					int check_db_depth = get_key(db_depth, db_dep);
					if(check_db_depth == -3)
						continue;
					
					//設定的 db_depth_name 文字轉 string
					std::string db_depth_name_str (db_depth_name);
					//取得annotation 內容
					std::string &db_depth_value = anno_rawbed.annotation_info_[db_idx][db_dep];
					//轉換 annotation 內容，db_depth_name_str 為 -1(全部分開), -2(全部合併), -3(不做), other(指定)
					std::string check_depth_name = get_key(db_depth_name_str, db_depth_value);
					if(check_depth_name == "-3")
						continue;
					
					//設定的 db_depth2_name 文字轉 string
					std::string db_depth2_name_str (db_depth_name2);
					
					// for miRNA，如果db_depth2_name_str != -3 ，改變 check_depth_name，也就是改變記錄的文字，主要是可以讓第二層文字取代
					// e.g., 上面先判斷是不是 miRNA，然後如果 db_depth2_name_str = -1，那就會把 miRNA 的所有細分名字做 length distribution 
					if(db_depth2_name_str != "-3" )
					{
						std::string &db_depth2_value = anno_rawbed.annotation_info_[db_idx][1];
						std::string check_depth2_name = get_key(db_depth2_name_str, db_depth2_value);
						check_depth_name = check_depth2_name;
					}	
					
					CalOutput (anno_rawbed, sys, filter, check_db_idx, check_db_depth, check_depth_name.c_str());		
					is_count = true;
				}
				
				//filter = 0，沒有任何 annotation 所以 size = 0，但 filter = 0 還是要加總 
				//annotation 都會有 db層，而不一定有 dep層
				if(is_count == false && filter != 1)
				{
					CalOutput (anno_rawbed, sys, filter, check_db_idx, 0, "");
				}
				
			}
		}
	}
	
	std::string get_key(const std::string &type, const std::string &value)
	{
		if(type == "-1")
		{
			return value;
		}
		else if(type == "-2")
		{
			return "";
		}
		else
		{
			if(type == value)
			{
				return value;
			}
			else
			{
				return "-3";
			}
		}
		
	}
	int get_key(const int &type, const int &value)
	{
		if(type == -1)
		{
			return value;
		}
		else if(type == -2)
		{
			return -2;
		}
		else
		{
			if(type == value)
			{
				return value;
			}
			else
			{
				return -3;
			}
		}
	}
	
};




#include "analyzer_length_distribution.hpp"
#include "analyzer_heterogeneity.hpp"
#include "analyzer_printer.hpp"
#include "analyzer_tobam.hpp"
#include "analyzer_tobwg.hpp"
#include "analyzer_printer_mappability.hpp"
#include "analyzer_printer_heterogeneity.hpp"
#include "analyzer_printer_dual_analyzer.hpp"
#include "analyzer_sequence_logo.hpp"
#include "analyzer_printer_sequence_logo.hpp"

#endif
