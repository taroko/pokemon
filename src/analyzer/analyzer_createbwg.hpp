/**
 *  @file createbwg.hpp
 *  @brief used to do AnnotationRawBed to Bwg conversion by calling function of kent tool.
 *  @author C-Salt Corp.
 */
#ifndef CREATEBWG_HPP_
#define CREATEBWG_HPP_
//#include "../file_writer/file_writter.hpp"
//#include "../file_writer/basespace_setting.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"
#include "../../include/fmemopen/fmemopen.h"
#include "../../include/fmemopen/open_memstream.h"

template <typename OUTPUTHANDLE>
class CreateBwg
{
public:
	int blockSize;
	int itemsPerSlot;
	boolean clipDontDie;
	boolean doCompress;
	std::map < std::string, bwgSection* >* ptr_;

	CreateBwg (std::map < std::string, bwgSection* >* ptr)
		: blockSize (256)
		, itemsPerSlot (1024)
		, clipDontDie (false)
		, doCompress (false)
		, ptr_ (ptr)
	{}

    void mkdir(std::string& sample_path)                                                                                                  
    {  
        boost::filesystem::path dir_output("output");
        boost::filesystem::create_directory(dir_output);
        boost::filesystem::path dir_analyzer(sample_path);
        boost::filesystem::create_directory(dir_analyzer);
    }

    void bigWigFileCreate(char *chromSizes, size_t barcode_index)
	{
		std::vector< std::shared_ptr<OUTPUTHANDLE> > vec;//IoHandlerBaseSpace> > vec;
		for ( auto & item : *ptr_)//gbwgSection_)
		{
			struct hash *chromSizeHash = bbiChromSizesFromFile(chromSizes);
			struct lm *lm = lmInit(0);
			struct bwgSection *sectionList = item.second;//bwgParseWig(inName, clipDontDie, chromSizeHash, itemsPerSlot, lm);
			if (sectionList == NULL)
				errAbort(" is empty of data");

			std::string output_path ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index]+"/bwg/");
			mkdir (output_path);

			std::stringstream ss2, ss;
			ss2 << output_path << item.first;
			ss << item.first;
			std::cerr<<"create_bwg: "<<ss.str().c_str();
//			FILE *ff = fopen( ss2.str().c_str(), "wb");//( (char*) (item.first+".bw").c_str(), "wb");
			char *buf;
			size_t len = 0;
			FILE *f = open_memstream(&buf, &len);
			wig2BigWig(sectionList, chromSizeHash, f);//, blockSize, itemsPerSlot, compress, f);
//			fwrite(buf, 1, len, ff);
//			fclose(ff);

			DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
			file_writter_parameter.set_filename (item.first , output_path);

			std::shared_ptr <OUTPUTHANDLE> fw = std::make_shared <OUTPUTHANDLE> (file_writter_parameter);
			vec.push_back (fw);
			fw->write (buf, len);
			fw->bs_async_close ();

//			printf("buf: %s\n", buf);
//			printf("len: %d\n", len);
			lmCleanup(&lm);
//std::cerr<<"done file create "<<item.first<<'\n';
		}
		for (auto& q : vec)
			q->close();
	}

    void bigWigFileCreate(char *chromSizes, std::string& bwg_path)
	{
//std::cerr<<"bwg creating"<<'\n';
		for ( auto & item : *ptr_)//gbwgSection_)
		{
//std::cerr<<"file : "<<item.first<<'\n';
			struct hash *chromSizeHash = bbiChromSizesFromFile(chromSizes);
			struct lm *lm = lmInit(0);
			struct bwgSection *sectionList = item.second;//bwgParseWig(inName, clipDontDie, chromSizeHash, itemsPerSlot, lm);
			if (sectionList == NULL)
				errAbort(" is empty of data");
//			size_t ii=0;
//			for (; (sectionList+ii)->next != NULL; ++ii)
//				Print (sectionList+ii);
//			Print (sectionList+ii);

			std::stringstream ss;
			ss << bwg_path << item.first << ".bwg";
//			std::cerr<<"create_bwg: "<<ss.str().c_str();
			FILE *ff = fopen( ss.str().c_str(), "wb");//( (char*) (item.first+".bw").c_str(), "wb");
			char *buf;
			size_t len = 0;
			FILE *f = open_memstream(&buf, &len);
			wig2BigWig(sectionList, chromSizeHash, f);//, blockSize, itemsPerSlot, compress, f);
			fwrite(buf, 1, len, ff);
			fclose(ff);
//			printf("buf: %s\n", buf);
//			printf("len: %d\n", len);
			lmCleanup(&lm);
//std::cerr<<"done file create "<<item.first<<'\n';
		}
	}


    void Print (bwgSection* sl)
    {  
        std::cerr<<"bwgsection sl "<<sl<<'\n';
        std::cerr<<"chr : start : end "<<sl->chrom<<" : "<<sl->start<<" : "<<sl->end<<'\n';
        std::cerr<<"step : span : count "<<sl->itemStep<<" : "<<sl->itemSpan<<" : "<<sl->itemCount<<'\n';
        std::cerr<<"id : offset "<<sl->chromId<<" : "<<sl->fileOffset<<'\n';
        std::cerr<<"current & next ptr "<<sl<<" : "<<sl->next<<'\n';

		size_t dd=0, count=0;
		for (auto idx=0; idx!=sl->itemCount; ++idx)
		{
            std::cerr<<"items "<<(sl->items.variableStepPacked)+idx<<'\n';
			std::cerr<<"# "<<((sl->items.variableStepPacked)+idx)->start
					 <<" : "<<((sl->items.variableStepPacked)+idx)->val<<"  ";
//			if ((sl->items.fixedStepPacked+idx)->val !=0.0)
//			{
//				std::cerr<<"# "<<dd<<":"<<((sl->items.fixedStepPacked)+idx)->val<<"  ";
//				++count;
//			}
//			++dd;
		}
		std::cerr<<'\n';
		std::cerr<<"dd : count "<<dd<<'\t'<<count<<'\n';
    }

	/* Create a bigWig file out of a sorted sectionList. */
	void wig2BigWig(struct bwgSection *sectionList, struct hash *chromSizeHash, //int blockSize, int itemsPerSlot, boolean doCompress, 
		FILE *f)
	{
//std::cerr<<"wig2bigwig: a"<<'\t';
		bits64 sectionCount = slCount(sectionList);
		//FILE *f = mustOpen(fileName, "wb");
		bits32 sig = bigWigSig;
		bits16 version = bbiCurrentVersion;
		bits16 summaryCount = 0;
		bits16 reserved16 = 0;
		bits32 reserved32 = 0;
		bits64 reserved64 = 0;
		bits64 dataOffset = 0, dataOffsetPos;
		bits64 indexOffset = 0, indexOffsetPos;
		bits64 chromTreeOffset = 0, chromTreeOffsetPos;
		bits64 totalSummaryOffset = 0, totalSummaryOffsetPos;
		bits32 uncompressBufSize = 0;
		bits64 uncompressBufSizePos;
		struct bbiSummary *reduceSummaries[10];
		bits32 reductionAmounts[10];
		bits64 reductionDataOffsetPos[10];
		bits64 reductionDataOffsets[10];
		bits64 reductionIndexOffsets[10];
		size_t file_size = 0;
		int i;
		
//std::cerr<<"wig2bigwig: b"<<'\t';
		/* Figure out chromosome ID's. */
		struct bbiChromInfo *chromInfoArray;
		int chromCount, maxChromNameSize;
		bwgMakeChromInfo(sectionList, chromSizeHash, &chromCount, &chromInfoArray, &maxChromNameSize);
		
		/* Figure out initial summary level - starting with a summary 10 times the amount
		 * of the smallest item.  See if summarized data is smaller than half input data, if
		 * not bump up reduction by a factor of 2 until it is, or until further summarying
		 * yeilds no size reduction. */
		int  minRes = bwgAverageResolution(sectionList);//240
		int initialReduction = minRes*10;
		
		//儲存在硬碟時，所有 section size
		bits64 fullSize = bwgTotalSectionSize(sectionList);
		
//		printf("bwgTotalSectionSize: %d\nminRes: %d\n", fullSize, minRes);
		
		bits64 maxReducedSize = fullSize/2;
		
		struct bbiSummary *firstSummaryList = NULL, *summaryList = NULL;
		bits64 lastSummarySize = 0, summarySize;
		for (;;)
		{
			summaryList = bwgReduceSectionList(sectionList, chromInfoArray, initialReduction);
			bits64 summarySize = bbiTotalSummarySize(summaryList);
			if (doCompress)
			{
				 summarySize *= 2;	// Compensate for summary not compressing as well as primary data
			}
			if (summarySize >= maxReducedSize && summarySize != lastSummarySize)
			{
				/* Need to do more reduction.	 First scale reduction by amount that it missed
				 * being small enough last time, with an extra 10% for good measure.	Then
				 * just to keep from spinning through loop two many times, make sure this is
				 * at least 2x the previous reduction. */
				int nextReduction = 1.1 * initialReduction * summarySize / maxReducedSize;
				if (nextReduction < initialReduction*2)
					  nextReduction = initialReduction*2;
				initialReduction = nextReduction;
				bbiSummaryFreeList(&summaryList);
				lastSummarySize = summarySize;
			}
			else
				break;
		}
				
		summaryCount = 1;
		reduceSummaries[0] = firstSummaryList = summaryList;
		reductionAmounts[0] = initialReduction;
		
//std::cerr<<"wig2bigwig: c"<<'\t';
		/* Now calculate up to 10 levels of further summary. */
		bits64 reduction = initialReduction;
		for (i=0; i<ArraySize(reduceSummaries)-1; i++)
		{
			 reduction *= 4;
			 if (reduction > 1000000000)
				 break;
			summaryList = bbiReduceSummaryList(reduceSummaries[summaryCount-1], chromInfoArray, reduction);
			summarySize = bbiTotalSummarySize(summaryList);
			if (summarySize != lastSummarySize)
			{
			 	reduceSummaries[summaryCount] = summaryList;
				reductionAmounts[summaryCount] = reduction;
				++summaryCount;
			}
			int summaryItemCount = slCount(summaryList);
			if (summaryItemCount <= chromCount)
				 break;
		 }
//		printf("summaryCount: %d\n", summaryCount);
		/* Write fixed header. */
		writeOne(f, sig);
		writeOne(f, version);
		writeOne(f, summaryCount);
		chromTreeOffsetPos = ftell(f);
		writeOne(f, chromTreeOffset);
		dataOffsetPos = ftell(f);
		writeOne(f, dataOffset);
		indexOffsetPos = ftell(f);
		writeOne(f, indexOffset);
		writeOne(f, reserved16);  /* fieldCount */
		writeOne(f, reserved16);  /* definedFieldCount */
		writeOne(f, reserved64);  /* autoSqlOffset. */
		totalSummaryOffsetPos = ftell(f);
		writeOne(f, totalSummaryOffset);
		uncompressBufSizePos = ftell(f);
		writeOne(f, uncompressBufSize);
		writeOne(f, reserved64);  /* nameIndexOffset */
		assert(ftell(f) == 64);
		
		/* Write summary headers */
		for (i=0; i<summaryCount; ++i)
		 {
			 writeOne(f, reductionAmounts[i]);
			 writeOne(f, reserved32);
			 reductionDataOffsetPos[i] = ftell(f);
			 writeOne(f, reserved64);	// Fill in with data offset later
			 writeOne(f, reserved64);	// Fill in with index offset later
			 //printf("reductionAmounts: %d" );
		 }
		
//std::cerr<<"wig2bigwig: d"<<'\t';
		/* Write dummy summary */
		struct bbiSummaryElement totalSum;
		ZeroVar(&totalSum);
		totalSummaryOffset = ftell(f);
		bbiSummaryElementWrite(f, &totalSum);
		
		/* Write chromosome bPlusTree */
		chromTreeOffset = ftell(f);
		int chromBlockSize = min(blockSize, chromCount);
		bptFileBulkIndexToOpenFile(chromInfoArray, sizeof(chromInfoArray[0]), chromCount, chromBlockSize,
			 bbiChromInfoKey, maxChromNameSize, bbiChromInfoVal, 
			 sizeof(chromInfoArray[0].id) + sizeof(chromInfoArray[0].size), 
			 f);
		
		/* Write out data section count and sections themselves. */
		dataOffset = ftell(f);
		//bits64 sectionCount2 = sectionCount*2;
		writeOne(f, sectionCount);
//		printf("sectionCount: %d\n", sectionCount);
		
		struct bwgSection *section;
		for (section = sectionList; section != NULL; section = section->next)
			 {
//			 printf("section->chromId: %d\n fileOffset: %d\n start: %d\n", section->chromId, section->fileOffset, section->start);
			 bits32 uncSizeOne = bwgSectionWrite(section, doCompress, f);
			 if (uncSizeOne > uncompressBufSize)
				  uncompressBufSize = uncSizeOne;
			 }
		/* Write out index - creating a temporary array rather than list representation of
		 * sections in the process. */
		indexOffset = ftell(f);
		struct bwgSection **sectionArray;
		
		
		
		//AllocArray(sectionArray, sectionCount);
		(sectionArray = (struct bwgSection **)needLargeZeroedMem(sizeof(*sectionArray) * (sectionCount)));
		
		
		for (section = sectionList, i=0; section != NULL; section = section->next, ++i)
			 sectionArray[i] = section;
		cirTreeFileBulkIndexToOpenFile(sectionArray, sizeof(sectionArray[0]), sectionCount,
			 blockSize, 1, NULL, bwgSectionFetchKey, bwgSectionFetchOffset, 
			 indexOffset, f);
		freez(&sectionArray);
		
//std::cerr<<"wig2bigwig: e"<<'\t';
		/* Write out summary sections. */
		verbose(2, "bwgCreate writing %d summaries\n", summaryCount);
		for (i=0; i<summaryCount; ++i)
			 {
			 reductionDataOffsets[i] = ftell(f);
			 reductionIndexOffsets[i] = bbiWriteSummaryAndIndex(reduceSummaries[i], blockSize, itemsPerSlot, doCompress, f);
			 verbose(3, "wrote %d of data, %d of index on level %d\n", (int)(reductionIndexOffsets[i] - reductionDataOffsets[i]), (int)(ftell(f) - reductionIndexOffsets[i]), i);
			 }
			 
		file_size = ftell(f);
			 
		//std::cout << "f1 " << ftell(f) << "index " << std::endl;
		/* Calculate summary */
		struct bbiSummary *sum = firstSummaryList;
		if (sum != NULL)
			 {
			 totalSum.validCount = sum->validCount;
			 totalSum.minVal = sum->minVal;
			 totalSum.maxVal = sum->maxVal;
			 totalSum.sumData = sum->sumData;
			 totalSum.sumSquares = sum->sumSquares;
			 for (sum = sum->next; sum != NULL; sum = sum->next)
			{
			totalSum.validCount += sum->validCount;
			if (sum->minVal < totalSum.minVal) totalSum.minVal = sum->minVal;
			if (sum->maxVal > totalSum.maxVal) totalSum.maxVal = sum->maxVal;
			totalSum.sumData += sum->sumData;
			totalSum.sumSquares += sum->sumSquares;
			}
			 /* Write real summary */
			 fseek(f, totalSummaryOffset, SEEK_SET);
			 bbiSummaryElementWrite(f, &totalSum);
			 }
		else
			 totalSummaryOffset = 0;	/* Edge case, no summary. */
		
		/* Go back and fill in offsets properly in header. */
		fseek(f, dataOffsetPos, SEEK_SET);
		writeOne(f, dataOffset);
		fseek(f, indexOffsetPos, SEEK_SET);
		writeOne(f, indexOffset);
		fseek(f, chromTreeOffsetPos, SEEK_SET);
		writeOne(f, chromTreeOffset);
		fseek(f, totalSummaryOffsetPos, SEEK_SET);
		writeOne(f, totalSummaryOffset);
		
		if (doCompress)
			 {
			 int maxZoomUncompSize = itemsPerSlot * sizeof(struct bbiSummaryOnDisk);
			 if (maxZoomUncompSize > uncompressBufSize)
			uncompressBufSize = maxZoomUncompSize;
			 fseek(f, uncompressBufSizePos, SEEK_SET);
			 writeOne(f, uncompressBufSize);
			 }
		
//std::cerr<<"wig2bigwig: f"<<'\t';
		/* Also fill in offsets in zoom headers. */
		for (i=0; i<summaryCount; ++i)
			 {
			 fseek(f, reductionDataOffsetPos[i], SEEK_SET);
			 writeOne(f, reductionDataOffsets[i]);
			 writeOne(f, reductionIndexOffsets[i]);
			 }
		
		/* Write end signature. */
		//std::cout << "f1 " << ftell(f) << std::endl;
		//fseek(f, 0L, SEEK_END);
		fseek(f, file_size, SEEK_SET);
		//std::cout << "f2 " << ftell(f) << std::endl;
		
		writeOne(f, sig);
		//std::cout << "f3 " << ftell(f) << std::endl;
		
		/* Clean up */
		freez(&chromInfoArray);
		carefulClose(&f);
//std::cerr<<"wig2bigwig: g"<<'\t';
	}

	int bwgSectionWrite(struct bwgSection *section, boolean doCompress, FILE *f)
		/* Write out section to file, filling in section->fileOffset. */
	{
		UBYTE type = section->type;
		UBYTE reserved8 = 0;
		int itemSize;
		switch (section->type)
		{
			case bwgTypeBedGraph:
				itemSize = 12;
				break;
			case bwgTypeVariableStep:
				itemSize = 8;
				break;
			case bwgTypeFixedStep:
				itemSize = 4;
				break;
			default:
				itemSize = 0;  // Suppress compiler warning
				internalErr();
				break;
		}
		int fixedSize = sizeof(section->chromId) + sizeof(section->start) + sizeof(section->end) + 
			sizeof(section->itemStep) + sizeof(section->itemSpan) + sizeof(type) + sizeof(reserved8) +
			sizeof(section->itemCount);
		int bufSize = section->itemCount * itemSize + fixedSize;
		char buf[bufSize];
		char *bufPt = buf;
	
		section->fileOffset = ftell(f);
		memWriteOne(&bufPt, section->chromId);
		memWriteOne(&bufPt, section->start);
		memWriteOne(&bufPt, section->end);
		memWriteOne(&bufPt, section->itemStep);
		memWriteOne(&bufPt, section->itemSpan);
		memWriteOne(&bufPt, type);
		memWriteOne(&bufPt, reserved8);
		memWriteOne(&bufPt, section->itemCount);
	
		int i;
		switch (section->type)
		{
			case bwgTypeBedGraph:
				{
					struct bwgBedGraphItem *item = section->items.bedGraphList;
					for (item = section->items.bedGraphList; item != NULL; item = item->next)
					{
						memWriteOne(&bufPt, item->start);
						memWriteOne(&bufPt, item->end);
						memWriteOne(&bufPt, item->val);
					}
					break;
				}
			case bwgTypeVariableStep:
				{
					struct bwgVariableStepPacked *items = section->items.variableStepPacked;
					for (i=0; i<section->itemCount; ++i)
					{
						memWriteOne(&bufPt, items->start);
						memWriteOne(&bufPt, items->val);
						items += 1;
					}
					break;
				}
			case bwgTypeFixedStep:
				{
					struct bwgFixedStepPacked *items = section->items.fixedStepPacked;
					for (i=0; i<section->itemCount; ++i)
					{
						memWriteOne(&bufPt, items->val);
						items += 1;
					}
					break;
				}
			default:
				internalErr();
				break;
		}
		assert(bufSize == (bufPt - buf) );
	
		if (doCompress)
		{
			size_t maxCompSize = zCompBufSize(bufSize);
			char compBuf[maxCompSize];
			int compSize = zCompress(buf, bufSize, compBuf, maxCompSize);
			mustWrite(f, compBuf, compSize);
		}
		else
			mustWrite(f, buf, bufSize);
		return bufSize;
	}
	
	int bwgSectionSize(struct bwgSection *section)
		/* Return size (on disk) of section. */
	{
		return bwgSectionHeaderSize + bwgItemSize(section->type) * section->itemCount;
	}
	
	bits64 bwgTotalSectionSize(struct bwgSection *sectionList)
		/* Return total size of all sections. */
	{
		bits64 total = 0;
		struct bwgSection *section;
		for (section = sectionList; section != NULL; section = section->next)
			total += bwgSectionSize(section);
		return total;
	}

	int bwgItemSize(enum bwgSectionType type)
		/* Return size of an item inside a particular section. */
	{
		switch (type)
		{
			case bwgTypeBedGraph:
				return 2*sizeof(bits32) + sizeof(float);
				break;
			case bwgTypeVariableStep:
				return sizeof(bits32) + sizeof(float);
				break;
			case bwgTypeFixedStep:
				return sizeof(float);
				break;
			default:
				internalErr();
				return 0;
		}
	}
	
	static struct cirTreeRange bwgSectionFetchKey(const void *va, void *context)
		/* Fetch bwgSection key for r-tree */
	{
		struct cirTreeRange res;
		const struct bwgSection *a = *((struct bwgSection **)va);
		res.chromIx = a->chromId;
		res.start = a->start;
		res.end = a->end;
		return res;
	}

	static bits64 bwgSectionFetchOffset(const void *va, void *context)
	/* Fetch bwgSection file offset for r-tree */
	{
		const struct bwgSection *a = *((struct bwgSection **)va);
		return a->fileOffset;
	}

	void bwgMakeChromInfo(struct bwgSection *sectionList, struct hash *chromSizeHash,
		int *retChromCount, struct bbiChromInfo **retChromArray,
		int *retMaxChromNameSize)
	/* Fill in chromId field in sectionList.  Return array of chromosome name/ids. 
	 * The chromSizeHash is keyed by name, and has int values. */
	{
		/* Build up list of unique chromosome names. */
		struct bwgSection *section;
		char *chromName = "";
		int chromCount = 0;
		int maxChromNameSize = 0;
		struct slRef *uniq, *uniqList = NULL;
		for (section = sectionList; section != NULL; section = section->next)
		{
			if (!sameString(section->chrom, chromName))
			{
				chromName = section->chrom;
				refAdd(&uniqList, chromName);
				++chromCount;
				int len = strlen(chromName);
				if (len > maxChromNameSize)
					maxChromNameSize = len;
			}
			section->chromId = chromCount-1;
		}
		slReverse(&uniqList);
	
		/* Allocate and fill in results array. */
		struct bbiChromInfo *chromArray;
		//AllocArray(chromArray, chromCount);
		(chromArray = (struct bbiChromInfo *)needLargeZeroedMem(sizeof(*chromArray) * (chromCount)));
		
		int i;
		for (i = 0, uniq = uniqList; i < chromCount; ++i, uniq = uniq->next)
		{
			chromArray[i].name = (char*)uniq->val;
			chromArray[i].id = i;
			chromArray[i].size = hashIntVal(chromSizeHash, (char*)uniq->val);
		}
	
		/* Clean up, set return values and go home. */
		slFreeList(&uniqList);
		*retChromCount = chromCount;
		*retChromArray = chromArray;
		*retMaxChromNameSize = maxChromNameSize;
	}
};


#endif
