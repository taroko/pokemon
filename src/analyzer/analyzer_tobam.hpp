/**
 *  @file analyzer_tobam.hpp
 *  @brief achieve tobam conversion for the analyzer 
 *  @author C-Salt Corp.
 */
#ifndef ANALYZER_TOBAM_HPP_
#define ANALYZER_TOBAM_HPP_

#include "../tuple_utility.hpp"
#include "../constant_def.hpp"
#include "../converter/sam2bam_up.hpp"
#include "analyzer_tobai.hpp"
#include "boost/filesystem.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"

/**
 * @brief Analyzer 此class為提供轉換bam檔的Analyzer參數
 * @tparam ANALYZER_TYPE 特化為 ToBam
 */
template<>
class AnalyzerParameter<AnalyzerTypes::ToBam>
{
public:
	/** 
	 * @brief 定義ToBam參數有哪些
	 */
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<1> FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定db。
	typedef boost::mpl::int_<2> DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef boost::mpl::int_<3> DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef boost::mpl::int_<4> DbDepthNameType;

	typedef boost::mpl::int_<5> IoHandlerType;

	typedef boost::mpl::int_<6> IoHandlerTypeForBAI;
};


/**
 * @brief Analyzer 的實作，此為產生bam檔的特化版本。
 * @tparam INPUT_TYPE 輸入資料的型別，一定為 vector，通常為 vector<AnnoRawBed>
 * @tparam ANALYZER_TYPELIST Analyzer要用的參數設定，通常為 boost::mpl::vector<boost::mpl::map<boost::mpl::pair<KEY, VALUE> > >
 * @tparam ANALYZER_TYPE 特化 Analyzer用的參數，此為ToBam 特化
 */
template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>
	:public AnalysisLenDistImpl<INPUT_TYPE>
{
public:
	typedef typename std::iterator_traits<INPUT_TYPE>::value_type::value_type READ_TYPE;
	
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaPara;
	
	/// @brief FilterType 決定Filter後，此analyzer 要取那個Tag，-1=>全，1=>去掉filter tag=1，0=>卻掉filter tag=0。每一個AnalyzerParameter都應該要有此參數
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::FilterType, boost::mpl::int_<1> >::type FilterType;
	
	/// @brief DbIndexType 決定annotation 的 db。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定db。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbIndexType, boost::mpl::int_<-2> >::type DbIndexType;
	
	/// @brief DbDepthType 決定 第N個 annotation。可以為空，代表不做，此轉為 -2。可以為 -1，代表全做。可以為數字，指定第N個 annotation。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthType, boost::mpl::int_<-2> >::type DbDepthType;
	
	/// @brief DbDepthNameType 決定annotation name。可以為空，此轉為 "-1"，代表全部名字。可以為字串(boost::mpl::string)，指定 annotation name為何。
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::DbDepthNameType, boost::mpl::string<'-1'> >::type DbDepthNameType;

	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType; 

	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerTypeForBAI, IoHandlerOfstream >::type IoHandlerTypeForBAI; 

	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	/** 
	 * @brief 輸出的資料型別 \n output map <file_name, bam_seq>
	 */
	typedef std::map<std::string, std::shared_ptr < Sam2BamUp<> > > OutPutType;
	
	/// @brief 輸出的資料
	OutPutType out_set_;
	
	/// @brief a buffer to hold the achieved pipeline result, as a pipeline index to its corresponding map of file_name and shared_ptr <stringstream>.  The result will be temporary buffered in gOutSet_ till the results corresponding to all of the precending pipeline indexes have been achieved.  Then the result will be merged into gBuf_ object.
	static std::map< int, std::map <std::string, std::shared_ptr <std::stringstream> > > gOutSet_;
	
	/// @brief a global mutex object for establishing lock_guard object for merging results coming from each of the posted tobam pipelines
	static std::mutex gOutMutex_;

	/// @brief a sequential index, keeping record till which pipe line index that the results have been achieved and been merged into gBuf_ object.  The design is a must be since the merging operation of each tobam pipelines must be achieved in the order of the pipeline index.
	static int gCurrentPipelineIndex_;
	
	/// @brief the object to hold the merged result.  The upload operation or file writing operation will be conducted based on the merged result stored in gBuf_ element
	//static std::map <std::string, std::shared_ptr <std::stringstream> > gBuf_;
	static std::map <std::string, std::shared_ptr < IoHandlerType > > gBuf_;

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)//, this_analyzer_count_(0)
 		, file_writter_parameter_ (PipelinePreparator<>::gDeviceParameter_)
 		, AnalysisLenDistImpl<INPUT_TYPE>(
			std::bind(
				&AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::CalOutput
				, this
				, std::placeholders::_1
				, std::placeholders::_2
				, std::placeholders::_3
				, std::placeholders::_4
				, std::placeholders::_5
				, std::placeholders::_6
			)
		)
	{}

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl()
	{}

	/// @brief a cluster of flags indicating which pipeline has been achieved and bufferred into the static gBuf_ object
	static std::vector <int> flag_;

	// @brief recording map [file_name, tuple < pair<start_block_position, current block_length>, map[pair<chromosome, 16K_tile_index>, tuple< min_uoffset, max_uoffset, read's_start, read's_end, min_coffset, max_coffset>] >]
	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int, int, int> > > > BamIndexType;

	/// @brief structure keeping the written bam file length, i.e. the coffset value, and multiple pieces of information having the # of 16kb tile and its corresponding offset value, i.e. uoffset value. 
	BamIndexType out_index_;

	static std::map < int, BamIndexType > gIndex_;

	DeviceParameter file_writter_parameter_;

	// map [pipe_index, map[file_name, map [chromosome_str, read_count] ] ]
	static std::map< int, std::map <std::string, std::map <std::string, uint32_t> > > gReadCount_;

	// map[file_name, map [chromosome_str, read_count] ]
	std::map <std::string, std::map<std::string, uint32_t> > ReadCount_;


    void mkdir(std::string& sample_path)
    {  
        boost::filesystem::path dir_output("output");
        boost::filesystem::create_directory(dir_output);
        boost::filesystem::path dir_analyzer(sample_path);
        boost::filesystem::create_directory(dir_analyzer);
    }

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief 主要執行Analyzer的對外介面
	 * @param this_analyzer_count 第N個 使用者要求的length distribution，因為輸出資料節過為 vector<map<...>>
	 * @return void
	 */
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, size_t barcode_index)
	{
	while (true)
	{
		int diff = 0;
		{
			std::lock_guard<std::mutex> lock(gOutMutex_);
			diff = pipe_index - gCurrentPipelineIndex_;
		}
		std::chrono::milliseconds dura( 10 );

		if (diff  > 20)
    		std::this_thread::sleep_for( dura );
		else
			break;
	}

		if(this_analyzer_count == 0)
		{
			/// @brief input, filter = each, db_idx = -1, db_depth=0, db_depth_value, sys
//			this->Analysis (in, -1, -1, 0, "-2", "-3", 0);
//			this->Analysis (in, 1, -1, 0, "-2", "-3", 0);
//			this->Analysis (in, 0, -1, 0, "-2", "-3", 0);
//			write bam files only with 1st database annotation, i.e. ensumbl biotype
			this->Analysis (in, -1, 0, 0, "-2", "-3", 0);
			this->Analysis (in, 1, 0, 0, "-2", "-3", 0);
			this->Analysis (in, 0, 0, 0, "-2", "-3", 0);
		}
		this->Analysis (in, FilterType::value, DbIndexType::value, DbDepthType::value, boost::mpl::c_str<DbDepthNameType>::value, "-3"); 
		MergeToBuf (pipe_index, barcode_index);
		if (eof_flag)
		{
/*
			for (auto& item0 : gReadCount_)
			{
				std::cerr<<"current pipe_index "<<item0.first<<'\n';
				for (auto& item1: item0.second)
				{
					std::cerr<<"current file: "<<item1.first<<'\n';
					for (auto& item2: item1.second)
						std::cerr<<"current chromsome "<<item2.first<<'\t'<<"read_count "<<item2.second<<'\n';
				}
			}
*/
			ReadCount_.clear();
			for (auto& item0 : gReadCount_)
				for (auto& item1: item0.second)
					for (auto& item2: item1.second)
						ReadCount_[item1.first][item2.first]+=item2.second;

			TerminateBamFile ();
			//PrintgIndex("pre_update_index_info");
			UpdateIndex ();
			//PrintgIndex("post_update_index_info");

			cross_board_bin_.clear();
			for (auto& crossmap: gcross_board_bin_)
				for (auto& file_chr_bin_pair: crossmap.second)
					for (auto& item: file_chr_bin_pair.second)
						cross_board_bin_[file_chr_bin_pair.first][item.first] = item.second;

			ToBai <IoHandlerTypeForBAI> (&gIndex_, barcode_index, ReadCount_, cross_board_bin_);
			ClearContent ();
		}
		return (void*) in;
	}

	void PrintgIndex (const std::string& filename)
	{
/*
		std::cerr<<"======tobam: PrintIndex========="<<'\n';
		for (auto& item0 : gIndex_)
		{
			std::cerr<<"pipe_index: "<<item0.first<<'\n';
			for (auto& item1 : item0.second)
			{
				std::cerr<<"file_name : hdr_block_length_ : ofss_length_ "<<item1.first<<'\t'<<std::get<0>(item1.second).first<<'\t'<<std::get<0>(item1.second).second<<'\n';
				for (auto& item2 : std::get<1>(item1.second) )
				{
					std::cerr<<"chromsome : tile "<<item2.first.first<<'\t'<<item2.first.second<<'\n';
					std::cerr<<"minuoffset : maxuoffset : start : end "<<std::get<0>(item2.second)<<'\t'<<std::get<1>(item2.second)<<'\t'<<std::get<2>(item2.second)<<'\t'<<std::get<3>(item2.second)<<'\n';
				}
			}
		}
*/
		std::ofstream qq (filename);
		qq<<"======tobam: PrintIndex========="<<'\n';
		for (auto& item0 : gIndex_)
		{
			qq<<"pipe_index: "<<item0.first<<'\n';
			for (auto& item1 : item0.second)
			{
				qq<<"file_name : hdr_block_length_ : ofss_length_ "<<item1.first<<'\t'<<std::get<0>(item1.second).first<<'\t'<<std::get<0>(item1.second).second<<'\n';
				for (auto& item2 : std::get<1>(item1.second) )
				{
					qq<<"chromsome : tile "<<item2.first.first<<'\t'<<item2.first.second<<'\n';
					qq<<"minuoffset : maxuoffset : start : end "<<std::get<0>(item2.second)<<'\t'<<std::get<1>(item2.second)<<'\t'<<std::get<2>(item2.second)<<'\t'<<std::get<3>(item2.second)<<'\n';
				}
			}
		}
	}

private:
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to clear all static members
	 * @return void
	 */
	void ClearContent (void)
	{
		gOutSet_.clear();
		free (gOutSet_);
		gCurrentPipelineIndex_=0;
		flag_.clear();
		if (boost::is_same<IoHandlerType, std::stringstream >::value)
			;
		else
			for (auto& item : gBuf_)
				item.second->close();
		gBuf_.clear();
		free (gBuf_);
		gIndex_.clear();
		free (gIndex_);
		gReadCount_.clear();
		free (gReadCount_);
		gcross_board_bin_.clear();
		free (gcross_board_bin_);
	}

	template <typename T>
	void free (T& qq)
	{
		T aa;
		aa.swap (qq);
	};

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief have the current obtained bam contents, recorded into out_set_, copied into gOutSet_ and, if possible, merged into gBuf_
	 * @return void
	 */

	void PrintOutIndex (void)
	{
		std::cerr<<"======tobam: PrintOutIndex========="<<'\n';
		for (auto& item1 : out_index_)
		{
			std::cerr<<"file_name : hdr_block_length_ : ofss_length_ "<<item1.first<<'\t'<<std::get<0>(item1.second).first<<'\t'<<std::get<0>(item1.second).second<<'\n';
			for (auto& item2 : std::get<1>(item1.second) )
			{
				std::cerr<<"chromsome : tile "<<item2.first.first<<'\t'<<item2.first.second<<'\n';
				std::cerr<<"minuoffset : maxuoffset : start : end "<<std::get<0>(item2.second)<<'\t'<<std::get<1>(item2.second)<<'\t'<<std::get<2>(item2.second)<<'\t'<<std::get<3>(item2.second)<<'\n';
			}
		}
	}

typedef std::map< std::pair<std::string,std::string>, std::map <int, int> > CrossMapType;
CrossMapType cross_board_bin_;
static std::map <int, CrossMapType > gcross_board_bin_;

	void MergeToBuf (size_t pipe_index, size_t barcode_index)
	{
		for (auto& item : out_set_)		
		{
			std::map <std::string, std::map<int, int> > dummy_map;
			out_index_[item.first] = item.second->end_Sam2Bam(dummy_map);	
			for (auto& dummy_pair: dummy_map)	//std::map<std::string, std::map<int, int> > cross_bin_map
			{
				for (auto& int_pair: dummy_pair.second)
				{
					cross_board_bin_[std::make_pair (item.first, dummy_pair.first)][int_pair.first]=5566;
				}
			}
		}

		//close current bam files, in stringstream format, without adding tailing empty bgzf block, i.e. preserve future appending possibility
		{
			std::lock_guard<std::mutex> lock(gOutMutex_);

			if ( pipe_index >= flag_.size() )
				flag_.resize (pipe_index+1);
			flag_[pipe_index]=7;	//use flag_ to to identify other pipeline that tobam operation corresponding to this very pipe_index has been achieved.  	
									//int 7 used as an indication for pipeline opeartion achieved

			CopyToBuf (gOutSet_[pipe_index], out_set_);	//having bam file contents of out_set_ recorded into gOutSet_
			MergeImpl (pipe_index, barcode_index);	//having bam fragments in gOutSet_ merged into gBuf_.  
			gIndex_.insert ({pipe_index, out_index_});
			gReadCount_[pipe_index].swap (ReadCount_);
			gcross_board_bin_[pipe_index].swap (cross_board_bin_);
		}
	}
	
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief implementation for copy out_set_ content into gOutSet_
	 * @return void
	 */
	void CopyToBuf(std::map<std::string, std::shared_ptr<std::stringstream> >& map_sum, OutPutType &map_item)
	{
		for (auto& q1 : map_item)
			map_sum[q1.first] = q1.second->ofss_;	//simply have [file_name, std::shared_ptr<std::stringstream>] pair, kept in the out_set_ copied into global object gOutSet_
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief have bam content in gOutSet_ merged into gBuf_.  It is noted that the merge operation must be achieved following the order of  pipe_index, e.g. merging operation corresponding to pipe_index=0 must be achieved prior to that corresponding to pipe_index=1, and so forth.
	 * @return void
	 */
	void MergeImpl (size_t pipe_index, size_t barcode_index)
	{
		int jump = gCurrentPipelineIndex_;
		for (auto ind=gCurrentPipelineIndex_; ind!=flag_.size(); ++ind)
		{
			if (flag_[ind]==7)
				++jump;
			else
				break;
		}	//scanning through flag_ to know till which pipe_index that all of its antecedent counterparts have been done, 
			//so that we can sequentially have each of which merged into gBuf_

		std::string sample_path ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[barcode_index]+"/bam/");
		mkdir (sample_path);

		for (int out_index=gCurrentPipelineIndex_; out_index!=jump; ++out_index)	//merge pipe content from gCurrentPipelineIndex_ till jump
		{
			for (auto& pipe_content : (gOutSet_[out_index]) )
			{
				if (gBuf_.find (pipe_content.first)==gBuf_.end())
				{
					file_writter_parameter_.set_filename (pipe_content.first , sample_path);
					gBuf_.insert ({pipe_content.first, std::make_shared < IoHandlerType > (file_writter_parameter_) });
					InitiateBamFile (pipe_content.first);	//write bam file header block in front of the bam content
				}
				else
					;
				auto bam_length = (gOutSet_[out_index][pipe_content.first])->tellp()-(gOutSet_[out_index][pipe_content.first])->tellg();
				char* aa = new char[bam_length+1];
				(gOutSet_[out_index][pipe_content.first])->read (aa, bam_length);
				(gBuf_[pipe_content.first])->write ( aa, bam_length );				
				delete aa;
				(gOutSet_[out_index][pipe_content.first]).reset ();	//have the bam content written into gBuf_
			}
		} 
		gCurrentPipelineIndex_=jump;
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to close bam files and append the tailing 27 byte bgzf blocks at the rear of each gBuf_ files to terminate bam file written for good. 
	 * @return void
	 */
	void TerminateBamFile (void)
	{
		for (auto& item : gBuf_)
		{
			Sam2BamUp<> SsCloser (item.first, PipelinePreparator<>::gBam_Header_);	//provide a Sam2BampUp object for tailing bgzf block written 
			std::map<std::string, std::map<int, int> > dummy_map;
			SsCloser.final_Sam2Bam(dummy_map);	
			auto content_length = SsCloser.ofss_->tellp() - SsCloser.ofss_->tellg();
			char* aa = new char [content_length+1];
			SsCloser.ofss_->read (aa, content_length);
			(gBuf_[item.first])->write (aa, content_length);
			delete aa;
			(gBuf_[item.first])->bs_async_close();
		}
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to close bam files and append the tailing 27 byte bgzf blocks at the rear of each gBuf_ files to terminate bam file written for good. 
	 * @return void
	 */
	void InitiateBamFile (const std::string& file_name)
	{
		Sam2BamUp<> Ssheader (file_name, PipelinePreparator<>::gBam_Header_);	//provide a Sam2BampUp object for header block written 
		Ssheader.WriteBamHeader();
		std::map<std::string, std::map<int, int> > dummy_map;
		std::get<0>(out_index_[file_name]).first = std::get<0>(Ssheader.end_Sam2Bam(dummy_map)).first;//hdr_length;	//record header block length in out_index_ object
		auto hdr_length = Ssheader.ofss_->tellp() - Ssheader.ofss_->tellg();
		char* aa = new char[hdr_length+1];
		Ssheader.ofss_->read (aa, hdr_length);
		(gBuf_[file_name])->write (aa, hdr_length); 
		delete aa;
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief have the start position corresponding to this pipe_index, i.e. std::get<0>(gIndex_[idx][item.first]).first, updated to be the sum of start_block_position and current block_length of previous pipe_index, i.e. std::get<0>(gIndex_[idx-1][item.first])).first and std::get<0>(gIndex_[idx-1][item.first]).second
	 * @return void
	 */
	void UpdateIndex (void)
	{
		for (auto& item : gBuf_)
		{
			int index=0;
			for (; index!=gIndex_.size() && gIndex_[index].find (item.first) == gIndex_[index].end(); ++index)
				;
			if (index==gIndex_.size())
				continue;
			else
				for (size_t idx=index+1; idx!=gIndex_.size(); ++idx)
					(std::get<0>(gIndex_[idx][item.first])).first = (std::get<0>(gIndex_[idx-1][item.first])).first+(std::get<0>(gIndex_[idx-1][item.first])).second;
		}
	}

	
	/**
	 * @brief Analysis在run時，所呼叫的function，主要是簡化重複程式碼，此部份主要負責 insert into map of output
	 * @tparam READ_TYPE 自動決定 anno rawbed data type
	 * @param anno_rawbed read 的資料結構，詳情請看 annotation_raw_bed.hpp
	 * @param sys 此為特殊參數，主要是output所必須記錄，0=> db_index為空 and db_depth為空，1=> db_index為空 and db_depth不為空，2=>db_index不為空 and db_depth不為空
	 * @param filter 使用者定的 filter 參數，詳情請看上面的 typedef
	 * @param db_index 使用者定的 db_index 參數，詳情請看上面的 typedef
	 * @param db_depth 使用者定的 db_depth 參數，詳情請看上面的 typedef
	 * @param db_depth_name 使用者定的 db_depth_name 參數，詳情請看上面的 typedef
	 * @return void
	 */
	void CalOutput (READ_TYPE &anno_rawbed ,const int sys, const int filter, const int db_index, const int db_depth, const char* db_depth_name)
	{
		std::stringstream name;
		std::string name_temp (db_depth_name);
		while (true)
		{
			auto pos = name_temp.find (' ');
			if (pos==std::string::npos)
				break;
			else
				name_temp[pos]='_';
		}
		if (name_temp.size()==0)
			name <<"BAM."<<sys<<"."<<filter<<"."<<db_index<<"."<<db_depth<<"."<<'_'<<".sorted.bam";	//output bam file name construction
		else
			name <<"BAM."<<sys<<"."<<filter<<"."<<db_index<<"."<<db_depth<<"."<<name_temp<<".sorted.bam";	//output bam file name construction

		auto name_str = name.str();
		if (out_set_.find (name_str) != out_set_.end() )
			(out_set_[name_str])->run (anno_rawbed);	//using established std::shared_ptr < Sam2BamUp<> > to have inputted annotation_raw_bed written into bam file
		else 
		{
			out_set_.insert ( {name_str, std::make_shared < Sam2BamUp<> > (name_str, PipelinePreparator<>::gBam_Header_)} ); //anno_rawbed.g_headerstr)});
			(out_set_[name_str])->run (anno_rawbed);	//establish [file_name, std::shared_ptr <Sam2BamUp<> > pair to do bam file written operation
		}

		ReadCount_[name_str][anno_rawbed.GetChr (anno_rawbed.chr_idx_)]+=anno_rawbed.reads_count_;//std::map<std::string, std::map <std::string, uint32_t> > ReadCount_;
	}
};

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::mutex
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gOutMutex_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map<int, std::map<std::string, std::shared_ptr<std::stringstream> > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gOutSet_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
int 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gCurrentPipelineIndex_=0;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::vector <int> 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::flag_(0);

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map <std::string, std::shared_ptr < typename at<ANALYZER_TYPELIST, typename AnalyzerParameter <AnalyzerTypes::ToBam>::IoHandlerType, IoHandlerOfstream >::type > > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gBuf_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map < int, std::map <std::string, std::tuple < std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int, int, int> > > > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gIndex_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map< int, std::map <std::string, std::map <std::string, uint32_t> > > 
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gReadCount_;

template<class INPUT_TYPE, class ANALYZER_TYPELIST>
std::map<int, std::map < std::pair<std::string,std::string>, std::map <int, int> > >
AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gcross_board_bin_;



template<class ANALYZER_TYPELIST>
class AnalyzerImpl< std::map <int, std::vector< Sam<> > >, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::ToBam> AnaPara;

	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType; 
	/// @brief 輸入的資料
	std::map <int, std::vector< Sam<> > > &in;
	
	/** 
	 * @brief 輸出的資料型別 \n output map <file_name, bam_seq>
	 */
	typedef std::map<std::string, std::shared_ptr < Sam2BamUp<> > > OutPutType;
	
	/// @brief 輸出的資料
	OutPutType out_set_;
	
	/// @brief a buffer to hold the achieved pipeline result, as a pipeline index to its corresponding map of file_name and shared_ptr <stringstream>.  The result will be temporary buffered in gOutSet_ till the results corresponding to all of the precending pipeline indexes have been achieved.  Then the result will be merged into gBuf_ object.
//	static std::map< int, std::map <std::string, std::shared_ptr <std::stringstream> > > gOutSet_;
	
	/// @brief a global mutex object for establishing lock_guard object for merging results coming from each of the posted tobam pipelines
	static std::mutex gOutMutex_;

	/// @brief a sequential index, keeping record till which pipe line index that the results have been achieved and been merged into gBuf_ object.  The design is a must be since the merging operation of each tobam pipelines must be achieved in the order of the pipeline index.
	static int gCurrentPipelineIndex_;
	
	/// @brief the object to hold the merged result.  The upload operation or file writing operation will be conducted based on the merged result stored in gBuf_ element
	static std::map <std::string, std::shared_ptr < IoHandlerType > > gBuf_;

	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(std::map <int, std::vector< Sam<> > > &i)
		: in (i)//, this_analyzer_count_(0)
 		, file_writter_parameter_ (PipelinePreparator<>::gDeviceParameter_)
	{}

	AnalyzerImpl()
	{}

	/// @brief a cluster of flags indicating which pipeline has been achieved and bufferred into the static gBuf_ object
	static std::vector <int> flag_;

	DeviceParameter file_writter_parameter_;

    void mkdir(std::string& sample_path)
    {  
        boost::filesystem::path dir_output("output");
        boost::filesystem::create_directory(dir_output);
        boost::filesystem::path dir_analyzer(sample_path);
        boost::filesystem::create_directory(dir_analyzer);
    }

	template <typename T>
	void free (T& q)
	{
		T gg;
		gg.swap (q);
	}

	void* operator()(size_t pipe_index, bool eof_flag)//, std::vector<std::string>& barcode_vec)
	{
		for (auto& item : in)	//in format of std::map <int, std::vector< Sam<> > >
		{
			std::string sample_path ("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+PipelinePreparator<>::gBarcode_vector_[item.first]);
			mkdir (sample_path);

			std::string name_str ("BAM.sample-"+PipelinePreparator<>::gBarcode_vector_[item.first]+".unsorted.bam");
			out_set_.insert ( {name_str, std::make_shared < Sam2BamUp<> > (name_str, PipelinePreparator<>::gBam_Header_)} ); 
			for (auto& sam : item.second)
				(out_set_[name_str])->run (sam.str());

			item.second.clear();
			free (item.second);

			std::map<std::string, std::map<int, int> > dummy_map;
			(out_set_[name_str])->end_Sam2Bam(dummy_map);

			{
				std::lock_guard<std::mutex> lock(gOutMutex_);
				if (gBuf_.find (name_str)==gBuf_.end())
				{
            		file_writter_parameter_.set_filename( name_str , sample_path);
					gBuf_.insert ({name_str, std::make_shared < IoHandlerType > (file_writter_parameter_) });
					InitiateBamFile (name_str);	//write bam file header block in front of the bam content
				}
				else
					;
			}
			item.second.clear();
			free (item.second);
		}

		{
			std::lock_guard<std::mutex> lock(gOutMutex_);
			for (auto& bam_content: out_set_)
			{
				auto file_name = bam_content.first;
				auto& bamstr = bam_content.second->ofss_;
				auto bam_length = bamstr->tellp() - bamstr->tellg();
				char* aa = new char[bam_length+1];
				bamstr->read (aa, bam_length);
				(gBuf_[file_name])->write (aa, bam_length);
				delete aa;
				bam_content.second.reset();
			}
			out_set_.clear();
		}

		if (eof_flag)
		{
			TerminateBamFile ();
			ClearContent ();
		}
		return NULL;
	}
private:
	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to clear all static members
	 * @return void
	 */
	void ClearContent (void)
	{
//		gOutSet_.clear();
		gCurrentPipelineIndex_=0;
		flag_.clear();
		if (boost::is_same<IoHandlerType, std::stringstream >::value)
			;
		else
			for (auto& item : gBuf_)
				item.second->close();
		gBuf_.clear();
		free(gBuf_);
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief implementation for copy out_set_ content into gOutSet_
	 * @return void
	 */
//	void CopyToBuf(std::map<std::string, std::shared_ptr<std::stringstream> >& map_sum, OutPutType &map_item)
//	{
//		for (auto& q1 : map_item)
//			map_sum[q1.first] = q1.second->ofss_;	//simply have [file_name, std::shared_ptr<std::stringstream>] pair, kept in the out_set_ copied into global object gOutSet_
//	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to close bam files and append the tailing 27 byte bgzf blocks at the rear of each gBuf_ files to terminate bam file written for good. 
	 * @return void
	 */
	void TerminateBamFile (void)
	{
		for (auto& item : gBuf_)
		{
			Sam2BamUp<> SsCloser (item.first, PipelinePreparator<>::gBam_Header_);	//provide a Sam2BampUp object for tailing bgzf block written 
			std::map<std::string, std::map<int, int> > dummy_map;
			SsCloser.final_Sam2Bam(dummy_map);	
			auto content_length = SsCloser.ofss_->tellp() - SsCloser.ofss_->tellg();
			char* aa = new char[content_length+1];
			SsCloser.ofss_->read (aa, content_length);
			(gBuf_[item.first])->write (aa, content_length);
			delete aa;			
			(gBuf_[item.first])->bs_async_close();
		}
	}

	/**
	 * @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	 * @brief provide an interface to close bam files and append the tailing 27 byte bgzf blocks at the rear of each gBuf_ files to terminate bam file written for good. 
	 * @return void
	 */
	void InitiateBamFile (const std::string& file_name)
	{
		Sam2BamUp<> Ssheader (file_name, PipelinePreparator<>::gBam_Header_);	//provide a Sam2BampUp object for header block written 
		Ssheader.WriteBamHeader();//		auto hdr_length = 
		std::map<std::string, std::map<int, int> > dummy_map;
		Ssheader.end_Sam2Bam(dummy_map);//hdr_length;	//record header block length in out_index_ object
		auto hdr_length = Ssheader.ofss_->tellp() - Ssheader.ofss_->tellg();
		char* aa = new char[hdr_length+1];
		Ssheader.ofss_->read (aa, hdr_length);
		(gBuf_[file_name])->write (aa, hdr_length);
		delete aa;
	}
};

template<class ANALYZER_TYPELIST>
std::mutex
AnalyzerImpl<std::map <int, std::vector< Sam<> > >, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gOutMutex_;

//template<class ANALYZER_TYPELIST>
//std::map<int, std::map<std::string, std::shared_ptr<std::stringstream> > >
//AnalyzerImpl<std::map <int, std::vector< Sam<> > >, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gOutSet_;

template<class ANALYZER_TYPELIST>
int 
AnalyzerImpl<std::map <int, std::vector< Sam<> > >, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gCurrentPipelineIndex_=0;

template<class ANALYZER_TYPELIST>
std::vector <int> 
AnalyzerImpl<std::map <int, std::vector< Sam<> > >, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::flag_(0);

template<class ANALYZER_TYPELIST>
std::map <std::string, std::shared_ptr < typename at<ANALYZER_TYPELIST, typename AnalyzerParameter <AnalyzerTypes::ToBam>::IoHandlerType, IoHandlerOfstream >::type > > 
AnalyzerImpl<std::map <int, std::vector< Sam<> > >, ANALYZER_TYPELIST, AnalyzerTypes::ToBam>::gBuf_;


template<class INPUT_TYPE>
class AnalyzerImplInitToBam
{
public:

	INPUT_TYPE in;
	size_t pipe_index_;
	bool eof_flag_;
	std::map<int,int> analyzer_count_type_;

	//typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map <int, std::tuple <int, int, int, int> > > > BamIndexType;
	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > BamIndexType;
	static std::map <int, BamIndexType >* gIndexPtr_;
	
	AnalyzerImplInitToBam(INPUT_TYPE i)
		: in (i)
	{}

	AnalyzerImplInitToBam()
	{}

	~AnalyzerImplInitToBam()
    {
		delete gIndexPtr_;
	}

	template<class ANALYZER_TYPELIST>
	void //	std::map <int, BamIndexType >*
	operator()(ANALYZER_TYPELIST t)
	{
		//取得 analyzer type
		typedef typename boost::mpl::at<ANALYZER_TYPELIST, boost::mpl::int_<0> >::type AnalyzerType;
		AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerType::value> Analysis(in);
		gIndexPtr_ = (std::map <int, BamIndexType >*) Analysis(analyzer_count_type_[AnalyzerType::value], 
				pipe_index_, eof_flag_ );	
		++analyzer_count_type_[AnalyzerType::value];
	}

	AnalyzerImplInitToBam(INPUT_TYPE i, size_t pipe_index, bool eof_flag)
		: in (i), pipe_index_ (pipe_index), eof_flag_ (eof_flag)
	{}
};

template<class INPUT_TYPE>
std::map <int, std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > >*
AnalyzerImplInitToBam <INPUT_TYPE>::gIndexPtr_;


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerToBam
{
public:
	typedef std::map <std::string, std::tuple <std::pair<int, int>, std::map < std::pair<std::string, int>, std::tuple <int, int, int, int> > > > BamIndexType;

	std::map <int, BamIndexType>* 
	run (INPUT_TYPE in, size_t pipe_index=0, bool eof_flag =false) 
	{
		boost::mpl::for_each<ANALYZER_TYPELIST> ( AnalyzerImplInitToBam<INPUT_TYPE>( in, pipe_index, eof_flag ) );
		return AnalyzerImplInitToBam<INPUT_TYPE>::gIndexPtr_;
	}
};

#endif
