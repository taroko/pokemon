#ifndef ANALYZER_PRINTER_DUAL_ANALYZER_HPP_
#define ANALYZER_PRINTER_DUAL_ANALYZER_HPP_

#include <fstream>
#include <array>
#include <boost/filesystem.hpp>
#include "../constant_def.hpp"
#include "../tuple_utility.hpp"
#include "../iohandler/iohandler.hpp"
#include "../iohandler/basespace_def.hpp"
#include "boost/algorithm/string/split.hpp"

template<>
class AnalyzerParameter<AnalyzerTypes::LenDistPrinterDualAnalyzer>
{
public:
	/** 
	 * @brief 定義 length distribution參數有哪些
	 */
	
	/// @brief AnalyzerType 特化Analyzer的type，每一個AnalyzerParameter都應該要有此參數
	typedef boost::mpl::int_<0> AnalyzerType;
	
	/// @brief 
	typedef boost::mpl::int_<1> AnnoIdx;
	
	typedef boost::mpl::int_<2> Xaxis;
	
	typedef boost::mpl::int_<3> Yaxis;
	
	typedef boost::mpl::int_<4> Zaxis;
	
	typedef boost::mpl::int_<5> Xlimit;
	
	typedef boost::mpl::int_<6> Ylimit;
	
	typedef boost::mpl::int_<7> Zlimit;
	
	typedef boost::mpl::int_<8> Len;
	
	typedef boost::mpl::int_<9> Anno;
	
	typedef boost::mpl::int_<10> Seq;
	
	typedef boost::mpl::int_<11> PrefixName;
	
	typedef boost::mpl::int_<12> IoHandlerType;

	typedef boost::mpl::int_<13> AnnoIdx2;

	typedef boost::mpl::int_<14> XSort;
	
	typedef boost::mpl::int_<15> YSort;
	
	typedef boost::mpl::int_<16> ZSort;
};


template<class INPUT_TYPE, class ANALYZER_TYPELIST>
class AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LenDistPrinterDualAnalyzer>
{
public:
	/// @brief 此為要使用的 analyzer parameter
	typedef AnalyzerParameter <AnalyzerTypes::LenDistPrinterDualAnalyzer> AnaPara;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::AnnoIdx, boost::mpl::int_<0> >::type AnnoIdx;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::AnnoIdx2, boost::mpl::int_<1> >::type AnnoIdx2;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Xaxis, boost::mpl::pair< AnaPara::Len, boost::mpl::string<'-1'> > >::type Xaxis;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Yaxis, boost::mpl::pair< AnaPara::Anno, boost::mpl::string<'-1'> > >::type Yaxis;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Zaxis, boost::mpl::pair< AnaPara::Seq, boost::mpl::string<'-2'> > >::type Zaxis;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Xlimit, boost::mpl::int_<0> >::type Xlimit;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Ylimit, boost::mpl::int_<0> >::type Ylimit;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::Zlimit, boost::mpl::int_<0> >::type Zlimit;

	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::XSort, boost::mpl::bool_<false> >::type XSort;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::YSort, boost::mpl::bool_<false> >::type YSort;
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::ZSort, boost::mpl::bool_<true> >::type ZSort;
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::IoHandlerType, IoHandlerOfstream >::type IoHandlerType; 
	
	
	typedef typename at<ANALYZER_TYPELIST, typename AnaPara::PrefixName, boost::mpl::string<'Tab', 'le'> >::type PrefixName;
	
	typedef std::vector< std::map<int, std::map<std::tuple<int,int,int,int,std::string,std::string>, std::tuple<double, double, double> > > > gOutSetType ;
	gOutSetType &gOutSet;
	
	std::vector<std::string> &gOutNamePrefix;
	
	std::string output_path;
	
	/// @brief 輸入的資料
	INPUT_TYPE &in;
	
	// Z(X(Y))
//	std::map<std::string, std::map<std::string, std::map<std::string, double> > > cal_table;
	std::map<std::string, std::map<std::string, std::map<std::string, std::tuple <double, double> > > > cal_table;
	std::map<std::string, std::map<std::string, std::map<std::string, std::tuple <double, double> > > > cal_tmp;
	/// @brief tuple< filter = -1, filter = 1, read_count>
//	std::map<std::string, std::vector<double> > cal_normalize;
	
	/// @memberof AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST, AnalyzerTypes::LengthDistribution>
	/// @brief AnalyzerImpl 建構子
	AnalyzerImpl(INPUT_TYPE &i)
		: in (i)
		, gOutSet(AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutSet_)
		, gOutNamePrefix(AnalyzerImpl<INPUT_TYPE, ANALYZER_TYPELIST_GLOBAL_LENDIST, AnalyzerTypes::LengthDistribution>::gOutNamePrefix_)
		, output_path("")
	{}
	
	void* operator()(int this_analyzer_count, size_t pipe_index, bool eof_flag, int barcode_index=0)
	{
		if(eof_flag)
		{
			//output_path = std::string("output/sample-") + std::to_string(barcode_index) + std::string("/analysis/");
			output_path = std::string("output/"+ PipelinePreparator<>::gDeviceParameter_.bs_analysis_prefix_name_ + "-sample-"+ PipelinePreparator<>::gBarcode_vector_[barcode_index] + std::string("/analysis/"));
			Analysis ();
			mkdir();
			//0, -1, -1, 0, ""
			//0, 1, -1, 0, ""
			//0, 0, -1, 0, "" ; no use
			//double normalize_filter = std::get<0>(gOutSet[0][-1][std::make_tuple(0,1,0,0,"","")]);
			//double normalize_all = std::get<0>(gOutSet[0][-1][std::make_tuple(0,-1,0,0,"","")]);
			
			//output ("read_count");
			//output ("allread_ppm", 0);
			//output ("ppm", 1);
			output ("tailing_ratio");			
			
			//std::cout << "filter seqdepth " << std::get<0>(gOutSet[0][-1][std::make_tuple(0,1,0,0,"","")]) << std::endl;
			//std::cout << "All seqdepth " << std::get<0>(gOutSet[0][-1][std::make_tuple(0,-1,0,0,"","")]) << std::endl;			
		}
		return (void*) &in;
	}

	std::string get_XYZ_name(const int i)
	{
		if( i == 8)
			return "LEN";
		else if(i == 9)
			return "ANNO";
		else if(i == 10)
			return "SEQ";
	}

	std::string get_filename(std::string Z_name, const std::string tail_name)
	{
		std::string NamePrefix = gOutNamePrefix[AnnoIdx::value];
		if (Z_name[0] == '0')
		{
			std::vector<std::string> tmp_v;
			boost::split( tmp_v, NamePrefix, boost::is_any_of( "." ));
			NamePrefix = "";
			for(int i=0; i != tmp_v.size()-2; i++)
			{
				NamePrefix += tmp_v[i]+".";
			}
			NamePrefix += "merged.no_detail";
		}
		if(Z_name[Z_name.size()-1] == '.')
		{
			Z_name += "NoSeq";
		}

		auto pos = NamePrefix.find ("GMPM");
		if (pos != std::string::npos)
			NamePrefix.replace (pos, 4,"TailingRatio");

//		NamePrefix.append("Tailing_ratio");//gOutNamePrefix[AnnoIdx::value];
		std::string filename(boost::mpl::c_str<PrefixName>::value);
		filename += ".";
		filename += NamePrefix + "."; //lendist.Normal.full.GMPM.biotype.no_detail


		std::vector<std::string> tmp_v;
		boost::split( tmp_v, Z_name, boost::is_any_of( "." ));
		Z_name = "";
		for (int ind=0; ind!=tmp_v.size()-1; ++ind)
			Z_name += (tmp_v[ind]+".");


		filename += Z_name;// + ".";//2.1.0.0.Total . 
		filename += get_XYZ_name(Zaxis::first::value) + ".";//SEQ.
		filename += get_XYZ_name(Xaxis::first::value) + "vs";//LENvsANNO.
		filename += get_XYZ_name(Yaxis::first::value) + ".";//read_count.
		filename += (tmp_v.back()+".");
		filename += tail_name;
//std::cerr<<"====== filename ======"<<filename<<'\n';

		return filename; 
	}

	void Analysis()
	{
		auto &OutSet = gOutSet[AnnoIdx::value];
		for(auto &len_pair : OutSet)
		{
			int len = len_pair.first;
			if(len == -1)
				continue;
			for(auto &anno_value_pair :len_pair.second)
			{
			
				const std::string &anno = std::get<4>(anno_value_pair.first);
				const std::string &seq = std::get<5>(anno_value_pair.first);
				
				double &read_count = std::get<0>(anno_value_pair.second);
				
				int dim_x = Xaxis::first::value-8;//1
				int dim_y = Yaxis::first::value-8;//2
				int dim_z = Zaxis::first::value-8;//0 len -2
				
				std::string type_x (boost::mpl::c_str<typename Xaxis::second>::value);//-1
				std::string type_y (boost::mpl::c_str<typename Yaxis::second>::value);//-1
				std::string type_z (boost::mpl::c_str<typename Zaxis::second>::value);//-2
				
				std::array<std::string, 3> origin_type ({{ type_x, type_y, type_z }});

				origin_type[dim_x] = type_x;// 1 -1
				origin_type[dim_y] = type_y;// 2 -1
				origin_type[dim_z] = type_z;// 0 -2


				std::string X = get_key(origin_type[0], std::to_string(len)); //-2
				std::string Y = get_key(origin_type[1], anno);//-1
				std::string Z = get_key(origin_type[2], seq);//-1
				
				std::string sum_x_name = "SUM_" + get_XYZ_name(Xaxis::first::value);
				std::string sum_y_name = "SUM_" + get_XYZ_name(Yaxis::first::value);
				
				std::array<std::string, 3> XYZ ({{ X, Y, Z }});
				
				std::string prefix("");
				prefix += std::to_string( std::get<0>(anno_value_pair.first) ) + "."; // sys
				prefix += std::to_string( std::get<1>(anno_value_pair.first) ) + "."; // filter
				prefix += std::to_string( std::get<2>(anno_value_pair.first) ) + "."; // db_idx
				prefix += std::to_string( std::get<3>(anno_value_pair.first) ) + "."; // db_depth
				
				std::get<0>(cal_tmp[ prefix + XYZ[dim_z] ][ XYZ[dim_x] ][ XYZ[dim_y] ] ) += read_count; // z-1 x -2  y-1
				std::get<0>(cal_tmp[ prefix + XYZ[dim_z] ][ sum_x_name ][ sum_y_name ] ) += read_count; 
				std::get<0>(cal_tmp[ prefix + XYZ[dim_z] ][ XYZ[dim_x] ][ sum_y_name ] ) += read_count;
				std::get<0>(cal_tmp[ prefix + XYZ[dim_z] ][ sum_x_name  ][ XYZ[dim_y] ]) += read_count;
				
				//record ppm
//				if(cal_normalize.find(prefix + XYZ[dim_z]) == cal_normalize.end())
//				{
//					int db_idx = std::get<2>(anno_value_pair.first);
//					double normalize_filter = std::get<0>(gOutSet[0][-1][std::make_tuple(0,1,db_idx,0,"","")]); // 1
//					double normalize_all = std::get<0>(gOutSet[0][-1][std::make_tuple(0,-1,db_idx,0,"","")]); // -1
//					cal_normalize.insert({ prefix + XYZ[dim_z], {normalize_all, normalize_filter, 1000000} });
//				}
			}
		}

		auto &OutSet2 = gOutSet[AnnoIdx2::value];
		for(auto &len_pair : OutSet2)
		{
			int len = len_pair.first;
			if(len == -1)
				continue;
			for(auto &anno_value_pair :len_pair.second)
			{
			
				const std::string &anno = std::get<4>(anno_value_pair.first);
				const std::string &seq = std::get<5>(anno_value_pair.first);
				
				double &read_count = std::get<0>(anno_value_pair.second);
				
				int dim_x = Xaxis::first::value-8;//1
				int dim_y = Yaxis::first::value-8;//2
				int dim_z = Zaxis::first::value-8;//0 len -2
				
				std::string type_x (boost::mpl::c_str<typename Xaxis::second>::value);//-1
				std::string type_y (boost::mpl::c_str<typename Yaxis::second>::value);//-1
				std::string type_z (boost::mpl::c_str<typename Zaxis::second>::value);//-2
				
				std::array<std::string, 3> origin_type ({{ type_x, type_y, type_z }});

				origin_type[dim_x] = type_x;// 1 -1
				origin_type[dim_y] = type_y;// 2 -1
				origin_type[dim_z] = type_z;// 0 -2


				std::string X = get_key(origin_type[0], std::to_string(len)); //-2
				std::string Y = get_key(origin_type[1], anno);//-1
				std::string Z = get_key(origin_type[2], seq);//-1
				
				std::string sum_x_name = "SUM_" + get_XYZ_name(Xaxis::first::value);
				std::string sum_y_name = "SUM_" + get_XYZ_name(Yaxis::first::value);
				
				std::array<std::string, 3> XYZ ({{ X, Y, Z }});
				
				std::string prefix("");
				prefix += std::to_string( std::get<0>(anno_value_pair.first) ) + "."; // sys
				prefix += std::to_string( std::get<1>(anno_value_pair.first) ) + "."; // filter
				prefix += std::to_string( std::get<2>(anno_value_pair.first) ) + "."; // db_idx
				prefix += std::to_string( std::get<3>(anno_value_pair.first) ) + "."; // db_depth
				
				std::get<1>(cal_tmp[ prefix + XYZ[dim_z] ][ XYZ[dim_x] ][ XYZ[dim_y] ] ) += read_count; // z-1 x -2  y-1
				std::get<1>(cal_tmp[ prefix + XYZ[dim_z] ][ sum_x_name ][ sum_y_name ] ) += read_count; 
				std::get<1>(cal_tmp[ prefix + XYZ[dim_z] ][ XYZ[dim_x] ][ sum_y_name ] ) += read_count;
				std::get<1>(cal_tmp[ prefix + XYZ[dim_z] ][ sum_x_name  ][ XYZ[dim_y] ]) += read_count;
				
				//record ppm
//				if(cal_normalize.find(prefix + XYZ[dim_z]) == cal_normalize.end())
//				{
//					int db_idx = std::get<2>(anno_value_pair.first);
//					double normalize_filter = std::get<0>(gOutSet[0][-1][std::make_tuple(0,1,db_idx,0,"","")]); // 1
//					double normalize_all = std::get<0>(gOutSet[0][-1][std::make_tuple(0,-1,db_idx,0,"","")]); // -1
//					cal_normalize.insert({ prefix + XYZ[dim_z], {normalize_all, normalize_filter, 1000000} });
//				}
			}
		}

		for (auto& pair_1: cal_tmp)
			for (auto& pair_2: pair_1.second)
				for (auto& pair_3: pair_2.second)
					cal_table[pair_1.first][pair_2.first][pair_3.first] = std::make_tuple (std::get<1>(pair_3.second) / std::get<0>(pair_3.second), std::get<1>(pair_3.second));
	}

	void mkdir()
	{
		boost::filesystem::path dir_output("output");
		boost::filesystem::create_directory(dir_output);
		
		boost::filesystem::path dir_analyzer(output_path);
		boost::filesystem::create_directory(dir_analyzer);
	}
	
	void output(const std::string &unit = "read_count", double normalize_idx = 2)
	{
		// sort Z by value, big -> small according to PM count, rather than PM/PMGM ratio
		std::vector< std::pair<std::string, std::tuple<double,double> > > sorted_z;
		for(auto & dim_z : cal_table)
		{
			std::string sum_x_name = "SUM_" + get_XYZ_name(Xaxis::first::value);
			std::string sum_y_name = "SUM_" + get_XYZ_name(Yaxis::first::value);
			sorted_z.push_back({ dim_z.first, std::make_tuple (std::get<0>(dim_z.second[sum_x_name][sum_y_name]), std::get<1>(dim_z.second[sum_x_name][sum_y_name])) });
		}
		if (ZSort::value==true)
		{
			std::sort(sorted_z.begin(), sorted_z.end(), 
				[](const std::pair<std::string, std::tuple<double,double> > &a, const std::pair<std::string, std::tuple<double, double> > &b)
				{
					return std::get<1>(a.second) > std::get<1>(b.second);
				}
			);
		}
		
		std::vector< std::shared_ptr<IoHandlerType> > vec;
		int count_z(0);
		
		for(auto &z_title : sorted_z)
		{
			if(count_z > Zlimit::value && Zlimit::value != 0)
				break;

			auto &dim_z_first = z_title.first;
			auto &dim_z_second = cal_table[dim_z_first];

			std::string sum_x_name = "SUM_" + get_XYZ_name(Xaxis::first::value);
			std::string sum_y_name = "SUM_" + get_XYZ_name(Yaxis::first::value);

			// empty table continue
			if (std::get<1>(dim_z_second[sum_x_name][sum_y_name])==0.0)
				continue;

			// iohandler object preparation
			std::string filename = get_filename(dim_z_first, unit + ".tsv");
            DeviceParameter file_writter_parameter (PipelinePreparator<>::gDeviceParameter_);
            file_writter_parameter.set_filename( filename , output_path);
			std::shared_ptr <IoHandlerType> out_ (std::make_shared <IoHandlerType> (file_writter_parameter));
			vec.push_back (out_);


			std::cerr << output_path + filename << std::endl;
			*(out_) << "====================" << std::endl;
			*(out_) << filename << std::endl;
			*(out_) << "--------------------" << std::endl;


			// sort X by value, big -> small according to PM count, rather than PM/PMGM ratio
			std::vector<std::pair <std::string, std::tuple<double, double> > > all_x_title;
			for (auto &axis_pair : dim_z_second)
				all_x_title.push_back ({axis_pair.first, std::make_tuple (std::get<0>(axis_pair.second[sum_y_name]), std::get<1>(axis_pair.second[sum_y_name]))});
			if (Xlimit::value !=0 || XSort::value==true)
			{
				std::sort(all_x_title.begin(), all_x_title.end(), 
				    [](const std::pair<std::string, std::tuple <double, double> > &a, const std::pair<std::string, std::tuple <double, double> > &b)
				    {
				        return std::get<1>(a.second) > std::get<1>(b.second);
				    }
				);
			}
			
			std::vector< std::pair< std::string, std::tuple<double,double> > > all_title;
			int count_x(0);

			for(auto &dim_x : all_x_title)
			{
				if(count_x > Xlimit::value && Xlimit::value != 0)
					break;
				
				if(count_x == 0)
				{
					*(out_) << "table" << "\t";

					// sort Y by value, big -> small according to PM count, rather than PM/PMGM ratio
					for(auto &dim_y : (dim_z_second)[sum_x_name])
					{
						//if(dim_y.first == "")
							//continue;
						//*(out_) << dim_y.first << "\t";
						all_title.push_back(dim_y);
						//all_title[dim_y.first] = dim_y.second;
					}
					// sort y (anno) big > small
					if (Ylimit::value !=0 || YSort::value==true)
					{
						std::sort(all_title.begin(), all_title.end(), 
							[](const std::pair<std::string, std::tuple<double, double> > &a, const std::pair<std::string, std::tuple<double, double> > &b)
							{
								return std::get<1>(a.second) > std::get<1>(b.second);
							}
						);
					}

					int count_y(0);
					for(auto &title : all_title)
					{
						if(count_y > Ylimit::value && Ylimit::value != 0)
							break;
						if(title.first == "")
							*(out_) << "tailing ratio" << "\t";
						else
							*(out_) << title.first << "\t";
						++count_y;
					}
					*(out_) << "\n" ;
				}

				// print no value when the sum corresponds to value of zero
				if ( std::get<1>((dim_z_second)[dim_x.first][sum_y_name])==0 )	
				{
					++count_x;
					continue;
				}

				*(out_) << dim_x.first << "\t";
				
				int count_y(0);
				for(auto &title : all_title)
				{
					if(count_y > Ylimit::value && Ylimit::value != 0)
						break;
					out_->precision(3);
					*(out_) << std::fixed << std::get<0>( (dim_z_second)[dim_x.first][title.first]) << "\t";
					++count_y;
				}
				
				*(out_) << "\n" ;
				++count_x;
			}
			*(out_) << "--------------------" << std::endl;
			++count_z;
			
			//out_->close ();
			out_->bs_async_close();
		}

		for (auto& q : vec)
			q->close();
	}
	
	std::string get_key(const std::string &type, const std::string &value)
	{
		if(type == "-1")
		{
			return value;
		}
		else if(type == "-2")
		{
			return "Merged";
		}
		else if(type == "-3")
		{
			if(value == "")
				return "No-seq";
			else
				return "Having-seq";
		}
		else
		{
			if(type == value)
				return value;
			else
				return "Other";
		}
	}
};
#endif
