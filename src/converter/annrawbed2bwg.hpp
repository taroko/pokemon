/**
 *  @file annrawbed2bwg.hpp
 *  @brief A pipeline implementation for calculating certain reads information, e.g. coverage for each of the aligned AnnotationRawBed<> reads, and used it to create a wig/bigwig file
 *  @author C-Salt Corp.
 */
#ifndef ANNRAWBED2BWG_HPP_
#define ANNRAWBED2BWG_HPP_
#include <sstream>
#include "../../pipeline/htslib/htslib/sam.h"
#include "../thread_pool_update.hpp"
#include "../format/annotation_raw_bed.hpp"

/**
 * @struct AnnRawBed2Bwg
 * @brief used to do reads information calculation and used it for wig/bigwig file creation
 * @tparam VALUE_ESTIMATE_TYPE designating which kind of way AnnRawBed2Bwg class is going to apply to calculate the wig value.\n The VALUE_ESTIMATE_TYPE must have a global function GetValue, which takes input data, wig_span_, and calculated wig values as parameters, and returns an uint32_t indicating to which tile of the chromosome, scaled by the wig_span_, the input read is actually corresponded.
 * @tparam INPUT_TYPE indicating the input read type
 */
template< class VALUE_ESTIMATE_TYPE, class INPUT_TYPE=AnnotationRawBed <> >
class AnnRawBed2Bwg
{
	typedef std::shared_ptr < std::map < std::string, // chromosome
				std::map < uint32_t, float >	//start, value
					> > ItemType;
/**
 * @memberof AnnRawBed2Bwg
 * @brief member to hold the content of wig/bigwig file, i.e. wig values corresponding to a series of interval scaled chromosomes.  In wig format, we use a value of wig_span_ to do interval scaling for each chromosomes, and a wig value is accordingly recorded for each of the chormosome tiles.  Therefore, the content of wig/bigwig file is recored in the type of shared_ptr of map [ chromosome, map [ wig_tile, wig_value] ].
 */
	ItemType items_;	
public:
/**
 * @memberof AnnRawBed2Bwg
 * @brief member indicating to which wig/bigwig file current AnnRawBed2Bwg is corresponded.
 */
	std::string output_name_;
/**
 * @memberof AnnRawBed2Bwg
 * @brief indicating the wig_step_ information
 */
	uint32_t wig_step_;
/**
 * @memberof AnnRawBed2Bwg
 * @brief indicating the wig_span_ information, i.e. the resolution of the wig/bigwig format
 */
	uint32_t wig_span_;
/**
 * @memberof AnnRawBed2Bwg
 * @brief constructor indicating the wig_step_ information
 */
	AnnRawBed2Bwg(size_t pipe_index, std::string file_name, size_t step=100, size_t span=100)
		: items_ ( std::make_shared < std::map< std::string, std::map < uint32_t, float > > >() )
		, output_name_ (file_name)
		, wig_step_ (step)
		, wig_span_ (span)
	{}
/**
 * @memberof AnnRawBed2Bwg
 * @brief the function of calling VALUE_ESTIMATE_TYPE::GetValue to calculate wig value according to inputted read, and append its value into items_.\n For each input read, it can be represented as a certain piece of quantity information corresponding to certain chromosome tiles.  The VALUE_ESTIMATE_TYPE::GetValue obtains the certain piece of quantity information and reports in a vector<float>, together with the tile information indicating to which tile of the chromosome, scaled by the wig_span_, the input read is actually corresponded.
 */
	void run(INPUT_TYPE in)
	{
/*
		std::string chr = in.chromosome_;
		std::vector<float> value;
		uint32_t wig_tile = VALUE_ESTIMATE_TYPE::GetValue (in, wig_span_, value);
//		uint32_t wig_tile = GetCoverage (in.start_, in.end_, wig_span_, value);

		for (auto& vax : value)
		{
			(*items_)[chr][wig_tile] += vax;
			++wig_tile;
		}
*/
		std::vector<float> value;
		std::pair<std::string, uint32_t> position = VALUE_ESTIMATE_TYPE::GetValue (in, wig_span_, value);
//		std::pair<std::string, uint32_t> position = GetCoverage (in.start_, in.end_, wig_span_, value);
		for (auto& vax : value)
		{
			(*items_)[position.first][position.second] += vax;
			++position.second;//++wig_tile;
		}

	}
/**
 * @memberof AnnRawBed2Bwg
 * @brief Give an illustrative example showing how the VALUE_ESTIMATE_TYPE::GetValue may actually looks like.  This function achieves coverage calculation by mappipng the inputted read on to the interval scaled chromosomes, and reports a vector<float> to indicate the corresponding wig/bigwig value, together with its corresponding chromosome tile information of std:pair<chromosome, tile>
 */
	std::pair <std::string, uint32_t> GetCoverage (INPUT_TYPE in, uint32_t span, std::vector<float>& value)
	{
		uint32_t bed_start = in.start_;
		uint32_t bed_end = in.end_; 
		value.resize ((size_t) bed_end/span - (size_t) bed_start/span + 1 );
		value.front() = (float) ( span - (bed_start%span) ) / span;
		if (value.size() > 1)
		{
			value.back() = (float) (bed_end%span) / span;
			for (auto itr=value.begin()+1; itr!=value.end()-1; ++itr)
				*itr = 1.0;
			if (value.back()==0)
				value.resize (value.size()-1);
		}
		return {in.chromosome_, ((uint32_t) bed_start / span)};
	}
/**
 * @memberof AnnRawBed2Bwg
 * @brief A print function to print the items_ out
 */
	void PrintItem (void)
	{
		for (auto& obj0 : *items_)
		{
			std::cerr<<obj0.first<<'\n';
			for (auto& obj1 : obj0.second)
				std::cerr<<obj1.first<<":"<<obj1.second<<'\t';
			std::cerr<<'\n';
		}
	}
/**
 * @memberof AnnRawBed2Bwg
 * @brief A getter function to access the items_
 */
	ItemType GetItem (void)
	{
		return items_;
	}
};

#endif
