/**
 *  @file sam2bam_up.hpp
 *  @brief Last pipeline for converting sam to bam
 *  @author C-Salt Corp.
 */
#ifndef SAM2BAM_UP_HPP_
#define SAM2BAM_UP_HPP_
#include <sstream>
#include "../../pipeline/htslib/htslib/sam.h"
#include "../thread_pool_update.hpp"
#include "../format/sam.hpp"
#include "../format/annotation_raw_bed.hpp"
/**
 * @struct Sam2BamUp
 * @brief used to do sam to bam conversion by calling htslib of samtool
 * @tparam INPUT_TYPE designed with the type of AnnotationRawBed<>
 */
template< class INPUT_TYPE=AnnotationRawBed <> >
class Sam2BamUp
{
private:
/**
 * @memberof Sam2BamUp
 * @brief provide two pipes for standard input & standard output.  Element 0 is set up and opened for reading, while element 1 is set up and opened for writing.\nVisually speaking, the output of fd1 becomes the input for fd0, and all data traveling through the pipe moves through the kernel.
 */
	int fd[2];
/**
 * @memberof Sam2BamUp
 * @brief keep track of bam header content
 */
	bam_hdr_t *bam_header;
/**
 * @memberof Sam2BamUp
 * @brief member keep track of the block length of the header block.
 */
	int hdr_block_length_;
/**
 * @memberof Sam2BamUp
 * @brief the actual file handle written by fd[1]
 */
	htsFile *out;
/**
 * @memberof Sam2BamUp
 * @brief member for holding bam content
 */
	bam1_t *bam;
/**
 * @memberof Sam2BamUp
 * @brief member indicating the bam file length 
 */
	int bam_length_;
/**
 * @static
 * @memberof Sam2BamUp
 * @brief member keeping track of the content of the 27 byte of ending blocks
 */
	static const uint8_t gBamStopBlock[28];
/**
 * @memberof Sam2BamUp
 * @brief member keeping track of the sam header content
 */
	std::string header_str_;
/**
 * @memberof Sam2BamUp
 * @brief member reading data from pipe fd[0] in a threading manner
 */
	std::thread *pipe_thread_;
/**
 * @memberof Sam2BamUp
 * @brief member keeping track of the length of ofss_, i.e. the total length of this current BGZF compressed block
 */
	int ofss_length_;
/**
 * @memberof Sam2BamUp
 * @brief define a data structure to hold information needed for Bai file.  It can be intepreted as: \n map[ pair[chromosome, # of 16Kbp tile], tuple [min_uoffset, max_uoffset, annotationrawbed.start, annotationrawbed.end, min_coffset, max_coffset] ]
 */
	typedef std::map <std::pair <std::string, int>, std::tuple <int, int, int, int, int, int> > OffsetMapType;
/**
 * @memberof Sam2BamUp
 * @brief keep record of the uoffset value corresponding to a certain 16Kb tile, e.g. tile 1 with a uoffset value of 45; tile 5566 with a uoffset value of 7788, and so forth.
 */
	OffsetMapType offset_map_;
/**
 * @memberof Sam2BamUp
 * @brief keep record of to which tile current the inputed AnnotationRawBed<> is corresponded to.
 */
	int current_tile_;
/**
 * @memberof Sam2BamUp
 * @brief keep record of the file offset length corresponding to the present AnnotationRawBed<>
 */
	int max_uoffset_, min_uoffset_;

	std::string current_chr_;

public:
/**
 * @memberof Sam2BamUp
 * @brief member keeping track of the bam file content, its size is recorded by bam_length_
 */
	std::shared_ptr <std::stringstream> ofss_;

/**
 * @memberof Sam2BamUp
 * @brief member keeping track of the bam file name information
 */
	std::string output_name_;

/**
 * @memberof Sam2BamUp
 * @brief constructor, with input pipeline index, output file name, and sam header content 
 */
	Sam2BamUp(std::string file_name, std::string& bam_headeri)
		: output_name_ (file_name)
		, bam_length_ (0)
		, ofss_ (new std::stringstream() )
		, header_str_(bam_headeri)
		, current_tile_ (-1)
		, max_uoffset_ (0)
		, min_uoffset_ (0)
		, hdr_block_length_ (0)
		, current_chr_ ("5566")
		, max_coffset_(0)
		, min_coffset_(0)
	{
		int tmp = pipe(fd);
		init_htsfile();
		init_get_bam();
		set_bam_header(header_str_);
		bam = bam_init1();
	}

	void WriteBamHeader (void)
	{
		write_bam_header (header_str_);
	}

/**
 * @memberof Sam2BamUp
 * @brief function handling bam file writing based on the inputted content, in the format of std::string&&
 */
	void run(std::string&& in)
	{
		write_bam(in);
	}

/**
 * @memberof Sam2BamUp
 * @brief function handling bam file writing based on the inputted content, in the format of INPUT_TYPE, e.g. AnnotationRawBed <> 
 */
	void run(INPUT_TYPE in)
	{
		write_bam(std::forward<INPUT_TYPE>(in));
	}

/**
 * @memberof Sam2BamUp
 * @brief function for terminting pipes, sam, bam, and bam header files.  Future possibility for concatenating other bam file content has been preserved by not appending the tailing 27 bytes empty bgzf block 
 */
	std::tuple < std::pair<int, int>, std::map < std::pair <std::string, int>, std::tuple <int, int, int, int, int, int> > > // add min & max coffset due to auto created block
//	std::tuple < std::pair<int, int>, std::map < std::pair <std::string, int>, std::tuple <int, int, int, int> > >
	end_Sam2Bam(std::map<std::string, std::map<int, int> >& cross_bin_map)
	{
		sam_close(out);
		bam_destroy1(bam);
		bam_hdr_destroy(bam_header);
		pipe_thread_->join();
		delete pipe_thread_;
//PrintMap();
		cross_bin_map.swap (cross_board_bin_);
		return std::make_tuple (std::make_pair(hdr_block_length_, ofss_length_), offset_map_);
	}
/**
 * @memberof Sam2BamUp
 * @brief function for terminting pipes, sam, bam, and bam header files.  The tailing 27 bytes empty bgzf block is contatenated here to signify that the current bam file is closed for good, and the possiblity for future bam file concatenation is closed.
 */
	std::tuple < std::pair<int, int>, std::map < std::pair <std::string, int>, std::tuple <int, int, int, int, int, int> > >
	//std::tuple < std::pair<int, int>, std::map < std::pair <std::string, int>, std::tuple <int, int, int, int> > >
	final_Sam2Bam(std::map<std::string, std::map<int, int> >& cross_bin_map)
	{
		bgzf_flush( ((BGZF*)((out->fp))) );
		fwrite( gBamStopBlock, 1, 28, (FILE*) ((BGZF*)((out->fp)))->fp );
		return end_Sam2Bam(cross_bin_map);
	}

	void PrintMap (void)
	{
		std::cerr<<"hdr_block_length & offset_length "<<hdr_block_length_<<' '<<ofss_length_<<'\n';
		for (auto& item0: offset_map_)
		{
			std::cerr<<"chr : tile "<<item0.first.first<<"  "<<item0.first.second<<'\n';
			std::cerr<<"min_uof, max_uof, in_start, in.end "<<std::get<0>(item0.second)<<' '<<std::get<1>(item0.second)<<' '<<std::get<2>(item0.second)<<' '<<std::get<3>(item0.second)<<std::endl;
		}
	}

private:	
/**
 * @memberof Sam2BamUp
 * @brief threading function running the operation of reading data from pipe fd[0] to ofss_
 */
	inline void init_get_bam()
	{
		pipe_thread_ = new std::thread(
			[this]()
			{
				size_t len;
				char* buf = new char[8193];//4096];
				//keep acquiring data from fd[0], through buf, and eventually to ofss_ till the end of fd[0] reading operation
				while((len=read(this->fd[0], buf, 8192))>0)//4096))>0) 
				{
					this->bam_length_ += len;
					(*ofss_).write(buf,len);
				}
				//remove the tailing 27 bytes empty bgzf block from ofss_ to preserve the possiblity for future bam file concatenation
            	std::streamsize read_tg = ofss_->tellg();
            	std::streamsize read_tp = ofss_->tellp();
            	char* ka = new char [read_tp-read_tg];
            	ofss_->read(ka, read_tp-read_tg);
            	ofss_->str("");
            	ofss_->clear();
            	ofss_->seekp (0, std::ios::end);
            	ofss_->write (ka, read_tp-read_tg-28);                                                                                          
            	ofss_->seekp (0, std::ios::end);
            	ofss_->seekg (0);
				ofss_length_ = ofss_->tellp()-ofss_->tellg();
            	delete [] ka;
				close(this->fd[0]);
				delete[] buf;
			}
		);
	}
	
/**
 * @memberof Sam2BamUp
 * @brief function initialize the htsfile out, which is then bundled with pipe fd[1] for bgzf file writing operation
 */
	inline void init_htsfile()
	{
		char modew[8];
		strcpy(modew, "w");
		sprintf(modew + 1, "%d", 1);
		strcat(modew, "b");
		
		out = (htsFile*)calloc(1, sizeof(htsFile));
		out->fn = strdup("-");
		out->is_be = ed_is_big();
		out->is_bin = 1;
		out->fp = bgzf_dopen(fd[1], modew);
	}
	
/**
 * @memberof Sam2BamUp
 * @brief function initialize the bam header content based on the inputted sam header string
 */
	inline void set_bam_header(std::string &header_string)
	{
		bam_header = sam_hdr_parse(header_string.length(), header_string.c_str() );
		char * cp_header = (char*) malloc(header_string.length()+1);
		strcpy(cp_header, (char*) header_string.c_str());
		bam_header->l_text = header_string.length();
		bam_header->text = cp_header;
		bam_header->ignore_sam_err = 0;
	}

/**
 * @memberof Sam2BamUp
 * @brief function actually do sam header writing, which is executed only for the very first block of a bam file.
 */
	inline void write_bam_header(std::string &header_string)
	{
		sam_hdr_write(out, bam_header);
		hdr_block_length_ = ((BGZF*)(out->fp))->block_address;
	}

/**
 * @memberof Sam2BamUp
 * @brief function do bam writing based on inputted sam content, in type of std::string.  Caution!!!: no bai operation is conducted in this write_bam function since we are facing unsorted bam file in our current implementation 
 */
	inline void write_bam(std::string &sam_string)
	{
		kstring_t ks;
		ks.l = sam_string.length();//length of last cigar
		ks.s = (char*) sam_string.c_str();//bb should include last cigar
		ks.m = sam_string.length();
		kroundup32(ks.m);
		sam_parse1( &ks, bam_header, bam);
		sam_write1(out, bam_header, bam);
	}

    int reg2bin(int beg, int end)
    {                                                                                                                                                                                    
        --end;
        if (beg>>14 == end>>14) return ((1<<15)-1)/7 + (beg>>14);
        if (beg>>17 == end>>17) return ((1<<12)-1)/7 + (beg>>17);
        if (beg>>20 == end>>20) return ((1<<9)-1)/7 + (beg>>20);
        if (beg>>23 == end>>23) return ((1<<6)-1)/7 + (beg>>23);
        if (beg>>26 == end>>26) return ((1<<3)-1)/7 + (beg>>26);
        return 0;
    }                                                                    

	std::map <std::string, std::map <int, int> > cross_board_bin_;


    int max_coffset_, min_coffset_;	

/**
 * @memberof Sam2BamUp
 * @brief function do bam writing based on inputted sam content, in type of AnnotationRawBed<>.  This version is exclusive for sorted file, and some extra bai information calculation is included
 */
	inline void write_bam(INPUT_TYPE in)
	{
		std::string sam_string = in.ToSam();
		if(sam_string == "")
			return;
		std::vector<std::string> split_vec;
		boost::split (split_vec, sam_string, boost::is_any_of ("\n") );
		for (auto& sam_string : split_vec)
		{
			kstring_t ks;
			ks.l = sam_string.length();//length of last cigar
			ks.s = (char*) sam_string.c_str();//bb should include last cigar
			ks.m = sam_string.length();
			kroundup32(ks.m);
			sam_parse1(&ks, bam_header, bam);
			sam_write1(out, bam_header, bam);
		}

		int update_tile_end = in.end_/16384;
		int update_tile = in.start_/16384;	//the tile index corresponding to the current input AnnotationRawBed
		if (update_tile != update_tile_end)
			cross_board_bin_[in.chromosome_].insert ({reg2bin(in.start_, in.end_), 5566});

		max_uoffset_ = ((BGZF*)(out->fp))->block_offset;	
		max_coffset_ = ((BGZF*)(out->fp))->block_address;	
		//std::cerr<<"max_uoffset_value "<<max_uoffset_<<'\n';


		if (in.chromosome_ != current_chr_)	//representing the initial state of a chromosome // || in.chromosome_ == current_chr_)
		{
			current_tile_=-1;
		}
		if (update_tile > current_tile_) 
		{
			offset_map_[{in.chromosome_, update_tile}] = std::make_tuple (min_uoffset_, max_uoffset_, in.start_, in.end_, min_coffset_, max_coffset_);
			current_tile_ = update_tile;
		}
		min_uoffset_ = max_uoffset_;
		min_coffset_ = max_coffset_;
		current_chr_ = in.chromosome_;

		//hdr_block_length_ = ((BGZF*)(out->fp))->block_address;	//add this line#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	}
};

template<class INPUT_TYPE>
const uint8_t Sam2BamUp<INPUT_TYPE>::gBamStopBlock [28] = "\037\213\010\4\0\0\0\0\0\377\6\0\102\103\2\0\33\0\3\0\0\0\0\0\0\0\0";

#endif
