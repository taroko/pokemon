#include <tuple>
#include <stdint.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "gtest/gtest.h"
#include "../src/format/fastq_barcode.hpp"

TEST(FastqBarcode, default_constructor) 
{
	FastqBarcode <> FastqBarcode_test;

	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"");
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, copy_constructor)
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
	
	FastqBarcode<> k( TupleType( name1, sequence, name2, quality ) );
	FastqBarcode<> FastqBarcode_test(k);
	
	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, copy_assignment_constructor)
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
	
	FastqBarcode<> k( TupleType( name1, sequence, name2, quality ) );
	FastqBarcode<> FastqBarcode_test;
	FastqBarcode_test = k;
	
	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, move_data_constructor) 
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;

	FastqBarcode<> k( TupleType( name1, sequence, name2, quality ) );
	FastqBarcode<> FastqBarcode_test( std::move(k) );

	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, move_copy_assignment_operator) 
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple< std::string,std::string,std::string,std::string > TupleType;

	FastqBarcode<> k( TupleType(name1, sequence, name2, quality ) );
	FastqBarcode<> FastqBarcode_test;
	FastqBarcode_test = std::move(k) ;

	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, move_data_tuple_constructor) 
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple< std::string,std::string,std::string,std::string > TupleType;

	TupleType k( name1, sequence, name2, quality );
	FastqBarcode<> FastqBarcode_test( std::move(k) );

	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
}

TEST(FastqBarcode, tupletype_move_constructor)
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
	
	FastqBarcode<> FastqBarcode_test( TupleType( name1, sequence, name2, quality ) );
	
	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, tupletype_move_assignment)
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
	
	FastqBarcode<> FastqBarcode_test( TupleType( name1, sequence, name2, quality ) );
	FastqBarcode<> k = FastqBarcode_test;
	
	EXPECT_EQ (std::get<0> (FastqBarcode_test.data),"name1");
	EXPECT_EQ (std::get<1> (FastqBarcode_test.data),"sequence");
	EXPECT_EQ (std::get<2> (FastqBarcode_test.data),"name2");
	EXPECT_EQ (std::get<3> (FastqBarcode_test.data),"quality1");
	ASSERT_FALSE (FastqBarcode_test.eof_flag);
}

TEST(FastqBarcode, overload_operator)
{
	std::string name1("name1");
	std::string sequence("sequence");
	std::string name2("name2");
	std::string quality("quality1");

	typedef std::tuple< std::string, std::string, std::string, std::string > TupleType;

	FastqBarcode<> k( TupleType( name1, sequence, name2, quality ) );
	FastqBarcode<> FastqBarcode_test( std::move(k) );

//	std::cout << FastqBarcode_test;
}

TEST(FastqBarcode, serialization)
{
	std::string name1("name1");
	std::string sequence("sequence");
	std::string name2("name2");
	std::string quality("quality1");
	typedef std::tuple< std::string, std::string, std::string, std::string > TupleType;

	FastqBarcode<> k( TupleType( name1, sequence, name2, quality ) );
	FastqBarcode<> FastqBarcode_test( std::move(k) );
	FastqBarcode<> FastqBarcode_result;
std::cerr<<"b"<<std::endl;
	std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/fastq_barcode_serialization.txt");
std::cerr<<"c"<<std::endl;
	boost::archive::text_oarchive arc0 (ofs);
std::cerr<<"d"<<std::endl;
	arc0 & FastqBarcode_test;
	ofs.close();
	std::string str_temp;
std::cerr<<"e"<<std::endl;
	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/fastq_barcode_serialization.txt");
std::cerr<<"f"<<std::endl;
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & FastqBarcode_result;
	EXPECT_EQ (std::get<0>(FastqBarcode_result.data), "name1");
	EXPECT_EQ (std::get<1>(FastqBarcode_result.data), "sequence");
	EXPECT_EQ (std::get<2>(FastqBarcode_result.data), "name2");
	EXPECT_EQ (std::get<3>(FastqBarcode_result.data), "quality1");
	ifs.close();
}
#include "../src/tuple_utility.hpp"

TEST (FastqBarcode, get_data_length)
{
	std::string name1("name1");
	std::string sequence("name2");
	std::string name2("name3");
	std::string quality("name4");
	typedef std::tuple< std::string, std::string, std::string, std::string > TupleType;
	FastqBarcode<> k ( TupleType( name1, sequence, name2, quality ) );
	std::cerr<<k.get_data_length()<<std::endl;
}

TEST(FastqBarcode, static_assert)
{  
//	FastqBarcode <int> gg;
//	std::string s1 ("fucker");
//	FastqBarcode <> aa = s1;
//	FastqBarcode <> bb (s1);
}   
