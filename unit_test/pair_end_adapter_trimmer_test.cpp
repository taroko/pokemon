#include "../src/trimmer/pair_end_adapter_trimmer.hpp"//PeatFree_0703.hpp"
#include "gtest/gtest.h"
#include "../src/file_reader.hpp"
#include <sstream>
#include "gtest/gtest.h"
TEST (PEAT, TEST)
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	std::vector<std::string> read_vec ({
					"/mnt/godzilla/johnny/PEAT/million_0/origin0_1.fq",//"/Users/obigbando/Documents/work/test_file/test_1.fq",
					"/mnt/godzilla/johnny/PEAT/million_0/origin0_2.fq"//"/Users/obigbando/Documents/work/test_file/test_2.fq"
					});
	std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc, cca;

	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
			QQA (read_vec, &cca);   
	QQA.Read (100000);

	FileReader < ParallelTypes::M_T, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
			QQ (read_vec, &ccc);    
	QQ.Read (100000);

	ParameterTrait <> i_parameter_trait;

	PairEndAdapterTrimmer <
							ParallelTypes::M_T,
							Fastq, 
							TUPLETYPE,
							TrimTrait<std::string, LinearStrMatch<double>, double, double > 
							> 
					QQP (//&ccc, 
						i_parameter_trait);

	QQP.Trim (&ccc);

	std::vector<size_t> trim_pos;
	for (auto ii=0; ii!=(*QQP.trim_position).size(); ++ii)
		for (auto jj=0; jj!=(*QQP.trim_position)[ii].size(); ++jj)
			trim_pos.push_back ( (*QQP.trim_position)[ii][jj] );

	for (auto ii=0; ii!=2; ++ii)
		for ( auto jj=0; jj!=ccc[0].size(); ++jj)
		{
			if (trim_pos[jj]!=0)
			{
					std::get<1>(cca[ii][jj].data).resize (trim_pos[jj]);
					std::get<3>(cca[ii][jj].data).resize (trim_pos[jj]);
			}
			EXPECT_EQ ( std::get<0>(cca[ii][jj].data), std::get<0>(ccc[ii][jj].data) ); 
			EXPECT_EQ ( std::get<1>(cca[ii][jj].data), std::get<1>(ccc[ii][jj].data) ); 
			EXPECT_EQ ( std::get<2>(cca[ii][jj].data), std::get<2>(ccc[ii][jj].data) ); 
			EXPECT_EQ ( std::get<3>(cca[ii][jj].data), std::get<3>(ccc[ii][jj].data) ); 
		}
}
//#include "../src/pair_end_adapter_trimmer_parameter_current_07.hpp"
/*
int main (int argc, char* argv[])
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	json_handler<curl_default_handle> jh (argv[1], "test_1_0703");
	jh.print();
	auto index=0;
	{
		std::cerr<<"init run # "<<index<<'\n';
		std::vector <std::string> upload_vec0 ({
				jh.upload_url_vec_[index][0]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
				,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
				, "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
				, "https://api.basespace.illumina.com/"
				});

		std::vector <std::string> upload_vec1 ({
				jh.upload_url_vec_[index][1]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
				,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
				, "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
				, "https://api.basespace.illumina.com/"
				});

		std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc;
		std::map<int, std::vector< size_t > > ttt;

		size_t startsize_in;
		std::stringstream ss (argv[2]);
		double m_indicator, a_mismatch, g_mismatch;
		char comma1, comma2, comma3;
		ss >> m_indicator >> comma1 >> a_mismatch >> comma2 >> g_mismatch >> comma3 >> startsize_in;

		ParameterTrait i_parameter_trait ( startsize_in );//, num_in, pool_size_in );

		PEAT_CLOUD < FileReaderComponent, PeatComponent	> QQ 
				(  //vec1, vec2, 
				  jh.url_vec_[index],   jh.size_vec_[index], //file_vec, 
				  upload_vec0, upload_vec1,
				  &ccc, &ttt, //RQ, WQ, 
				  i_parameter_trait, m_indicator, a_mismatch, g_mismatch);
		size_t sum = 0, rd_count = 0;
		int i = 0;

		std::cerr<<"current buf size "<<QQ.Curl_device_[0]->thread_recv_volume_<<'\t'<<QQ.Curl_device_[1]->thread_recv_volume_<<std::endl;
		while (true)//(i<2)// (true)
		{
			std::cout<<"round # "<<i<<'\n';
			if (!QQ.Parse_N_Trim_all (3))
				break;
			++i;
		}
		std::cerr<<"done PNTA # "<<index<<std::endl;
		if ( index==jh.url_vec_.size() )
			std::cerr<<"fake termination "<<'\n';
		//	  QQ.terminate ("http://www.jhhlab.tw/basespace/query_task_PEAT_CLOUD/status_finish/");
	}
	std::cerr<<"done of all "<<std::endl;
}																																									  
*/

//int main (int argc, char* argv[])
//{
//	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
//	json_handler<curl_default_handle> jh (argv[1], "test_1_0703");
//	jh.print();
//
//
//    for (auto index=0; index!=jh.url_vec_.size(); ++index)
//	//auto index=0;
//	{
//		std::cerr<<"init run # "<<index<<'\n';
//		std::vector <std::string> upload_vec0 ({
//				jh.upload_url_vec_[index][0]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
//				,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
//				, "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
//				, "https://api.basespace.illumina.com/"
//				});
//
//		std::vector <std::string> upload_vec1 ({
//				jh.upload_url_vec_[index][1]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
//				,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
//				, "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
//				, "https://api.basespace.illumina.com/"
//				});
//
//		std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc;
//		std::map<int, std::vector< size_t > > ttt;
//
//		size_t startsize_in;
//		std::stringstream ss (argv[2]);
//		double m_indicator, a_mismatch, g_mismatch;
//		char comma1, comma2, comma3;
//		ss >> m_indicator >> comma1 >> a_mismatch >> comma2 >> g_mismatch >> comma3 >> startsize_in;
//
//		ParameterTrait i_parameter_trait ( startsize_in );//, num_in, pool_size_in );
//
//		PEAT_CLOUD_MT < FileReaderComponentMT, PeatComponentMT	> QQ 
//			( jh.url_vec_[index],   jh.size_vec_[index], 
//			  upload_vec0, upload_vec1,
//			  &ccc, &ttt, 
//			  i_parameter_trait, m_indicator, a_mismatch, g_mismatch);
//		size_t sum = 0, rd_count = 0;
//		int i = 0;
//
//		std::cerr<<"current buf size "<<QQ.Curl_device_[0]->thread_recv_volume_<<'\t'<<QQ.Curl_device_[1]->thread_recv_volume_<<std::endl;
//		while (true)//(i<2)// (true)
//		{
//			std::cout<<"round # "<<i<<'\n';
//			if (!QQ.Parse_N_Trim_all (3))
//				break;
//			++i;
//		}
//		std::cerr<<"done PNTA # "<<index<<std::endl;
//		if ( index==jh.url_vec_.size() )
//		{
//			std::cerr<<"fake termination "<<'\n';
//        //      QQ.terminate ("http://www.jhhlab.tw/basespace/query_task_peat/status_finish/");
//		}
//	}
//	std::cerr<<"done of all "<<std::endl;
//}																																									  
//

