//#include "../src/pair_end_adapter_trimmer_parameter.hpp"
#include "../src/pair_end_adapter_trimmer_parameter_current_06.hpp"
#include "../src/format/json_handler.hpp"
//#include "../src/format/curl_send.hpp"
#include <sstream>

int main (int argc, char* argv[])
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	json_handler<curl_default_handle> jh (argv[1], "test_1_0624");
	jh.print();
	//	for (auto index=0; index!=jh.url_vec_.size(); ++index)
	auto index=0;
	{
		std::cerr<<"init run # "<<index<<'\n';
		//	std::vector <std::string> vec1  
		//		({
		//	"https://api.basespace.illumina.com/v1pre3/files/113647546/content?access_token=4b2bf2c94ea34721ac01fc177111707f",
		//	"https://api.basespace.illumina.com/v1pre3/files/113645006/content?access_token=4b2bf2c94ea34721ac01fc177111707f"
		////	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
		////	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
		////	"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
		////	"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
		//		});  
		//	std::vector <size_t> vec2 
		//		({
		//		18632783,
		//		20187631
		//		//7493990,
		//		//7493990
		//		//459083647,
		//		//478161401
		//		});
		std::vector <std::string> upload_vec0 ({
				jh.upload_url_vec_[index][0]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
				,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
				//, "Content-Type: application/octet-stream"//
				, "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
				, "https://api.basespace.illumina.com/"
				});

		std::vector <std::string> upload_vec1 ({
				jh.upload_url_vec_[index][1]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
				,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
				//, "Content-Type: application/octet-stream" //
				, "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
				, "https://api.basespace.illumina.com/"
				});

		//	std::vector<std::ifstream*> RQ;
		//	std::vector<std::string> file_vec ({
		//		argv[2],//"/nfs_sharing/oman/orig1.fastq",
		//		argv[3]//"/nfs_sharing/oman/orig2.fastq"
		//		});
		//	std::for_each (read_vec.begin(), read_vec.end(),
		//		[&] (const std::string& q)
		//		{ RQ.push_back (new std::ifstream (q) ); });
		//	std::vector<std::ofstream*> WQ;
		//	std::vector < std::string > write_vec ({
		//	"qq1",//argv[1],//"/dev/shm/omanrealout1.txt" ,
		//	"qq2"//argv[2]//"/dev/shm/omanrealout2.txt" 
		//		});
		//	std::for_each (write_vec.begin(), write_vec.end(),
		//		[&] (const std::string& q)
		//		{ WQ.push_back (new std::ofstream (q) ); });

		std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc;
		std::map<int, std::vector< size_t > > ttt;

		size_t startsize_in;
		std::stringstream ss (argv[2]);
		double m_indicator, a_mismatch, g_mismatch;
		char comma1, comma2, comma3;
		ss >> m_indicator >> comma1 >> a_mismatch >> comma2 >> g_mismatch >> comma3 >> startsize_in;

		ParameterTrait i_parameter_trait ( startsize_in );//, num_in, pool_size_in );


		PairEndAdaptiveTrimmer <ParallelTypes::NORMAL, 
							   Fastq, 
							   TUPLETYPE,
							   SOURCE_TYPE::CURL_TYPE_GZ, 
							   curl_default_handle,   
							   TrimTrait<std::string, LinearStrMatch<double> >, 
							   double,
							   double > QQ 
								   (  //vec1, vec2, 
									  jh.url_vec_[index],	jh.size_vec_[index], //file_vec, 
									  upload_vec0, upload_vec1,
									  &ccc, &ttt, //RQ, WQ, 
									  i_parameter_trait, m_indicator, a_mismatch, g_mismatch);
		size_t sum = 0, rd_count = 0;
		int i = 0;

		std::cerr<<"current buf size "<<QQ.Curl_device_[0]->thread_recv_volume_<<'\t'<<QQ.Curl_device_[1]->thread_recv_volume_<<std::endl;
		while (true)//(i<2)// (true)
		{
			std::cout<<"round # "<<i<<'\n';
			if (!QQ.Parse_N_Trim_all (3))
				break;
			++i;
		}
		//	for (auto gq : RQ)
		//	{
		//		gq->close();
		//		delete gq;
		//	}
		//	for (auto gq : WQ)
		//	{
		//		gq->close();
		//		delete gq;
		//	}
		std::cerr<<"done PNTA # "<<index<<std::endl;
		if ( index==jh.url_vec_.size() )
			std::cerr<<"fake termination "<<'\n';
		//		QQ.terminate ("http://www.jhhlab.tw/basespace/query_task_peat/status_finish/");
	}


	std::cerr<<"done of all "<<std::endl;
}
