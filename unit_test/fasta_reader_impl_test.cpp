#include <tuple>
#include "gtest/gtest.h"
#include "../src/format_io_iterator.hpp"
#include "../src/format/fasta_reader_impl.hpp"
#include "../src/format/fasta.hpp"
#include "file_generator.hpp"
#include <fstream>

TEST(Fasta_reader, io_end)
{
    FileReader_impl <Fasta, std::tuple <std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE > Fasta_Reader_test;
    EXPECT_EQ( ( (Fasta_Reader_test.io_end() ) -> eof_flag ), true );
};

TEST(Fasta_reader, get_next_entry)
{
	std::vector < std::string > qq ({ "test.fa", "test.fa", "test.fa" });
	FileReader_impl <Fasta , std::tuple <std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE > Fasta_Reader_test (qq);//(fg.temp_file_name);

	std::cerr<<"file_num_  "<<Fasta_Reader_test.file_num_<<'\n';
	for (auto ii=0; ii!=Fasta_Reader_test.file_num_; ++ii)
	{
		std::cerr<<"current index "<<ii<<'\n';
		while (true)
		{
			Fasta < std::tuple <std::string, std::string > > object
				= Fasta_Reader_test.get_next_entry (ii);
			bool flag = true;
			if ( object.eof_flag )
				break;
			std::cerr<<"result "<<object<<'\n';
		}
	}
};

/*
TEST(Fasta_reader, get_all_entry)
{
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
	RandomFileGenerator fg;
	fg.GenRandFile< Fasta<> >();
	std::vector < std::string > qq ( {fg.temp_file_name });
	FileReader_impl <Fasta , std::tuple < std::string, std::string, std::string, std::string >, SOURCE_TYPE::IFSTREAM_TYPE > 
		Fasta_Reader_test (qq);
//	std::string file_name = fg.temp_file_name;
	std::vector < std::vector < Fasta <TUPLETYPE> > > result;
	//FileReader_impl <Fasta, TUPLETYPE> :: 
	Fasta_Reader_test.get_all_entry (result);//(file_name, result);
	std::for_each ( result.begin(), result.end(), 
		[] ( std::vector < Fasta<TUPLETYPE> > qq )
		{ std::for_each ( qq.begin(), qq.end(),
			[] ( Fasta<TUPLETYPE> q )
			{	q4 << q <<'\n'; }); });
//	EXPECT_EQ(std::get<0> (result.front().front().data), fg.GetContent(0) );
//	EXPECT_EQ(std::get<1> (result.front().front().data), fg.GetContent(1) );
//	EXPECT_EQ(std::get<2> (result.front().front().data), fg.GetContent(2) );
//	EXPECT_EQ(std::get<3> (result.front().front().data), fg.GetContent(3) );
}
*/


// curl_version not fully tested yet

TEST(Fasta_reader_url_two_parameters, io_end)
{
	std::vector <std::string> vec1 ({"140.113.15.99/aa.fa", "140.113.15.99/aa.fa"});
//  	std::vector <size_t> vec2 ({7493990});
    FileReader_impl <Fasta, std::tuple <std::string, std::string, std::string, std::string >, SOURCE_TYPE::CURL_TYPE_PLAIN, curl_default_handle_mutex > Fasta_Reader_test (vec1);//, vec2);
//    EXPECT_EQ( ( (Fasta_Reader_test.io_end() ) -> eof_flag ), true );
};
/*
TEST(Fasta_reader_url_two_parameters, get_next_entry)
{
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
	std::vector <std::string> vec1 ({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
	});
  	std::vector <size_t> vec2 ({7493990, 7493990});
	FileReader_impl <Fasta , std::tuple < std::string, std::string, std::string, std::string >, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle > Fasta_Reader_test (vec1, vec2);

	while ( true )
	{
		std::vector < Fasta<TUPLETYPE> > object = Fasta_Reader_test.get_next_entry ();//(file_handle);
		bool flag = true;
		for ( auto i = 0; i!=object.size(); ++i )
		//for ( auto i : object ) //use copy constructor, rather than actually iterating through each of the element !!!!!
			flag *= object[i].eof_flag;
		if (flag)
			break;
		q1<<object[0];
		q3<<object[1];

	}
std::cerr<<"done url two parameter next entry test"<<std::endl;
};

TEST(Fasta_reader_url_two_parameters, get_all_entry)
{
	std::vector <std::string> vec1 ({"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"});
  	std::vector <size_t> vec2 ({7493990});
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
	FileReader_impl <Fasta , std::tuple < std::string, std::string, std::string, std::string >, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle > Fasta_Reader_test (vec1, vec2);

	std::vector < std::vector < Fasta <TUPLETYPE> > > result;
	Fasta_Reader_test.get_all_entry (result);//(file_name, result);

	for ( auto i : result )
	{
		for ( auto j : i )
			q2 << j;
	}

std::cerr<<"done url two parameter all entry test"<<std::endl;
}
*/
