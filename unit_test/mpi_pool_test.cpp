#include <queue>
#include <vector>
#include <map>
#include <string>
#include <tuple>
#include <fstream>
#include <iostream>
#include "boost/serialization/utility.hpp"
#include "boost/mpi.hpp"
#include "boost/bind.hpp"
#include "boost/ref.hpp"
#include "gtest/gtest.h"
#include "../src/mpi_job_creator.hpp"
#include "../src/format/fasta.hpp"
#include "../src/constant_def.hpp"
#include "../src/tuple_utility.hpp"
#include "../src/mpi_pool.hpp"
#include "../src/file_reader.hpp"
//#include "../src/format/wig.hpp"
//#include "test_all.cpp"
Fasta<std::tuple < std::string, std::string> > get_next_entry (std::string name)//fifstream& file_handle)
{
	std::ifstream file_handle;
	file_handle.open (name);
	std::string data, line, head;
	int is_head=1; //a flag indicating whether is the head line.
	Fasta< std::tuple <std::string, std::string> > result;
	if (file_handle.eof() || file_handle.fail() || file_handle.bad())
	{
	result.eof_flag = true;
	return result;
	}

	while(std::getline(file_handle, line))
	{
	if(line[0] == '>') //the head line
	{
		if(is_head != 0) //confirm it's the first tuple
		{
			head = line.substr(1); //get the head without '>'
			is_head = 0;
		}
		else
		{
			file_handle.seekg(static_cast<long long int>(file_handle.tellg()) - static_cast<long long int>(line.length()) - 1); //move the pointer to the right position to the end of this tuple.
			break;
		}

	}
	else
	{
		data += line; //sequence data
	}
	}

	result.data = std::make_tuple (head, data);
	return result;
};

class ReadTest
{
public:
	typedef std::tuple < std::string, std::string> TUPLETYPE;
	Fasta <TUPLETYPE> result;
	std::string file_name;
	ReadTest ()  : result ( std::tuple < std::string, std::string > ("s1", "s2") ) {};
	ReadTest (std::string s1) : result(), file_name (s1) {};
	void operator() (void)
	{	
		result = get_next_entry (file_name);
		std::ifstream file_in;
		file_in.open (file_name);
		file_in.close();
	}
	template<class Archive>
	void serialize (Archive &ar, const unsigned int)
	{
		ar & result; 
	}
};

class adder
{
public:
	int a, b, result;
	adder () {}
	adder (int A, int B) : a (A), b (B), result (0) {}
	void operator() (void)
	{
		result = a+b;
	}	
	template<class Archive>
	void serialize (Archive &ar, const unsigned int)
	{
		ar & result; 
	}
};

//TEST (MPI, TEST)
int main (int argc, char*argv[])
{
	MPIPool MP (argc, argv);//my_argc & my_argv are defined in test_all.cpp
	std::string s1 = "/Users/obigbando/Documents/work/test_file/mpi_test2.txt";
	ReadTest gg (s1);
	adder tt (3, 5);

//test1
	auto xx = MP.JobPost ( tt );
	auto yy = MP.JobPost ( gg );	
	auto zz = MP.JobPost ( tt );	
	auto ww = MP.JobPost ( tt );	
	auto uu = MP.JobPost ( gg );	
		MP.FlushPool();
	if (MP.world.rank() == 0 )
	{
		ReadTest outy, outu;	
		adder outx, outz, outw;
		*xx>>outx;
		*yy>>outy;
		*zz>>outz;
		*ww>>outw;
		*uu>>outu;
		std::cerr<<"test 1"<<std::endl;
		std::cerr<<"obtained outx\t"<<'\t'<<outx.result<<std::endl;
		std::cerr<<"obtained outy\t"<<'\t'<<outy.result<<std::endl;
		std::cerr<<"obtained outz\t"<<'\t'<<outz.result<<std::endl;
		std::cerr<<"obtained outw\t"<<'\t'<<outw.result<<std::endl;
		std::cerr<<"obtained outu\t"<<'\t'<<outu.result<<std::endl;
		std::cerr<<"aloha"<<std::endl;
	}

//test2
	auto x1 = MP.JobPost ( tt );
	auto y1 = MP.JobPost ( gg );	
	auto z1 = MP.JobPost ( tt );	
	auto w1 = MP.JobPost ( tt );	
	auto u1 = MP.JobPost ( gg );	
		MP.FlushPool();
	if (MP.world.rank() == 0 )
	{
		ReadTest outy1, outu1;	
		adder outx1, outz1, outw1;
		*x1>>outx1;
		*y1>>outy1;
		*z1>>outz1;
		*w1>>outw1;
		*u1>>outu1;
		std::cerr<<"test 2"<<std::endl;
		std::cerr<<"obtained outx1\t"<<'\t'<<outx1.result<<std::endl;
		std::cerr<<"obtained outy1\t"<<'\t'<<outy1.result<<std::endl;
		std::cerr<<"obtained outz1\t"<<'\t'<<outz1.result<<std::endl;
		std::cerr<<"obtained outw1\t"<<'\t'<<outw1.result<<std::endl;
		std::cerr<<"obtained outu1\t"<<'\t'<<outu1.result<<std::endl;
		std::cerr<<"aloha"<<std::endl;
	}

//test3
	boost::shared_ptr <boost::mpi::packed_iarchive> arc_ptr10 ( new boost::mpi::packed_iarchive (MP.world) );
	boost::shared_ptr <boost::mpi::packed_iarchive> arc_ptr11 ( new boost::mpi::packed_iarchive (MP.world) );
	boost::shared_ptr <boost::mpi::packed_iarchive> arc_ptr12 ( new boost::mpi::packed_iarchive (MP.world) );
	boost::shared_ptr <boost::mpi::packed_iarchive> arc_ptr13 ( new boost::mpi::packed_iarchive (MP.world) );
	boost::shared_ptr <boost::mpi::packed_iarchive> arc_ptr14 ( new boost::mpi::packed_iarchive (MP.world) );

	MP.JobPost<adder> ( tt , arc_ptr10 );
	MP.JobPost ( gg , arc_ptr11 );	
	MP.JobPost ( tt , arc_ptr12 );	
	MP.JobPost ( tt , arc_ptr13 );
	MP.JobPost ( gg , arc_ptr14 );	
	MP.FlushPool();
	if (MP.world.rank() == 0 )
	{
		ReadTest outy1, outu1;	
		adder outx1, outz1, outw1;
		*arc_ptr10>>outx1;
		*arc_ptr11>>outy1;
		*arc_ptr12>>outz1;
		*arc_ptr13>>outw1;
		*arc_ptr14>>outu1;
		std::cerr<<"test 3"<<std::endl;
		std::cerr<<"obtained outx1\t"<<'\t'<<outx1.result<<std::endl;
		std::cerr<<"obtained outy1\t"<<'\t'<<outy1.result<<std::endl;
		std::cerr<<"obtained outz1\t"<<'\t'<<outz1.result<<std::endl;
		std::cerr<<"obtained outw1\t"<<'\t'<<outw1.result<<std::endl;
		std::cerr<<"obtained outu1\t"<<'\t'<<outu1.result<<std::endl;
		std::cerr<<"aloha"<<std::endl;
	}

//test4
	std::string name1 ("/Users/obigbando/Documents/work/test_file/test.fasta");
	typedef std::tuple <std::string, std::string> FASTA_TUPLE;
	std::vector < Fasta<FASTA_TUPLE> > result1;
	auto w = JobSerializer<void> :: create ( 
			std::bind (
				&FileReader_impl<Fasta, FASTA_TUPLE> :: get_all_entry, 
				std::placeholders::_1, 
				std::placeholders::_2
						), 
			std::ref(name1), 
			std::ref(result1) 
						);
	std::string name2 ("/Users/obigbando/Documents/work/test_file/test.fastq");
	typedef std::tuple < std::string, std::string, std::string, std::string > FASTQ_TUPLE;
	std::vector < Fastq<FASTQ_TUPLE> > result2;
	auto w2 = JobSerializer<void>:: create ( 
			std::bind (
				&FileReader_impl<Fastq, FASTQ_TUPLE>:: get_all_entry, 
				std::placeholders::_1, 
				std::placeholders::_2
						), 
			std::ref(name2), 
			std::ref(result2)
					 	);

	auto warchive = MP.JobPost ( w );
	auto yy4 = MP.JobPost ( gg );
	auto zz4 = MP.JobPost ( tt );
	auto ww4 = MP.JobPost ( tt );	
	auto w2archive = MP.JobPost ( w2 );

	MP.FlushPool();

	if (MP.world.rank() == 0 )
	{
		ReadTest yy4r;
		adder zz4r, ww4r;
		auto w1r = JobSerializer<void>:: create ( 
			std::bind (
				&FileReader_impl<Fasta, FASTA_TUPLE> :: get_all_entry, 
				std::placeholders::_1, 
				std::placeholders::_2
						), 
			std::ref(name1), 
			std::ref(result1) 
						);

		auto w2r = JobSerializer<void>:: create ( 
			std::bind (
				&FileReader_impl<Fastq, FASTQ_TUPLE> :: get_all_entry, 
				std::placeholders::_1, 
				std::placeholders::_2 
						), 
			std::ref(name2), 
			std::ref(result2) 
						);
		*warchive>>w1r;
		*yy4>>yy4r;
		*zz4>>zz4r;
		*ww4>>ww4r;
		*w2archive>>w2r;
		std::cerr<<"test 4"<<std::endl;
		std::cerr<<"de-serialized string\t"<<std::get<0>(w1r.p_).get()<<std::endl;
		std::cerr<<"de-serialized content"<<std::endl;	
		for (auto itr = std::get<1>(w1r.p_).get().begin(); itr!= std::get<1>(w1r.p_).get().end(); ++itr)
			std::cerr<<*itr<<std::endl;	
		std::cerr<<"obtained outy\t"<<'\t'<<yy4r.result<<std::endl;
		std::cerr<<"obtained outz\t"<<'\t'<<zz4r.result<<std::endl;
		std::cerr<<"obtained outw\t"<<'\t'<<ww4r.result<<std::endl;
		std::cerr<<"aloha"<<std::endl;
		std::cerr<<"de-serialized string\t"<<std::get<0>(w2r.p_).get()<<std::endl;
		std::cerr<<"de-serialized content"<<std::endl;	
		for (auto itr = std::get<1>(w2r.p_).get().begin(); itr!= std::get<1>(w2r.p_).get().end(); ++itr)
			std::cerr<<*itr<<std::endl;	
	}					
};

