#include <bitset>
#include <iostream>
#include <deque>
#include <string>
#include <algorithm>
#include <fstream>
#include "boost/dynamic_bitset.hpp"
#include "boost/utility/binary.hpp"
#include "boost/lexical_cast.hpp"
#include "../src/compression/abit.hpp"
#include "../src/abwt_old.hpp"
#include "gtest/gtest.h"
#include "../src/thread_pool.hpp"
//#include "../src/parser.hpp"

/*
TEST (abit, test_operator_correct)
{
	std::ifstream chrY("/dev/shm/human/chrY.fa");
	std::string seq(""), tmp("");
	std::getline(chrY,tmp);
	while(std::getline(chrY,tmp))
		seq += tmp;
		
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, false); //sequence, ignore lower
	z.Printer();
	bool test(true);
	
	std::cout << "Test forward..." << std::endl;
	for(uint32_t i =0; i != z.size(); ++i)
	{
		if(seq[i] != z[i])
		{
			test=false;
			std::cout << i << seq[i] << z[i] << std::endl;
			break;
		}
	}
	if(test) std::cout << "No Error!" << std::endl;
	
	//Test for random
	std::cout << "Test random..." << std::endl;
	uint32_t p(0);
	for(uint32_t i =0; i != z.size(); ++i)
	{
		p = (std::rand() % z.size());
		if(seq[p] != z[p])
		{
			test=false;
			std::cout << p << seq[p] << z[p] << std::endl;
			break;
		}
	}
	if(test) std::cout << "No Error!" << std::endl;
	//z.test();
//	EXPECT_EQ (z.MakeSeqString(), s1);
}
*/
/*
TEST (abit, test_random_sort_speed)
{
	
	std::ifstream chrY("/dev/shm/human/chrY.fa");
	std::string seq(""), tmp("");
	std::getline(chrY,tmp);
	while(std::getline(chrY,tmp))
		seq += tmp;
		
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, false); //sequence, ignore lower
	
	bool test(true);
	
	ABWT b (z);
	b.setRandomTableSize(50000);
	b.BuildingIndex();
	b.sortRandomTable();

	//b.printRT();
	//EXPECT_EQ (z.MakeSeqString(), s1);
}
*/
/*
TEST (abit, test_mt)
{
	
	std::ifstream chrY("/dev/shm/human/chrY.fa");
	std::string seq(""), tmp("");
	std::getline(chrY,tmp);
	while(std::getline(chrY,tmp))
		seq += tmp;
		
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, false); //sequence, ignore lower
	
	ABWT b (z);
	b.mtSort(50);
	
	//b.sortRandomTable();

	//b.printRT();
	//EXPECT_EQ (z.MakeSeqString(), s1);
}
*/
/*
TEST (abit, test_sort_correct)
{
	
	std::ifstream chrY("/dev/shm/human/chrY.fa");
	std::string seq(""), tmp("");
	std::getline(chrY,tmp);
	std::string s2 ("NNNNNNNNNNNNAA");
	
	while(std::getline(chrY,tmp))
		seq += tmp;
		
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, false); //sequence, ignore lower
	
	bool test(true);
	
	ABWT b (z);
	b.setRandomTableSize(10);
	b.BuildingIndex();
	b.sortRandomTable();
	b.printRT();
	std::cout << "\n";
	b.sortRandomTable2();
	b.printRT();
	//b.printRT();
	//	EXPECT_EQ (z.MakeSeqString(), s1);
}
*/
/*
TEST (indexer, test_mt_XY)   //whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{
	std::vector<std::string> file_names (
			{
					"/dev/shm/human/chrX.fa",
					"/dev/shm/human/chrY.fa"
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	
	std::string seq(""), tmp("");
			
	for (int i=0; i<file_names.size(); i++)
	{
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
			seq += tmp;
		(*file_handles[i]).close();
	}
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, true); //sequence, ignore lower

	//EXPECT_EQ (z.MakeSeqString(), seq);
	//z.Printer();
	ABWT b (z);
	
	b.mtSort(60);

	
}
*/

TEST (indexer, test_mt_all)   //whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{
	std::vector<std::string> file_names (
			{
			    
					/*
					"/dev/shm/human/chr1.fa"
					
					"/dev/shm/human/chr2.fa",
					"/dev/shm/human/chr3.fa",
					"/dev/shm/human/chr4.fa",
					"/dev/shm/human/chr5.fa",
					
					"/dev/shm/human/chr6.fa",
					"/dev/shm/human/chr7.fa",
					"/dev/shm/human/chr8.fa",
					"/dev/shm/human/chr9.fa",
					"/dev/shm/human/chr10.fa",
					
					"/dev/shm/human/chr11.fa",
					"/dev/shm/human/chr12.fa",
					"/dev/shm/human/chr13.fa",
					"/dev/shm/human/chr14.fa",
					"/dev/shm/human/chr15.fa",
					
					"/dev/shm/human/chr16.fa",
					
					"/dev/shm/human/chr17.fa",
					
					"/dev/shm/human/chr18.fa",
					"/dev/shm/human/chr19.fa",
					"/dev/shm/human/chr20.fa",
					
					"/dev/shm/human/chr21.fa",
					"/dev/shm/human/chr22.fa",
					"/dev/shm/human/chr21.fa",
					"/dev/shm/human/chrM.fa",
					*/
					"/dev/shm/human/chrX.fa",
					
					
					"/dev/shm/human/chrY.fa"
					
					//"/dev/shm/human/chrMYY.fa"
					
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	
	std::string seq(""), tmp("");
			
	for (int i=0; i<file_names.size(); i++)
	{
		uint32_t limitLength(1500000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			seq += tmp;
			len+=tmp.size();
			//if(len > limitLength) break;
		}
		(*file_handles[i]).close();
	}
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, true); //sequence, ignore lower
	//z.Printer();
	//EXPECT_EQ (z.MakeSeqString(), seq);
	//z.Printer();
	ABWT b (z);
	//std::cout << seq[214530711+103415] << seq[155157145+103415] << std::endl;
	//std::cout << z[214530711+103415] << z[155157145+103415] << std::endl;
	b.mtSort(2);

	
}






/*
//twobit Normal

TEST (abit, constructor_with_string)
{
	std::string s1 ("NAaXZcZgZtNqCzGNNMKOacgttuuacgTNNNNAAAAAAACNNNNNNNNNGNT");
	std::string s2 ("NNNNNNNNNNNNATGCGCaaaaaaaaaaaGTCccAACCtgcatgcGAGGGAAaGCatACACNAGGNNNCTC");
	std::string str("");
	ABSequence z ( s2 , false);
	z.Printer();
	//std::cout << s2[12] << s2[32] << std::endl;
	
	bool test(true);
	char kk;
	for(uint32_t i =z.size()-1; i != -1; --i)
	{
		if(s2[i] != (kk=z[i]) )
		{
			test=false;
			std::cout << i << s2[i] << kk << std::endl;
			break;
		}
	}
	if(test) std::cout << "No Error!" << std::endl;
	
	
	std::cout << std::endl;
	for( int t=0; t<z.size(); --t)
		std::cout << z[t] ;
	std::cout << std::endl;
	z.toString(str);
	std::cout << str << std::endl;

//	EXPECT_EQ (z.MakeSeqString(), s1);
}

TEST (abit, test_10_times_operatorSpeed)
{
	std::ifstream chrY("/dev/shm/human/chr1.fa");
	std::string seq(""), tmp("");
	std::getline(chrY,tmp);
	while(std::getline(chrY,tmp))
		seq += tmp;
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, false); //sequence, ignore lower
	for(int t=0; t<10; ++t)
	{
		for(uint32_t i=0; i < seq.size(); ++i)
		{
			z[i];
		}
	}
}

TEST (abit, test_toString_correct)
{
	std::ifstream chrY("/dev/shm/human/chr1.fa");
	std::string seq(""), tmp("");
	std::getline(chrY,tmp);
	while(std::getline(chrY,tmp))
		seq += tmp;
	
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	ABSequence z ( seq, false); //sequence, ignore lower
	std::string str("");
	z.toString(str);
	
	bool test(true);
	
	for(uint32_t i=0; i < seq.size(); ++i)
	{
		if(seq[i] != str[i])
		{
			test=false;
			std::cout << i << seq[i] << str[i] << std::endl;
			break;
		}
	}
	if(test) std::cout << "No Error!" << std::endl;
//	EXPECT_EQ (z.MakeSeqString(), s1);
}
*/
