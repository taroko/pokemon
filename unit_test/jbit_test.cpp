#include "gtest/gtest.h"
#include "abit_include.hpp"
#include "../src/compression/jbit.hpp"
#include <map>
TEST (jbit, alphabet)
{
// 	Char2byte test; 
//	Word2byte test;
//	Byte2word test;
std::string seq(""),test_seq(""), tmp("");
    std::vector<std::string> file_names (
	{"/run/shm/human/chrY.fa"}
	);
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
	  file_handles.push_back (new std::ifstream ( file_names[i] ) );

	for (int i=0; i<file_names.size(); i++)
	{
	  uint32_t limitLength(12000), len(0);
	  std::getline(*file_handles[i],tmp);
	  while(std::getline(*file_handles[i],tmp))
	  {
		seq += tmp;
		len+=tmp.size();
		//if(len > limitLength) break;
	  }
	  (*file_handles[i]).close();
	}

	//seq="ACGTAGTCGT";
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	long none_N =0;
	for (long i=0; i<seq.size(); i++)
	{
	  if (seq[i]!='N')
	  {
		seq[none_N] = seq[i];
		none_N++;
	  }
	}
	seq.resize(none_N);

	std::string overhang ( 3-seq.length()%3, '$');
	seq += overhang;	
//	std::cout<<seq<<"->"<<overhang<<std::endl;
	JBit jbit( seq );
	seq = jbit.seq_+(*gAlphabet)[0];
/*
	std::vector<uint32_t> qq( jbit.seq_.size() );
//	std::vector<uint32_t> qq( seq.size() );
	for (uint32_t i =0; i< qq.size(); ++i)
	  qq[i]=i;
	std::sort(qq.begin(), qq.end(), 
		[&]( uint32_t a, uint32_t b)
		{
			if ( strcmp(jbit.seq_.c_str()+a, jbit.seq_.c_str()+b ) < 0 )
//			if ( strcmp(seq.c_str()+a, seq.c_str()+b ) < 0 )
				return true;
			else
				return false;
		}
	);
	std::cout<<jbit.seq_<<std::endl;
	std::cout<<seq<<std::endl;
*/
//	ABSequence<std::string> x ( jbit.seq_ );
	{
		ABSequence<std::string> x ( seq );
		ABWT<ABSequence<std::string>> y (x);
	}
	ABWT_table abwtt;
	abwtt.readBWT("t_bwt.bwt");
	abwtt.readTable("t_table.bwt");
	ABSequence<std::string> bseq;
	abwtt.readSEQ("t_seq.bwt", bseq);
	std::cerr<<"start testing decompression ..."<<std::endl;
	char tt;
	int ii(0), l(0);
	std::string str(" ");
	while(1)
	{
	  if(l % 1000000==0)
		std::cerr << l << std::endl;  
	  tt = abwtt.bwt[ii];                                                                                                                                    
	  if(tt == ' ')
		break;
	  str += tt;
	  ii = abwtt.back_tracking(ii);
	  l++;
	  if(l > abwtt.bwt.size())
		break;
	}
	std::reverse(str.begin(), str.end());
	EXPECT_EQ (str, seq );

	std::cerr<<"start testing searching ..."<<std::endl;
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, seq.length()-1000);

	ABWT_search<ABWT_table> searcher (abwtt);
	int start_pos = distribution(generator);      
	std::string query= seq.substr( start_pos, 20 );
	for (int i=0;i<1000;i++)
	{
	  int start_pos = distribution(generator);    
	  std::string query= seq.substr( start_pos, 20 );
	  //std::cerr<<"start matching "<<start_pos<<"\t->\t"<<query<<std::endl;
	  std::vector<uint32_t> all_hit_pos;
	  searcher.start_exact_match(query, all_hit_pos);
	  //std::cerr<<"all_hit_pos number "<<all_hit_pos.size()<<std::endl;
	  for (auto& k : all_hit_pos)
		EXPECT_EQ(  query, seq.substr( k, 20) );
	}

/*	std::cerr<<y.seq_table.size()<<"->"<<qq.size()<<std::endl;
//	EXPECT_EQ (y.seq_table, qq);
	for (int i=0;i<y.seq_table.size();i++)
	{
		std::cout<<jbit.seq_.c_str()+y.seq_table[i]<<std::endl;  
//		std::cout<<seq.c_str()+y.seq_table[i]<<std::endl; 
	}
	for (int i=0;i<y.seq_table.size();i++)
	{
		std::cout<<jbit.seq_.c_str()+qq[i]<<std::endl;  
//		std::cout<<seq.c_str()+y.seq_table[i]<<std::endl; 
	}
	*/
//std::map<std::string, char> test {{ "AAA", '!' }, {"AAT", '?'} };

//std::cerr<<test["AAA"];

}  

