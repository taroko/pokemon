/*
#include <bitset>
#include <iostream>
#include <deque>
#include <string>
#include <algorithm>
#include <fstream>

#include "cstring"
#include "boost/dynamic_bitset.hpp"
#include "boost/utility/binary.hpp"
#include "boost/lexical_cast.hpp"
#include "../src/compression/abit.hpp"
#include "../src/abwt.hpp"
#include "../src/difference_cover.hpp"
#include "../src/difference_cover_new.hpp"
#include "../src/thread_pool.hpp"
#include "../src/mkq_sort.hpp"
#include "../src/bucket_sort.hpp"
#include "gtest/gtest.h"
*/
#include "../src/constant_def.hpp"
#include "abit_include.hpp"
#include <chrono>
#include <ctime>

//#include "../src/atailer.hpp"
//#include "../src/PeatCloud/PeatFree.hpp"

//#include <vector>

#include "../src/aligner/aligner.hpp"
#include "../src/file_reader.hpp"
#include "../src/iohandler/iohandler.hpp"
#include "../src/pipeline/reader_wrapper.hpp"





TEST (indexer, test_BWT_build)
{
					
	std::vector<std::string> filelist 
	{
		//"/mnt/godzilla/GENOME/db/hg19/chrY.fa"
		"/home/andy/andy/pokemon_0505/sbwt_test2/genome_hg19/hg19_1X_test_400M.fa"
	};
	
	//Aligner< Aligner_trait<> > aligner;
	
	Aligner< 
		Aligner_trait<
			Aligner_types::SBWT_Aligner
			, ParallelTypes::M_T
			, FileReader_impl< 
				Fastq 
				,std::tuple <std::string, std::string, std::string, std::string>
				,SOURCE_TYPE::IFSTREAM_TYPE
			>
		>
	> aligner;
	//aligner.build(filelist, "tmp2");
	////
	
}
TEST (indexer, test_BWT_search)
{
	typedef Aligner< 
		Aligner_trait<
			Aligner_types::SBWT_Aligner
			, ParallelTypes::M_T
			, FileReader_impl< 
				Fastq 
				,std::tuple <std::string, std::string, std::string, std::string>
				,SOURCE_TYPE::IFSTREAM_TYPE
			>
		>
	> ALIGNER_TYPE;
	
	ALIGNER_TYPE aligner;
	aligner.load_table("tmp2");
	
	typedef std::tuple <std::string, std::string, std::string, std::string > TUPLETYPE;
	//std::map <int, std::vector< Fastq <TUPLETYPE> > > result;	
	std::vector <std::string> fastq_file_vec({ "s_hg19_400M_reads_50_200000.fq" });
	std::vector <uint64_t> fastq_size_vec(0);
	
	//bool Read_NSkip (std::map< int, std::vector< FORMAT <TUPLETYPE> > >* result, size_t Num = 1)
	
	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE> fastq_reader (fastq_file_vec, fastq_size_vec);
	
	int nthreads = 1;
	int limitNumber = 1000;
	int strand = 2;//0+, 1-, 2+-
	
	GlobalPool.ChangePoolSize(nthreads);
	
	std::ofstream out("result.sam");
	int total_sam_number = 0;
	bool flag(false);
	while (!flag)
	{
		
		std::map <int, std::vector< Fastq <TUPLETYPE> > > fastqs;
		std::vector< ALIGNER_TYPE::SAM_FORMAT_TYPE > sams;
		flag = fastq_reader.Read_NSkip(&fastqs, 5000*nthreads);
		
		aligner.search(fastqs, sams, limitNumber, strand);
		total_sam_number += sams.size();
		for(auto& sam : sams)
		{
			out << sam;
		}
		
	}
	out.close();
	//
	std::cerr << "Total reads: " << std::get<0>(aligner.reads_map_count_) << "\t" << "Single mapped reads: " << std::get<2>(aligner.reads_map_count_) << "\t";
	std::cerr << "Multiple mapped reads: " << std::get<3>(aligner.reads_map_count_);
	std::cerr << "\t Mappability: " << ((double)std::get<1>(aligner.reads_map_count_) / std::get<0>(aligner.reads_map_count_) ) << std::endl;
	std::cerr << "Total sam results number: " << total_sam_number << std::endl;
	//aligner.search(filelist_fq, 4, "result", 18);
	exit(0);
	
}


/*
////////////
TEST (indexer, test_atailer_building)
{
	
	std::vector<std::string> filelist {"/pokemon/andy/pokemon/gtest_genome.fa"};
	std::string prefix_name {"tmp_"};
	//
	ATailer aa;
	aa.buildBWT(filelist, prefix_name);
}

TEST (indexer, test_atailer_searching)
{
	
	std::vector<std::string> filelist_fq {"gtest.fq"};
	std::vector<std::string> filelist_fa {"gtest.fa"};
	
	std::string prefix_name {"tmp_"};
	
	ATailer aa;
	aa.loadBWT(prefix_name);
	
	std::cerr << "Test search<Fastq_parser>" << std::endl;
	aa.search<Fastq_parser>(filelist_fq,4,"result",18);
	
	std::cerr << "Test search<Fasta_parser>" << std::endl;
	aa.search<Fasta_parser>(filelist_fa,4,"result",18);
	//aa.search_Fastq(filelist_fq,5,"result",18);
	//aa.search_Fasta();
	//aa.search_row();
}
//
*/

//#define INTTYPE INTTYPE

template<class TIME_POINT_TYPE = std::chrono::steady_clock::time_point>
class RunTimer
{
	//std::clock_t c_start = std::clock();
	//std::chrono::duration_cast<std::chrono::milliseconds>(t_end - t_start).count()
	//std::chrono::time_point
	public:
	
};

std::string g_path("/home/andy/andy/pokemon/sbwt_test2");

class run_test
{
public:
	std::vector< std::pair<INTTYPE,INTTYPE> > para;
	std::vector< std::string > para_filename;
	std::vector< std::vector<double> > result_table_abwt;
	std::vector< std::vector<double> > result_table_sbwt;
	std::vector< std::vector < std::vector<double> > > result_search_abwt;
	std::vector< std::vector < std::vector<double> > > result_search_sbwt;
	int repeat;
	std::string path;
	
	run_test(int repeat_times = 5)
		:repeat(repeat_times), path(g_path)
	{
		//para.push_back({128,16});
		//para.push_back({128,32});
		para.push_back({256,32});
		para.push_back({256,64});
		para_filename.push_back(g_path + "/genome_hg19/hg19_1X_test_400M.fa");
		para_filename.push_back(g_path + "/genome_hg19/hg19_2X_test_400M.fa");
		para_filename.push_back(g_path + "/genome_hg19/hg19_4X_test_400M.fa");
		para_filename.push_back(g_path + "/genome_hg19/hg19_8X_test_400M.fa");
		para_filename.push_back(g_path + "/genome_hg19/hg19_16X_test_400M.fa");
		
		
		result_table_sbwt.resize(para_filename.size(), std::vector<double>(para.size(),0));
		result_table_abwt.resize(para_filename.size(), std::vector<double>(para.size(),0));
		result_search_sbwt.resize(para_filename.size(), std::vector<std::vector<double>>(para.size(), std::vector<double>(5,0) ));
		result_search_abwt.resize(para_filename.size(), std::vector<std::vector<double>>(para.size(), std::vector<double>(5,0) ));
	}
	void run()
	{
		for(auto r=0; r<repeat; r++)
		{
			for(auto i=0; i<para_filename.size(); i++)
			{
				for(auto j=0; j<para.size(); j++)
				{
					std::cout << para_filename[i] << " " << para[j].first << " " << para[j].second << std::endl;
					test_table_sbwt(j, i);
					test_table_abwt(j, i);
					test_search_sbwt(j, i);
					test_search_abwt(j, i);
				}
			}
		}
	}
	void print2()
	{
		for(auto i=0; i<result_table_sbwt.size();i++)
		{
			std::cout << para_filename[i] << "\t\t";
			for(auto j=0; j<result_table_sbwt[i].size();j++)
			{
				std::cout << result_table_sbwt[i][j]/repeat << "\t";
			}
			std::cout << "\t";
			for(auto j=0; j<result_table_abwt[i].size();j++)
			{
				std::cout << result_table_abwt[i][j]/repeat << "\t";
			}
			std::cout << "\n";
		}
		
		for(auto i=0; i<result_search_sbwt.size();i++) // filename
		{
			for(auto k=0; k<result_search_sbwt[i][0].size();k++) //length
			{
				std::cout << para_filename[i] << "\t";
				std::cout << (k)*20+30 << "\t";
				for(auto j=0; j<result_search_sbwt[i].size();j++) // dcs and interval type
				{
					std::cout << result_search_sbwt[i][j][k]/repeat << "\t";
				}
				std::cout << "\t";
				for(auto j=0; j<result_search_abwt[i].size();j++) // dcs and interval type
				{					
					std::cout << result_search_abwt[i][j][k]/repeat << "\t";
				}
				std::cout << "\n";
			}
			std::cout << "\n";
		}
		
		
	}
	void print()
	{
		for(auto i=0; i<result_table_sbwt.size();i++)
		{
			std::cout << para_filename[i] << "\t";
			for(auto j=0; j<result_table_sbwt[i].size();j++)
			{
				std::cout << result_table_sbwt[i][j] << "\t";
			}
			std::cout << "\n";
		}
		for(auto i=0; i<result_table_abwt.size();i++)
		{
			std::cout << para_filename[i] << "\t";
			for(auto j=0; j<result_table_abwt[i].size();j++)
			{
				std::cout << result_table_abwt[i][j] << "\t";
			}
			std::cout << "\n";
		}
		
		for(auto i=0; i<result_search_sbwt.size();i++)
		{
			std::cout << para_filename[i] << "\t";
			
			for(auto k=0; k<result_search_sbwt[i][0].size();k++)
			{
				for(auto j=0; j<result_search_sbwt[i].size();j++)
				{
					std::cout << result_search_sbwt[i][j][k] << "\t";
				}
			}
			std::cout << "\n";
		}
		for(auto i=0; i<result_search_abwt.size();i++)
		{
			std::cout << para_filename[i] << "\t";
			for(auto k=0; k<result_search_abwt[i][0].size();k++)
			{
				for(auto j=0; j<result_search_abwt[i].size();j++)
				{					
					std::cout << result_search_abwt[i][j][k] << "\t";
				}
			}
			
			std::cout << "\n";
		}
	}
	void test_table_sbwt(int idx, int fidx)
	{
		clock_t start, stop;
		start = clock();
		
		std::string seq(""),test_seq(""), tmp("");
		std::vector<std::string> file_names (
			{
				para_filename[fidx]
			}
		);
		std::vector <std::ifstream*> file_handles;
		for (int i=0; i<file_names.size(); i++)
				file_handles.push_back (new std::ifstream ( file_names[i] ) );
			
		for (int i=0; i<file_names.size(); i++)
		{
			INTTYPE limitLength(10000000), len(0);
			std::getline(*file_handles[i],tmp);
			while(std::getline(*file_handles[i],tmp))
			{
				seq += tmp;
				len+=tmp.size();
				//if(len > limitLength) break;
			}
			(*file_handles[i]).close();
		}
			
		std::cout << "sequence size : " << seq.size() << std::endl;	
		std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	
		seq+="$";	
		
		ABSequence<std::string> x ( seq );
		SBWT<ABSequence<std::string>> z (x, para[idx].first, para[idx].second); // (seq, dcs_size, table_interval_size)
		stop = clock();
		std::cout << "SBWT Total, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
		result_table_sbwt[fidx][idx] += (double(stop - start) / CLOCKS_PER_SEC);
	}
	void test_table_abwt(int idx, int fidx)
	{
		clock_t start, stop;
		start = clock();
		
		std::string seq(""),test_seq(""), tmp("");
		std::vector<std::string> file_names (
				{
					para_filename[fidx]
				});
		std::vector <std::ifstream*> file_handles;
		for (int i=0; i<file_names.size(); i++)
				file_handles.push_back (new std::ifstream ( file_names[i] ) );
			
		for (int i=0; i<file_names.size(); i++)
		{
			INTTYPE limitLength(10000000), len(0);
			std::getline(*file_handles[i],tmp);
			while(std::getline(*file_handles[i],tmp))
			{
				seq += tmp;
				len+=tmp.size();
				//if(len > limitLength) break;
			}
			(*file_handles[i]).close();
		}
			
		std::cout << "sequence size : " << seq.size() << std::endl;	
		std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	
		seq+="$";	
		
		ABSequence<std::string> x ( seq );
		ABWT<ABSequence<std::string>> y (x, para[idx].first, para[idx].second);
		stop = clock();
		std::cout << "BWT Total, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
		result_table_abwt[fidx][idx] += (double(stop - start) / CLOCKS_PER_SEC);
	}
	void test_search_sbwt(int idx, int fidx)
	{
		clock_t start, stop;
	
		for(auto i=30; i<=100; i+=20)
		{
			std::cout << "SBWT : length " << i << std::endl;
			if( i+para[idx].second > para[idx].first)
				break;
			start = clock();
			
			ABWT_table abwtt;
			//abwtt.readBWT("t_sbwt.bwt");
			abwtt.readTable("t_stable.bwt");
			ABSequence<std::string> seq;
			//abwtt.readSEQ("t_sseq.bwt", seq);
			ABWT_search<ABWT_table> searcher (abwtt);
			
			std::string filename = g_path + "/reads_hg19/s_hg19_400M_reads_" + std::to_string(i);
			std::string output_name = g_path + "/result_hg19/result_hg19_400M_reads_s_" + std::to_string(i) + "_" + std::to_string(idx) + "_" + std::to_string(fidx);
			std::ifstream in(filename);
			std::ofstream out(output_name);
			std::vector<INTTYPE> all_hit_pos;
			all_hit_pos.reserve(10000);
			std::string query;
			
			for (int i=0;i<4000000;i++)
			{
				std::getline(in,query, '\n');
				searcher.start_sbwt_match(query, all_hit_pos);
				for (auto& k : all_hit_pos)
				{
					out << k << " " << query << " " << "\n";
				}
				all_hit_pos.clear();	
			}
			in.close();
			out.close();
			stop = clock();
			std::cout << "SBWT search time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
			result_search_sbwt[fidx][idx][i/20-1] += (double(stop - start) / CLOCKS_PER_SEC);
			
			print2();
		}
	}
	void test_search_abwt(int idx, int fidx)
	{
		clock_t start, stop;
		
		for(auto i=30; i<=100; i+=20)
		{
			std::cout << "ABWT : length " << i << std::endl;
			start = clock();
			
			ABWT_table abwtt;
			//abwtt.readBWT("t_bwt.bwt");
			abwtt.readTable("t_table.bwt");
			ABSequence<std::string> seq;
			//abwtt.readSEQ("t_seq.bwt", seq);
			ABWT_search<ABWT_table> searcher (abwtt);
			
			std::string filename = g_path + "/reads_hg19/s_hg19_400M_reads_" + std::to_string(i);
			std::string output_name = g_path + "/result_hg19/result_hg19_400M_reads_a_" + std::to_string(i) + "_" + std::to_string(idx) + "_" + std::to_string(fidx);
			std::ifstream in(filename);
			std::ofstream out(output_name);
			std::vector<INTTYPE> all_hit_pos;
			all_hit_pos.reserve(10000);
			std::string query;
			
			for (int i=0;i<4000000;i++)
			{
				std::getline(in,query, '\n');
				searcher.start_exact_match(query, all_hit_pos);
				for (auto& k : all_hit_pos)
				{
					out << k << " " << query << " " << "\n";
				}
				all_hit_pos.clear();	
			}
			in.close();
			out.close();
			stop = clock();
			std::cout << "ABWT search time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
			result_search_abwt[fidx][idx][i/20-1] += (double(stop - start) / CLOCKS_PER_SEC);
			
			print2();
		}
	}
};

/*
TEST (sbwt_speed_test, make_genome)
{
	std::vector<std::string> file_names ({
		"/mnt/godzilla/GENOME/db/hg19/hg19.fa"
	});
	
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	
	std::string seq, seq2, tmp;
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(1*1000*1000*1000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			if(tmp[0] == '>')
				continue;
			seq += tmp;
			len+=tmp.size();
			if(len > limitLength) break;
		}
		(*file_handles[i]).close();
	}
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	long none_N =0;
	for (long i=0; i<seq.size(); i++)
	{
		if (seq[i] == 'A' || seq[i] == 'C' || seq[i] == 'G' || seq[i] == 'T')
		{
			seq[none_N] = seq[i];
			none_N++;
		}
	}
	seq.resize(none_N);
	
	//400M
	INTTYPE make_ref_length(400*1000*1000);

	std::string all_char("ACGT"), path(g_path);
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution( make_ref_length/16 , make_ref_length-100);
	
	seq2 = seq.substr(0, make_ref_length);
	for(int i(0); i< (make_ref_length*0.004); ++i)
	{
		int pos = distribution(generator);
		seq2[pos] = all_char[(pos & 3)];
	}
	std::ofstream Q1(path + "/genome_hg19/hg19_1X_test_400M.fa");
	Q1 << ">chr1X" << std::endl;
	Q1 << seq2 << std::endl;
	Q1.close();
	
	seq2 = seq.substr(0, make_ref_length/2);
	seq2+=seq2;//2x
	for(int i(0); i< (make_ref_length*0.004); ++i)
	{
		int pos = distribution(generator);
		seq2[pos] = all_char[(pos & 3)];
	}
	std::ofstream Q2(path + "/genome_hg19/hg19_2X_test_400M.fa");
	Q2 << ">chr2X" << std::endl;
	Q2 << seq2 << std::endl;
	Q2.close();
	
	seq2 = seq.substr(0, make_ref_length/4);
	seq2+=seq2;//2x
	seq2+=seq2;//4x
	for(int i(0); i< (make_ref_length*0.004); ++i)
	{
		int pos = distribution(generator);
		seq2[pos] = all_char[(pos & 3)];
	}
	std::ofstream Q4(path + "/genome_hg19/hg19_4X_test_400M.fa");
	Q4 << ">chr4X" << std::endl;
	Q4 << seq2 << std::endl;
	Q4.close();
	
	seq2 = seq.substr(0, make_ref_length/8);
	seq2+=seq2;//2x
	seq2+=seq2;//4x
	seq2+=seq2;//8x
	for(int i(0); i< (make_ref_length*0.004); ++i)
	{
		int pos = distribution(generator);
		seq2[pos] = all_char[(pos & 3)];
	}
	std::ofstream Q8(path + "/genome_hg19/hg19_8X_test_400M.fa");
	Q8 << ">chr8X" << std::endl;
	Q8 << seq2 << std::endl;
	Q8.close();
	
	seq2 = seq.substr(0, make_ref_length/16);
	seq2+=seq2;//2x
	seq2+=seq2;//4x
	seq2+=seq2;//8x
	seq2+=seq2;//16x
	for(int i(0); i< (make_ref_length*0.004); ++i)
	{
		int pos = distribution(generator);
		seq2[pos] = all_char[(pos & 3)];
	}
	std::ofstream Q16(path + "/genome_hg19/hg19_16X_test_400M.fa");
	Q16 << ">chr16X" << std::endl;
	Q16 << seq2 << std::endl;
	Q16.close();
}


TEST (sbwt_speed_test, make_reads)
{
	std::string seq(""),test_seq(""), tmp("");
	std::vector<std::string> file_names (
		{
			"/mnt/godzilla/GENOME/db/hg19/hg19.fa"
			//"/home/andy/andy/pokemon/sbwt_test/genome_hg19/hg19_1X_test_400M.fa"
		}
	);
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(40*1000*1000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			if(tmp[0] == '>')
				continue;
			seq += tmp;
			len+=tmp.size();
			if(len > limitLength) break;
		}
		(*file_handles[i]).close();
	}
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	long none_N =0;
	for (long i=0; i<seq.size(); i++)
	{
		if (seq[i] == 'A' || seq[i] == 'C' || seq[i] == 'G' || seq[i] == 'T')
		{
			seq[none_N] = seq[i];
			none_N++;
		}
	}
	seq.resize(none_N);
	
	seq = seq.substr(0, 25*1000*1000);
	std::cout << seq.size() << std::endl;
	
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, seq.size()-1000);
	std::string query;
	
	for(auto i=30; i<=100; i+=20)
	{
		INTTYPE read_length(i);
		std::string filename = g_path + "/reads_hg19/s_hg19_400M_reads_" + std::to_string(i);
		std::ofstream* fp = new std::ofstream ( filename );
		
		for(auto j=0; j<4*1000*1000; j++)
		{
			int start_pos = distribution(generator);
			query = seq.substr( start_pos, read_length );
			(*fp) << query << "\n";
		}
		fp->close();
	}
}
*/



TEST (indexer, test_all_speed)
{
	//run_test tt(5);
	//tt.run();
	//std::cout << "====================================================" << std::endl;
	//tt.print2();
	
	//exit(0);
}








INTTYPE g_len = 256;
INTTYPE g_interval = 64;


//////

TEST (indexer, test_create_sbwt_idx_speed)	
{
	clock_t start, stop;
	start = clock();
	
	std::string seq(""),test_seq(""), tmp("");
	std::vector<std::string> file_names (
			{
				//"/pokemon/andy/sbwt_test2/hcv.genome.fa"
				//"/pokemon/andy/sbwt_test/chrX_test.fa"
				//"/pokemon/andy/sbwt_test/chrX_test_80M"
				"/home/andy/andy/pokemon/sbwt_test2/genome_hg19/hg19_1X_test_400M.fa"
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
		
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(10000000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			seq += tmp;
			len+=tmp.size();
			//if(len > limitLength) break;
		}
		(*file_handles[i]).close();
	}
		
	std::cout << "sequence size : " << seq.size() << std::endl;	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);

	seq+="$";	
	
	ABSequence<std::string> x ( seq );
	
	SBWT<ABSequence<std::string>> z (x, g_len, g_interval); // (seq, dcs_size, table_interval_size)
	
	
	stop = clock();
	std::cerr << "SBWT Total, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;	
}



/*
TEST (indexer, test_create_bwt_idx_speed)	
{
	clock_t start, stop;
	start = clock();
	
	std::string seq(""),test_seq(""), tmp("");
	std::vector<std::string> file_names (
			{
				"/pokemon/andy/sbwt_test/chrX_test_80M"
				//"/pokemon/andy/sbwt_test2/hcv.genome.fa"
				//"/pokemon/andy/sbwt_test/chrX_test_80M"
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
		
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(10000000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			seq += tmp;
			len+=tmp.size();
			//if(len > limitLength) break;
		}
		(*file_handles[i]).close();
	}
		
	std::cout << "sequence size : " << seq.size() << std::endl;	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);

	seq+="$";	
	
	ABSequence<std::string> x ( seq );
	
	
	ABWT<ABSequence<std::string>> y (x, g_len, g_interval);
	stop = clock();
	std::cerr << "BWT Total, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
}
*/

/*
TEST (indexer, make_reads)
{
	std::string seq(""),test_seq(""), tmp("");
	std::vector<std::string> file_names (
		{
			//"/run/shm/human/chrX.fa"
			"/mnt/godzilla/GENOME/db/hg18/chrX.fa",
			"/mnt/godzilla/GENOME/db/hg18/chrY.fa"
		}
	);
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(60000000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			seq += tmp;
			len+=tmp.size();
			//if(len > limitLength) break;
		}
		(*file_handles[i]).close();
	}
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	long none_N =0;
	for (long i=0; i<seq.size(); i++)
	{
		if (seq[i]!='N')
		{
			seq[none_N] = seq[i];
			none_N++;
		}
	}
	seq.resize(none_N);
	
	seq = seq.substr(0,10000000);
	std::cout << seq.size() << std::endl;
	
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, seq.size()-1000);
	std::string query;
	
	for(auto i=20; i<=100; i+=20)
	{
		INTTYPE read_length(i);
		std::string filename = "/home/andy/andy/sbwt_test/s_chrX_160M_reads_" + std::to_string(i);
		std::ofstream* fp = new std::ofstream ( filename );
		
		for(auto j=0; j<400000; j++)
		{
			int start_pos = distribution(generator);
			query = seq.substr( start_pos, read_length );
			(*fp) << query << std::endl;
		}
		fp->close();
	}
}
*/

/*
TEST (indexer, search_bwt_speed)
{
	clock_t start, stop;
	
	start = clock();
	ABWT_table abwtt;
	//abwtt.readBWT("t_bwt.bwt");
	abwtt.readTable("t_table.bwt");
	//ABSequence<std::string> seq;
	//abwtt.readSEQ("t_seq.bwt", seq);
	stop = clock();
	std::cerr << "ReadTable, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	
	//abwtt.using_jbwt();
	ABWT_search<ABWT_table> searcher (abwtt);
	
	std::string query;
	
	for(auto i=40; i<=100; i+=20)
	{
		std::cout << "ABWT : length " << i << std::endl;
		start = clock();
		
		std::string filename = "/pokemon/andy/sbwt_test/s_chrX_80M_reads_" + std::to_string(i);
		std::string output_name = "/pokemon/andy/sbwt_test/result_chrX_80M_reads_" + std::to_string(i);
		std::ifstream in(filename);
		std::ofstream out(output_name);
		std::vector<INTTYPE> all_hit_pos;
		all_hit_pos.reserve(1000);
		std::string query;
		
		for (int i=0;i<400000;i++)
		{
			std::getline(in,query, '\n');
			//searcher.start_exact_match(query, all_hit_pos);
			searcher.start_one_mismatch2(query, all_hit_pos);
			for (INTTYPE& k : all_hit_pos)
			{
				out << k << " " << query << " " << "\n";
			}
			all_hit_pos.clear();	
		}
		in.close();
		out.close();
		stop = clock();
		std::cerr << "ABWT search time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	}
}
*/

/*
TEST (indexer, search_sbwt_speed)
{
	clock_t start, stop;
	
	start = clock();
	ABWT_table abwtt;
	abwtt.readBWT("t_sbwt.bwt");
	abwtt.readTable("t_stable.bwt");
	//ABSequence<std::string> seq;
	//abwtt.readSEQ("t_sseq.bwt", seq);
	
	ABWT_search<ABWT_table> searcher (abwtt);
	stop = clock();
	std::cerr << "ReadTable, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	
	//abwtt.using_jbwt();
	
	std::string query;
	
	for(auto i=20; i<=20; i+=20)
	{
		std::cout << "SBWT : length " << i << std::endl;
		if( i+g_interval > g_len)
			break;
		start = clock();
		
		std::string filename = "/pokemon/andy/sbwt_test/s_chrX_80M_reads_" + std::to_string(i);
		std::string output_name = "/pokemon/andy/sbwt_test/result_chrX_80M_reads_" + std::to_string(i);
		std::ifstream in(filename);
		std::ofstream out(output_name);
		std::vector<INTTYPE> all_hit_pos;
		all_hit_pos.reserve(1000);
		std::string query;
		
		for (int i=0;i<400000;i++)
		{
			std::getline(in,query, '\n');
			searcher.start_sbwt_match(query, all_hit_pos);
			for (INTTYPE & k : all_hit_pos)
			{
				out << k << " " << query << " " << "\n";
			}
			all_hit_pos.clear();	
		}
		in.close();
		out.close();
		stop = clock();
		std::cerr << "SBWT search time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	}
}
*/

/*
TEST (indexer, search_sbwt_speed)
{
	clock_t start, stop;
	
	start = clock();
	ABWT_table abwtt;
	abwtt.readBWT("t_sbwt.bwt");
	abwtt.readTable("t_stable.bwt");
	//ABSequence<std::string> seq;
	//abwtt.readSEQ("t_sseq.bwt", seq);
	
	ABWT_search<ABWT_table> searcher (abwtt);
	stop = clock();
	std::cerr << "ReadTable, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	
	//abwtt.using_jbwt();
	
	std::string query;
	
	for(auto i=15; i<=25; i+=10)
	{
		std::cout << "SBWT : length " << i << std::endl;
		if( i+g_interval > g_len)
			break;
		start = clock();
		
		std::string filename = "/pokemon/andy/sbwt_test2/hcv.reads." + std::to_string(i) + ".raw";
		std::string output_name = "/pokemon/andy/sbwt_test2/hcv.result." + std::to_string(i) + ".raw";
		std::ifstream in(filename);
		std::ofstream out(output_name);
		std::vector<INTTYPE> all_hit_pos;
		all_hit_pos.reserve(1000);
		std::string query;
		
		for (int i=0;i<317235;i++)
		{
			std::getline(in,query, '\n');
			searcher.start_sbwt_match(query, all_hit_pos);
			for (INTTYPE & k : all_hit_pos)
			{
				out << k << " " << query << " " << "\n";
			}
			all_hit_pos.clear();	
		}
		in.close();
		out.close();
		stop = clock();
		std::cerr << "SBWT search time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	}
}

TEST (indexer, search_bwt_speed)
{
	clock_t start, stop;
	
	start = clock();
	ABWT_table abwtt;
	abwtt.readBWT("t_bwt.bwt");
	abwtt.readTable("t_table.bwt");
	//ABSequence<std::string> seq;
	//abwtt.readSEQ("t_sseq.bwt", seq);
	
	ABWT_search<ABWT_table> searcher (abwtt);
	stop = clock();
	std::cerr << "ReadTable, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	
	//abwtt.using_jbwt();
	
	std::string query;
	
	for(auto i=15; i<=25; i+=10)
	{
		std::cout << "BWT : length " << i << std::endl;
		if( i+g_interval > g_len)
			break;
		start = clock();
		
		std::string filename = "/pokemon/andy/sbwt_test2/hcv.reads." + std::to_string(i) + ".raw";
		std::string output_name = "/pokemon/andy/sbwt_test2/hcv.result.bwt." + std::to_string(i) + ".raw";
		std::ifstream in(filename);
		std::ofstream out(output_name);
		std::vector<INTTYPE> all_hit_pos;
		all_hit_pos.reserve(1000);
		std::string query;
		
		for (int i=0;i<317235;i++)
		{
			std::getline(in,query, '\n');
			searcher.start_exact_match(query, all_hit_pos);
			for (INTTYPE & k : all_hit_pos)
			{
				out << k << " " << query << " " << "\n";
			}
			all_hit_pos.clear();	
		}
		in.close();
		out.close();
		stop = clock();
		std::cerr << "BWT search time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	}
}
*/
//TEST (indexer, test_speed)	 //whole read, parse, encode, serialize, and de-serialize takes about 16 secs
//{
//	
//
//	std::string seq(""),test_seq(""), tmp("");
//	std::vector<std::string> file_names (
//			{
//				/*
//				"/run/shm/human/chr1.fa",	
//				"/run/shm/human/chr2.fa",
//				"/run/shm/human/chr3.fa",
//				"/run/shm/human/chr4.fa",
//				"/run/shm/human/chr5.fa",
//				
//				"/run/shm/human/chr6.fa",
//				"/run/shm/human/chr7.fa",
//				"/run/shm/human/chr8.fa",
//				"/run/shm/human/chr9.fa",
//				"/run/shm/human/chr10.fa",
//				
//				"/run/shm/human/chr11.fa",
//				"/run/shm/human/chr12.fa",
//				"/run/shm/human/chr13.fa",
//				"/run/shm/human/chr14.fa",
//				"/run/shm/human/chr15.fa",
//				"/run/shm/human/chr16.fa",
//				
//				"/run/shm/human/chr17.fa",
//				"/run/shm/human/chr18.fa",
//				"/run/shm/human/chr19.fa",
//				"/run/shm/human/chr20.fa",
//				"/run/shm/human/chr21.fa",
//				"/run/shm/human/chr22.fa",
//				"/run/shm/human/chr21.fa",
//				"/run/shm/human/chrM.fa",
//				*/
//				"/mnt/godzilla/GENOME/db/hg18/chrX.fa",
//				"/mnt/godzilla/GENOME/db/hg18/chrY.fa"
//				//"/pokemon/andy/sbwt_test/chrX_test_80M"
//				//"/run/shm/human/chrX.fa"
//				//"/pokemon/andy/all_seqs.fa"
//				//"metagenomics.fa"
//				//"/run/shm/human/chrY.fa"
//				//
//				//"/pokemon/andy/tomme/Andy_Sample_B332.all.g0_g1_100_50_2/macs_peaks.seq"
//				//"/pokemon/andy/Ago1-IP-1.fa"
//				//	
//			});
//	std::vector <std::ifstream*> file_handles;
//	for (int i=0; i<file_names.size(); i++)
//			file_handles.push_back (new std::ifstream ( file_names[i] ) );
//		
//	for (int i=0; i<file_names.size(); i++)
//	{
//		INTTYPE limitLength(200000000), len(0);
//		std::getline(*file_handles[i],tmp);
//		while(std::getline(*file_handles[i],tmp))
//		{
//			if(tmp[0] == '>')
//				continue;
//			seq += tmp;
//			len+=tmp.size();
//			//if(len > limitLength) break;
//		}
//		(*file_handles[i]).close();
//	}
//	
//
//
//	/*
//	std::cout << "sequence size : " << seq.size() << std::endl;
//	
//	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
//	//std::cerr << "A" << std::endl;
//	
//	long none_N =0;
//	for (long i=0; i<seq.size(); i++)
//	{
//		if (seq[i]=='A' || seq[i]=='T' || seq[i]=='C' || seq[i]=='G')
//		{
//			seq[none_N] = seq[i];
//			none_N++;
//		}
//	}
//	seq.resize(none_N);
//	std::cout << "sequence size no N : " << seq.size() << std::endl;
//	//std::cerr << ">chrX_NoneN_test" << std::endl;
//	
//	
//	std::string all_char("ACGT"), path("/home/andy/andy/sbwt_test");
//	
//	INTTYPE make_ref_length(160000000);
//
//	
//	std::default_random_engine generator;
//	std::uniform_int_distribution<int> distribution(0, make_ref_length-100);
//	
//	seq = seq.substr(0, make_ref_length);
//	for(int i(0); i< (seq.size()*0.003); ++i)
//	{
//		int pos = distribution(generator);
//		seq[pos] = all_char[(pos & 3)];
//	}
//	std::ofstream Q1(path + "/chrX_test_160M");
//	Q1 << ">chrX" << std::endl;
//	Q1 << seq << std::endl;
//	Q1.close();
//	
//	seq = seq.substr(0, make_ref_length/2);
//	seq+=seq;//2x
//	for(int i(0); i< (seq.size()*0.003); ++i)
//	{
//		int pos = distribution(generator);
//		seq[pos] = all_char[(pos & 3)];
//	}
//	std::ofstream Q2(path + "/chr2X_test_160M");
//	Q2 << ">chr2X" << std::endl;
//	Q2 << seq << std::endl;
//	Q2.close();
//	
//	seq = seq.substr(0, make_ref_length/4);
//	seq+=seq;//2x
//	seq+=seq;//4x
//	for(int i(0); i< (seq.size()*0.003); ++i)
//	{
//		int pos = distribution(generator);
//		seq[pos] = all_char[(pos & 3)];
//	}
//	std::ofstream Q4(path + "/chr4X_test_160M");
//	Q4 << ">chr4X" << std::endl;
//	Q4 << seq << std::endl;
//	Q4.close();
//	
//	seq = seq.substr(0, make_ref_length/8);
//	seq+=seq;//2x
//	seq+=seq;//4x
//	seq+=seq;//8x
//	for(int i(0); i< (seq.size()*0.003); ++i)
//	{
//		int pos = distribution(generator);
//		seq[pos] = all_char[(pos & 3)];
//	}
//	std::ofstream Q8(path + "/chr8X_test_160M");
//	Q8 << ">chr8X" << std::endl;
//	Q8 << seq << std::endl;
//	Q8.close();
//	
//	seq = seq.substr(0, make_ref_length/16);
//	seq+=seq;//2x
//	seq+=seq;//4x
//	seq+=seq;//8x
//	seq+=seq;//16x
//	for(int i(0); i< (seq.size()*0.003); ++i)
//	{
//		int pos = distribution(generator);
//		seq[pos] = all_char[(pos & 3)];
//	}
//	std::ofstream Q16(path + "/chr16X_test_160M");
//	Q16 << ">chr16X" << std::endl;
//	Q16 << seq << std::endl;
//	Q16.close();
//	*/
//	
//	
//	//seq="ACTAACTAAAAACG";
//	//seq+=seq;//2x
//	//seq+=seq;//4x
//	//seq+=seq;//8x
//	//seq+=seq;//16x
//	
//	
//	//std::ofstream QQ("hg4Y.fa");
//	//QQ << ">chr4Y" << std::endl;
//	//QQ << seq << std::endl;
//	//QQ.close();
//	
//	
//	seq+="$";	
//	
//	/*
//	std::vector<INTTYPE> qq( seq.size() );
//	for (INTTYPE i =0; i< qq.size(); ++i)
//		qq[i]=i;
//	std::sort(qq.begin(), qq.end(), 
//		[&]( INTTYPE a, INTTYPE b)
//		{
//			
//			char temp_a = *(seq.c_str()+a+2);
//			char temp_b = *(seq.c_str()+b+2);
//			*((char*)seq.c_str()+a+2) = '\0';
//			*((char*)seq.c_str()+b+2) = '\0';
//			if ( strcmp(seq.c_str()+a, seq.c_str()+b ) < 0 )
//			{
//			*((char*)seq.c_str()+a+2) = temp_a;
//			*((char*)seq.c_str()+b+2) = temp_b;	
//				return true;
//			}
//			else
//			{
//			*((char*)seq.c_str()+a+2) = temp_a;
//			*((char*)seq.c_str()+b+2) = temp_b;
//				return false;
//			}
//		}
//	);
//	*///
//	//////
//	
//	//ABSequence<std::string> x ( seq );
//	//SBWT<ABSequence<std::string>> z (x, 256, 64); // (seq, dcs_size, table_interval_size)
//	//ABWT<ABSequence<std::string>> y (x, 256, 32);
//	
//	
//	//std::cout << std::endl;
//	exit;
//	//EXPECT_EQ (y.seq_table, qq);
//}








//

//TEST (indexer, test_abwt_table)
//{
	
	//ABSequence<std::string> x ( seq );
	//ABWT<ABSequence<std::string>> y (x);
/*
	clock_t start, stop;
	start = clock();
	
	ABWT_table abwtt;
	abwtt.readBWT("t_bwt.bwt");
	abwtt.readTable("t_table.bwt");
	ABSequence<std::string> seq;
	abwtt.readSEQ("t_seq.bwt", seq);
	std::cerr << "bwt size : " << abwtt.bwt.size() << std::endl;

	stop = clock();
	std::cerr << "ReadTable, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	start = clock();
	//abwtt.using_jbwt();
	
	ABWT_table sabwtt;
	sabwtt.readBWT("t_sbwt.bwt");
	sabwtt.readTable("t_stable.bwt");
	ABSequence<std::string> sseq;
	sabwtt.readSEQ("t_sseq.bwt", sseq);
	std::cerr << "bwt size : " << sabwtt.bwt.size() << std::endl;

	stop = clock();
	std::cerr << "ReadTable, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	start = clock();
	//sabwtt.using_jbwt();
*/
	
	
	/*
	//location table
	auto pp = std::lower_bound( 
		sabwtt.location_table.begin(), 
		sabwtt.location_table.end(), 
		std::pair<INTTYPE, INTTYPE>{3361000, 0}, 
			[]( const std::pair<INTTYPE, INTTYPE>& a, const std::pair<INTTYPE, INTTYPE>& b)
			{
				return a.first < b.first;
			} 
	);
	for(auto i=0; i<10;i++)
	{
		std::cerr << "f : " << pp->first << " e: " << pp->second << std::endl;
		pp++;
	}
	
	std::cerr << std::endl;
	
	pp = std::lower_bound( 
		abwtt.location_table.begin(), 
		abwtt.location_table.end(), 
		std::pair<INTTYPE, INTTYPE>{3361000, 0}, 
			[]( const std::pair<INTTYPE, INTTYPE>& a, const std::pair<INTTYPE, INTTYPE>& b)
			{
				return a.first < b.first;
			} 
	);
	for(auto i=0; i<10;i++)
	{
		std::cerr << "f : " << pp->first << " e: " << pp->second << std::endl;
		pp++;
	}
	*/
	
	
	
	//std::vector<std::pair<INTTYPE, INTTYPE> > c_number;
	/*
	INTTYPE total=0;
	for(auto i(0); i < abwtt.c_functions.size(); ++i)
	{
		
		INTTYPE number = abwtt.c_functions[i+1] - abwtt.c_functions[i];
		if(number!=0)
		{
			total++;
			c_number.push_back({i,number});
		}
			
		
	}
	
	std::cerr <<"total number:" <<total << std::endl;
	
	std::sort( c_number.begin(), c_number.end(), [](std::pair<INTTYPE, INTTYPE> a, std::pair<INTTYPE, INTTYPE> b) {
		return b.second < a.second;
	});
	
	
	std::vector<char> tb({'A','C','G','T'});
	for(auto i(0); i < 100; ++i)
	{
		std::string tmp;
		INTTYPE kk = c_number[i].first;
		INTTYPE cc = kk & 3;
		tmp = tb[cc];
		
		for(auto j(1); j<12; ++j)
		{
			kk = kk >> 2;
			cc = kk & 3;
			tmp += tb[cc];
		}
		std::cerr << "index : " << c_number[i].first << " str : " << tmp << " number : " << c_number[i].second << std::endl;
	}
	*/
	
	
	//print c funciton table
	//for(auto i(abwtt.c_functions.size()-1 ) ; i > abwtt.c_functions.size()-10 ; i--)
	//{
	//	std::cout << "i" << i << "v"<< abwtt.c_functions[i] << std::endl;
	//}
	
	
	
	//print c funciton table
	//std::cout << "C functions table:" << std::endl;
	//for(auto k : abwtt.c_function)
	//{
	//	std::cout << k << std::endl;
	//}
	//print c funciton table
	//std::cout << "SBWT C function table:" << std::endl;
	//for(auto kk : sabwtt.c_function)
	//{
	//	std::cout << kk << std::endl;
	//}
	
	//print occ funciton
	//int i(0);
	//for(auto k : abwtt.occ_function)
	//{
	//	std::cout << i*2 << "\t";
	//	for(auto j : k)
	//	{
	//		std::cout << j << "\t";
	//	}
	//	std::cout << std::endl;
	//	++i;
	//}
/*
	start = clock();
	char tt;
	int ii(0), l(0);
	std::string str("$");
	while(1)
	{
		if(l % 1000000==0)
			std::cerr << l << std::endl;	
		tt = abwtt.bwt[ii];
		if(tt == '$')
			break;
		str += tt;
		//std::cerr << tt << std::endl;
		
		ii = abwtt.back_tracking(ii);
		
		l++;
		if(l >= abwtt.bwt.size())
			break;
	}
	stop = clock();
	std::cerr << "TraceBack, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
	std::reverse(str.begin(), str.end());
	EXPECT_EQ (str, seq.getContent());	
	std::cout << std::endl;
*/
	
	/*
	int kkk(0);
	for(auto k : abwtt.c_function8)
	{
		if(kkk == 100)
			break;
		std::cerr << kkk << " : " << k << std::endl;
		++kkk;
	}
	*/
	
	
/*
	start =clock();
	ABWT_search<ABWT_table> searcher (abwtt);
	//std::ifstream in("qq_mismatch_50w");
	std::ifstream in("qq10.fa");
	
	std::string query;
	std::vector<INTTYPE> all_hit_pos;
	all_hit_pos.reserve(1000);
	for (int i=0;i<100000;i++)
	{
		std::getline(in,query, '\n');
		searcher.start_exact_match(query, all_hit_pos);
		//std::reverse(query.begin(), query.end());
		//searcher.start_one_mismatch2(query, all_hit_pos);
		//for (auto& k : all_hit_pos)
		//{
		//	k-=30;
		//}
		//std::reverse(query.begin(), query.end());
		//searcher.start_one_mismatch2( query, all_hit_pos);
		
		//std::sort( all_hit_pos.begin(), all_hit_pos.end() );
		//all_hit_pos.resize( std::unique( all_hit_pos.begin(), all_hit_pos.end() ) - all_hit_pos.begin() ) ;
		//all_hit_pos.end() );
		//std::reverse(query.begin(), query.end());
		for (auto& k : all_hit_pos)
		{
			std::string tttt = seq.substr(k, 30);
			std::cout<< k << " " << query << " " << tttt << " ";
			for (INTTYPE j(0); j<30; j++)
			{
				if(query[j] != tttt[j])
					std::cout << j << " ";
			}
			std::cout << "\n";
		}
		all_hit_pos.clear();	
	}
	stop = clock();
	std::cerr << "ABWT time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;

	
	
	
	start =clock();
	ABWT_search<ABWT_table> ssearcher (sabwtt);
	//std::ifstream in("qq_mismatch_50w");
	std::ifstream sin("qq10.fa");
	
	std::string squery;
	std::vector<INTTYPE> sall_hit_pos;
	sall_hit_pos.reserve(1000);
	for (int i=0;i<100000;i++)
	{
		std::getline(sin,squery, '\n');
		ssearcher.start_sbwt_match(squery, sall_hit_pos);
		for (auto& k : sall_hit_pos)
		{
			std::string tttt = seq.substr(k, 30);
			std::cout<< k << " " << squery << " " << tttt << " ";
			for (INTTYPE j(0); j<30; j++)
			{
				if(squery[j] != tttt[j])
					std::cout << j << " ";
			}
			std::cout << "\n";
		}
		sall_hit_pos.clear();	
	}
	stop = clock();
	std::cerr << "SBWT time : " << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
*/

/*
std::cerr << "first location : " << sabwtt.first_location << std::endl;

	std::cerr<<"start testing searching ..."<<std::endl;
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, seq.length()-1000);

	ABWT_search<ABWT_table> searcher (abwtt);
	ABWT_search<ABWT_table> ssearcher (sabwtt);
	start =clock();
	
	INTTYPE read_length(50);
	std::cerr << " seq length" << seq.size() << " " << sseq.size() << std::endl;
	EXPECT_EQ(seq, sseq);
	
	int start_pos = distribution(generator);		
	std::string query= seq.substr( start_pos, read_length );
	for (int i=0;i<1;i++)
	{
		//
		int start_pos = distribution(generator);
		//start_pos = 1511831;
		//start_pos = 371219;
		//start_pos = 16772;
		//start_pos = 143819;
		//start_pos = 2047300;
		std::string query = seq.substr( start_pos, read_length );
		//query[start_pos%30] = 'A';
		//std::cout << query <<std::endl;
	std::cerr<<"start matching "<<start_pos<<"\t->\t"<<query<<std::endl;
	
	
		std::vector<INTTYPE> all_hit_pos;
		std::vector<INTTYPE> sall_hit_pos;
		
		//searcher.start_all_possible(query, all_hit_pos);
		
		searcher.start_exact_match(query, all_hit_pos);
	 
		INTTYPE ccc(0);
		for (auto& k : all_hit_pos)
		{
			if(k >= sseq.size())
				continue;
			//std::cerr << k << std::endl;
			//if(query != seq.substr( k, read_length))
			//	std::cout << "ccc " << ccc << " k " << k << "start_pos" << start_pos <<	std::endl;
			EXPECT_EQ(	query, seq.substr( k, read_length) );
			ccc++;
		}
			
		//ssearcher.start_exact_match(query, sall_hit_pos);
		ssearcher.start_sbwt_match(query, sall_hit_pos);
		
		if(all_hit_pos.size() != sall_hit_pos.size())
		{
			
			std::cout<<"all_hit_pos number "<< all_hit_pos.size() << " vs " << sall_hit_pos.size() << " pos " << start_pos <<std::endl;
		}
		//3550253
		ccc=0;
		for (auto& k : sall_hit_pos)
		{
			//std::cerr << k << std::endl;
			//if(query != seq.substr( k, read_length))
				
			
			if(k >= sseq.size())
			{
				std::cout << "error ccc " << ccc << " k " << k << "start_pos" << start_pos <<	std::endl;
				continue;
			}
				
			EXPECT_EQ(	query, seq.substr( k, read_length) );
			ccc++;
		}
			
		
	}
	stop = clock();

	std::cerr << "TraceBack, time:" << double(stop - start) / CLOCKS_PER_SEC << "\n" << std::endl;
*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	//std::cerr<<"binary search time: "<< double(searcher.binary_search_time)/ CLOCKS_PER_SEC << "\n" << std::endl;
	//std::cerr<<"get c time: "<< double(abwtt.get_c_time)/ CLOCKS_PER_SEC << "\n" << std::endl;
	//std::cerr<<"get occ time: "<< double(abwtt.get_occ_time)/ CLOCKS_PER_SEC << "\n" << std::endl;

//}









/*
TEST (indexer, test_mt_dcs)	 //whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{

	std::string seq(""),test_seq(""), tmp("");
	std::vector<INTTYPE> rank1, rank2;
	
	std::vector<std::string> file_names (
			{
					"/run/shm/human/chrX.fa",
					"/run/shm/human/chrY.fa"
					
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(2000000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			seq += tmp;
			len+=tmp.size();
			if(len > limitLength) break;
		}
		//(*file_handles[i]).close();
	}
	seq+="$";
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	

	
	ABSequence<ABmpl> a ( seq, true); //sequence, ignore lower
	ABWT<ABSequence<ABmpl>> b (a);

	ABSequence<std::string> x ( seq );
	ABWT<ABSequence<std::string>> y (x);

	for(auto i(0); i<b.dcs.iDs.size();++i)
	{
		if(b.dcs.iDs[i] != y.dcs.iDs[i])
		{
			std::cout << b.dcs.iDs[i] << ":" << y.dcs.iDs[i];
			break;
		}
	}

	EXPECT_EQ (b.dcs.iDs, y.dcs.iDs);
	
	std::cout << std::endl;
	
}
*/


/*
TEST (indexer, test_result)	 //whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{
	std::string seq(""),test_seq(""), tmp("");
	std::vector<std::string> file_names (
			{
					"/run/shm/human/chrX.fa",
					"/run/shm/human/chrY.fa"
					
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
		
	for (int i=0; i<file_names.size(); i++)
	{
		INTTYPE limitLength(500000), len(0);
		std::getline(*file_handles[i],tmp);
		while(std::getline(*file_handles[i],tmp))
		{
			//if(tmp == std::string(50,'N')) continue;
			seq += tmp;
			len+=tmp.size();
			if(len > limitLength) break;
		}
		//(*file_handles[i]).close();
	}
	
	seq ="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	seq+="ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT";
	
	seq+="$";
	std::cout << "sequence size : " << seq.size() << std::endl;
	
	std::transform(seq.begin(), seq.end(), seq.begin(), (int (*)(int))std::toupper);
	
	
	std::vector<INTTYPE> qq( seq.size() );
	for (INTTYPE i =0; i< qq.size(); ++i)
		qq[i]=i;
	std::sort(qq.begin(), qq.end(), 
		[&]( INTTYPE a, INTTYPE b)
		{
			if ( strcmp(seq.c_str()+a, seq.c_str()+b ) < 0 )
				return true;
			else
				return false;
		}
	);
	

	
	
	//ABSequence<std::string> x ( seq );
	//ABWT<ABSequence<std::string>> y (x);
	//EXPECT_EQ (y.seq_table, qq);
	std::vector<std::pair<INTTYPE,INTTYPE>> iD_same_rank;
	std::vector< INTTYPE > iD;
	for(int i=0; i< seq.size(); ++i)
	{
		iD.push_back(i);
	}
	
	Bucket_sort< std::string, std::vector< INTTYPE > > bkt(seq, iD, iD.size(), iD_same_rank);
	bkt.setRecord_rank_enable();
	
	bkt.bucket_sort(0,iD.size(),0);
	
	EXPECT_EQ (iD, qq);
	
	
	std::cout << std::endl;
}
*/


