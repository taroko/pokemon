#include <fstream>
#include <deque>
#include <map>
#include <unordered_map>
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "../src/parser.hpp"
//#include "../src/bwt.hpp"
#include "gtest/gtest.h"

template <typename mapType, typename addType>
class addMap
{
public:
	// { geneType, {tail, reads_number} }
	void operator()(mapType &map, mapType value, addType test=0){
		typename mapType::iterator map_it;
		if((map_it = map.find( (*value.begin() ).first )) ==  map.end())
		{
			map.insert( { (*value.begin() ).first, (*value.begin() ).second } );
		}
		else
		{
			addMap< typename mapType::mapped_type , addType> am;
			am( (*map_it).second, (*value.begin() ).second , test);
		}	
	}
};
template <typename T>
class addMap<T,T>
{
public:
	void operator()(T &v1, T &v2, T v3){
		v1 += v2;
	}
};



TEST (LandScape, Normal)
{
	std::vector<std::string> file_names (
		{
			"/nfs_sharing/stefan/polII_bedGraph/PolII_Ser2P.fastq.mapped.bed.sorted.peak"
		});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<1; i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string, uint32_t, uint32_t, uint32_t > TUPLETYPE;

	FileParser < ParallelTypes::NORMAL, Bed, TUPLETYPE, CompressType::Plain, std::deque, 2 > LandScape;
	
	bool check_eof(true);
	do
	{
		check_eof=true;
		auto x = LandScape.Read ( file_handles );
		
		std::for_each (	x->begin(), x->end(),
			[&] (const std::pair<int, Bed<TUPLETYPE> >& Q)
			{
				if(!Q.second.eof_flag){
					check_eof=false;
					LandScape.push_back (Q.second);
				}
			}
		);
	}while(!check_eof);
	
	//chr , start, num
	std::map<std::string, std::map<uint32_t, uint32_t> > landscape;
	addMap< std::map<std::string, std::map<uint32_t, uint32_t> >, uint32_t> addmap;
	uint32_t scape_size = 10000;
	
	std::for_each (LandScape.begin(), LandScape.end(),
		[&] (const Bed<TUPLETYPE>& Q)
		{
			std::string chr( std::get<0>(Q.data) );
			std::uint32_t start( std::get<1>(Q.data) );
			std::uint32_t num( std::get<3>(Q.data) );
			addmap(landscape, {{chr,{{start/scape_size,num}} }} );
		}	
	);
	std::for_each (landscape.begin(), landscape.end(),
		[&] (const std::pair< std::string, std::map<uint32_t, uint32_t> > &Q)
		{
			std::for_each (Q.second.begin(), Q.second.end(),
				[&] (const std::pair< uint32_t, uint32_t > &K)
				{
					//std::cout << Q.first << "\t" << K.first*scape_size << "\t" << (K.first+1)*scape_size << "\t" << K.second << std::endl;
				}
			);
		}
	);
}


TEST (LandScape, Quickly)
{
	std::vector<std::string> file_names (
		{
			"/nfs_sharing/stefan/polII_bedGraph/PolII_Ser2P.fastq.mapped.bed.sorted.peak"
		});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<1; i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string, uint32_t, uint32_t, uint32_t > TUPLETYPE;

	FileParser < ParallelTypes::NORMAL, Bed, TUPLETYPE, CompressType::Plain, std::deque, 2 > LandScape;
	
	std::map<std::string, std::map<uint32_t, uint32_t> > landscape;
	addMap< std::map<std::string, std::map<uint32_t, uint32_t> >, uint32_t> addmap;
	
	uint32_t scape_size(10000), overlap_number(2);
	
	
	bool check_eof(true);
	do
	{
		check_eof=true;
		auto x = LandScape.Read ( file_handles );
		
		std::for_each (	x->begin(), x->end(),
			[&] (const std::pair<int, Bed<TUPLETYPE> >& Q)
			{
				if(!Q.second.eof_flag){
					check_eof=false;
					std::string chr( std::get<0>(Q.second.data) );
					uint32_t start( std::get<1>(Q.second.data) );
					uint32_t num( std::get<3>(Q.second.data) );
					addmap(landscape, {{chr,{{start/scape_size,num}} }} );
				}
			}
		);
	}while(!check_eof);
	
	std::for_each (landscape.begin(), landscape.end(),
		[&] (const std::pair< std::string, std::map<uint32_t, uint32_t> > &Q)
		{
			for (auto K : Q.second)
			{
				std::string chr(Q.first);
				uint32_t start(K.first*scape_size), end((K.first+overlap_number)*scape_size), number(0);
				
				for(uint32_t i(0); i < overlap_number; ++i)
				{
					auto it = (Q.second).find(K.first+i);
					if( it != Q.second.end() )
						number += (*it).second;
				}
				std::cout << chr << "\t" << start << "\t" << end << "\t" << number << std::endl;
			}
		}
	);
}

