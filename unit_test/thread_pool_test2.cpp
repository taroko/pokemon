#include "../src/thread_pool_update.hpp"
//#include "../src/thread_pool_update_map_version.hpp"
#include <iostream>
#include "gtest/gtest.h"
#include <fstream>

template < typename T >
void wait ( T a, T b, T& c )
{
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
};


template < typename T >
void adder ( T a, T b, T& c )
{
	c = a+b;
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
//	std::cerr<<a<<'\t'<<b<<'\t'<<"sum "<<c<<std::endl;

	std::ofstream QQ;
	QQ.open ( "~/test/write_add", std::ios::app );
	QQ << c;
	QQ.close();
};

template < typename T >
void multiplier ( T a, T b, T& c )
{
	c = a * b;
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
//	std::this_thread::sleep_for(std::chrono::seconds(1));
//	std::cerr<<a<<'\t'<<b<<'\t'<<"product "<<c<<std::endl;
	std::ofstream QQ;
	QQ.open ( "~/test/write_multiply", std::ios::app );
	QQ << c;
	QQ.close();
};

template < typename T >
void deducer ( T a, T b, T& c )
{
	c = a - b;
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
//	std::cerr<<a<<'\t'<<b<<'\t'<<"deduct "<<c<<std::endl;
	std::ofstream QQ;
 	QQ.open ( "~/test/write_deduce", std::ios::app );
	QQ << c;
	QQ.close();
};

TEST (brutal_post, test1)
{
	double a = 3, b = 4;
	std::vector<double> c ({-5566.0});
	auto i = GlobalPool.BrutalJobPost (boost::bind (multiplier<double>, a, b, std::ref(c[0]) ) );
	GlobalPool.FlushPool ();
	std::cerr<<"brutal post return "<<c[0]<<'\n';
}

TEST (brutal_post, test2)
{
	double a = 3, b = 4;
	std::vector<double> c (5, -5566.0);
	auto i = GlobalPool.BrutalJobPost (boost::bind (adder<double>, a, b, std::ref(c[3]) ) );
	GlobalPool.BrutalFlushOne (i);
	std::cerr<<"brutal post return "<<c[3]<<'\n';
}

TEST (brutal_post, test3)
{
	double a = 3, b = 4;
	std::vector<double> c ({-5566.0});
	auto i = GlobalPool.BrutalJobPost (boost::bind (deducer<double>, a, b, std::ref(c[0]) ) );
	GlobalPool.ResetPool ();
	std::cerr<<"brutal post return "<<c[0]<<'\n';
}

//int main (void)
TEST ( flush, test1 )
{
	double a = 3, b = 4;
	std::vector<double> c (305, -5566);

	// post post post
	auto i = GlobalPool.JobPost (boost::bind (wait<double>, a, b, std::ref(c[0]) ), std::vector<size_t>(0) );//, std::ref(Q) );
	std::cerr<<"post return i "<<i<<'\n';

	auto j = GlobalPool.JobPost (boost::bind (adder<double>, a, b, std::ref(c[1]) ), std::vector<size_t>(0));//, std::ref(Q) );
	std::cerr<<"post return j "<<j<<'\n';

	for ( auto i=2; i<100; ++i)
	{
		auto k = GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[i]) ), std::vector<size_t>({1}));//, std::ref(Q) );
		std::cerr<<"post return k "<<k<<'\n';
	}

	auto l = GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[100]) ), std::vector<size_t>({2,3,4,5,6,99}));//, std::ref(Q) );
	std::cerr<<"post return l "<<l<<'\n';

	for (auto i=101; i<199; ++i)
	{
		auto m = GlobalPool.JobPost (boost::bind (adder<double>, a, b, std::ref(c[i]) ), std::vector<size_t>({100}));//, std::ref(Q) );
		std::cerr<<"post return m "<<m<<'\n';
	}

	for (auto i=199; i<300; ++i)
	{
		auto n = GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[i]) ), std::vector<size_t>({101, 103, 109, 198}));//, std::ref(Q) );
		std::cerr<<"post return n "<<n<<'\n';
	}

	auto o = GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[300]) ), std::vector<size_t>({299, 235, 105}));//, std::ref(Q) );
	std::cerr<<"post return n "<<o<<'\n';

	// flush
	std::cerr<<"flushing"<<std::endl;
	GlobalPool.FlushPool ();
	std::cerr<<"flushed"<<std::endl;

	{	//post post post again 
		auto i = GlobalPool.JobPost (boost::bind (wait<double>, a, b, std::ref(c[0]) ), std::vector<size_t>(0) );//, std::ref(Q) );
		std::cerr<<"post return i "<<i<<'\n';

		auto j = GlobalPool.JobPost (boost::bind (adder<double>, a, b, std::ref(c[1]) ), std::vector<size_t>(0));//, std::ref(Q) );
		std::cerr<<"post return j "<<j<<'\n';

		for ( auto i=2; i<100; ++i)
		{
			auto k = GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[i]) ), std::vector<size_t>({1}));//, std::ref(Q) );
			std::cerr<<"post return k "<<k<<'\n';
		}

		auto l = GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[100]) ), std::vector<size_t>({2,3,4,5,6,99}));//, std::ref(Q) );
		std::cerr<<"post return l "<<l<<'\n';

		for (auto i=101; i<199; ++i)
		{
			auto m = GlobalPool.JobPost (boost::bind (adder<double>, a, b, std::ref(c[i]) ), std::vector<size_t>({100}));//, std::ref(Q) );
			std::cerr<<"post return m "<<m<<'\n';
		}

		for (auto i=199; i<300; ++i)
		{
			auto n = GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[i]) ), std::vector<size_t>({101, 103, 109, 198}));//, std::ref(Q) );
			std::cerr<<"post return n "<<n<<'\n';
		}

		auto o = GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[300]) ), std::vector<size_t>({299, 235, 105}));//, std::ref(Q) );
		std::cerr<<"post return n "<<o<<'\n';
	}

	// actual flush
	std::cerr<<"flushing"<<std::endl;
	GlobalPool.ResetPool ();//FlushPool (1);
	std::cerr<<"flushed"<<std::endl;

	std::for_each ( c.begin(), c.end(),
			[] ( double& q )
			{ std::cerr<<q<< ' ';});
	std::cerr<<std::endl;
}


