#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include "boost/serialization/map.hpp"
#include "../src/format/raw_bed.hpp"
#include "../src/format/sam.hpp"
#include "../src/file_reader_impl.hpp"

int main (void)
{
    typedef std::tuple <
        std::string, //QNAME
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
		UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
//		UserDefineContent
//        int, // NH
//        std::string //TailSeq
                        > TUPLETYPE;

	std::string file_name ("test.sam");
	std::ifstream file_handle (file_name);
	std::vector<std::string> QQ ({file_name});
	FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);

	size_t index=0;
	std::vector <Sam <TUPLETYPE> > AAA;
	std::vector < RawBed<> > BBB;
	std::map < RawBed<>, int > CCC;
	std::map < RawBed<>, uint16_t > DDD, DDDb;

	const size_t RawBedSize = sizeof (RawBed<>);
std::cerr<<"size of rawbed "<<RawBedSize<<'\n';
	while (index!=3)
	{
std::cerr<<"index "<<index<<'\n';
		const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
//		BBB.push_back (RawBed <>(qq));
		RawBed <> yy (qq);
std::cerr<<"input rawbed\n"<<yy;
//		RawBed <>* Q = &yy;
//		std::cerr<<"test "<< sizeof (*Q)<<std::endl;
		++CCC[yy];
//		++CCC[RawBed <>(qq)];

		//uint16_t* ptr = 
		++ (*((&(DDD[RawBed<>(qq)]))-9));
//		uint16_t* ptr = (uint16_t*) ( &( DDD[RawBed<>(qq)]) );	
//		++(*(ptr-5));
		AAA.push_back (qq);
		++index;
	}
std::cerr<<"DDD"<<'\n';
	for (auto itr=DDD.begin(); itr!=DDD.end(); ++itr)
	{
//		std::cerr<<"chridx_ "<<(itr->first).chr_idx_<<'\n';
//		std::cerr<<"tail_length "<<(itr->first).tail_length_<<'\n';
//		std::cerr<<"length "<<(itr->first).length_<<'\n';
		std::cerr<<" multiple alignment count "<<(itr->first).multiple_alignment_site_count_<<'\n';
		std::cerr<<"reads_count_ "<<(itr->first).reads_count_<<'\n';
		std::cerr<<"start_ "<<(itr->first).start_<<'\n';
		std::cerr<<"tail mismatch "<<(int) (itr->first).tail_mismatch_<<'\n';
	}	
std::cerr<<"CCC"<<'\n';
	for (auto itr=CCC.begin(); itr!=CCC.end(); ++itr)
	{
//		std::cerr<<"chridx_ "<<(itr->first).chr_idx_<<'\n';
//		std::cerr<<"tail_length "<<(itr->first).tail_length_<<'\n';
//		std::cerr<<"length "<<(itr->first).length_<<'\n';
		std::cerr<<" multiple alignment count "<<(itr->first).multiple_alignment_site_count_<<'\n';
		std::cerr<<"reads_count_ "<<(itr->first).reads_count_<<'\n';
		std::cerr<<"start_ "<<(itr->first).start_<<'\n';
//		std::cerr<<"tail mismatch "<<(itr->first).tail_mismatch_<<'\n';
		
		std::cerr<<itr->second<<'\n';
	}
std::cerr<<"archive output"<<'\n';

    std::ofstream ofs ("yy");//("rawbed_serialization_test.txt");
    boost::archive::text_oarchive arc0 (ofs);
	int ii = DDD.size();
//	arc0 & ii;
	//for (auto& Q : DDD)
	//    arc0 & Q.first;
	arc0 & DDD;
    ofs.close();
std::cerr<<"archive input"<<'\n';

    std::ifstream ifs ("yy");//("rawbed_serialization_test.txt");
    boost::archive::text_iarchive arc1 (ifs);
	arc1 & DDDb;
//	int size;
//	arc1 & size;
//	for (auto i=0; i!=size; ++i)
//	{
//		RawBed <> yy;	
//		arc1 & yy;
//		DDDb.insert ({yy, 0});
//	}

//	for (auto ind=0; ind!=DDD.size(); ++ind)
//	{
//		RawBed <> yy;	
  //  	arc1 & yy;
//		DDDb.insert ({yy, 0});
//		if (itr->first != itr_D->first)
//			std::cerr<<"fucked"<<'\n';
//	}
	//for (auto& q : DDDb)
	//	std::cerr<<q.first;
};
	

//	std::sort (BBB.begin(), BBB.end());

	//for (auto i=0; i!=4; ++i)
	//{
	//	bool b1 = BBB[i] < BBB[i+1], b2 = BBB[i+1] < BBB[i];
	//	std::cerr<<"< overload "<<i<<"<"<<i+1<<" & "<<i+1<<"<"<<i<<'\t'<<b1<<'\t'<<b2<<'\n';
	//}
	//for (auto itr=CCC.begin(); itr!=CCC.end(); ++itr)
	//	std::cerr<<itr->second<<'\n';

