#include <bitset>
#include <cstdint>
#include <vector>
#include <fstream>
#include <boost/algorithm/string/split.hpp>
#include "../src/format/sam.hpp"
#include "../src/format/sam_reader_impl.hpp"
#include "../src/tuple_utility.hpp"
#include "../src/wrapper_tuple_utility.hpp"
#include "../src/format_io_iterator.hpp"
#include "../src/constant_def.hpp"
#include <boost/algorithm/string.hpp>
#include "gtest/gtest.h"

TEST(Sam_reader, io_end)
{
	typedef std::tuple <
						std::string, //header 
						std::string, //QNAME
						int,//std::bitset<11>, //FLAG
						std::string, //RNAME
						uint32_t, //POS
						int, //MAPQ
						std::string, //CIGAR
						std::string, //RNEXT
						uint32_t, //PNEXT
						int, //TLEN
						std::string
						> TUPLETYPE;
	FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test;
	EXPECT_EQ( ( (Sam_Reader_test.io_end() ) -> eof_flag ), true );
};

TEST(Sam_reader, serial_get_next_entry)
{
	typedef std::tuple < 
//		std::string, //header
		std::string, //QNAME
		size_t, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
		std::string, //RNAME
		uint64_t, //POS
		int, //MAPQ
		std::string, //CIGAR
		std::string, //RNEXT
		uint64_t, //PNEXT
		int64_t, //TLEN
		std::string,//, //SEQ
		std::string, //QUAL
		//int, // NH
		std::string //TailSeq
						> TUPLETYPE;
	std::string file_name ("test_file/test.sam");
	std::ifstream file_handle (file_name);
		std::vector<std::string> QQ ({file_name});
		FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);

	int i = 0;
//	while (i<100)
//	{
		auto x = Sam_Reader_test.get_next_entry (0);
//		++i;
//	}
//		EXPECT_EQ (std::get< 0> (x.data), "");
		EXPECT_EQ (std::get< 0> (x.data), "HWI-ST570:51:D122LACXX:3:2308:7509:172589");
		EXPECT_EQ (std::get< 1> (x.data), 89);
		EXPECT_EQ (std::get< 2> (x.data), "chr7");
		EXPECT_EQ (std::get< 3> (x.data), 51101378);
		EXPECT_EQ (std::get< 4> (x.data), 40);
		EXPECT_EQ (std::get< 5> (x.data), "50M" );
		EXPECT_EQ (std::get< 6> (x.data), "=" );
		EXPECT_EQ (std::get< 7> (x.data), 51101378 );
		EXPECT_EQ (std::get< 8> (x.data), 0 );
		EXPECT_EQ (std::get< 9> (x.data), "TCTATGAAGATGGGGTTGGGAAGGGGCTGGGTGTAGGACTTAGATTGGAG");
		EXPECT_EQ (std::get<10> (x.data), "IFIGEIIGIEIIIGGGHEIFIJJIGGHIGGIJJIJJJHHHHHFFFDD@@B");
		EXPECT_EQ (std::get<11> (x.data), "AS:i:-6 XN:i:0 XM:i:1 XO:i:0 XG:i:0 NM:i:1 MD:Z:5A44 YT:Z:UP"); 
		TupleUtility<TUPLETYPE, std::tuple_size<TUPLETYPE>::value>:: PrintTuple(std::cerr, x.data);
}
/*
TEST(Sam_reader, serial_get_all_entry)
{
	typedef std::tuple < std::string, //header 
						std::string, //QNAME
						int,//std::bitset<11>, //FLAG
						std::string, //RNAME
						uint32_t, //POS
						int, //MAPQ
						std::string, //CIGAR
						std::string, //RNEXT
						uint32_t, //PNEXT
						int, //TLEN
						std::string
						> TUPLETYPE;
	std::string file_name ("/Users/obigbando/Documents/work/test_file/test.sam");
	std::vector< Sam <TUPLETYPE> > result;
	FileReader_impl<Sam, TUPLETYPE> :: get_all_entry (file_name, result);
	EXPECT_EQ (std::get<0> (result[0].data), "");
	EXPECT_EQ (std::get<1> (result[0].data), "HWI-ST570:51:D122LACXX:3:2308:7509:172589");
	EXPECT_EQ (std::get<2> (result[0].data), 89);
	EXPECT_EQ (std::get<3> (result[0].data), "chr7");
	EXPECT_EQ (std::get<4> (result[0].data), 51101378);
	EXPECT_EQ (std::get<5> (result[0].data), 40);
	EXPECT_EQ (std::get<6> (result[0].data), "50M" );
	EXPECT_EQ (std::get<7> (result[0].data), "=" );
	EXPECT_EQ (std::get<8> (result[0].data), 51101378 );
	EXPECT_EQ (std::get<9> (result[0].data), 0 );
	EXPECT_EQ (std::get<10> (result[0].data), "TCTATGAAGATGGGGTTGGGAAGGGGCTGGGTGTAGGACTTAGATTGGAG IFIGEIIGIEIIIGGGHEIFIJJIGGHIGGIJJIJJJHHHHHFFFDD@@B AS:i:-6 XN:i:0 XM:i:1 XO:i:0 XG:i:0 NM:i:1 MD:Z:5A44 YT:Z:UP");
	EXPECT_EQ (result[0].eof_flag, false);

	EXPECT_EQ (std::get<0> (result[21].data), "");
	EXPECT_EQ (std::get<1> (result[21].data), "HWI-ST570:51:D122LACXX:3:2308:7513:172663");
	EXPECT_EQ (std::get<2> (result[21].data), 147);
	EXPECT_EQ (std::get<3> (result[21].data), "chr13");
	EXPECT_EQ (std::get<4> (result[21].data), 115118658);
	EXPECT_EQ (std::get<5> (result[21].data), 42);
	EXPECT_EQ (std::get<6> (result[21].data), "50M" );
	EXPECT_EQ (std::get<7> (result[21].data), "=" );
	EXPECT_EQ (std::get<8> (result[21].data), 115118535 );
	EXPECT_EQ (std::get<9> (result[21].data), -173 );
	EXPECT_EQ (std::get<10> (result[21].data), "GGTGGGGACCCTCTACTGGATTCTTGGCATTCACTGGCAACATGAACGGT EGDGGIGFIIIGJJHIGJIJJJJIJJJIIIHEHHGIJHHGHHFFFFFC@@ AS:i:0 XN:i:0 XM:i:0 XO:i:0 XG:i:0 NM:i:0 MD:Z:50 YS:i:0 YT:Z:CP");
	EXPECT_EQ (result[21].eof_flag, false);
}
*/

/*
TEST(Sam_reader, multi_thread)
{
	typedef std::tuple < std::string, //QNAME
						int,//std::bitset<11>, //FLAG
						std::string, //RNAME
						uint32_t, //POS
						int, //MAPQ
						std::string, //CIGAR
						std::string, //RNEXT
						uint32_t, //PNEXT
						int, //TLEN
//						wrapper< 2, std::vector < std::string > > //SEQ&QUAL
						std::string
						> TUPLETYPE;
	std::vector < std::string > file_names ({
	"/Users/obigbando/Documents/work/project/test.txt",
	"/Users/obigbando/Documents/work/project/test1.txt",
	"/Users/obigbando/Documents/work/project/test2.txt"
   });

	std::vector<std::ifstream*> file_handles;
	for (int i=0 ;i<file_names.size(); i++)
		file_handles.push_back ( new std::ifstream( file_names[i] ) );
	std::map <int, Sam <TUPLETYPE> > result;
	FileReader < ParallelTypes::M_T, Sam, TUPLETYPE > GG (&result);//(3, &result) ;
	GG.Read ( file_handles, result );
	GG.ptr_to_GlobalPool->FlushPool();
	GG.ptr_to_GlobalPool->ChangePoolSize (5);
//	GG.Printer ();
// result[0]
		EXPECT_EQ (std::get<0> (result[0].data), "HWI-ST570:51:D122LACXX:3:2308:7509:172589");
		EXPECT_EQ (std::get<1> (result[0].data), 89);
		EXPECT_EQ (std::get<2> (result[0].data), "chr7");
		EXPECT_EQ (std::get<3> (result[0].data), 51101378);
		EXPECT_EQ (std::get<4> (result[0].data), 40);
		EXPECT_EQ (std::get<5> (result[0].data), "50M" );
		EXPECT_EQ (std::get<6> (result[0].data), "=" );
		EXPECT_EQ (std::get<7> (result[0].data), 51101378 );
		EXPECT_EQ (std::get<8> (result[0].data), 0 );
		EXPECT_EQ (std::get<9> (result[0].data), "TCTATGAAGATGGGGTTGGGAAGGGGCTGGGTGTAGGACTTAGATTGGAG IFIGEIIGIEIIIGGGHEIFIJJIGGHIGGIJJIJJJHHHHHFFFDD@@B AS:i:-6 XN:i:0 XM:i:1 XO:i:0 XG:i:0 NM:i:1 MD:Z:5A44 YT:Z:UP");

// result[1]
		EXPECT_EQ (std::get<0> (result[1].data), "HWI-ST570:51:D122LACXX:3:2308:7747:172603");
		EXPECT_EQ (std::get<1> (result[1].data), 83);
		EXPECT_EQ (std::get<2> (result[1].data), "chr5");
		EXPECT_EQ (std::get<3> (result[1].data), 30856428);
		EXPECT_EQ (std::get<4> (result[1].data), 42);
		EXPECT_EQ (std::get<5> (result[1].data), "50M" );
		EXPECT_EQ (std::get<6> (result[1].data), "=" );
		EXPECT_EQ (std::get<7> (result[1].data), 30856331 );
		EXPECT_EQ (std::get<8> (result[1].data), -147 );
		EXPECT_EQ (std::get<9> (result[1].data), "TCTGTGAAAAGCAATGTGAAGTCATTGTAGAGCGTCATTCTGAGAAATGA BHGF?H@HEHIIIIIGIIIGIHIHDEHGHGHA@HDIGDDF>DFFFDD<@< AS:i:0 XN:i:0 XM:i:0 XO:i:0 XG:i:0 NM:i:0 MD:Z:50 YS:i:-2 YT:Z:CP");

// result[2]
		EXPECT_EQ (std::get<0> (result[2].data), "HWI-ST570:51:D122LACXX:3:2308:7509:172589");
		EXPECT_EQ (std::get<1> (result[2].data), 89);
		EXPECT_EQ (std::get<2> (result[2].data), "chr7");
		EXPECT_EQ (std::get<3> (result[2].data), 51101378);
		EXPECT_EQ (std::get<4> (result[2].data), 40);
		EXPECT_EQ (std::get<5> (result[2].data), "50M" );
		EXPECT_EQ (std::get<6> (result[2].data), "=" );
		EXPECT_EQ (std::get<7> (result[2].data), 51101378 );
		EXPECT_EQ (std::get<8> (result[2].data), 0 );
		EXPECT_EQ (std::get<9> (result[2].data), "TCTATGAAGATGGGGTTGGGAAGGGGCTGGGTGTAGGACTTAGATTGGAG IFIGEIIGIEIIIGGGHEIFIJJIGGHIGGIJJIJJJHHHHHFFFDD@@B AS:i:-6 XN:i:0 XM:i:1 XO:i:0 XG:i:0 NM:i:1 MD:Z:5A44 YT:Z:UP");
}
*/

/*
TEST(Sam_reader, read_bam)
{
	typedef std::tuple < std::string, //QNAME
						int,//std::bitset<11>, //FLAG
						std::string, //RNAME
						uint32_t, //POS
						int, //MAPQ
						std::string, //CIGAR
						std::string, //RNEXT
						uint32_t, //PNEXT
						int, //TLEN
						std::string
						> TUPLETYPE;
	std::string Fin ("/Users/obigbando/Documents/work/test_file/readtest.bam"), Fout ("/Users/obigbando/Documents/work/test_file/result.sam");
	FileReader_impl<Sam, TUPLETYPE> :: read_bam ( const_cast<char*> (Fin.c_str()), const_cast<char*> (Fout.c_str()) );
}

TEST(Sam_reader, write_bam)
{
	typedef std::tuple < std::string, //QNAME
						int,//std::bitset<11>, //FLAG
						std::string, //RNAME
						uint32_t, //POS
						int, //MAPQ
						std::string, //CIGAR
						std::string, //RNEXT
						uint32_t, //PNEXT
						int, //TLEN
						std::string
						> TUPLETYPE;
	std::string Fin ("/Users/obigbando/Documents/work/test_file/writetest.sam"), Fout ("/Users/obigbando/Documents/work/test_file/result.bam");
	FileReader_impl<Sam, TUPLETYPE> :: write_bam ( const_cast<char*> (Fin.c_str()), const_cast<char*> (Fout.c_str()) );
}
*/
