#include <tuple>
#include <vector>
#include <string>
#include <cstdint>
#include <iostream>
#include "gtest/gtest.h"
#include "../src/format/sam.hpp"
#include "../src/tuple_utility.hpp"
#include <fstream>
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"

std::string s10 ("@ff @gg"),
	s11 ( "HWI-ST570:51:D122LACXX:3:2308:7747:172603" ),
	s12 ( "chr5" ),
	s13 ( "50M" ),
	s14 ("=" ),
	s15 ( "TCTGTGAAAAGCAATGTGAAGTCATTGTAGAGCGTCATTCTGAGAAATGA BHGF?H@HEHIIIIIGIIIGIHIHDEHGHGHA@HDIGDDF>DFFFDD<@< AS:i:0 XN:i:0 XM:i:0 XO:i:0 XG:i:0 NM:i:0 MD:Z:50 YS:i:-2 YT:Z:CP" );
int int11 = 83, int12 = 42, int13 = -147 ;
uint32_t start1 = 30856428, end1 = 30856331;

TEST(SAM, stream_operator_overload)
{
	typedef std::tuple < 
                        std::string, //QNAME                                                                                                                         
                        SAM_FLAG, //int, //FLAG
                        std::string, //RNAME
                        uint64_t, //POS
                        int, //MAPQ
                        std::string, //CIGAR
                        std::string, //RNEXT
                        uint64_t, //PNEXT
                        int64_t, //TLEN
                        std::string, //SEQ
                        std::string, //QUAL
                        int, // NH
                        std::string //TailSeq
	  				   > 
	TUPLETYPE; 

	TUPLETYPE TP2 
	{
	"ABCDE",                                                                                                                    
	SAM_FLAG::REVERSE_COMPLEMENTED,
	"chr11",
	1357913579,
	255,
	"ABCABCABC",
	"*",
	0,
	0,
	"wwwww",
	"ggyy",
	0,
	"bbb"
	};
	Sam <TUPLETYPE> QR (TP2);

	EXPECT_EQ (std::get<0> (QR.data), "ABCDE" ); 
	EXPECT_EQ (std::get<1> (QR.data), SAM_FLAG::REVERSE_COMPLEMENTED ); 
	EXPECT_EQ (std::get<2> (QR.data), "chr11");
	EXPECT_EQ (std::get<3> (QR.data), 1357913579 );
	EXPECT_EQ (std::get<4> (QR.data), 255);
	EXPECT_EQ (std::get<5> (QR.data), "ABCABCABC");
	EXPECT_EQ (std::get<6> (QR.data), "*" );
	EXPECT_EQ (std::get<7> (QR.data), 0 );
	EXPECT_EQ (std::get<8> (QR.data), 0 );
	EXPECT_EQ (std::get<9> (QR.data), "wwwww" );
	EXPECT_EQ (std::get<10> (QR.data), "ggyy" );
	EXPECT_EQ (std::get<11> (QR.data), 0 );
	EXPECT_EQ (std::get<12> (QR.data), "bbb" );
	EXPECT_EQ (QR.eof_flag, false);

	std::ofstream qq ("test_file/sam_test_file");
	qq << QR;
	qq << QR;
	qq.close();
	std::ifstream qr ("test_file/sam_test_file");
	std::string output;
	std::getline (qr, output);//qr >> output;
	EXPECT_EQ (output, "ABCDE\t16\tchr11\t1357913579\t255\tABCABCABC\t*\t0\t0\twwwww\tggyy\t0\tbbb");
}

TEST(SAM, constructor_default)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
//	Default constructor
	Sam <TUPLETYPE> TT;
	EXPECT_EQ (std::get<0> (TT.data), std::string("") );
	EXPECT_EQ (std::get<1> (TT.data), std::string("") );
	EXPECT_EQ (std::get<2> (TT.data), 0);
	EXPECT_EQ (std::get<3> (TT.data), std::string("") );
	EXPECT_EQ (std::get<4> (TT.data), 0);
	EXPECT_EQ (std::get<5> (TT.data), 0);
	EXPECT_EQ (std::get<6> (TT.data), std::string("") );
	EXPECT_EQ (std::get<7> (TT.data), std::string("") );
	EXPECT_EQ (std::get<8> (TT.data), 0);
	EXPECT_EQ (std::get<9> (TT.data), 0);
	EXPECT_EQ (std::get<10> (TT.data), std::string("") );
	EXPECT_EQ (TT.eof_flag, false);
	//std::cerr<<"default constructor done test "<<std::endl;
};

TEST(SAM, constructor_tuple)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
//	Constructor with a tuple <TUPLETYPE> 
	auto yy = std::make_tuple (s10, s11, int11, s12, start1, int12, s13, s14, end1, int13, s15);
	Sam <TUPLETYPE> gg (yy);
	EXPECT_EQ (std::get<0> (gg.data), s10 ); 
	EXPECT_EQ (std::get<1> (gg.data), s11 ); 
	EXPECT_EQ (std::get<2> (gg.data), int11);
	EXPECT_EQ (std::get<3> (gg.data), s12 );
	EXPECT_EQ (std::get<4> (gg.data), start1);
	EXPECT_EQ (std::get<5> (gg.data), int12);
	EXPECT_EQ (std::get<6> (gg.data), s13 );
	EXPECT_EQ (std::get<7> (gg.data), s14 );
	EXPECT_EQ (std::get<8> (gg.data), end1 );
	EXPECT_EQ (std::get<9> (gg.data), int13 );
	EXPECT_EQ (std::get<10> (gg.data), s15 );
	EXPECT_EQ (gg.eof_flag, false);
	//std::cerr << "#2 normal constructor with tuple <TUPLETYPE> done test " << std::endl;
};

TEST(SAM, copy_constructor)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
	auto yy = std::make_tuple (s10, s11, int11, s12, start1, int12, s13, s14, end1, int13, s15);
	Sam <TUPLETYPE> gg (yy);
	Sam <TUPLETYPE> XX (gg);
	EXPECT_EQ (std::get<0> (XX.data), s10 );
	EXPECT_EQ (std::get<1> (XX.data), s11 ); 
	EXPECT_EQ (std::get<2> (XX.data), int11);
	EXPECT_EQ (std::get<3> (XX.data), s12 );
	EXPECT_EQ (std::get<4> (XX.data), start1);
	EXPECT_EQ (std::get<5> (XX.data), int12);
	EXPECT_EQ (std::get<6> (XX.data), s13 );
	EXPECT_EQ (std::get<7> (XX.data), s14 );
	EXPECT_EQ (std::get<8> (XX.data), end1);
	EXPECT_EQ (std::get<9> (XX.data), int13);
	EXPECT_EQ (std::get<10> (XX.data), s15 );
	EXPECT_EQ (XX.eof_flag, false);
};

TEST(SAM, normal_assignment)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
//	Constructor with a tuple <TUPLETYPE> 
	auto YY = std::make_tuple (s10, s11, int11, s12, start1, int12, s13, s14, end1, int13, s15);
	
	Sam < TUPLETYPE > GG (YY);
	Sam < TUPLETYPE > AA;
	AA = GG;
	EXPECT_EQ (std::get<0> (AA.data), s10 ); 
	EXPECT_EQ (std::get<1> (AA.data), s11 ); 
	EXPECT_EQ (std::get<2> (AA.data), int11);
	EXPECT_EQ (std::get<3> (AA.data), s12 );
	EXPECT_EQ (std::get<4> (AA.data), start1);
	EXPECT_EQ (std::get<5> (AA.data), int12);
	EXPECT_EQ (std::get<6> (AA.data), s13 );
	EXPECT_EQ (std::get<7> (AA.data), s14 );
	EXPECT_EQ (std::get<8> (AA.data), end1 );
	EXPECT_EQ (std::get<9> (AA.data), int13 );
	EXPECT_EQ (std::get<10> (AA.data), s15 );
	EXPECT_EQ (AA.eof_flag, false);
	//std::cerr << "normal assignment done test " << std::endl;
};

TEST(SAM, eof)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
	bool EofFlag (false);
	Sam < TUPLETYPE> SS (EofFlag);
	EXPECT_EQ (SS.eof_flag, false );
	EXPECT_EQ (std::get<0> (SS.data), std::string("") ); 
	EXPECT_EQ (std::get<1> (SS.data), std::string("") ); 
	EXPECT_EQ (std::get<2> (SS.data), 0);
	EXPECT_EQ (std::get<3> (SS.data), std::string("") );
	EXPECT_EQ (std::get<4> (SS.data), 0);
	EXPECT_EQ (std::get<5> (SS.data), 0);
	EXPECT_EQ (std::get<6> (SS.data), std::string("") );
	EXPECT_EQ (std::get<7> (SS.data), std::string("") );
	EXPECT_EQ (std::get<8> (SS.data), 0);
	EXPECT_EQ (std::get<9> (SS.data), 0);
	EXPECT_EQ (std::get<10> (SS.data), std::string("") );
	//std::cerr<<"constructor with eof_flag of false done test "<<std::endl;

    bool EofFlag2 (true);
    Sam < TUPLETYPE> SS2 (EofFlag2);
    EXPECT_EQ (SS2.eof_flag, true ); 
    EXPECT_EQ (std::get<0> (SS2.data), std::string("") ); 
    EXPECT_EQ (std::get<1> (SS2.data), std::string("") ); 
    EXPECT_EQ (std::get<2> (SS2.data), 0);
    EXPECT_EQ (std::get<3> (SS2.data), std::string("") );
    EXPECT_EQ (std::get<4> (SS2.data), 0);
    EXPECT_EQ (std::get<5> (SS2.data), 0);
    EXPECT_EQ (std::get<6> (SS2.data), std::string("") );
    EXPECT_EQ (std::get<7> (SS2.data), std::string("") );
    EXPECT_EQ (std::get<8> (SS2.data), 0);
    EXPECT_EQ (std::get<9> (SS2.data), 0);
    EXPECT_EQ (std::get<10> (SS2.data), std::string("") );
    //std::cerr<<"constructor with eof_flag of true done test "<<std::endl;
};

TEST(SAM, move_constructor)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
	auto YY = std::make_tuple (s10, s11, int11, s12, start1, int12, s13, s14, end1, int13, s15);
	
	Sam < TUPLETYPE > GG (YY);
	Sam <TUPLETYPE> ZZ (std::move(GG));
	EXPECT_EQ (std::get<0> (ZZ.data), s10 ); 
	EXPECT_EQ (std::get<1> (ZZ.data), s11 ); 
	EXPECT_EQ (std::get<2> (ZZ.data), int11);
	EXPECT_EQ (std::get<3> (ZZ.data), s12 );
	EXPECT_EQ (std::get<4> (ZZ.data), start1);
	EXPECT_EQ (std::get<5> (ZZ.data), int12);
	EXPECT_EQ (std::get<6> (ZZ.data), s13 );
	EXPECT_EQ (std::get<7> (ZZ.data), s14 );
	EXPECT_EQ (std::get<8> (ZZ.data), end1 );
	EXPECT_EQ (std::get<9> (ZZ.data), int13 );
	EXPECT_EQ (std::get<10> (ZZ.data), s15 );
	EXPECT_EQ (ZZ.eof_flag, false);
	//std::cerr << "move constructor done test " << std::endl;
};

TEST(SAM, move_assignment)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
	auto YY = std::make_tuple (s10, s11, int11, s12, start1, int12, s13, s14, end1, int13, s15);
	Sam < TUPLETYPE > GG (YY);

//	move assignment
	Sam <TUPLETYPE> WW;
	WW = std::move (GG);
	EXPECT_EQ (std::get<0> (WW.data), s10 ); 
	EXPECT_EQ (std::get<1> (WW.data), s11 ); 
	EXPECT_EQ (std::get<2> (WW.data), int11);
	EXPECT_EQ (std::get<3> (WW.data), s12 );
	EXPECT_EQ (std::get<4> (WW.data), start1);
	EXPECT_EQ (std::get<5> (WW.data), int12);
	EXPECT_EQ (std::get<6> (WW.data), s13 );
	EXPECT_EQ (std::get<7> (WW.data), s14 );
	EXPECT_EQ (std::get<8> (WW.data), end1);
	EXPECT_EQ (std::get<9> (WW.data), int13);
	EXPECT_EQ (std::get<10> (WW.data), s15 );
	EXPECT_EQ (WW.eof_flag, false);
	//std::cerr << "move assignment done test " << std::endl;
};

TEST(SAM, serialization)
{
	typedef std::tuple < 
						 std::string, //header
						 std::string, //QNAME
						 int, //FLAG
						 std::string, //RNAME
						 uint32_t, //POS
						 int, //MAPQ
						 std::string, //CIGAR
						 std::string, //RNEXT
						 uint32_t, //PNEXT
						 int, //TLEN
						 std::string //SEQ&QUAL
	  				   > 
	TUPLETYPE;						 
	auto YY = std::make_tuple (s10, s11, int11, s12, start1, int12, s13, s14, end1, int13, s15);
	Sam < TUPLETYPE > WW (YY);

	Sam<TUPLETYPE> k( WW );
	Sam<TUPLETYPE> kk;
	std::ofstream ofs ("test_file/sam_serialization_test");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & k;
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("test_file/sam_serialization_test");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & kk;
    EXPECT_EQ (std::get<0> (kk.data), s10 ); 
    EXPECT_EQ (std::get<1> (kk.data), s11 ); 
    EXPECT_EQ (std::get<2> (kk.data), int11);
    EXPECT_EQ (std::get<3> (kk.data), s12 );
    EXPECT_EQ (std::get<4> (kk.data), start1);
    EXPECT_EQ (std::get<5> (kk.data), int12);
    EXPECT_EQ (std::get<6> (kk.data), s13 );
    EXPECT_EQ (std::get<7> (kk.data), s14 );
    EXPECT_EQ (std::get<8> (kk.data), end1);
    EXPECT_EQ (std::get<9> (kk.data), int13);
    EXPECT_EQ (std::get<10> (kk.data), s15 );
	EXPECT_EQ (kk.eof_flag, false);	

	ifs.close();
	//std::cerr << "Serialization done test " << std::endl;
};

TEST(Sam, static_assert)
{
//	Sam <int> gg;
//	std::string s1 ("fucker");
///	Sam <> aa = s1;
//	Sam <> bb (s1);
}

