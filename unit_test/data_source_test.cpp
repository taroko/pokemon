#include <iostream>
#include <fstream>
#include "../src/format/data_source_impl.hpp"
#include "gtest/gtest.h"

TEST (data_source_curl_gz, source_get_line_sequential) 
{
	// 400mbx2
	//VIRT 1732mb res 1g bufsize x 10
	//VIRT 320mb res 123m bufsize x 1
	// 10gbx2
	//VIRT 1550mb res 850mb bufsize x10, VIRT 2gb res 1.1gb toward ending
	//VIRT 346mb res 98mb bufsize x1
	std::ofstream q1 ("test_file/data_source_getline_seq_1"), q2 ("test_file/data_source_getline_seq_2");
	std::vector <std::string> vec_url
	({
		"http://localhost/test/big.fq.gz",
		"http://localhost/test/big.fq.gz"
		//"https://api.basespace.illumina.com/v1pre3/files/536326/content?access_token=9d3951d89ba74c60b29f90899ca2fa35",
		//"https://api.basespace.illumina.com/v1pre3/files/536328/content?access_token=9d3951d89ba74c60b29f90899ca2fa35"
		//"http://140.113.15.98/test/400m-1.fastq.gz",
		//"http://140.113.15.98/test/400m-2.fastq.gz"
		//"http://140.113.15.98/test/10g-1.fastq.gz",
		//"http://140.113.15.98/test/10g-2.fastq.gz"
	});	 

	std::vector <uint64_t> vec_size 
	({	
		7353346, 7353346
//		33739645, 32969564	
//		444670062, 463990642 
//		10548716385, 10739796450
	});

	DataSource < SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex >//, CompressFormat::GZ > 
		Q2 ( vec_url, vec_size );
	while (true)
	{
		std::string r1, r2;
		r1 = Q2.source_get_line (0), r2 = Q2.source_get_line (1);
		q1 <<r1<<'\n';
		q2 <<r2<<'\n';
		EXPECT_EQ (r1, r2);
		if ( Q2.end_of_curl_[0] && Q2.end_of_curl_[1] )
			break;
	}
}

TEST (data_source_curl_gz, source_get_line_sequential_no_file_size) 
{
	// 400mbx2
	//VIRT 1732mb res 1g bufsize x 10
	//VIRT 320mb res 123m bufsize x 1
	// 10gbx2
	//VIRT 1550mb res 850mb bufsize x10, VIRT 2gb res 1.1gb toward ending
	//VIRT 346mb res 98mb bufsize x1
	std::ofstream q1 ("test_file/data_source_getline_seq_1_no_file_size"), q2 ("test_file/data_source_getline_seq_2_no_file_size");
	std::vector <std::string> vec_url
	({
		"http://localhost/test/big.fq.gz",
		"http://localhost/test/big.fq.gz"
		//"https://api.basespace.illumina.com/v1pre3/files/536326/content?access_token=9d3951d89ba74c60b29f90899ca2fa35",
		//"https://api.basespace.illumina.com/v1pre3/files/536328/content?access_token=9d3951d89ba74c60b29f90899ca2fa35"
		//"http://140.113.15.98/test/400m-1.fastq.gz",
		//"http://140.113.15.98/test/400m-2.fastq.gz"
		//"http://140.113.15.98/test/10g-1.fastq.gz",
		//"http://140.113.15.98/test/10g-2.fastq.gz"
	});	 
	std::vector <uint64_t> vec_size 
	({	
		0, 0
//		7353346, 7353346
//		33739645, 32969564	
//		444670062, 463990642 
//		10548716385, 10739796450
	});

	DataSource < SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex >//, CompressFormat::GZ > 
		Q2 ( vec_url );
	while (true)
	{
		std::string r1, r2;
		r1 = Q2.source_get_line (0), r2 = Q2.source_get_line (1);
		q1 <<r1<<'\n';
		q2 <<r2<<'\n';
		EXPECT_EQ (r1, r2);
		if ( Q2.end_of_curl_[0] && Q2.end_of_curl_[1] )
			break;
	}
}

TEST (data_source_curl_gz, source_get_line_MT) 
{
	// 400mbx2
	//VIRT 1732mb res 1g bufsize x 10
	//VIRT 479mb res 106m bufsize x 1; res 125mb toward ending
	// 10gbx2
	//VIRT 1700mb res 1gb bufsize x10
	//VIRT 479mb res 106mb bufsize x1; VIRT 511mb res 124mb toward ending

	std::ofstream q1 ("test_file/data_source_getline_MT_1"), q2 ("test_file/data_source_getline_MT_2");
	std::vector <std::string> vec_url
	({
		"http://localhost/test/big.fq.gz",
		"http://localhost/test/big.fq.gz"
		//"https://api.basespace.illumina.com/v1pre3/files/536326/content?access_token=9d3951d89ba74c60b29f90899ca2fa35",
		//"https://api.basespace.illumina.com/v1pre3/files/536328/content?access_token=9d3951d89ba74c60b29f90899ca2fa35"
		//"http://localhost/test/400m-1.fastq.gz",
		//"http://localhost/test/400m-2.fastq.gz"
		//"http://140.113.15.98/test/10g-1.fastq.gz",
		//"http://140.113.15.98/test/10g-2.fastq.gz"
	});	 
	std::vector <uint64_t> vec_size 
	({	
		7353346, 7353346
		//33739645, 32969564	
		//444670062, 463990642 
		//10548716385, 10739796450
	});
	DataSource < SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex >//, CompressFormat::GZ > 
		Q2 ( vec_url, vec_size );
	std::vector<std::thread> thread_vec;

	thread_vec.push_back (	
		std::move ( 
			std::thread (
				[&q1, &Q2] ()	
				{
					while (true)
					{
						q1 << Q2.source_get_line (0)<<'\n';
						if ( Q2.end_of_curl_[0] )
							break;
					}
				}
			)
		) 
	);
	thread_vec.push_back (	
		std::move ( 
			std::thread (
				[&q2, &Q2] ()
				{
					while (true)
					{
						q2 << Q2.source_get_line (1)<<'\n';
						if ( Q2.end_of_curl_[1] )
							break;
					}
				}
			) 
		) 
	);

	for (auto& g : thread_vec )
		g.join();
	q1.close(), q2.close();

	std::ifstream qq1 ("test_file/data_source_getline_MT_1"), qq2("test_file/data_source_getline_MT_2");
	std::vector <std::ifstream*> ifvec ({ &qq1, &qq2 });
	while (!ifvec[0]->eof() && !ifvec[1]->eof())
	{
		std::string temp1, temp2;
		std::getline ( (*ifvec[0]), temp1);
		std::getline ( (*ifvec[1]), temp2);
		EXPECT_EQ (temp1, temp2);
	}
}

TEST (data_source_curl_gz, source_get_line_MT_no_file_size) 
{
	// 400mbx2
	//VIRT 1732mb res 1g bufsize x 10
	//VIRT 479mb res 106m bufsize x 1; res 125mb toward ending
	// 10gbx2
	//VIRT 1700mb res 1gb bufsize x10
	//VIRT 479mb res 106mb bufsize x1; VIRT 511mb res 124mb toward ending

	std::ofstream q1 ("test_file/data_source_getline_MT_1_no_file_size"), q2 ("test_file/data_source_getline_MT_2_no_file_size");
	std::vector <std::string> vec_url
	({
		"http://localhost/test/big.fq.gz",
		"http://localhost/test/big.fq.gz"
		//"https://api.basespace.illumina.com/v1pre3/files/536326/content?access_token=9d3951d89ba74c60b29f90899ca2fa35",
		//"https://api.basespace.illumina.com/v1pre3/files/536328/content?access_token=9d3951d89ba74c60b29f90899ca2fa35"
		//"http://localhost/test/400m-1.fastq.gz",
		//"http://localhost/test/400m-2.fastq.gz"
		//"http://140.113.15.98/test/10g-1.fastq.gz",
		//"http://140.113.15.98/test/10g-2.fastq.gz"
	});	 

	std::vector <uint64_t> vec_size 
	({	
		0, 0
//		7353346, 7353346
//		33739645, 32969564	
//		444670062, 463990642 
//		10548716385, 10739796450
	});

	DataSource < SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex >//, CompressFormat::GZ > 
		Q2 ( vec_url );
	std::vector<std::thread> thread_vec;

	thread_vec.push_back (	
		std::move ( 
			std::thread (
				[&q1, &Q2] ()	
				{
					while (true)
					{
						q1 << Q2.source_get_line (0)<<'\n';
						if ( Q2.end_of_curl_[0] )
							break;
					}
				}
			)
		) 
	);
	thread_vec.push_back (	
		std::move ( 
			std::thread (
				[&q2, &Q2] ()
				{
					while (true)
					{
						q2 << Q2.source_get_line (1)<<'\n';
						if ( Q2.end_of_curl_[1] )
							break;
					}
				}
			) 
		) 
	);

	for (auto& g : thread_vec )
		g.join();
	q1.close(), q2.close();

	std::ifstream qq1 ("test_file/data_source_getline_MT_1_no_file_size"), qq2("test_file/data_source_getline_MT_2_no_file_size");
	std::vector <std::ifstream*> ifvec ({ &qq1, &qq2 });
	while (!ifvec[0]->eof() && !ifvec[1]->eof())
	{
		std::string temp1, temp2;
		std::getline ( (*ifvec[0]), temp1);
		std::getline ( (*ifvec[1]), temp2);
		EXPECT_EQ (temp1, temp2);
	}
}

TEST (data_source_curl_ungz, source_get_line_sequential) 
{
	std::ofstream q1 ("test_file/data_source_getline_seq_ungz_1"), q2 ("test_file/data_source_getline_seq_ungz_2");
	std::vector <std::string> vec_url
	({
		"http://localhost/test/plain.fq",
		"http://localhost/test/plain.fq"
	});	 

	std::vector <uint64_t> vec_size 
	({0, 0});

	DataSource < SOURCE_TYPE::CURL_TYPE_PLAIN, curl_default_handle_mutex >//, CompressFormat::GZ > 
		Q2 ( vec_url, vec_size );
	while (true)
	{
		std::string r1, r2;
		r1 = Q2.source_get_line (0), r2 = Q2.source_get_line (1);
		q1 <<r1<<'\n';
		q2 <<r2<<'\n';
		EXPECT_EQ (r1, r2);
		if ( Q2.end_of_curl_[0] && Q2.end_of_curl_[1] )
			break;
	}
}

TEST (data_source_curl_ungz, source_get_line_sequential_no_file_size) 
{
	std::ofstream q1 ("test_file/data_source_getline_seq_ungz_1_no_file_size"), q2 ("test_file/data_source_getline_seq_ungz_2_no_file_size");
	std::vector <std::string> vec_url
	({
		"http://localhost/test/plain.fq",
		"http://localhost/test/plain.fq"
	});	 

	DataSource < SOURCE_TYPE::CURL_TYPE_PLAIN, curl_default_handle_mutex >//, CompressFormat::GZ > 
		Q2 ( vec_url );
	while (true)
	{
		std::string r1, r2;
		r1 = Q2.source_get_line (0), r2 = Q2.source_get_line (1);
		q1 <<r1<<'\n';
		q2 <<r2<<'\n';
		EXPECT_EQ (r1, r2);
		if ( Q2.end_of_curl_[0] && Q2.end_of_curl_[1] )
			break;
	}
}

