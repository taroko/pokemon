/// @file bed_test.cpp
/// @brief define unit test cases for bed.hpp
/// @author C-Salt Corp.
#include <tuple>
#include <stdint.h>
#include <string>
#include "gtest/gtest.h"
#include "../src/format/bed.hpp"
#include "../src/tuple_utility.hpp"
#include <fstream>
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"

///@fn TEST(Bed, default_constructor)
TEST(Bed, default_constructor)
{
	Bed<> bed_test;

	EXPECT_EQ (std::get<0> (bed_test.data), "");
	EXPECT_EQ (std::get<1> (bed_test.data), 0);
	EXPECT_EQ (std::get<2> (bed_test.data), 0);
	ASSERT_FALSE (bed_test.eof_flag);
/// 
}
///@fn TEST(Bed, copy_constructor)
TEST(Bed, copy_constructor)
{
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	
	Bed<> k( TupleType(chrom, start, end) );
	Bed<> bed_test( k );

	EXPECT_EQ (std::get<0> ( bed_test.data ), "CHXYZ");
	EXPECT_EQ (std::get<1> ( bed_test.data ), 10535);
	EXPECT_EQ (std::get<2> ( bed_test.data ), 155555);
	ASSERT_FALSE ( k.eof_flag );
	ASSERT_FALSE ( bed_test.eof_flag);
/// 
}

/// @fn TEST(Bed, assignment_constructor)
TEST(Bed, assignment_constructor)
{
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	
	Bed<> k( TupleType(chrom, start, end) );
	Bed<> bed_test;
	bed_test = k;

	EXPECT_EQ (std::get<0> ( bed_test.data ), "CHXYZ");
	EXPECT_EQ (std::get<1> ( bed_test.data ), 10535);
	EXPECT_EQ (std::get<2> ( bed_test.data ), 155555);
	ASSERT_FALSE ( k.eof_flag );
	ASSERT_FALSE ( bed_test.eof_flag );
/// 
}

/// @fn TEST(Bed, move_constructor)
TEST(Bed, move_constructor)
{
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	
	Bed<> k( TupleType(chrom, start, end) );
	Bed<> bed_test (std::move (k) );

	EXPECT_EQ (std::get<0> (bed_test.data), "CHXYZ");
	EXPECT_EQ (std::get<1> (bed_test.data), 10535);
	EXPECT_EQ (std::get<2> (bed_test.data), 155555);
/// 
}

/// @fn TEST(Bed, move_assignment)
TEST(Bed, move_assignment)
{
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	
	Bed<> k( TupleType( chrom, start, end ) );
	Bed<> bed_test;
	bed_test = std::move (k);

	EXPECT_EQ (std::get<0> (bed_test.data), "CHXYZ");
	EXPECT_EQ (std::get<1> (bed_test.data), 10535);
	EXPECT_EQ (std::get<2> (bed_test.data), 155555);/// ok
	ASSERT_FALSE ( k.eof_flag );
	ASSERT_FALSE ( bed_test.eof_flag );
/// 
}

/// @fn TEST(Bed, tupletype_copy_constructor)
TEST(Bed, tupletype_copy_constructor)
{
	/// dskfjsalkjdfasdjfkl
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple< std::string, uint32_t, uint32_t > TupleType;
	
	TupleType k(chrom, start, end);
	Bed<> bed_test(k);
	
	EXPECT_EQ (std::get<0> (bed_test.data),"CHXYZ");
	EXPECT_EQ (std::get<1> (bed_test.data),10535);
	EXPECT_EQ (std::get<2> (bed_test.data),155555);
	//ASSERT_FALSE (k.eof_flag);
	//ASSERT_FALSE (fasta_test.eof_flag);
/// 
}

/// @fn TEST(Bed, tupletype_move_constructor)
TEST(Bed, tupletype_move_constructor)
{
	/// fn TEST(Bed, tupletype_move_constructor)
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	
	TupleType k( chrom, start, end );
	Bed<> bed_test (std::move (k) );

	EXPECT_EQ (std::get<0> (bed_test.data), "CHXYZ");
	EXPECT_EQ (std::get<1> (bed_test.data), 10535);
	EXPECT_EQ (std::get<2> (bed_test.data), 155555);
/// 
}

/// @fn TEST(Bed, eof_constructor)
TEST(Bed, eof_constructor)
{
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	bool EofFlag = false;
	Bed < TupleType > AA (EofFlag);
    EXPECT_EQ (AA.eof_flag, false ); ///string is not c_string
    EXPECT_EQ (std::get<0> (AA.data), std::string("") ); ///string is not c_string
    EXPECT_EQ (std::get<1> (AA.data), 0); ///string is not c_string
    EXPECT_EQ (std::get<2> (AA.data), 0);
/// 
}

/// @fn TEST(Bed, 4_or_more_items)
TEST(Bed, 4_or_more_items)
{
	std::string chrom ("CHXYZ"), name ("fucker");
	uint32_t start = 10535, end = 155555, thickStart = 333, thickEnd = 555;
	int score (777);
	char strand ('+');
	
	typedef std::tuple <std::string, uint32_t, uint32_t, std::string, int, char, uint32_t, uint32_t> TupleType;
	TupleType k(chrom, start, end, name, score, strand, thickStart, thickEnd);
	Bed <TupleType> test_bed (std::move (k) );

	EXPECT_EQ (std::get<0> (test_bed.data), "CHXYZ");
	EXPECT_EQ (std::get<1> (test_bed.data), 10535);
	EXPECT_EQ (std::get<2> (test_bed.data), 155555);
	EXPECT_EQ (std::get<3> (test_bed.data), "fucker");
	EXPECT_EQ (std::get<4> (test_bed.data), 777);
	EXPECT_EQ (std::get<5> (test_bed.data), '+');
	EXPECT_EQ (std::get<6> (test_bed.data), 333);
	EXPECT_EQ (std::get<7> (test_bed.data), 555);
/// 
}

/// @fn TEST(Bed, overload_smaller_operator)
TEST(Bed, overload_smaller_operator)
{
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;
	
	Bed<> k( TupleType(chrom, start, end) );
	Bed<> bed_test (std::move (k) );

	std::ostringstream s1;
	s1 << bed_test;
	std::string s2 = s1.str();
	EXPECT_EQ ( s2, "CHXYZ\t10535\t155555\n");
	//std::cout << std::get<0>(bed_test.data) << std::endl; 
	//std::cout << bed_test << std::endl; //test overload operator<<
/// 
}

/// @fn TEST(Bed, serialization)
TEST(Bed, serialization)
{
	std::string chrom ("CHXYZ");
	uint32_t start = 10535;
	uint32_t end = 155555;
	typedef std::tuple <std::string, uint32_t, uint32_t> TupleType;

	Bed<> k( TupleType(chrom, start, end) );
	Bed<> bed_test (std::move (k) );
	Bed<> bed_result;
 
	std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/bed_serialization_test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & bed_test;
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/bed_serialization_test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & bed_result;

	EXPECT_EQ (std::get<0>(bed_result.data), std::get<0>(bed_test.data));//"CHXYZ");
	EXPECT_EQ (std::get<1>(bed_result.data), std::get<1>(bed_test.data));//10535);
	EXPECT_EQ (std::get<2>(bed_result.data), std::get<2>(bed_test.data));//155555);

	ifs.close();
/// 
}

