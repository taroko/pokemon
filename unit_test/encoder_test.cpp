#include "../src/encoder.hpp"
#include <string>
#include "gtest/gtest.h"

TEST (Encoder, Plain)
{
	Encoder<2, CompressType::Plain> a;
	std::string s1 ("ACGTACGTabcdefg");
	auto x1 = a.encode (s1);
	EXPECT_EQ (x1, s1);

	Encoder<6, CompressType::Plain> a6;
	auto x6 = a6.encode (s1);
	EXPECT_EQ (x6, s1);
}

TEST (Encoder, N_BITS)
{
	std::string s1 ("ACGTACGTAbCdefG");
	Encoder<2, CompressType::N_BITS> b;
	auto x2 = b.encode (s1);
	std::string sstr;
	x2.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);

	Encoder<6, CompressType::N_BITS> b6;
	auto x6 = b6.encode (s1);
	std::string sstr6;
	x6.MakeSeqString(sstr6);
	EXPECT_EQ (sstr6, s1);
}

TEST (Encoder, N_BITS_MASK)
{
	std::string s1 ("ACGTACGTabcdefg");
	Encoder<2, CompressType::N_BITS_MASK> c;
	auto x3 = c.encode (s1);
	std::string sstr;
	x3.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);

	Encoder<6, CompressType::N_BITS_MASK> c6;
	auto x6 = c6.encode (s1);
	std::string sstr6;
	x3.MakeSeqString(sstr6);
	EXPECT_EQ (sstr6, s1);
}
