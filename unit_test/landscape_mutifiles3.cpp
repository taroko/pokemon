#include <fstream>
#include <deque>
#include <map>
#include <unordered_map>
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "../src/parser.hpp"
//#include "../src/bwt.hpp"
#include "gtest/gtest.h"

int main(int argc, char** argv)
{
	if(argc < 4)
	{
		std::cout << "usage ./landscape_mutifiles scape_size overlap_number file1 file2 ..." << std::endl;
		std::cout << "for landscape(overview), count overlap with window, ..." << std::endl;
		return 0;
	}
	
	uint32_t scape_size(0), overlap_number(0);
	
	scape_size = std::stoi(argv[1]);
	overlap_number = std::stoi(argv[2]);
	
	
	std::vector <std::ifstream*> file_handles;
	for (int i=3; i < argc; ++i)
		file_handles.push_back (new std::ifstream ( argv[i] ) );
	typedef std::tuple < std::string, uint32_t, uint32_t, double > TUPLETYPE;

	FileParser < ParallelTypes::NORMAL, Bed, TUPLETYPE, CompressType::Plain, std::deque, 2 > LandScape;
	
	//chr, start, file_idx, num
	std::map<std::string, std::vector< std::vector< double > > > landscape;
	
	
	uint32_t min_start(3000000000),max_start(0);
	bool check_eof(true);
	do
	{
		check_eof=true;
		auto x = LandScape.Read ( file_handles );
		int file_idx=0;
		std::for_each (	x->begin(), x->end(),
			[&] (const std::pair<int, Bed<TUPLETYPE> >& Q)
			{
				if(!Q.second.eof_flag){
					check_eof=false;
					std::string chr( std::get<0>(Q.second.data) );
					uint32_t start( std::get<1>(Q.second.data) / scape_size);
					double num( std::get<3>(Q.second.data) );
					
					auto it = landscape.find(chr);
					if(it == landscape.end())
					{
						std::vector<std::vector<double> > tmp(300000000/scape_size, std::vector<double>(file_handles.size(), 0) );
						landscape.insert( {chr, tmp} );
					}
					
					if(start >= landscape[chr].size())
						landscape[chr].resize(start+1, std::vector<double>(file_handles.size(), 0) );
					
					landscape[chr][start][file_idx] += num;
					
					max_start = std::max( (start), max_start);
					min_start = std::min( (start), min_start);
				}
				++file_idx;
			}
		);
	}while(!check_eof);
	
	bool is_zero(true);
	std::stringstream tmp_string;
	std::string chr("");
	int file_num = file_handles.size();
	
	//std::cout << "chr\tstart\tend";
	//for (int i=3; i < argc; ++i) std::cout << "\t" << argv[i];
	//std::cout << "\n";
	
	std::for_each (landscape.begin(), landscape.end(),
		[&] (const std::pair<std::string, std::vector< std::vector< double > > > &Q)
		{
			std::string chr(Q.first);
			
			if(min_start > overlap_number) min_start -= overlap_number;
			for (uint32_t i_start(min_start); i_start < max_start+overlap_number; ++i_start)
			{
				uint32_t start(i_start*scape_size), end((i_start+overlap_number)*scape_size);
				is_zero=true;
				
				for(int i_file(0); i_file < file_num; ++i_file)
				{
					double number(0.0);
					
					for(uint32_t i(0); i < overlap_number; ++i)
						if( (i_start+i) < Q.second.size())
							number += (Q.second)[i_start+i][i_file];
					
					if(number != 0) is_zero=false;
					tmp_string << "\t" << number;
				}
				if(!is_zero)
					std::cout << chr << "\t" << start << "\t" << end << tmp_string.str() << "\n";
				tmp_string.str("");
			}
		}
	);
	
	
}

