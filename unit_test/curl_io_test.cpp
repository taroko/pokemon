#include "gtest/gtest.h"
#include "../src/format/curl_recv_impl.hpp"
#include <fstream>
#include <iostream>


TEST (curl_gz, get_from_url_impl_single)
{
	INITIALIZE_CURL ();
	CurlWrapper < CompressFormat::GZ, curl_default_handle_mutex > QQ
	("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	 7493990, 1 );
	// normal get_from_url test 
	QQ.get_from_url_impl ();
	//std::cerr<<"amargadon"<<std::endl;
	for (auto& Q: QQ.curl_in_thread_vec_)
		Q.join();
}

TEST (curl_gz, parse_single)
{
	INITIALIZE_CURL ();
	CurlWrapper < CompressFormat::GZ, curl_default_handle_mutex > QQ
	("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	 7493990, 1 );
	CurlWrapper < CompressFormat::GZ, curl_default_handle_mutex > QQ2
	("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	 7493990, 1 );
	// normal get_from_url test 
	std::stringstream stri;
	QQ.get_from_url_impl ();//stri);
	int offset_1 = stri.tellg(), offset_2 = stri.tellp();
	while ( !QQ.terminate_flag_ )//&& offset_2-offset_1 < 1000000 )
	{
		QQ.parse (stri);
		//std::cerr<<"keep parsing stri's length "<<stri.tellp()-stri.tellg()<<std::endl;
	}

	std::stringstream stri2;
	QQ2.get_from_url_impl ();//stri);
	int offset2_1 = stri2.tellg(), offset2_2 = stri2.tellp();
	while ( !QQ2.terminate_flag_ )//&& offset_2-offset_1 < 1000000 )
	{
		QQ2.parse (stri2);
		//std::cerr<<"keep parsing stri's length "<<stri2.tellp()-stri2.tellg()<<std::endl;
	}

	//std::cerr<<"amargadon"<<std::endl;
}

TEST (curl_gz, get_from_url_impl_MT)
{
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex>* > TT
	({
		new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex > 
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990, 1 ),
		new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex > 
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990, 1 )
	});

	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, i] ()
				{
				TT[i]->get_from_url_impl ();//stri);
				//std::cerr<<"amargadon"<<std::endl;
				for (auto& Q: TT[i]->curl_in_thread_vec_)
					Q.join();
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
		Q.join();
}

TEST (curl_gz, parse_MT)
{
//	std::vector < std::ofstream* > q 
//	({
//		new std::ofstream ("direct_download_1"),
//		new std::ofstream ("direct_download_2")
//	});

	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex>* > TT
	({
//		new CurlWrapper <CompressFormat::GZ, curl_default_handle > 
//			("https://api.basespace.illumina.com/v1pre3/files/536326/content?access_token=9d3951d89ba74c60b29f90899ca2fa35", 33739645),
//		new CurlWrapper <CompressFormat::GZ, curl_default_handle > 
//			("https://api.basespace.illumina.com/v1pre3/files/536328/content?access_token=9d3951d89ba74c60b29f90899ca2fa35", 32969564)
        new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex >
        	//("https://api.basespace.illumina.com/v1pre3/files/5595019/content?access_token=cceb99b15662407588261f3fdd000c68", 664808493, 1),
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990, 1 ),
        new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex >
        	//("https://api.basespace.illumina.com/v1pre3/files/5595020/content?access_token=cceb99b15662407588261f3fdd000c68", 701153480, 1)                        
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990, 1 )
	});

	std::vector <std::stringstream> stri (2);
	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, &stri, i] ()
				{
					TT[i]->get_from_url_impl ();
					while ( !TT[i]->terminate_flag_ )
					{
						TT[i]->parse (stri[i]);
						std::cerr << stri[i].str();
						//std::cerr<<"keep parsing stri's length "<<stri[i].tellp()-stri[i].tellg()<<std::endl;
					}
					//std::cerr<<"amargadon"<<std::endl;
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
	{
		//std::cerr<<"joning"<<std::endl;
		Q.join();
	}
	for (auto& q : TT)
		delete q;
}


TEST (curl_plain, get_from_url_impl_single)
{
	INITIALIZE_CURL ();
	std::stringstream stri;
	CurlWrapper < CompressFormat::PLAIN, curl_default_handle_mutex > TT
//	("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 0, 0);
//	("http://api.basespace.illumina.com/v1pre3/samples/16019/files?access_token=a9af229287d54018b38223462fdc8f4f", 0, 0);
	("140.113.15.99/aa.fa", 0, 0);

	TT.get_from_url_impl ();
	for (auto& Q: TT.curl_in_thread_vec_)
		Q.join();
};

TEST (curl_plain, parse_single)
{
	INITIALIZE_CURL ();
	CurlWrapper < CompressFormat::PLAIN, curl_default_handle_mutex > QQ
//	("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 0, 0);
//	("http://api.basespace.illumina.com/v1pre3/samples/16019/files?access_token=a9af229287d54018b38223462fdc8f4f", 0, 0);
	("140.113.15.99/aa.fa", 0, 0);
	std::stringstream stri;
	QQ.get_from_url_impl ();
	int offset_1 = stri.tellg(), offset_2 = stri.tellp();
	while ( !QQ.terminate_flag_ )//&& offset_2-offset_1 < 1000000 )
	{
		QQ.parse (stri);
		//std::cerr<<"keep parsing stri's length "<<stri.tellp()-stri.tellg()<<std::endl;
	}
}

TEST (curl_plain, get_from_url_impl_MT)
{
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex>* > TT
	({
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("140.113.15.99/aa.fa", 0, 0),
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("140.113.15.99/aa.fa", 0, 0)
	});

	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, i] ()
				{
				TT[i]->get_from_url_impl ();//stri);
				for (auto& Q: TT[i]->curl_in_thread_vec_)
					Q.join();
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
		Q.join();
}

TEST (curl_plain, parse_MT)
{
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex>* > TT
	({
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("140.113.15.99/aa.fa", 0, 0),
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("140.113.15.99/aa.fa", 0, 0)
	});

	std::vector <std::stringstream> stri (2);
	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, &stri, i] ()
				{
					TT[i]->get_from_url_impl ();
					while ( !TT[i]->terminate_flag_ )
					{
						TT[i]->parse (stri[i]);
						std::cerr << stri[i].str();
					}
					//std::cerr<<"amargadon"<<std::endl;
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
	{
		//std::cerr<<"joning"<<std::endl;
		Q.join();
	}
	for (auto& q : TT)
		delete q;
}

TEST (curl_plain, curl_plain)
{
	INITIALIZE_CURL ();
	std::stringstream stri;
	CurlWrapper < CompressFormat::PLAIN, curl_default_handle_mutex > TT
//		("http://api.basespace.illumina.com/v1pre3/samples/16019/files?access_token=a9af229287d54018b38223462fdc8f4f", 0, 0);
		("140.113.15.99/aa.fa", 0, 0);

	TT.get_from_url_impl ();
	while ( !TT.terminate_flag_ )
	{
		TT.parse (stri);
		std::cerr<< stri.str();
	}
	//std::cerr<<"amargadon"<<std::endl;
};


//	std::string line_temp;
//	while ( true )
//	{
//		if ( stri.eof() )
//			break;
//		std::getline ( stri, line_temp );
//		std::cout << line_temp << std::endl;
//	}

// get_from_url combo test 
	//std::stringstream stri2;
	//gg kk;
	//auto zzz = std::bind (&gg::operator(), kk, std::ref(stri2), std::placeholders::_1 );
	//QQ.get_from_url (stri2, zzz);

//	QQ.get_from_url (stri2, std::bind (&gg::operator(), kk, std::ref(stri2), std::placeholders::_1 ) );	
//	std::cerr<<"amargadon"<<std::endl;


// CurlWrapper < PLAIN > test

//	constructor with file_url & file_size
//	CurlWrapper < CompressFormat::PLAIN, curl_default_handle > QQQ
//	("http://api.basespace.illumina.com/v1pre3/samples/16019/files?access_token=a9af229287d54018b38223462fdc8f4f");

//	constructor with file_url & file_size
	//CurlWrapper < CompressFormat::PLAIN, curl_default_handle > QQQ
	//("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	// 7493990 );

// normal get_from_url test 
//	std::stringstream striQ;
//	QQQ.get_from_url (striQ);
//	std::cerr<<"amargadon Q"<<std::endl;
//	std::string line_tempQ;
//	while ( true )
//	{
//		if ( striQ.eof() )
//			break;
//		std::getline ( striQ, line_tempQ );
//		std::cout << line_tempQ;// << std::endl;
//	}

// get_from_url combo test 
	//std::stringstream stri3;
	//gg kk2;
	//QQQ.get_from_url (stri3, std::bind (&gg::operator(), kk2, std::ref(stri3) ) );	
	//std::cerr<<"amargadon"<<std::endl;
