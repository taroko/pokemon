#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "../src/file_reader_impl.hpp"
#include "../src/thread_pool_update.hpp"
#include "sam2rawbed.hpp"

int main (void)
{
    typedef std::tuple <
        std::string, //QNAME
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
        int, // NH
        std::string //TailSeq
                        > TUPLETYPE;

	std::string file_name ("../unit_test/test.sam");
	std::ifstream file_handle (file_name);
	std::vector<std::string> QQ ({file_name});
	FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);

	size_t index=0;
	std::vector <Sam <TUPLETYPE> > AAA, AAB, AAC;

	while (index!=5)
	{
		const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
		AAA.push_back (qq);
		AAB.push_back (qq);
		AAC.push_back (qq);
		++index;
	}

	std::vector < size_t > job_vec;
	job_vec.push_back ( 
		GlobalPool.JobPost (
			[&AAA] ()//(std::vector< Sam<TUPLETYPE> >& AAA )
			{
				Sam2RawBed < std::vector< Sam<> >* > QOO;
				QOO.Convert (&AAA);
			} ) );

	job_vec.push_back ( 
		GlobalPool.JobPost (
			[&AAB] ()//(std::vector< Sam<TUPLETYPE> >& AAA )
			{
				Sam2RawBed < std::vector< Sam<> >* > QOO;
				QOO.Convert (&AAB);
			} ) );

	job_vec.push_back ( 
		GlobalPool.JobPost (
			[&AAC] ()//(std::vector< Sam<TUPLETYPE> >& AAA )
			{
				Sam2RawBed < std::vector< Sam<> >* > QOO;
				QOO.Convert (&AAC);
			} ) );

	for (auto i : job_vec)
		GlobalPool.FlushOne (i);

	for (auto& Q : AAA)
		std::cerr<<Q;

	for (auto& Q : (*Sam2RawBed <std::vector< Sam<> >*>::rawbed_map_) )
		std::cerr<<Q.first;
};

