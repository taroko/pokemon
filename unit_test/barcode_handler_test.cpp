#include "../src/barcode_handler/barcode_handler_impl.hpp"
#include "gtest/gtest.h"
#include "../src/file_reader.hpp"

TEST (barcode_handler_5, ScanBarcodeSeq)
{
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
    std::vector<std::string> read_vec ({
			"barcode_test_five_prime.fq"
//            "/Users/obigbando/Documents/work/test_file/test_1.fq",
        });
    std::map<int, std::vector< Fastq<TUPLETYPE> > > cca;

    FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQA (read_vec, &cca);
    QQA.Read (100000);

	std::vector <std::string> barcode_vec ({"ACGT","CGTA","GTAC","TACG"});
	BarcodeHandlerImpl <
						BarcodeHandleScheme::Five_Prime,
						Fastq,
						TUPLETYPE
						> QQ (barcode_vec);

//	for (auto itr=0; itr!=cca[0].size(); ++itr)
//		if (QQ.ScanBarcodeSeq (cca[0][itr])==0)
//			std::cerr<<cca[0][itr];
};

TEST (barcode_handler_5, remove)
{
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
    std::vector<std::string> read_vec ({	"barcode_test_five_prime.fq"	});
    std::map<int, std::vector< Fastq<TUPLETYPE> > > cca, ccc;

    FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQA (read_vec, &cca);
    QQA.Read (100000);

    FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQC (read_vec, &ccc);
    QQC.Read (100000);
	
	std::vector <std::string> barcode_vec ({"ACGT","CGTA","GTAC","TACG"});
	BarcodeHandlerImpl <
						BarcodeHandleScheme::Five_Prime,
						Fastq,
						TUPLETYPE
						> QQ (barcode_vec);

	QQ.Remove (&cca);
	for (auto ind=0; ind!=cca[0].size(); ++ind)
	{
		EXPECT_EQ ( std::get<0>(cca[0][ind].data), std::get<0>(ccc[0][ind].data) );
		EXPECT_EQ ( std::get<1>(cca[0][ind].data), std::get<1>(ccc[0][ind].data).substr (4) );
		EXPECT_EQ ( std::get<2>(cca[0][ind].data), std::get<2>(ccc[0][ind].data) );
		EXPECT_EQ ( std::get<3>(cca[0][ind].data), std::get<3>(ccc[0][ind].data).substr (4) );
	}
}; 

TEST (barcode_handler_3, ScanBarcodeSeq)
{
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
    std::vector<std::string> read_vec ({
			"barcode_test_three_prime.fq"
        });
    std::map<int, std::vector< Fastq<TUPLETYPE> > > cca;

    FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQA (read_vec, &cca);
    QQA.Read (100000);

	std::vector <std::string> barcode_vec ({"ACGT","CGTA","GTAC","TACG"});
	BarcodeHandlerImpl <
						BarcodeHandleScheme::Three_Prime,
						Fastq,
						TUPLETYPE
						> QQ (barcode_vec);

//	for (auto itr=0; itr!=cca[0].size(); ++itr)
//		if (QQ.ScanBarcodeSeq (cca[0][itr])==std::get<1>(cca[0][itr].data).size()-4)
//			std::cerr<<cca[0][itr];
};

TEST (barcode_handler_3, remove)
{
	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
    std::vector<std::string> read_vec ({	"barcode_test_three_prime.fq"	});
    std::map<int, std::vector< Fastq<TUPLETYPE> > > cca, ccc;

    FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQA (read_vec, &cca);
    QQA.Read (100000);

    FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQC (read_vec, &ccc);
    QQC.Read (100000);
	
	std::vector <std::string> barcode_vec ({"ACGT","CGTA","GTAC","TACG"});
	BarcodeHandlerImpl <
						BarcodeHandleScheme::Three_Prime,
						Fastq,
						TUPLETYPE
						> QQ (barcode_vec);

	QQ.Remove (&cca);
	for (auto ind=0; ind!=cca[0].size(); ++ind)
	{
		EXPECT_EQ ( std::get<0>(cca[0][ind].data), std::get<0>(ccc[0][ind].data) );
		EXPECT_EQ ( std::get<1>(cca[0][ind].data), std::get<1>(ccc[0][ind].data).substr (0, std::get<1>(cca[0][ind].data).size()));
		EXPECT_EQ ( std::get<2>(cca[0][ind].data), std::get<2>(ccc[0][ind].data) );
		EXPECT_EQ ( std::get<3>(cca[0][ind].data), std::get<3>(ccc[0][ind].data).substr (0, std::get<1>(cca[0][ind].data).size()));
	}
}; 

