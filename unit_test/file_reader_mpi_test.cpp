#include "../src/constant_def.hpp"
#include "../src/file_reader.hpp"
#include "../src/wrapper_tuple_utility.hpp"
#include "gtest/gtest.h"
#include <vector>
#include <map>
#include <utility>
#include "../src/thread_pool.hpp"

//TEST (file_reader_intf_MPI, Wig_possible_invalid_ofile_name)
int main (int argc, char* argv[])
{
	MPIPool MP (argc, argv);
	MPIPool* vp = &MP;

	std::vector < std::string > file_names ({
		"/Users/obigbando/Documents/work/test_file/test1.wig",
		"/Users/obigbando/Documents/work/test_file/test2.wig",
		"/Users/obigbando/Documents/work/test_file/test3.wig",
		"/Users/obigbando/Documents/work/test_file/test3.wig",
		"/Users/obigbando/Documents/work/test_file/test1.wig",
		"/Users/obigbando/Documents/work/test_file/test2.wig"
	});				  
	typedef  std::tuple <std::string,
						 std::string,
						 int,
						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > >
						> TUPLETYPE;
	std::map <int, std::vector< Wig <TUPLETYPE> > > result;
	FileReader < ParallelTypes::M_P_I, Wig, TUPLETYPE > GG (&result, vp);
	
	GG.Read_all (file_names);
	std::cerr<<"aloha"<<std::endl;

	if (MP.world.rank()==0)	
	{
	std::for_each (result.begin(), result.end(),
	[] (const std::pair <int, std::vector<Wig<TUPLETYPE> > >& Q)
	{ std::for_each (Q.second.begin(), Q.second.end(), 
		[] (const Wig<TUPLETYPE>& q)
		{ std::cerr<<q<<std::endl;});
	});
	}
//	if (MP.world.rank()==0)
//		GG.Printer();
};

/*
//TEST (file_reader_intf_MPI, Fasta_possible_invalid_ofile_name)
int main (int argc, char* argv[])
{
	MPIPool MP (argc, argv);
	MPIPool* vp = &MP;

	std::vector < std::string > file_names ({
//		"/Users/obigbando/Documents/work/test_file/test1.wig",
//		"/Users/obigbando/Documents/work/test_file/test2.wig",
//		"/Users/obigbando/Documents/work/test_file/test3.wig",
//		"/Users/obigbando/Documents/work/test_file/test2.wig",
//		"/Users/obigbando/Documents/work/test_file/test3.wig",
		"/Users/obigbando/Documents/work/test_file/test.fasta",
		"/Users/obigbando/Documents/work/test_file/test.fasta"
	});				  
	typedef  std::tuple <std::string,
						 std::string//,
//						 int,
						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > >
						> TUPLETYPE;
	std::map <int, std::vector< Fasta <TUPLETYPE> > > result;
	FileReader < ParallelTypes::M_P_I, Fasta, TUPLETYPE > GG (&result, vp);//(3, &result) ;
	
	GG.Read_all (file_names);//, result);
	if (MP.world.rank()==0)
		GG.Printer();
};

*/

