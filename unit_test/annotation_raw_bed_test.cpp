#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

#include "../src/format/raw_bed.hpp"
#include "../src/format/annotation_raw_bed.hpp"
#include "../src/format/sam.hpp"
#include "../src/file_reader_impl.hpp"
#include "../src/pipeline/pipeline_preparator.hpp"
//std::map <std::string, std::string> g_genometable ({
//{ std::string ("chrX"), std::string (500000000, 'A')} ,
//});

int main (void)
{
    typedef std::tuple <
        std::string, //QNAME
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
//		UserDefineContent
		UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
                        > TUPLETYPE;



	std::string file_name //("../out.sam");//("test.sam");
	("tmp.sam");
	std::ifstream file_handle (file_name);
	std::vector<std::string> QQ ({file_name});
	FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);

	size_t index=0;
	std::vector <Sam <TUPLETYPE> > AAA;
	std::vector < RawBed<> > BBB;
	std::map < RawBed<>, uint16_t > DDD;

	while //(true)//
	(index!=10)
	{
		std::cerr<<"index "<<index<<'\n';
		const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
		if (qq.eof_flag)
			break;
		RawBed <> yy (qq);
		std::cerr<<"rawbed "<<yy<<'\n';
		++ ( *( ( &( DDD[RawBed<>(qq)]) ) - 9 ) );	
		++index;
	}

	std::vector < AnnotationRawBed<> >EEE;

	std::cerr<<"rawbed "<<'\n';
	for (auto itr=DDD.begin(); itr!=DDD.end(); ++itr)
	{
		std::cerr<<itr->first;
		EEE.push_back ( AnnotationRawBed <> (itr->first) );
	}

    std::string qqa //("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//("/mnt/godzilla/GENOME/db/hg19/hg19.fa");//
	("/mnt/godzilla/GENOME/db/mm10/mm10.fa");      
    std::string qqa2 //("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//("/mnt/mammoth/jones/bwt_table/hg19/hg19");//
	("/mnt/godzilla/BOWTIE_INDEX/tailer/mm10/mm10"); 
    PipelinePreparator<> PPa (qqa, qqa2);                                                      
    PPa.Load();                                                                              
    std::cerr<<"mm10 "<<"chrX 151912382 "<<(PipelinePreparator<>::gGenometable_["chrX"]).substr (151912382, 43)<<'\n';
    std::cerr<<"mm10 "<<"chrX 75563770 "<<(PipelinePreparator<>::gGenometable_["chrX"]).substr (75563770, 23)<<'\n';  

/*
	std::string qq //("/mnt/godzilla/GENOME/db/hg18/chrX.fa");//
	("/mnt/godzilla/GENOME/db/hg19/hg19.fa");//("/mnt/godzilla/GENOME/db/mm9/mm9.fa");
	std::string qq2 //("/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");//
	("/mnt/mammoth/jones/bwt_table/hg19/hg19");//("/mnt/godzilla/BOWTIE_INDEX/tailer/mm9");
	PipelinePreparator<> PP (qq, qq2);
	PP.Load();

	std::cerr<<"chrX 151912382 "<<(PipelinePreparator<>::gGenometable_["chrX"]).substr (151912382, 43)<<'\n';
	std::cerr<<"chrX 75563770 "<<(PipelinePreparator<>::gGenometable_["chrX"]).substr (75563770, 23)<<'\n';
*/

	std::cerr<<" !!! annotatoinrawbed "<<'\n';
	for (auto& Q : EEE)
		std::cerr<<Q;
	std::cerr<<" !!! BBB annotatoinrawbed "<<'\n';


	for (auto& Q : EEE)
		std::cerr<<Q.ToSam()<<'\n';

//	for (auto itr=DDD.begin(); itr!=DDD.end(); ++itr)
//		std::cerr<<(itr->first).reads_count_<<'\n';	
//	for (auto itr=CCC.begin(); itr!=CCC.end(); ++itr)
//		std::cerr<<itr->second<<'\n';

};
	

//	std::sort (BBB.begin(), BBB.end());

	//for (auto i=0; i!=4; ++i)
	//{
	//	bool b1 = BBB[i] < BBB[i+1], b2 = BBB[i+1] < BBB[i];
	//	std::cerr<<"< overload "<<i<<"<"<<i+1<<" & "<<i+1<<"<"<<i<<'\t'<<b1<<'\t'<<b2<<'\n';
	//}
	//for (auto itr=CCC.begin(); itr!=CCC.end(); ++itr)
	//	std::cerr<<itr->second<<'\n';

