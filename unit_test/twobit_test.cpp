#include <bitset>
#include <iostream>
#include <deque>
#include <string>
#include <algorithm>
#include "/opt/local/include/boost/dynamic_bitset/dynamic_bitset.hpp"
#include "boost/utility/binary.hpp"
#include "boost/lexical_cast.hpp"
#include "../src/compression/twobit.hpp"
#include "gtest/gtest.h"

//twobit Normal
//Constructors
TEST (twobit,default_constructor)
{
	TwoBitSequence< TwoBit_types::Normal > q;
//	q.Printer();
	EXPECT_EQ ( q.GetSeq().size(), 0);
	EXPECT_EQ ( q.GetExceptDeq().size(), 0);
}

TEST (twobit, constructor_with_size)
{
	TwoBitSequence< TwoBit_types::Normal > a(20);
//	a.Printer();
	EXPECT_EQ ( a.GetSeq().size(), 20);
	EXPECT_EQ ( a.GetExceptDeq().size(), 0);
}

TEST (twobit, constructor_with_string)
{
	std::string s1 ("NAaXZcZgZtNqCzGNNMKOacgttuuacgTNNNNACNNNNNNNNNGNT");
	TwoBitSequence< TwoBit_types::Normal > z ( s1 );
//	z.Printer();
//	EXPECT_EQ (z.MakeSeqString(), s1);
}

TEST (twobit, copy_constructor)
{
	std::string s1 ("NANNCGNNNTNNNNACNNNNNNNNNGNT");
	TwoBitSequence< TwoBit_types::Normal > z ( s1 );
	TwoBitSequence< TwoBit_types::Normal > w ( z );
//	w.Printer();
	EXPECT_EQ (z.GetSeq(), w.GetSeq());
	EXPECT_EQ (z.GetExceptDeq(), w.GetExceptDeq());
//	EXPECT_EQ (w.MakeSeqString(), s1);
}

TEST (twobit, constructor_with_elements)
{
	boost::dynamic_bitset<> db (20);
	std::deque < std::pair<uint32_t, std::pair<char, uint32_t> > > Nd (5);
	TwoBitSequence< TwoBit_types::Normal > s ( db, Nd );
//	s.Printer();
	EXPECT_EQ ( s.GetSeq().size(), 20);
	EXPECT_EQ ( s.GetExceptDeq().size(), 5);
}

TEST (twobit, move_constructor_with_elements)
{
	boost::dynamic_bitset<> db (20);
	std::deque < std::pair<uint32_t, std::pair<char, uint32_t> > > Nd (5);
	TwoBitSequence< TwoBit_types::Normal > x ( std::move(db), std::move(Nd) );
//	x.Printer();
	EXPECT_EQ ( x.GetSeq().size(), 20);
	EXPECT_EQ ( x.GetExceptDeq().size(), 5);
}

TEST (twobit, move_constructor)
{
	boost::dynamic_bitset<> db (20);
	std::deque < std::pair<uint32_t, std::pair<char, uint32_t> > > Nd (5);
	TwoBitSequence< TwoBit_types::Normal > s ( db, Nd );
	TwoBitSequence< TwoBit_types::Normal > e ( std::move (s) );
//	e.Printer();
	EXPECT_EQ ( e.GetSeq().size(), 20 );
	EXPECT_EQ ( e.GetExceptDeq().size(), 5);
}

TEST (twobit, move_assign)
{
	std::string s1 ("NANNCGNNNTNNNNACNNNNNNNNNGNT");
	TwoBitSequence< TwoBit_types::Normal > z ( s1 );
	TwoBitSequence< TwoBit_types::Normal > d;
	d = std::move (z);
//	d.Printer();
	EXPECT_EQ (d.MakeSeqString(), s1);
}

TEST (twobit, assign)
{
	std::string s1 ("NANNCGNNNTNNNNACNNNNNNNNNGNT");
	TwoBitSequence< TwoBit_types::Normal > d ( s1 );
	TwoBitSequence< TwoBit_types::Normal > c;
	c = d;
//	c.Printer();
	EXPECT_EQ (c.MakeSeqString(), d.MakeSeqString());
	EXPECT_EQ (c.GetExceptDeq(), d.GetExceptDeq());
	EXPECT_EQ (c.GetSeq(), d.GetSeq());
}

//Base functions
TEST (twobit, MakeString)
{
//	std::cerr<<"\nMakeTWBString test"<<std::endl;
	std::string s1 ("NANNCGNNNTNNNNAcklzjbkqoeiruoQQRERJIOBDFSewkrlerjoiCNNNNNNNNNGNT");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 );
	EXPECT_EQ (c.MakeTWBString(), boost::lexical_cast <std::string> (c.GetSeq()) );
//	std::cerr<<"\nMakeSeqString test"<<std::endl;
//	EXPECT_EQ (c.MakeSeqString(), s1);

	std::string s2 ("NANbvnmNCGNNyiokjpwsxNTNNNNACNGGTTAGGCCCGTTNNNNNNNNGNTATCGGTNAAAGGCCCCGGATACCqzwsxderfvbnhyiokjpGA");
	TwoBitSequence< TwoBit_types::Normal > c2 ( s2 );
	EXPECT_EQ (c2.MakeSeqString(), s2);
}

TEST (twobit, GetSize)
{
	//std::cerr<<"\noperator[] test"<<std::endl;
	std::string s1 ("NANNCGNNNTNNNNACNGGTTAGGCCCGTTNNNNNNNNGNTATCGGTNAAAGGCCCCGGATACCGAazczioqeiOGPOIEERJQoifdasdjoi");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 );
//	std::cerr<<"c.GetSize() "<<c.GetSize()<<std::endl;
	EXPECT_EQ ( c.GetSize(), s1.size()<<1 ); 
}

TEST (twobit, opeartor_middle_bracket)
{
	//std::cerr<<"\noperator[] test"<<std::endl;
	std::string s1 ("NAGVUEIWJFLDKIDSabcdefgNCGNNNTNNNNACNGGTTAGGCCCGTTNNNNNNNNGNTATCGGTNAAAGGCCCCGGATACCGA");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 );
	//std::cerr<<"main A"<<std::endl;
	for ( auto index = 0; index!=s1.size(); ++index)
	{
		//std::cerr<<"main looping for "<<index<<" th char of input string"<<std::endl;
		auto gg = c[index];
//		std::cerr<<" current character of "<<gg<<std::endl;
//		EXPECT_EQ ( gg, s1[index]);
	}
//	std::cerr<<"\noperator[] test 2"<<std::endl;
	std::string s2 ("NAGCCJKKOWOPEIRTPTOWINBMVKWEPOIJNCGNNNTNNNNACNNNNNNNNNGNT");
	TwoBitSequence< TwoBit_types::Normal > c2 ( s2 );
//	std::cerr<<"main A"<<std::endl;
	for ( auto index = 0; index!=s2.size(); ++index)
	{
		//std::cerr<<"main looping for "<<index<<" th char of input string"<<std::endl;
		auto g = c2[index];
		//std::cerr<<" current character of "<<g.first<<std::endl;
		EXPECT_EQ ( g, s2[index]);
	}
}

TEST (twobit, GetSubStr)
{
//	std::cerr<<"\nGetSubStr test\t"<<std::endl;
	std::string s1 ("NANrfvbnhjkoqNCGNNNTNNNNqzxswweACNGGTTAGbnmjkllppGCCCGTTNNNNNNzzzppllsrrvbhhjnnmNNGNTATCGGTNAAAGGCCCCGGATACCGA");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 );
//	std::cerr<<"c.GetSubStr (0,48) "<<c.GetSubStr(0, 48)<<std::endl;
//	std::cerr<<"s1.substr (0,48) "<<s1.substr(0, 48)<<std::endl;
	EXPECT_EQ ( c.GetSubStr (0, 68), s1.substr(0, 68) );
}

//Complexes
TEST (twobit, reverse )
{
//	std::cerr<<"\nreverse test"<<std::endl;
	std::string s1 ("NATCGNvAdjhfkTGJGIOXC");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 );
//	c.Printer();
//	c.TWBPrinter();
	auto str = c.MakeSeqString();
//	std::cerr<<"c's converted back code\t"<<str<<std::endl;
	c.reverse();
	auto str3 = c.MakeSeqString();
//	std::cerr<<"c's reverse string\t"<<str3<<std::endl;
	auto str4 = c.MakeTWBString();
//	std::cerr<<"c's reverse code\t"<<str4<<std::endl;
	for (auto i = 0; i!=s1.size(); ++i)
//		std::cerr<<"str[i]\t"<<str[i]<<std::endl;
//		std::cerr<<"str[size-i]\t"<<s1[(c.GetSeq().size()>>1)-i-1]<<std::endl;
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}

TEST (twobit, complement )
{
//	std::cerr<<"\ncomplement test"<<std::endl;
	std::string s1 ("NATCGN");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 ), cc (s1);
	auto l = (s1.size()<<1);
	c.complement();
	for (auto i = 0; i != l; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (cc.GetSeq()[i] ^= 1) );
	}
}

TEST (twobit, reverse_complement)
{
	std::string s1 ("NACGTNNNACGTTGCANGNANCNTNNNA");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 ), cc (s1);
	auto str2 = cc.MakeSeqString();
	auto l = s1.size() << 1;
	c.reverse_complement();
	auto str1 = c.MakeSeqString();
	auto index = 0;
	while (index < l)
//	for (auto index = 0; index<l; index+2)
	{
		EXPECT_EQ ( c.GetSeq()[index], (cc.GetSeq()[l-index-2] ^= 1) );
//		std::cerr<<"c["<<index<<"] and c["<<index+1<<"] "<<c.GetSeq()[index]<<'\t'<<
//		c.GetSeq()[index+1] <<'\t';
//		std::cerr<<"cc["<<l - index-2<<"] and cc["<< l - index-1<<" ] "<<cc.GetSeq()[l-index-2]<<'\t'<<
//		cc.GetSeq()[l-index-1] <<std::endl;
		index += 2;
	}
}

TEST (twobit, reverse_copy)
{
//	std::cerr<<"\nreverse_copy test"<<std::endl;
	std::string s1 ("NATCGN");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 );
//	c.Printer();
//	c.TWBPrinter();
	auto str = c.MakeSeqString();
//	std::cerr<<"c's converted back code\t"<<str<<std::endl;

	auto gg = c.reverse_copy();
	auto str3 = gg.MakeSeqString();
//	std::cerr<<"c's reverse string\t"<<str3<<std::endl;
	auto str4 = c.MakeTWBString();
//	std::cerr<<"c's reverse code\t"<<str4<<std::endl;
	for (auto i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}

TEST (twobit, complement_copy)
{
//	std::cerr<<"\ncomplement_copy test"<<std::endl;
	std::string s1 ("NATCGN");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 ), cc (s1);
	auto l = (s1.size()<<1);
	auto gg = c.complement_copy();
	for (auto i = 0; i != l; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (gg.GetSeq()[i] ^= 1) );
	}
}

TEST (twobit, reverse_complement_copy)
{
	std::string s1 ("NACGTNNNACGTTGCANGNANCNTNNNA");
	TwoBitSequence< TwoBit_types::Normal > c ( s1 ), cc (s1);
	auto str2 = cc.MakeSeqString();
	auto l = s1.size() << 1;
	auto gg = c.reverse_complement_copy();
	auto str1 = c.MakeSeqString();
	auto index = 0;
	while (index < l)
	{
		EXPECT_EQ ( gg.GetSeq()[index], (cc.GetSeq()[l-index-2] ^= 1) );
		index += 2;
	}
}


//twobit Mask
//Constructors
TEST (twobit_2, default_constructor)
//int main (void)
{
	TwoBitSequence <TwoBit_types::Mask> q;
	EXPECT_EQ ( q.GetSeq().size(), 0);
	EXPECT_EQ ( q.GetExceptDeq().size(), 0);
	EXPECT_EQ ( q.GetMaskDeq().size(), 0);
}

TEST (twobit_2, constructor_with_size )
{
	TwoBitSequence< TwoBit_types::Mask >a(20);
//	a.Printer();
	EXPECT_EQ ( a.GetSeq().size(), 20);
	EXPECT_EQ ( a.GetExceptDeq().size(), 0);
	EXPECT_EQ ( a.GetMaskDeq().size(), 0);
}

TEST (twobit_2, constructor_with_string)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
	EXPECT_EQ (z.MakeSeqString(), s1);
//	z.Printer();
//	z.TWBPrinter();
}

TEST (twobit_2, copy_constructor)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
	TwoBitSequence< TwoBit_types::Mask > zz ( z );
	EXPECT_EQ ( z.GetSeq(), zz.GetSeq());
	EXPECT_EQ ( z.GetMaskDeq(), zz.GetMaskDeq() );
	EXPECT_EQ ( z.GetExceptDeq(), zz.GetExceptDeq() );
}

TEST (twobit_2, constructor_with_elements)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence < TwoBit_types::Mask > z ( s1 );
	auto x = z.GetSeq();
	auto y = z.GetExceptDeq();
	auto w = z.GetMaskDeq();
	TwoBitSequence < TwoBit_types::Mask > zz ( x, y, w);
	EXPECT_EQ ( z.GetSeq(), zz.GetSeq());
	EXPECT_EQ ( z.GetMaskDeq(), zz.GetMaskDeq() );
	EXPECT_EQ ( z.GetExceptDeq(), zz.GetExceptDeq() );

}

TEST (twobit_2, move_constructor_with_elements)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence < TwoBit_types::Mask > z ( s1 );
	TwoBitSequence < TwoBit_types::Mask > zz ( z.GetSeq(), z.GetExceptDeq(), z.GetMaskDeq() );
//( std::move(x), std::move(y), std::move(w) );
	EXPECT_EQ ( z.GetSeq(), zz.GetSeq());
	EXPECT_EQ ( z.GetMaskDeq(), zz.GetMaskDeq() );
	EXPECT_EQ ( z.GetExceptDeq(), zz.GetExceptDeq() );
}

TEST (twobit_2, move_constructor)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence < TwoBit_types::Mask > z ( s1 ), gg (s1);
	TwoBitSequence < TwoBit_types::Mask > zz ( std::move (gg) );
//( std::move(x), std::move(y), std::move(w) );
	EXPECT_EQ ( z.GetSeq(), zz.GetSeq());
	EXPECT_EQ ( z.GetMaskDeq(), zz.GetMaskDeq() );
	EXPECT_EQ ( z.GetExceptDeq(), zz.GetExceptDeq() );
}

TEST (twobit_2, move_assignment)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence < TwoBit_types::Mask > z ( s1 ), gg (s1);
	TwoBitSequence < TwoBit_types::Mask > zz;
	zz = std::move (gg);
//( std::move(x), std::move(y), std::move(w) );
	EXPECT_EQ ( z.GetSeq(), zz.GetSeq());
	EXPECT_EQ ( z.GetMaskDeq(), zz.GetMaskDeq() );
	EXPECT_EQ ( z.GetExceptDeq(), zz.GetExceptDeq() );
}

TEST (twobit_2, assignment)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence < TwoBit_types::Mask > z ( s1 ), gg (s1);
	TwoBitSequence < TwoBit_types::Mask > zz;
	zz = gg;
	EXPECT_EQ ( z.GetSeq(), zz.GetSeq());
	EXPECT_EQ ( z.GetMaskDeq(), zz.GetMaskDeq() );
	EXPECT_EQ ( z.GetExceptDeq(), zz.GetExceptDeq() );
}


//Basic functions
TEST (twobit_2, operator_middle_bracket)
{
//	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
//	std::string s1 ("ATCGRREGVVCCDD");
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
//	z.Printer();
	for ( auto index = 0; index!=s1.size(); ++index)
	{
		auto gg = z[index];
//		std::cerr<<"current char, Normal, Except, Mask conts "<< gg << '\t' << z.Normal_count <<'\t'<< z.Except_count<<'\t'<< z.Mask_count<<std::endl;
		EXPECT_EQ ( gg, s1[index]);
	}
}

TEST (twobit_2, GetSubStr)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
	EXPECT_EQ ( z.GetSubStr(0,77), s1.substr(0,77) );
//	std::cerr<< z.GetSubStr (0, 77)<<std::endl;
//	std::cerr<< s1.substr (0, 77)<<std::endl;
}

TEST (twobit_2, MakeString)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
	EXPECT_EQ ( z.MakeSeqString(), s1 );
	EXPECT_EQ ( z.MakeTWBString(), boost::lexical_cast <std::string> (z.GetSeq()));
}

TEST (twobit_2, GetSize)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
	EXPECT_EQ ( z.GetSize(), s1.size()<<1 );
}

//Complexes
TEST (twobit_2, reverse)
{
	std::string s1 ("ATCGRREGVVCCDD");//RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	TwoBitSequence< TwoBit_types::Mask > z ( s1 );
	z.reverse();
	std::reverse (s1.begin(), s1.end() );

	EXPECT_EQ ( z.MakeSeqString(), s1);
}

TEST (twobit_2, complement)
{
//	std::string s1 ("abcATCGRRabcEGVVabcdeqrCCDDbcxji");//
//	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	std::string s1 ("aCCzbjRFVeefbjCCIOJCATGGCGp");
	TwoBitSequence< TwoBit_types::Mask > z (s1), cc (s1);
	z.complement();
	for (auto i = 0; i != s1.size()<<1; ++i)
	{
		EXPECT_EQ (z.GetSeq()[i], (cc.GetSeq()[i] ^= 1) );
	}

}

TEST (twobit_2, reverse_complement)
{
	std::string s1 ("aNewrAJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");
	TwoBitSequence< TwoBit_types::Mask > c ( s1 ), cc (s1);
//	auto str2 = cc.MakeSeqString();
	auto l = s1.size() << 1;
	c.reverse_complement();
//	auto str1 = c.MakeSeqString();
	auto index = 0;
	while (index < l)
//	for (auto index = 0; index<l; index+2)
	{
		EXPECT_EQ ( c.GetSeq()[index], (cc.GetSeq()[l-index-2] ^= 1) );
//		std::cerr<<"c["<<index<<"] and c["<<index+1<<"] "<<c.GetSeq()[index]<<'\t'<<
//		c.GetSeq()[index+1] <<'\t';
//		std::cerr<<"cc["<<l - index-2<<"] and cc["<< l - index-1<<" ] "<<cc.GetSeq()[l-index-2]<<'\t'<<
//		cc.GetSeq()[l-index-1] <<std::endl;
		index += 2;
	}
}

TEST (twobit_2, reverse_copy)
{
	std::string s1 ("NATCGNabcGJIOEROTGHDSDFHJIERWEYITYaJREWEOIJGGCC");
	TwoBitSequence< TwoBit_types::Mask > c ( s1 );
//	c.Printer();
//	c.TWBPrinter();
	auto str = c.MakeSeqString();
//	std::cerr<<"c's converted back code\t"<<str<<std::endl;
	auto gg = c.reverse_copy();
	auto str3 = gg.MakeSeqString();
//	std::cerr<<"c's reverse string\t"<<str3<<std::endl;
	auto str4 = c.MakeTWBString();
//	std::cerr<<"c's reverse code\t"<<str4<<std::endl;
	for (auto i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}

TEST (twobit_2, complement_copy)
{
//	std::cerr<<"\ncomplement_copy test"<<std::endl;
	std::string s1 ("NATCGNabcdQWWQQVVXMCDJFQHGzvbAAGGGTTT");
	TwoBitSequence< TwoBit_types::Mask > c ( s1 ), cc (s1);
	auto l = (s1.size()<<1);
	auto gg = c.complement_copy();
	for (auto i = 0; i != l; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (gg.GetSeq()[i] ^= 1) );
	}
}

TEST (twobit_2, reverse_complement_copy)
{
	std::string s1 ("NAaCabGcTfNNgqNqAeCrGTTqGeCAqNebGqNANGNJNCVOJEOICNWEFIJOINBJFTNNNA");
	TwoBitSequence< TwoBit_types::Mask > c ( s1 ), cc (s1);
	auto str2 = cc.MakeSeqString();
	auto l = s1.size() << 1;
	auto gg = c.reverse_complement_copy();
	auto str1 = c.MakeSeqString();
	auto index = 0;
	while (index < l)
	{
		EXPECT_EQ ( gg.GetSeq()[index], (cc.GetSeq()[l-index-2] ^= 1) );
		index += 2;
	}
}

