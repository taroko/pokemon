#include "gtest/gtest.h"
#include "sam2bam_up.hpp"
#include "../src/file_reader_impl.hpp"
#include "../src/format/annotation_raw_bed.hpp"
#include "../src/format/raw_bed.hpp"

//int main (void)
TEST (Sam2BamUp, test_from_sam)
{
    typedef std::tuple <
        std::string, //QNAME                                                                                                                                           
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR 
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
		UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
//        UserDefineContent
                        > TUPLETYPE;

    std::string file_name ("../out.sam");//("test.sam");
    std::ifstream file_handle (file_name);
    std::vector<std::string> QQ ({file_name});
    FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);
	size_t index=0;
	std::vector <Sam <TUPLETYPE> > AAA, BBB;


//=========round #1 reading
std::cerr<<"read round #1"<<'\n';
    while //(true)
	(index!=1)
    {   
//        std::cerr<<"index "<<index<<'\n';
        //AAA.push_back //
		Sam <TUPLETYPE> qq = 
		( Sam_Reader_test.get_next_entry (0) );
        if (qq.eof_flag)
            break;
		AAA.push_back ( (qq) );
		BBB.push_back ( (qq) );
        ++index;
    }
	
	auto ccc = std::make_tuple (std::string("@HD\tVN:1.0\tSO:unsorted\n@SQ\tSN:chrX\tLN:151058754\n"), AAA);
	auto ccd = std::make_tuple (std::string("@HD\tVN:1.0\tSO:unsorted\n@SQ\tSN:chrX\tLN:151058754\n"), BBB);
	Sam2BamUp<>//std::tuple<std::string, std::vector< Sam<> > >* > 
	s2b(0, "o1.bam", &ccc), s2b2(0, "o2.bam", &ccd);
std::cerr<<"done read round #1"<<'\n';

	//reader 要給eof訊息
	s2b.run(&ccc);
	s2b2.run(&ccd);

//=========round #2 reading
std::cerr<<"read round #2"<<'\n';
	AAA.resize(0), BBB.resize(0);
	index=0;
    while //(true)
	(index!=10)
    {   
		Sam <TUPLETYPE> qq = 
		( Sam_Reader_test.get_next_entry (0) );
        if (qq.eof_flag)
            break;
		AAA.push_back ( (qq) );
		BBB.push_back ( (qq) );
        ++index;
    }
std::cerr<<"done read round #2"<<'\n';

	s2b.run(&ccc);
	s2b2.run(&ccd);

//=========round #3 reading
std::cerr<<"read round #3"<<'\n';
	AAA.resize(0), BBB.resize(0);
	index=0;
    while //(true)
	(index!=10)
    {   
		Sam <TUPLETYPE> qq = 
		( Sam_Reader_test.get_next_entry (0) );
        if (qq.eof_flag)
            break;
		AAA.push_back ( (qq) );
		BBB.push_back ( (qq) );
        ++index;
    }
std::cerr<<"done read round #3"<<'\n';

	s2b.run(&ccc);
	s2b2.run(&ccd);

	//if( eof_)
	{
		std::cerr<<"eof terminator"<<std::endl;
		s2b.final_Sam2Bam();
		s2b2.final_Sam2Bam();
	}
};

TEST (Sam2BamUp, test_from_annotation_rawbed)
{
    typedef std::tuple <
        std::string, //QNAME                                                                                                                                           
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR 
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
        UserDefineContent
                        > TUPLETYPE;

    std::string file_name ("../out.sam");//("test.sam");
    std::ifstream file_handle (file_name);
    std::vector<std::string> QQ ({file_name});
    FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);
	std::vector <Sam <TUPLETYPE> > AAA;
    std::vector < RawBed<> > BBB;
    std::map < RawBed<>, uint16_t > DDD;

//=========round #1 reading
std::cerr<<"read round #1"<<'\n';
	size_t index=0;
    while (index!=1)
    {   
        std::cerr<<"index "<<index<<'\n';
        const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
        if (qq.eof_flag)
            break;
        RawBed <> yy (qq);
        //std::cerr<<"rawbed "<<yy<<'\n';
        ++ ( *( ( &( DDD[RawBed<>(qq)]) ) - 5 ) );
        ++index;
    }
    std::vector < AnnotationRawBed<> >EEE;
    for (auto itr=DDD.begin(); itr!=DDD.end(); ++itr)
        EEE.push_back ( AnnotationRawBed <> (itr->first) );
	auto ccc = std::make_tuple (std::string("@HD\tVN:1.0\tSO:unsorted\n@SQ\tSN:chrX\tLN:151058754\n"), AAA);
std::cerr<<"done read round #1"<<'\n';
	std::string hdr ("@HD\tVN:1.0\tSO:unsorted\n@SQ\tSN:chrX\tLN:151058754\n");

{
//	Sam2Bam< std::tuple<std::string, std::vector< Sam<> > >* > s2b(0, "o3.bam", &ccc), s2b2(0, "o4.bam", &ccc);
	Sam2BamUp< AnnotationRawBed <> > s2b(0, std::string("o3.bam"), hdr), s2b2(0, std::string("o4.bam"), hdr);
	//reader 要給eof訊息
	for (auto& qq : EEE)
	{
		std::string yy = qq.ToSam();
		s2b.run(yy);
		s2b2.run(yy);
	}
	//if( eof_)
	{
		std::cerr<<"eof terminator"<<std::endl;
//		s2b.final_Sam2Bam();
		s2b.end_Sam2Bam();
		s2b2.end_Sam2Bam();
	}
}

{
//	Sam2Bam< std::tuple<std::string, std::vector< Sam<> > >* > s2b(0, "o3.bam", &ccc), s2b2(0, "o4.bam", &ccc);
	Sam2BamUp< AnnotationRawBed <> > s4b(0, std::string("o3.bam"), hdr), s4b2(0, std::string("o4.bam"), hdr);
	//reader 要給eof訊息
	for (auto& qq : EEE)
	{
		//std::string yy = qq.ToSam();
		//s4b.run(yy);
		//s4b2.run(yy);
		s4b.run (qq);
		s4b2.run (qq);
	}
	//if( eof_)
	{
		std::cerr<<"eof terminator"<<std::endl;
//		s2b.final_Sam2Bam();
		s4b.end_Sam2Bam();
		s4b2.end_Sam2Bam();
	}
}

{
//=========round #2 reading
std::cerr<<"read round #2"<<'\n';
	AAA.resize(0), EEE.resize(0);
	index=0;
    while (index!=1)
    {   
        std::cerr<<"index "<<index<<'\n';
        const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
        if (qq.eof_flag)
            break;
        RawBed <> yy (qq);
        //std::cerr<<"rawbed "<<yy<<'\n';
        ++ ( *( ( &( DDD[RawBed<>(qq)]) ) - 5 ) );
        ++index;
    }
//	Sam2BamUp< std::tuple<std::string, std::vector< Sam<> > >* > s3b(1, "o5.bam", &ccc), s3b2(1, "o6.bam", &ccc);
	Sam2BamUp< AnnotationRawBed <> > s3b(1, std::string("o5.bam"), hdr), s3b2(1, std::string("o6.bam"), hdr);
    for (auto itr=DDD.begin(); itr!=DDD.end(); ++itr)
        EEE.push_back ( AnnotationRawBed <> (itr->first) );
std::cerr<<"done read round #2"<<'\n';

	for (auto& qq : EEE)
	{
//		std::string yy = qq.ToSam();
//		s3b.run(yy);
//		s3b2.run(yy);
		s3b.run (qq);
		s3b2.run (qq);
	}
	std::cerr<<"eof terminator"<<std::endl;
	s3b.final_Sam2Bam();
	s3b2.final_Sam2Bam();
}

};


