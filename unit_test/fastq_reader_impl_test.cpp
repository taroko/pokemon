#include <tuple>
#include "gtest/gtest.h"
#include "../src/format_io_iterator.hpp"
#include "../src/format/fastq_reader_impl.hpp"
#include "../src/format/fastq.hpp"
#include "file_generator.hpp"
#include <fstream>

TEST(Fastq_reader_ifstream, io_end)
{
	FileReader_impl <Fastq, std::tuple <std::string, std::string, std::string, std::string >, SOURCE_TYPE::IFSTREAM_TYPE > Fastq_Reader_test;
	EXPECT_EQ( ( (Fastq_Reader_test.io_end() ) -> eof_flag ), true );
};

TEST(Fastq_reader_ifstream, get_next_entry)
{
	RandomFileGenerator fg;
	fg.GenRandFile< Fastq< std::tuple < std::string, std::string, std::string, std::string > > >();
	std::ifstream file_handle;
	file_handle.open (fg.temp_file_name, std::ifstream::in);
	std::vector < std::string > qq ({ fg.temp_file_name });
	//std::vector < std::string > qq ({ "test_file" });// ( {fg.temp_file_name });
	FileReader_impl <Fastq , std::tuple < std::string, std::string, std::string, std::string >, SOURCE_TYPE::IFSTREAM_TYPE > Fastq_Reader_test (qq);//(fg.temp_file_name);
	size_t index = 0;
	while (true)//(index < 252)
	{
		Fastq < std::tuple <std::string, std::string, std::string, std::string > > object
			= Fastq_Reader_test.get_next_entry (0);//(file_handle);
		bool flag = true;
		//for ( auto i =0; i!=object.size(); ++i )
		//	flag *= object[0].eof_flag;
		if ( object.eof_flag )
			break;
//		for ( auto i =0; i!=object.size(); ++i )
		std::cerr<<object;
		index++;
	EXPECT_EQ(std::get<0> (object.data), fg.GetContent(0) );
	EXPECT_EQ(std::get<1> (object.data), fg.GetContent(1) );
	EXPECT_EQ(std::get<2> (object.data), fg.GetContent(2) );
	EXPECT_EQ(std::get<3> (object.data), fg.GetContent(3) );
	}
};

TEST(Fastq_reader_gz_curl, io_end)
{
	std::vector <std::string> vec1 
	({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
	});
  	std::vector <uint64_t> vec2 ({7493990});
	FileReader_impl <Fastq, std::tuple <std::string, std::string, std::string, std::string >, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex > Fastq_Reader_test (vec1, vec2);
	EXPECT_EQ( ( (Fastq_Reader_test.io_end() ) -> eof_flag ), true );
};

TEST(Fastq_reader_gz_curl, get_next_entry)
{
	//sequential
	//----------------------------------------------------------------
	//400mb*2
	//VIRT 233mb RES 55mb buf x1, VIRT 284mb RES 105mb toward ending
	//120sec cpu time, 90sec real time

	//MT
	//----------------------------------------------------------------
	//400mb*2 
	//VIRT 485mb RES 107mb buf x1, VIRT 284mb RES 105mb toward ending
	//104sec cpu time, 82sec real time

	//10gb*2
	//VIRT 481mb RES 106mb buf x1, VIRT 481mb RES 162mb toward ending
	//4467 sec cpu time, 40:18.22 real time

	std::vector <std::string> fn ({ "test_file/output1_reader_impl", "test_file/output2_reader_impl"});
	std::vector<std::shared_ptr <std::ofstream> > of;
	for (auto i=0; i!=fn.size(); ++i)
		of.push_back (std::make_shared<std::ofstream>(fn[i]));

	typedef std::tuple < std::string, std::string, std::string, std::string > TUPLETYPE;
	std::vector <std::string> vec1 ({
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
	//"http://localhost/test/400m-1.fastq.gz",
	//"http://localhost/test/400m-2.fastq.gz"																		  
	//"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259",
	//"https://api.basespace.illumina.com/v1pre3/files/3360156/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"
	//"http://localhost/test/10g-1.fastq.gz",
	//"http://localhost/test/10g-2.fastq.gz"
	});
  	std::vector <uint64_t> vec2 ({
	7493990, 7493990
	//444670062, 463990642 
	//10548716385, 10739796450 
	});
	FileReader_impl <Fastq , TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex > 
	QQ (vec1, vec2);

	std::vector <std::thread> thread_vec;

	for (auto ii=0; ii!=vec1.size(); ++ii)
	{
		std::cerr<<"current index "<<ii<<'\r';
		thread_vec.push_back ( std::move (
			std::thread (
		[ii, &QQ, &of ] ()
		{
		while (true)
		{
			Fastq < TUPLETYPE > object
				= QQ.get_next_entry (ii);
			bool flag = true;
			if ( object.eof_flag )
				break;
			(*of[ii].get())<<object;
			//std::cerr<<"result "<<object<<'\r';
		}
		}) ) );
	}
	for (auto& q : thread_vec)
		q.join();
};

TEST(FastqBarcode_reader_ifstream, get_next_entry)
{
	RandomFileGenerator fg;
	fg.GenRandFile< Fastq< std::tuple < std::string, std::string, std::string, std::string > > >();
	std::ifstream file_handle;
	file_handle.open (fg.temp_file_name, std::ifstream::in);
	std::vector < std::string > qq ({ fg.temp_file_name });
	//std::vector < std::string > qq ({ "test_file" });// ( {fg.temp_file_name });
	FileReader_impl <FastqBarcode, 
					 std::tuple < std::string, std::string, std::string, std::string >, 
					 SOURCE_TYPE::IFSTREAM_TYPE > Fastq_Reader_test (qq, 3);//(fg.temp_file_name);
	size_t index = 0;
	while (true)//(index < 252)
	{
		Fastq < std::tuple <std::string, std::string, std::string, std::string > > object
			= Fastq_Reader_test.get_next_entry (0);//(file_handle);
		bool flag = true;
		if ( object.eof_flag )
			break;
		std::cerr<<object;
		index++;
		EXPECT_EQ(std::get<0> (object.data), fg.GetContent(0) );
		EXPECT_EQ(std::get<1> (object.data), fg.GetContent(1).replace(0, 3, ""));
		EXPECT_EQ(std::get<2> (object.data), fg.GetContent(2) );
		EXPECT_EQ(std::get<3> (object.data), fg.GetContent(3).replace(0, 3, ""));
	}
};


/*
TEST(Fastq_reader_plain_curl, io_end)
{
	std::vector <std::string> vec1 ({"140.113.15.99/aa.fa", "140.113.15.99/aa.fa"});
	std::vector <size_t> vec2 ({0, 0});
	FileReader_impl <Fastq, std::tuple <std::string, std::string, std::string, std::string >, SOURCE_TYPE::CURL_TYPE_PLAIN, curl_default_handle_mutex > Fastq_Reader_test (vec1, vec2);
	EXPECT_EQ( ( (Fastq_Reader_test.io_end() ) -> eof_flag ), true );
};

TEST(Fastq_reader_plain_curl, get_next_entry)
{
	typedef std::tuple < std::string, std::string > TUPLETYPE;
	std::vector <std::string> vec1 ({"140.113.15.99/aa.fa", "140.113.15.99/aa.fa"});
	std::vector <size_t> vec2 ({0, 0});
	FileReader_impl <Fastq, std::tuple <std::string, std::string, std::string, std::string >, SOURCE_TYPE::CURL_TYPE_PLAIN, curl_default_handle_mutex> Fastq_Reader_test (vec1, vec2);

	for (auto ii=0; ii!=vec1.size(); ++ii)
	{
		std::cerr<<"current index "<<ii<<'\n';
		while (true)
		{
			Fastq < std::tuple <std::string, std::string, std::string, std::string > > object
				= Fastq_Reader_test.get_next_entry (ii);
			bool flag = true;
			if ( object.eof_flag )
				break;
			std::cerr<<"result "<<object<<'\n';
		}
	}
};
*/
