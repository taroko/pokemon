#include "../src/tuple_utility.hpp"
#include "../src/wrapper_tuple_utility.hpp"
#include "gtest/gtest.h"
//#include "gtest/gtest.h"
using namespace std;

TEST(tuple_utility, Fill_print)
{
	uint32_t a = 55699, b=55699;
	tuple<string, uint32_t, uint32_t> target;
	vector<string> split_temp = {"abcd", "55699", "55699"};
	TupleUtility< tuple<string, uint32_t, uint32_t>, 3 >::FillTuple( target, split_temp );
	EXPECT_EQ (std::get<0> (target),"abcd");
	EXPECT_EQ (std::get<1> (target), a);
	EXPECT_EQ (std::get<2> (target), b);
//	TupleUtility< tuple<string, uint32_t, uint32_t>, 3 >::PrintTuple(std::cout, target);
    std::string s1;
	TupleUtility< tuple<string, uint32_t, uint32_t>, 3 >::PrintTuple(target, s1);
    EXPECT_EQ (s1, "abcd\n55699\n55699\n");
}

TEST(tuple_utility, Wrapper_TUPLE_Wrapper)
{
	typedef Wrapper< tuple<int, double> > TupleType;
	vector<string> split_temp = {"*523#19.3"};
	std::tuple < TupleType > qq;
	TupleUtility< std::tuple< TupleType > , 1>::FillTuple( qq, split_temp );
	EXPECT_EQ (std::get<0>( std::get<0> (qq).data_content ) ,523);
	EXPECT_EQ (std::get<1>( std::get<0> (qq).data_content ) ,19.3);
//	TupleUtility< std::tuple< TupleType > , 1>::PrintTuple( std::cout, qq);
    std::string s1;
	TupleUtility< tuple< TupleType >, 1 >::PrintTuple(qq, s1);
    EXPECT_EQ (s1, "523\n19.3\n");
}

TEST(tuple_uitility, Wrapper_VECTOR_Wrapper)
{
    typedef Wrapper< tuple<int, double> > TupleType;
    typedef Wrapper< std::vector< TupleType > > VectorType;
    typedef std::tuple < std::string, std::string, int, VectorType > w_i_g;
    w_i_g qq;
    std::vector <std::string> split_temp ({
    "abcdefg",
    "12345",
    "999",
    "*1230#777.65*1220#666.54*1210#555.13"} );
    TupleUtility< w_i_g, 4>::FillTuple ( qq, split_temp );
    EXPECT_EQ (std::get<0>(qq), "abcdefg");
    EXPECT_EQ (std::get<1>(qq), "12345");
    EXPECT_EQ (std::get<2>(qq), 999);
    EXPECT_EQ (std::get<3>(qq).get<0>().get<0>(), 1230);
    EXPECT_EQ (std::get<3>(qq).get<0>().get<1>(), 777.65);
    EXPECT_EQ (std::get<3>(qq).get<1>().get<0>(), 1220);
    EXPECT_EQ (std::get<3>(qq).get<1>().get<1>(), 666.54);
    EXPECT_EQ (std::get<3>(qq).get<2>().get<0>(), 1210);
    EXPECT_EQ (std::get<3>(qq).get<2>().get<1>(), 555.13);
}
