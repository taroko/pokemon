
#include "abit_include.hpp"
#include <chrono>
using namespace std;

int main (int argc, char** argv) {

	if (argc < 3) {
		std::cerr << argv[0] << " input.fa prefix query.fq" << std::endl;
		exit (1);
	}
	bowhan::buildBWT (argv[1], argv[2]);
	bowhan::tailing (argv[2], argv[3]);
}
