#include <fstream>
#include <deque>
#include <map>
#include <unordered_map>
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "../src/parser.hpp"
//#include "../src/bwt.hpp"
#include "gtest/gtest.h"


TEST (parser, test1)
{
	std::vector<std::string> file_names (
		{
			"/Users/obigbando/Documents/work/test_file/test.fastq",
			"/Users/obigbando/Documents/work/test_file/test.fastq"
		});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<2; i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string,
						std::string,
						std::string
						> TUPLETYPE;
	FileParser < ParallelTypes::NORMAL, Fastq, TUPLETYPE, CompressType::N_BITS_MASK, std::deque, 2 > GG;
	auto x = GG.Read ( file_handles );
	std::for_each (	x->begin(), x->end(),
					[&GG] (const std::pair<int, std::vector< Fastq<TUPLETYPE> > >& Q)
					{ 
					std::for_each (Q.second.begin(), Q.second.end(),
						[&GG] (const Fastq<TUPLETYPE>& q)
							{GG.push_back (q);});
					});
//	std::for_each (GG.begin(), GG.end(),
//					[] (const Fastq<TUPLETYPE>& Q)
//					{ std::cerr<<Q<<std::endl;}
//					);
}

TEST (parser, test2)
{
	std::vector<std::string> file_names (
		{
			"/Users/obigbando/Documents/work/test_file/test.fastq",
			"/Users/obigbando/Documents/work/test_file/test.fastq"
		});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<2; i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string,
						std::string,
						std::string
						> TUPLETYPE;
	FileParser < ParallelTypes::NORMAL, Fastq, TUPLETYPE, CompressType::N_BITS_MASK, std::map, 2 > GG2;
	auto xx = GG2.Read (file_handles);
	int index = 0;
	std::for_each (	xx->begin(), xx->end(),
					[&] (const std::pair<int, std::vector< Fastq<TUPLETYPE> > >& Q)
					{ 
					std::for_each (Q.second.begin(), Q.second.end(),
						[&] (const Fastq<TUPLETYPE>& q)
						{	GG2.insert ({index, q}); ++index; });
					});
//	std::for_each (GG2.begin(), GG2.end(),
//					[] (const std::pair<int, Fastq<TUPLETYPE> >& Q)
//					{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;}
//					);
}

TEST (parser, test3)
{
	std::vector<std::string> file_names (
		{
			"/Users/obigbando/Documents/work/test_file/test.fastq",
			"/Users/obigbando/Documents/work/test_file/test.fastq"
		});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<2; i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string,
						std::string,
						std::string
						> TUPLETYPE;
	FileParser < ParallelTypes::NORMAL, Fastq, TUPLETYPE, CompressType::N_BITS_MASK, std::unordered_map, 2 > GG3;
	auto xxx = GG3.Read (file_handles);
	int index = 0;
	std::for_each (	xxx->begin(), xxx->end(),
					[&] (const std::pair<int, std::vector< Fastq<TUPLETYPE> > >& Q)
					{ 
					std::for_each (Q.second.begin(), Q.second.end(),
						[&] (const Fastq<TUPLETYPE>& q)
						{	GG3.insert ({index, q}); ++index; });
					});

//	std::for_each (GG3.begin(), GG3.end(),
//					[] (const std::pair<int, Fastq<TUPLETYPE> >& Q)
//					{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;}
//					);
}

/// @brief an exemplary test illustrating how can we use the FileParser class
template< ParallelTypes ParaType,
			template < typename > class FORMAT,
			typename TUPLETYPE,
			CompressType CompType,
			template < typename... > class CONTAINER,
			int N
		 >
class MyParser
	: public FileParser <ParaType, FORMAT, TUPLETYPE, CompType, CONTAINER, N>
{
public:
	MyParser ()
		: FileParser <ParaType, FORMAT, TUPLETYPE, CompType, CONTAINER, N> ()
	{}
	std::map<int, std::vector< FORMAT<TUPLETYPE> > >* Parse ( std::vector<std::ifstream*>& read_queue, 
						std::vector <NBitCompress<CompressRuleTrait<N, CompType> > >& vec_comp )
	{
		std::cerr<<"overloading parse function"<<'\r';
		auto x = this->Read (read_queue);
		std::cerr<<"done reading, with x's size of "<<x->size()<<'\r';
		std::for_each ( x->begin(), x->end(),
				[& vec_comp, this] (const std::pair<int, std::vector< FORMAT<TUPLETYPE> > >& Q)
				{
					std::for_each ( Q.second.begin(), Q.second.end(),
					[&] (const FORMAT<TUPLETYPE>& q)
					{	vec_comp.push_back (this->encode (std::get<1>(q.data)));});
				});
		std::cerr<<"done parsing, with vec_comp's size of "<<vec_comp.size()<<'\r';
		return x;
	}

	void Merge (std::vector<NBitCompress<CompressRuleTrait<N, CompType> > >& vec_comp, 
									NBitCompress<CompressRuleTrait<N, CompType> >& result)
	{
		std::cerr<<"sequence merging"<<'\r';
		std::for_each (vec_comp.begin(), vec_comp.end(),
			[&] (const NBitCompress<CompressRuleTrait<N, CompType> >& Q)
			{result.append (Q);});
		NBitCompress<CompressRuleTrait<N, CompType> > terminal ("$");
		result.append (terminal);
		std::cerr<<"done merging"<<'\r';
	}
};

/*
TEST (indexer, encode_step)   //whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{
	std::vector<std::string> file_names (
			{
					"/Users/obigbando/Documents/work/db/chr1.fa",
					"/Users/obigbando/Documents/work/db/chr2.fa",
					"/Users/obigbando/Documents/work/db/chr3.fa",
					"/Users/obigbando/Documents/work/db/chr4.fa",
					"/Users/obigbando/Documents/work/db/chr5.fa",
					"/Users/obigbando/Documents/work/db/chr6.fa",
					"/Users/obigbando/Documents/work/db/chr7.fa",
					"/Users/obigbando/Documents/work/db/chr8.fa",
					"/Users/obigbando/Documents/work/db/chr9.fa",
					"/Users/obigbando/Documents/work/db/chr10.fa",
					"/Users/obigbando/Documents/work/db/chr11.fa",
					"/Users/obigbando/Documents/work/db/chr12.fa",
					"/Users/obigbando/Documents/work/db/chr13.fa",
					"/Users/obigbando/Documents/work/db/chr14.fa",
					"/Users/obigbando/Documents/work/db/chr15.fa",
					"/Users/obigbando/Documents/work/db/chr16.fa",
					"/Users/obigbando/Documents/work/db/chr17.fa",
					"/Users/obigbando/Documents/work/db/chr18.fa",
					"/Users/obigbando/Documents/work/db/chr19.fa",
					"/Users/obigbando/Documents/work/db/chr20.fa",
					"/Users/obigbando/Documents/work/db/chr21.fa",
					"/Users/obigbando/Documents/work/db/chr22.fa",
					"/Users/obigbando/Documents/work/db/chrM.fa",
					"/Users/obigbando/Documents/work/db/chrX.fa",
					"/Users/obigbando/Documents/work/db/chrY.fa"
			});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<file_names.size(); i++)
			file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string, std::string > TUPLETYPE;
	MyParser < ParallelTypes::M_T, Fasta, TUPLETYPE, CompressType::N_BITS, std::vector, 2 > GG4;
	std::vector < NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > > vec_comp1;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > result;

	auto mp = GG4.Parse (file_handles, vec_comp1);
	GG4.Merge (vec_comp1, result);
*/
/*	EXPECT_EQ (result.MakeSeqString(),  std::get<1>((*mp)[0].data) + 
										std::get<1>((*mp)[1].data) +
										std::get<1>((*mp)[2].data) +
										std::get<1>((*mp)[3].data) +
										std::get<1>((*mp)[4].data) +
										std::get<1>((*mp)[5].data) +
										std::get<1>((*mp)[6].data) +
										std::get<1>((*mp)[7].data) +
										std::get<1>((*mp)[8].data) +
										std::get<1>((*mp)[9].data) +
										std::get<1>((*mp)[10].data) +
										std::get<1>((*mp)[11].data) +
										std::get<1>((*mp)[12].data) +
										std::get<1>((*mp)[13].data) +
										std::get<1>((*mp)[14].data) +
										std::get<1>((*mp)[15].data) +
										std::get<1>((*mp)[16].data) +
										std::get<1>((*mp)[17].data) +
										std::get<1>((*mp)[18].data) +
										std::get<1>((*mp)[19].data) +
										std::get<1>((*mp)[20].data) +
										std::get<1>((*mp)[21].data) +
										std::get<1>((*mp)[22].data) +
										std::get<1>((*mp)[23].data) +
										std::get<1>((*mp)[24].data) +
										"$"
				);
	std::cerr<<"merged result's seq, exceptdeq, maskdeq sizeinfo "<<result.GetSize()<<'\t'<<result.ExceptDeq.size()<<'\t'<<result.MaskDeq.size()<<std::endl;	
	std::cerr<<"aloha"<<std::endl;
*/
//	std::cerr<<"result's length "<<result.GetSize()/2<<'\t'<<result.ExceptDeq.size()<<std::endl;
/*
	BWT< CompressRuleTrait<2, CompressType::N_BITS>, 59373567> Q (result, 1000);
	std::for_each (Q.random_sort_temp.begin(), Q.random_sort_temp.end(),
		[&] ( const std::pair <uint64_t, NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > >& Q)
			{std::cerr<< Q.first << '\t' << Q.second.GetSize()/2 <<std::endl;});
	std::cerr<<"aloha"<<std::endl;
}
*/

/*
TEST (parser, test4_NORMAL)	//whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{
	std::vector<std::string> file_names (
		{			   
			"/Users/obigbando/Downloads/human_chr/chr1.fa"
		});
	std::vector <std::ifstream*> file_handles;
	for (size_t i=0; i<file_names.size(); i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string
						> TUPLETYPE;
	MyParser < ParallelTypes::M_T, Fasta, TUPLETYPE, CompressType::N_BITS, std::vector, 2 > GG4;
	std::vector < NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > > vec_comp1, vec_comp2(1);

	GG4.Parse (file_handles, vec_comp1);

	std::ofstream out_file ("/Users/obigbando/Downloads/human_chr/chr1parse.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	std::for_each (vec_comp1.begin(), vec_comp1.end(),
					[&] (NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> >& Q)
					{ arc & Q;}
					);	//simply savie archive takes nearly no time
	out_file.close();

	std::cerr<<"done archive saving"<<'\r';	
	std::ifstream in_file ("/Users/obigbando/Downloads/human_chr/chr1parse.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	std::for_each (vec_comp2.begin(), vec_comp2.end(),
					[&] (NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> >& Q)
					{ arci & Q;	EXPECT_EQ (Q, vec_comp1[0]);}
					);	//simply load archive takes nearly no time
	std::cerr<<"done archive loading"<<'\r';
	in_file.close();
}

TEST (parser, test4_MASK)   // whole read, parse, encode, serialize, and de-serialize takes about 19 secs
{							// can be better: MaskDeq holds a std::string, which can be further encoded with similar encode/compress scheme
	std::vector<std::string> file_names (
		{			   
			"/Users/obigbando/Downloads/human_chr/chr1.fa"
		});
	std::vector <std::ifstream*> file_handles;
	for (size_t i=0; i<file_names.size(); i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string
						> TUPLETYPE;
	MyParser < ParallelTypes::M_T, Fasta, TUPLETYPE, CompressType::N_BITS_MASK, std::vector, 2 > GG4;
	std::vector < NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > > vec_comp1, vec_comp2;

	GG4.Parse (file_handles, vec_comp1);

	std::ofstream out_file ("/Users/obigbando/Downloads/human_chr/chr1parsemask.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	std::for_each (vec_comp1.begin(), vec_comp1.end(),
					[&] (NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> >& Q)
					{ arc & Q;}
					);	//simply savie archive takes nearly no time
	out_file.close();
	std::cerr<<"done archive saving"<<'\r';	

	std::ifstream ofs ("/Users/obigbando/Downloads/human_chr/chr1.fa");
	std::string sstr1;
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);

	std::ifstream in_file ("/Users/obigbando/Downloads/human_chr/chr1parsemask.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	std::for_each (vec_comp2.begin(), vec_comp2.end(),
					[&] (NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> >& Q)
					{ arci & Q;	
					EXPECT_EQ (Q, vec_comp1[0]);
	Q.MakeSeqString(sstr1);
	EXPECT_EQ (sstr1, std::get<1>(x.data));						
					}
					);	//simply load archive takes nearly no time
	in_file.close();
	std::cerr<<"done archive loading"<<'\r';
//	std::ifstream ofs ("/Users/obigbando/Downloads/chr1.fa");
//	std::cerr<<"A"<<std::endl;
//	std::string sstr1;
//	std::cerr<<"A"<<std::endl;
//	Q.MakeSeqString(sstr1);
//	std::cerr<<"A"<<std::endl;
//	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
//	std::cerr<<"A"<<std::endl;
//	EXPECT_EQ (sstr1, std::get<1>(x.data));						
//	std::cerr<<"A"<<std::endl;
}

TEST(parser, test4_MASK_check)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/human_chr/chr1.fa");
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
	std::cerr<<"done reading"<<'\r';

	typedef std::tuple < std::string,
						std::string 
						> TUPLETYPE;
	MyParser < ParallelTypes::M_T, Fasta, TUPLETYPE, CompressType::N_BITS_MASK, std::vector, 2 > GG4;
	std::vector < NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > > vec_comp2(1);
	std::cerr<<"done initialize"<<'\r';

	std::ifstream in_file ("/Users/obigbando/Downloads/human_chr/chr1parsemask.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	std::for_each (vec_comp2.begin(), vec_comp2.end(),
					[&] (NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> >& Q)
					{ arci & Q; });  //simply load archive takes nearly no time
	std::cerr<<"done archive loading"<<'\r';
	in_file.close();

	std::string sstr;
	vec_comp2[0].MakeSeqString(sstr);
	EXPECT_EQ (sstr, std::get<1>(x.data));	//since NORMAL scheme have all lowercased acgt converted into their capitalized form, mismatch is due to happen
}

TEST (parser, test4_NORMAL_6)	//whole read, parse, encode, serialize, and de-serialize takes about 16 secs
{
	std::vector<std::string> file_names (
		{			   
			"/Users/obigbando/Downloads/human_chr/chr1.fa"
		});
	std::vector <std::ifstream*> file_handles;
	for (size_t i=0; i<file_names.size(); i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string
						> TUPLETYPE;
	MyParser < ParallelTypes::M_T, Fasta, TUPLETYPE, CompressType::N_BITS, std::vector, 6 > GG4;
	std::vector < NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > > vec_comp1, vec_comp2(1);

	GG4.Parse (file_handles, vec_comp1);

	std::ofstream out_file ("/Users/obigbando/Downloads/human_chr/chr1parse_6.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	std::for_each (vec_comp1.begin(), vec_comp1.end(),
					[&] (NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> >& Q)
					{ arc & Q;}
					);	//simply savie archive takes nearly no time
	out_file.close();

	std::cerr<<"done archive saving"<<'\r';	
	std::ifstream in_file ("/Users/obigbando/Downloads/human_chr/chr1parse_6.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	std::for_each (vec_comp2.begin(), vec_comp2.end(),
					[&] (NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> >& Q)
					{ arci & Q;	
					EXPECT_EQ (Q, vec_comp1[0]);}
					);	//simply load archive takes nearly no time
	std::cerr<<"done archive loading"<<'\r';
	in_file.close();
}

TEST (parser, test4_MASK_6)   // whole read, parse, encode, serialize, and de-serialize takes about 19 secs
{							// can be better: MaskDeq holds a std::string, which can be further encoded with similar encode/compress scheme
std::cerr<<"test4_MASK_6"<<'\r';
	std::vector<std::string> file_names (
		{			   
			"/Users/obigbando/Downloads/human_chr/chr1.fa"
		});
	std::vector <std::ifstream*> file_handles;
	for (size_t i=0; i<file_names.size(); i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string
						> TUPLETYPE;
	MyParser < ParallelTypes::M_T, Fasta, TUPLETYPE, CompressType::N_BITS_MASK, std::vector, 6 > GG4;
	std::vector < NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> > > vec_comp1(0), vec_comp2(0);

	GG4.Parse (file_handles, vec_comp1);

	std::ofstream out_file ("/Users/obigbando/Downloads/human_chr/chr1parse_mask_6.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	std::for_each (vec_comp1.begin(), vec_comp1.end(),
					[&] (NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> >& Q)
					{ arc & Q;}
					);	//simply savie archive takes nearly no time
	out_file.close();
	std::cerr<<"done archive saving"<<'\r';	
	
	std::ifstream in_file ("/Users/obigbando/Downloads/human_chr/chr1parse_mask_6.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	
	std::for_each (vec_comp2.begin(), vec_comp2.end(),
					[&] (NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> >& Q)
					{ arci & Q;	
					EXPECT_EQ (Q, vec_comp1[0]);
	std::string sstr1;
	Q.MakeSeqString(sstr1);
	std::ifstream ofs ("/Users/obigbando/Downloads/human_chr/chr1.fa");
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
	EXPECT_EQ (sstr1, std::get<1>(x.data));						
					}
					);	//simply load archive takes nearly no time
	in_file.close();
//	std::cerr<<"done archive loading"<<std::endl;
//	std::ifstream ofs ("/Users/obigbando/Downloads/chr1.fa");
//	std::string sstr1;
//	vec_comp2[0].MakeSeqString(sstr1);
//	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
//	EXPECT_EQ (sstr1, std::get<1>(x.data));						

}
*/

/*
TEST (parser, test3)
{
	std::vector<std::string> file_names (
		{
			"/Users/obigbando/Documents/work/test_file/test.fastq",
			"/Users/obigbando/Documents/work/test_file/test.fastq"
		});
	std::vector <std::ifstream*> file_handles;
	for (int i=0; i<2; i++)
		file_handles.push_back (new std::ifstream ( file_names[i] ) );
	typedef std::tuple < std::string,
						std::string,
						std::string,
						std::string
						> TUPLETYPE;
	FileParser < ParallelTypes::NORMAL, Fastq, TUPLETYPE, CompressType::N_BITS_MASK, std::unordered_map > GG3;
	auto xxx = GG3.Read (file_handles);

	std::for_each (	xxx->begin(), xxx->end(),
					[&GG3] (const std::pair<int, Fastq<TUPLETYPE> >& Q)
					{ GG3.insert (Q);}
					);

	std::for_each (GG3.begin(), GG3.end(),
					[] (const std::pair<int, Fastq<TUPLETYPE> >& Q)
					{ std::cerr<<Q.first<<'\t'<<Q.second<<std::endl;}
					);
}
*/
