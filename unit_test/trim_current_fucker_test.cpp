//#include "../src/pair_end_adapter_trimmer_parameter.hpp"
#include "../src/pair_end_adapter_trimmer_parameter_current_06.hpp"
#include <sstream>

int main (int argc, char* argv[])
{
	std::vector <std::string> vec1  
		({
//	"https://api.basespace.illumina.com/v1pre3/files/113647546/content?access_token=4b2bf2c94ea34721ac01fc177111707f",
//	"https://api.basespace.illumina.com/v1pre3/files/113645006/content?access_token=4b2bf2c94ea34721ac01fc177111707f"
//	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
//	"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
	"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
	"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
		});  
	std::vector <size_t> vec2 
		({
		//18632783,
		//20187631
		//7493990,
		//7493990
		459083647,
		478161401
		});

	std::vector <std::string> upload_vec ({
		"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
		"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
		"Content-Type: application/text",
		"https://api.basespace.illumina.com/"
	});

	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;

//	std::vector<std::ifstream*> RQ;
	std::vector<std::string> file_vec ({
		argv[1],//"/nfs_sharing/oman/orig1.fastq",
		argv[2]//"/nfs_sharing/oman/orig2.fastq"
		});
//	std::for_each (read_vec.begin(), read_vec.end(),
//		[&] (const std::string& q)
//		{ RQ.push_back (new std::ifstream (q) ); });

	std::vector<std::stringstream*> WQ;
	std::vector < std::string > write_vec ({
	"qq1",//argv[1],//"/dev/shm/omanrealout1.txt" ,
	"qq2"//argv[2]//"/dev/shm/omanrealout2.txt" 
		});
	std::for_each (write_vec.begin(), write_vec.end(),
		[&] (const std::string& q)
		{ WQ.push_back (new std::stringstream (q//, std::ios_base::app
											) ); });

	std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc;
	std::map<int, std::vector< size_t > > ttt;

	size_t startsize_in;
//	double m_indicator = 0.4, a_mismatch = 0.6, g_mismatch = 0.4;
	std::stringstream ss (argv[3]);
	double m_indicator, a_mismatch, g_mismatch;
	char comma1, comma2, comma3;
	ss >> m_indicator >> comma1 >> a_mismatch >> comma2 >> g_mismatch >> comma3 >> startsize_in;
	
	ParameterTrait i_parameter_trait ( startsize_in );//, num_in, pool_size_in );


	PairEndAdaptiveTrimmer <ParallelTypes::NORMAL, 
						   Fastq, 
						   TUPLETYPE,
						   SOURCE_TYPE::CURL_TYPE_GZ, 
						   curl_default_handle,   
						   TrimTrait<std::string, LinearStrMatch<double> >, 
						   double,
						   double > QQ 
							 (  vec1, vec2, file_vec, upload_vec,
								&ccc, &ttt, //RQ, 
								WQ, i_parameter_trait, m_indicator, a_mismatch, g_mismatch);
	size_t sum = 0, rd_count = 0;
	int i = 0;

std::cerr<<"current buf size "<<QQ.Curl_device_[0]->thread_recv_volume_<<'\t'<<QQ.Curl_device_[1]->thread_recv_volume_<<std::endl;
	while (true)//(i<2)// (true)
	{
std::cout<<"round # "<<i<<'\n';
		if (!QQ.Parse_N_Trim_all (3))
			break;
		++i;
	}
std::cerr<<"done PNTA"<<std::endl;
//	for (auto gq : RQ)
//	{
//		gq->close();
//		delete gq;
//	}

	for (auto gq : WQ)
	{
		gq->close();
		delete gq;
	}
std::cerr<<"done of all "<<std::endl;
}
