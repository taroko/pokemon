#include "../src/trimmer/single_end_adapter_trimmer_impl.hpp"
#include "../src/file_reader.hpp"
#include "gtest/gtest.h"

TEST (single_end_adapter_trimmer, TrimImpl_test)
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	std::vector<std::string> read_vec ({"/mnt/godzilla/johnny/PEAT/million_0/origin0_1.fq", "/mnt/godzilla/johnny/PEAT/million_0/origin0_1.fq"});//({"test.fq", "test.fq"});
	std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc;//, cca;
	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
		QQA (read_vec, &ccc);
	QQA.Read (10000);

	ParameterTraitSeat <QualityScoreType::SANGER> Qoo ("AGATCGGAAGAGCGG");//("ATCGACGT");
	SingleEndAdapterTrimmer_impl < Fastq, TUPLETYPE, QualityScoreType::SANGER > QQ (Qoo);//(adapter_seq);////("ATCGACGT");
	std::vector<int> trim_pos;

	for (auto index=0; index!=10000; ++index)
		QQ.TrimImpl (ccc[0][index], trim_pos);

	for ( auto jj=0; jj!=ccc[0].size(); ++jj)
	{
		if (trim_pos[jj]!=0)
		{
			std::get<1>(ccc[1][jj].data).resize (trim_pos[jj]);
			std::get<3>(ccc[1][jj].data).resize (trim_pos[jj]);
		}
//std::cerr<<"ccc[0].size [1].size trim_pos.size "<<std::get<1>(ccc[0][jj].data).size()<<'\t'<<std::get<1>(ccc[1][jj].data).size()<<'\t'<<trim_pos[jj]<<std::endl;
		EXPECT_EQ ( std::get<0>(ccc[0][jj].data), std::get<0>(ccc[1][jj].data) ); 
		EXPECT_EQ ( std::get<1>(ccc[0][jj].data), std::get<1>(ccc[1][jj].data) ); 
		EXPECT_EQ ( std::get<2>(ccc[0][jj].data), std::get<2>(ccc[1][jj].data) ); 
		EXPECT_EQ ( std::get<3>(ccc[0][jj].data), std::get<3>(ccc[1][jj].data) ); 
	}
}


TEST (single_end_adapter_trimmer, TrimImpl_test2)
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	std::map<int, std::vector< Fastq<TUPLETYPE> > > mp1, mp2;
	std::vector<std::string> read_vec ({"/mnt/godzilla/johnny/PEAT/million_0/origin0_1.fq"});
	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
		QQ1 (read_vec, &mp1);
	QQ1.Read (10000);
	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
		QQ2 (read_vec, &mp2);
	QQ2.Read (10000);

	for (auto i=0; i!=10000; ++i)
	{
		EXPECT_EQ (mp1[0][i].getSeq(), mp2[0][i].getSeq());
		EXPECT_EQ (mp1[0][i].getQuality(), mp2[0][i].getQuality());
		EXPECT_EQ (mp1[0][i].getName(), mp2[0][i].getName());
	}
std::cerr<<"read pass"<<'\n';

	ParameterTraitSeat <QualityScoreType::SANGER> Qoo ("AGATCGGAAGAGCGG");//("ATCGACGT");
	SingleEndAdapterTrimmer_impl < Fastq, TUPLETYPE, QualityScoreType::SANGER > QQ (Qoo);//(adapter_seq);////("ATCGACGT");
	std::vector<int> trim_pos;

	for (auto index=0; index!=10000; ++index)
		QQ.TrimImpl (mp1[0][index], trim_pos);

	for (auto index=0; index!=10000; ++index)
		QQ.TrimImpl (mp2[0][index], trim_pos);

	for (auto i=0; i!=10000; ++i)
	{
		EXPECT_EQ (mp1[0][i].getSeq(), mp2[0][i].getSeq());
		EXPECT_EQ (mp1[0][i].getQuality(), mp2[0][i].getQuality());
		EXPECT_EQ (mp1[0][i].getName(), mp2[0][i].getName());
	}
std::cerr<<"trim pass"<<'\n';
}


