#include <iostream>
#include <cstdint>
#include <tuple>
#include <vector>
#include <fstream>
#include "boost/lexical_cast.hpp"
#include "../src/tuple_utility.hpp"
#include "../src/constant_def.hpp"
#include "../src/wrapper_tuple_utility.hpp"
#include "gtest/gtest.h"
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"

TEST(Wrapper_tuple_utility, default_constructor)
{
	uint32_t a=0;
	typedef std::tuple <uint32_t, double> MyType;
	Wrapper < MyType > wrapped;
	EXPECT_EQ (std::get<0> (wrapped.data_content), a ); ///string is not c_string
	EXPECT_EQ (std::get<1> (wrapped.data_content), 0 );
	//std::cerr<<"default done test"<<std::endl;
}

TEST(Wrapper_tuple_utility, copy_constructor_with_wrapper)
{
	uint32_t a=0;
	typedef std::tuple <uint32_t, double> MyType;
	Wrapper < MyType > wrapped;

	Wrapper < MyType > wrapped_2 ( wrapped );
	EXPECT_EQ (std::get<0> (wrapped_2.data_content), a ); ///string is not c_string
	EXPECT_EQ (std::get<1> (wrapped_2.data_content), 0 );
	//std::cerr<<"copy constructor done test"<<std::endl;
}

TEST(Wrapper_tuple_utility, constructor_with_tuple)
{
	uint32_t a1 = 1314;
	double a2 = 10.13;
	typedef std::tuple <uint32_t, double> MyType;
	MyType a = std::make_tuple (a1, a2);

	Wrapper < MyType > wrapped_2 ( a );
	EXPECT_EQ (wrapped_2.get<0>(), a1 ); ///string is not c_string
	EXPECT_EQ (wrapped_2.get<1>(), a2 );
	EXPECT_EQ (std::get<0> (wrapped_2.data_content), a1 ); ///string is not c_string
	EXPECT_EQ (std::get<1> (wrapped_2.data_content), a2 );
	//std::cerr<<"copy constructor done test"<<std::endl;
}

TEST(Wrapper_tuple_utility, assignment)
{
	uint32_t a=0;
	typedef std::tuple <uint32_t, double> MyType;
	Wrapper < MyType > wrapped_2;
	Wrapper < MyType > wrapped_3;
	wrapped_3 = wrapped_2;
	EXPECT_EQ (std::get<0> (wrapped_3.data_content), a ); ///string is not c_string
	EXPECT_EQ (std::get<1> (wrapped_3.data_content), 0 );
	//std::cerr<<"assignment done test"<<std::endl;
}

TEST(Wrapper_tuple_utility, move_assignment)
{
	typedef std::tuple <uint32_t, double> MyType;
	Wrapper < MyType > wrapped;

	uint32_t b=1230;
	Wrapper < MyType > wrapped_4;
	wrapped_4 = std::move (boost::lexical_cast < Wrapper <  MyType > > ("*1230#777.77") );
	EXPECT_EQ (std::get<0> (wrapped_4.data_content), b ); ///string is not c_string
	EXPECT_EQ (std::get<1> (wrapped_4.data_content), 777.77 );
	//std::cerr<<"move assignment done test"<<std::endl;
}

TEST(Wrapper_tuple_utility, move_constructor)
{
	uint32_t a=0;
	typedef std::tuple <uint32_t, double> MyType;
	Wrapper < MyType > wrapped;

	Wrapper < MyType > wrapped_5 (std::move (wrapped));
	EXPECT_EQ (std::get<0> (wrapped_5.data_content), a ); ///string is not c_string
	EXPECT_EQ (std::get<1> (wrapped_5.data_content), 0 );
}

TEST(Wrapper_tuple_utility, opeartor_double_smaller)
{
	typedef std::tuple <uint32_t, double> MyType;
	MyType a = std::make_tuple (1001, 33.5);
	Wrapper < MyType > wrapped (a);
//	std::cerr<<wrapped;
}

TEST(Wrapper_tuple_utility, opeartor_double_greater)
{
	typedef std::tuple <uint32_t, double> MyType;
	MyType a = std::make_tuple (1001, 33.5);
	Wrapper < MyType > wrapped (a);
//	std::cin>>wrapped;
}

TEST(Wrapper_tuple_utility, serialization)
{
    uint32_t b=1230;
	typedef std::tuple <uint32_t, double> MyType;
	auto wrapped_4 = std::move (boost::lexical_cast < Wrapper <  MyType > > ("*1230#777.77") );

	Wrapper < MyType> cc;
	std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/test.txt");//("/pokemon/oman/test_file/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & wrapped_4;
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/test.txt");//("/pokemon/oman/test_file/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & cc;
	EXPECT_EQ (std::get<0>(cc.data_content), b);
	EXPECT_EQ (std::get<1>(cc.data_content), 777.77);
	ifs.close();
};

TEST(Wrapper_vector_utility, constructor_with_tuple)
{
    std::tuple <uint32_t, double > ut1 (100000, 9.3), ut2 (22222, 1.1), ut3(33113, 105.13);
	std::vector < Wrapper < std::tuple<uint32_t, double> > > q 
		({ ut1, ut2, ut3 });
	Wrapper < std::vector < Wrapper < std::tuple <uint32_t, double> > > > Wrapper1 ( q );
	EXPECT_EQ (std::get<0>(ut1), std::get<0>(Wrapper1.data_content[0].data_content));
	EXPECT_EQ (std::get<0>(ut1), Wrapper1.get<0>().get<0>());
	EXPECT_EQ (std::get<1>(ut1), Wrapper1.get<0>().get<1>());

	EXPECT_EQ (std::get<0>(ut2), Wrapper1.get<1>().get<0>());
	EXPECT_EQ (std::get<1>(ut2), Wrapper1.get<1>().get<1>());
	EXPECT_EQ (std::get<0>(ut3), Wrapper1.get<2>().get<0>());
	EXPECT_EQ (std::get<1>(ut3), Wrapper1.get<2>().get<1>());
}

TEST(Wrapper_tuple_utility, VECTOR_Wrapper_test)
{//  test for Wrapper < 1, VECTORTYPE >
	uint32_t a=0, b=1230, c=1220, d=1210;
	typedef std::tuple <uint32_t, double> MyType;
	Wrapper < std::vector < Wrapper < MyType > > > wrapped_vector,x;
	EXPECT_EQ (wrapped_vector.data_content.size(), a);
	//std::cerr<<"default constructor done test"<<std::endl;

	x = boost::lexical_cast < Wrapper < std::vector < Wrapper < MyType > > > > ("*1230#777.65*1220#666.54*1210#555.13") ;
	wrapped_vector = x;
	EXPECT_EQ (wrapped_vector.get<0>().get<0>(), b);
	EXPECT_EQ (wrapped_vector.get<0>().get<1>(), 777.65);
	EXPECT_EQ (wrapped_vector.get<1>().get<0>(), c);
	EXPECT_EQ (wrapped_vector.get<1>().get<1>(), 666.54);
	EXPECT_EQ (wrapped_vector.get<2>().get<0>(), d);
	EXPECT_EQ (wrapped_vector.get<2>().get<1>(), 555.13);
	//std::cerr<<"assignment done test"<<std::endl;

	auto wrapped_vector2 ( wrapped_vector );
	EXPECT_EQ (wrapped_vector2.get<0>().get<0>(), b);
	EXPECT_EQ (wrapped_vector2.get<0>().get<1>(), 777.65);
	EXPECT_EQ (wrapped_vector2.get<1>().get<0>(), c);
	EXPECT_EQ (wrapped_vector2.get<1>().get<1>(), 666.54);
	EXPECT_EQ (wrapped_vector2.get<2>().get<0>(), d);
	EXPECT_EQ (wrapped_vector2.get<2>().get<1>(), 555.13);
	//std::cerr<<"copy constructor done test"<<std::endl;

	Wrapper < std::vector < Wrapper < MyType > > > wrapped_vector3;
	wrapped_vector3 = std::move (wrapped_vector);
	EXPECT_EQ (wrapped_vector3.get<0>().get<0>(), b);
	EXPECT_EQ (wrapped_vector3.get<0>().get<1>(), 777.65);
	EXPECT_EQ (wrapped_vector3.get<1>().get<0>(), c);
	EXPECT_EQ (wrapped_vector3.get<1>().get<1>(), 666.54);
	EXPECT_EQ (wrapped_vector3.get<2>().get<0>(), d);
	EXPECT_EQ (wrapped_vector3.get<2>().get<1>(), 555.13);
	//std::cerr<<"move assignment done test"<<std::endl;

	Wrapper < std::vector < Wrapper < MyType > > > wrapped_vector4 ( std::move (wrapped_vector2));
	EXPECT_EQ (wrapped_vector4.get<0>().get<0>(), b);
	EXPECT_EQ (wrapped_vector4.get<0>().get<1>(), 777.65);
	EXPECT_EQ (wrapped_vector4.get<1>().get<0>(), c);
	EXPECT_EQ (wrapped_vector4.get<1>().get<1>(), 666.54);
	EXPECT_EQ (wrapped_vector4.get<2>().get<0>(), d);
	EXPECT_EQ (wrapped_vector4.get<2>().get<1>(), 555.13);
	
	Wrapper < std::vector < Wrapper < MyType > > > cc;
	std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/test_vec.txt");//("/pokemon/oman/test_file/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & wrapped_vector4;
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/test_vec.txt");//("/pokemon/oman/test_file/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & cc;
	EXPECT_EQ (cc.get<0>().get<0>(), b);
	EXPECT_EQ (cc.get<0>().get<1>(), 777.65);
	EXPECT_EQ (cc.get<1>().get<0>(), c);
	EXPECT_EQ (cc.get<1>().get<1>(), 666.54);
	EXPECT_EQ (cc.get<2>().get<0>(), d);
	EXPECT_EQ (cc.get<2>().get<1>(), 555.13);
	ifs.close();
};

