#ifndef TEST_ALL_CPP
#define TEST_ALL_CPP
#include "tuple_utility_test.cpp"
#include "wrapper_tuple_utility_test.cpp"
#include "fasta_test.cpp"
#include "fasta_reader_impl_test.cpp"
#include "fastq_test.cpp"
#include "fastq_reader_impl_test.cpp"
#include "bed_test.cpp"
#include "bed_reader_impl_test.cpp"
#include "wig_test.cpp"
#include "wig_reader_impl_test.cpp"
#include "sam_test.cpp"
#include "sam_reader_impl_test.cpp"
#include "mpi_job_creator_test.cpp"
//#include "twobit_test.cpp"
#include "n_bit_compress_update_test.cpp"
#include "file_reader_test.cpp"
#include "encoder_test.cpp"
#include "parser_test.cpp"
//#include "mpi_pool_test.cpp"
//#include "file_reader_intf_mpi_test.cpp"
//#include "pair_end_adapter_trimmer_test.cpp"
int my_argc;
char** my_argv;

int main(int argc, char** argv) 
{
  ::testing::InitGoogleTest(&argc, argv);
  my_argc = argc;
  my_argv = argv;
  ::testing::FLAGS_gtest_death_test_style = "fast";
  return RUN_ALL_TESTS();
}

#endif
