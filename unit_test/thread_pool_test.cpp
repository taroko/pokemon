//#include "../src/thread_pool.hpp"
#include "../src/thread_pool_update.hpp"
#include <iostream>
#include "gtest/gtest.h"
#include <fstream>

template < typename T >
void adder ( T a, T b, T& c )
{
	c = a+b;
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
//	std::cerr<<a<<'\t'<<b<<'\t'<<"sum "<<c<<std::endl;

	std::ofstream QQ;
	QQ.open ( "/oman/test_file/write_add", std::ios::app );
	QQ << c;
	QQ.close();
};

template < typename T >
void multiplier ( T a, T b, T& c )
{
	c = a * b;
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
//	std::this_thread::sleep_for(std::chrono::seconds(1));
//	std::cerr<<a<<'\t'<<b<<'\t'<<"product "<<c<<std::endl;
	std::ofstream QQ;
	QQ.open ( "/oman/test_file/write_multiply", std::ios::app );
	QQ << c;
	QQ.close();
};

template < typename T >
void deducer ( T a, T b, T& c )
{
	c = a - b;
	std::this_thread::sleep_for(std::chrono::seconds(1));
//	std::cerr<<a<<'\t'<<b<<'\t'<<"deduct "<<c<<std::endl;
	std::ofstream QQ;
 	QQ.open ( "/oman/test_file/write_deduce", std::ios::app );
	QQ << c;
	QQ.close();
};

//int main (void)
TEST ( flush, test1 )
{
	double a = 3, b = 4;
	std::vector<double> c (6, -5566);

	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[0]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[1]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[2]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[3]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[4]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[5]) ) );//, std::ref(Q) );

	GlobalPool.FlushPool ();

	std::for_each ( c.begin(), c.end(),
	[] ( double& q )
	{ std::cerr<<q<< ' ';});
	std::cerr<<std::endl;

	GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[1]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[2]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (adder<double>, a, b, std::ref(c[3]) ) );//, std::ref(Q) );

	GlobalPool.FlushPool ();

	std::for_each ( c.begin(), c.end(),
	[] ( double& q )
	{ std::cerr<<q<< ' ';});
	std::cerr<<std::endl;
}

TEST ( test_any_thread_available, test1 )
{
	double a = 3, b = 4;
	std::vector<double> c (2000, -5566);
	GlobalPool.ChangePoolSize (20);

	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[0]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[1]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[2]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[3]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[4]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[5]) ) );//, std::ref(Q) );

	int i = 0;
//	while ( i < 1000 )
	{
		auto gg = GlobalPool.set_pool_index();//Test_any_thread_available();
		std::cerr<<"test pass, with future_vec_.size() "<<gg<<'\t'<<GlobalPool.future_map_.size()<<std::endl;
		GlobalPool.JobPost ( boost::bind (adder<double>, a, b, std::ref(c[i+6]) ) );//, std::ref(Q) );
		GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[i+7]) ) );//, std::ref(Q) );
		++i;
	}

	GlobalPool.FlushPool ();

	std::for_each ( c.begin(), c.end(),
	[] ( double& q )
	{ std::cerr<<q<< ' ';});
	std::cerr<<std::endl;
}

TEST ( test_any_thread_available, test2 )
{
	double a = 3, b = 4;
	std::vector<double> c (2000, -5566);
	GlobalPool.ChangePoolSize (20);

	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[0]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[1]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[2]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[3]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (multiplier<double>, a, b, std::ref(c[4]) ) );//, std::ref(Q) );
	GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[5]) ) );//, std::ref(Q) );

	int i = 0;
	while ( i < 100 )
	{
		auto gg = GlobalPool.set_pool_index();//Test_any_thread_available();
		std::cerr<<"test pass, with future_vec_.size() "<<gg<<'\t'<<GlobalPool.future_map_.size()<<std::endl;
		GlobalPool.JobPost ( boost::bind (adder<double>, a, b, std::ref(c[i+6]) ) );//, std::ref(Q) );
		GlobalPool.JobPost (boost::bind (deducer<double>, a, b, std::ref(c[i+7]) ) );//, std::ref(Q) );
		++i;
	}

	for ( auto i = GlobalPool.future_map_.begin(); i!= GlobalPool.future_map_.end(); ++ i)
		GlobalPool.single_flush (i->first);
//	GlobalPool.FlushPool ();

	std::for_each ( c.begin(), c.end(),
	[] ( double& q )
	{ std::cerr<<q<< ' ';});
	std::cerr<<std::endl;
}

