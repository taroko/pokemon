#include "../src/constant_def.hpp"
#include "../src/file_reader.hpp"
#include "../src/wrapper_tuple_utility.hpp"
#include "gtest/gtest.h"
#include <vector>
#include <map>
#include <utility>
// Mind that MPI version of fil_reader_intf is tested in file_reader_intf_test_mpi.cpp
TEST (file_reader_curl_gz_normal, read )
{
	//MT
	//----------------------------------------------------------------
	//400mb*2 
	//VIRT 2438mb RES 137mb buf x1, readx1 		// VIRT 2438mb RES 162mb toward ending
	//VIRT 2566mb RES 383mb buf x1, readx100000 // VIRT 2438mb RES 162mb toward ending
	//143sec cpu time, 385sec real time

	//10gb*2
	//VIRT 2566mb RES 300mb buf x1, readx100000 //VIRT 284mb RES 105mb toward ending
	//
	std::vector <std::string> fn ({
		"test_result/outfile_1",
		"test_result/outfile_2"
			});
	std::vector < std::ofstream* > of//; 
	({
		new std::ofstream (fn[0]),
		new std::ofstream (fn[1])
	});
//	for (auto index=0; index!=fn.size(); ++index)
//		of.push_back (&fn[index]);

	typedef std::tuple <std::string, std::string,
			std::string, std::string > TUPLETYPE;
	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;

	std::vector <std::string> vec1 	
		({
		 //"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
		 //"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
		 //"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
		 //"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
		 "http://localhost/test/400m-1.fastq.gz", "http://localhost/test/400m-1.fastq.gz"
		 //"http://140.113.15.98/test/10g-1.fastq.gz", "http://140.113.15.98/test/10g-2.fastq.gz"
		 });	 
	std::vector <uint64_t> vec2 
		({
		 //7493990, 7493990
		 //459083647, 478161401
		 444670062, 444670062//463990642 	
		 //10548716385, 10739796450 
		 });

	FileReader < ParallelTypes::M_T, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex > 
		QQ (vec1, &result, vec2);	

	size_t read_round=0;
	while (true)
	{
		bool ind = true;
		auto rrr = QQ.Read (100000);
		for ( auto ii=0; ii!=vec1.size(); ++ii)
			for ( auto jj=0; jj!=(*rrr)[ii].size(); ++jj )
				(*of[ii]) << (*rrr)[ii][jj];
		for ( auto jj=0; jj!=(*rrr)[0].size(); ++jj )
		{
			EXPECT_EQ (std::get<0>((*rrr)[0][jj].data), std::get<0>((*rrr)[1][jj].data));
			EXPECT_TRUE ((*rrr)[0].size()<=100000);
			EXPECT_TRUE ((*rrr)[0].size()>0);
			EXPECT_TRUE ((*rrr)[1].size()<=100000);
			EXPECT_TRUE ((*rrr)[1].size()>0);
		}
		std::for_each ( QQ.file_handle.begin(), QQ.file_handle.end(), [&] ( std::stringstream* Q )
				{   if ( ! Q->good() )  ind = false;   });
		if (!ind)
			break;		
	}
}

TEST (file_reader_curl_gz_normal_serial, read_combo )
{
    //MT
    //----------------------------------------------------------------
    //400mb*2 
    //VIRT 2438mb RES 137mb buf x1, readx1 		// VIRT 2438mb RES 162mb toward ending
    //VIRT 2566mb RES 383mb buf x1, readx100000 // VIRT 2438mb RES mb toward ending
    //143sec cpu time, 385sec real time

    //10gb*2
    //VIRT 2566mb RES 300mb buf x1, readx100000 //VIRT 284mb RES 105mb toward ending
    //
	std::vector <std::string> fn ({
		"test_result/outfile_combo_NM1",
		"test_result/outfile_combo_NM2"
	});
	std::vector <std::shared_ptr< std::ofstream > > of; 
	for (auto index=0; index!=fn.size(); ++index)
		of.push_back (std::make_shared<std::ofstream> (fn[index]));

	typedef std::tuple <std::string, std::string,
						std::string, std::string > TUPLETYPE;
	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;

	std::vector <std::string> vec1 	
		({
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
    //"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
    //"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
	//"https://api.basespace.illumina.com/v1pre3/files/3360154/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259",
	//"https://api.basespace.illumina.com/v1pre3/files/3360156/content?access_token=bfb36a45cfa747d88b9b5d933c7c0259"
	"http://localhost/test/400m-1.fastq.gz", "http://localhost/test/400m-1.fastq.gz"
	//"http://localhost/test/10g-1.fastq.gz", "http://localhost/test/10g-2.fastq.gz"                                                                          
		});	 
	std::vector <uint64_t> vec2 
		({
		//7493990, 7493990
        //459083647, 478161401
		444670062, 444670062//463990642 	
		//10548716385, 10739796450 
		});

	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex > 
		QQ (vec1, &result, vec2);	

	size_t read_round=0;
	while (true)
	{
		bool ind = true;
		std::map <int, std::vector< Fastq <TUPLETYPE> > >* rrr = QQ.Read_combo (
		[&result, &of] ()
		{
		for ( auto ii=0; ii!=2; ++ii)
			for ( auto jj=0; jj!=result[ii].size(); ++jj )
				(*of[ii]) <<result[ii][jj];
		}
		, 100000 );
		for ( auto jj=0; jj!=(*rrr)[0].size(); ++jj )
		{
			EXPECT_EQ (std::get<0>((*rrr)[0][jj].data), std::get<0>((*rrr)[1][jj].data));
			EXPECT_TRUE ((*rrr)[0].size()<=100000);
			EXPECT_TRUE ((*rrr)[0].size()>0);
			EXPECT_TRUE ((*rrr)[1].size()<=100000);
			EXPECT_TRUE ((*rrr)[1].size()>0);
		}
		std::for_each ( QQ.file_handle.begin(), QQ.file_handle.end(), [&] ( std::stringstream* Q )
			{   if ( ! Q->good() )  ind = false;   });
		if (!ind)
			break;		
	}
}

TEST (file_reader_curl_gz_MT, read )
{
    //MT
    //----------------------------------------------------------------
    //400mb*2 
    //VIRT 2438mb RES 137mb buf x1, readx1 		// VIRT 2438mb RES 162mb toward ending
    //VIRT 2566mb RES 383mb buf x1, readx100000 // VIRT 2438mb RES mb toward ending
    //143sec cpu time, 385sec real time

    //10gb*2
    //VIRT 2566mb RES 300mb buf x1, readx100000 //VIRT 284mb RES 105mb toward ending
    //
	std::vector <std::string> fn ({
		"test_result/outfile_MT_1",
		"test_result/outfile_MT_2"
	});
	std::vector <std::shared_ptr< std::ofstream > > of; 
	for (auto index=0; index!=fn.size(); ++index)
		of.push_back (std::make_shared<std::ofstream> (fn[index]));

	typedef std::tuple <std::string, std::string,
						std::string, std::string > TUPLETYPE;
	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;

	std::vector <std::string> vec1 	
		({
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
    //"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
    //"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
	"http://localhost/test/400m-1.fastq.gz", "http://localhost/test/400m-1.fastq.gz"
	//"http://localhost/test/10g-1.fastq.gz", "http://localhost/test/10g-2.fastq.gz"                                                                          
		});	 
	std::vector <size_t> vec2 
		({
		//7493990, 7493990
        //459083647, 478161401
		444670062, 444670062//463990642 	
		//10548716385, 10739796450 
		});

	FileReader < ParallelTypes::M_T, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex > 
		QQ (vec1, &result, vec2);	

	size_t read_round=0;
	while (true)
	{
		bool ind = true;
		std::map <int, std::vector< Fastq <TUPLETYPE> > >* rrr = QQ.Read ( 100000 );
		for ( auto jj=0; jj!=(*rrr)[0].size(); ++jj )
		{
			EXPECT_EQ (std::get<0>((*rrr)[0][jj].data), std::get<0>((*rrr)[1][jj].data));
			EXPECT_TRUE ((*rrr)[0].size()<=100000);
			EXPECT_TRUE ((*rrr)[0].size()>0);
			EXPECT_TRUE ((*rrr)[1].size()<=100000);
			EXPECT_TRUE ((*rrr)[1].size()>0);
		}
		std::for_each ( QQ.file_handle.begin(), QQ.file_handle.end(), [&] ( std::stringstream* Q )
			{   if ( ! Q->good() )  ind = false;   });
		if (!ind)
			break;		
	}
}

TEST (file_reader_curl_gz_normal_MT, read_combo )
{
    //MT
    //----------------------------------------------------------------
    //400mb*2 
    //VIRT 2438mb RES 137mb buf x1, readx1 		// VIRT 2438mb RES 162mb toward ending
    //VIRT 2566mb RES 383mb buf x1, readx100000 // VIRT 2438mb RES mb toward ending
    //143sec cpu time, 385sec real time

    //10gb*2
    //VIRT 2566mb RES 300mb buf x1, readx100000 //VIRT 284mb RES 105mb toward ending
    //
	std::vector <std::string> fn ({
		"test_result/outfile_combo_MT_1",
		"test_result/outfile_combo_MT_2"
	});
	std::vector <std::shared_ptr< std::ofstream > > of; 
	for (auto index=0; index!=fn.size(); ++index)
		of.push_back (std::make_shared<std::ofstream> (fn[index]));

	typedef std::tuple <std::string, std::string,
						std::string, std::string > TUPLETYPE;
	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;

	std::vector <std::string> vec1 	
		({
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
    //"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
    //"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
	"http://localhost/test/400m-1.fastq.gz", "http://localhost/test/400m-1.fastq.gz"                                                                          
	//"http://localhost/test/10g-1.fastq.gz", "http://localhost/test/10g-2.fastq.gz"                                                                          
		});	 
	std::vector <uint64_t> vec2 
		({
		//7493990, 7493990
        //459083647, 478161401
		444670062, 444670062//463990642 	
		//10548716385, 10739796450 
		});

	FileReader < ParallelTypes::M_T, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle_mutex > 
		QQ (vec1, &result, vec2);	

	size_t read_round=0;
	while (true)
	{
		bool ind = true;
		std::map <int, std::vector< Fastq <TUPLETYPE> > >* rrr = QQ.Read_combo (
		[&result, &of] ()
		{
		for ( auto ii=0; ii!=2; ++ii)
			for ( auto jj=0; jj!=result[ii].size(); ++jj )
				(*of[ii]) <<result[ii][jj];
		}
		, 10000 );
		for ( auto jj=0; jj!=(*rrr)[0].size(); ++jj )
		{
			EXPECT_EQ (std::get<0>((*rrr)[0][jj].data), std::get<0>((*rrr)[1][jj].data));
			EXPECT_TRUE ((*rrr)[0].size()<=100000);
			EXPECT_TRUE ((*rrr)[0].size()>0);
			EXPECT_TRUE ((*rrr)[1].size()<=100000);
			EXPECT_TRUE ((*rrr)[1].size()>0);
		}
		std::for_each ( QQ.file_handle.begin(), QQ.file_handle.end(), [&] ( std::stringstream* Q )
			{   if ( ! Q->good() )  ind = false;   });
		if (!ind)
			break;		
	}
}


/*
TEST (file_reader_intf_NORMAL, read_combo )
{
	typedef std::tuple <std::string, std::string,
						std::string, std::string > TUPLETYPE;
	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;

	std::vector <std::string> vec1 	
		({
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7",
	//"https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7"
    //"https://api.basespace.illumina.com/v1pre3/files/3360155/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427",
    //"https://api.basespace.illumina.com/v1pre3/files/3360157/content?access_token=c4b6c398dd98435cb0dd7dc8acbf1427"
		"test11",
		"test22"
		});	 
	std::vector <size_t> vec2 
		({
		//7493990, 
		//7493990
        //459083647,
        //478161401
		});

	std::vector < std::string > file_name	
		({
			"file_reader_test_combo_1",
			"file_reader_test_combo_2"
		});
//	std::ofstream qq ("file_reader_test1");
//	std::ofstream qqq ("file_reader_test2");
	ADDox < 
			ParallelTypes::NORMAL, 
			Fastq, 
			TUPLETYPE, 
			SOURCE_TYPE::IFSTREAM_TYPE//CURL_TYPE_GZ, 
			//curl_default_handle  
		 > GG (file_name, vec1, &result, vec2);

//std::cerr<<(GG.Curl_device_[0])->thread_recv_volume_<<std::endl;
//std::cerr<<(GG.Curl_device_[1])->thread_recv_volume_<<std::endl;

size_t read_round=0;
	while (true)
	{
		std::cerr<<"round #"<<read_round<<'\n';
		bool ind = true;
		for ( auto i : GG.file_handle )
			ind *= i->eof();
		if ( ind )
			break;
	  	GG ();// file_handles );
		++read_round;
	}
std::cerr<<"GG"<<std::endl;
}
*/
//
//TEST (file_reader_intf_M_T, Read_N )
//{
//	std::vector < std::string > file_names ({
//			"/oman/test_file/test1.wig",
//			"/oman/test_file/test3.wig",
//			"/oman/test_file/test2.wig"
//	});
//	std::vector<std::ifstream*> file_handles;
//	for (size_t i=0 ;i < file_names.size(); i++)
//		file_handles.push_back ( new std::ifstream( file_names[i] ) );
//	typedef  std::tuple <std::string,
//						 std::string,
//						 int,
//						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > >
//						> TUPLETYPE;
//	std::map <int, std::vector< Wig <TUPLETYPE> > > result;
//	FileReader < ParallelTypes::M_T, Wig, TUPLETYPE > GG (&result);//(3, &result) ;
//
//	auto RR = GG.Read (file_handles, 5);
//
//	std::for_each (RR->begin(), RR->end(),
//		[&] ( const std::pair <int, std::vector< Wig<TUPLETYPE> > >& Q)
//		{ 
//			std::cerr<<"Q.first"<<Q.first<<std::endl;
//			std::for_each (Q.second.begin(), Q.second.end(),
//				[&] (const Wig<TUPLETYPE>& q)
//				{ std::cerr<<q<<'\n'; });
//			std::cerr<<std::endl;});
//}
//
//TEST (file_reader_intf_M_T, Read_all )
//{
//	std::vector < std::string > file_names ({
//			"/oman/test_file/test1.wig",
//			"/oman/test_file/test3.wig",
//			"/oman/test_file/test2.wig"
//	});
//	typedef  std::tuple <std::string,
//						 std::string,
//						 int,
//						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > >
//						> TUPLETYPE;
//	std::map <int, std::vector< Wig <TUPLETYPE> > > result;
//	FileReader < ParallelTypes::M_T, Wig, TUPLETYPE > GG (&result);//(3, &result) ;
//
//	GG.Read_all (file_names);
//
////	std::for_each (result.begin(), result.end(),
////		[&] ( const std::pair <int, std::vector< Wig<TUPLETYPE> > >& Q)
////		{ 
////			std::cerr<<"Q.first"<<Q.first<<std::endl;
////			std::for_each (Q.second.begin(), Q.second.end(),
////				[&] (const Wig<TUPLETYPE>& q)
////				{ std::cerr<<q<<'\n'; });
////			std::cerr<<std::endl;});
//}
//
//struct ADDo
//{
////	typedef  std::tuple <std::string,
////						 std::string,
////						 std::string,
////						 std::string
////						> TUPLETYPE;
//
//	void func (size_t i)//size_t start, size_t end)
//	{
//		std::cerr<<" ADD "<<std::endl;//<<start<<'\t'<<end<<std::endl;
//	}
//	void operator () (void)//size_t start, size_t end)
//	{
//		std::cerr<<" ADD "<<std::endl;//<<start<<'\t'<<end<<std::endl;
//	}
//
//};
//
//template < ParallelTypes ParaType,
//		   template <typename> class FORMAT,
//		   typename TUPLETYPE >
//struct ADDox
//	: public FileReader < ParaType, FORMAT, TUPLETYPE >
//{};
//
//template < template <typename> class FORMAT,
//		   typename TUPLETYPE >
//struct ADDox < ParallelTypes::M_T, FORMAT, TUPLETYPE >
//	: public FileReader < ParallelTypes::M_T, FORMAT, TUPLETYPE >
//{
//	std::vector < std::string > out_file_;
//	ADDox ( std::map < int, std::vector< FORMAT <TUPLETYPE> > >* content , 
//			std::vector < std::string > output_file,
//			ThreadPool* tp = &GlobalPool )
//		: FileReader <ParallelTypes::M_T, FORMAT, TUPLETYPE> (content )
//		, out_file_ ( output_file )
//	{
//		this -> local_pool_.ChangePoolSize (10);
//	}
//
//	void func (size_t i)
//	{
//		this -> local_pool_.JobPost ( boost::bind (
//			&ADDox::funcx, this, i ) );
//	}
//
//	void funcx ( size_t i )
//	{
//		for ( FORMAT<TUPLETYPE> index : (*(this->result2))[i] )
//			std::get<0> (index.data)+=("$$$");
//		for ( auto index : (*(this->result2))[i] )
//			std::get<2> (index.data)+=("$$$");
//		for ( auto index : (*(this->result2))[i+1] )
//			std::get<0> (index.data)+=("%%%");
//		for ( auto index : (*(this->result2))[i+1] )
//			std::get<2> (index.data)+=("%%%");
//		write_to_file ( i );
//	}
//
//	void write_to_file (size_t i)
//	{
//		for ( auto index = 0; index != 2; ++ index )
//			this -> ptr_to_GlobalPool -> JobPost ( boost::bind (
//				&ADDox::write_to_file_impl, this, i+index ) ); 
//		this -> ptr_to_GlobalPool -> FlushPool();
//	}
//
//	void write_to_file_impl ( size_t index )
//	{
//		std::ofstream QQ;
//		QQ.open ( out_file_[index], std::ios_base::app );
//		for ( FORMAT<TUPLETYPE> itr : (*(this ->result2))[index] )
//			QQ<<itr;
//		QQ.close();
//	}
//
//	std::map< int, std::vector <FORMAT <TUPLETYPE> > >* operator () (std::vector < std::ifstream* >& read_deque)
//	{
//		return this -> Read_combo3 ( read_deque, std::bind
//			(&ADDox::func, this, std::placeholders::_1), 10000);
//	}
//
//	void operator () (void)//size_t start, size_t end)
//	{
//		std::cerr<<" ADD "<<std::endl;//<<start<<'\t'<<end<<std::endl;
//	}
//};
//
//
//TEST (file_reader_intf_M_T, Read_combo2)
//{
//	std::vector < std::string > file_names ({
//			"/oman/test_file/test1.wig",
//			"/oman/test_file/test3.wig",
//			"/oman/test_file/test2.wig"
//	});
//	std::vector<std::ifstream*> file_handles;
//	for (size_t i=0 ;i < file_names.size(); i++)
//		file_handles.push_back ( new std::ifstream( file_names[i] ) );
//	typedef  std::tuple <std::string,
//						 std::string,
//						 int,
//						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > >
//						> TUPLETYPE;
//	std::map <int, std::vector< Wig <TUPLETYPE> > > result;
//	FileReader < ParallelTypes::M_T, Wig, TUPLETYPE > GG (&result);//(3, &result) ;
//
//	ADDo add;
//
//	auto RR = GG.Read_combo2 (file_handles, add, 20 );
//
//	std::for_each (RR->begin(), RR->end(),
//		[&] ( const std::pair <int, std::vector< Wig<TUPLETYPE> > >& Q)
//		{ 
//			std::cerr<<"Q.first"<<Q.first<<std::endl;
//			std::for_each (Q.second.begin(), Q.second.end(),
//				[&] (const Wig<TUPLETYPE>& q)
//				{ std::cerr<<q<<'\n'; });
//			std::cerr<<std::endl;});
//}
//
//TEST (file_reader_intf_M_T, Read_combo3)
//{
//	std::vector < std::string > file_names ({
//		"/oman/johnny_test/QQ1.fq",
//		"/oman/johnny_test/QQ2.fq"
//	});
//	std::vector<std::ifstream*> file_handles;
//	for (size_t i=0 ;i < file_names.size(); i++)
//		file_handles.push_back ( new std::ifstream( file_names[i] ) );
//
//	typedef std::tuple <std::string, std::string,
//						std::string, std::string > TUPLETYPE;
//	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;
//
//	std::vector < std::string > out_file ({ 
//		"/oman/of0", "/oman/of1", "/oman/of2", "/oman/of3",
//		"/oman/of4", "/oman/of5", "/oman/of6", "/oman/of7",
//		"/oman/of8", "/oman/of9", "/oman/of10","/oman/of11",
//		"/oman/of12","/oman/of13","/oman/of14","/oman/of15",
//		"/oman/of16","/oman/of17","/oman/of18","/oman/of19" });
//
//	ADDox < ParallelTypes::M_T, Fastq, TUPLETYPE> QQ ( &result, out_file );
//	size_t i = 0;
//	while (true)
//	{
//		bool ind = true;
//		for ( auto i : file_handles )
//			ind *= i->eof();
//		if ( ind )
//			break;
//
//	  	QQ ( file_handles );
//	}
//	QQ.local_pool_.FlushPool();
//	QQ.ptr_to_GlobalPool->FlushPool();
//}
//
