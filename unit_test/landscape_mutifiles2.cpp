#include <fstream>
#include <deque>
#include <map>
#include <unordered_map>
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "../src/parser.hpp"
//#include "../src/bwt.hpp"
#include "gtest/gtest.h"

template <typename mapType, typename addType>
class addMap
{
public:
	// { geneType, {tail, reads_number} }
	void operator()(mapType &map, mapType value, addType test=0){
		typename mapType::iterator map_it;
		if((map_it = map.find( (*value.begin() ).first )) ==  map.end())
		{
			map.insert( { (*value.begin() ).first, (*value.begin() ).second } );
		}
		else
		{
			addMap< typename mapType::mapped_type , addType> am;
			am( (*map_it).second, (*value.begin() ).second , test);
		}	
	}
};
template <typename T>
class addMap<T,T>
{
public:
	void operator()(T &v1, T &v2, T v3){
		v1 += v2;
	}
};

int main(int argc, char** argv)
{
	if(argc < 4)
	{
		std::cout << "usage ./landscape_mutifiles scape_size overlap_number file1 file2 ..." << std::endl;
		std::cout << "for landscape(overview), count overlap with window, ..." << std::endl;
		return 0;
	}
	
	uint32_t scape_size(0), overlap_number(0);
	
	scape_size = std::stoi(argv[1]);
	overlap_number = std::stoi(argv[2]);
	
	
	std::vector <std::ifstream*> file_handles;
	for (int i=3; i < argc; ++i)
		file_handles.push_back (new std::ifstream ( argv[i] ) );
	typedef std::tuple < std::string, uint32_t, uint32_t, uint32_t > TUPLETYPE;

	FileParser < ParallelTypes::NORMAL, Bed, TUPLETYPE, CompressType::Plain, std::deque, 2 > LandScape;
	
	//chr, start, file_idx, num
	std::map< std::string ,std::map<uint32_t, std::map<int, uint32_t> > > landscape;
	addMap< std::map<std::string ,std::map<uint32_t, std::map<int, uint32_t> > >, uint32_t> addmap;
	
	//for quickly
	std::map < std::string, std::set<uint32_t> > total_scape;
	
	uint32_t min_start(3000000000),max_start(0);
	bool check_eof(true);
	do
	{
		check_eof=true;
		auto x = LandScape.Read ( file_handles );
		int file_idx=0;
		std::for_each (	x->begin(), x->end(),
			[&] (const std::pair<int, Bed<TUPLETYPE> >& Q)
			{
				if(!Q.second.eof_flag){
					check_eof=false;
					std::string chr( std::get<0>(Q.second.data) );
					uint32_t start( std::get<1>(Q.second.data) );
					uint32_t num( std::get<3>(Q.second.data) );
					addmap(landscape, {{chr, {{start/scape_size,{{file_idx, num}} }} }});
					
					//total_scape[chr].insert(start/scape_size);
					
					//max_start = std::max( (start/scape_size), max_start);
					//min_start = std::min( (start/scape_size), min_start);
				}
				++file_idx;
			}
		);
	}while(!check_eof);
	
	bool is_zero(true);
	std::stringstream tmp_string;
	std::string chr("");
	int file_num = file_handles.size();
	
	std::cout << "chr\tstart\tend";
	for (int i=3; i < argc; ++i) std::cout << "\t" << argv[i];
	
	
	std::for_each (landscape.begin(), landscape.end(),
		[&] (const std::pair<std::string, std::map<uint32_t, std::map<int, uint32_t> > > &Q)
		{
			std::string chr(Q.first);
			
			for (auto &K : Q.second)
			{
				uint32_t start(K.first*scape_size), end((K.first+overlap_number)*scape_size);
				
				std::cout << "\n" << chr << "\t" << start << "\t" << end;
				
				int i_test(0);
				for(auto &R : K.second)
				{
					uint32_t number(0);
					
					for(int tmp(0); tmp < R.first-i_test; ++tmp)
					{
						std::cout << "\t" << "0";
						++i_test;
					}
					++i_test;
					for(uint32_t i(0); i < overlap_number; ++i)
					{
						auto it = (Q.second).find(K.first+i);
						if( it != Q.second.end() )
						{
							auto it2 = (*it).second.find(R.first);
							if( it2 != (*it).second.end() )
								number += (*it2).second;
						}
					}
					std::cout << "\t" << number;
				}
				for(int tmp(0); tmp < file_num-i_test; ++tmp)
				{
					std::cout << "\t" << "0";
				}
				//std::cout << chr << "\t" << start << "\t" << end << "\t" << number << std::endl;
			}
		}
	);
	
	
}

