#include <bitset>
#include <iostream>
#include <deque>
#include <string>
#include <algorithm>
#include <fstream>

#include "cstring"
#include "boost/dynamic_bitset.hpp"
#include "boost/utility/binary.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"
#include "../src/constant_def.hpp"

#include "../src/abwt.hpp"

#include "../src/sbwt.hpp"
//#include "../src/abwt_search.hpp"
//#include "../src/abwt_table.hpp"
//#include "../src/abwt_format.hpp"
//#include "../src/compression/abit.hpp"
//#include "../src/compression/jbit.hpp"
//#include "../src/tailer.hpp" // tailer.hpp is previously called bowhan.hpp
//#include "../src/difference_cover.hpp"
//#include "../src/thread_pool.hpp"
//#include "../src/mkq_sort.hpp"
//#include "../src/bucket_sort.hpp"
#include "gtest/gtest.h"

