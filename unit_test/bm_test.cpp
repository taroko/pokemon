#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include "../src/bm.hpp"
#include "gtest/gtest.h"

TEST (bm, test1)
{
	std::string pattern ("abba"), txt ("abcdefghijklmnopqrstuvwxyzabbabababcdefghijklmnopqrstuvwxyzabbabab");	
	BMSearch qq (pattern, txt); 
//	qq.Print();
	std::vector<int> found_vec;
//	std::cerr<<"found at pos "<< qq.bmSearch(found_vec)<<std::endl;

//	std::cerr<<"all of found pattern "<<std::endl;
//	std::for_each (found_vec.begin(), found_vec.end(),
//	[] (const int& Q)
//	{std::cerr<<Q<<std::endl;});
	EXPECT_EQ ( qq.bmSearch(), 59);
}
