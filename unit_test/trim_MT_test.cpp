#include "../src/pair_end_adapter_trimmer_parameter_current_06.hpp"
#include "../src/format/json_handler.hpp"
#include <sstream>

int main (int argc, char* argv[])
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	json_handler<curl_default_handle> jh (argv[1], "test_1_0627");
	jh.print();
	//	for (auto index=0; index!=jh.url_vec_.size(); ++index)
	auto index=0;
	{
		std::vector <std::string> upload_vec0 
			({
			 jh.upload_url_vec_[index][0]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
			 ,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
			 , "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
			 , "https://api.basespace.illumina.com/"
			 });

		std::vector <std::string> upload_vec1 
			({
			 jh.upload_url_vec_[index][1]//"https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=text.txt&multipart=true",
			 ,jh.access_token_//"x-access-token: 4b2bf2c94ea34721ac01fc177111707f",
			 , "Content-Type: application/gz"//, "Content-Type: application/binary"//text"
			 , "https://api.basespace.illumina.com/"
			 });

		std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc;
		std::map<int, std::vector< size_t > > ttt;

		size_t startsize_in;
		std::stringstream ss (argv[2]);
		double m_indicator, a_mismatch, g_mismatch;
		char comma1, comma2, comma3;
		ss >> m_indicator >> comma1 >> a_mismatch >> comma2 >> g_mismatch >> comma3 >> startsize_in;

		ParameterTrait i_parameter_trait ( startsize_in );//, num_in, pool_size_in );

		PairEndAdaptiveTrimmer <ParallelTypes::M_T,//NORMAL, 
							   Fastq, 
							   TUPLETYPE,
							   SOURCE_TYPE::CURL_TYPE_GZ, 
							   curl_default_handle,   
							   TrimTrait<std::string, LinearStrMatch<double> >, 
							   double,
							   double > QQ 
								   (	jh.url_vec_[index],	jh.size_vec_[index], //file_vec, 
										upload_vec0, upload_vec1,
										&ccc, &ttt, //RQ, WQ, 
										i_parameter_trait, m_indicator, a_mismatch, g_mismatch);
		size_t sum = 0, rd_count = 0;
		int i = 0;

		while (true)//(i<2)// (true)
		{
			std::cout<<"round # "<<i<<'\n';
			if (!QQ.Parse_N_Trim_all (3))
				break;
			++i;
		}
		std::cerr<<"done PNTA # "<<index<<std::endl;
		if ( index==jh.url_vec_.size() )
			std::cerr<<"fake termination "<<'\n';
		//		QQ.terminate ("http://www.jhhlab.tw/basespace/query_task_peat/status_finish/");
	}
	std::cerr<<"done of all "<<std::endl;
}
