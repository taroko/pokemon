#include <tuple>
#include <vector>
#include <cstdint>
#include "gtest/gtest.h"
#include "../src/format_io_iterator.hpp"
#include "../src/constant_def.hpp"
#include "../src/format/bed_reader_impl.hpp"
#include "../src/tuple_utility.hpp"
#include "file_generator.hpp"
#include <fstream>
#include "boost/lexical_cast.hpp"
TEST(Bed_reader, io_end)
{
//	FileReader_impl <format_types::BED , std::tuple <std::string,uint32_t,uint32_t> > Bed_Reader_test;
	FileReader_impl < Bed, std::tuple <std::string, uint32_t, uint32_t >, SOURCE_TYPE::IFSTREAM_TYPE > Bed_Reader_test;
	EXPECT_EQ( ( (Bed_Reader_test.io_end() ) -> eof_flag ), true );
};

TEST(Bed_reader, get_next_entry)
{
	RandomFileGenerator fg;
	fg.GenRandFile< Bed<> >();
	std::ifstream file_handle;
	file_handle.open (fg.temp_file_name , std::ifstream::in);
    std::vector < std::string > qq ({ fg.temp_file_name });
	FileReader_impl < Bed, std::tuple <std::string, uint32_t, uint32_t >, SOURCE_TYPE::IFSTREAM_TYPE > Bed_Reader_test (qq);

	while (1)
	{

	Bed < std::tuple <std::string, uint32_t, uint32_t > > object;
	object = Bed_Reader_test.get_next_entry (0);//(file_handle);
    if ( object.eof_flag )
        break;
	std::cerr<<object;

	std::vector<std::string> strs;
	std::string temp_str(fg.GetContent(0) );
	boost::split(strs, temp_str, boost::is_any_of(" \t") );

	EXPECT_EQ(std::get<0> (object.data), strs[0] );
	EXPECT_EQ(std::get<1> (object.data), boost::lexical_cast<uint32_t>(strs[1]) );
	EXPECT_EQ(std::get<2> (object.data), boost::lexical_cast<uint32_t>(strs[2]) );
	}
};
/*
TEST(Bed_reader, get_all_entry)
{
	typedef std::tuple<std::string, uint32_t, uint32_t> TUPLETYPE;
	FileReader_impl < Bed, TUPLETYPE > Bed_Reader_test;
	std::string s1 ("/Users/obigbando/Documents/work/test_file/test.bed");	
	std::vector < Bed <TUPLETYPE> > result;
	Bed_Reader_test.get_all_entry ( s1, result );
	EXPECT_EQ(std::get<0> (result[0].data), "chr7" );
	EXPECT_EQ(std::get<1> (result[0].data), 115000000);
	EXPECT_EQ(std::get<2> (result[0].data), 116000000);

	EXPECT_EQ(std::get<0> (result[11].data), "chr9" );
	EXPECT_EQ(std::get<1> (result[11].data), 1300 );
	EXPECT_EQ(std::get<2> (result[11].data), 2000);
//	std::for_each ( result.begin(), 
//					result.end(), 
//					[] (const typename std::vector< Bed<TUPLETYPE> > :: value_type Q)
//					{ TupleUtility< TUPLETYPE, 3 > :: PrintTuple (std::cout, Q.data);}
//				  );
}

TEST(Bed_reader, read_bwg) 
{
	typedef std::tuple < std::string,
						uint32_t,
						uint32_t
						, double
					   > TUPLETYPE;
	char s1[] = "/Users/obigbando/Downloads/kent/src/utils/bedToBigBed/test.bb ";
	char s2[] = "/Users/obigbando/Documents/work/test_file/kent_sample_convert.bed";
//	auto y = FileReader_impl<format_types::BED, TUPLETYPE>::read_Bigbed (s1, s2);
	auto y = FileReader_impl< Bed, TUPLETYPE>::read_Bigbed (s1, s2);
	std::ifstream file_handle;
	file_handle.open (y, std::ifstream::in);
	auto x = FileReader_impl< Bed, TUPLETYPE>::get_next_entry (file_handle);
};

TEST(Bed_reader, write_bwg)
{
	typedef std::tuple < std::string,
						uint32_t,
						uint32_t
						, double
					   > TUPLETYPE;
	char s3[] = "/Users/obigbando/Documents/work/test_file/kent_sample_convert.bed";
	char s4[] = "/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/test.sizes";
	char s5[] = "/Users/obigbando/Documents/work/test_file/kent_sample_convert.bb";
//	auto z = FileReader_impl<format_types::BED, TUPLETYPE>::write_Bigbed (s3, s4, s5);
	auto z = FileReader_impl< Bed, TUPLETYPE>::write_Bigbed (s3, s4, s5);
};
*/

