#include "../src/compression/n_bit_compress_update.hpp"
#include <iostream>
#include <unordered_map>
#include <string>
#include <cstdint>
#include <deque>
#include <fstream>
#include <bitset>
#include "gtest/gtest.h"
#include "boost/lexical_cast.hpp"
#include "boost/dynamic_bitset.hpp"


//Constructors
/// @fn TEST(NBitCompress, default_constructor_Normal)
TEST(NBitCompress, default_constructor_Normal)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > gg;
	CompressRuleTrait<6, CompressType::N_BITS> qq;
	EXPECT_EQ (gg.TraitObject.alphabet, qq.alphabet);
	CompressRuleTrait<6, CompressType::N_BITS> qq2;
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > gg2;
	EXPECT_EQ (gg2.TraitObject.alphabet, qq2.alphabet);
}
/// @fn TEST(NBitCompress, default_constructor_Mask)
TEST(NBitCompress, default_constructor_Mask)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g1;
	CompressRuleTrait<6, CompressType::N_BITS_MASK> qq;
	EXPECT_EQ (g1.TraitObject.alphabet, qq.alphabet);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g12;
	CompressRuleTrait<2, CompressType::N_BITS_MASK> qq2;
	EXPECT_EQ (g12.TraitObject.alphabet, qq2.alphabet);
}
/// @fn TEST(NBitCompress, constructor_with_length_Normal)
TEST(NBitCompress, constructor_with_length_Normal)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g2 (64);
	EXPECT_EQ( g2.GetSeq().size(), 64);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > g22 (64);
	EXPECT_EQ( g22.GetSeq().size(), 64);
}
/// @fn TEST(NBitCompress, constructor_with_length_Mask)
TEST(NBitCompress, constructor_with_length_Mask)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g3 (64);
	EXPECT_EQ( g3.GetSeq().size(), 64);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g32 (64);
	EXPECT_EQ( g32.GetSeq().size(), 64);
}
/// @fn TEST(NBitCompress, constructor_with_string_Normal)
TEST(NBitCompress, constructor_with_string_Normal)
{
	std::string s1 ("ATCGATCGAbCdefGCXDWQRMM");
//	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g4 (s1); //("ATCGatcgabcdefgCXDWQRMM");
//	EXPECT_EQ (g4.MakeSeqString(), s1);
	std::string sstr, sstr6;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	g42.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g4 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	g42.MakeSeqString(sstr6);
	EXPECT_EQ (sstr6, s1);
}
/// @fn TEST(NBitCompress, constructor_with_string_Mask)
TEST(NBitCompress, constructor_with_string_Mask)
{
	//std::string s1 ("ATCGatcgaMMVVCSSFaCggTcc");
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s1, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s1+=s2;
		++i;
	}
//	std::string s1 ("gggggCccDaaccggttDDz");
	std::string sstr, sstr6;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK>	> g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	g42.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g46 (s1);
//	g46.MakeSeqString(sstr6);
//	EXPECT_EQ (sstr6, s1);
}
/// @fn TEST(NBitCompress, copy_constructor_Normal)
TEST(NBitCompress, copy_constructor_Normal)
{
	std::string s1 ("ATCGATCGAbCdefGCXDWQRMM");//("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > g52 (g42);
	EXPECT_EQ ( g42, g52);

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g4 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g5 (g4);
	EXPECT_EQ ( g4, g5);
}
/// @fn TEST(NBitCompress, copy_constructor_Mask)
TEST(NBitCompress, copy_constructor_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s1, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s1+=s2;
		++i;
	}
//	std::string s1 ("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g52 (g42);
	EXPECT_EQ ( g42, g52);
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> > g46 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g56 (g46);
	EXPECT_EQ ( g46, g56);
}
/// @fn TEST(NBitCompress, constructor_with_elements_Normal)
TEST(NBitCompress, constructor_with_elements_Normal)
{
	std::string s6 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > zz1 ( s6 );
	auto x2 = zz1.GetSeq();
	auto y2 = zz1.GetExceptDeq();
	auto z2 = zz1.GetMaskDeq();
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > g2 ( x2, y2);//, w);
	EXPECT_EQ ( zz1, g2);

	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z1 ( s6 );
	auto x = z1.GetSeq();
	auto y = z1.GetExceptDeq();
	auto z = z1.GetMaskDeq();
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g6 ( x, y);//, w);
	EXPECT_EQ ( z1, g6);
}
/// @fn TEST(NBitCompress, costructor_with_elements_Mask)
TEST(NBitCompress, costructor_with_elements_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s1, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s1+=s2;
		++i;
	}
//	std::string s6 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > zz2 ( s1 );
	auto x2 = zz2.GetSeq();
	auto y2 = zz2.GetExceptDeq();
	auto w2 = zz2.GetMaskDeq();
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g2 ( x2, y2, w2);
	EXPECT_EQ ( zz2, g2);

	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z6 ( s1 );
	auto x6 = z6.GetSeq();
	auto y6 = z6.GetExceptDeq();
	auto w6 = z6.GetMaskDeq();
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g6 ( x6, y6, w6);
	EXPECT_EQ ( z6, g6);
}
/// @fn TEST(NBitCompress, constructor_with_rvalue_elements_Normal)
TEST(NBitCompress, constructor_with_rvalue_elements_Normal)
{
	std::string s7 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > z ( s7 ), zz (s7);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > g (z.GetSeq(), z.GetExceptDeq());
	EXPECT_EQ ( zz, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z3 ( s7 ), zz3 (s7);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g7 (z3.GetSeq(), z3.GetExceptDeq());
	EXPECT_EQ ( zz3, g7);
}
/// @fn TEST(NBitCompress, constructor_with_rvalue_elements_Mask)
TEST(NBitCompress, constructor_with_rvalue_elements_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s7, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s7+=s2;
		++i;
	}
//	std::string s7 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > z ( s7 ), zz (s7);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g (z.GetSeq(), z.GetExceptDeq(), z.GetMaskDeq());
	EXPECT_EQ ( zz, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z4 ( s7 ), zz4 (s7);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g72 (z4.GetSeq(), z4.GetExceptDeq(), z4.GetMaskDeq());
	EXPECT_EQ ( zz4, g72);
}
/// @fn TEST(NBitCompress, move_constructor_Normal)
TEST(NBitCompress, move_constructor_Normal)
{
	std::string s8 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > z ( s8 ), zz (s8);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > g ( std::move(z) );
	EXPECT_EQ (zz, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z8 ( s8 ), zz8 (s8);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g8 ( std::move(z8) );
	EXPECT_EQ (zz8, g8);
}
/// @fn TEST(NBitCompress, move_constructor_Mask)
TEST(NBitCompress, move_constructor_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s8, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s8+=s2;
		++i;
	}
//	std::string s8 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > z ( s8 ), zz (s8);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g ( std::move(z) );
	EXPECT_EQ (zz, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z82 ( s8 ), zz82 (s8);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g82 ( std::move(z82) );
	EXPECT_EQ (zz82, g82);
}
/// @fn TEST(NBitCompress, Move_assignment_Normal)
TEST(NBitCompress, Move_assignment_Normal)
{
	std::string s9 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > z ( s9 ), zz (s9);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g ( std::move(z) );
	EXPECT_EQ (zz, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z9 ( s9 ), zz9 (s9);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g9;
	g9 = std::move(z9);
	EXPECT_EQ (zz9, g9);
}
/// @fn TEST(NBitCompress, Move_assignment_Mask)
TEST(NBitCompress, Move_assignment_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s9, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s9+=s2;
		++i;
	}
	//std::string s9 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > z ( s9 ), zz (s9);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g;
	g = std::move(z);
	EXPECT_EQ (zz, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z92 ( s9 ), zz92 (s9);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g92;
	g92 = std::move(z92);
	EXPECT_EQ (zz92, g92);
}
/// @fn TEST(NBitCompress, assignment_Normal)
TEST(NBitCompress, assignment_Normal)
{
	std::string s10 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > z (s10);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS> > g;
	g = z;
	EXPECT_EQ (z, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z10 (s10);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g10;
	g10 = z10;
	EXPECT_EQ (z10, g10);
}
/// @fn TEST(NBitCompress, assignment_Mask)
TEST(NBitCompress, assignment_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s10, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s10+=s2;
		++i;
	}
	//std::string s10 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > z (s10);
	NBitCompress < CompressRuleTrait<2, CompressType::N_BITS_MASK> > g; 
	g = z;
	EXPECT_EQ (z, g);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z102 (s10);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g102; 
	g102 = z102;
	EXPECT_EQ (z102, g102);
}
//Base Functions
/// @fn TEST(NBitCompress, MakeString_Normal)
TEST(NBitCompress, MakeString_Normal)
{
	std::string s11 ("dsxxkkzmnnnvbbCmxmmXZXMZMVNBM<ZVZXMATCGATCGAbCdefGCXDWQRMM");//("ATCGatcgabcdefgCXDWQRMM");
	std::string sstr;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g (s11); //("ATCGatcgabcdefgCXDWQRMM");
	g.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s11);
	EXPECT_EQ ( g.MakeNBitString(), boost::lexical_cast<std::string> (g.GetSeq()) );

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g11 (s11); //("ATCGatcgabcdefgCXDWQRMM");
	g11.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s11);
	EXPECT_EQ ( g11.MakeNBitString(), boost::lexical_cast<std::string> (g11.GetSeq()) );
}
/// @fn TEST(NBitCompress, MakeString_Mask)
TEST(NBitCompress, MakeString_Mask)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	std::string s1, s2;
	std::getline (ofs, s2);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s2) ;
		s1+=s2;
		++i;
	}
//	std::string s1 ("zzxcvcnvmzZMCNMVeworpzJFKEVJFBLKFJFOWIJIFRLKWEFLATCGATCGabcdefgCXDWQRMM");//("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > g11 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	std::string sstr;
	g11.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s1);
	EXPECT_EQ ( g11.MakeNBitString(), boost::lexical_cast<std::string> (g11.GetSeq()) );

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> > g (s1); //("ATCGatcgabcdefgCXDWQRMM");
//	auto sstr6 = g.MakeSeqString();
	std::string sstr6;
	g.MakeSeqString(sstr6);
	EXPECT_EQ ( sstr6, s1);
	EXPECT_EQ ( g.MakeNBitString(), boost::lexical_cast<std::string> (g.GetSeq()) );
}


/// @fn TEST(NBitCompress, middle_bracket_Normal)
TEST(NBitCompress, middle_bracket_Normal)
{
//	std::string s1 ("ATivxzweoolCGRREGVqqzmnblkVCCDD");
//	std::string s1 ("NINNNaNNNcgtIIACGXXTaAcCgGtT");
//	std::string s1 ("RAdGrbEFD1EqrREqeqrRFbeADAGGGGCTNAbCdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnAbCdefGNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	std::string s1 ("AAAAAAAAAACCGGGGGTTTTTJJIESSSssserBBBBjFFJFFF");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS> >  g42 ( s1 );
	for ( size_t index = 0; index!=s1.size(); ++index)
	{
		auto gg = g42[index];
		EXPECT_EQ ( gg, s1[index]);
	}
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS> >  g ( s1 );
	for ( size_t index = 0; index!=s1.size(); ++index)
	{
		auto gg = g[index];
		EXPECT_EQ ( gg, s1[index]);
	}
}
/// @fn TEST(NBitCompress, middle_bracket_Mask)
TEST(NBitCompress, middle_bracket_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chr1.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//std::cerr<<"A"<<std::endl;
//	while //(i<10000)//
//	(!ofs.eof())
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
std::cerr<<"A"<<std::endl;
//	std::string s1 ("aaaaAAaaaACCGggGGTTTTTJJIESSSssserBBBBjFFJFFFaccgt");
	std::string s1 ("ATivxzweoolCGRREGVqqzmnblkVCCAAggTTTcccCCGattaGDgggggCcDaaccggttDDz");
//	std::string s1 ("AaaCcccggattcNgAGgTtttaNNaNNaANNGGgNgNSnA");//"RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijqaaaaacccccgggttaccggattaccgatJDERJREJREKBBIIaccJEREktERERttagjtttioIMiIMIgg");
//	std::string s1 ("ATCGNnnNaNNNttNctgnNaatAAACCGGTT");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> >  z ( s1 );
std::cerr<<"A"<<std::endl;
//	for (auto i=0; i!=10; ++i)
//	{
//std::cerr<<"B"<<std::endl;
	for ( size_t index = 0; index!=s1.size(); ++index)
//		z[index];
//	}
		EXPECT_EQ ( z[index], s1[index]);
//	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> >  z6 ( s1 );
//	for ( auto index = 0; index!=s1.size(); ++index)
//	{
//		EXPECT_EQ ( z6[index], s1[index]);
//	}
}

/// @fn TEST(NBitCompress, Substr_Normal)
TEST(NBitCompress, Substr_Normal)
{
	std::string s11 ("ABBTERCFDWERGATCGAbCdefGCXDWQRMMMTTDJDFBBQQ");
	std::string sstr, sstr6;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g11 (s11); //("ATCGatcgabcdefgCXDWQRMM");
	g11.GetSubStr(sstr, 2, 33);
	EXPECT_EQ (s11.substr(2,33), sstr);//g11.GetSubStr(5, 33));

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g16 (s11);
	g16.GetSubStr(sstr6, 2, 33);
	EXPECT_EQ (s11.substr(2, 33), sstr6);

}
/// @fn TEST(NBitCompress, Substr_Mask)
TEST(NBitCompress, Substr_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
//	std::string s1 ("AaaCcccggattcNgAGgTtttaNNaNNaANNGGgNgNSnA");
	std::string s1 ("aabBKkabcdgeAAAATTTCeCCGGGNNNNfNNNNAAAAAgAATTTaaTCcCCGGGggGGGGttGGaaGGCGATCGAbCdefabGCcXDgWQwtozsqDerqGKEROvdskojRMvcbzaeAfMACCFGGA");
	std::string sstr, sstr6;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > g11 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	auto kk = s1.size();
	g11.GetSubStr (sstr, kk>>1, (kk>>1)-5);
	EXPECT_EQ (s1.substr(kk>>1, (kk>>1)-5), sstr);//g11.GetSubStr(3,23));

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> > g16 (s1);
	g16.GetSubStr (sstr6, kk>>1, (kk>>1)-5);
	EXPECT_EQ (s1.substr(kk>>1, (kk>>1)-5), sstr6);//g11.GetSubStr(3,23));
}
/// @fn TEST(NBitCompress, GetSize)
TEST(NBitCompress, GetSize)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
//	std::string s1 ("abcde");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > z ( s1 );
	EXPECT_EQ ( z.GetSize(), s1.size() * 6);//s1.size()<<1 );
}
TEST(NBitCompress, Printer)
{
	std::string s2 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");
//  	std::ifstream ofs ("/Users/obigbando/Downloads/chr1.fa");
//	std::string s1, s2;
//	std::getline (ofs, s1);
//	for (auto i=0; i!=100000; ++i)
//	{
//		std::getline (ofs, s1);
//		s2+=s1;
//	}
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> > z ( s2 );
}
#include "../src/file_reader_impl.hpp"
TEST(NBitCompress, serialize_NBITS)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
//	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > z (std::get<1>(x.data)), z2("");

	std::string s2 ("RAdGrbEFD1EqrREqeqrRFbeADAGGGGCTNAbCdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > z (s2), z2("");

	std::ofstream out_file ("/Users/obigbando/Downloads/nbit_test.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	arc & z;
	out_file.close();

	std::ifstream in_file ("/Users/obigbando/Downloads/nbit_test.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	arci & z2;

	EXPECT_EQ (z, z2);
//	EXPECT_EQ (z2.MakeSeqString(), std::get<1>(x.data));
	EXPECT_EQ (z2.MakeSeqString(), s2);
}
TEST(NBitCompress, serialize_MASK1)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > z (std::get<1>(x.data)), z2;
	std::string s1, s2;
	std::getline (ofs, s1);
	int i =0;
	while (i<10000)
	{
		std::getline (ofs,s1) ;
		s2+=s1;
		++i;
	}
//	std::string s2 ("ACGTacgtNNacgtAacgtNNRadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");
//	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > z (s2), z2("");

	std::ofstream out_file ("/Users/obigbando/Downloads/nbitmask_test.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	arc & z;
	out_file.close();

	std::ifstream in_file ("/Users/obigbando/Downloads/nbitmask_test.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	arci & z2;

	EXPECT_EQ (z, z2);
//	EXPECT_EQ (z2.MakeSeqString(), s2);
	EXPECT_EQ (z.MakeSeqString(), std::get<1>(x.data));
	in_file.close();
}
// Complex functions
// @fn TEST(NBitCompress, reverse_Normal)
TEST(NBitCompress, reverse_Normal)
{
	std::string s12 ("ATCGATCGAbCdefGCXDWQRMM");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g (s12); //("ATCGatcgabcdefgCXDWQRMM");
	g.reverse();
	std::reverse (s12.begin(), s12.end() );
	std::string sstr2;
	g.MakeSeqString(sstr2);
	EXPECT_EQ ( sstr2, s12);

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g12 (s12); //("ATCGatcgabcdefgCXDWQRMM");
	g12.reverse();
	std::reverse (s12.begin(), s12.end() );
	std::string sstr;
	g12.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s12);
}
/// @fn TEST(NBitCompress, reverse_Mask)
TEST(NBitCompress, reverse_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
//	std::string s1 ("ATCGRREGVVCCDDabc");
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress< CompressRuleTrait <2, CompressType::N_BITS_MASK> > g ( s1 );
	g.reverse();
	std::reverse (s1.begin(), s1.end() );
	std::string sstr2;
	g.MakeSeqString(sstr2);
	EXPECT_EQ (sstr2, s1);

	NBitCompress< CompressRuleTrait <6, CompressType::N_BITS_MASK> > g12 ( s1 );
	g12.reverse();
	std::reverse (s1.begin(), s1.end() );
	std::string sstr;
	g12.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);
}
/// @fn TEST(NBitCompress, complement_Normal)
TEST(NBitCompress, complement_Normal)
{
//	std::string s12 ("ATCGatcgabcdefgCXDWQRMM");
	std::string s12 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g (s12), z2 (s12); 
	g.complement();
	for (size_t i = 0; i != s12.size()*g.TraitObject.bit_count; ++i)
	{
		EXPECT_EQ (g.GetSeq()[i], (z2.GetSeq()[i] ^= 1) );
	}

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g12 (s12), z (s12); 
	g12.complement();
	for (size_t i = 0; i != s12.size()*g12.TraitObject.bit_count; ++i)
	{
		EXPECT_EQ (g12.GetSeq()[i], (z.GetSeq()[i] ^= 1) );
	}
}
/// @fn TEST(NBitCompress, complement_Mask)
TEST(NBitCompress, complement_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s12, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s12+=s2;
//		++i;
//	}
	std::string s12 ("ATCGatcgabcdefgCXDWQRMM");

	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK>	> g (s12), z2 (s12);
	g.complement();
	for (size_t i = 0; i != s12.size()*g.TraitObject.bit_count; ++i)
	{
		EXPECT_EQ (g.GetSeq()[i], (z2.GetSeq()[i] ^= 1) );
	}

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g122 (s12), z (s12);
	g122.complement();
	for (size_t i = 0; i != s12.size()*g122.TraitObject.bit_count; ++i)
	{
		EXPECT_EQ (g122.GetSeq()[i], (z.GetSeq()[i] ^= 1) );
	}
}
/// @fn TEST(NBitCompress, reverse_complement_Normal)
TEST(NBitCompress, reverse_complement_Normal)
{
	std::string s1 ("ANewrAJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");
//	std::string s1 ("ACGT");//AJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g (s1), z2 (s1); 
	g.reverse_complement();
	auto l = s1.size() * g.TraitObject.bit_count;	//i.e. non-type template parameter N of CompressRuleTrait
	size_t index = 0;
	while (index < l)
	{
		EXPECT_EQ (g.GetSeq()[index], (z2.GetSeq()[l-index-g.TraitObject.bit_count] ^=1));
		index+=g.TraitObject.bit_count;
	}

	std::string s16 ("ACGTAJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g12 (s16), z (s16); 
	g12.reverse_complement();
	auto l6 = s16.size() * g12.TraitObject.bit_count;	//i.e. non-type template parameter N of CompressRuleTrait
	size_t index6 = 0;
	while (index6 < l6)
	{
		EXPECT_EQ (g12.GetSeq()[index6], (z.GetSeq()[l6-index6-g12.TraitObject.bit_count] ^=1));
		index6+=g12.TraitObject.bit_count;
	}
}
/// @fn TEST(NBitCompress, reverse_complement_Mask)
TEST(NBitCompress, reverse_complement_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
	std::string s1 ("aNewrAJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");

	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK>	> g (s1), z2 (s1); 
	g.reverse_complement();
	auto l = s1.size() * g.TraitObject.bit_count;
	size_t index = 0;
	while (index < l)
	{
		EXPECT_EQ (g.GetSeq()[index], (z2.GetSeq()[l-index-g.TraitObject.bit_count] ^=1));
		index+=g.TraitObject.bit_count;
	}

	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g12 (s1), z (s1); 
	g12.reverse_complement();
	auto ll = s1.size() * g12.TraitObject.bit_count;
	size_t iindex = 0;
	while (iindex < l)
	{
		EXPECT_EQ (g12.GetSeq()[iindex], (z.GetSeq()[ll-iindex-g12.TraitObject.bit_count] ^=1));
		iindex+=g12.TraitObject.bit_count;
	}
};
/// @fn TEST(NBitCompress, reverse_copy_Normal)
TEST(NBitCompress, reverse_copy_Normal)
{
	std::string s1 ("NATCGNAbCGJIOEROTGHDSDFHJIERWEYITYAJREbvxzlpWEOIJGGCC");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS>	> c2 ( s1 );
	auto gg2 = c2.reverse_copy();
	std::string str2;
	gg2.MakeSeqString(str2);
	for (size_t i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str2[i], s1[(s1.size()-i-1)]);

	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS>	> c ( s1 );
	auto gg = c.reverse_copy();
	std::string str3;
	gg.MakeSeqString(str3);
	for (size_t i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}   
/// @fn TEST(NBitCompress, reverse_copy_Mask)
TEST(NBitCompress, reverse_copy_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
	std::string s1 ("NATCGNAbCGJIOEROacgjqqeTbjfqerGHDSDFHJIERWEYITYAJREbvxzlpWEOIJGGCC");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> > c2 ( s1 );
	auto gg2 = c2.reverse_copy();
	std::string str2;
	gg2.MakeSeqString(str2);
	for (size_t i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str2[i], s1[(s1.size()-i-1)]);

	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > c ( s1 );
	auto gg = c.reverse_copy();
	std::string str3;
	gg.MakeSeqString(str3);
	for (size_t i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}   
/// @fn TEST(NBitCompress, complement_copy_Normal)
TEST(NBitCompress, complement_copy_Normal)
{   
	std::string s1 ("NATCGNAbCdQWWQQVVXMCDJFQHGzvbAAGGGTTT");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS> > c2 ( s1 ), cc2 (s1);
	auto l = s1.size()*c2.TraitObject.bit_count;
	auto gg2 = c2.complement_copy();
	for (size_t i = 0; i != l; ++i)
	{
		EXPECT_EQ (c2.GetSeq()[i], (gg2.GetSeq()[i] ^= 1) );
	}   
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS> > c ( s1 ), cc (s1);
	auto ll = s1.size()*c.TraitObject.bit_count;
	auto gg = c.complement_copy();
	for (size_t i = 0; i != ll; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (gg.GetSeq()[i] ^= 1) );
	}   
}	   
/// @fn TEST(NBitCompress, complement_copy_Mask)
TEST(NBitCompress, complement_copy_Mask)
{   
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
	std::string s1 ("NATCGNabcdQWWQQVVXMCDJFQHGzvbAAGGGTTT");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> > c2 ( s1 ), cc2 (s1);
	auto ll = s1.size()*c2.TraitObject.bit_count;
	auto gg2 = c2.complement_copy();
	for (size_t i = 0; i != ll; ++i)
	{
		EXPECT_EQ (c2.GetSeq()[i], (gg2.GetSeq()[i] ^= 1) );
	}   
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > c ( s1 ), cc (s1);
	auto l = s1.size()*c.TraitObject.bit_count;
	auto gg = c.complement_copy();
	for (size_t i = 0; i != l; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (gg.GetSeq()[i] ^= 1) );
	}   
}	   
/// @fn TEST(NBitCompress, reverse_complement_copy_Normal)
TEST(NBitCompress, reverse_complement_copy_Normal)
{
	std::string s1 ("NAaCabGcTfNNgqNqAeCrGTTqGeCAqNebGqNANGNJNCVOJEOICNWEFIJOINBJFTNNNA");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS>	> c2( s1 ), cc2 (s1);
	auto ll = s1.size()* c2.TraitObject.bit_count;
	auto gg2 = c2.reverse_complement_copy();
	size_t iindex = 0;
	while (iindex < ll)
	{
		EXPECT_EQ ( gg2.GetSeq()[iindex], (cc2.GetSeq()[ll-iindex-c2.TraitObject.bit_count] ^= 1) );
		iindex += c2.TraitObject.bit_count;
	}

	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS>	> c ( s1 ), cc (s1);
	auto l = s1.size()* c.TraitObject.bit_count;
	auto gg = c.reverse_complement_copy();
	size_t index = 0;
	while (index < l)
	{
		EXPECT_EQ ( gg.GetSeq()[index], (cc.GetSeq()[l-index-c.TraitObject.bit_count] ^= 1) );
		index += c.TraitObject.bit_count;
	}
}
/// @fn TEST(NBitCompress, reverse_complement_copy_Mask)
TEST(NBitCompress, reverse_complement_copy_Mask)
{
//	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
//	std::string s1, s2;
//	std::getline (ofs, s2);
//	int i =0;
//	while (i<10000)
//	{
//		std::getline (ofs,s2) ;
//		s1+=s2;
//		++i;
//	}
	std::string s1 ("NAaCabGcTfNNgqNqAeCrGTTqGeCAqNebGqNANGNJNCVOJEOICNWEFIJOINBJFTNNNA");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> > c2 ( s1 ), cc2 (s1);
	auto l2 = s1.size()* c2.TraitObject.bit_count;
	auto gg2 = c2.reverse_complement_copy();
	size_t index2 = 0;
	while (index2 < l2)
	{
		EXPECT_EQ ( gg2.GetSeq()[index2], (cc2.GetSeq()[l2-index2-c2.TraitObject.bit_count] ^= 1) );
		index2 += c2.TraitObject.bit_count;
	}
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > c ( s1 ), cc (s1);
	auto l = s1.size()* c.TraitObject.bit_count;
	auto gg = c.reverse_complement_copy();
	size_t index = 0;
	while (index < l)
	{
		EXPECT_EQ ( gg.GetSeq()[index], (cc.GetSeq()[l-index-c.TraitObject.bit_count] ^= 1) );
		index += c.TraitObject.bit_count;
	}
}

/*
TEST(NBitCompress, append)
{
	std::string s1 ("ACGNACGN");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > NB1 (s1);
//	std::cerr<<"NB1's sequence info, size, content "<<NB1.GetSize()<<'\t'<<NB1.GetSeq()<<std::endl;
	std::string s2 ("TANNCGT");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > NB2 (s2); 
//	std::cerr<<"NB2's sequence info, size, content "<<NB2.GetSize()<<'\t'<<NB2.GetSeq()<<std::endl;
	NB1.append (NB2);
	EXPECT_EQ (NB1.MakeSeqString(), s1+s2);
}

TEST(NBitCompress, append_Mask)
{
	std::string s1 ("ACGNACGN");
	std::string s2 ("TacgaacttG");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB3 (s1);
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB4 (s2); 
	NB3.append (NB4);
	EXPECT_EQ (NB3.MakeSeqString(), s1+s2);
}

TEST(NBitCompress, opeartor_IsSmallerThan)
{
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > NB1 ("CTCGANNNNNNNNNNNN");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > NB2 ("CTCGANNNNNNNNNNNA");
//	auto i = NB1.IsSmallerThan(NB2);
//	auto i = NB1(NB2);
	EXPECT_EQ (NB1(NB2), false);
	EXPECT_EQ (NB2(NB1), true);
}

TEST(NBitCompress, opeartor_IsSmallerThan_MASK)
{
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB1 ("CTCGANNNNNNNNNNNN");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB2 ("CTCGANNNNNNNNNNNA");
//	auto i = NB1.IsSmallerThan(NB2);
//	auto i = NB1(NB2);
	EXPECT_EQ (NB1(NB2), false);
	EXPECT_EQ (NB2(NB1), true);
}

TEST(NBitCompress, GetSubNBitCompress_Normal)
{
	std::string s1 ("AACCGGTTNNNA");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > NB1(s1), NB2;
	for (auto i=0; i!=s1.size(); ++i)
	{
		NB1.GetSubNBitCompress (i, NB2);
		EXPECT_EQ (NB2.MakeSeqString(), s1.substr(i,s1.size()-i));
	}
}

TEST(NBitCompress, GetSubNBitCompress_Mask)
{
	std::string s1 ("AaCcGgTtNNNabcAATTsxxkkzmnnnvbbCmxmmXZXMZMVNBM<ZVZXMATCGATCGAbCdefGCXDWQRMM");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB1 (s1), NB2;
	for (auto i=0; i!=s1.size(); ++i)
	{
		NB1.GetSubNBitCompress (i, NB2);
		EXPECT_EQ (NB2.MakeSeqString(), s1.substr(i,s1.size()-i));
	}
}


TEST(NBitCompress, opeartor_smaller_MASK)
{
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB1 ("ATCGA");
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > NB2 ("ATCGC");
	auto i = NB1<NB2;
	std::cerr<<"whether NB1 < NB2 "<<i<<std::endl;
}
*/
