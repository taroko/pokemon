#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "../src/adaptive_trimmer.hpp"
TEST (trimmer, linear)
{
    std::string sadapt ("xyz"), txt ("abcdefghijklmnopqrstuvwxyz"), result;
    AdaptiveTrimmer < TrimTrait<0, 3> >  q (sadapt, txt);
    q.Trim(result);
    std::cerr<<result<<std::endl;
	EXPECT_EQ (result, "abcdefghijklmnopqrstuvw");
}

TEST (trimmer, BM)
{
    std::string sadapt ("abcd"), txt ("abcdefghijklmnopqrstuvwxyz"), result;
    AdaptiveTrimmer < TrimTrait<2, 2>,BM >  q (sadapt, txt);
    q.Trim(result);
    std::cerr<<result<<std::endl;
	EXPECT_EQ (result, "ab");
}
