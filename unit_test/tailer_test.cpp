#include <vector>

#include "../src/constant_def.hpp"
#include "../src/aligner/aligner.hpp"
#include "gtest/gtest.h"

/*
TEST (indexer, test_BWT_build)
{
					
	//std::vector<std::string> filelist {"/mnt/godzilla/GENOME/db/mm9/mm9.fa"};
	//std::vector<std::string> filelist {"/mnt/godzilla/GENOME/db/hg18/chrX.fa"};
	//Aligner< Aligner_trait<> > aligner;
	//aligner.build(filelist, "/mnt/godzilla/BOWTIE_INDEX/tailer/hg18_chrX");
}
*/


TEST (searcher, test_BWT_search)
{
	std::cerr << "Loading index..." << std::endl;
	
	/// @brief 建構 Aligner
	Aligner< Aligner_trait<> > aligner;	
	
	/// @brief 讀取 index
	aligner.load_table("/mnt/godzilla/BOWTIE_INDEX/tailer/hg19/hg19");
	
	/// @brief 初始化，input and output 容器
	std::map<int, std::vector<Fastq<std::tuple<std::string,std::string,std::string,std::string>> > > datas;
	std::vector< Sam<> > result;
	
	/// @brief 模擬 pipeline 需求，一次 N 筆 
	int datas_size_limit = 10000;
	
	/// @brief fastq file list
	std::vector<std::string> filelist_fq {"/home/jones/sagittarius/clasher/clasher_input_test.fastq"};
	
	std::cerr << "Prepare file parser ..." << std::endl;
	
	/// @brief 建構 file parser
	typedef FileReader_impl
	< Fastq 
	 ,std::tuple <std::string, std::string, std::string, std::string>
	 ,SOURCE_TYPE::IFSTREAM_TYPE
	> QueryParserType;
	QueryParserType file_parser(filelist_fq);
	
	std::cerr << "Preparing start run..." << std::endl;
	
	/// @brief 模擬 pipeline 行為
	bool last = false;
	for(auto file_idx(0); file_idx != file_parser.file_num_; file_idx++)
	{
		while(true)
		{
			datas[0].emplace_back( std::move (file_parser.get_next_entry (file_idx) ) );
			if ( datas[0].back().eof_flag )
			{
				//remove last nothing fastq
				datas[0].pop_back();	
				// this file is eof, change to next file
				last = true;
			}
			std::cout << "data size " << datas[0].size() << std::endl;
			if(datas[0].size() == datas_size_limit || last)
			{
				/// @brief aligner pipeline 的實際使用方法，input (map<int, std::vector<Format> >) ,output (vector<sam>) 
				aligner.search(datas, result, 1, 18, 1000);
				datas[0].clear();
			}
			if(last)
			{
				break;
			}
		}
	}
	for(auto &i : result)
	{
		std::cout << i;
	}
	std::cerr << "size: " << result.size() << std::endl;
	
}

