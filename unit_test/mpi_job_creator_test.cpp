#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/ref.hpp"
#include "boost/lexical_cast.hpp"
#include "../src/tuple_utility.hpp"
#include "../src/mpi_job_creator.hpp"
#include <vector>
#include <tuple>
#include <fstream>
#include <string>
#include <iostream>
#include <functional>
#include "gtest/gtest.h"

//test functions & functiors
int add(int a, int b, int& c)
{
	c = a+b;
	return 15;
}

void add_void(int a, int b, int& c)
{
	c = a+b;
}

class ADD
{
public:
	int operator() ( int& a, int& b, int& c )
	{
		c = a+b;
		return 51;
	}
};

class ADD_void
{
public:
	void operator() ( int& a, int& b, int& c )
	{
		c = a+b;
	}
};

//Simple TEST embodiments
TEST (MPI_JOB_CREATOR, JobSerializer_function_RETURN)
{
	int a =1, b=2, c = -3;//, r=-1111;
//	using namespace std::placeholders;
	auto w = JobSerializer<int>::create(bind(&add, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) , 
										boost::ref(a), boost::ref(b), std::ref(c) );
	EXPECT_EQ ( std::get<0>(w.p_), 1 );
	EXPECT_EQ ( std::get<1>(w.p_), 2 );
	EXPECT_EQ ( std::get<2>(w.p_), -3);
	w();
	EXPECT_EQ ( std::get<0>(w.p_), 1 );
	EXPECT_EQ ( std::get<1>(w.p_), 2 );
	EXPECT_EQ ( std::get<2>(w.p_), 3 );
	EXPECT_EQ ( w.r_, 15);

	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & w;
//	w.serialize (arc0);
//	w.serialize (arc0, &w);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(w.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(w.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(w.p_)) ) == std::string::npos) , false);
	ifs.close();

}

TEST (MPI_JOB_CREATOR, JobSerializer_function_RETURN2)
{
	int a1 = 11, b1 = 12, c1 = -13;//, r1 =-1111;
	auto w1 = JobSerializer<int>::create(bind(&add, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) , 
										a1, std::ref(b1), boost::ref(c1) );
	EXPECT_EQ ( std::get<0>(w1.p_), 11 );
	EXPECT_EQ ( std::get<1>(w1.p_), 12 );
	EXPECT_EQ ( std::get<2>(w1.p_), -13 );
	w1();
	EXPECT_EQ ( std::get<0>(w1.p_), 11 );
	EXPECT_EQ ( std::get<1>(w1.p_), 12 );
	EXPECT_EQ ( std::get<2>(w1.p_), 23 );
	EXPECT_EQ ( w1.r_, 15);
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & w1;
//	w1.serialize (arc0);
//	w1.serialize (arc0, &w1);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(w1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(w1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(w1.p_)) ) == std::string::npos) , false);
	ifs.close();
}

TEST (MPI_JOB_CREATOR, JobSerializer_function_RETURN3)
{
	int a2 =21, b2 =22, c2 = -23;//, r2=-1111;
	auto w2 = JobSerializer<int>::create(bind(&add, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) , 
										a2, b2, boost::ref(c2) );
	EXPECT_EQ ( std::get<0>(w2.p_), 21 );
	EXPECT_EQ ( std::get<1>(w2.p_), 22 );
	EXPECT_EQ ( std::get<2>(w2.p_), -23 );
	w2();
	EXPECT_EQ ( std::get<0>(w2.p_), 21 );
	EXPECT_EQ ( std::get<1>(w2.p_), 22 );
	EXPECT_EQ ( std::get<2>(w2.p_), 43 );
	EXPECT_EQ ( w2.r_, 15);
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & w2;
//	w2.serialize (arc0);
//	w2.serialize (arc0, &w2);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(w2.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(w2.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(w2.p_)) ) == std::string::npos) , false);
	ifs.close();
}

TEST (MPI_JOB_CREATOR, JobSerializer_function_RETURN4)
{
	int a3 =31, b3 =32, c3 = -33;//, r3=-1111;
	auto w3 = JobSerializer<int>::create(bind(&add, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) , 
										a3, b3, c3 );
	EXPECT_EQ ( std::get<0>(w3.p_), 31 );
	EXPECT_EQ ( std::get<1>(w3.p_), 32 );
	EXPECT_EQ ( std::get<2>(w3.p_), -33 );
	w3();
	EXPECT_EQ ( std::get<0>(w3.p_), 31 );
	EXPECT_EQ ( std::get<1>(w3.p_), 32 );
	EXPECT_EQ ( std::get<2>(w3.p_), -33 );
	EXPECT_EQ ( w3.r_, 15);
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & w3;
//	w3.serialize (arc0);
//	w3.serialize (arc0, &w3);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(w3.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(w3.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(w3.p_)) ) == std::string::npos) , false);
	ifs.close();
}

TEST (MPI_JOB_CREATOR, job_Serializer_function_VOID)
{
	int A0 = 11;
//	int B0 = 12;
	int C0 = -100;
	auto W0 = JobSerializer<void>::create (
			bind(
				&add_void, 
				std::placeholders::_1, 
				std::placeholders::_2, 
				std::placeholders::_3
				), 
			A0, 
			12, 
			boost::ref(C0) 
				);
	EXPECT_EQ ( std::get<0>(W0.p_), 11 );
	EXPECT_EQ ( std::get<1>(W0.p_), 12 );
	EXPECT_EQ ( std::get<2>(W0.p_), -100 );
	W0();
	EXPECT_EQ ( std::get<0>(W0.p_), 11 );
	EXPECT_EQ ( std::get<1>(W0.p_), 12 );
	EXPECT_EQ ( std::get<2>(W0.p_), 23 );
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & W0;
//	W0.serialize (arc0);
//	W0.serialize (arc0, &W0);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(W0.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(W0.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(W0.p_)) ) == std::string::npos) , false);
	ifs.close();
}

TEST (MPI_JOB_CREATOR, job_Serializer_function_VOID2)
{
	int A1 = 21, B1 = 22, C1 = -200;
	auto W1 = JobSerializer<void>::create (
			bind (
					&add_void, 
					std::placeholders::_1, 
					std::placeholders::_2, 
					std::placeholders::_3
					), 
			A1, 
			B1, 
			C1 
					);
	EXPECT_EQ ( std::get<0>(W1.p_), 21 );
	EXPECT_EQ ( std::get<1>(W1.p_), 22 );
	EXPECT_EQ ( std::get<2>(W1.p_), -200 );
	W1();
	EXPECT_EQ ( std::get<0>(W1.p_), 21 );
	EXPECT_EQ ( std::get<1>(W1.p_), 22 );
	EXPECT_EQ ( std::get<2>(W1.p_), -200 );
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & W1;
//	W1.serialize (arc0);
//	W1.serialize (arc0, &W1);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(W1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(W1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(W1.p_)) ) == std::string::npos) , false);
	ifs.close();
}

TEST (MPI_JOB_CREATOR, job_Serializer_functor_RETURN)
{
	int a0 = 1, b0 = 2, c0 = -10;
	ADD q;
	auto w1 = JobSerializer<char>::create( q, std::ref(a0), boost::ref(b0), boost::ref(c0) );
	EXPECT_EQ ( std::get<0>(w1.p_), 1 );
	EXPECT_EQ ( std::get<1>(w1.p_), 2 );
	EXPECT_EQ ( std::get<2>(w1.p_), -10 );
	w1();
	EXPECT_EQ ( std::get<0>(w1.p_), 1 );
	EXPECT_EQ ( std::get<1>(w1.p_), 2 );
	EXPECT_EQ ( std::get<2>(w1.p_), 3 );
	EXPECT_EQ ( w1.r_, 51); 

	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & w1;
//	w1.serialize (arc0);
//	w1.serialize (arc0, &w1);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(w1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(w1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(w1.p_)) ) == std::string::npos) , false);
	ifs.close();
}

TEST (MPI_JOB_CREATOR, job_Serializer_functor_VOID)
{
	int a0 = 1, b0 = 2, c0 = -10;
	ADD_void q;
	auto w1 = JobSerializer<void>::create( q, std::ref(a0), boost::ref(b0), boost::ref(c0) );
	EXPECT_EQ ( std::get<0>(w1.p_), 1 );
	EXPECT_EQ ( std::get<1>(w1.p_), 2 );
	EXPECT_EQ ( std::get<2>(w1.p_), -10 );
	w1();
	EXPECT_EQ ( std::get<0>(w1.p_), 1 );
	EXPECT_EQ ( std::get<1>(w1.p_), 2 );
	EXPECT_EQ ( std::get<2>(w1.p_), 3 );
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & w1;
//	w1.serialize (arc0);
//	w1.serialize (arc0, &w1);
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	std::getline (ifs, str_temp);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<0>(w1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<1>(w1.p_)) ) == std::string::npos) , false);
	EXPECT_EQ ( (str_temp.find (boost::lexical_cast<std::string>(std::get<2>(w1.p_)) ) == std::string::npos) , false);
	ifs.close();
}

//FileReader_impl TEST embodiments
#include "../src/file_reader_impl.hpp"
TEST (mpi_job_creator, fasta_get_all_entry_function)
{
	std::string s1 ("/Users/obigbando/Documents/work/test_file/test.fasta");
	std::vector < Fasta< std::tuple<std::string, std::string> > > result, result_unpack;
	auto w = JobSerializer<void>
			:: create ( std::bind (&FileReader_impl<Fasta, std::tuple<std::string, std::string> > 
				:: get_all_entry, std::placeholders::_1, std::placeholders::_2), std::ref(s1), std::ref(result) );
	w();
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt"); 
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & result;
/*	std::for_each ( result.begin(),
					result.end(),
					[&] (const Fasta<std::tuple<std::string, std::string> >& Q)
					{ arc0 & Q;}
				);*/
	ofs.close();
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & result_unpack;
	
	for (size_t j = 0; j!=result_unpack.size(); ++j )
	{
		EXPECT_EQ ( std::get<0>(result_unpack[j].data), std::get<0>(result[j].data) );
		EXPECT_EQ ( std::get<1>(result_unpack[j].data), std::get<1>(result[j].data) );
	}
	ifs.close();
};

TEST (mpi_job_creator, fastq_get_all_entry_function)
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	std::string s1 ("/Users/obigbando/Documents/work/test_file/test.fastq");

	std::vector < Fastq<TUPLETYPE> > result, result_unpack;
	auto w = JobSerializer<void>
			:: create ( std::bind (&FileReader_impl<Fastq, TUPLETYPE> 
				:: get_all_entry, std::placeholders::_1, std::placeholders::_2), std::ref(s1), std::ref(result) );
	w();
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt"); 
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & result;
/*	std::for_each ( result.begin(),
					result.end(),
					[&] (const Fasta<std::tuple<std::string, std::string> >& Q)
					{ arc0 & Q;}
				);*/
	ofs.close();
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & result_unpack;
	
	for (size_t j = 0; j!=result_unpack.size(); ++j )
	{
		EXPECT_EQ ( std::get<0>(result_unpack[j].data), std::get<0>(result[j].data) );
		EXPECT_EQ ( std::get<1>(result_unpack[j].data), std::get<1>(result[j].data) );
		EXPECT_EQ ( std::get<2>(result_unpack[j].data), std::get<2>(result[j].data) );
		EXPECT_EQ ( std::get<3>(result_unpack[j].data), std::get<3>(result[j].data) );
	}
	ifs.close();
};

TEST(mpi_job_creator, bed_get_all_entry_function)
{
	typedef std::tuple<std::string, uint32_t, uint32_t> TUPLETYPE;
	std::string s1 ("/Users/obigbando/Documents/work/test_file/test.bed");

	std::vector < Bed<TUPLETYPE> > result, result_unpack;
	auto w = JobSerializer<void>
			:: create ( std::bind (&FileReader_impl<Bed, TUPLETYPE> 
				:: get_all_entry, std::placeholders::_1, std::placeholders::_2), std::ref(s1), std::ref(result) );
	w();
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt"); 
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & result;
/*	std::for_each ( result.begin(),
					result.end(),
					[&] (const Fasta<std::tuple<std::string, std::string> >& Q)
					{ arc0 & Q;}
				);*/
	ofs.close();
	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & result_unpack;
	
	for (size_t j = 0; j!=result_unpack.size(); ++j )
	{
		EXPECT_EQ ( std::get<0>(result_unpack[j].data), std::get<0>(result[j].data) );
		EXPECT_EQ ( std::get<1>(result_unpack[j].data), std::get<1>(result[j].data) );
		EXPECT_EQ ( std::get<2>(result_unpack[j].data), std::get<2>(result[j].data) );
	}
	ifs.close();
};
#include "../src/wrapper_tuple_utility.hpp"
TEST (mpi_job_creator, VarWig_get_all_entry_function)
{
	typedef std::tuple< std::string, 
						std::string, 
						int, 
						Wrapper < std::vector < Wrapper < std::tuple <uint32_t, double> > > 
								> 
					 > TUPLETYPE;
	std::string s1 ("/Users/obigbando/Documents/work/test_file/testVar.wig");

	std::vector < Wig<TUPLETYPE> > result, result_unpack;
	auto w = JobSerializer<void>
			:: create ( std::bind (&FileReader_impl<Wig, TUPLETYPE> 
				:: get_all_entry, std::placeholders::_1, std::placeholders::_2), std::ref(s1), std::ref(result) );
	w();
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt"); 
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & result;
	ofs.close();

	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & result_unpack;
	for (size_t j = 0; j!=result_unpack.size(); ++j )
	{
		EXPECT_EQ ( std::get<0>(result_unpack[j].data), std::get<0>(result[j].data) );
		EXPECT_EQ ( std::get<1>(result_unpack[j].data), std::get<1>(result[j].data) );
		EXPECT_EQ ( std::get<2>(result_unpack[j].data), std::get<2>(result[j].data) );
		for (size_t i = 0; i != std::get<3>(result_unpack[j].data).data_content.size(); ++i)
		{
			EXPECT_EQ ( (std::get<3>(result_unpack[j].data).data_content)[i].get<0>(), 
						(std::get<3>(result[j].data).data_content)[i].get<0>() );
			EXPECT_EQ ( (std::get<3>(result_unpack[j].data).data_content)[i].get<1>(),
						(std::get<3>(result[j].data).data_content)[i].get<1>() );
		}
	}
	ifs.close();
};

TEST (mpi_job_creator, FixedWig_get_all_entry_function)
{
	typedef std::tuple< std::string, 
						std::string, 
						int, 
						Wrapper < std::vector < Wrapper < std::tuple <uint32_t, double> > > 
								> 
					 > TUPLETYPE;
	std::string s1 ("/Users/obigbando/Documents/work/test_file/testFixed.wig");

	std::vector < Wig<TUPLETYPE> > result, result_unpack;
	auto w = JobSerializer<void>
			:: create ( std::bind (&FileReader_impl<Wig, TUPLETYPE> 
				:: get_all_entry, std::placeholders::_1, std::placeholders::_2), std::ref(s1), std::ref(result) );
	w();
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt"); 
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & result;
	ofs.close();

	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & result_unpack;

	for (size_t j = 0; j!=result_unpack.size(); ++j )
	{
		EXPECT_EQ ( std::get<0>(result_unpack[j].data), std::get<0>(result[j].data) );
		EXPECT_EQ ( std::get<1>(result_unpack[j].data), std::get<1>(result[j].data) );
		EXPECT_EQ ( std::get<2>(result_unpack[j].data), std::get<2>(result[j].data) );
		for (size_t i = 0; i != std::get<3>(result_unpack[j].data).data_content.size(); ++i)
		{
			EXPECT_EQ ( (std::get<3>(result_unpack[j].data).data_content)[i].get<0>(), 
						(std::get<3>(result[j].data).data_content)[i].get<0>() );
			EXPECT_EQ ( (std::get<3>(result_unpack[j].data).data_content)[i].get<1>(),
						(std::get<3>(result[j].data).data_content)[i].get<1>() );
		}
	}
	ifs.close();
};

TEST (mpi_job_creator, sam_get_all_entry_function)
{
	typedef std::tuple < 
						std::string, //header
						std::string, //QNAME
                        int, //FLAG
                        std::string, //RNAME
                        uint32_t, //POS
                        int, //MAPQ
                        std::string, //CIGAR
                        std::string, //RNEXT
                        uint32_t, //PNEXT
                        int, //TLEN
                        std::string //SEQ&QUAL
                        > TUPLETYPE;
	std::string s1 ("/Users/obigbando/Documents/work/test_file/test.sam");

	std::vector < Sam<TUPLETYPE> > result, result_unpack;
	auto w = JobSerializer<void>
			:: create ( std::bind (&FileReader_impl<Sam, TUPLETYPE> 
				:: get_all_entry, std::placeholders::_1, std::placeholders::_2), std::ref(s1), std::ref(result) );
	w();
	std::ofstream ofs ("/Users/obigbando/Documents/work/hung_temp/test.txt"); 
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & result;
	ofs.close();

	std::ifstream ifs ("/Users/obigbando/Documents/work/hung_temp/test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & result_unpack;

	for (size_t j = 0; j!=result_unpack.size(); ++j )
	{
		EXPECT_EQ ( std::get<0>(result_unpack[j].data), std::get<0>(result[j].data) );
		EXPECT_EQ ( std::get<1>(result_unpack[j].data), std::get<1>(result[j].data) );
		EXPECT_EQ ( std::get<2>(result_unpack[j].data), std::get<2>(result[j].data) );
		EXPECT_EQ ( std::get<3>(result_unpack[j].data), std::get<3>(result[j].data) );
		EXPECT_EQ ( std::get<4>(result_unpack[j].data), std::get<4>(result[j].data) );
		EXPECT_EQ ( std::get<5>(result_unpack[j].data), std::get<5>(result[j].data) );
		EXPECT_EQ ( std::get<6>(result_unpack[j].data), std::get<6>(result[j].data) );
		EXPECT_EQ ( std::get<7>(result_unpack[j].data), std::get<7>(result[j].data) );
		EXPECT_EQ ( std::get<8>(result_unpack[j].data), std::get<8>(result[j].data) );
		EXPECT_EQ ( std::get<9>(result_unpack[j].data), std::get<9>(result[j].data) );
		EXPECT_EQ ( std::get<10>(result_unpack[j].data), std::get<10>(result[j].data) );
	}
}

