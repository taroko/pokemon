#include <tuple>
#include <stdint.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "gtest/gtest.h"
#include "../src/format/fastq.hpp"

TEST(Fastq, default_constructor) 
{
	Fastq <> fastq_test;

	EXPECT_EQ (std::get<0> (fastq_test.data),"");
	EXPECT_EQ (std::get<1> (fastq_test.data),"");
	EXPECT_EQ (std::get<2> (fastq_test.data),"");
	EXPECT_EQ (std::get<3> (fastq_test.data),"");
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, copy_constructor)
{
    std::string name1 ("name1");
    std::string sequence ("sequence");
    std::string name2 ("name2");
    std::string quality("quality1");
    typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
    
    Fastq<> k( TupleType( name1, sequence, name2, quality ) );
	Fastq<> fastq_test(k);
    
	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
    EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
    EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
    EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, copy_assignment_constructor)
{
    std::string name1 ("name1");
    std::string sequence ("sequence");
    std::string name2 ("name2");
    std::string quality("quality1");
    typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
    
    Fastq<> k( TupleType( name1, sequence, name2, quality ) );
	Fastq<> fastq_test;
	fastq_test = k;
    
	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
    EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
    EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
    EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, move_data_constructor) 
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;

	Fastq<> k( TupleType( name1, sequence, name2, quality ) );
	Fastq<> fastq_test( std::move(k) );

	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
	EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
	EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
	EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, move_copy_assignment_operator) 
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple< std::string,std::string,std::string,std::string > TupleType;

	Fastq<> k( TupleType(name1, sequence, name2, quality ) );
	Fastq<> fastq_test;
	fastq_test = std::move(k) ;

	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
	EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
	EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
	EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, move_data_tuple_constructor) 
{
	std::string name1 ("name1");
	std::string sequence ("sequence");
	std::string name2 ("name2");
	std::string quality("quality1");
	typedef std::tuple< std::string,std::string,std::string,std::string > TupleType;

	TupleType k( name1, sequence, name2, quality );
	Fastq<> fastq_test( std::move(k) );

	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
	EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
	EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
	EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
}

TEST(Fastq, tupletype_move_constructor)
{
    std::string name1 ("name1");
    std::string sequence ("sequence");
    std::string name2 ("name2");
    std::string quality("quality1");
    typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
    
    Fastq<> fastq_test( TupleType( name1, sequence, name2, quality ) );
    
	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
    EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
    EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
    EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, tupletype_move_assignment)
{
    std::string name1 ("name1");
    std::string sequence ("sequence");
    std::string name2 ("name2");
    std::string quality("quality1");
    typedef std::tuple<std::string,std::string,std::string,std::string> TupleType;
    
    Fastq<> fastq_test( TupleType( name1, sequence, name2, quality ) );
	Fastq<> k = fastq_test;
    
	EXPECT_EQ (std::get<0> (fastq_test.data),"name1");
    EXPECT_EQ (std::get<1> (fastq_test.data),"sequence");
    EXPECT_EQ (std::get<2> (fastq_test.data),"name2");
    EXPECT_EQ (std::get<3> (fastq_test.data),"quality1");
	ASSERT_FALSE (fastq_test.eof_flag);
}

TEST(Fastq, overload_operator)
{
	std::string name1("name1");
	std::string sequence("sequence");
	std::string name2("name2");
	std::string quality("quality1");

	typedef std::tuple< std::string, std::string, std::string, std::string > TupleType;

	Fastq<> k( TupleType( name1, sequence, name2, quality ) );
	Fastq<> fastq_test( std::move(k) );

//	std::cout << fastq_test;
}

TEST(Fastq, serialization)
{
    std::string name1("name1");
    std::string sequence("sequence");
    std::string name2("name2");
    std::string quality("quality1");
    typedef std::tuple< std::string, std::string, std::string, std::string > TupleType;

    Fastq<> k( TupleType( name1, sequence, name2, quality ) );
    Fastq<> fastq_test( std::move(k) );
    Fastq<> fastq_result;
    std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/fastq_serialization.txt");
    boost::archive::text_oarchive arc0 (ofs);
    arc0 & fastq_test;
    ofs.close();
    std::string str_temp;
    std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/fastq_serialization.txt");
    boost::archive::text_iarchive arc1 (ifs);
    arc1 & fastq_result;
    EXPECT_EQ (std::get<0>(fastq_result.data), "name1");
    EXPECT_EQ (std::get<1>(fastq_result.data), "sequence");
    EXPECT_EQ (std::get<2>(fastq_result.data), "name2");
    EXPECT_EQ (std::get<3>(fastq_result.data), "quality1");
    ifs.close();
}
#include "../src/tuple_utility.hpp"

TEST (Fastq, get_data_length)
{
    std::string name1("name1");
    std::string sequence("name2");
    std::string name2("name3");
    std::string quality("name4");
    typedef std::tuple< std::string, std::string, std::string, std::string > TupleType;
	Fastq<> k ( TupleType( name1, sequence, name2, quality ) );
    std::cerr<<k.get_data_length()<<std::endl;
}
TEST(Fastq, static_assert)
{  
//	Fastq <int> gg;
//	std::string s1 ("fucker");
//	Fastq <> aa = s1;
//	Fastq <> bb (s1);
}   
