#include "../src/peat/pair_end_adapter_trimmer_impl.hpp"//"pair_end_adapter_trimmer_parameter_current.hpp"
#include "../src/file_reader.hpp"
#include <sstream>
#include "gtest/gtest.h"
TEST (PEAT_impl, test)
//int main (int argc, char* argv[])
{
	typedef std::tuple<std::string, std::string, std::string, std::string> TUPLETYPE;
	std::vector<std::string> read_vec ({
			"/Users/obigbando/Documents/work/test_file/test_1.fq",
			"/Users/obigbando/Documents/work/test_file/test_2.fq"
		});
	std::map<int, std::vector< Fastq<TUPLETYPE> > > ccc, cca;

	FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQA (read_vec, &cca);	
	QQA.Read (100000);

	FileReader < ParallelTypes::M_T, Fastq, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE >
        QQ (read_vec, &ccc);	
	QQ.Read (100000);

	ParameterTrait <> i_parameter_trait;// (std::get<1>(cca[0].front().data).size(), 100000, 1, 0.0, 0.0, 0.0);	//goes without trimming 

	PairEndAdapterTrimmer_impl <
								Fastq, 
								TUPLETYPE,
								TrimTrait<std::string, LinearStrMatch<double>, double, double > 
								> 
								QQP (//&ccc, 
									i_parameter_trait);
	std::vector <size_t> trim_pos;

	QQP.TrimImpl (&ccc, trim_pos, 0, 1000);

	for (auto ii=0; ii!=2; ++ii)
		for ( auto jj=0; jj!=ccc[0].size(); ++jj)
		{
			if (trim_pos[jj]!=0)
			{
				std::get<1>(cca[ii][jj].data).resize (trim_pos[jj]);
				std::get<3>(cca[ii][jj].data).resize (trim_pos[jj]);
			}
			EXPECT_EQ ( std::get<0>(cca[ii][jj].data), std::get<0>(ccc[ii][jj].data) ); 
			EXPECT_EQ ( std::get<1>(cca[ii][jj].data), std::get<1>(ccc[ii][jj].data) ); 
			EXPECT_EQ ( std::get<2>(cca[ii][jj].data), std::get<2>(ccc[ii][jj].data) ); 
			EXPECT_EQ ( std::get<3>(cca[ii][jj].data), std::get<3>(ccc[ii][jj].data) ); 
		}
}
