#include <tuple>
#include <cstdint>
#include "gtest/gtest.h"
#include "../src/format_io_iterator.hpp"
#include "../src/constant_def.hpp"
#include "../src/format/wig.hpp"
#include "../src/format/wig_reader_impl.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

TEST(Wig_reader, io_end)
{
	typedef std::tuple < std::string, 
						 std::string, 
						 int, 
						 Wrapper< std::vector < Wrapper< std::tuple <uint32_t, double > > > > 
					   > TUPLETYPE;
//	FileReader_impl < format_types::WIG, 
//				 TUPLETYPE > Wig_Reader_test;
	FileReader_impl < Wig, TUPLETYPE > Wig_Reader_test; 

	EXPECT_EQ( ( (Wig_Reader_test.io_end() ) -> eof_flag ), true );
}

TEST(Wig_reader, var_get_next_entry)
{
	typedef  std::tuple <std::string, 
						 std::string, 
						 int, 
						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > > 
						> TUPLETYPE;
	std::ifstream file_handle;
	file_handle.open ("/Users/obigbando/Documents/work/test_file/testVar.wig", std::ifstream::in);
//"/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/testVar.wig" , std::ifstream::in);
//	std::string file_name ("/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/testVar.wig");
	auto x = FileReader_impl< Wig, TUPLETYPE >::get_next_entry (file_handle);
	uint32_t a=14430066;
	EXPECT_EQ(std::get<0> (x.data), "track"); 
	EXPECT_EQ(std::get<1> (x.data), "chr22");
	EXPECT_EQ(std::get<2> (x.data), 50);
	EXPECT_EQ(std::get<3> (x.data).get<0>().get<0>(), a);
	EXPECT_EQ(std::get<3> (x.data).get<0>().get<1>(), 0.78  );
	EXPECT_EQ(std::get<3> (x.data).get<1>().get<0>(), 14430166 );
	EXPECT_EQ(std::get<3> (x.data).get<1>().get<1>(), -0.05  );
	EXPECT_EQ(std::get<3> (x.data).get<2>().get<0>(), 14430266 );
	EXPECT_EQ(std::get<3> (x.data).get<2>().get<1>(), -0.16  );
}

TEST(Wig_reader, get_next_entry)
{
    typedef  std::tuple <std::string,
                         std::string,
                         int,
                         Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > >
                        > TUPLETYPE;
    std::ifstream file_handle;
    file_handle.open ("/Users/obigbando/Documents/work/test_file/test2.wig", std::ifstream::in);
	std::vector< Wig<TUPLETYPE> > gg;
	for (auto i = 0; i!=5; ++i)
	{
		auto x = FileReader_impl< Wig, TUPLETYPE >::get_next_entry (file_handle);
		gg.push_back (x);
	}
//	std::for_each (gg.begin(), gg.end(),
//					[] (const Wig<TUPLETYPE>& Q)
//					{ std::cerr<<Q<<std::endl;}
//					);
}

TEST(Wig_reader, fix_get_next_entry)
{
	typedef  std::tuple <std::string, 
						 std::string, 
						 int, 
						 Wrapper< std::vector < Wrapper < std::tuple <uint32_t, double> > > > 
						> TUPLETYPE;
	std::ifstream file_handle;
	file_handle.open ("/Users/obigbando/Documents/work/test_file/testFixed.wig", std::ifstream::in);
//	file_handle.open ("/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/testFixed.wig" , std::ifstream::in);
//	std::string file_name ("/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/testFixed.wig");
	auto x = FileReader_impl< Wig, TUPLETYPE >::get_next_entry (file_handle);
	EXPECT_EQ(std::get<0> (x.data), "track"); 
	EXPECT_EQ(std::get<1> (x.data), "chr7");
	EXPECT_EQ(std::get<2> (x.data), 1);
	EXPECT_EQ(std::get<3> (x.data).get<0>().get<0>(), 115597761);
	EXPECT_EQ(std::get<3> (x.data).get<0>().get<1>(), 19.0  );
	EXPECT_EQ(std::get<3> (x.data).get<1>().get<0>(), 115597766);
	EXPECT_EQ(std::get<3> (x.data).get<1>().get<1>(), 19.1  );
	EXPECT_EQ(std::get<3> (x.data).get<2>().get<0>(), 115597771);
	EXPECT_EQ(std::get<3> (x.data).get<2>().get<1>(), 59.2  );
	EXPECT_EQ(std::get<3> (x.data).get<9>().get<0>(), 115597806);
	EXPECT_EQ(std::get<3> (x.data).get<9>().get<1>(), 19.9  );
}

TEST(Wig_reader_Bwig, read_bigwig)
{
	typedef std::tuple < std::string,
						 std::string,
						 int,
						 Wrapper< std::vector < Wrapper< std::tuple <uint32_t, double > > > >
					   > TUPLETYPE;
//	std::string s1 = "/Users/obigbando/Documents/work/test_file/original_test_tool.bw";
//	std::string s1 = "/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/testVar_converted.bw";
	std::string s1 = "/Users/obigbando/Documents/work/test_file/testVar.bw";
	std::string s2 = "/Users/obigbando/Documents/work/test_file/original_test_tool.wig";
//	auto y = FileReader_impl<format_types::WIG, TUPLETYPE>::read_bigwig (const_cast<char*>( s1.c_str() ), const_cast<char*> (s2.c_str() ) );
	auto y = FileReader_impl< Wig, TUPLETYPE >::read_bigwig (const_cast<char*>( s1.c_str() ), const_cast<char*>( s2.c_str() ) );
	std::ifstream file_handle;
	file_handle.open (y, std::ifstream::in);
//	auto x = FileReader_impl<format_types::WIG, TUPLETYPE>::get_next_entry (file_handle);
	auto x = FileReader_impl< Wig, TUPLETYPE >::get_next_entry (file_handle);
	typedef std::tuple <uint32_t, double> MyType;
	typedef std::vector < Wrapper <MyType> > VectorType;
	EXPECT_EQ(std::get<0> (x.data), "track");
	EXPECT_EQ(std::get<1> (x.data), "chr22");
	EXPECT_EQ(std::get<2> (x.data), 50);
	EXPECT_EQ(std::get<3> (x.data).get<0>().get<0>(), 14430066);
	EXPECT_EQ(std::get<3> (x.data).get<0>().get<1>(),   0.78);
	EXPECT_EQ(std::get<3> (x.data).get<1>().get<0>(), 14430166);
	EXPECT_EQ(std::get<3> (x.data).get<1>().get<1>(), -0.05 );
	EXPECT_EQ(std::get<3> (x.data).get<2>().get<0>(), 14430266);
	EXPECT_EQ(std::get<3> (x.data).get<2>().get<1>(), -0.16 );
}

TEST(Wig_reader_Bwig, write_bigwig)
{
	typedef std::tuple < std::string,
						 std::string,
						 int,
						 Wrapper< std::vector < Wrapper< std::tuple <uint32_t, double > > > >
					   > TUPLETYPE;
	char in[] = "/Users/obigbando/Documents/work/test_file/original_test_tool.wig";
//	char cs[] = "/Users/obigbando/Downloads/kent/src/utils/wigToBigWig/test.sizes";
	char cs[] = "/Users/obigbando/Documents/work/test_file/wig_test.sizes";
	char out[] = "/Users/obigbando/Documents/work/test_file/original_test_tool_2.bw";
//	char* k = FileReader_impl<format_types::WIG, TUPLETYPE>::write_bigwig (in, cs, out);
	FileReader_impl< Wig, TUPLETYPE >::write_bigwig (in, cs, out);
}
