#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include "annotation_set.hpp"
#include "annotation.hpp"
#include "annrawbed2bwg.hpp"
#include "../src/format/raw_bed.hpp"
#include "../src/format/sam.hpp"
#include "../src/file_reader_impl.hpp"

class AnnoTrait_MGI
{
public:
    const char* file_path = "/home/andy/db/MGI_mm9_no_cluster.bed";
};

class AnnoTrait_QQQ
{
public:
    const char* file_path = "/home/andy/db/MGI_mm9_no_cluster.bed";
};



int main (void)
{
    typedef std::tuple <
        std::string, //QNAME
        int, //std::bitset<SAM_FLAG::FLAG_SIZE>,//SAM_FLAG,
        std::string, //RNAME
        uint64_t, //POS
        int, //MAPQ
        std::string, //CIGAR
        std::string, //RNEXT
        uint64_t, //PNEXT
        int64_t, //TLEN
        std::string,//, //SEQ
        std::string, //QUAL
		UserDefineTags< boost::mpl::string<'NH'>, boost::mpl::string<'TL'> >
//		UserDefineContent
//        int, // NH
//        std::string //TailSeq
                        > TUPLETYPE;

	std::string file_name ("test.sam");
	std::ifstream file_handle (file_name);
	std::vector<std::string> QQ ({file_name});
	FileReader_impl < Sam, TUPLETYPE, SOURCE_TYPE::IFSTREAM_TYPE  > Sam_Reader_test (QQ);

	size_t index=0;
	std::vector <Sam <TUPLETYPE> > AAA;
	std::vector < RawBed<> > BBB;
//	std::map < RawBed<>, int > CCC;
	std::map < RawBed<>, uint16_t > DDD;

	const size_t RawBedSize = sizeof (RawBed<>);
	while (index!=20)
	{
		const Sam <TUPLETYPE> qq =( Sam_Reader_test.get_next_entry (0) );
		RawBed <> yy (qq);
std::cerr<<"input rawbed\n"<<yy;
//		++CCC[yy];
		++ ( *( ( &( DDD[RawBed<>(qq)]) ) - 5 ) );	
		AAA.push_back (qq);
		++index;
	}


	typedef AnnotationSet < 
		//std::function < void ( AnnotationRawBed<>& ) >,
		std::vector< AnnotationRawBed<> >,
		1,
		Annotation< FileReader_impl < Bed, std::tuple<std::string, uint32_t, uint32_t, char, std::string, std::string>, SOURCE_TYPE::IFSTREAM_TYPE >// Parser type
			,AnnoTrait_MGI
			,AnnoIgnoreStrand::IGNORE
			,AnnoType::INTERSET
			>
	> Annotations;

std::cerr<<"=================="<<'\n';
	for (auto& D : DDD)
		std::cerr<<D.first;
std::cerr<<"=================="<<'\n';


	std::vector< AnnotationRawBed<> > *ptr_vector = new std::vector <AnnotationRawBed<> > ();
	for (auto& ii : DDD)
		ptr_vector->push_back (ii.first);

	std::vector<std::string> AnnoDBPath
	{
		"/home/andy/db/MGI_mm9_no_cluster.bed"
		,"/home/andy/db/MGI_mm9_no_cluster.bed"
	};

	Annotations annos;  
	annos.AnnotateAll(*ptr_vector);

	size_t ind=0;
	for (auto& Q : *ptr_vector)
	{
		std::cerr<<ind<<'\n'<<Q;
		++ind;
	}


	AnnRawBed2Bwg<> gg (0, "abc.qq");	
	for (auto& Q : *ptr_vector)
	{
		gg.run (Q);//(*ptr_vector)[0]);
//		std::cerr<<ind<<'\n'<<Q;
//		++ind;
		gg.PrintItem();
	}
	
};
