#include "../src/format/curl_send_impl.hpp"
#include "../src/file_reader.hpp"
int main (void)
{
	typedef std::tuple <std::string, std::string,
			std::string, std::string > TUPLETYPE;
	std::map <int, std::vector< Fastq <TUPLETYPE> > > result;

	std::vector <std::string> vec1  
		({
	"https://api.basespace.illumina.com/v1pre3/files/113647546/content?access_token=4b2bf2c94ea34721ac01fc177111707f",
	"https://api.basespace.illumina.com/v1pre3/files/113645006/content?access_token=4b2bf2c94ea34721ac01fc177111707f"
		 });
	std::vector <size_t> vec2
		({
		18632783,
		20187631
		 });

	//FileReader < ParallelTypes::NORMAL, Fastq, TUPLETYPE, SOURCE_TYPE::CURL_TYPE_GZ, curl_default_handle  > GG (vec1, &result, vec2);
	
	CurlSendImpl <curl_default_handle> Q1 (
		 "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=ttf1.txt&multipart=true"
		,"x-access-token: 4b2bf2c94ea34721ac01fc177111707f"
		// "https://api.basespace.illumina.com/v1pre3/appresults/1671671/files?name=ttf1.txt&multipart=true"
		//,"x-access-token: f4f68db924e54889bccd93e2bb9b8a7e"
		,"Content-Type: application/text"
		,"https://api.basespace.illumina.com/");

	CurlSendImpl <curl_default_handle> Q2 (
		 "https://api.basespace.illumina.com/v1pre3/appresults/1650649/files?name=ttf2.txt&multipart=true"
		,"x-access-token: 4b2bf2c94ea34721ac01fc177111707f"
		// "https://api.basespace.illumina.com/v1pre3/appresults/1671671/files?name=ttf2.txt&multipart=true"
		//,"x-access-token: f4f68db924e54889bccd93e2bb9b8a7e"
		,"Content-Type: application/text"
		,"https://api.basespace.illumina.com/");
	
	size_t read_round = 0;
	bool flag;
	while (true)
	{

		std::stringstream ss1, ss2;
		ss1 << "abcdefghi";
//		std::cerr<<"round #"<<read_round<<'\n';
//		GG.Read (49997);// file_handles );
//		flag = true;
//		for ( auto i = 0; i!=result.size(); ++i )
//			flag *= (result)[i].front().eof_flag;
//		if (flag)
//			break;
//		std::for_each ( (result)[0].begin(), (result)[0].end(),
//				[&ss1] ( const Fastq<TUPLETYPE>& q )
//				{ ss1<<q;}); 
//		std::for_each ( (result)[1].begin(), (result)[1].end(),
//				[&ss2] ( const Fastq<TUPLETYPE>& q )
//				{ ss2<<q;}); 
//		++read_round;
		std::cerr<<"obtained ss1 & ss2 length "<<ss1.str().size()<<'\t'<<ss2.str().size()<<'\n';//<<ss1.tellp()<<'\t'<<ss2.tellp()<<'\n';
		size_t count = (result)[0].size();
		std::cerr<<(result)[0].size()<<'\t'<<(result)[1].size()<<std::endl;

//		bool keep_on = false;
//		if ( (result)[0].size() == 49997 || (result)[1].size() == 49997 )
//			keep_on = true;
//		else
//			keep_on = false;

		std::streamsize read_tg = ss1.tellg();
		std::streamsize read_tp = ss1.tellp();
		char* ka = new char [read_tp-read_tg];
		ss1.write ( ka, read_tp-read_tg );
		Q1.send (ka, 1, ss1.str().size());//flag);
//		Q2.send (ss2.str().c_str(), 1, keep_on);//flag);
		ss1.str(""), ss2.str("");
	}
	std::cerr<<(result)[0].size()<<'\t'<<(result)[1].size()<<std::endl;
//	Q1.send_end ();// (ss1, false);//flag);
//	Q2.send_end ();// (ss2, false);//flag);
};
