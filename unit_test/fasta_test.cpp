#include <tuple>
#include <stdint.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "gtest/gtest.h"
#include "../src/format/fasta.hpp"

TEST(Fasta, default_constructor)
{
	//typedef std::tuple<std::string,std::string> TupleType;
	Fasta <> fasta_test;
	
	EXPECT_EQ (std::get<0> (fasta_test.data),"");
	EXPECT_EQ (std::get<1> (fasta_test.data),"");
	ASSERT_FALSE (fasta_test.eof_flag);
}

TEST(Fasta, default_constructor_initialdata)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	//TupleType k(head,line);
	Fasta <> fasta_test(TupleType(head,line));
	
	EXPECT_EQ (std::get<0> (fasta_test.data),"name1");
	EXPECT_EQ (std::get<1> (fasta_test.data),"sequence");
	ASSERT_FALSE (fasta_test.eof_flag);
}

TEST(Fasta, copy_constructor)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	Fasta<> k( TupleType(head,line) );
	Fasta<> fasta_test(k);
	
	EXPECT_EQ (std::get<0> (fasta_test.data),"name1");
	EXPECT_EQ (std::get<1> (fasta_test.data),"sequence");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (fasta_test.eof_flag);
}

TEST(Fasta, copy_assignment_operator)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	Fasta<> k( TupleType(head,line) );
	Fasta<> j;
	j = k;
	
	EXPECT_EQ (std::get<0> (j.data),"name1");
	EXPECT_EQ (std::get<1> (j.data),"sequence");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (j.eof_flag);
}

TEST(Fasta, move_data_constructor)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	Fasta<> k( TupleType(head,line) );
	Fasta<> fasta_test( std::move(k) );
	
	EXPECT_EQ (std::get<0> (fasta_test.data),"name1");
	EXPECT_EQ (std::get<1> (fasta_test.data),"sequence");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (fasta_test.eof_flag);
}

TEST(Fasta, move_copy_assignment_operator)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	Fasta<> k( TupleType(head,line) );
	Fasta<> j;
	j = std::move(k);
	
	EXPECT_EQ (std::get<0> (j.data),"name1");
	EXPECT_EQ (std::get<1> (j.data),"sequence");
	ASSERT_FALSE (k.eof_flag);
	ASSERT_FALSE (j.eof_flag);
}

TEST(Fasta, copy_tupletype_constructor)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	TupleType k(head,line);
	Fasta<> fasta_test(k);
	
	EXPECT_EQ (std::get<0> (fasta_test.data),"name1");
	EXPECT_EQ (std::get<1> (fasta_test.data),"sequence");
	//ASSERT_FALSE (k.eof_flag);
	//ASSERT_FALSE (fasta_test.eof_flag);
}

TEST(Fasta, move_data_tupletype_constructor)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	TupleType k(head,line);
	Fasta<> fasta_test( std::move(k) );
	
	EXPECT_EQ (std::get<0> (fasta_test.data),"name1");
	EXPECT_EQ (std::get<1> (fasta_test.data),"sequence");
	//ASSERT_FALSE (k.eof_flag);
	//ASSERT_FALSE (fasta_test.eof_flag);
}


TEST(Fasta, overload_operator)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	TupleType k(head,line);
	Fasta<> fasta_test( std::move(k) );

	std::ostringstream s1;
	s1 << fasta_test;
	std::string s2 = s1.str();
	EXPECT_EQ (s2,">name1\nsequence\n");
}

TEST(Fasta, serialization)
{
	std::string head ("name1");
	std::string line ("sequence");
	typedef std::tuple<std::string,std::string> TupleType;
	
	TupleType k(head,line);
	Fasta<> fasta_test( std::move(k) );
	Fasta<> fasta_result;
	std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/fasta_serialization_test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & fasta_test;
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/fasta_serialization_test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & fasta_result;
	EXPECT_EQ (std::get<0>(fasta_result.data), "name1");
	EXPECT_EQ (std::get<1>(fasta_result.data), "sequence");
	ifs.close();
}

TEST(Fasta, static_assert)
{
//	Fasta <int> gg;
//	std::string s1 ("fucker");
//	Fasta <> aa = s1;
//	Fasta <> bb (s1);
}
