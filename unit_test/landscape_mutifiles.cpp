#include <fstream>
#include <deque>
#include <map>
#include <unordered_map>
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "../src/parser.hpp"
//#include "../src/bwt.hpp"
#include "gtest/gtest.h"

template <typename mapType, typename addType>
class addMap
{
public:
	// { geneType, {tail, reads_number} }
	void operator()(mapType &map, mapType value, addType test=0){
		typename mapType::iterator map_it;
		if((map_it = map.find( (*value.begin() ).first )) ==  map.end())
		{
			map.insert( { (*value.begin() ).first, (*value.begin() ).second } );
		}
		else
		{
			addMap< typename mapType::mapped_type , addType> am;
			am( (*map_it).second, (*value.begin() ).second , test);
		}	
	}
};
template <typename T>
class addMap<T,T>
{
public:
	void operator()(T &v1, T &v2, T v3){
		v1 += v2;
	}
};

int main(int argc, char** argv)
{
	if(argc < 4)
	{
		std::cout << "usage ./landscape_mutifiles scape_size overlap_number file1 file2 ..." << std::endl;
		std::cout << "for landscape(overview), count overlap with window, ..." << std::endl;
		return 0;
	}
	
	uint32_t scape_size(0), overlap_number(0);
	
	scape_size = std::stoi(argv[1]);
	overlap_number = std::stoi(argv[2]);
	
	
	std::vector <std::ifstream*> file_handles;
	for (int i=3; i < argc; ++i)
		file_handles.push_back (new std::ifstream ( argv[i] ) );
	typedef std::tuple < std::string, uint32_t, uint32_t, uint32_t > TUPLETYPE;

	FileParser < ParallelTypes::NORMAL, Bed, TUPLETYPE, CompressType::Plain, std::deque, 2 > LandScape;
	
	//chr,file_idx, start, num
	std::map< std::string ,std::map<int, std::map<uint32_t, uint32_t> > > landscape;
	addMap< std::map<std::string ,std::map<int, std::map<uint32_t, uint32_t> > >, uint32_t> addmap;
	
	//for quickly
	std::map < std::string, std::set<uint32_t> > total_scape;
	
	uint32_t min_start(3000000000),max_start(0);
	bool check_eof(true);
	do
	{
		check_eof=true;
		auto x = LandScape.Read ( file_handles );
		int file_idx=0;
		std::for_each (	x->begin(), x->end(),
			[&] (const std::pair<int, Bed<TUPLETYPE> >& Q)
			{
				if(!Q.second.eof_flag){
					check_eof=false;
					std::string chr( std::get<0>(Q.second.data) );
					uint32_t start( std::get<1>(Q.second.data) );
					uint32_t num( std::get<3>(Q.second.data) );
					addmap(landscape, {{chr, {{file_idx,{{start/scape_size,num}} }} }});
					
					total_scape[chr].insert(start/scape_size);
					
					max_start = std::max( (start/scape_size), max_start);
					min_start = std::min( (start/scape_size), min_start);
					
				}
				++file_idx;
			}
		);
	}while(!check_eof);
	
	bool is_zero(true);
	std::stringstream tmp_string;
	std::string chr("");
	
	
	std::for_each (landscape.begin(), landscape.end(),
		[&] (const std::pair<std::string, std::map< int, std::map<uint32_t, uint32_t> > > &Q)
		{
			chr = Q.first;
			
			//if(min_start > overlap_number) min_start -= overlap_number;
			//for(uint32_t i_start(min_start); i_start <= max_start; ++i_start)
			for(uint32_t i_start : total_scape[chr])
			{
				uint32_t start(i_start*scape_size), end((i_start+overlap_number)*scape_size);
				is_zero = true;
				
				std::for_each (Q.second.begin(), Q.second.end(),
					[&] (const std::pair<int, std::map<uint32_t, uint32_t> > &K)
					{
						uint32_t number(0);
						for(uint32_t i_overlap(0); i_overlap < overlap_number; ++i_overlap)
						{
							auto it = (K.second).find(i_start + i_overlap);
							if( it != K.second.end() )
								number += (*it).second;
						}
						tmp_string << "\t" << number;
						if(number != 0) is_zero = false;
					}
				);
				if(!is_zero)
					std::cout << chr << "\t" << start << "\t" << end << tmp_string.str() << "\n";
				tmp_string.str("");
				
			}
		}
	);
	
	
}

