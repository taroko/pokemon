//#include "../src/compression/n_bit_compress.hpp"
#include "../src/compression/n_bit_compress.hpp"
#include <iostream>
#include <unordered_map>
#include <string>
#include <cstdint>
#include <deque>
#include <fstream>
#include <bitset>
#include "gtest/gtest.h"
#include "boost/lexical_cast.hpp"
#include "boost/dynamic_bitset.hpp"
//Constructors
/*
/// @fn TEST(NBitCompress, default_constructor_Normal)
TEST(NBitCompress, default_constructor_Normal)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > gg;
	EXPECT_EQ (gg.CharNormal, gg.TraitObject.alphabet);
	gg.Printer();
}
/// @fn TEST(NBitCompress, default_constructor_Mask)
TEST(NBitCompress, default_constructor_Mask)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g1;
	EXPECT_EQ (g1.CharNormal, g1.TraitObject.alphabet);
	g1.Printer();
}
/// @fn TEST(NBitCompress, constructor_with_length_Normal)
TEST(NBitCompress, constructor_with_length_Normal)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g2 (64);
	EXPECT_EQ( g2.GetSeq().size(), 64);
}
/// @fn TEST(NBitCompress, constructor_with_length_Mask)
TEST(NBitCompress, constructor_with_length_Mask)
{
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g3 (64);
	EXPECT_EQ( g3.GetSeq().size(), 64);
}
/// @fn TEST(NBitCompress, constructor_with_string_Normal)
TEST(NBitCompress, constructor_with_string_Normal)
{
	std::string s1 ("ATCGATCGAbCdefGCXDWQRMM");
//	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g4 (s1); //("ATCGatcgabcdefgCXDWQRMM");
//	EXPECT_EQ (g4.MakeSeqString(), s1);
	std::string sstr;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	g42.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);
}

/// @fn TEST(NBitCompress, constructor_with_string_Mask)
TEST(NBitCompress, constructor_with_string_Mask)
{
	std::string s1 ("ATCGatcgaM");
	std::string sstr;
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	g42.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);
}
/// @fn TEST(NBitCompress, copy_constructor_Normal)
TEST(NBitCompress, copy_constructor_Normal)
{
	std::string s1 ("ATCGATCGAbCdefGCXDWQRMM");//("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g4 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g5 (g4);
	EXPECT_EQ ( g4.GetSeq(), g5.GetSeq());
//	EXPECT_EQ ( g4.GetMaskDeq(), g5.GetMaskDeq() );
	EXPECT_EQ ( g4.GetExceptDeq(), g5.GetExceptDeq() );
	for (auto i=0; i!=g4.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g4.GetMaskDeq()[i].second.GetSeq(), g5.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g4.GetMaskDeq()[i].second.GetExceptDeq(), g5.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g4.GetMaskDeq()[i].second.GetMaskDeq(), g5.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, copy_constructor_Mask)
TEST(NBitCompress, copy_constructor_Mask)
{
	std::string s1 ("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK> > g42 (s1); //("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g52 (g42);
	EXPECT_EQ ( g42.GetSeq(), g52.GetSeq());
	for (auto i=0; i!=g42.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g42.GetMaskDeq()[i].second.GetSeq(), g52.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g42.GetMaskDeq()[i].second.GetExceptDeq(), g52.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g42.GetMaskDeq()[i].second.GetMaskDeq(), g52.GetMaskDeq()[i].second.GetMaskDeq());
	}
	EXPECT_EQ ( g42.GetExceptDeq(), g52.GetExceptDeq() );
}
/// @fn TEST(NBitCompress, constructor_with_elements_Normal)
TEST(NBitCompress, constructor_with_elements_Normal)
{
	std::string s6 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z1 ( s6 );
	auto x = z1.GetSeq();
	auto y = z1.GetExceptDeq();
	auto z = z1.GetMaskDeq();
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g6 ( x, y);//, w);
	EXPECT_EQ ( x, g6.GetSeq());
	EXPECT_EQ ( y, g6.GetExceptDeq() );
	for (auto i=0; i!=g6.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g6.GetMaskDeq()[i].second.GetSeq(), z[i].second.GetSeq());
		EXPECT_EQ (g6.GetMaskDeq()[i].second.GetExceptDeq(), z[i].second.GetExceptDeq());
//		EXPECT_EQ (g6.GetMaskDeq()[i].second.GetMaskDeq(), z[i].second.GetMaskDeq());
	}
//	EXPECT_EQ ( z, g6.GetMaskDeq() );
}
/// @fn TEST(NBitCompress, costructor_with_elements_Mask)
TEST(NBitCompress, costructor_with_elements_Mask)
{
	std::string s6 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z2 ( s6 );
	auto x2 = z2.GetSeq();
	auto y2 = z2.GetExceptDeq();
	auto w2 = z2.GetMaskDeq();
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g62 ( x2, y2, w2);
	EXPECT_EQ ( x2, g62.GetSeq());
	EXPECT_EQ ( y2, g62.GetExceptDeq() );
	for (auto i=0; i!=g62.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g62.GetMaskDeq()[i].second.GetSeq(), w2[i].second.GetSeq());
		EXPECT_EQ (g62.GetMaskDeq()[i].second.GetExceptDeq(), w2[i].second.GetExceptDeq());
//		EXPECT_EQ (g62.GetMaskDeq()[i].second.GetMaskDeq(), w2[i].second.GetMaskDeq());
	}
//	EXPECT_EQ ( w2, g62.GetMaskDeq() );
}
/// @fn TEST(NBitCompress, constructor_with_rvalue_elements_Normal)
TEST(NBitCompress, constructor_with_rvalue_elements_Normal)
{
	std::string s7 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z3 ( s7 ), zz3 (s7);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g7 (z3.GetSeq(), z3.GetExceptDeq());
	EXPECT_EQ ( zz3.GetSeq(), g7.GetSeq());
//	EXPECT_EQ ( zz3.GetMaskDeq(), g7.GetMaskDeq() );
	EXPECT_EQ ( zz3.GetExceptDeq(), g7.GetExceptDeq() );
	for (auto i=0; i!=g7.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetSeq(), zz3.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetExceptDeq(), zz3.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, constructor_with_rvalue_elements_Mask)
TEST(NBitCompress, constructor_with_rvalue_elements_Mask)
{
	std::string s7 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z4 ( s7 ), zz4 (s7);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g72 (z4.GetSeq(), z4.GetExceptDeq(), z4.GetMaskDeq());
	EXPECT_EQ ( zz4.GetSeq(), g72.GetSeq());
//	EXPECT_EQ ( zz4.GetMaskDeq(), g72.GetMaskDeq() );
	EXPECT_EQ ( zz4.GetExceptDeq(), g72.GetExceptDeq() );
	for (auto i=0; i!=g72.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g72.GetMaskDeq()[i].second.GetSeq(), zz4.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g72.GetMaskDeq()[i].second.GetExceptDeq(), zz4.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, move_constructor_Normal)
TEST(NBitCompress, move_constructor_Normal)
{
	std::string s8 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z8 ( s8 ), zz8 (s8);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g8 ( std::move(z8) );
	EXPECT_EQ (zz8.GetSeq(), g8.GetSeq());
//	EXPECT_EQ (zz8.GetMaskDeq(), g8.GetMaskDeq());
	EXPECT_EQ (zz8.GetExceptDeq(), g8.GetExceptDeq());	
	for (auto i=0; i!=g8.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g8.GetMaskDeq()[i].second.GetSeq(), zz8.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g8.GetMaskDeq()[i].second.GetExceptDeq(), zz8.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, move_constructor_Mask)
TEST(NBitCompress, move_constructor_Mask)
{
	std::string s8 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z82 ( s8 ), zz82 (s8);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g82 ( std::move(z82) );

	EXPECT_EQ (zz82.GetSeq(), g82.GetSeq());
//	EXPECT_EQ (zz82.GetMaskDeq(), g82.GetMaskDeq());
	EXPECT_EQ (zz82.GetExceptDeq(), g82.GetExceptDeq());	
	for (auto i=0; i!=g82.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g82.GetMaskDeq()[i].second.GetSeq(), zz82.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g82.GetMaskDeq()[i].second.GetExceptDeq(), zz82.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, Move_assignment_Normal)
TEST(NBitCompress, Move_assignment_Normal)
{
	std::string s9 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z9 ( s9 ), zz9 (s9);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g9;
	g9 = std::move(z9);
	EXPECT_EQ (zz9.GetSeq(), g9.GetSeq());
	EXPECT_EQ (zz9.GetExceptDeq(), g9.GetExceptDeq());
//	EXPECT_EQ (zz9.GetMaskDeq(), g9.GetMaskDeq());
	for (auto i=0; i!=g9.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g9.GetMaskDeq()[i].second.GetSeq(), zz9.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g9.GetMaskDeq()[i].second.GetExceptDeq(), zz9.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, Move_assignment_Mask)
TEST(NBitCompress, Move_assignment_Mask)
{
	std::string s9 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z92 ( s9 ), zz92 (s9);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g92;
	g92 = std::move(z92);
	EXPECT_EQ (zz92.GetSeq(), g92.GetSeq());
	EXPECT_EQ (zz92.GetExceptDeq(), g92.GetExceptDeq());
//	EXPECT_EQ (zz92.GetMaskDeq(), g92.GetMaskDeq());
	for (auto i=0; i!=g92.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g92.GetMaskDeq()[i].second.GetSeq(), zz92.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g92.GetMaskDeq()[i].second.GetExceptDeq(), zz92.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, assignment_Normal)
TEST(NBitCompress, assignment_Normal)
{
	std::string s10 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > z10 (s10);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS> > g10;
	g10 = z10;
	EXPECT_EQ (z10.GetSeq(), g10.GetSeq());
	EXPECT_EQ (z10.GetExceptDeq(), g10.GetExceptDeq());
//	EXPECT_EQ (z10.GetMaskDeq(), g10.GetMaskDeq());
	for (auto i=0; i!=g10.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g10.GetMaskDeq()[i].second.GetSeq(), z10.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g10.GetMaskDeq()[i].second.GetExceptDeq(), z10.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
/// @fn TEST(NBitCompress, assignment_Mask)
TEST(NBitCompress, assignment_Mask)
{
	std::string s10 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > z102 (s10);
	NBitCompress < CompressRuleTrait<6, CompressType::N_BITS_MASK> > g102; 
	g102 = z102;
	EXPECT_EQ (z102.GetSeq(), g102.GetSeq());
	EXPECT_EQ (z102.GetExceptDeq(), g102.GetExceptDeq());
//	EXPECT_EQ (z102.GetMaskDeq(), g102.GetMaskDeq());
	for (auto i=0; i!=g102.GetMaskDeq().size(); ++i)
	{
		EXPECT_EQ (g102.GetMaskDeq()[i].second.GetSeq(), z102.GetMaskDeq()[i].second.GetSeq());
		EXPECT_EQ (g102.GetMaskDeq()[i].second.GetExceptDeq(), z102.GetMaskDeq()[i].second.GetExceptDeq());
//		EXPECT_EQ (g7.GetMaskDeq()[i].second.GetMaskDeq(), zz3.GetMaskDeq()[i].second.GetMaskDeq());
	}
}
*/
/*
//Base Functions
/// @fn TEST(NBitCompress, MakeString_Normal)
TEST(NBitCompress, MakeString_Normal)
{
	std::string s11 ("dsxxkkzmnnnvbbCmxmmXZXMZMVNBM<ZVZXMATCGATCGAbCdefGCXDWQRMM");//("ATCGatcgabcdefgCXDWQRMM");
	std::string sstr;
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g11 (s11); //("ATCGatcgabcdefgCXDWQRMM");
	g11.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s11);
	EXPECT_EQ ( g11.MakeNBitString(), boost::lexical_cast<std::string> (g11.GetSeq()) );
}
/// @fn TEST(NBitCompress, MakeString_Mask)
TEST(NBitCompress, MakeString_Mask)
{
	std::string s11 ("zzxcvcnvmzZMCNMVeworpzJFKEVJFBLKFJFOWIJIFRLKWEFLATCGATCGabcdefgCXDWQRMM");//("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g11 (s11); //("ATCGatcgabcdefgCXDWQRMM");
//	std::cerr<<"constructed sequence"<<g11.MakeNBitString().size()<<std::endl;
//	std::cerr<<"constructed sequence"<<g11.MakeSeqString().size()<<std::endl;
	std::string sstr;
	g11.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s11);
	EXPECT_EQ ( g11.MakeNBitString(), boost::lexical_cast<std::string> (g11.GetSeq()) );
}

*/

/// @fn TEST(NBitCompress, middle_bracket_Normal)
TEST(NBitCompress, middle_bracket_Normal)
{
//	std::string s1 ("ATivxzweoolCGRREGVqqzmnblkVCCDD");
	std::string s1 ("RAdGrbEFD1EqrREqeqrRFbeADAGGGGCTNAbCdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnAbCdefGNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS> >  z ( s1 );
	for ( auto index = 0; index!=s1.size(); ++index)
	{
		auto gg = z[index];
		EXPECT_EQ ( gg, s1[index]);
	}
}
/// @fn TEST(NBitCompress, middle_bracket_Mask)
TEST(NBitCompress, middle_bracket_Mask)
{
    std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
    std::string s1, s2;
    std::getline (ofs, s2);
    int i =0;
    while (i<250)
    {
        std::getline (ofs,s2) ;
        s1+=s2;
        ++i;
    }
//  std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
//	std::string s1 ("ATivxzweoolCGRREGVqqzmnblkVCCDD");
//	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiEnabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> >  z ( s1 );
	std::cerr<<s1.size()<<std::endl;
	for ( auto index = 0; index!=s1.size(); ++index)
	{
//		auto gg = z[index];
		EXPECT_EQ ( z[index], s1[index]);
		std::cerr<<"count info "<<z.Normal_count<<'\t'<<z.Except_count<<'\t'<<z.Mask_count<<std::endl;
	}
}
/// @fn TEST(NBitCompress, Substr_Normal)
TEST(NBitCompress, Substr_Normal)
{
	std::string s11 ("ABBTERCFDWERGATCGAbCdefGCXDWQRMMMTTDJDFBBQQ");
	std::string sstr;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > g11 (s11); //("ATCGatcgabcdefgCXDWQRMM");
//	std::cerr<<g11.GetSubStr(5, 33)<<std::endl;;
	g11.GetSubStr(sstr, 2, 33);
	EXPECT_EQ (s11.substr(2,33), sstr);//g11.GetSubStr(5, 33));
}
/// @fn TEST(NBitCompress, Substr_Mask)
TEST(NBitCompress, Substr_Mask)
{
    std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
    std::string s1, s2;
    std::getline (ofs, s2);
    int i =0;
    while (i<250)
    {
        std::getline (ofs,s2) ;
        s1+=s2;
        ++i;
    }
//	std::string s11 ("ATCGATCGVcbzaabcdAeigeABCDEFGa");
//	std::string s11 ("abBKkabcdgeAAAATTTCeCCGGGNNNNfNNNNAAAAAgAATTTTCCCGGGGGGGGGGGCGATCGAbCdefabGCcXDgWQwtozsqDerqGKEROvdskojRMvcbzaeAfMACCFGG");
	std::string sstr;
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK>	> g11 (s1); //("ATCGatcgabcdefgCXDWQRMM");
//	std::cerr<<g11.GetSubStr(3, 23)<<std::endl;
	g11.GetSubStr (sstr, 5, 11959);
	EXPECT_EQ (s1.substr(5, 11959), sstr);//g11.GetSubStr(3,23));
}
/// @fn TEST(NBitCompress, GetSize)
TEST(NBitCompress, GetSize)
{
	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
//	std::string s1 ("abcde");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > z ( s1 );
	EXPECT_EQ ( z.GetSize(), s1.size() * 6);//s1.size()<<1 );
}
TEST(NBitCompress, Printer)
{
	std::string s2 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");
//  	std::ifstream ofs ("/Users/obigbando/Downloads/chr1.fa");
//	std::string s1, s2;
//	std::getline (ofs, s1);
//	for (auto i=0; i!=100000; ++i)
//	{
//		std::getline (ofs, s1);
//		s2+=s1;
//	}
	NBitCompress< CompressRuleTrait<2, CompressType::N_BITS_MASK> > z ( s2 );
}

#include "../src/file_reader_impl.hpp"

TEST(NBitCompress, serialize_NBITS)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > z (std::get<1>(x.data)), z2("");

//	std::string s2 ("RAdGrbEFD1EqrREqeqrRFbeADAGGGGCTNAbCdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");
//	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS> > z (s2), z2("");
//	std::cerr<<"\t sizeof NBitCompress "<<sizeof(z)<<std::endl;

	std::ofstream out_file ("/Users/obigbando/Downloads/nbit_test.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	arc & z;
	out_file.close();

	std::cerr<<"done archive saving"<<std::endl;
	std::ifstream in_file ("/Users/obigbando/Downloads/nbit_test.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	arci & z2;
//	z2.Printer();

    std::cerr<<"A"<<std::endl;
    EXPECT_EQ (z, z2);
    std::cerr<<"A"<<std::endl;
    EXPECT_EQ (z.GetSeq(), z2.GetSeq());
    std::cerr<<"A"<<std::endl;
    EXPECT_EQ (z.GetExceptDeq(), z2.GetExceptDeq());
    std::cerr<<"A"<<std::endl;
    EXPECT_EQ (z.CharNormal, z2.CharNormal);
    std::cerr<<"A"<<std::endl;
    EXPECT_EQ (z.MakeSeqString(), z2.MakeSeqString());
    std::cerr<<"A"<<std::endl;
//	EXPECT_EQ (z2.MakeSeqString(), std::get<1>(x.data));
//	EXPECT_EQ (z2.MakeSeqString(), s2);
}


TEST(NBitCompress, serialize_MASK1)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chrY.fa");
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > z (std::get<1>(x.data)), z2;

//	std::string s1, s2;
//	std::getline (ofs, s1);
//	int i =0;
//	while (i<250)
//	{
//		std::getline (ofs,s1) ;
//		s2+=s1;
//		++i;
//	}
//	std::string s2 ("ACGTacgtNNacgtAacgtNNRadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");
//	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > z (s2), z2("");

	std::ofstream out_file ("/Users/obigbando/Downloads/nbitmask_test.fa", std::ios_base::binary);
	boost::archive::binary_oarchive arc (out_file);
	arc & z;
	out_file.close();

	std::cerr<<"\ndone archive saving\n"<<std::endl;
	std::ifstream in_file ("/Users/obigbando/Downloads/nbitmask_test.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	arci & z2;
	std::cerr<<"maskdeq's size "<<z2.GetMaskDeq().size()<<std::endl;

	std::cerr<<"A"<<std::endl;
	EXPECT_EQ (z, z2);
	std::cerr<<"A"<<std::endl;
	EXPECT_EQ (z.GetSeq(), z2.GetSeq());
	std::cerr<<"A"<<std::endl;
	EXPECT_EQ (z.GetExceptDeq(), z2.GetExceptDeq());
	std::cerr<<"A"<<std::endl;
	EXPECT_EQ (z.CharNormal, z2.CharNormal);
	std::cerr<<"A"<<std::endl;
	EXPECT_EQ (z.MakeSeqString(), z2.MakeSeqString());
	std::cerr<<"A"<<std::endl;
//	EXPECT_EQ (z2.MakeSeqString(), s2);
//	EXPECT_EQ (z.MakeSeqString(), s2);
	EXPECT_EQ (z.MakeSeqString(), std::get<1>(x.data));
//std::cerr<<"z.MakeSeqString()\n"<<z.MakeSeqString()<<std::endl;
//std::cerr<<"x.data's first string\n"<<std::get<1>(x.data)<<std::endl;
	in_file.close();
}

/*
TEST(NBitCompress, serialize_2)
{
	std::ifstream ofs ("/Users/obigbando/Downloads/chr1.fa");
	auto x = FileReader_impl < Fasta, std::tuple <std::string, std::string > > :: get_next_entry (ofs);
	NBitCompress<CompressRuleTrait<2, CompressType::N_BITS_MASK> > z2("");

	std::ifstream in_file ("/Users/obigbando/Downloads/chr1cc2.fa", std::ios_base::binary);
	boost::archive::binary_iarchive arci (in_file);
	arci & z2;
	EXPECT_EQ (z2.MakeSeqString(), std::get<1>(x.data));
}

// Complex functions
// @fn TEST(NBitCompress, reverse_Normal)
TEST(NBitCompress, reverse_Normal)
{
	std::string s12 ("ATCGATCGAbCdefGCXDWQRMM");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g12 (s12); //("ATCGatcgabcdefgCXDWQRMM");
	g12.reverse();
	std::reverse (s12.begin(), s12.end() );
	std::string sstr;
	g12.MakeSeqString(sstr);
	EXPECT_EQ ( sstr, s12);
}
/// @fn TEST(NBitCompress, reverse_Mask)
TEST(NBitCompress, reverse_Mask)
{
	std::string s1 ("ATCGRREGVVCCDDabc");
//	std::string s1 ("RadgrbEFD1EqrREqeqrRFbeaDAGGGGCTNabcdefNNNNACGTAGCTTQQQQQQATHEEqzvjklqqeioroiE");//nabcdefgNNNNNNNNTVVWWZZXXQQfqpoeijrqeopirjqopbijq");
	NBitCompress< CompressRuleTrait <6, CompressType::N_BITS_MASK> > g12 ( s1 );
	g12.reverse();
	std::reverse (s1.begin(), s1.end() );
	std::string sstr;
	g12.MakeSeqString(sstr);
	EXPECT_EQ (sstr, s1);
}
/// @fn TEST(NBitCompress, complement_Normal)
TEST(NBitCompress, complement_Normal)
{
	std::string s12 ("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g12 (s12), z (s12); 
	g12.complement();
	for (auto i = 0; i != s12.size()*g12.TraitObject.bit_count; ++i)
	{
		EXPECT_EQ (g12.GetSeq()[i], (z.GetSeq()[i] ^= 1) );
	}
}
/// @fn TEST(NBitCompress, complement_Mask)
TEST(NBitCompress, complement_Mask)
{
	std::string s12 ("ATCGatcgabcdefgCXDWQRMM");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g122 (s12), z (s12);
	g122.complement();
	for (auto i = 0; i != s12.size()*g122.TraitObject.bit_count; ++i)
	{
		EXPECT_EQ (g122.GetSeq()[i], (z.GetSeq()[i] ^= 1) );
	}
};
/// @fn TEST(NBitCompress, reverse_complement_Normal)
TEST(NBitCompress, reverse_complement_Normal)
{
	std::string s1 ("aNewrAJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS> > g12 (s1), z (s1); 
	g12.reverse_complement();
//	auto l = s1.size() << 1;
	auto l = s1.size() * g12.TraitObject.bit_count;	//i.e. non-type template parameter N of CompressRuleTrait
	auto index = 0;
	while (index < l)
	{
		EXPECT_EQ (g12.GetSeq()[index], (z.GetSeq()[l-index-g12.TraitObject.bit_count] ^=1));
		index+=g12.TraitObject.bit_count;
	}
}
/// @fn TEST(NBitCompress, reverse_complement_Mask)
TEST(NBitCompress, reverse_complement_Mask)
{
	std::string s1 ("aNewrAJOBIJDFWwefEOJwrqCGqqrTNNNACGTTGCANGNANCNTNNNA");
	NBitCompress<CompressRuleTrait<6, CompressType::N_BITS_MASK>	> g12 (s1), z (s1); 
	g12.reverse_complement();
//	auto l = s1.size() << 1;
	auto l = s1.size() * g12.TraitObject.bit_count;
//	std::string str1;
//	z.MakeSeqString(str1);
	auto index = 0;
	while (index < l)
	{
		EXPECT_EQ (g12.GetSeq()[index], (z.GetSeq()[l-index-g12.TraitObject.bit_count] ^=1));
		index+=g12.TraitObject.bit_count;
	}
};
/// @fn TEST(NBitCompress, reverse_copy_Normal)
TEST(NBitCompress, reverse_copy_Normal)
{
	std::string s1 ("NATCGNAbCGJIOEROTGHDSDFHJIERWEYITYAJREbvxzlpWEOIJGGCC");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS>	> c ( s1 );
//	auto str = c.MakeSeqString();
	auto gg = c.reverse_copy();
	std::string str3;
	gg.MakeSeqString(str3);
//	auto str3 = gg.MakeSeqString();
//	auto str4 = c.MakeTWBString();
	for (auto i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}   
/// @fn TEST(NBitCompress, reverse_copy_Mask)
TEST(NBitCompress, reverse_copy_Mask)
{
	std::string s1 ("NATCGNAbCGJIOEROacgjqqeTbjfqerGHDSDFHJIERWEYITYAJREbvxzlpWEOIJGGCC");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > c ( s1 );
	auto gg = c.reverse_copy();
	std::string str3;
	gg.MakeSeqString(str3);
//	auto str3 = gg.MakeSeqString();
	for (auto i = 0; i!=s1.size(); ++i)
		EXPECT_EQ (str3[i], s1[(s1.size()-i-1)]);
}   
/// @fn TEST(NBitCompress, complement_copy_Normal)
TEST(NBitCompress, complement_copy_Normal)
{   
	std::string s1 ("NATCGNAbCdQWWQQVVXMCDJFQHGzvbAAGGGTTT");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS> > c ( s1 ), cc (s1);
	auto l = s1.size()*c.TraitObject.bit_count;
	auto gg = c.complement_copy();
	for (auto i = 0; i != l; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (gg.GetSeq()[i] ^= 1) );
	}   
}	   
/// @fn TEST(NBitCompress, complement_copy_Mask)
TEST(NBitCompress, complement_copy_Mask)
{   
//  std::cerr<<"\ncomplement_copy test"<<std::endl;
	std::string s1 ("NATCGNabcdQWWQQVVXMCDJFQHGzvbAAGGGTTT");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > c ( s1 ), cc (s1);
	auto l = s1.size()*c.TraitObject.bit_count;
	auto gg = c.complement_copy();
	for (auto i = 0; i != l; ++i)
	{
		EXPECT_EQ (c.GetSeq()[i], (gg.GetSeq()[i] ^= 1) );
	}   
}	   
/// @fn TEST(NBitCompress, reverse_complement_copy_Normal)
TEST(NBitCompress, reverse_complement_copy_Normal)
{
	std::string s1 ("NAaCabGcTfNNgqNqAeCrGTTqGeCAqNebGqNANGNJNCVOJEOICNWEFIJOINBJFTNNNA");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS>	> c ( s1 ), cc (s1);
	auto l = s1.size()* c.TraitObject.bit_count;
	auto gg = c.reverse_complement_copy();
	auto index = 0;
	while (index < l)
	{
		EXPECT_EQ ( gg.GetSeq()[index], (cc.GetSeq()[l-index-c.TraitObject.bit_count] ^= 1) );
		index += c.TraitObject.bit_count;
	}
}
/// @fn TEST(NBitCompress, reverse_complement_copy_Mask)
TEST(NBitCompress, reverse_complement_copy_Mask)
{
	std::string s1 ("NAaCabGcTfNNgqNqAeCrGTTqGeCAqNebGqNANGNJNCVOJEOICNWEFIJOINBJFTNNNA");
	NBitCompress< CompressRuleTrait<6, CompressType::N_BITS_MASK> > c ( s1 ), cc (s1);
	auto l = s1.size()* c.TraitObject.bit_count;
	auto gg = c.reverse_complement_copy();
	auto index = 0;
	while (index < l)
	{
		EXPECT_EQ ( gg.GetSeq()[index], (cc.GetSeq()[l-index-c.TraitObject.bit_count] ^= 1) );
		index += c.TraitObject.bit_count;
	}
}
*/
