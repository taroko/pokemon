#include "gtest/gtest.h"
#include "../src/format/curl_recv_wrapper.hpp"
#include <fstream>
#include <iostream>

TEST (curl_gz, get_from_url_impl_MT)
{
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex>* > TT
	({
		new CurlWrapper <CompressFormat::GZ>//, curl_default_handle_mutex > 
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 ),
		new CurlWrapper <CompressFormat::GZ>//, curl_default_handle_mutex > 
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 )
	});

	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, i] ()
				{
				TT[i]->get_from_url_impl ();//stri);
				for (auto& Q: TT[i]->curl_in_thread_vec_)
					Q.join();
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
		Q.join();
}

TEST (curl_gz, get_from_url_impl_MT_no_file_size)
{
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex>* > TT2
	({
		new CurlWrapper <CompressFormat::GZ>//, curl_default_handle_mutex > 
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7" ),
		new CurlWrapper <CompressFormat::GZ>//, curl_default_handle_mutex > 
			("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7" )
	});
	std::vector < std::thread > qvec2;
	for (auto i=0; i!=TT2.size(); ++i)
	{
		qvec2.push_back (
			std::move ( 
				std::thread (
				[&TT2, i] ()
				{
				TT2[i]->get_from_url_impl ();//stri);
				for (auto& Q: TT2[i]->curl_in_thread_vec_)
					Q.join();
				}
				)
			)
		);
	}
	for (auto& Q : qvec2)
		Q.join();
}

TEST (curl_gz, get_n_parse_single)
{
//  maximum virt 1G/ res 600mb for file1 
//  maximum virt 1.6G/ res 900mb for file 2
//  when the stri is defined inside the while loop
	std::ofstream q1 ("test_file/curl_gz_single_file1");
	INITIALIZE_CURL ();
	CurlWrapper < CompressFormat::GZ, curl_default_handle_mutex > QQ
//		("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 );
		("http://localhost/test/big.fq.gz", 7353346);
	QQ.get_from_url_impl ();//stri);
	while ( !QQ.terminate_flag_ )
	{
		std::stringstream stri;
		int offset_1 = stri.tellg(), offset_2 = stri.tellp();
		QQ.parse (stri);
		q1 << stri.str();
	}
}

TEST (curl_gz, get_n_parse_single_no_file_size)
{
	std::ofstream q1 ("test_file/curl_gz_single_file1_no_file_size");
	INITIALIZE_CURL ();
	CurlWrapper < CompressFormat::GZ, curl_default_handle_mutex > QQ
//		("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 );
		("http://localhost/test/big.fq.gz");
	QQ.get_from_url_impl ();
	while ( !QQ.terminate_flag_ )
	{
		std::stringstream stri;
		int offset_1 = stri.tellg(), offset_2 = stri.tellp();
		QQ.parse (stri);
		q1 << stri.str();
	}
}

TEST (curl_gz, get_n_parse_MT)
{
	std::ofstream qq1 ("test_file/curl_gz_MT_file1"), qq2("test_file/curl_gz_MT_file2");
	std::vector <std::ofstream*> qv ({	&qq1, &qq2	});
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex>* > TT
	({
        new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex >
		("http://localhost/test/big.fq.gz", 7353346),
		//("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 ),
        new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex >
		("http://localhost/test/big.fq.gz", 7353346)
		//("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 )
	});

	std::vector < std::thread > qvec;
	std::vector <std::stringstream> stri (2);
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, i, &qv, &stri] ()
				{
					TT[i]->get_from_url_impl ();
					while ( !TT[i]->terminate_flag_ )
					{
						TT[i]->parse (stri[i]);
						(*qv[i]) << stri[i].str();
						stri[i].str("");
					}
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
	{
		Q.join();
	}
	for (auto& q : TT)
		delete q;
//	for (auto& q : qv)
//		delete q;
}

TEST (curl_gz, get_n_parse_MT_no_file_size)
{
	std::ofstream qq1 ("test_file/curl_gz_MT_file1_no_file_size"), qq2("test_file/curl_gz_MT_file2_no_file_size");
	std::vector <std::ofstream*> qv ({	&qq1, &qq2	});
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex>* > TT
	({
        new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex >
		("http://localhost/test/big.fq.gz"),
		//("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 ),
        new CurlWrapper <CompressFormat::GZ, curl_default_handle_mutex >
		("http://localhost/test/big.fq.gz")
		//("https://api.basespace.illumina.com/v1pre3/files/535642/content?access_token=7b390a45253943ac83c047a0b9372ac7", 7493990 )
	});

	std::vector < std::thread > qvec;
	std::vector <std::stringstream> stri (2);
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, i, &qv, &stri] ()
				{
					TT[i]->get_from_url_impl ();
					while ( !TT[i]->terminate_flag_ )
					{
						TT[i]->parse (stri[i]);
						(*qv[i]) << stri[i].str();
						stri[i].str("");
					}
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
	{
		Q.join();
	}
	for (auto& q : TT)
		delete q;
}

TEST (curl_plain, get_from_url_impl_MT)
{
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex>* > TT
	({
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("http://localhost/test/plain.fq"),
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("http://localhost/test/plain.fq")
	});

	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, i] ()
				{
				TT[i]->get_from_url_impl ();//stri);
				for (auto& Q: TT[i]->curl_in_thread_vec_)
					Q.join();
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
		Q.join();
}

TEST (curl_plain, get_n_parse_single)
{
	std::ofstream qq ("test_file/curl_plain_single_file");
	INITIALIZE_CURL ();
	CurlWrapper < CompressFormat::PLAIN, curl_default_handle_mutex > QQ
	("http://localhost/test/plain.fq");
	std::stringstream stri;
	QQ.get_from_url_impl ();
	int offset_1 = stri.tellg(), offset_2 = stri.tellp();
	while ( !QQ.terminate_flag_ )
	{
		QQ.parse (stri);
		qq << stri.str();
		//std::cerr<<"keep parsing stri's length "<<stri.tellp()-stri.tellg()<<std::endl;
	}
}

TEST (curl_plain, get_n_parse_MT)
{
	std::ofstream qq1 ("test_file/curl_plain_MT_file1"), qq2("test_file/curl_plain_MT_file2");
	std::vector <std::ofstream*> qv
	({	&qq1, &qq2	});
	INITIALIZE_CURL ();
	std::vector < CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex>* > TT
	({
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("http://localhost/test/plain.fq"),
		new CurlWrapper <CompressFormat::PLAIN, curl_default_handle_mutex > ("http://localhost/test/plain.fq")
	});

	std::vector <std::stringstream> stri (2);
	std::vector < std::thread > qvec;
	for (auto i=0; i!=TT.size(); ++i)
	{
		qvec.push_back (
			std::move ( 
				std::thread (
				[&TT, &stri, i, &qv] ()
				{
					TT[i]->get_from_url_impl ();
					while ( !TT[i]->terminate_flag_ )
					{
						TT[i]->parse (stri[i]);
						(*qv[i]) << stri[i].str();
					}
					//std::cerr<<"amargadon"<<std::endl;
				}
				)
			)
		);
	}
	for (auto& Q : qvec)
	{
		//std::cerr<<"joning"<<std::endl;
		Q.join();
	}
	for (auto& q : TT)
		delete q;
//	for (auto& q : qv)
//		delete q;
}

