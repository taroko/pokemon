#include <tuple>
#include <vector>
#include <string>
#include <cstdint>
#include <iostream>
#include <fstream>
#include "gtest/gtest.h"
#include "../src/format/wig.hpp"
#include "../src/tuple_utility.hpp"
#include "../src/wrapper_tuple_utility.hpp"

TEST(Wig, default_constructor)
{
	size_t a=0;
	int b=0;
	Wig <> TT;
	EXPECT_EQ (std::get<0> (TT.data), std::string("") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (TT.data), std::string("") );
	EXPECT_EQ (std::get<2> (TT.data), b);
	EXPECT_EQ (std::get<3> (TT.data).data_content.size(), a);
}

TEST(Wig, copy_constructor)
{
	typedef  std::tuple < std::string, std::string, int, std::vector < std::tuple <uint32_t, double> > > TUPLETYPE;
	std::string stack_info ("type wiggle_o"), chrom ("VariableStep" );
	int span = 30;
	uint32_t a=100000;
	std::tuple <uint32_t, double > data_tuple (100000, 9.3);
	std::vector < std::tuple <uint32_t, double> > data_content;
	data_content.push_back (data_tuple);
//	bool EofFlag =false;
	TUPLETYPE kk = std::make_tuple ( stack_info, chrom, span, data_content );

	Wig < TUPLETYPE > gg (kk);
	Wig < TUPLETYPE > GG (gg);
	EXPECT_EQ (std::get<0> (GG.data), std::string("type wiggle_o") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (GG.data), std::string("VariableStep") );
	EXPECT_EQ (std::get<2> (GG.data), span);
	EXPECT_EQ (std::get<0>(std::get<3> (GG.data)[0]), a);
	EXPECT_EQ (std::get<1>(std::get<3> (GG.data)[0]), 9.3);
}

TEST(Wig, assignment_constructor)
{
	typedef  std::tuple < std::string, std::string, int, std::vector < std::tuple <uint32_t, double> > > TUPLETYPE;
	std::string stack_info ("type wiggle_o"), chrom ("VariableStep" );
	int span = 30;
	uint32_t a=100000;
	std::tuple <uint32_t, double > data_tuple (100000, 9.3);
	std::vector < std::tuple <uint32_t, double> > data_content;
	data_content.push_back (data_tuple);
//	bool EofFlag (false);
	TUPLETYPE kk = std::make_tuple ( stack_info, chrom, span, data_content );
	Wig <TUPLETYPE> gg ( kk );

	Wig < TUPLETYPE > GG;
	GG = gg;
	EXPECT_EQ (std::get<0> (GG.data), std::string("type wiggle_o") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (GG.data), std::string("VariableStep") );
	EXPECT_EQ (std::get<2> (GG.data), 30);
	EXPECT_EQ (std::get<0>(std::get<3> (GG.data)[0]), a);
	EXPECT_EQ (std::get<1>(std::get<3> (GG.data)[0]), 9.3);
	std::cerr << "" << std::endl;
}

TEST(Wig, move_constructor)
{
    typedef Wrapper< std::tuple<int, double> > TupleType;
    typedef Wrapper< std::vector< TupleType > > VectorType;
    typedef std::tuple < std::string, std::string, int, VectorType > TUPLETYPE;
    TUPLETYPE qq;
    std::vector <std::string> split_temp ({
    "abcdefg",
    "12345",
    "999",
    "*1230#777.65*1220#666.54*1210#555.13"} );
    TupleUtility< TUPLETYPE, 4>::FillTuple ( qq, split_temp );
	Wig <TUPLETYPE> gg ( qq );
	Wig <TUPLETYPE> ZZ (std::move(gg));
	EXPECT_EQ (std::get<0> (ZZ.data), std::string("abcdefg") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (ZZ.data), std::string("12345") );
	EXPECT_EQ (std::get<2> (ZZ.data), 999);
	EXPECT_EQ (std::get<3>(ZZ.data).get<0>().get<0>(), 1230);
	EXPECT_EQ (std::get<3>(ZZ.data).get<0>().get<1>(), 777.65);
	EXPECT_EQ (std::get<3>(ZZ.data).get<1>().get<0>(), 1220);
	EXPECT_EQ (std::get<3>(ZZ.data).get<1>().get<1>(), 666.54);
	EXPECT_EQ (std::get<3>(ZZ.data).get<2>().get<0>(), 1210);
	EXPECT_EQ (std::get<3>(ZZ.data).get<2>().get<1>(), 555.13);
}

TEST(Wig, move_assigment)
{
    typedef Wrapper< std::tuple<int, double> > TupleType;
    typedef Wrapper< std::vector< TupleType > > VectorType;
    typedef std::tuple < std::string, std::string, int, VectorType > TUPLETYPE;
    TUPLETYPE qq;
    std::vector <std::string> split_temp ({
    "abcdefg",
    "12345",
    "999",
    "*1230#777.65*1220#666.54*1210#555.13"} );
    TupleUtility< TUPLETYPE, 4>::FillTuple ( qq, split_temp );
    Wig <TUPLETYPE> gg ( qq );
	Wig <TUPLETYPE> WW;
	WW = std::move (gg);
	EXPECT_EQ (std::get<0> (WW.data), std::string("abcdefg") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (WW.data), std::string("12345") );
	EXPECT_EQ (std::get<2> (WW.data), 999);
	EXPECT_EQ (std::get<3>(WW.data).get<0>().get<0>(), 1230);
	EXPECT_EQ (std::get<3>(WW.data).get<0>().get<1>(), 777.65);
	EXPECT_EQ (std::get<3>(WW.data).get<1>().get<0>(), 1220);
	EXPECT_EQ (std::get<3>(WW.data).get<1>().get<1>(), 666.54);
	EXPECT_EQ (std::get<3>(WW.data).get<2>().get<0>(), 1210);
	EXPECT_EQ (std::get<3>(WW.data).get<2>().get<1>(), 555.13);
}

TEST(Wig, tupletype_copy_constructor)
{
    typedef Wrapper< std::tuple<int, double> > TupleType;
    typedef Wrapper< std::vector< TupleType > > VectorType;
    typedef std::tuple < std::string, std::string, int, VectorType > TUPLETYPE;
    TUPLETYPE qq;
    std::vector <std::string> split_temp ({
    "abcdefg",
    "12345",
    "999",
    "*1230#777.65*1220#666.54*1210#555.13"} );
    TupleUtility< TUPLETYPE, 4>::FillTuple ( qq, split_temp );
	Wig <TUPLETYPE> QQ (qq);
	EXPECT_EQ (std::get<0> (QQ.data), std::string("abcdefg") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (QQ.data), std::string("12345") );
	EXPECT_EQ (std::get<2> (QQ.data), 999);
	EXPECT_EQ (std::get<3>(QQ.data).get<0>().get<0>(), 1230);
	EXPECT_EQ (std::get<3>(QQ.data).get<0>().get<1>(), 777.65);
	EXPECT_EQ (std::get<3>(QQ.data).get<1>().get<0>(), 1220);
	EXPECT_EQ (std::get<3>(QQ.data).get<1>().get<1>(), 666.54);
	EXPECT_EQ (std::get<3>(QQ.data).get<2>().get<0>(), 1210);
	EXPECT_EQ (std::get<3>(QQ.data).get<2>().get<1>(), 555.13);
}

TEST(Wig, tupletype_move_constructor)
{
    typedef Wrapper< std::tuple<int, double> > TupleType;
    typedef Wrapper< std::vector< TupleType > > VectorType;
    typedef std::tuple < std::string, std::string, int, VectorType > TUPLETYPE;
    TUPLETYPE qq;
    std::vector <std::string> split_temp ({
    "abcdefg",
    "12345",
    "999",
    "*1230#777.65*1220#666.54*1210#555.13"} );
    TupleUtility< TUPLETYPE, 4>::FillTuple ( qq, split_temp );
	Wig <TUPLETYPE> QQ (std::move (qq));
	EXPECT_EQ (std::get<0> (QQ.data), std::string("abcdefg") ); ///string is not c_string
	EXPECT_EQ (std::get<1> (QQ.data), std::string("12345") );
	EXPECT_EQ (std::get<2> (QQ.data), 999);
	EXPECT_EQ (std::get<3>(QQ.data).get<0>().get<0>(), 1230);
	EXPECT_EQ (std::get<3>(QQ.data).get<0>().get<1>(), 777.65);
	EXPECT_EQ (std::get<3>(QQ.data).get<1>().get<0>(), 1220);
	EXPECT_EQ (std::get<3>(QQ.data).get<1>().get<1>(), 666.54);
	EXPECT_EQ (std::get<3>(QQ.data).get<2>().get<0>(), 1210);
	EXPECT_EQ (std::get<3>(QQ.data).get<2>().get<1>(), 555.13);
}

TEST(Wig, eof_constructor)
{
	int a=0;
	size_t b=0;
	bool EofFlag = false;
	Wig <> QQ (EofFlag);
	EXPECT_EQ (QQ.eof_flag, false ); ///string is not c_string
	EXPECT_EQ (std::get<0> (QQ.data), "" ); ///string is not c_string
	EXPECT_EQ (std::get<1> (QQ.data), "" );
	EXPECT_EQ (std::get<2> (QQ.data), a);
	EXPECT_EQ (std::get<3> (QQ.data).data_content.size(), b);
}

TEST(Wig, overload_smaller_operator)
{
    typedef Wrapper< std::tuple<int, double> > TupleType;
    typedef Wrapper< std::vector< TupleType > > VectorType;
    typedef std::tuple < std::string, std::string, int, VectorType > TUPLETYPE;
    TUPLETYPE qq;
    std::vector <std::string> split_temp ({
    "abcdefg",
    "12345",
    "999",
    "*1230#777.65*1220#666.54*1210#555.13"} );
    TupleUtility< TUPLETYPE, 4>::FillTuple ( qq, split_temp );
	Wig <TUPLETYPE> QQ (std::move (qq));

    std::ostringstream s1;
    s1 << QQ;
    std::string s2 = s1.str();
    EXPECT_EQ ( s2, "abcdefg\n12345\n999\n1230\n777.65\n1220\n666.54\n1210\n555.13\n");
}

TEST(Wig, serialization)
{
	typedef std::tuple <  std::string, 
						std::string,  //chrom
						int, //span
						Wrapper< std::vector < 
							Wrapper< std::tuple<uint32_t, double> > > > > TUPLETYPE;
	std::string stack_info ("type wiggle_o"), chrom ("VariableStep" );
	int span = 30;
	uint32_t a = 100000;
	std::tuple <uint32_t, double > unit_tuple (100000, 9.3);
	Wrapper < std::tuple<uint32_t, double> > Wrapper0 ( unit_tuple ); 
	std::vector < Wrapper <std::tuple < uint32_t, double > > > unit_vector;
	unit_vector.push_back (Wrapper0);
	Wrapper < std::vector < Wrapper <std::tuple <uint32_t, double> > > > Wrapper1 ( unit_vector );
	auto wig_tuple = std::make_tuple ( stack_info, chrom, span, Wrapper1 );
	Wig <> cc ( wig_tuple );
	Wig <> CC;
// Serialization
	std::ofstream ofs ("/Users/obigbando/Documents/work/test_file/wig_serialization_test.txt");
	boost::archive::text_oarchive arc0 (ofs);
	arc0 & cc;
	ofs.close();
	std::string str_temp;
	std::ifstream ifs ("/Users/obigbando/Documents/work/test_file/wig_serialization_test.txt");
	boost::archive::text_iarchive arc1 (ifs);
	arc1 & CC;
    EXPECT_EQ (std::get<0>(CC.data), "type wiggle_o");
    EXPECT_EQ (std::get<1>(CC.data), "VariableStep");
    EXPECT_EQ (std::get<2>(CC.data), span);
	EXPECT_EQ (std::get<3>(CC.data).get<0>().get<0>(), a);
	EXPECT_EQ (std::get<3>(CC.data).get<0>().get<1>(), 9.3);
};
